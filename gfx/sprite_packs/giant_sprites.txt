giant_sprite_pack = {
	# Tests against every tag in game
	country_trigger = {
		culture_group = giant_cg
	}
	
	sprite_infantry = {
		0 = GIANT_INFANTRY_1
		1 = GIANT_INFANTRY_1
		2 = GIANT_INFANTRY_1
		3 = GIANT_INFANTRY_1
	}
	
	sprite_cavalry = {
		0 = GIANT_INFANTRY_1
		1 = GIANT_INFANTRY_1
		2 = GIANT_INFANTRY_1
		3 = GIANT_INFANTRY_1
	}
	sprite_artillery = {
		0 = GIANT_INFANTRY_1
		1 = GIANT_INFANTRY_1
		2 = GIANT_INFANTRY_1
		3 = GIANT_INFANTRY_1
	}

}