kapotun_sprite_pack = {
	# Tests against every tag in game
	country_trigger = {
		OR = {
			culture_group = po_tun_cg
			culture_group = tangmo_cg # Till monkeys are added
		}
	}
	
	sprite_infantry = {
		0 = KAPOTUN_INFANTRY_1
		1 = KAPOTUN_INFANTRY_1
		2 = KAPOTUN_INFANTRY_1
		3 = KAPOTUN_INFANTRY_1
	}
	
	sprite_cavalry = {
		0 = KAPOTUN_INFANTRY_1
		1 = KAPOTUN_INFANTRY_1
		2 = KAPOTUN_INFANTRY_1
		3 = KAPOTUN_INFANTRY_1
	}
	sprite_artillery = {
		0 = KAPOTUN_INFANTRY_1
		1 = KAPOTUN_INFANTRY_1
		2 = KAPOTUN_INFANTRY_1
		3 = KAPOTUN_INFANTRY_1
	}

}