velothi_sprite_pack = {
	# Tests against every tag in game
	country_trigger = {
		OR = {
			primary_culture = ashlander_chimer
			primary_culture = cantemiric
		}
	}
	
	sprite_infantry = {
		0 = VELOTHI_INFANTRY_1
		1 = VELOTHI_INFANTRY_1
		2 = VELOTHI_INFANTRY_1
		3 = VELOTHI_INFANTRY_1
	}
	
	sprite_cavalry = {
		0 = VELOTHI_INFANTRY_1
		1 = VELOTHI_INFANTRY_1
		2 = VELOTHI_INFANTRY_1
		3 = VELOTHI_INFANTRY_1
	}
	sprite_artillery = {
		0 = VELOTHI_INFANTRY_1
		1 = VELOTHI_INFANTRY_1
		2 = VELOTHI_INFANTRY_1
		3 = VELOTHI_INFANTRY_1
	}

}