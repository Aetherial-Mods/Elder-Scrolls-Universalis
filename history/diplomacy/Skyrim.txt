##### Dawn of the First Era #####

# Nords
alliance = {
	first = NA9 
	second = NE9
	start_date = 57.1.1
	end_date = 9999.1.1
}

march = {
	first = NF1 
	second = NB8
	start_date = 57.1.1
	end_date = 9999.1.1
}

march = {
	first = NB6 
	second = NC1
	start_date = 57.1.1
	end_date = 9999.1.1
}

vassal = {
	first = NF2
	second = NE6
	start_date = 57.1.1
	end_date = 9999.1.1
}

vassal = {
	first = NA3
	second = NE1
	start_date = 57.1.1
	end_date = 9999.1.1
}

dependency = {
	subject_type = "holy_order_1"
	first = NA3
	second = ND9
	start_date = 57.1.1
	end_date = 110.1.1
}

# Reachmen subjects of the Nighthollow Clan
dependency = {
	subject_type = "tributary_state"
	first = NGH
	second = NA7
	start_date = 57.1.1
	end_date = 110.1.1
}

dependency = {
	subject_type = "tributary_state"
	first = NGH
	second = ND2
	start_date = 57.1.1
	end_date = 110.1.1
}

dependency = {
	subject_type = "tributary_state"
	first = NGH
	second = NC7
	start_date = 57.1.1
	end_date = 110.1.1
}

dependency = {
	subject_type = "tributary_state"
	first = NGH
	second = ND0
	start_date = 57.1.1
	end_date = 110.1.1
}

##### Second Dragon War #####

# Nords
dependency = {
	subject_type = "alliance_member"
	first = NA8 
	second = WIT
	start_date = 91.1.1
	end_date = 9999.1.1
}

##### Second Nordic-Elven War #####

# Snow Elves
union = {
	first = SNW 
	second = NG7
	start_date = 126.1.1
	end_date = 152.1.1
}

# Reachmen
dependency = {
	subject_type = "alliance_member"
	first = REA
	second = ND2
	start_date = 126.1.1
	end_date = 9999.1.1
}

dependency = {
	subject_type = "alliance_member"
	first = REA
	second = NA7
	start_date = 126.1.1
	end_date = 9999.1.1
}

dependency = {
	subject_type = "alliance_member"
	first = REA
	second = ND0
	start_date = 126.1.1
	end_date = 9999.1.1
}

##### Narfinsel Schism #####

# Nords
vassal = {
	first = SKY
	second = NA1
	start_date = 152.1.1
	end_date = 9999.1.1
}

vassal = {
	first = SKY
	second = NA5
	start_date = 152.1.1
	end_date = 9999.1.1
}