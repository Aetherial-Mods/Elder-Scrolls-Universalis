##### Second Dragon War #####

# Nords

alliance = {
	first = MA1
	second = SKA
	start_date = 57.1.1
	end_date = 152.1.1
}

##### Narfinsel Schism #####

# Skaals

vassal = {
	first = SKY
	second = SKA
	start_date = 152.1.1
	end_date = 9999.1.1
}

#Tonal Architects

vassal = {
	first = TON
	second = MG6		#Nchuleft
	start_date = 57.1.1
	end_date = 9999.1.1
}