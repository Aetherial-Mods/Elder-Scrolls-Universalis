union = {
	first = PA4
	second = PC1
	start_date = 57.1.1
	end_date = 9999.1.1
}

vassal = {
	first = PA1
	second = PB7
	start_date = 57.1.1
	end_date = 9999.1.1
}

vassal = {
	first = PC2
	second = PA8
	start_date = 57.1.1
	end_date = 9999.1.1
}

vassal = {
	first = PB4
	second = PA7
	start_date = 57.1.1
	end_date = 9999.1.1
}

dependency = {
    subject_type = holy_order_1
    first = PA5
    second = PB9
    start_date = 57.1.1
	end_date = 9999.1.1
}

alliance = {
	first = PB6
	second = PB5
	start_date = 57.1.1
	end_date = 9999.1.1
}

alliance = {
	first = PB8
	second = PA1
	start_date = 57.1.1
	end_date = 9999.1.1
}

alliance = {
	first = PB8
	second = PA2
	start_date = 57.1.1
	end_date = 9999.1.1
}

alliance = {
	first = PA3
	second = PA2
	start_date = 57.1.1
	end_date = 9999.1.1
}

alliance = {
	first = PB8
	second = PA3
	start_date = 57.1.1
	end_date = 9999.1.1
}

alliance = {
	first = PA5
	second = PA3
	start_date = 57.1.1
	end_date = 9999.1.1
}

alliance = {
	first = PA5
	second = PC0
	start_date = 57.1.1
	end_date = 9999.1.1
}

alliance = {
	first = PA6
	second = PC2
	start_date = 57.1.1
	end_date = 9999.1.1
}

alliance = {
	first = PA9
	second = PA4
	start_date = 57.1.1
	end_date = 9999.1.1
}

alliance = {
	first = PA9
	second = PB0
	start_date = 57.1.1
	end_date = 9999.1.1
}

alliance = {
	first = PB2
	second = PB0
	start_date = 57.1.1
	end_date = 9999.1.1
}

alliance = {
	first = PB3
	second = PB0
	start_date = 57.1.1
	end_date = 9999.1.1
}

alliance = {
	first = PC3
	second = PB1
	start_date = 57.1.1
	end_date = 9999.1.1
}

alliance = {
	first = PB3
	second = PB4
	start_date = 57.1.1
	end_date = 9999.1.1
}

alliance = {
	first = PA4
	second = PB4
	start_date = 57.1.1
	end_date = 9999.1.1
}

alliance = {
	first = PC0
	second = PA9
	start_date = 57.1.1
	end_date = 9999.1.1
}
