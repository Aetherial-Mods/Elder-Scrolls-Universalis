##### Dawn of the First Era #####

# Bosmer Treethanes
dependency = {
	subject_type = "treethane"
	first = VA3
	second = VC7
	start_date = 57.1.1
	end_date = 91.1.1
}

dependency = {
	subject_type = "treethane"
	first = VA3
	second = VD0
	start_date = 57.1.1
	end_date = 9999.1.1
}

dependency = {
	subject_type = "treethane"
	first = VA3
	second = VA7
	start_date = 57.1.1
	end_date = 9999.1.1
}

dependency = {
	subject_type = "treethane"
	first = VA3
	second = VB6
	start_date = 57.1.1
	end_date = 9999.1.1
}

dependency = {
	subject_type = "treethane"
	first = VA3
	second = VA1
	start_date = 57.1.1
	end_date = 9999.1.1
}

dependency = {
	subject_type = "treethane"
	first = VA3
	second = VA4
	start_date = 57.1.1
	end_date = 9999.1.1
}

dependency = {
	subject_type = "treethane"
	first = VA3
	second = VB9
	start_date = 57.1.1
	end_date = 9999.1.1
}

dependency = {
	subject_type = "treethane"
	first = VA3
	second = VA2
	start_date = 57.1.1
	end_date = 9999.1.1
}

dependency = {
	subject_type = "treethane"
	first = VA3
	second = VC4
	start_date = 57.1.1
	end_date = 9999.1.1
}

dependency = {
	subject_type = "treethane"
	first = VA3
	second = VB5
	start_date = 57.1.1
	end_date = 9999.1.1
}

dependency = {
	subject_type = "treethane"
	first = VA3
	second = VA7
	start_date = 57.1.1
	end_date = 9999.1.1
}

dependency = {
	subject_type = "treethane"
	first = VA3
	second = VC1
	start_date = 57.1.1
	end_date = 9999.1.1
}

dependency = {
	subject_type = "treethane"
	first = VA3
	second = EC7
	start_date = 57.1.1
	end_date = 9999.1.1
}

dependency = {
	subject_type = "treethane"
	first = VA3
	second = VA9
	start_date = 57.1.1
	end_date = 9999.1.1
}

dependency = {
	subject_type = "treethane"
	first = VA3
	second = VB0
	start_date = 57.1.1
	end_date = 9999.1.1
}

dependency = {
	subject_type = "treethane"
	first = VA3
	second = VA8
	start_date = 57.1.1
	end_date = 9999.1.1
}

dependency = {
	subject_type = "treethane"
	first = VA3
	second = VB7
	start_date = 57.1.1
	end_date = 9999.1.1
}

dependency = {
	subject_type = "treethane"
	first = VA3
	second = VB8
	start_date = 57.1.1
	end_date = 9999.1.1
}

dependency = {
	subject_type = "treethane"
	first = VA3
	second = VC9
	start_date = 57.1.1
	end_date = 9999.1.1
}

# Bosmers
alliance = {
	first = VA3
	second = VB1
	start_date = 57.1.1
	end_date = 9999.1.1
}

alliance = {
	first = VA3
	second = VC8
	start_date = 57.1.1
	end_date = 9999.1.1
}

alliance = {
	first = VA3
	second = VA6
	start_date = 57.1.1
	end_date = 9999.1.1
}

# Khajiits
alliance = {
	first = ED2
	second = VD5
	start_date = 57.1.1
	end_date = 9999.1.1
}

# Ayleids
alliance = {
	first = EB9
	second = HF1
	start_date = 57.1.1
	end_date = 9999.1.1
}

# Altmer Colonies
march = {
	first = VC0
	second = VE0
	start_date = 57.1.1
	end_date = 9999.1.1
}

##### Second Dragon War #####

# Bosmers
union = {
	first = VA3
	second = VC7
	start_date = 91.1.1
	end_date = 9999.1.1
}