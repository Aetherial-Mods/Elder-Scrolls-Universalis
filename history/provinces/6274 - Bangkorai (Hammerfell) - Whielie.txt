religion = druidism
culture = horsemen
base_tax = 1
base_production = 1
trade_goods = unknown
base_manpower = 1
capital = "Whielie"
is_city = yes
owner = HF3
controller = HF3
add_core = HF3
add_permanent_province_modifier = { name = "land_trade_modifier" duration = -1 }