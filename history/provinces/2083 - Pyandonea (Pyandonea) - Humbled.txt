owner = PB3
controller = PB3
add_core = PB3
religion = serpant_king
culture = maormer
hre = no
base_tax = 4
base_production = 4
trade_goods = unknown
base_manpower = 4
capital = "Humbled"
is_city = yes
fort_15th = yes
add_permanent_province_modifier = { name = "water_trade_modifier" duration = -1 }
57.1.1 = {
	owner = PB3
	controller = PB3
	add_core = PB3
}