religion = sheogorath_cult
culture = ayleid
hre = no
base_tax = 1
base_production = 1
trade_goods = unknown
base_manpower = 1
capital = "Hawkmoth"
owner = SOT
controller = SOT
add_core = SOT
is_city = yes
add_province_triggered_modifier = ihinipalit_province
57.1.1 = { owner = --- controller = --- }
190.1.1 = { owner = SOT controller = SOT add_core = SOT }