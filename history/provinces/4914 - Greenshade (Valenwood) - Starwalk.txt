religion = bosmer_pantheon
culture = bosmer
hre = no
base_tax = 2
base_production = 2
trade_goods = unknown
base_manpower = 2
capital = "Starwalk"
owner = VD9
controller = VD9
add_core = VD9
is_city = yes
add_permanent_province_modifier = { name = "water_trade_modifier" duration = -1 }