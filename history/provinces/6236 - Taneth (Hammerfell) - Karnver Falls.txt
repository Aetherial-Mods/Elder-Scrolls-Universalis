religion = redguard_pantheon
culture = redguard
hre = no
base_tax = 1
base_production = 1
trade_goods = unknown
base_manpower = 1
capital = "Karnver Falls"
owner = HE4
controller = HE4
is_city = yes
add_core = HE4
fort_15th = yes
center_of_trade = 1
57.1.1 = {
	religion = malacath_cult
	culture = giant_goblin
	owner = ---
	controller = ---
	remove_core = HE4
}

