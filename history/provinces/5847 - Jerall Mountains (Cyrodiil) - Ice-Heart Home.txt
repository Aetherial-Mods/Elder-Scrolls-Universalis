religion = meridia_cult
culture = ayleid
hre = no
base_tax = 3
base_production = 3
trade_goods = unknown
base_manpower = 3
capital = "Ice-Heart Home"
owner = CE1
controller = CE1
is_city = yes
add_core = CE1