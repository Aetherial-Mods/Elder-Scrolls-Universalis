religion = meridia_cult
culture = ayleid
hre = no
base_tax = 4
base_production = 5
trade_goods = magic_goods
base_manpower = 2
capital = "The Arcane University"
owner = CB8
controller = CB8
add_core = CB8
is_city = yes
add_permanent_province_modifier = { name = "water_trade_modifier" duration = -1 }
add_permanent_province_modifier = { name = "land_trade_modifier" duration = -1 }
57.1.1 = { owner = CH1 controller = CH1 add_core = CH1 } 
152.1.1 = { owner = AYL controller = AYL add_core = AYL } 