owner = MF8
controller = MF8
add_core = MF8
religion = chimer_pantheon
culture = ashlander_chimer
hre = no
base_tax = 1
base_production = 1
trade_goods = unknown
base_manpower = 1
capital = "Berandas"
is_city = yes
add_permanent_province_modifier = { name = "water_trade_modifier" duration = -1 }
57.1.1 = { owner = --- controller = --- }
190.1.1 = { owner = MF8 controller = MF8 add_core = MF8 }