religion = meridia_cult
culture = ayleid
hre = no
base_tax = 2
base_production = 2
trade_goods = unknown
base_manpower = 2
capital = "Adellia"
owner = CE7
controller = CE7
is_city = yes
add_core = CE7
57.1.1 = { owner = CH1 controller = CH1 add_core = CH1 } 
152.1.1 = { owner = AYL controller = AYL add_core = AYL } 