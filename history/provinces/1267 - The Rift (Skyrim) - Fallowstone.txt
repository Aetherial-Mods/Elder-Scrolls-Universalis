religion = snow_elves_pantheon
culture = snow_elven
hre = no
base_tax = 1
base_production = 1
trade_goods = gold
base_manpower = 1
capital = "Followstone"
owner = NA1
controller = NA1
add_core = NA1
is_city = yes
57.1.1 = { owner = NG7 controller = NG7 add_core = NG7 } 
152.1.1 = { 
    owner = NA1
    controller = NA1
    add_core = NA1
    religion = nordic_pantheon
    hre = yes
}
