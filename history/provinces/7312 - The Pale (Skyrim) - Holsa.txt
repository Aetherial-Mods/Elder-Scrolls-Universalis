religion = reason_and_logic_cult
culture = dwemer
base_tax = 3
base_production = 3
trade_goods = unknown
base_manpower = 3
capital = "Holsa"
is_city = yes
owner = ND5
controller = ND5
add_core = ND5
add_permanent_province_modifier = { name = "water_trade_modifier" duration = -1 }