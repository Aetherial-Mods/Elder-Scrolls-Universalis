religion = meridia_cult
culture = ayleid
hre = no
base_tax = 1
base_production = 1
trade_goods = unknown
base_manpower = 1
capital = "Memorial"
owner = CI8
controller = CI8
is_city = yes
add_core = CI8
57.1.1 = { owner = CH1 controller = CH1 add_core = CH1 } 
152.1.1 = { owner = AYL controller = AYL add_core = AYL }