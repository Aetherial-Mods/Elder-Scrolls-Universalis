religion = vaermina_cult
culture = ayleid
base_tax = 7
base_production = 7
trade_goods = unknown
base_manpower = 7
capital = "Mist Morrow Vale"
is_city = yes
owner = CF4
controller = CF4
add_core = CF4
hre = no
add_permanent_province_modifier = { name = "land_trade_modifier" duration = -1 }