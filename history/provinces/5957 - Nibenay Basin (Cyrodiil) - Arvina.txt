religion = meridia_cult
culture = ayleid
base_tax = 2
base_production = 2
trade_goods = unknown
base_manpower = 2
capital = "Arvina Estate"
is_city = yes
owner = CF6
controller = CF6
add_core = CF6
hre = no
add_permanent_province_modifier = { name = "land_trade_modifier" duration = -1 }