religion = reason_and_logic_cult
culture = dwemer
base_tax = 2
base_production = 2
trade_goods = unknown
base_manpower = 2
capital = "Hob's Fall"
owner = ND8
controller = ND8
is_city = yes
add_core = ND8
add_permanent_province_modifier = { name = "water_trade_modifier" duration = -1 }