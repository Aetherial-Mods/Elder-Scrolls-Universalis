religion = ayleid_pantheon
culture = nedic
base_tax = 3
base_production = 3
trade_goods = unknown
base_manpower = 3
capital = "Bay Roan"
is_city = yes
owner = CB7
controller = CB7
add_core = CB7
add_permanent_province_modifier = { name = "water_trade_modifier" duration = -1 }
57.1.1 = { owner = CH2 controller = CH2 add_core = CH2 } 
178.1.1 = { religion = mehrunes_dagon_cult }