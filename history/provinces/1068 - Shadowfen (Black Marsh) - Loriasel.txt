religion = ayleid_pantheon
culture = barsaebic
hre = no
base_tax = 1
base_production = 1
trade_goods = unknown
base_manpower = 1
capital = "Loriasel"
owner = AE8
controller = AE8
add_core = AE8
is_city = yes
fort_15th = yes
add_permanent_province_modifier = { name = "water_trade_modifier" duration = -1 }
57.1.1 = {
	religion = hist
	culture = naga
	owner = ---
	controller = ---
	remove_core = AE8
}
178.1.1 = {
	religion = ayleid_pantheon
	culture = barsaebic
	owner = AE8
	controller = AE8
	add_core = AE8
}
