owner = WIT
controller = WIT
add_core = WIT
is_city = yes
religion = students_of_magnus
culture = nord
hre = yes
base_tax = 1
base_production = 1
trade_goods = magic_goods
base_manpower = 1
capital = "College of Winterhold"
57.1.1 = { owner = NA8 controller = NA8 add_core = NA8 } 
91.1.1 = { owner = WIT controller = WIT add_core = WIT } 