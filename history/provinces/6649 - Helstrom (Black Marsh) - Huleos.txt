religion = hist
culture = sarpa
hre = no
base_tax = 3
base_production = 3
trade_goods = unknown
base_manpower = 3
capital = "Huleos"
owner = AC3
controller = AC3
add_core = AC3
is_city = yes
add_permanent_province_modifier = { name = "land_trade_modifier" duration = -1 }
57.1.1 = { owner = --- controller = --- remove_core = AC3 } 