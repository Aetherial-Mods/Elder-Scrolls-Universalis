religion = altmeri_pantheon
culture = altmer
hre = no
base_tax = 2
base_production = 2
trade_goods = lapis_lazuli
base_manpower = 2
capital = "Dawnbreak"
owner = SC1
controller = SC1
add_core = SC1
is_city = yes
add_permanent_province_modifier = { name = "land_trade_modifier" duration = -1 }