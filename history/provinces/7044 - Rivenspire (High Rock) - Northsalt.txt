religion = druidism
culture = breton
hre = no
base_tax = 3
base_production = 3
trade_goods = unknown
base_manpower = 3
capital = "Northsalt"
owner = BC7
controller = BC7
add_core = BC7
is_city = yes
add_permanent_province_modifier = { name = "water_trade_modifier" duration = -1 }
57.1.1 = { owner = BB2 controller = BB2 add_core = BB2 } 