religion = dragon_cult
culture = atmoran
base_tax = 2
base_production = 2
trade_goods = unknown
base_manpower = 2
capital = "Valkygg"
is_city = yes
owner = NB3
controller = NB3
add_core = NB3
110.1.1 = { 
    owner = NE0
    controller = NE0
    add_core = NE0
    culture = nord
    religion = nordic_pantheon
}

126.1.1 = { owner = NB3 controller = NB3 add_core = NB3 
culture = nord religion = students_of_magnus }