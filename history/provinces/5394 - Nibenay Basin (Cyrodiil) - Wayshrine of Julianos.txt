religion = nedic_pantheon
culture = nedic
hre = no
base_tax = 1
base_production = 1
trade_goods = unknown
base_manpower = 1
capital = "Wayshrine of Julianos"
is_city = yes
owner = CI8
controller = CI8
add_core = CI8
57.1.1 = { owner = CH1 controller = CH1 add_core = CH1 } 
152.1.1 = { owner = AYL controller = AYL add_core = AYL } 
178.1.1 = { religion = meridia_cult }