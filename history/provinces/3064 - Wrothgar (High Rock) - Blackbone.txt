religion = cult_of_ancestors
culture = sea_giant
hre = no
base_tax = 1
base_production = 1
trade_goods = ivory
base_manpower = 3
capital = "Blackbone"
owner = BC9
controller = BC9
add_core = BC9
is_city = yes
91.1.1 = { owner = WTB controller = WTB add_core = WTB } 