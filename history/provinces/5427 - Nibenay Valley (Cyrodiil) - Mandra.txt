religion = magne_ge
culture = nedic
base_tax = 3
base_production = 3
trade_goods = unknown
base_manpower = 3
capital = "Mandra"
is_city = yes
owner = CG5
controller = CG5
add_core = CG5
57.1.1 = { owner = MOG controller = MOG add_core = MOG } 
152.1.1 = { 
    owner = ---
    controller = ---
    remove_core = MOG
    religion = cult_of_the_insect
}
