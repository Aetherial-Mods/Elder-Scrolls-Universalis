religion = hist
culture = agaceph
hre = no
base_tax = 4
base_production = 4
trade_goods = unknown
base_manpower = 4
capital = "Bogwater"
is_city = yes
owner = CG9
controller = CG9
add_core = CG9
center_of_trade = 1
57.1.1 = { owner = ALG controller = ALG add_core = ALG } 
91.1.1 = { religion = nedic_pantheon }
110.1.1 = { culture = nedic }