owner = HD6
controller = HD6
is_city = yes
add_core = HD6
culture = dwemer
religion = reason_and_logic_cult
hre = no
base_tax = 2
base_production = 2
trade_goods = unknown
base_manpower = 2
capital = "Volenfell"
fort_15th = yes
center_of_trade = 1
57.1.1 = {
	religion = malacath_cult
	culture = giant_goblin
	owner = ---
	controller = ---
	remove_core = HD6
}

