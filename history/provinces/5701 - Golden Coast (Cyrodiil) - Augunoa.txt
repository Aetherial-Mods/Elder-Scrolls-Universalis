religion = molag_bal_cult
culture = ayleid
hre = no
base_tax = 2
base_production = 2
trade_goods = unknown
base_manpower = 2
capital = "Augunoa"
owner = CC6
controller = CC6
add_core = CC6
is_city = yes
add_permanent_province_modifier = { name = "water_trade_modifier" duration = -1 }