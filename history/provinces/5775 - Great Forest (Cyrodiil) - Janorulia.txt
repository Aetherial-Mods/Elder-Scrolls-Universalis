religion = meridia_cult
culture = ayleid
base_tax = 1
base_production = 1
trade_goods = unknown
base_manpower = 1
capital = "Janorulia"
is_city = yes
owner = CB5
controller = CB5
add_core = CB5
hre = no
57.1.1 = { owner = CH6 controller = CH6 add_core = CH6 } 
91.1.1 = { owner = CH1 controller = CH1 add_core = CH1 } 
152.1.1 = { owner = AYL controller = AYL add_core = AYL } 