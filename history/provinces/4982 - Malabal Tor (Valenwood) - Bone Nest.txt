religion = bosmer_pantheon
culture = bosmer
base_tax = 1
base_production = 1
trade_goods = unknown
base_manpower = 1
capital = "Bone Nest"
owner = VA5
controller = VA5
add_core = VA5
is_city = yes