owner = MB7
controller = MB7
add_core = MB7
religion = chimer_pantheon
culture = ashlander_chimer
hre = no
base_tax = 2
base_production = 2
trade_goods = unknown
base_manpower = 2
capital = "Arinathram"
is_city = yes
add_permanent_province_modifier = { name = "water_trade_modifier" duration = -1 } 
57.1.1 = { owner = --- controller = --- }
190.1.1 = { owner = MB7 controller = MB7 add_core = MB7 }