religion = altmeri_pantheon
culture = altmer
hre = no
base_tax = 2
base_production = 2
trade_goods = unknown
base_manpower = 2
capital = "Josausa"
owner = BB3
controller = BB3
add_core = BB3
is_city = yes
add_permanent_province_modifier = { name = "water_trade_modifier" duration = -1 }
57.1.1 = { owner = BA3 controller = BA3 add_core = BA3 } 