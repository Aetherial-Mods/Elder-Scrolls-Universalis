religion = nordic_pantheon
culture = nord
hre = yes
base_tax = 2
base_production = 2
trade_goods = unknown
base_manpower = 2
capital = "Frozen Coast"
owner = NE7
controller = NE7
is_city = yes
add_core = NE7
center_of_trade = 1
add_permanent_province_modifier = { name = "water_trade_modifier" duration = -1 }
57.1.1 = { owner = NB5 controller = NB5 add_core = NB5 } 