religion = nedic_pantheon
culture = nedic
hre = no
base_tax = 5
base_production = 3
trade_goods = cloth
base_manpower = 4
capital = "Harcane Grove"
owner = CD8
controller = CD8
is_city = yes
add_core = CD8
center_of_trade = 1
57.1.1 = { owner = CG7 controller = CG7 add_core = CG7 } 
178.1.1 = { religion = meridia_cult }