culture = reachmen
religion = old_gods_cult
hre = no
base_tax = 3
base_production = 3
trade_goods = unknown
base_manpower = 3
capital = "Jehanna Port"
owner = BC9
controller = BC9
add_core = BC9
is_city = yes
center_of_trade = 1
add_permanent_province_modifier = { name = "land_trade_modifier" duration = -1 }
91.1.1 = { owner = WTB controller = WTB add_core = WTB } 