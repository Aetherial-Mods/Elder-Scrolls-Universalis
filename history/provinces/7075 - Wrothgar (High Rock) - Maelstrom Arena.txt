religion = old_gods_cult
culture = reachmen
hre = no
base_tax = 2
base_production = 2
trade_goods = unknown
base_manpower = 2
capital = "Maelstrom Arena"
owner = BC9
controller = BC9
add_core = BC9
is_city = yes
add_permanent_province_modifier = { name = "land_trade_modifier" duration = -1 }
91.1.1 = { owner = WTB controller = WTB add_core = WTB } 