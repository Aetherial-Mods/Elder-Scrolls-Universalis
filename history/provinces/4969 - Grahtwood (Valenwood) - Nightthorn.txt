religion = bosmer_pantheon
culture = bosmer
hre = no
base_tax = 2
base_production = 2
trade_goods = unknown
base_manpower = 2
capital = "Nightthorn"
owner = VB7
controller = VB7
is_city = yes
add_core = VB7
add_permanent_province_modifier = { name = "land_trade_modifier" duration = -1 }