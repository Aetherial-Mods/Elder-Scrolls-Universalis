owner = EB4
controller = EB4
add_core = EB4
is_city = yes
religion = khajiiti_pantheon
culture = khajiiti
hre = no
base_tax = 3
base_production = 3
trade_goods = unknown
base_manpower = 3
capital = "Dune"
center_of_trade = 2
add_province_triggered_modifier = thieves_guild_province
add_province_triggered_modifier = wharf_rats_province