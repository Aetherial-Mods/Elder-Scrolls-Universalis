owner = VC5
controller = VC5
is_city = yes
add_core = VC5
religion = orcish_pantheon
culture = wood_orsimer
base_tax = 2
base_production = 2
trade_goods = unknown
base_manpower = 2
capital = "Bloodtoil Valley"
fort_15th = yes
57.1.1 = {
	owner = VC6
	controller = VC6
	add_core = VC6
	remove_core = VC5
}

