religion = molag_bal_cult
culture = ayleid
base_tax = 2
base_production = 2
trade_goods = unknown
base_manpower = 2
capital = "Istirus"
is_city = yes
owner = ED3
controller = ED3
add_core = ED3
hre = no
add_permanent_province_modifier = { name = "water_trade_modifier" duration = -1 }
57.1.1 = { owner = CC7 controller = CC7 add_core = CC7 } 