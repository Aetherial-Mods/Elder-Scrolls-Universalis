owner = MF4
controller = MF4
add_core = MF4
religion = chimer_pantheon
culture = ashlander_chimer
hre = no
base_tax = 3
base_production = 3
trade_goods = unknown
base_manpower = 3
capital = "Gnaar Mok"
is_city = yes
center_of_trade = 2
fort_15th = yes
57.1.1 = { owner = MJ9 controller = MJ9 add_core = MJ9 }