owner = HC0
controller = HC0
is_city = yes
add_core = HC0
religion = redguard_pantheon
culture = redguard
base_tax = 6
base_production = 6
trade_goods = unknown
base_manpower = 6
capital = "Helland"
fort_15th = yes
center_of_trade = 1
57.1.1 = {
	religion = malacath_cult
	culture = iron_orc
	owner = ---
	controller = ---
	remove_core = HC0
}

