religion = meridia_cult
culture = ayleid
hre = no
base_tax = 2
base_production = 3
trade_goods = ale
base_manpower = 1
capital = "Roxey Inn"
owner = CE5
controller = CE5
is_city = yes
add_core = CE5
add_permanent_province_modifier = { name = "water_trade_modifier" duration = -1 } 
178.1.1 = { religion = mephala_cult }