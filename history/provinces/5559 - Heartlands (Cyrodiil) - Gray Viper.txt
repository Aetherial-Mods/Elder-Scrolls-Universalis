religion = ayleid_pantheon
culture = ayleid
base_tax = 1
base_production = 2
trade_goods = nirnroot
base_manpower = 1
capital = "Vipercrossing"
is_city = yes
owner = CA4
controller = CA4
add_core = CA4
hre = no
178.1.1 = { owner = AYL controller = AYL add_core = AYL }