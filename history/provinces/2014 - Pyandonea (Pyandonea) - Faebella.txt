owner = PC1
controller = PC1
add_core = PC1
religion = serpant_king
culture = maormer
hre = no
base_tax = 2
base_production = 2
trade_goods = unknown
base_manpower = 2
capital = "Faebella"
is_city = yes
add_permanent_province_modifier = { name = "water_trade_modifier" duration = -1 }