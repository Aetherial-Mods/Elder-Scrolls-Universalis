religion = khajiiti_pantheon
culture = khajiiti
base_tax = 1
base_production = 1
trade_goods = unknown
base_manpower = 1
capital = "Harridan's Lair"
is_city = yes
owner = VB4
controller = VB4
add_core = VB4
add_permanent_province_modifier = { name = "land_trade_modifier" duration = -1 }
57.1.1 = { owner = --- controller = --- remove_core = VB4 } 