religion = meridia_cult
culture = nedic
hre = no
base_tax = 2
base_production = 2
trade_goods = colovian_battlecry
base_manpower = 2
capital = "Tullich"
owner = CF0
controller = CF0
add_core = CF0
is_city = yes
57.1.1 = { owner = PER controller = PER add_core = PER } 
91.1.1 = { religion = keptu_pantheon } 
110.1.1 = { culture = keptu } 