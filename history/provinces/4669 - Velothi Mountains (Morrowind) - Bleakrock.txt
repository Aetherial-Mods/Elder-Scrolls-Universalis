religion = nordic_pantheon
culture = nord
hre = no
base_tax = 2
base_production = 3
trade_goods = crabs
base_manpower = 3
capital = "Bleakrock"
owner = MH3
controller = MH3
is_city = yes
add_core = MH3

57.1.1 = {
   owner = ---
   controller = ---
   #add_core = ---
}

190.1.1 = {
   owner = MH3
   controller = MH3
   add_core = MH3
}