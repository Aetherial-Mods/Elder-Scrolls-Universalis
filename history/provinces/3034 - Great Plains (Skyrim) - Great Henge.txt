owner = NB8
controller = NB8
add_core = NB8
is_city = yes
religion = nordic_pantheon
culture = nord
hre = yes
base_tax = 1
base_production = 1
trade_goods = dragon_scales
base_manpower = 1
capital = "Great Henge"
57.1.1 = { owner = NF1 controller = NF1 add_core = NF1 } 