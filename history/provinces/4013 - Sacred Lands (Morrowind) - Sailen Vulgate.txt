owner = MF6
controller = MF6
add_core = MF6
religion = chimer_pantheon
culture = ashlander_chimer
hre = no
base_tax = 3
base_production = 3
trade_goods = unknown
base_manpower = 3
capital = "Sailen Vulgate"
is_city = yes
center_of_trade = 1
fort_15th = yes
57.1.1 = { owner = MF5 controller = MF5 add_core = MF5 }