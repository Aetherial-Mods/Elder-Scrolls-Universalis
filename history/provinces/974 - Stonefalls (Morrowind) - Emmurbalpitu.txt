owner = MD6
controller = MD6
add_core = MD6
religion = malacath_cult
culture = orsimer
hre = no
base_tax = 1
base_production = 1
trade_goods = unknown
base_manpower = 4
capital = "Emmurbalpitu"
is_city = yes
add_permanent_province_modifier = { name = "land_trade_modifier" duration = -1 } 
57.1.1 = { owner = --- controller = --- }
190.1.1 = { owner = MD6 controller = MD6 add_core = MD6 }