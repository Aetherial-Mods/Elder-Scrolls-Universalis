owner = MC4
controller = MC4
add_core = MC4
religion = reason_and_logic_cult
culture = dwemer
hre = no
base_tax = 2
base_production = 2
trade_goods = unknown
base_manpower = 2
capital = "Rideveri"
is_city = yes