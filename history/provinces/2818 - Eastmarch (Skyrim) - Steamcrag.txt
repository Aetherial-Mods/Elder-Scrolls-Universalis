owner = NE3
controller = NE3
add_core = NE3
religion = cult_of_ancestors
culture = giant
hre = no
base_tax = 2
base_production = 2
trade_goods = unknown
base_manpower = 2
capital = "Steamcrag Camp"
is_city = yes