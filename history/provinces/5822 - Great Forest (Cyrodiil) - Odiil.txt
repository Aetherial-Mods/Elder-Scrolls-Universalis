owner = CD5
controller = CD5
add_core = CD5
is_city = yes
religion = meridia_cult
culture = nedic
hre = no
base_tax = 2
base_production = 2
trade_goods = unknown
base_manpower = 2
capital = "Odiil Estate"
fort_15th = yes
center_of_trade = 1
57.1.1 = { owner = CF0 controller = CF0 add_core = CF0 } 
91.1.1 = { religion = nedic_pantheon } 