owner = CC2
controller = CC2
is_city = yes
add_core = CC2
religion = molag_bal_cult
culture = nedic
hre = no
base_tax = 2
base_production = 2
trade_goods = unknown
base_manpower = 2
capital = "Brina Cross"
fort_15th = yes
center_of_trade = 1
57.1.1 = {
	owner = CF5
	controller = CF5
	add_core = CF5
}

