religion = altmeri_pantheon
culture = altmer
hre = no
base_tax = 3
base_production = 3
trade_goods = unknown
base_manpower = 3
capital = "Angaelle"
owner = SB0
controller = SB0
add_core = SB0
is_city = yes
add_permanent_province_modifier = { name = "land_trade_modifier" duration = -1 }