religion = meridia_cult
culture = ayleid
base_tax = 1
base_production = 1
trade_goods = unknown
base_manpower = 1
capital = "Coldcorn"
is_city = yes
owner = CE2
controller = CE2
add_core = CE2
hre = no
91.1.1 = { owner = CH1 controller = CH1 add_core = CH1 } 
152.1.1 = { owner = AYL controller = AYL add_core = AYL } 