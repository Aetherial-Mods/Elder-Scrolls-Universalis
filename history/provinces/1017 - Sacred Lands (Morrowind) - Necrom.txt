owner = MB4
controller = MB4
add_core = MB4
religion = chimer_pantheon
culture = ashlander_chimer
hre = no
base_tax = 4
base_production = 4
trade_goods = paper
base_manpower = 4
capital = "Necrom"
is_city = yes
center_of_trade = 1

add_province_triggered_modifier = bal_molagmer_province
add_province_triggered_modifier = wharf_rats_province