owner = HC1
controller = HC1
is_city = yes
add_core = HC1
religion = redguard_pantheon
culture = redguard
base_tax = 3
base_production = 3
trade_goods = unknown
base_manpower = 3
capital = "Corten Mont"
fort_15th = yes
center_of_trade = 1
57.1.1 = {
	religion = malacath_cult
	culture = iron_orc
	owner = ---
	controller = ---
	remove_core = HC1
}

