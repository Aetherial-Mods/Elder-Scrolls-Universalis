owner = MA6
controller = MA6
add_core = MA6
religion = chimer_pantheon
culture = ashlander_chimer
hre = no
base_tax = 1
base_production = 1
trade_goods = unknown
base_manpower = 1
capital = "Greymist Falls"
is_city = yes
57.1.1 = { owner = MA3 controller = MA3 add_core = MA3 }