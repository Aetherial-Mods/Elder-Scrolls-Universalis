religion = hist
culture = agaceph
hre = no
base_tax = 1
base_production = 1
trade_goods = unknown
base_manpower = 1
capital = "Methees"
owner = AD1
controller = AD1
add_core = AD1
is_city = yes
add_permanent_province_modifier = { name = "land_trade_modifier" duration = -1 }
57.1.1 = { owner = --- controller = --- remove_core = AD1 } 