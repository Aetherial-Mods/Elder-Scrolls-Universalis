owner = CA8
controller = CA8
is_city = yes
add_core = CA8
religion = meridia_cult
culture = ayleid
hre = no
base_tax = 4
base_production = 5
trade_goods = cyrodilic_brandy
base_manpower = 5
capital = "Vindasel"
center_of_trade = 1
add_permanent_province_modifier = { name = "land_trade_modifier" duration = -1 }

110.1.1 = {
	add_permanent_province_modifier = { name = "02_wailing_wheel" duration = -1 }
}

190.1.1 = {
	remove_province_modifier = 02_wailing_wheel
}