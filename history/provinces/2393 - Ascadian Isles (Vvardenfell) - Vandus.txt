owner = MJ3
controller = MJ3
religion = chimer_pantheon
culture = ashlander_chimer
base_tax = 3
base_production = 3
trade_goods = unknown
base_manpower = 3
capital = "Vandus"
is_city = yes
add_core = MJ3
add_permanent_province_modifier = { name = "water_trade_modifier" duration = -1 } 
57.1.1 = { owner = --- controller = --- } 
190.1.1 = { owner = SOT controller = SOT add_core = SOT }