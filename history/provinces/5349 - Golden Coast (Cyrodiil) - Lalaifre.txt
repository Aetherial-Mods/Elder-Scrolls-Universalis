religion = meridia_cult
culture = ayleid
hre = no
base_tax = 3
base_production = 3
trade_goods = unknown
base_manpower = 3
capital = "Lalaifre"
owner = CA9
controller = CA9
add_core = CA9
is_city = yes
57.1.1 = { owner = CH4 controller = CH4 add_core = CH4 } 
178.1.1 = { religion = molag_bal_cult }