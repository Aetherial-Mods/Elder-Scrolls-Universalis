religion = ayleid_pantheon
culture = nedic
base_tax = 2
base_production = 2
trade_goods = unknown
base_manpower = 2
capital = "Facia"
is_city = yes
owner = CG1
controller = CG1
add_core = CG1
add_permanent_province_modifier = { name = "water_trade_modifier" duration = -1 }
178.1.1 = { religion = meridia_cult }