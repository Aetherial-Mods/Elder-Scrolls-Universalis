religion = altmeri_pantheon
culture = altmer
hre = no
base_tax = 2
base_production = 2
trade_goods = potions
base_manpower = 2
capital = "Western Overlook"
owner = SB2
controller = SB2
add_core = SB2
is_city = yes
add_province_triggered_modifier = summerset_shadows_province
add_province_triggered_modifier = wharf_rats_province