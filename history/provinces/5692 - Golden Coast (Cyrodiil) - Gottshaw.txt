owner = CC3
controller = CC3
is_city = yes
add_core = CC3
religion = molag_bal_cult
culture = nedic
hre = no
base_tax = 2
base_production = 2
trade_goods = unknown
base_manpower = 2
capital = "Gottshaw"
fort_15th = yes
57.1.1 = {
	owner = CH3
	controller = CH3
	add_core = CH3
}

