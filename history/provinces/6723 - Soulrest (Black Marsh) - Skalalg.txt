religion = hist
culture = archein
hre = no
base_tax = 2
base_production = 2
trade_goods = unknown
base_manpower = 2
capital = "Skalalg"
owner = AB6
controller = AB6
add_core = AB6
is_city = yes
add_permanent_province_modifier = { name = "land_trade_modifier" duration = -1 }
57.1.1 = { owner = --- controller = --- remove_core = AB6 } 