religion = khajiiti_pantheon
culture = khajiiti
hre = no
base_tax = 3
base_production = 3
trade_goods = unknown
base_manpower = 3
capital = "Tavani"
owner = EA8
controller = EA8
add_core = EA8
is_city = yes
add_permanent_province_modifier = { name = "land_trade_modifier" duration = -1 }