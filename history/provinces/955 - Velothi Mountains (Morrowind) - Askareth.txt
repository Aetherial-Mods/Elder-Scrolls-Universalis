owner = MA2
controller = MA2
add_core = MA2
religion = chimer_pantheon
culture = ashlander_chimer
hre = no
base_tax = 2
base_production = 2
trade_goods = unknown
base_manpower = 2
capital = "Askareth"
is_city = yes
center_of_trade = 1
add_permanent_province_modifier = { name = "water_trade_modifier" duration = -1 }

57.1.1 = {
   owner = ---
   controller = ---
   #add_core = ---
}