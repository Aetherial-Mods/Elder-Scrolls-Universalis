owner = MI2
controller = MI2
add_core = MI2
religion = chimer_pantheon
culture = ashlander_chimer
hre = no
base_tax = 2
base_production = 2
trade_goods = unknown
base_manpower = 2
capital = "Sandas"
is_city = yes
fort_15th = yes
add_permanent_province_modifier = { name = "water_trade_modifier" duration = -1 }
add_province_triggered_modifier = baandari_pedlars_province
190.1.1 = { owner = SOT controller = SOT add_core = SOT }