religion = khajiiti_pantheon
culture = khajiiti
hre = no
base_tax = 1
base_production = 1
trade_goods = unknown
base_manpower = 1
capital = "Ashnada"
owner = EB2
controller = EB2
add_core = EB2
is_city = yes
add_permanent_province_modifier = { name = "land_trade_modifier" duration = -1 }
57.1.1 = { owner = --- controller = --- remove_core = EB2 } 