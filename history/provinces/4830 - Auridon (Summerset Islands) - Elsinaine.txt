religion = altmeri_pantheon
culture = altmer
hre = no
base_tax = 3
base_production = 3
trade_goods = unknown
base_manpower = 3
capital = "Elsinaine"
owner = SC1
controller = SC1
add_core = SC1
is_city = yes
add_permanent_province_modifier = { name = "land_trade_modifier" duration = -1 }