religion = altmeri_pantheon
culture = altmer
hre = no
base_tax = 1
base_production = 1
trade_goods = unknown
base_manpower = 1
capital = "Indrik Frolic"
owner = SA7
controller = SA7
add_core = SA7
is_city = yes
add_permanent_province_modifier = { name = "land_trade_modifier" duration = -1 }