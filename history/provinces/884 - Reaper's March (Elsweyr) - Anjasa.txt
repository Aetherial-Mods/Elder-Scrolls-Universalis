religion = khajiiti_pantheon
culture = khajiiti
hre = no
base_tax = 2
base_production = 2
trade_goods = unknown
base_manpower = 2
capital = "Anjasa"
owner = EB3
controller = EB3
add_core = EB3
is_city = yes
add_permanent_province_modifier = { name = "land_trade_modifier" duration = -1 }
57.1.1 = { owner = --- controller = --- remove_core = EB3 } 