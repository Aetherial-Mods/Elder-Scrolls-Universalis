religion = hist
culture = agaceph
base_tax = 2
base_production = 2
trade_goods = unknown
base_manpower = 2
capital = "Philes"
is_city = yes
owner = CG9
controller = CG9
add_core = CG9
57.1.1 = { owner = ALG controller = ALG add_core = ALG } 
91.1.1 = { religion = nedic_pantheon }
110.1.1 = { culture = nedic }