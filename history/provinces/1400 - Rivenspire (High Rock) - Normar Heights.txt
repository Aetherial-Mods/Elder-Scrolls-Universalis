religion = druidism
culture = breton
hre = no
base_tax = 5
base_production = 5
trade_goods = unknown
base_manpower = 5
capital = "Normar Heights"
owner = BE1
controller = BE1
add_core = BE1
is_city = yes
fort_15th = yes
57.1.1 = {
	owner = BB2
	controller = BB2
	add_core = BB2
}

