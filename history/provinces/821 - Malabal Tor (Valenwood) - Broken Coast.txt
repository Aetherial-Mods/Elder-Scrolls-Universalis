religion = bosmer_pantheon
culture = bosmer
hre = no
base_tax = 3
base_production = 3
trade_goods = unknown
base_manpower = 3
capital = "Broken Coast"
owner = VC6
controller = VC6
add_core = VC6
is_city = yes
center_of_trade = 1
add_permanent_province_modifier = { name = "land_trade_modifier" duration = -1 }