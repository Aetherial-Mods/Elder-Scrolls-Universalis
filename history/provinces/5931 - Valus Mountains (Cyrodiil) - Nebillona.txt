religion = hircine_cult
culture = nedic
base_tax = 2
base_production = 2
trade_goods = unknown
base_manpower = 2
capital = "Nebillona"
is_city = yes
owner = CF7
controller = CF7
add_core = CF7
57.1.1 = { owner = ALH controller = ALH add_core = ALH } 
126.1.1 = { owner = CH1 controller = CH1 add_core = CH1 } 
152.1.1 = { owner = AYL controller = AYL add_core = AYL } 
178.1.1 = { religion = meridia_cult }