religion = chimer_pantheon
culture = ashlander_chimer
hre = no
base_tax = 1
base_production = 1
trade_goods = unknown
base_manpower = 1
capital = "Tel Fyr"
is_city = yes
owner = MG9
controller = MG9
add_core = MG9
fort_15th = yes
center_of_trade = 1
add_province_triggered_modifier = magas_volar_province
190.1.1 = { owner = MOA controller = MOA add_core = MOA }