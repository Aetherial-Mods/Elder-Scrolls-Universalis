religion = zeqqi_cult
culture = keptu
hre = no
base_tax = 2
base_production = 2
trade_goods = unknown
base_manpower = 2
capital = "Ichidag"
is_city = yes
owner = HG8
controller = HG8
add_core = HG8
add_permanent_province_modifier = { name = "water_trade_modifier" duration = -1 } 