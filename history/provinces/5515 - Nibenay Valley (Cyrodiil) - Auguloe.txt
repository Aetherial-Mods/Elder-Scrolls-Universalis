religion = mehrunes_dagon_cult
culture = ayleid
hre = no
base_tax = 2
base_production = 2
trade_goods = unknown
base_manpower = 2
capital = "Auguloe"
is_city = yes
owner = CB9
controller = CB9
add_core = CB9
add_permanent_province_modifier = { name = "water_trade_modifier" duration = -1 }