religion = nedic_pantheon
culture = nedic
base_tax = 1
base_production = 1
trade_goods = unknown
base_manpower = 1
capital = "Forsaken Mine"
is_city = yes
owner = CA1
controller = CA1
add_core = CA1
add_permanent_province_modifier = { name = "water_trade_modifier" duration = -1 }