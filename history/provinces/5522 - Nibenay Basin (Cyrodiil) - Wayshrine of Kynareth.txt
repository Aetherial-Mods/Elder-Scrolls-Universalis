religion = nedic_pantheon
culture = nedic
hre = no
base_tax = 2
base_production = 2
trade_goods = unknown
base_manpower = 2
capital = "Wayshrine of Kynareth"
is_city = yes
owner = CF9
controller = CF9
add_core = CF9
57.1.1 = { owner = MOK controller = MOK add_core = MOK } 
91.1.1 = { religion = boethiah_cult }