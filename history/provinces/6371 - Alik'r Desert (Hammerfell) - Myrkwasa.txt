religion = malacath_cult
culture = giant_goblin
base_tax = 1
base_production = 1
trade_goods = unknown
base_manpower = 1
capital = "Myrkwasa"
is_city = yes
owner = HD5
controller = HD5
add_core = HD5
add_province_triggered_modifier = coven_of_the_tide_province
57.1.1 = { owner = --- controller = --- remove_core = HD5 } 