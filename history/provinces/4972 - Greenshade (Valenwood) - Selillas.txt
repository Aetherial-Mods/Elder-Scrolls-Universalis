religion = bosmer_pantheon
culture = imga
hre = no
base_tax = 2
base_production = 2
trade_goods = unknown
base_manpower = 2
capital = "Selillas"
owner = VB3
controller = VB3
add_core = VB3
is_city = yes
57.1.1 = { 
    owner = IMG
    controller = IMG
    remove_core = VB3
    add_core = IMG
}