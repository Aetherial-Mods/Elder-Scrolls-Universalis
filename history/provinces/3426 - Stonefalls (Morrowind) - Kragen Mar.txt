owner = MD4
controller = MD4
add_core = MD4
religion = chimer_pantheon
culture = ashlander_chimer
hre = no
base_tax = 3
base_production = 3
trade_goods = unknown
base_manpower = 3
capital = "Kragen Mar"
is_city = yes
center_of_trade = 1

57.1.1 = { owner = MA5 controller = MA5 add_core = MA5 }