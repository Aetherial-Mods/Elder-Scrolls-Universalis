religion = molag_bal_cult
culture = nedic
hre = no
base_tax = 3
base_production = 3
trade_goods = unknown
base_manpower = 3
capital = "Serpent Hollow"
owner = CE8
controller = CE8
add_core = CE8
is_city = yes
57.1.1 = { owner = CD1 controller = CD1 add_core = CD1 } 
178.1.1 = { religion = meridia_cult }