religion = four_parents
culture = keptu
base_tax = 5
base_production = 5
trade_goods = unknown
base_manpower = 5
capital = "Wether's Cleft"
is_city = yes
owner = HC7
controller = HC7
add_core = HC7
add_permanent_province_modifier = { name = "land_trade_modifier" duration = -1 }