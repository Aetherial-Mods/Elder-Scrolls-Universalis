owner = HB2
controller = HB2
is_city = yes
add_core = HB2
base_tax = 3
base_production = 3
trade_goods = iron
base_manpower = 3
capital = "Rkundzelft"
religion = reason_and_logic_cult
culture = dwemer
57.1.1 = { 
    culture = keptu
    religion = celestials
    owner = HF6
    controller = HF6
    add_core = HF6
}
