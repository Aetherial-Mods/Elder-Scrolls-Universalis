owner = BB4
controller = BB4
add_core = BB4
is_city = yes
religion = druidism
culture = breton
hre = no
base_tax = 1
base_production = 1
trade_goods = unknown
base_manpower = 1
capital = "Phrygias"
fort_15th = yes
center_of_trade = 1
add_province_triggered_modifier = skeffington_coven_province
57.1.1 = {
	owner = BC0
	controller = BC0
	add_core = BC0
}

