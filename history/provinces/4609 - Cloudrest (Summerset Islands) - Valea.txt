religion = altmeri_pantheon
culture = altmer
hre = no
base_tax = 1
base_production = 1
trade_goods = unknown
base_manpower = 1
capital = "Valea"
owner = SA2
controller = SA2
add_core = SA2
is_city = yes
add_permanent_province_modifier = { name = "water_trade_modifier" duration = -1 }