owner = MD9
controller = MD9
add_core = MD9
religion = chimer_pantheon
culture = ashlander_chimer
hre = no
base_tax = 2
base_production = 2
trade_goods = unknown
base_manpower = 2
capital = "Favadal"
is_city = yes
57.1.1 = { owner = --- controller = --- }
190.1.1 = { owner = MD9 controller = MD9 add_core = MD9 }