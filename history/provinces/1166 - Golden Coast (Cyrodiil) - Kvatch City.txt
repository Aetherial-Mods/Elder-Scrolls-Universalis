religion = molag_bal_cult
culture = ayleid
hre = no
base_tax = 1
base_production = 1
trade_goods = ayleid_nose_hash
base_manpower = 1
capital = "Kvatch City"
owner = CA7
controller = CA7
add_core = CA7
is_city = yes
add_province_triggered_modifier = thieves_guild_province
add_province_triggered_modifier = wharf_rats_province
57.1.1 = { owner = CH3 controller = CH3 add_core = CH3 } 