religion = orcish_pantheon
culture = orsimer
hre = no
base_tax = 2
base_production = 2
trade_goods = unknown
base_manpower = 2
capital = "Exile's Barrow"
owner = BB0
controller = BB0
add_core = BB0
is_city = yes

add_province_triggered_modifier = bloodmist_clan_province