owner = ME9
controller = ME9
add_core = ME9
religion = ghost_snake
culture = ashlander_chimer
hre = no
base_tax = 1
base_production = 1
trade_goods = unknown
base_manpower = 1
capital = "Mabrigash"
is_city = yes
add_province_triggered_modifier = mabrigash_tribe_province
57.1.1 = { owner = --- controller = --- }
190.1.1 = { owner = ME9 controller = ME9 add_core = ME9 }
