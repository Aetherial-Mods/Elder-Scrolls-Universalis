religion = mehrunes_dagon_cult
culture = ayleid
hre = no
base_tax = 3
base_production = 3
trade_goods = unknown
base_manpower = 3
capital = "Irony"
is_city = yes
owner = CB9
controller = CB9
add_core = CB9
add_permanent_province_modifier = { name = "water_trade_modifier" duration = -1 }
add_permanent_province_modifier = { name = "land_trade_modifier" duration = -1 }