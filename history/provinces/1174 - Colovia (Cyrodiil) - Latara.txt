religion = molag_bal_cult
culture = ayleid
hre = no
base_tax = 1
base_production = 1
trade_goods = unknown
base_manpower = 1
capital = "Latara"
owner = CC6
controller = CC6
add_core = CC6
is_city = yes