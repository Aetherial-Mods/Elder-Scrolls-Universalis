religion = meridia_cult
culture = ayleid
hre = no
base_tax = 3
base_production = 2
trade_goods = fish
base_manpower = 3
capital = "Sena"
owner = CB8
controller = CB8
add_core = CB8
is_city = yes
add_permanent_province_modifier = { name = "land_trade_modifier" duration = -1 }
57.1.1 = { owner = CH1 controller = CH1 add_core = CH1 } 
152.1.1 = { owner = AYL controller = AYL add_core = AYL } 