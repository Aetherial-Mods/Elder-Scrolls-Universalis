owner = NG1
controller = NG1
religion = reason_and_logic_cult
culture = dwemer
base_tax = 2
base_production = 2
trade_goods = unknown
base_manpower = 2
capital = "Ruskahr"
is_city = yes
add_core = NG1
add_permanent_province_modifier = { name = "land_trade_modifier" duration = -1 }