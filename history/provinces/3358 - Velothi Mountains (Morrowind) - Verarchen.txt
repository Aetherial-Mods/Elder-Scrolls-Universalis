owner = MC7
controller = MC7
add_core = MC7
religion = reason_and_logic_cult
culture = dwemer
hre = no
base_tax = 2
base_production = 2
trade_goods = unknown
base_manpower = 2
capital = "Verarchen"
is_city = yes
add_permanent_province_modifier = { name = "water_trade_modifier" duration = -1 } 