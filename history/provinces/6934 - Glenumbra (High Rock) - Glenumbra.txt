owner = BA8
controller = BA8
add_core = BA8
is_city = yes
religion = druidism
culture = breton
hre = no
base_tax = 4
base_production = 4
trade_goods = unknown
base_manpower = 4
capital = "Glenumbra"
fort_15th = yes
center_of_trade = 1
57.1.1 = {
	owner = BF2
	controller = BF2
	add_core = BF2
}

