religion = sload_faith
culture = sload
hre = no
base_tax = 2
base_production = 2
trade_goods = unknown
base_manpower = 2
capital = "Terdith"
is_city = yes
owner = SE4
controller = SE4
add_core = SE4
add_permanent_province_modifier = { name = "water_trade_modifier" duration = -1 }
57.1.1 = { owner = SC6 controller = SC6 add_core = SC6 } 