religion = malacath_cult
culture = giant_goblin
hre = no
base_tax = 2
base_production = 2
trade_goods = unknown
base_manpower = 2
capital = "Dak'fron"
is_city = yes
owner = HD6
controller = HD6
add_core = HD6
add_province_triggered_modifier = kykos_coven_province
57.1.1 = { owner = --- controller = --- remove_core = HD6 } 