owner = CF6
controller = CF6
is_city = yes
add_core = CF6
religion = meridia_cult
culture = ayleid
hre = no
base_tax = 2
base_production = 2
trade_goods = unknown
base_manpower = 2
capital = "Nornal"
center_of_trade = 1