owner = NC2
controller = NC2
add_core = NC2
religion = nordic_pantheon
culture = nord
hre = yes
base_tax = 4
base_production = 4
trade_goods = silver
base_manpower = 4
capital = "Karthwasten"
is_city = yes
fort_15th = yes
center_of_trade = 1
57.1.1 = {
	owner = ND7
	controller = ND7
	add_core = ND7
}

