owner = BA1
controller = BA1
add_core = BA1
is_city = yes
religion = druidism
culture = breton
hre = no
base_tax = 1
base_production = 1
trade_goods = unknown
base_manpower = 1
capital = "Sigil-a-Dant"
fort_15th = yes
center_of_trade = 2
add_province_triggered_modifier = thieves_guild_province
add_province_triggered_modifier = wharf_rats_province
57.1.1 = {
	religion = altmeri_pantheon
	culture = altmer
	owner = BF2
	controller = BF2
	add_core = BF2
}

