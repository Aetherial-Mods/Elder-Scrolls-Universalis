religion = molag_bal_cult
culture = vampire
hre = yes
base_tax = 4
base_production = 3
trade_goods = crabs
base_manpower = 4
capital = "Kyne's Aegis"
owner = NB6
controller = NB6
add_core = NB6
is_city = yes