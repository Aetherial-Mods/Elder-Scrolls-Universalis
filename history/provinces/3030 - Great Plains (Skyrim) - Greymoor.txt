religion = cult_of_ancestors
culture = giant
hre = no
base_tax = 6
base_production = 6
trade_goods = unknown
base_manpower = 6
capital = "Fort Greymoor"
owner = NG3
controller = NG3
add_core = NG3
is_city = yes
fort_15th = yes
center_of_trade = 1
57.1.1 = {
	owner = ---
	controller = ---
	remove_core = NG3
}

