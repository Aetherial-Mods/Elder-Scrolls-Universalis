religion = reason_and_logic_cult
culture = dwemer
hre = no
base_tax = 2
base_production = 2
trade_goods = stalhrim
base_manpower = 2
capital = "Angarvunde"
owner = NF9
controller = NF9
add_core = NF9
is_city = yes