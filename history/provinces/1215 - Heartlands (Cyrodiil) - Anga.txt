owner = CE4
controller = CE4
is_city = yes
add_core = CE4
religion = mephala_cult
culture = ayleid
hre = no
base_tax = 3
base_production = 3
trade_goods = unknown
base_manpower = 3
capital = "Anga"