owner = MF5
controller = MF5
add_core = MF5
religion = reason_and_logic_cult
culture = dwemer
hre = no
base_tax = 2
base_production = 2
trade_goods = unknown
base_manpower = 2
capital = "Reyadyn"
is_city = yes
add_permanent_province_modifier = { name = "water_trade_modifier" duration = -1 } 