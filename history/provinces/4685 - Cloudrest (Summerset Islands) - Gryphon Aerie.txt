religion = altmeri_pantheon
culture = altmer
hre = no
base_tax = 6
base_production = 6
trade_goods = unknown
base_manpower = 6
capital = "Gryphon Aerie"
owner = SB5
controller = SB5
add_core = SB5
is_city = yes
add_permanent_province_modifier = { name = "water_trade_modifier" duration = -1 }