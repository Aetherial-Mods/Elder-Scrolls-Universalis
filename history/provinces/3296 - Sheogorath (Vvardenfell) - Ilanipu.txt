owner = MH4
controller = MH4
add_core = MH4
religion = chimer_pantheon
culture = ashlander_chimer
hre = no
base_tax = 1
base_production = 1
trade_goods = unknown
base_manpower = 1
capital = "Ilanipu"
is_city = yes
57.1.1 = { owner = MJ1 controller = MJ1 add_core = MJ1 }