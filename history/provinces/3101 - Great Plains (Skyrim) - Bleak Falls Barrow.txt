religion = nordic_pantheon
culture = nord
base_tax = 1
base_production = 1
trade_goods = stalhrim
base_manpower = 1
capital = "Bleak Falls Barrow"
is_city = yes
owner = NB2
controller = NB2
add_core = NB2
hre = yes
center_of_trade = 1