owner = CB6
controller = CB6
add_core = CB6
religion = nedic_pantheon
culture = nedic
hre = no
base_tax = 6
base_production = 6
trade_goods = unknown
base_manpower = 6
capital = "Bruma Castle"
is_city = yes
fort_15th = yes
center_of_trade = 2
add_province_triggered_modifier = thieves_guild_province
add_province_triggered_modifier = wharf_rats_province
57.1.1 = {
	religion = namira_cult
	culture = ayleid
	owner = CH7
	controller = CH7
	add_core = CH7
}

