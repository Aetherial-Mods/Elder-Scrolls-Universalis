religion = druidism
culture = breton
hre = no
base_tax = 3
base_production = 3
trade_goods = unknown
base_manpower = 3
capital = "Sanguine Barrows"
owner = BD9
controller = BD9
add_core = BD9
is_city = yes
add_permanent_province_modifier = { name = "water_trade_modifier" duration = -1 }
57.1.1 = { owner = BE2 controller = BE2 add_core = BE2 } 