religion = meridia_cult
culture = ayleid
hre = no
base_tax = 2
base_production = 4
trade_goods = crabs
base_manpower = 3
capital = "City Sewers"
owner = CB8
controller = CB8
add_core = CB8
is_city = yes
57.1.1 = { owner = CH1 controller = CH1 add_core = CH1 } 
152.1.1 = { owner = AYL controller = AYL add_core = AYL } 