owner = NB6
controller = NB6
add_core = NB6
religion = nordic_pantheon
culture = nord
hre = yes
base_tax = 5
base_production = 5
trade_goods = paper
base_manpower = 5
capital = "Solitude"
is_city = yes
center_of_trade = 2

add_province_triggered_modifier = thieves_guild_province
add_province_triggered_modifier = wharf_rats_province