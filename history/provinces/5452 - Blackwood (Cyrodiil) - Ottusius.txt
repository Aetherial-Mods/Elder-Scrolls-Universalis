religion = elder_gods
culture = orma
hre = no
base_tax = 2
base_production = 2
trade_goods = unknown
base_manpower = 2
capital = "Ottusius"
owner = CG9
controller = CG9
is_city = yes
add_core = CG9
57.1.1 = { owner = ALG controller = ALG add_core = ALG } 
91.1.1 = { religion = nedic_pantheon } 
110.1.1 = { culture = nedic } 