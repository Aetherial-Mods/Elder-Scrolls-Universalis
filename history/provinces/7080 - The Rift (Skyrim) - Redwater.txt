religion = snow_elves_pantheon
culture = snow_elven
hre = no
base_tax = 2
base_production = 2
trade_goods = unknown
base_manpower = 2
capital = "Redwater"
owner = NE1
controller = NE1
add_core = NE1
is_city = yes
57.1.1 = { owner = NG7 controller = NG7 add_core = NG7 } 
126.1.1 = { 
    owner = NA3
    controller = NA3
    add_core = NA3
    hre = yes
}

152.1.1 = { 
    owner = SKY
    controller = SKY
    add_core = SKY
    religion = nordic_pantheon
}
