religion = nedic_pantheon
culture = nedic
hre = no
base_tax = 6
base_production = 6
trade_goods = unknown
base_manpower = 6
capital = "Bleak Flats"
owner = CJ0
controller = CJ0
add_core = CJ0
is_city = yes
center_of_trade = 1
57.1.1 = { owner = CD0 controller = CD0 add_core = CD0 } 
178.1.1 = { religion = sanguine_cult }