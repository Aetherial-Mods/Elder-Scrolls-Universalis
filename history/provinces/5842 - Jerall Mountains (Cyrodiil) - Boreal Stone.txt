religion = namira_cult
culture = nedic
hre = no
base_tax = 3
base_production = 3
trade_goods = unknown
base_manpower = 3
capital = "Boreal Stone"
owner = CI9
controller = CI9
add_core = CI9
is_city = yes
57.1.1 = { owner = CE3 controller = CE3 add_core = CE3 } 
152.1.1 = { owner = AYL controller = AYL add_core = AYL } 
178.1.1 = { religion = meridia_cult }