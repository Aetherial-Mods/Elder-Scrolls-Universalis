owner = NA3
controller = NA3
add_core = NA3
religion = nordic_pantheon
culture = nord
hre = yes
base_tax = 4
base_production = 4
trade_goods = naval_supplies
base_manpower = 4
capital = "Windhelm"
is_city = yes
center_of_trade = 2
add_permanent_province_modifier = { name = "land_trade_modifier" duration = -1 }
add_permanent_province_modifier = { name = "water_trade_modifier" duration = -1 }
add_province_triggered_modifier = thieves_guild_province
add_province_triggered_modifier = wharf_rats_province
add_province_triggered_modifier = baandari_pedlars_province
152.1.1 = { owner = SKY controller = SKY add_core = SKY }