religion = druidism
culture = breton
hre = no
base_tax = 1
base_production = 1
trade_goods = unknown
base_manpower = 1
capital = "Bregha-Fin"
owner = BE1
controller = BE1
add_core = BE1
is_city = yes
add_permanent_province_modifier = { name = "water_trade_modifier" duration = -1 }
57.1.1 = { owner = BB2 controller = BB2 add_core = BB2 } 