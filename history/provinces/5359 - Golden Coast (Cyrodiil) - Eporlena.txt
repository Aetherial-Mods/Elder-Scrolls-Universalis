religion = molag_bal_cult
culture = ayleid
hre = no
base_tax = 2
base_production = 2
trade_goods = unknown
base_manpower = 2
capital = "Eporlena"
is_city = yes
owner = CC4
controller = CC4
add_core = CC4
add_permanent_province_modifier = { name = "water_trade_modifier" duration = -1 }