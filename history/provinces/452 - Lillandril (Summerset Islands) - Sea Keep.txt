owner = SB9
controller = SB9
add_core = SB9
is_city = yes
religion = altmeri_pantheon
culture = altmer
hre = no
base_tax = 1
base_production = 1
trade_goods = unknown
base_manpower = 1
capital = "Sea Keep"
center_of_trade = 1
add_permanent_province_modifier = { name = "water_trade_modifier" duration = -1 }