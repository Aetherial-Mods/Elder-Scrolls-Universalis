religion = snow_elves_pantheon
culture = snow_elven
hre = no
base_tax = 1
base_production = 1
trade_goods = iron
base_manpower = 1
capital = "Autumn's-Gate"
owner = NA5
controller = NA5
add_core = NA5
is_city = yes
57.1.1 = { owner = NG6 controller = NG6 add_core = NG6 } 
126.1.1 = { owner = SNW controller = SNW add_core = SNW } 
152.1.1 = { 
    owner = NA5
    controller = NA5
    add_core = NA5
    religion = nordic_pantheon
    hre = yes
}
