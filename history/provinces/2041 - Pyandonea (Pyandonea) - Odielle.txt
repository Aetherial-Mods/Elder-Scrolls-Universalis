owner = PC2
controller = PC2
add_core = PC2
religion = serpant_king
culture = maormer
hre = no
base_tax = 3
base_production = 3
trade_goods = unknown
base_manpower = 3
capital = "Odielle"
is_city = yes
add_permanent_province_modifier = { name = "water_trade_modifier" duration = -1 }