owner = MC5
controller = MC5
add_core = MC5
religion = chimer_pantheon
culture = ashlander_chimer
hre = no
base_tax = 2
base_production = 2
trade_goods = unknown
base_manpower = 2
capital = "Gargen Huul"
is_city = yes
center_of_trade = 1
57.1.1 = { owner = --- controller = --- } 
190.1.1 = { owner = MH3 controller = MH3 add_core = MH3 }