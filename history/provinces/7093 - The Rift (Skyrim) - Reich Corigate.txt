religion = snow_elves_pantheon
culture = snow_elven
base_tax = 2
base_production = 2
trade_goods = unknown
base_manpower = 2
capital = "Reich Corigate"
owner = NE1
controller = NE1
is_city = yes
add_core = NE1
57.1.1 = { owner = NA3 controller = NA3 add_core = NA3 } 
91.1.1 = { religion = nordic_pantheon } 
126.1.1 = { hre = yes } 
152.1.1 = { owner = SKY controller = SKY add_core = SKY } 