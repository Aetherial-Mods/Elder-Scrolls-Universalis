religion = sheogorath_cult
culture = ayleid
hre = no
base_tax = 1
base_production = 1
trade_goods = unknown
base_manpower = 1
capital = "Zarira"
owner = HG5
controller = HG5
is_city = yes
add_core = HG5
add_permanent_province_modifier = { name = "land_trade_modifier" duration = -1 }