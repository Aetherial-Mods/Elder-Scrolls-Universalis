religion = druidism
culture = horsemen
base_tax = 3
base_production = 3
trade_goods = unknown
base_manpower = 3
capital = "Jackdaw Cove"
is_city = yes
owner = BE8
controller = BE8
add_core = BE8
add_permanent_province_modifier = { name = "water_trade_modifier" duration = -1 }