religion = ayleid_pantheon
culture = ayleid
base_tax = 1
base_production = 1
trade_goods = unknown
base_manpower = 1
capital = "Arananga"
owner = VD3
controller = VD3
add_core = VD3
is_city = yes
hre = no
fort_15th = yes
57.1.1 = {
	owner = VE0
	controller = VE0
	add_core = VE0
	remove_core = VD3
}

