religion = hist
culture = agaceph
hre = no
base_tax = 1
base_production = 1
trade_goods = unknown
base_manpower = 1
capital = "Ahelene"
owner = CG9
controller = CG9
is_city = yes
add_core = CG9
57.1.1 = { owner = ALG controller = ALG add_core = ALG } 
91.1.1 = { religion = nedic_pantheon }
110.1.1 = { culture = nedic }