religion = nordic_pantheon
culture = nord
hre = yes
base_tax = 3
base_production = 3
trade_goods = unknown
base_manpower = 3
capital = "Lydny"
owner = NA3
controller = NA3
is_city = yes
add_core = NA3
add_permanent_province_modifier = { name = "land_trade_modifier" duration = -1 }
152.1.1 = { owner = SKY controller = SKY add_core = SKY }