religion = khajiiti_pantheon
culture = khajiiti
hre = no
base_tax = 1
base_production = 1
trade_goods = unknown
base_manpower = 1
capital = "Abode of Ignominy"
owner = EA6
controller = EA6
add_core = EA6
is_city = yes
add_permanent_province_modifier = { name = "land_trade_modifier" duration = -1 }