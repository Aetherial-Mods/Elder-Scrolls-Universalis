religion = altmeri_pantheon
culture = altmer
hre = no
base_tax = 2
base_production = 2
trade_goods = unknown
base_manpower = 2
capital = "Hlidalaure"
owner = SB3
controller = SB3
add_core = SB3
is_city = yes
add_permanent_province_modifier = { name = "water_trade_modifier" duration = -1 }