religion = reason_and_logic_cult
culture = dwemer
hre = no
base_tax = 2
base_production = 2
trade_goods = unknown
base_manpower = 2
capital = "Brothers of Strife"
is_city = yes
owner = ME1
controller = ME1
add_core = ME1
add_permanent_province_modifier = { name = "water_trade_modifier" duration = -1 } 