owner = MD6
controller = MD6
add_core = MD6
religion = chimer_pantheon
culture = ashlander_chimer
hre = no
base_tax = 1
base_production = 1
trade_goods = unknown
base_manpower = 1
capital = "Vathras"
is_city = yes
57.1.1 = { owner = --- controller = --- }
190.1.1 = { owner = MD6 controller = MD6 add_core = MD6 }