religion = hist
culture = naga
hre = no
base_tax = 1
base_production = 1
trade_goods = unknown
base_manpower = 1
capital = "Deeteeta"
owner = AE8
controller = AE8
add_core = AE8
is_city = yes
add_permanent_province_modifier = { name = "water_trade_modifier" duration = -1 } 
57.1.1 = { owner = --- controller = --- remove_core = AE8 } 
178.1.1 = { religion = ayleid_pantheon culture = barsaebic owner = AE8 controller = AE8 add_core = AE8 }