owner = MI9
controller = MI9
add_core = MI9
religion = chimer_pantheon
culture = ashlander_chimer
hre = no
base_tax = 2
base_production = 2
trade_goods = unknown
base_manpower = 2
capital = "Khuul"
is_city = yes
add_permanent_province_modifier = { name = "water_trade_modifier" duration = -1 }