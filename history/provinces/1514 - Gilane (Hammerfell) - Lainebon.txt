religion = redguard_pantheon
culture = redguard
hre = no
base_tax = 3
base_production = 3
trade_goods = unknown
base_manpower = 3
capital = "Lainebon"
owner = HE9
controller = HE9
is_city = yes
add_core = HE9
fort_15th = yes
center_of_trade = 1
add_permanent_province_modifier = { name = "water_trade_modifier" duration = -1 }
57.1.1 = {
	religion = malacath_cult
	culture = giant_goblin
	owner = ---
	controller = ---
	remove_core = HE9
}

