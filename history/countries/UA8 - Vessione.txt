government = monarchy
government_rank = 1
mercantilism = 1
technology_group = akavir_tg
religion = dragon_cult
primary_culture = akaviri
capital = 3534

54.1.1 = {
	monarch = {
 		name = "Nisolas"
		dynasty = "Vi-Acsais"
		birth_date = 32.1.1
		adm = 5
		dip = 1
		mil = 1
    }
	queen = {
 		name = "Daphnaia"
		dynasty = "Vi-Acsais"
		birth_date = 28.1.1
		adm = 2
		dip = 0
		mil = 0
		female = yes
    }
	heir = {
 		name = "Paphopheu"
		monarch_name = "Paphopheu I"
		dynasty = "Vi-Acsais"
		birth_date = 48.1.1
		death_date = 117.1.1
		claim = 100
		adm = 6
		dip = 5
		mil = 5
		female = yes
    }
}

117.1.1 = {
	monarch = {
 		name = "Salmysios"
		dynasty = "Du-Demithios"
		birth_date = 92.1.1
		adm = 1
		dip = 6
		mil = 1
    }
}

171.1.1 = {
	monarch = {
 		name = "Thesassea"
		dynasty = "Vu-Lephenis"
		birth_date = 120.1.1
		adm = 1
		dip = 3
		mil = 1
		female = yes
    }
}

233.1.1 = {
	monarch = {
 		name = "Thanistae"
		dynasty = "Vu-Thelophi"
		birth_date = 183.1.1
		adm = 6
		dip = 2
		mil = 5
		female = yes
    }
	queen = {
 		name = "Salmeros"
		dynasty = "Vu-Thelophi"
		birth_date = 195.1.1
		adm = 3
		dip = 1
		mil = 4
    }
}

271.1.1 = {
	monarch = {
 		name = "Arsastus"
		dynasty = "Vi-Rhaeniophai"
		birth_date = 243.1.1
		adm = 1
		dip = 0
		mil = 0
    }
}

322.1.1 = {
	monarch = {
 		name = "Xanthatos"
		dynasty = "Di-Dionysanos"
		birth_date = 293.1.1
		adm = 6
		dip = 6
		mil = 4
    }
}

390.1.1 = {
	monarch = {
 		name = "Taphithe"
		dynasty = "Vi-Acaliphelia"
		birth_date = 342.1.1
		adm = 5
		dip = 5
		mil = 1
		female = yes
    }
}

450.1.1 = {
	monarch = {
 		name = "Alpheas"
		dynasty = "Du-Caesanthus"
		birth_date = 423.1.1
		adm = 3
		dip = 4
		mil = 4
    }
}

506.1.1 = {
	monarch = {
 		name = "Salmyx"
		dynasty = "Di-Salmyx"
		birth_date = 463.1.1
		adm = 2
		dip = 1
		mil = 5
    }
}

601.1.1 = {
	monarch = {
 		name = "Alethrusa"
		dynasty = "Di-Typhaus"
		birth_date = 551.1.1
		adm = 1
		dip = 0
		mil = 1
		female = yes
    }
	queen = {
 		name = "Absyristhus"
		dynasty = "Di-Typhaus"
		birth_date = 583.1.1
		adm = 4
		dip = 6
		mil = 0
    }
	heir = {
 		name = "Isocrerios"
		monarch_name = "Isocrerios I"
		dynasty = "Di-Typhaus"
		birth_date = 592.1.1
		death_date = 666.1.1
		claim = 100
		adm = 5
		dip = 2
		mil = 1
    }
}

666.1.1 = {
	monarch = {
 		name = "Rhaenosia"
		dynasty = "Di-Krathisthus"
		birth_date = 630.1.1
		adm = 4
		dip = 5
		mil = 1
		female = yes
    }
}

708.1.1 = {
	monarch = {
 		name = "Typhaus"
		dynasty = "Du-Zetiaraus"
		birth_date = 689.1.1
		adm = 2
		dip = 5
		mil = 5
    }
	queen = {
 		name = "Thronassea"
		dynasty = "Du-Zetiaraus"
		birth_date = 667.1.1
		adm = 6
		dip = 3
		mil = 4
		female = yes
    }
	heir = {
 		name = "Hespips"
		monarch_name = "Hespips I"
		dynasty = "Du-Zetiaraus"
		birth_date = 701.1.1
		death_date = 748.1.1
		claim = 100
		adm = 3
		dip = 2
		mil = 3
    }
}

748.1.1 = {
	monarch = {
 		name = "Aristorus"
		dynasty = "Vi-Nethoesa"
		birth_date = 701.1.1
		adm = 6
		dip = 3
		mil = 5
    }
}

783.1.1 = {
	monarch = {
 		name = "Rhaenosia"
		dynasty = "Du-Phrixice"
		birth_date = 746.1.1
		adm = 4
		dip = 2
		mil = 2
		female = yes
    }
}

867.1.1 = {
	monarch = {
 		name = "Othrithoe"
		dynasty = "Vi-Thyxaphine"
		birth_date = 839.1.1
		adm = 6
		dip = 4
		mil = 4
		female = yes
    }
}

956.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Ephiste"
		monarch_name = "Ephiste I"
		dynasty = "Di-Ephiste"
		birth_date = 941.1.1
		death_date = 959.1.1
		claim = 100
		adm = 4
		dip = 3
		mil = 0
    }
}

959.1.1 = {
	monarch = {
 		name = "Brethiophai"
		dynasty = "Du-Phlegixus"
		birth_date = 935.1.1
		adm = 0
		dip = 4
		mil = 3
		female = yes
    }
}

1049.1.1 = {
	monarch = {
 		name = "Zalisthus"
		dynasty = "Vu-Syrixie"
		birth_date = 999.1.1
		adm = 5
		dip = 3
		mil = 6
    }
	queen = {
 		name = "Selassa"
		dynasty = "Vu-Syrixie"
		birth_date = 1004.1.1
		adm = 2
		dip = 2
		mil = 5
		female = yes
    }
	heir = {
 		name = "Demithios"
		monarch_name = "Demithios I"
		dynasty = "Vu-Syrixie"
		birth_date = 1045.1.1
		death_date = 1136.1.1
		claim = 100
		adm = 6
		dip = 0
		mil = 4
    }
}

1136.1.1 = {
	monarch = {
 		name = "Mosophor"
		dynasty = "Di-Antipherios"
		birth_date = 1092.1.1
		adm = 5
		dip = 4
		mil = 4
    }
	queen = {
 		name = "Nyshophila"
		dynasty = "Di-Antipherios"
		birth_date = 1107.1.1
		adm = 6
		dip = 0
		mil = 6
		female = yes
    }
	heir = {
 		name = "Syrixie"
		monarch_name = "Syrixie I"
		dynasty = "Di-Antipherios"
		birth_date = 1132.1.1
		death_date = 1171.1.1
		claim = 100
		adm = 2
		dip = 6
		mil = 4
		female = yes
    }
}

1171.1.1 = {
	monarch = {
 		name = "Salmeros"
		dynasty = "Vu-Selassa"
		birth_date = 1131.1.1
		adm = 2
		dip = 2
		mil = 5
    }
	queen = {
 		name = "Selassa"
		dynasty = "Vu-Selassa"
		birth_date = 1146.1.1
		adm = 6
		dip = 1
		mil = 4
		female = yes
    }
}

1243.1.1 = {
	monarch = {
 		name = "Soteryx"
		dynasty = "Vi-Rhaenosia"
		birth_date = 1206.1.1
		adm = 4
		dip = 0
		mil = 0
    }
}

1310.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Amycades"
		monarch_name = "Amycades I"
		dynasty = "Vu-Taphiphe"
		birth_date = 1295.1.1
		death_date = 1313.1.1
		claim = 100
		adm = 5
		dip = 4
		mil = 5
    }
}

1313.1.1 = {
	monarch = {
 		name = "Galixaia"
		dynasty = "Di-Therrastos"
		birth_date = 1284.1.1
		adm = 0
		dip = 5
		mil = 1
		female = yes
    }
}

1372.1.1 = {
	monarch = {
 		name = "Leuceshi"
		dynasty = "Vi-Prisenis"
		birth_date = 1319.1.1
		adm = 6
		dip = 5
		mil = 4
		female = yes
    }
}

1468.1.1 = {
	monarch = {
 		name = "Alethrusa"
		dynasty = "Vi-Mystophi"
		birth_date = 1435.1.1
		adm = 5
		dip = 1
		mil = 5
		female = yes
    }
}

1513.1.1 = {
	monarch = {
 		name = "Absyristhus"
		dynasty = "Du-Isocrerios"
		birth_date = 1491.1.1
		adm = 3
		dip = 0
		mil = 1
    }
}

1596.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Antipherios"
		monarch_name = "Antipherios I"
		dynasty = "Vu-Misypso"
		birth_date = 1596.1.1
		death_date = 1614.1.1
		claim = 100
		adm = 4
		dip = 4
		mil = 6
    }
}

1614.1.1 = {
	monarch = {
 		name = "Xenades"
		dynasty = "Di-Lichax"
		birth_date = 1575.1.1
		adm = 0
		dip = 6
		mil = 2
    }
	queen = {
 		name = "Adrasaxaura"
		dynasty = "Di-Lichax"
		birth_date = 1580.1.1
		adm = 4
		dip = 4
		mil = 0
		female = yes
    }
	heir = {
 		name = "Arsax"
		monarch_name = "Arsax I"
		dynasty = "Di-Lichax"
		birth_date = 1602.1.1
		death_date = 1667.1.1
		claim = 100
		adm = 1
		dip = 3
		mil = 6
    }
}

1667.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Dephophoros"
		monarch_name = "Dephophoros I"
		dynasty = "Du-Laestithous"
		birth_date = 1655.1.1
		death_date = 1673.1.1
		claim = 100
		adm = 6
		dip = 2
		mil = 3
    }
}

1673.1.1 = {
	monarch = {
 		name = "Erasmys"
		dynasty = "Vu-Silishia"
		birth_date = 1646.1.1
		adm = 2
		dip = 3
		mil = 5
    }
	queen = {
 		name = "Nyxophi"
		dynasty = "Vu-Silishia"
		birth_date = 1629.1.1
		adm = 5
		dip = 2
		mil = 4
		female = yes
    }
	heir = {
 		name = "Demithios"
		monarch_name = "Demithios I"
		dynasty = "Vu-Silishia"
		birth_date = 1672.1.1
		death_date = 1759.1.1
		claim = 100
		adm = 2
		dip = 0
		mil = 3
    }
}

1759.1.1 = {
	monarch = {
 		name = "Nisolas"
		dynasty = "Du-Sotalus"
		birth_date = 1725.1.1
		adm = 2
		dip = 4
		mil = 4
    }
	queen = {
 		name = "Prisenis"
		dynasty = "Du-Sotalus"
		birth_date = 1707.1.1
		adm = 6
		dip = 2
		mil = 3
		female = yes
    }
}

1800.1.1 = {
	monarch = {
 		name = "Anathios"
		dynasty = "Vu-Prisishia"
		birth_date = 1776.1.1
		adm = 4
		dip = 1
		mil = 6
    }
	queen = {
 		name = "Acsais"
		dynasty = "Vu-Prisishia"
		birth_date = 1774.1.1
		adm = 1
		dip = 0
		mil = 5
		female = yes
    }
	heir = {
 		name = "Salmepios"
		monarch_name = "Salmepios I"
		dynasty = "Vu-Prisishia"
		birth_date = 1791.1.1
		death_date = 1852.1.1
		claim = 100
		adm = 2
		dip = 1
		mil = 3
    }
}

1852.1.1 = {
	monarch = {
 		name = "Demithios"
		dynasty = "Vi-Nethiasse"
		birth_date = 1831.1.1
		adm = 5
		dip = 2
		mil = 5
    }
}

1889.1.1 = {
	monarch = {
 		name = "Ulyssolos"
		dynasty = "Vi-Siliphi"
		birth_date = 1844.1.1
		adm = 3
		dip = 1
		mil = 2
    }
	queen = {
 		name = "Oriphosa"
		dynasty = "Vi-Siliphi"
		birth_date = 1855.1.1
		adm = 0
		dip = 0
		mil = 1
		female = yes
    }
	heir = {
 		name = "Vasios"
		monarch_name = "Vasios I"
		dynasty = "Vi-Siliphi"
		birth_date = 1886.1.1
		death_date = 1941.1.1
		claim = 100
		adm = 4
		dip = 5
		mil = 0
    }
}

1941.1.1 = {
	monarch = {
 		name = "Xanthatos"
		dynasty = "Vu-Galixeilla"
		birth_date = 1923.1.1
		adm = 3
		dip = 2
		mil = 0
    }
}

2021.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Herastus"
		monarch_name = "Herastus I"
		dynasty = "Vu-Iasephia"
		birth_date = 2017.1.1
		death_date = 2035.1.1
		claim = 100
		adm = 0
		dip = 4
		mil = 0
    }
}

2035.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Anasztochus"
		monarch_name = "Anasztochus I"
		dynasty = "Di-Ulyssolos"
		birth_date = 2033.1.1
		death_date = 2051.1.1
		claim = 100
		adm = 2
		dip = 5
		mil = 2
    }
}

2051.1.1 = {
	monarch = {
 		name = "Zalothius"
		dynasty = "Di-Arthatos"
		birth_date = 1998.1.1
		adm = 5
		dip = 6
		mil = 4
    }
	queen = {
 		name = "Ashertes"
		dynasty = "Di-Arthatos"
		birth_date = 2011.1.1
		adm = 2
		dip = 5
		mil = 3
		female = yes
    }
	heir = {
 		name = "Hespocia"
		monarch_name = "Hespocia I"
		dynasty = "Di-Arthatos"
		birth_date = 2045.1.1
		death_date = 2114.1.1
		claim = 100
		adm = 6
		dip = 3
		mil = 2
    }
}

2114.1.1 = {
	monarch = {
 		name = "Caphaneithes"
		dynasty = "		"
		birth_date = 2085.1.1
		adm = 1
		dip = 5
		mil = 4
    }
	queen = {
 		name = "Nyshassa"
		dynasty = "		"
		birth_date = 2092.1.1
		adm = 5
		dip = 3
		mil = 3
		female = yes
    }
	heir = {
 		name = "Salmoebus"
		monarch_name = "Salmoebus I"
		dynasty = "		"
		birth_date = 2112.1.1
		death_date = 2161.1.1
		claim = 100
		adm = 0
		dip = 4
		mil = 1
    }
}

2161.1.1 = {
	monarch = {
 		name = "Tarasatos"
		dynasty = "Vi-Alethrusa"
		birth_date = 2136.1.1
		adm = 6
		dip = 0
		mil = 1
    }
	queen = {
 		name = "Savariphei"
		dynasty = "Vi-Alethrusa"
		birth_date = 2129.1.1
		adm = 3
		dip = 6
		mil = 0
		female = yes
    }
	heir = {
 		name = "Misypso"
		monarch_name = "Misypso I"
		dynasty = "Vi-Alethrusa"
		birth_date = 2152.1.1
		death_date = 2206.1.1
		claim = 100
		adm = 0
		dip = 4
		mil = 6
		female = yes
    }
}

2206.1.1 = {
	monarch = {
 		name = "Brasas"
		dynasty = "Di-Lichax"
		birth_date = 2155.1.1
		adm = 3
		dip = 6
		mil = 2
    }
	queen = {
 		name = "Orethaeia"
		dynasty = "Di-Lichax"
		birth_date = 2180.1.1
		adm = 6
		dip = 4
		mil = 0
		female = yes
    }
	heir = {
 		name = "Hespice"
		monarch_name = "Hespice I"
		dynasty = "Di-Lichax"
		birth_date = 2198.1.1
		death_date = 2261.1.1
		claim = 100
		adm = 3
		dip = 3
		mil = 6
    }
}

2261.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Thespisha"
		monarch_name = "Thespisha I"
		dynasty = "Du-Soterixus"
		birth_date = 2252.1.1
		death_date = 2270.1.1
		claim = 100
		adm = 2
		dip = 2
		mil = 3
		female = yes
    }
}

2270.1.1 = {
	monarch = {
 		name = "Antiphoebus"
		dynasty = "Vi-Misais"
		birth_date = 2250.1.1
		adm = 1
		dip = 5
		mil = 3
    }
}

2307.1.1 = {
	monarch = {
 		name = "Jaselphos"
		dynasty = "Du-Soterixus"
		birth_date = 2280.1.1
		adm = 6
		dip = 5
		mil = 0
    }
}

2403.1.1 = {
	monarch = {
 		name = "Ancochus"
		dynasty = "Du-Athanersis"
		birth_date = 2383.1.1
		adm = 4
		dip = 4
		mil = 4
    }
}

2461.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Salmysios"
		monarch_name = "Salmysios I"
		dynasty = "Du-Eustolos"
		birth_date = 2448.1.1
		death_date = 2466.1.1
		claim = 100
		adm = 3
		dip = 3
		mil = 0
    }
}

2466.1.1 = {
	monarch = {
 		name = "Thitous"
		dynasty = "Vu-Selassa"
		birth_date = 2421.1.1
		adm = 5
		dip = 4
		mil = 3
    }
}

