government = tribal
government_rank = 1
mercantilism = 1
technology_group = cyrodiil_tg
religion = nedic_pantheon
primary_culture = nedic
capital = 5830

54.1.1 = {
	monarch = {
 		name = "Zakysor"
		dynasty = "Hynhaolen"
		birth_date = 31.1.1
		adm = 1
		dip = 2
		mil = 0
    }
}

112.1.1 = {
	monarch = {
 		name = "Ellur"
		dynasty = "Taatysoth"
		birth_date = 68.1.1
		adm = 6
		dip = 2
		mil = 3
		female = yes
    }
}

151.1.1 = {
	monarch = {
 		name = "Ytas"
		dynasty = "Malrasoc"
		birth_date = 130.1.1
		adm = 1
		dip = 3
		mil = 5
    }
}

197.1.1 = {
	monarch = {
 		name = "Dadrades"
		dynasty = "Vrurlauleth"
		birth_date = 148.1.1
		adm = 0
		dip = 2
		mil = 1
    }
}

291.1.1 = {
	monarch = {
 		name = "Nathael"
		dynasty = "Lysteth"
		birth_date = 241.1.1
		adm = 5
		dip = 1
		mil = 5
		female = yes
    }
}

383.1.1 = {
	monarch = {
 		name = "Haonuvic"
		dynasty = "Tirlaasir"
		birth_date = 343.1.1
		adm = 3
		dip = 0
		mil = 2
    }
}

459.1.1 = {
	monarch = {
 		name = "Oman"
		dynasty = "Kynhesac"
		birth_date = 439.1.1
		adm = 1
		dip = 0
		mil = 5
		female = yes
    }
}

541.1.1 = {
	monarch = {
 		name = "Viladuh"
		dynasty = "Dulros"
		birth_date = 516.1.1
		adm = 0
		dip = 6
		mil = 2
		female = yes
    }
}

593.1.1 = {
	monarch = {
 		name = "Bakumoth"
		dynasty = "Gumrok"
		birth_date = 560.1.1
		adm = 5
		dip = 5
		mil = 6
    }
}

658.1.1 = {
	monarch = {
 		name = "Linne"
		dynasty = "Tirlaasir"
		birth_date = 634.1.1
		adm = 3
		dip = 4
		mil = 2
		female = yes
    }
}

718.1.1 = {
	monarch = {
 		name = "Resa"
		dynasty = "Vauleso"
		birth_date = 696.1.1
		adm = 5
		dip = 6
		mil = 4
		female = yes
    }
}

755.1.1 = {
	monarch = {
 		name = "Zassun"
		dynasty = "Vauleso"
		birth_date = 702.1.1
		adm = 3
		dip = 5
		mil = 0
		female = yes
    }
}

796.1.1 = {
	monarch = {
 		name = "Nemehul"
		dynasty = "Vauleso"
		birth_date = 766.1.1
		adm = 1
		dip = 4
		mil = 4
		female = yes
    }
}

843.1.1 = {
	monarch = {
 		name = "Essar"
		dynasty = "Dadrades"
		birth_date = 822.1.1
		adm = 0
		dip = 3
		mil = 1
		female = yes
    }
}

901.1.1 = {
	monarch = {
 		name = "Gukhedas"
		dynasty = "Vrurlauleth"
		birth_date = 876.1.1
		adm = 5
		dip = 2
		mil = 4
    }
}

985.1.1 = {
	monarch = {
 		name = "Tirlaasir"
		dynasty = "Tirleros"
		birth_date = 967.1.1
		adm = 3
		dip = 2
		mil = 1
    }
}

1038.1.1 = {
	monarch = {
 		name = "Aroc"
		dynasty = "Dyrmec"
		birth_date = 1002.1.1
		adm = 1
		dip = 1
		mil = 5
    }
}

1087.1.1 = {
	monarch = {
 		name = "Essar"
		dynasty = "Demralis"
		birth_date = 1042.1.1
		adm = 0
		dip = 0
		mil = 1
		female = yes
    }
}

1122.1.1 = {
	monarch = {
 		name = "Gukhedas"
		dynasty = "Vrurlauleth"
		birth_date = 1077.1.1
		adm = 2
		dip = 1
		mil = 3
    }
}

1162.1.1 = {
	monarch = {
 		name = "Vallal"
		dynasty = "Virdanath"
		birth_date = 1130.1.1
		adm = 0
		dip = 0
		mil = 0
		female = yes
    }
}

1241.1.1 = {
	monarch = {
 		name = "Larduve"
		dynasty = "Vaorylan"
		birth_date = 1204.1.1
		adm = 5
		dip = 0
		mil = 3
    }
}

1314.1.1 = {
	monarch = {
 		name = "Ralreloc"
		dynasty = "Tirdor"
		birth_date = 1287.1.1
		adm = 3
		dip = 6
		mil = 0
    }
}

1373.1.1 = {
	monarch = {
 		name = "Atholur"
		dynasty = "Aalezos"
		birth_date = 1338.1.1
		adm = 2
		dip = 5
		mil = 3
		female = yes
    }
}

1448.1.1 = {
	monarch = {
 		name = "Ashodev"
		dynasty = "Runhath"
		birth_date = 1401.1.1
		adm = 0
		dip = 4
		mil = 0
		female = yes
    }
}

1492.1.1 = {
	monarch = {
 		name = "Larduve"
		dynasty = "Nirdoc"
		birth_date = 1459.1.1
		adm = 5
		dip = 3
		mil = 4
    }
}

1551.1.1 = {
	monarch = {
 		name = "Ralreloc"
		dynasty = "Ludec"
		birth_date = 1533.1.1
		adm = 3
		dip = 3
		mil = 0
    }
}

1638.1.1 = {
	monarch = {
 		name = "Dulros"
		dynasty = "Ralreloc"
		birth_date = 1610.1.1
		adm = 5
		dip = 4
		mil = 2
    }
}

1705.1.1 = {
	monarch = {
 		name = "Vristidok"
		dynasty = "Tykisok"
		birth_date = 1673.1.1
		adm = 2
		dip = 6
		mil = 2
    }
}

1746.1.1 = {
	monarch = {
 		name = "Naamaulec"
		dynasty = "Yhin"
		birth_date = 1706.1.1
		adm = 0
		dip = 5
		mil = 6
    }
}

1809.1.1 = {
	monarch = {
 		name = "Zunhe"
		dynasty = "Ytas"
		birth_date = 1770.1.1
		adm = 2
		dip = 6
		mil = 0
    }
}

1882.1.1 = {
	monarch = {
 		name = "Zahoti"
		dynasty = "Vredraonir"
		birth_date = 1863.1.1
		adm = 1
		dip = 6
		mil = 4
		female = yes
    }
}

1976.1.1 = {
	monarch = {
 		name = "Dirrinuh"
		dynasty = "Idith"
		birth_date = 1941.1.1
		adm = 6
		dip = 5
		mil = 0
		female = yes
    }
}

2057.1.1 = {
	monarch = {
 		name = "Nhekhedin"
		dynasty = "Vaulic"
		birth_date = 2026.1.1
		adm = 4
		dip = 4
		mil = 4
    }
}

2101.1.1 = {
	monarch = {
 		name = "Reshoha"
		dynasty = "Nuvner"
		birth_date = 2058.1.1
		adm = 2
		dip = 3
		mil = 1
		female = yes
    }
}

2198.1.1 = {
	monarch = {
 		name = "Shelren"
		dynasty = "Idith"
		birth_date = 2166.1.1
		adm = 1
		dip = 2
		mil = 4
    }
}

2279.1.1 = {
	monarch = {
 		name = "Lukaazon"
		dynasty = "Dyrmec"
		birth_date = 2259.1.1
		adm = 6
		dip = 2
		mil = 1
    }
}

2372.1.1 = {
	monarch = {
 		name = "Oshan"
		dynasty = "Vrurlauleth"
		birth_date = 2334.1.1
		adm = 4
		dip = 1
		mil = 5
		female = yes
    }
}

2416.1.1 = {
	monarch = {
 		name = "Hiherev"
		dynasty = "Gaohen"
		birth_date = 2398.1.1
		adm = 6
		dip = 2
		mil = 6
		female = yes
    }
}

2469.1.1 = {
	monarch = {
 		name = "Vaorylan"
		dynasty = "Likoth"
		birth_date = 2422.1.1
		adm = 4
		dip = 1
		mil = 3
    }
}

