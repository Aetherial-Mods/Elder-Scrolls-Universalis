government = monarchy
government_rank = 1
mercantilism = 1
technology_group = northern_tg
religion = druidism
primary_culture = breton
capital = 1369

54.1.1 = {
	monarch = {
 		name = "Gaelora"
		dynasty = "Daggerfall"
		birth_date = 1.1.1
		adm = 4
		dip = 3
		mil = 2
		female = yes
    }
	queen = {
 		name = "Phillic"
		dynasty = "Fanis"
		birth_date = 17.1.1
		adm = 0
		dip = 2
		mil = 1
    }
}

135.1.1 = {
	monarch = {
 		name = "Milo"
		dynasty = "Daggerfall"
		birth_date = 112.1.1
		adm = 6
		dip = 1
		mil = 4
    }
}

210.1.1 = {
	monarch = {
 		name = "Gib"
		dynasty = "Daggerfall"
		birth_date = 169.1.1
		adm = 4
		dip = 0
		mil = 1
    }
}

271.1.1 = {
	monarch = {
 		name = "Charleric"
		dynasty = "Daggerfall"
		birth_date = 223.1.1
		adm = 2
		dip = 6
		mil = 5
    }
	queen = {
 		name = "Arzhela"
		dynasty = "Ales"
		birth_date = 251.1.1
		adm = 6
		dip = 5
		mil = 4
		female = yes
    }
	heir = {
 		name = "Belic"
		monarch_name = "Belic I"
		dynasty = "Daggerfall"
		birth_date = 269.1.1
		death_date = 366.1.1
		claim = 100
		adm = 3
		dip = 3
		mil = 3
    }
}

366.1.1 = {
	monarch = {
 		name = "Michel"
		dynasty = "Daggerfall"
		birth_date = 332.1.1
		adm = 2
		dip = 0
		mil = 3
    }
	queen = {
 		name = "Lynie"
		dynasty = "Bruhl"
		birth_date = 333.1.1
		adm = 3
		dip = 3
		mil = 4
		female = yes
    }
	heir = {
 		name = "Blasius"
		monarch_name = "Blasius I"
		dynasty = "Daggerfall"
		birth_date = 354.1.1
		death_date = 407.1.1
		claim = 100
		adm = 6
		dip = 2
		mil = 3
    }
}

407.1.1 = {
	monarch = {
 		name = "Miranda"
		dynasty = "Daggerfall"
		birth_date = 375.1.1
		adm = 6
		dip = 5
		mil = 3
		female = yes
    }
	queen = {
 		name = "Atroque"
		dynasty = "Jes"
		birth_date = 373.1.1
		adm = 3
		dip = 4
		mil = 2
    }
}

471.1.1 = {
	monarch = {
 		name = "Adair"
		dynasty = "Daggerfall"
		birth_date = 418.1.1
		adm = 1
		dip = 3
		mil = 6
    }
	queen = {
 		name = "Germonde"
		dynasty = "Fausta"
		birth_date = 429.1.1
		adm = 5
		dip = 2
		mil = 5
		female = yes
    }
	heir = {
 		name = "Velian"
		monarch_name = "Velian I"
		dynasty = "Daggerfall"
		birth_date = 462.1.1
		death_date = 543.1.1
		claim = 100
		adm = 2
		dip = 0
		mil = 4
    }
}

543.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Mira"
		monarch_name = "Mira I"
		dynasty = "Daggerfall"
		birth_date = 539.1.1
		death_date = 557.1.1
		claim = 100
		adm = 0
		dip = 6
		mil = 0
		female = yes
    }
}

557.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Gordyn"
		monarch_name = "Gordyn I"
		dynasty = "Daggerfall"
		birth_date = 548.1.1
		death_date = 566.1.1
		claim = 100
		adm = 5
		dip = 5
		mil = 4
    }
}

566.1.1 = {
	monarch = {
 		name = "Achane"
		dynasty = "Daggerfall"
		birth_date = 536.1.1
		adm = 1
		dip = 0
		mil = 6
    }
	queen = {
 		name = "Urvien"
		dynasty = "Jenseric"
		birth_date = 526.1.1
		adm = 5
		dip = 5
		mil = 5
		female = yes
    }
	heir = {
 		name = "Varnis"
		monarch_name = "Varnis I"
		dynasty = "Daggerfall"
		birth_date = 556.1.1
		death_date = 623.1.1
		claim = 100
		adm = 2
		dip = 4
		mil = 4
    }
}

623.1.1 = {
	monarch = {
 		name = "Igleric"
		dynasty = "Daggerfall"
		birth_date = 602.1.1
		adm = 1
		dip = 0
		mil = 5
    }
	queen = {
 		name = "Guendeline"
		dynasty = "DuBois"
		birth_date = 585.1.1
		adm = 1
		dip = 4
		mil = 6
		female = yes
    }
}

669.1.1 = {
	monarch = {
 		name = "Remius"
		dynasty = "Daggerfall"
		birth_date = 628.1.1
		adm = 3
		dip = 5
		mil = 0
    }
	queen = {
 		name = "Muiri"
		dynasty = "Tardif"
		birth_date = 636.1.1
		adm = 3
		dip = 1
		mil = 1
		female = yes
    }
	heir = {
 		name = "Dorothella"
		monarch_name = "Dorothella I"
		dynasty = "Daggerfall"
		birth_date = 667.1.1
		death_date = 742.1.1
		claim = 100
		adm = 0
		dip = 0
		mil = 0
		female = yes
    }
}

742.1.1 = {
	monarch = {
 		name = "Robier"
		dynasty = "Daggerfall"
		birth_date = 707.1.1
		adm = 0
		dip = 3
		mil = 0
    }
}

782.1.1 = {
	monarch = {
 		name = "Kilian"
		dynasty = "Daggerfall"
		birth_date = 762.1.1
		adm = 5
		dip = 3
		mil = 4
    }
}

825.1.1 = {
	monarch = {
 		name = "Enri"
		dynasty = "Daggerfall"
		birth_date = 782.1.1
		adm = 3
		dip = 2
		mil = 1
    }
	queen = {
 		name = "Mortine"
		dynasty = "Ragon"
		birth_date = 774.1.1
		adm = 0
		dip = 0
		mil = 0
		female = yes
    }
}

903.1.1 = {
	monarch = {
 		name = "Marcelyn"
		dynasty = "Daggerfall"
		birth_date = 870.1.1
		adm = 5
		dip = 0
		mil = 3
		female = yes
    }
}

951.1.1 = {
	monarch = {
 		name = "Francynak"
		dynasty = "Daggerfall"
		birth_date = 901.1.1
		adm = 4
		dip = 6
		mil = 0
    }
	queen = {
 		name = "Oriane"
		dynasty = "Bachand"
		birth_date = 903.1.1
		adm = 0
		dip = 4
		mil = 6
		female = yes
    }
	heir = {
 		name = "Endemel"
		monarch_name = "Endemel I"
		dynasty = "Daggerfall"
		birth_date = 944.1.1
		death_date = 997.1.1
		claim = 100
		adm = 4
		dip = 3
		mil = 5
    }
}

997.1.1 = {
	monarch = {
 		name = "Shillin"
		dynasty = "Daggerfall"
		birth_date = 973.1.1
		adm = 0
		dip = 4
		mil = 0
		female = yes
    }
}

1095.1.1 = {
	monarch = {
 		name = "Louveau"
		dynasty = "Daggerfall"
		birth_date = 1046.1.1
		adm = 2
		dip = 5
		mil = 2
    }
}

1153.1.1 = {
	monarch = {
 		name = "Frelausa"
		dynasty = "Daggerfall"
		birth_date = 1107.1.1
		adm = 0
		dip = 5
		mil = 5
		female = yes
    }
}

1207.1.1 = {
	monarch = {
 		name = "Marien"
		dynasty = "Daggerfall"
		birth_date = 1189.1.1
		adm = 5
		dip = 4
		mil = 2
		female = yes
    }
	queen = {
 		name = "Alessio"
		dynasty = "Orinth"
		birth_date = 1160.1.1
		adm = 2
		dip = 2
		mil = 1
    }
	heir = {
 		name = "Laroche"
		monarch_name = "Laroche I"
		dynasty = "Daggerfall"
		birth_date = 1205.1.1
		death_date = 1253.1.1
		claim = 100
		adm = 6
		dip = 1
		mil = 0
    }
}

1253.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Esmond"
		monarch_name = "Esmond I"
		dynasty = "Daggerfall"
		birth_date = 1250.1.1
		death_date = 1268.1.1
		claim = 100
		adm = 4
		dip = 0
		mil = 4
    }
}

1268.1.1 = {
	monarch = {
 		name = "Tamien"
		dynasty = "Daggerfall"
		birth_date = 1218.1.1
		adm = 0
		dip = 1
		mil = 6
    }
	queen = {
 		name = "Edeonore"
		dynasty = "Delrusc"
		birth_date = 1228.1.1
		adm = 4
		dip = 0
		mil = 5
		female = yes
    }
}

1354.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Gaboryan"
		monarch_name = "Gaboryan I"
		dynasty = "Daggerfall"
		birth_date = 1351.1.1
		death_date = 1369.1.1
		claim = 100
		adm = 5
		dip = 4
		mil = 3
    }
}

1369.1.1 = {
	monarch = {
 		name = "Vimy"
		dynasty = "Daggerfall"
		birth_date = 1322.1.1
		adm = 0
		dip = 5
		mil = 5
		female = yes
    }
	queen = {
 		name = "Sebastien"
		dynasty = "Restane"
		birth_date = 1336.1.1
		adm = 4
		dip = 4
		mil = 4
    }
	heir = {
 		name = "Surane"
		monarch_name = "Surane I"
		dynasty = "Daggerfall"
		birth_date = 1355.1.1
		death_date = 1430.1.1
		claim = 100
		adm = 1
		dip = 3
		mil = 3
		female = yes
    }
}

1430.1.1 = {
	monarch = {
 		name = "Giraud"
		dynasty = "Daggerfall"
		birth_date = 1411.1.1
		adm = 1
		dip = 6
		mil = 4
    }
}

1519.1.1 = {
	monarch = {
 		name = "Christophe"
		dynasty = "Daggerfall"
		birth_date = 1466.1.1
		adm = 6
		dip = 5
		mil = 0
    }
}

1610.1.1 = {
	monarch = {
 		name = "Guillaume"
		dynasty = "Daggerfall"
		birth_date = 1565.1.1
		adm = 4
		dip = 4
		mil = 4
    }
	queen = {
 		name = "Sabinette"
		dynasty = "Mouriou"
		birth_date = 1566.1.1
		adm = 1
		dip = 3
		mil = 3
		female = yes
    }
	heir = {
 		name = "Gastau"
		monarch_name = "Gastau I"
		dynasty = "Daggerfall"
		birth_date = 1599.1.1
		death_date = 1652.1.1
		claim = 100
		adm = 5
		dip = 1
		mil = 2
    }
}

1652.1.1 = {
	monarch = {
 		name = "Vicente"
		dynasty = "Daggerfall"
		birth_date = 1633.1.1
		adm = 1
		dip = 3
		mil = 4
    }
	queen = {
 		name = "Farlivere"
		dynasty = "Gedanis"
		birth_date = 1610.1.1
		adm = 4
		dip = 1
		mil = 3
		female = yes
    }
	heir = {
 		name = "Thibault"
		monarch_name = "Thibault I"
		dynasty = "Daggerfall"
		birth_date = 1647.1.1
		death_date = 1699.1.1
		claim = 100
		adm = 1
		dip = 0
		mil = 2
    }
}

1699.1.1 = {
	monarch = {
 		name = "Gryf"
		dynasty = "Daggerfall"
		birth_date = 1679.1.1
		adm = 4
		dip = 1
		mil = 4
    }
	queen = {
 		name = "Frelausa"
		dynasty = "Yunlin"
		birth_date = 1646.1.1
		adm = 1
		dip = 0
		mil = 3
		female = yes
    }
	heir = {
 		name = "Gisele"
		monarch_name = "Gisele I"
		dynasty = "Daggerfall"
		birth_date = 1697.1.1
		death_date = 1756.1.1
		claim = 100
		adm = 5
		dip = 5
		mil = 2
		female = yes
    }
}

1756.1.1 = {
	monarch = {
 		name = "Vivonne"
		dynasty = "Daggerfall"
		birth_date = 1726.1.1
		adm = 4
		dip = 2
		mil = 3
		female = yes
    }
	queen = {
 		name = "Sosil"
		dynasty = "Ashcroft"
		birth_date = 1719.1.1
		adm = 4
		dip = 5
		mil = 4
    }
	heir = {
 		name = "Helainie"
		monarch_name = "Helainie I"
		dynasty = "Daggerfall"
		birth_date = 1753.1.1
		death_date = 1815.1.1
		claim = 100
		adm = 1
		dip = 4
		mil = 3
		female = yes
    }
}

1815.1.1 = {
	monarch = {
 		name = "Wilkes"
		dynasty = "Daggerfall"
		birth_date = 1794.1.1
		adm = 1
		dip = 0
		mil = 3
    }
}

1859.1.1 = {
	monarch = {
 		name = "Nanoyra"
		dynasty = "Daggerfall"
		birth_date = 1818.1.1
		adm = 6
		dip = 6
		mil = 0
		female = yes
    }
	queen = {
 		name = "Bastibien"
		dynasty = "Sorick"
		birth_date = 1838.1.1
		adm = 3
		dip = 5
		mil = 6
    }
	heir = {
 		name = "Menoit"
		monarch_name = "Menoit I"
		dynasty = "Daggerfall"
		birth_date = 1852.1.1
		death_date = 1932.1.1
		claim = 100
		adm = 0
		dip = 3
		mil = 4
    }
}

1932.1.1 = {
	monarch = {
 		name = "Daimbert"
		dynasty = "Daggerfall"
		birth_date = 1903.1.1
		adm = 2
		dip = 5
		mil = 0
    }
}

1981.1.1 = {
	monarch = {
 		name = "Malyna"
		dynasty = "Daggerfall"
		birth_date = 1942.1.1
		adm = 2
		dip = 1
		mil = 0
		female = yes
    }
	queen = {
 		name = "Sylvain"
		dynasty = "Masoriane"
		birth_date = 1942.1.1
		adm = 5
		dip = 2
		mil = 2
    }
	heir = {
 		name = "Tom"
		monarch_name = "Tom I"
		dynasty = "Daggerfall"
		birth_date = 1966.1.1
		death_date = 2018.1.1
		claim = 100
		adm = 1
		dip = 1
		mil = 1
    }
}

2018.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Rogeraud"
		monarch_name = "Rogeraud I"
		dynasty = "Daggerfall"
		birth_date = 2015.1.1
		death_date = 2033.1.1
		claim = 100
		adm = 1
		dip = 4
		mil = 2
    }
}

2033.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Labhraidh"
		monarch_name = "Labhraidh I"
		dynasty = "Daggerfall"
		birth_date = 2032.1.1
		death_date = 2050.1.1
		claim = 100
		adm = 6
		dip = 4
		mil = 5
    }
}

2050.1.1 = {
	monarch = {
 		name = "Bereditte"
		dynasty = "Daggerfall"
		birth_date = 1998.1.1
		adm = 2
		dip = 5
		mil = 1
    }
}

2085.1.1 = {
	monarch = {
 		name = "Sunel"
		dynasty = "Daggerfall"
		birth_date = 2052.1.1
		adm = 0
		dip = 4
		mil = 4
    }
	queen = {
 		name = "Raelyn"
		dynasty = "Volcy"
		birth_date = 2034.1.1
		adm = 4
		dip = 3
		mil = 3
		female = yes
    }
	heir = {
 		name = "Ronyssa"
		monarch_name = "Ronyssa I"
		dynasty = "Daggerfall"
		birth_date = 2073.1.1
		death_date = 2121.1.1
		claim = 100
		adm = 1
		dip = 1
		mil = 2
		female = yes
    }
}

2121.1.1 = {
	monarch = {
 		name = "Frostien"
		dynasty = "Daggerfall"
		birth_date = 2093.1.1
		adm = 0
		dip = 5
		mil = 3
    }
}

2175.1.1 = {
	monarch = {
 		name = "Benoit"
		dynasty = "Daggerfall"
		birth_date = 2124.1.1
		adm = 5
		dip = 4
		mil = 6
    }
	queen = {
 		name = "Amora"
		dynasty = "Levys"
		birth_date = 2140.1.1
		adm = 2
		dip = 2
		mil = 5
		female = yes
    }
	heir = {
 		name = "Leobert"
		monarch_name = "Leobert I"
		dynasty = "Daggerfall"
		birth_date = 2174.1.1
		death_date = 2245.1.1
		claim = 100
		adm = 3
		dip = 6
		mil = 6
    }
}

2245.1.1 = {
	monarch = {
 		name = "Breywenne"
		dynasty = "Daggerfall"
		birth_date = 2225.1.1
		adm = 2
		dip = 2
		mil = 6
		female = yes
    }
	queen = {
 		name = "Jimy"
		dynasty = "Varin"
		birth_date = 2211.1.1
		adm = 6
		dip = 1
		mil = 5
    }
	heir = {
 		name = "Audric"
		monarch_name = "Audric I"
		dynasty = "Daggerfall"
		birth_date = 2231.1.1
		death_date = 2309.1.1
		claim = 100
		adm = 3
		dip = 6
		mil = 4
    }
}

2309.1.1 = {
	monarch = {
 		name = "Marrec"
		dynasty = "Daggerfall"
		birth_date = 2273.1.1
		adm = 5
		dip = 1
		mil = 0
    }
	queen = {
 		name = "Amelia"
		dynasty = "Dalomax"
		birth_date = 2256.1.1
		adm = 2
		dip = 6
		mil = 6
		female = yes
    }
	heir = {
 		name = "Lysaux"
		monarch_name = "Lysaux I"
		dynasty = "Daggerfall"
		birth_date = 2300.1.1
		death_date = 2350.1.1
		claim = 100
		adm = 6
		dip = 5
		mil = 5
		female = yes
    }
}

2350.1.1 = {
	monarch = {
 		name = "Blithe"
		dynasty = "Daggerfall"
		birth_date = 2305.1.1
		adm = 2
		dip = 6
		mil = 0
    }
	queen = {
 		name = "Annalysse"
		dynasty = "Simiseph"
		birth_date = 2301.1.1
		adm = 6
		dip = 5
		mil = 6
		female = yes
    }
	heir = {
 		name = "Loic"
		monarch_name = "Loic I"
		dynasty = "Daggerfall"
		birth_date = 2350.1.1
		death_date = 2390.1.1
		claim = 100
		adm = 3
		dip = 3
		mil = 5
    }
}

2390.1.1 = {
	monarch = {
 		name = "Marnest"
		dynasty = "Daggerfall"
		birth_date = 2337.1.1
		adm = 2
		dip = 6
		mil = 5
    }
}

2481.1.1 = {
	monarch = {
 		name = "Tom"
		dynasty = "Daggerfall"
		birth_date = 2453.1.1
		adm = 0
		dip = 6
		mil = 2
    }
}

