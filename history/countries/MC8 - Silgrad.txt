government = republic
government_rank = 3
mercantilism = 1
technology_group = dwemer_tg
religion = reason_and_logic_cult
primary_culture = dwemer
capital = 3365

54.1.1 = {
	monarch = {
 		name = "Somzlin"
		dynasty = "Az'Jozlen"
		birth_date = 36.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

141.1.1 = {
	monarch = {
 		name = "Tugradac"
		dynasty = "Av'Ishevnorz"
		birth_date = 104.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

223.1.1 = {
	monarch = {
 		name = "Ishevnorz"
		dynasty = "Aq'Krilnif"
		birth_date = 175.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

268.1.1 = {
	monarch = {
 		name = "Mlirloar"
		dynasty = "Az'Doudrys"
		birth_date = 237.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

365.1.1 = {
	monarch = {
 		name = "Nhezril"
		dynasty = "Av'Mzaglan"
		birth_date = 341.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

439.1.1 = {
	monarch = {
 		name = "Jrudit"
		dynasty = "Az'Mrobwarn"
		birth_date = 408.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

480.1.1 = {
	monarch = {
 		name = "Jlarerhunch"
		dynasty = "Af'Klalzrak"
		birth_date = 458.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

575.1.1 = {
	monarch = {
 		name = "Mlirloar"
		dynasty = "Av'Talchanf"
		birth_date = 526.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

620.1.1 = {
	monarch = {
 		name = "Irhadac"
		dynasty = "Af'Rafk"
		birth_date = 571.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

683.1.1 = {
	monarch = {
 		name = "Izvulzarf"
		dynasty = "Az'Krevzyrn"
		birth_date = 647.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

734.1.1 = {
	monarch = {
 		name = "Yzranrida"
		dynasty = "Aq'Grigarn"
		birth_date = 689.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

786.1.1 = {
	monarch = {
 		name = "Kolzarf"
		dynasty = "Aq'Mrotchatz"
		birth_date = 740.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

860.1.1 = {
	monarch = {
 		name = "Mravlara"
		dynasty = "Aq'Rlovrin"
		birth_date = 808.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

901.1.1 = {
	monarch = {
 		name = "Choalchanf"
		dynasty = "Az'Yabaln"
		birth_date = 853.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

956.1.1 = {
	monarch = {
 		name = "Goundam"
		dynasty = "Az'Ruerbira"
		birth_date = 904.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

994.1.1 = {
	monarch = {
 		name = "Nhetchatz"
		dynasty = "Aq'Nromgunch"
		birth_date = 956.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1064.1.1 = {
	monarch = {
 		name = "Ghafuan"
		dynasty = "Aq'Cfradrys"
		birth_date = 1036.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1156.1.1 = {
	monarch = {
 		name = "Tahron"
		dynasty = "Az'Chzebchasz"
		birth_date = 1104.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1220.1.1 = {
	monarch = {
 		name = "Bzrazgar"
		dynasty = "Az'Dzreglan"
		birth_date = 1186.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1317.1.1 = {
	monarch = {
 		name = "Nhetchatz"
		dynasty = "Av'Sthomin"
		birth_date = 1291.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1358.1.1 = {
	monarch = {
 		name = "Ghafuan"
		dynasty = "Af'Rhzorhunch"
		birth_date = 1316.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1433.1.1 = {
	monarch = {
 		name = "Azsanwess"
		dynasty = "Az'Choahretz"
		birth_date = 1401.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1529.1.1 = {
	monarch = {
 		name = "Bzrazgar"
		dynasty = "Aq'Byrahken"
		birth_date = 1510.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1568.1.1 = {
	monarch = {
 		name = "Jholzarf"
		dynasty = "Af'Irhadac"
		birth_date = 1545.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1638.1.1 = {
	monarch = {
 		name = "Ararbira"
		dynasty = "Av'Jlarerhunch"
		birth_date = 1603.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1685.1.1 = {
	monarch = {
 		name = "Mhanrazg"
		dynasty = "Az'Nhetchatz"
		birth_date = 1644.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1738.1.1 = {
	monarch = {
 		name = "Jhourlac"
		dynasty = "Aq'Rlovrin"
		birth_date = 1701.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1824.1.1 = {
	monarch = {
 		name = "Jholzarf"
		dynasty = "Az'Izvuvretch"
		birth_date = 1799.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1878.1.1 = {
	monarch = {
 		name = "Ychogrenz"
		dynasty = "Az'Jravarn"
		birth_date = 1844.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1952.1.1 = {
	monarch = {
 		name = "Rhzomrond"
		dynasty = "Az'Jribwyr"
		birth_date = 1923.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1987.1.1 = {
	monarch = {
 		name = "Ithorves"
		dynasty = "Af'Dzredras"
		birth_date = 1956.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

2046.1.1 = {
	monarch = {
 		name = "Tnadrak"
		dynasty = "Az'Aknanwess"
		birth_date = 2009.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2086.1.1 = {
	monarch = {
 		name = "Tzenruz"
		dynasty = "Az'Krincha"
		birth_date = 2034.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2178.1.1 = {
	monarch = {
 		name = "Achygrac"
		dynasty = "Av'Tarlatz"
		birth_date = 2137.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2256.1.1 = {
	monarch = {
 		name = "Mzaglan"
		dynasty = "Af'Jlarenzgar"
		birth_date = 2236.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2337.1.1 = {
	monarch = {
 		name = "Ylrefwinn"
		dynasty = "Aq'Nromgunch"
		birth_date = 2302.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

2412.1.1 = {
	monarch = {
 		name = "Gzonrynn"
		dynasty = "Aq'Chzegrenz"
		birth_date = 2381.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

2450.1.1 = {
	monarch = {
 		name = "Cfradrys"
		dynasty = "Aq'Jhonzcharn"
		birth_date = 2410.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

2488.1.1 = {
	monarch = {
 		name = "Mzaglan"
		dynasty = "Af'Ynzatarn"
		birth_date = 2437.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

