government = republic
add_government_reform = pirate_republic_reform
government_rank = 1
mercantilism = 1
technology_group = marshan_tg
religion = clavicus_vile_cult
primary_culture = agaceph
capital = 6485

54.1.1 = {
	monarch = {
 		name = "Ereel-Sa"
		dynasty = "Derkeeja"
		birth_date = 27.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

105.1.1 = {
	monarch = {
 		name = "Argar"
		dynasty = "Jeetum-Lei"
		birth_date = 58.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

155.1.1 = {
	monarch = {
 		name = "Tana-Teeus"
		dynasty = "Meerhaj"
		birth_date = 122.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

204.1.1 = {
	monarch = {
 		name = "Miharil"
		dynasty = "Nileesh"
		birth_date = 173.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

291.1.1 = {
	monarch = {
 		name = "Haxara"
		dynasty = "Xeirtius"
		birth_date = 257.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

373.1.1 = {
	monarch = {
 		name = "Asheemar"
		dynasty = "Ushureel"
		birth_date = 347.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

431.1.1 = {
	monarch = {
 		name = "Sureeus"
		dynasty = "Xemclesh"
		birth_date = 395.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

491.1.1 = {
	monarch = {
 		name = "Murrax"
		dynasty = "Canimesh"
		birth_date = 442.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

589.1.1 = {
	monarch = {
 		name = "Teeba-Makka"
		dynasty = "Theerdaresh"
		birth_date = 547.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

671.1.1 = {
	monarch = {
 		name = "Muranatepa"
		dynasty = "Caligeepa"
		birth_date = 643.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

726.1.1 = {
	monarch = {
 		name = "Hiis-Ja"
		dynasty = "Saliz'k"
		birth_date = 691.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

805.1.1 = {
	monarch = {
 		name = "Azbai-Meenus"
		dynasty = "Mahekus"
		birth_date = 753.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

848.1.1 = {
	monarch = {
 		name = "Teeka"
		dynasty = "Tikeerdeseer"
		birth_date = 819.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

887.1.1 = {
	monarch = {
 		name = "Narnum"
		dynasty = "Nileesh"
		birth_date = 857.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

972.1.1 = {
	monarch = {
 		name = "Gatulm"
		dynasty = "Madeian"
		birth_date = 930.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1018.1.1 = {
	monarch = {
 		name = "Bahrez"
		dynasty = "Andreecalees"
		birth_date = 998.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1108.1.1 = {
	monarch = {
 		name = "Gom-Tulm"
		dynasty = "Peteus"
		birth_date = 1058.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1178.1.1 = {
	monarch = {
 		name = "Beel-Ranu"
		dynasty = "Sakeides"
		birth_date = 1154.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1248.1.1 = {
	monarch = {
 		name = "Topeeth-Gih"
		dynasty = "Caseus"
		birth_date = 1205.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1330.1.1 = {
	monarch = {
 		name = "Neeti-Gai"
		dynasty = "Miloeeja"
		birth_date = 1291.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1417.1.1 = {
	monarch = {
 		name = "Gjomee-Loh"
		dynasty = "Tikeerdeseer"
		birth_date = 1378.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1502.1.1 = {
	monarch = {
 		name = "Bahrei"
		dynasty = "Endoredeseer"
		birth_date = 1454.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1574.1.1 = {
	monarch = {
 		name = "Tlosee"
		dynasty = "Androteus"
		birth_date = 1525.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1657.1.1 = {
	monarch = {
 		name = "Neer"
		dynasty = "Caseus"
		birth_date = 1634.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1731.1.1 = {
	monarch = {
 		name = "Tsleitzei"
		dynasty = "Sakeides"
		birth_date = 1687.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1784.1.1 = {
	monarch = {
 		name = "Nema-Pachat"
		dynasty = "An-Wulm"
		birth_date = 1763.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1829.1.1 = {
	monarch = {
 		name = "Jasaii"
		dynasty = "Caligeepa"
		birth_date = 1793.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1928.1.1 = {
	monarch = {
 		name = "Betzi"
		dynasty = "Yelnneen"
		birth_date = 1894.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1983.1.1 = {
	monarch = {
 		name = "Tseedasi"
		dynasty = "Menes"
		birth_date = 1946.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

2069.1.1 = {
	monarch = {
 		name = "Neexi"
		dynasty = "Theodesh"
		birth_date = 2017.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2113.1.1 = {
	monarch = {
 		name = "Haj-Ru"
		dynasty = "Casdorus"
		birth_date = 2073.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2157.1.1 = {
	monarch = {
 		name = "Batar-Meej"
		dynasty = "Effe-Shei"
		birth_date = 2124.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2221.1.1 = {
	monarch = {
 		name = "Hixeeh-Raj"
		dynasty = "Derkeeja"
		birth_date = 2187.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2299.1.1 = {
	monarch = {
 		name = "Bewlus"
		dynasty = "Antigoneeixth"
		birth_date = 2248.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2348.1.1 = {
	monarch = {
 		name = "Uazee"
		dynasty = "Effe-Shei"
		birth_date = 2309.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

2390.1.1 = {
	monarch = {
 		name = "Nukorash"
		dynasty = "Xeirtius"
		birth_date = 2352.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

2445.1.1 = {
	monarch = {
 		name = "Heir-Marza"
		dynasty = "Miloeeja"
		birth_date = 2411.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2495.1.1 = {
	monarch = {
 		name = "Beshnus"
		dynasty = "Tikeerdeseer"
		birth_date = 2450.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

