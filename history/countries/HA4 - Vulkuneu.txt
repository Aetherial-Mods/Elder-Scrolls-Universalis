government = monarchy
government_rank = 1
mercantilism = 1
technology_group = yokudan_tg
religion = redguard_pantheon
primary_culture = redguard
capital = 6171

54.1.1 = {
	monarch = {
 		name = "Sticey"
		dynasty = "Nathe"
		birth_date = 16.1.1
		adm = 0
		dip = 3
		mil = 6
    }
}

143.1.1 = {
	monarch = {
 		name = "Monsey"
		dynasty = "Dinok"
		birth_date = 113.1.1
		adm = 5
		dip = 2
		mil = 2
		female = yes
    }
	queen = {
 		name = "Lathahim"
		dynasty = "Dinok"
		birth_date = 90.1.1
		adm = 2
		dip = 1
		mil = 1
    }
}

184.1.1 = {
	monarch = {
 		name = "Trithik"
		dynasty = "M'efylur"
		birth_date = 144.1.1
		adm = 0
		dip = 0
		mil = 5
    }
	queen = {
 		name = "Salma"
		dynasty = "M'efylur"
		birth_date = 146.1.1
		adm = 4
		dip = 5
		mil = 4
		female = yes
    }
}

236.1.1 = {
	monarch = {
 		name = "Ehwat"
		dynasty = "Ahtaelan"
		birth_date = 198.1.1
		adm = 2
		dip = 5
		mil = 1
    }
}

298.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Orinthal"
		monarch_name = "Orinthal I"
		dynasty = "Curtyvond"
		birth_date = 297.1.1
		death_date = 315.1.1
		claim = 100
		adm = 3
		dip = 2
		mil = 6
    }
}

315.1.1 = {
	monarch = {
 		name = "Perridah"
		dynasty = "M'efylur"
		birth_date = 276.1.1
		adm = 2
		dip = 5
		mil = 6
		female = yes
    }
}

370.1.1 = {
	monarch = {
 		name = "Zafrik"
		dynasty = "Greelan"
		birth_date = 317.1.1
		adm = 1
		dip = 4
		mil = 3
    }
}

461.1.1 = {
	monarch = {
 		name = "Rada"
		dynasty = "Sticert"
		birth_date = 429.1.1
		adm = 6
		dip = 3
		mil = 6
    }
}

520.1.1 = {
	monarch = {
 		name = "Khadzalar"
		dynasty = "Jaganuin"
		birth_date = 486.1.1
		adm = 4
		dip = 3
		mil = 3
    }
	queen = {
 		name = "Shelmar"
		dynasty = "Jaganuin"
		birth_date = 489.1.1
		adm = 1
		dip = 1
		mil = 2
		female = yes
    }
	heir = {
 		name = "Kahaba"
		monarch_name = "Kahaba I"
		dynasty = "Jaganuin"
		birth_date = 512.1.1
		death_date = 593.1.1
		claim = 100
		adm = 5
		dip = 0
		mil = 1
		female = yes
    }
}

593.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Darius"
		monarch_name = "Darius I"
		dynasty = "Jartnado"
		birth_date = 579.1.1
		death_date = 597.1.1
		claim = 100
		adm = 3
		dip = 6
		mil = 4
    }
}

597.1.1 = {
	monarch = {
 		name = "Raand"
		dynasty = "Fairgki"
		birth_date = 578.1.1
		adm = 6
		dip = 0
		mil = 0
    }
	queen = {
 		name = "Namsah"
		dynasty = "Fairgki"
		birth_date = 562.1.1
		adm = 3
		dip = 6
		mil = 6
		female = yes
    }
	heir = {
 		name = "Domalen"
		monarch_name = "Domalen I"
		dynasty = "Fairgki"
		birth_date = 591.1.1
		death_date = 660.1.1
		claim = 100
		adm = 0
		dip = 4
		mil = 5
    }
}

660.1.1 = {
	monarch = {
 		name = "Erdelan"
		dynasty = "Chestem"
		birth_date = 608.1.1
		adm = 6
		dip = 1
		mil = 5
    }
}

701.1.1 = {
	monarch = {
 		name = "Khubzyat"
		dynasty = "Sosccean"
		birth_date = 667.1.1
		adm = 4
		dip = 0
		mil = 2
    }
	queen = {
 		name = "Sudi"
		dynasty = "Sosccean"
		birth_date = 681.1.1
		adm = 1
		dip = 5
		mil = 1
		female = yes
    }
	heir = {
 		name = "Kalorter"
		monarch_name = "Kalorter I"
		dynasty = "Sosccean"
		birth_date = 700.1.1
		death_date = 777.1.1
		claim = 100
		adm = 5
		dip = 4
		mil = 0
    }
}

777.1.1 = {
	monarch = {
 		name = "Zartosht"
		dynasty = "Erneni"
		birth_date = 732.1.1
		adm = 1
		dip = 5
		mil = 2
    }
}

816.1.1 = {
	monarch = {
 		name = "Rama"
		dynasty = "Jelen"
		birth_date = 787.1.1
		adm = 6
		dip = 5
		mil = 6
    }
}

883.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Pawalar"
		monarch_name = "Pawalar I"
		dynasty = "Provmin"
		birth_date = 881.1.1
		death_date = 899.1.1
		claim = 100
		adm = 0
		dip = 2
		mil = 3
    }
}

899.1.1 = {
	monarch = {
 		name = "Farasad"
		dynasty = "Aviell"
		birth_date = 873.1.1
		adm = 2
		dip = 3
		mil = 6
    }
}

961.1.1 = {
	monarch = {
 		name = "Zamnell"
		dynasty = "Kithithon"
		birth_date = 922.1.1
		adm = 4
		dip = 4
		mil = 0
    }
	queen = {
 		name = "Tannida"
		dynasty = "Kithithon"
		birth_date = 920.1.1
		adm = 5
		dip = 1
		mil = 1
		female = yes
    }
	heir = {
 		name = "Khojifa"
		monarch_name = "Khojifa I"
		dynasty = "Kithithon"
		birth_date = 956.1.1
		death_date = 1012.1.1
		claim = 100
		adm = 1
		dip = 6
		mil = 0
		female = yes
    }
}

1012.1.1 = {
	monarch = {
 		name = "Abadarun"
		dynasty = "Sticert"
		birth_date = 974.1.1
		adm = 1
		dip = 3
		mil = 1
    }
}

1069.1.1 = {
	monarch = {
 		name = "Rhina"
		dynasty = "Owytis"
		birth_date = 1050.1.1
		adm = 6
		dip = 2
		mil = 4
		female = yes
    }
}

1106.1.1 = {
	monarch = {
 		name = "Lihidar"
		dynasty = "Rinek"
		birth_date = 1057.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1174.1.1 = {
	monarch = {
 		name = "Firas"
		dynasty = "Rhajtha"
		birth_date = 1126.1.1
		adm = 3
		dip = 0
		mil = 5
    }
}

1273.1.1 = {
	monarch = {
 		name = "Nazz"
		dynasty = "B'ollka"
		birth_date = 1247.1.1
		adm = 1
		dip = 6
		mil = 1
    }
}

1341.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Usraya"
		monarch_name = "Usraya I"
		dynasty = "Jaganuin"
		birth_date = 1337.1.1
		death_date = 1355.1.1
		claim = 100
		adm = 2
		dip = 4
		mil = 6
		female = yes
    }
}

1355.1.1 = {
	monarch = {
 		name = "Kourazir"
		dynasty = "Dhosher"
		birth_date = 1308.1.1
		adm = 1
		dip = 0
		mil = 6
    }
	queen = {
 		name = "Junah"
		dynasty = "Dhosher"
		birth_date = 1304.1.1
		adm = 1
		dip = 3
		mil = 1
		female = yes
    }
	heir = {
 		name = "Youss"
		monarch_name = "Youss I"
		dynasty = "Dhosher"
		birth_date = 1353.1.1
		death_date = 1413.1.1
		claim = 100
		adm = 5
		dip = 2
		mil = 6
    }
}

1413.1.1 = {
	monarch = {
 		name = "Maereeda"
		dynasty = "Nathe"
		birth_date = 1384.1.1
		adm = 4
		dip = 5
		mil = 0
		female = yes
    }
}

1501.1.1 = {
	monarch = {
 		name = "Gianon"
		dynasty = "Shelamon"
		birth_date = 1467.1.1
		adm = 3
		dip = 5
		mil = 3
		female = yes
    }
}

1540.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Enneh"
		monarch_name = "Enneh I"
		dynasty = "Faliaron"
		birth_date = 1539.1.1
		death_date = 1557.1.1
		claim = 100
		adm = 3
		dip = 2
		mil = 1
    }
}

1557.1.1 = {
	monarch = {
 		name = "Rissinia"
		dynasty = "Cilp'kern"
		birth_date = 1526.1.1
		adm = 6
		dip = 3
		mil = 4
    }
}

1595.1.1 = {
	monarch = {
 		name = "Lewand"
		dynasty = "Boistim"
		birth_date = 1575.1.1
		adm = 4
		dip = 2
		mil = 0
    }
}

1661.1.1 = {
	monarch = {
 		name = "Ghaninad"
		dynasty = "Rusnnison"
		birth_date = 1610.1.1
		adm = 3
		dip = 1
		mil = 4
    }
}

1724.1.1 = {
	monarch = {
 		name = "Abri"
		dynasty = "Jaganuin"
		birth_date = 1701.1.1
		adm = 5
		dip = 3
		mil = 6
    }
	queen = {
 		name = "Toubeh"
		dynasty = "Jaganuin"
		birth_date = 1685.1.1
		adm = 5
		dip = 6
		mil = 0
		female = yes
    }
}

1793.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Paldezh"
		monarch_name = "Paldezh I"
		dynasty = "Curorter"
		birth_date = 1789.1.1
		death_date = 1807.1.1
		claim = 100
		adm = 0
		dip = 0
		mil = 1
    }
}

1807.1.1 = {
	monarch = {
 		name = "Fahrzuzh"
		dynasty = "Ghuldka"
		birth_date = 1765.1.1
		adm = 2
		dip = 2
		mil = 3
    }
}

1891.1.1 = {
	monarch = {
 		name = "Zakriah"
		dynasty = "Torlhael"
		birth_date = 1845.1.1
		adm = 1
		dip = 1
		mil = 0
		female = yes
    }
	queen = {
 		name = "Islaf"
		dynasty = "Torlhael"
		birth_date = 1842.1.1
		adm = 5
		dip = 6
		mil = 6
    }
	heir = {
 		name = "Vhos"
		monarch_name = "Vhos I"
		dynasty = "Torlhael"
		birth_date = 1890.1.1
		death_date = 1971.1.1
		claim = 100
		adm = 1
		dip = 5
		mil = 5
    }
}

1971.1.1 = {
	monarch = {
 		name = "Khavid"
		dynasty = "Dudlim"
		birth_date = 1942.1.1
		adm = 4
		dip = 6
		mil = 0
    }
}

2017.1.1 = {
	monarch = {
 		name = "Fahara'jad"
		dynasty = "Sticert"
		birth_date = 1973.1.1
		adm = 6
		dip = 1
		mil = 2
    }
	queen = {
 		name = "Clelian"
		dynasty = "Sticert"
		birth_date = 1983.1.1
		adm = 6
		dip = 4
		mil = 3
		female = yes
    }
	heir = {
 		name = "Parvaneh"
		monarch_name = "Parvaneh I"
		dynasty = "Sticert"
		birth_date = 2017.1.1
		death_date = 2093.1.1
		claim = 100
		adm = 3
		dip = 3
		mil = 2
		female = yes
    }
}

2093.1.1 = {
	monarch = {
 		name = "Fayna"
		dynasty = "Dudldr"
		birth_date = 2060.1.1
		adm = 3
		dip = 6
		mil = 2
		female = yes
    }
	queen = {
 		name = "Nazeem"
		dynasty = "Dudldr"
		birth_date = 2052.1.1
		adm = 6
		dip = 5
		mil = 1
    }
	heir = {
 		name = "Dudley"
		monarch_name = "Dudley I"
		dynasty = "Dudldr"
		birth_date = 2090.1.1
		death_date = 2146.1.1
		claim = 100
		adm = 3
		dip = 3
		mil = 0
    }
}

2146.1.1 = {
	monarch = {
 		name = "Razhab"
		dynasty = "Irgher"
		birth_date = 2107.1.1
		adm = 6
		dip = 4
		mil = 3
    }
}

2196.1.1 = {
	monarch = {
 		name = "Lette"
		dynasty = "Lakir"
		birth_date = 2178.1.1
		adm = 4
		dip = 4
		mil = 6
		female = yes
    }
}

2278.1.1 = {
	monarch = {
 		name = "Favia"
		dynasty = "Kewnado"
		birth_date = 2226.1.1
		adm = 3
		dip = 3
		mil = 3
		female = yes
    }
}

2372.1.1 = {
	monarch = {
 		name = "Zimaneh"
		dynasty = "Jinrgel"
		birth_date = 2346.1.1
		adm = 1
		dip = 2
		mil = 0
		female = yes
    }
}

2441.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Kaye"
		monarch_name = "Kaye I"
		dynasty = "Jateif"
		birth_date = 2433.1.1
		death_date = 2451.1.1
		claim = 100
		adm = 2
		dip = 6
		mil = 4
    }
}

2451.1.1 = {
	monarch = {
 		name = "Kinther"
		dynasty = "Athobur"
		birth_date = 2408.1.1
		adm = 1
		dip = 3
		mil = 5
    }
}

