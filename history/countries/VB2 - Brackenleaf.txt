government = theocracy
government_rank = 1
mercantilism = 1
technology_group = elven_tg
religion = bosmer_pantheon
primary_culture = bosmer
capital = 5101

54.1.1 = {
	monarch = {
 		name = "Orcholin"
		dynasty = "Ca'Hasaael"
		birth_date = 17.1.1
		adm = 0
		dip = 4
		mil = 6
    }
}

145.1.1 = {
	monarch = {
 		name = "Egenor"
		dynasty = "Or'Velyn"
		birth_date = 93.1.1
		adm = 6
		dip = 1
		mil = 6
    }
}

236.1.1 = {
	monarch = {
 		name = "Vole"
		dynasty = "Or'Parndra"
		birth_date = 213.1.1
		adm = 5
		dip = 0
		mil = 3
    }
}

300.1.1 = {
	monarch = {
 		name = "Elethir"
		dynasty = "Ca'Manthia"
		birth_date = 260.1.1
		adm = 3
		dip = 6
		mil = 6
    }
}

364.1.1 = {
	monarch = {
 		name = "Adwihill"
		dynasty = "Ur'Eldenroot"
		birth_date = 326.1.1
		adm = 1
		dip = 5
		mil = 3
		female = yes
    }
}

444.1.1 = {
	monarch = {
 		name = "Nienras"
		dynasty = "Or'Telarwen"
		birth_date = 413.1.1
		adm = 6
		dip = 5
		mil = 6
		female = yes
    }
}

506.1.1 = {
	monarch = {
 		name = "Godadras"
		dynasty = "Ur'Applegrass"
		birth_date = 457.1.1
		adm = 4
		dip = 4
		mil = 3
    }
}

557.1.1 = {
	monarch = {
 		name = "Elruin"
		dynasty = "Ur'Donae"
		birth_date = 534.1.1
		adm = 3
		dip = 3
		mil = 0
		female = yes
    }
}

605.1.1 = {
	monarch = {
 		name = "Nedras"
		dynasty = "Or'Southpoint"
		birth_date = 554.1.1
		adm = 1
		dip = 2
		mil = 3
    }
}

659.1.1 = {
	monarch = {
 		name = "Modradil"
		dynasty = "Ca'Meneia"
		birth_date = 609.1.1
		adm = 3
		dip = 3
		mil = 5
    }
}

701.1.1 = {
	monarch = {
 		name = "Gloron"
		dynasty = "Ca'Falinesti"
		birth_date = 681.1.1
		adm = 1
		dip = 3
		mil = 2
    }
}

759.1.1 = {
	monarch = {
 		name = "Niminthil"
		dynasty = "Ur'Eledan"
		birth_date = 718.1.1
		adm = 5
		dip = 5
		mil = 2
		female = yes
    }
}

828.1.1 = {
	monarch = {
 		name = "Farthalem"
		dynasty = "Ca'Lichenrock"
		birth_date = 785.1.1
		adm = 0
		dip = 6
		mil = 3
    }
}

881.1.1 = {
	monarch = {
 		name = "Brimlirdor"
		dynasty = "Or'Telael"
		birth_date = 842.1.1
		adm = 5
		dip = 5
		mil = 0
    }
}

964.1.1 = {
	monarch = {
 		name = "Firchel"
		dynasty = "Ur'Ardndra"
		birth_date = 940.1.1
		adm = 4
		dip = 4
		mil = 4
		female = yes
    }
}

1025.1.1 = {
	monarch = {
 		name = "Cayliss"
		dynasty = "Or'Mossmire"
		birth_date = 984.1.1
		adm = 2
		dip = 4
		mil = 0
		female = yes
    }
}

1094.1.1 = {
	monarch = {
 		name = "Ranedor"
		dynasty = "Or'Telarwen"
		birth_date = 1057.1.1
		adm = 0
		dip = 3
		mil = 4
    }
}

1178.1.1 = {
	monarch = {
 		name = "Ithgagon"
		dynasty = "Ur'Eldenroot"
		birth_date = 1144.1.1
		adm = 5
		dip = 2
		mil = 0
    }
}

1235.1.1 = {
	monarch = {
 		name = "Fildir"
		dynasty = "Ca'Glien"
		birth_date = 1212.1.1
		adm = 4
		dip = 1
		mil = 4
    }
}

1325.1.1 = {
	monarch = {
 		name = "Calabenn"
		dynasty = "		"
		birth_date = 1296.1.1
		adm = 2
		dip = 0
		mil = 1
    }
}

1382.1.1 = {
	monarch = {
 		name = "Ralds"
		dynasty = "Ur'Eldenroot"
		birth_date = 1348.1.1
		adm = 4
		dip = 2
		mil = 2
    }
}

1458.1.1 = {
	monarch = {
 		name = "Irielis"
		dynasty = "Or'Silvenar"
		birth_date = 1431.1.1
		adm = 2
		dip = 1
		mil = 6
    }
}

1493.1.1 = {
	monarch = {
 		name = "Sendranir"
		dynasty = "Ur'Borondrae"
		birth_date = 1469.1.1
		adm = 0
		dip = 0
		mil = 3
		female = yes
    }
}

1585.1.1 = {
	monarch = {
 		name = "Lenahawn"
		dynasty = "Ca'Hasaael"
		birth_date = 1567.1.1
		adm = 5
		dip = 6
		mil = 6
    }
}

1674.1.1 = {
	monarch = {
 		name = "Fintholor"
		dynasty = "Ca'Ivybranch"
		birth_date = 1631.1.1
		adm = 4
		dip = 5
		mil = 3
    }
}

1720.1.1 = {
	monarch = {
 		name = "Cingunor"
		dynasty = "Or'Silvenar"
		birth_date = 1673.1.1
		adm = 2
		dip = 5
		mil = 0
    }
}

1784.1.1 = {
	monarch = {
 		name = "Selynia"
		dynasty = "Ur'Eledan"
		birth_date = 1736.1.1
		adm = 0
		dip = 4
		mil = 3
		female = yes
    }
}

1850.1.1 = {
	monarch = {
 		name = "Legodir"
		dynasty = "Or'Naerilin"
		birth_date = 1800.1.1
		adm = 5
		dip = 3
		mil = 0
    }
}

1893.1.1 = {
	monarch = {
 		name = "Finnalir"
		dynasty = "Or'Rosebranch"
		birth_date = 1840.1.1
		adm = 0
		dip = 4
		mil = 1
    }
}

1976.1.1 = {
	monarch = {
 		name = "Cerwaenor"
		dynasty = "Or'Vulkwasten"
		birth_date = 1931.1.1
		adm = 4
		dip = 6
		mil = 1
		female = yes
    }
}

2053.1.1 = {
	monarch = {
 		name = "Eglabenn"
		dynasty = "Ca'Green"
		birth_date = 2017.1.1
		adm = 3
		dip = 6
		mil = 5
    }
}

2098.1.1 = {
	monarch = {
 		name = "Gwanur"
		dynasty = "Or'Run"
		birth_date = 2047.1.1
		adm = 5
		dip = 0
		mil = 0
    }
}

2161.1.1 = {
	monarch = {
 		name = "Narirasi"
		dynasty = "Ur'Applerun"
		birth_date = 2133.1.1
		adm = 3
		dip = 6
		mil = 3
		female = yes
    }
}

2214.1.1 = {
	monarch = {
 		name = "Agafos"
		dynasty = "Ca'Hynhruviel"
		birth_date = 2176.1.1
		adm = 6
		dip = 0
		mil = 6
		female = yes
    }
}

2282.1.1 = {
	monarch = {
 		name = "Galbenel"
		dynasty = "Ca'Hasaael"
		birth_date = 2257.1.1
		adm = 6
		dip = 4
		mil = 4
		female = yes
    }
}

2319.1.1 = {
	monarch = {
 		name = "Cunendor"
		dynasty = "Ur'Disticia"
		birth_date = 2281.1.1
		adm = 2
		dip = 6
		mil = 6
    }
}

2373.1.1 = {
	monarch = {
 		name = "Sylthendel"
		dynasty = "Ur'Archen"
		birth_date = 2332.1.1
		adm = 4
		dip = 0
		mil = 0
		female = yes
    }
}

2442.1.1 = {
	monarch = {
 		name = "Malareth"
		dynasty = "Ca'Lichenrock"
		birth_date = 2397.1.1
		adm = 1
		dip = 2
		mil = 0
		female = yes
    }
}

