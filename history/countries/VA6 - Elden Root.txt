government = theocracy
government_rank = 3
mercantilism = 1
technology_group = elven_tg
religion = bosmer_pantheon
primary_culture = bosmer
capital = 834
secondary_religion = hircine_cult

54.1.1 = {
	monarch = {
 		name = "Maenrin"
		dynasty = "Ur'Ebon"
		birth_date = 19.1.1
		adm = 6
		dip = 3
		mil = 5
    }
}

114.1.1 = {
	monarch = {
 		name = "Forinor"
		dynasty = "Or'Physandra"
		birth_date = 64.1.1
		adm = 4
		dip = 2
		mil = 2
    }
}

150.1.1 = {
	monarch = {
 		name = "Dathangil"
		dynasty = "Ca'Filen"
		birth_date = 101.1.1
		adm = 6
		dip = 4
		mil = 4
    }
}

224.1.1 = {
	monarch = {
 		name = "Saldir"
		dynasty = "Or'Wasten"
		birth_date = 178.1.1
		adm = 4
		dip = 3
		mil = 0
    }
}

317.1.1 = {
	monarch = {
 		name = "Dolirdor"
		dynasty = "Ur'Applerun"
		birth_date = 268.1.1
		adm = 2
		dip = 2
		mil = 4
		female = yes
    }
}

387.1.1 = {
	monarch = {
 		name = "Thollebros"
		dynasty = "Ur'Applepool"
		birth_date = 367.1.1
		adm = 1
		dip = 1
		mil = 0
    }
}

454.1.1 = {
	monarch = {
 		name = "Malvir"
		dynasty = "Ca'Lieval"
		birth_date = 433.1.1
		adm = 6
		dip = 0
		mil = 4
    }
}

517.1.1 = {
	monarch = {
 		name = "Gaerthgorn"
		dynasty = "Or'Wasten"
		birth_date = 495.1.1
		adm = 4
		dip = 0
		mil = 1
    }
}

602.1.1 = {
	monarch = {
 		name = "Dirdelas"
		dynasty = "Or'Seledra"
		birth_date = 558.1.1
		adm = 2
		dip = 6
		mil = 4
    }
}

695.1.1 = {
	monarch = {
 		name = "Thalienglas"
		dynasty = "Ur'Donraenriel"
		birth_date = 662.1.1
		adm = 1
		dip = 5
		mil = 1
		female = yes
    }
}

761.1.1 = {
	monarch = {
 		name = "Malionar"
		dynasty = "Or'Riverwood"
		birth_date = 734.1.1
		adm = 2
		dip = 6
		mil = 3
    }
}

851.1.1 = {
	monarch = {
 		name = "Gaenengeval"
		dynasty = "Ca'Marbruk"
		birth_date = 831.1.1
		adm = 1
		dip = 6
		mil = 6
    }
}

918.1.1 = {
	monarch = {
 		name = "Marilrin"
		dynasty = "Ur'Elrainthil"
		birth_date = 878.1.1
		adm = 6
		dip = 5
		mil = 3
    }
}

980.1.1 = {
	monarch = {
 		name = "Geldirel"
		dynasty = "Ur'Ardndra"
		birth_date = 930.1.1
		adm = 4
		dip = 4
		mil = 0
		female = yes
    }
}

1041.1.1 = {
	monarch = {
 		name = "Donnanel"
		dynasty = "		"
		birth_date = 1002.1.1
		adm = 2
		dip = 3
		mil = 3
		female = yes
    }
}

1079.1.1 = {
	monarch = {
 		name = "Thorinor"
		dynasty = "Ca'Glien"
		birth_date = 1059.1.1
		adm = 1
		dip = 2
		mil = 0
    }
}

1170.1.1 = {
	monarch = {
 		name = "Mindileth"
		dynasty = "Ur'Applerun"
		birth_date = 1144.1.1
		adm = 6
		dip = 2
		mil = 3
		female = yes
    }
}

1268.1.1 = {
	monarch = {
 		name = "Gathviel"
		dynasty = "Or'Tarlain"
		birth_date = 1224.1.1
		adm = 4
		dip = 1
		mil = 0
		female = yes
    }
}

1362.1.1 = {
	monarch = {
 		name = "Dorinlas"
		dynasty = "Ca'Elsyon"
		birth_date = 1318.1.1
		adm = 6
		dip = 2
		mil = 2
    }
}

1405.1.1 = {
	monarch = {
 		name = "Erothel"
		dynasty = "Ur'Bothenandriah"
		birth_date = 1359.1.1
		adm = 3
		dip = 4
		mil = 2
    }
}

1448.1.1 = {
	monarch = {
 		name = "Arganar"
		dynasty = "Ca'Lieval"
		birth_date = 1400.1.1
		adm = 1
		dip = 3
		mil = 5
    }
}

1522.1.1 = {
	monarch = {
 		name = "Ninglos"
		dynasty = "Ca'Minweneth"
		birth_date = 1483.1.1
		adm = 3
		dip = 4
		mil = 0
    }
}

1573.1.1 = {
	monarch = {
 		name = "Hadras"
		dynasty = "Ur'Eineneth"
		birth_date = 1533.1.1
		adm = 2
		dip = 4
		mil = 4
    }
}

1618.1.1 = {
	monarch = {
 		name = "Odrethin"
		dynasty = "Ca'Elsesse"
		birth_date = 1573.1.1
		adm = 0
		dip = 3
		mil = 0
    }
}

1674.1.1 = {
	monarch = {
 		name = "Haron"
		dynasty = "Ca'Estrin"
		birth_date = 1637.1.1
		adm = 5
		dip = 2
		mil = 4
    }
}

1748.1.1 = {
	monarch = {
 		name = "Ethodan"
		dynasty = "Ur'Archen"
		birth_date = 1721.1.1
		adm = 3
		dip = 1
		mil = 1
    }
}

1828.1.1 = {
	monarch = {
 		name = "Benduin"
		dynasty = "Or'Mossmire"
		birth_date = 1783.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1914.1.1 = {
	monarch = {
 		name = "Obenion"
		dynasty = "Or'Niveneth"
		birth_date = 1875.1.1
		adm = 0
		dip = 0
		mil = 1
    }
}

1953.1.1 = {
	monarch = {
 		name = "Imriel"
		dynasty = "Ca'Green"
		birth_date = 1916.1.1
		adm = 5
		dip = 6
		mil = 4
		female = yes
    }
}

1995.1.1 = {
	monarch = {
 		name = "Ethgin"
		dynasty = "Ca'Glien"
		birth_date = 1959.1.1
		adm = 0
		dip = 0
		mil = 6
    }
}

2069.1.1 = {
	monarch = {
 		name = "Behelir"
		dynasty = "Or'Vulkwasten"
		birth_date = 2051.1.1
		adm = 5
		dip = 6
		mil = 3
    }
}

2163.1.1 = {
	monarch = {
 		name = "Fauridil"
		dynasty = "Ca'Laenlin"
		birth_date = 2136.1.1
		adm = 3
		dip = 6
		mil = 6
		female = yes
    }
}

2216.1.1 = {
	monarch = {
 		name = "Borelril"
		dynasty = "Ca'Falinesti"
		birth_date = 2185.1.1
		adm = 2
		dip = 5
		mil = 3
    }
}

2277.1.1 = {
	monarch = {
 		name = "Orchelos"
		dynasty = "Ur'Bluewood"
		birth_date = 2234.1.1
		adm = 0
		dip = 4
		mil = 0
    }
}

2338.1.1 = {
	monarch = {
 		name = "Henodras"
		dynasty = "Ur'Arenthia"
		birth_date = 2285.1.1
		adm = 5
		dip = 3
		mil = 3
    }
}

2415.1.1 = {
	monarch = {
 		name = "Faerin"
		dynasty = "Or'Oakenwood"
		birth_date = 2366.1.1
		adm = 3
		dip = 2
		mil = 0
    }
}

2462.1.1 = {
	monarch = {
 		name = "Borcholim"
		dynasty = "Ca'Laenlin"
		birth_date = 2410.1.1
		adm = 2
		dip = 2
		mil = 4
    }
}

