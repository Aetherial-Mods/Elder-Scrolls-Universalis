government = native
government_rank = 1
mercantilism = 1
technology_group = daedric_tg
religion = molag_bal_cult
primary_culture = harvester
capital = 2903

54.1.1 = {
	monarch = {
 		name = "Endarvah"
		dynasty = "Edzu-Revumi"
		birth_date = 36.1.1
		adm = 6
		dip = 1
		mil = 0
		female = yes
    }
}

105.1.1 = {
	monarch = {
 		name = "Begnegead"
		dynasty = "Edzu-Netavie"
		birth_date = 67.1.1
		adm = 4
		dip = 0
		mil = 4
    }
}

189.1.1 = {
	monarch = {
 		name = "Amroldan"
		dynasty = "Edzu-Ana"
		birth_date = 161.1.1
		adm = 3
		dip = 6
		mil = 0
    }
}

272.1.1 = {
	monarch = {
 		name = "Ervohna"
		dynasty = "Edzu-Havanea"
		birth_date = 247.1.1
		adm = 3
		dip = 3
		mil = 5
		female = yes
    }
}

336.1.1 = {
	monarch = {
 		name = "Foldogne"
		dynasty = "Edzu-Dovomi"
		birth_date = 312.1.1
		adm = 2
		dip = 3
		mil = 2
		female = yes
    }
}

432.1.1 = {
	monarch = {
 		name = "Ralrakral"
		dynasty = "Edzu-Hare"
		birth_date = 402.1.1
		adm = 0
		dip = 2
		mil = 5
    }
}

531.1.1 = {
	monarch = {
 		name = "Evelbal"
		dynasty = "Edzu-Maphevea"
		birth_date = 500.1.1
		adm = 5
		dip = 1
		mil = 2
    }
}

604.1.1 = {
	monarch = {
 		name = "Yegusned"
		dynasty = "Edzu-Maphevea"
		birth_date = 586.1.1
		adm = 3
		dip = 0
		mil = 6
    }
}

639.1.1 = {
	monarch = {
 		name = "Dalrulbal"
		dynasty = "Edzu-Yolea"
		birth_date = 602.1.1
		adm = 2
		dip = 6
		mil = 2
    }
}

714.1.1 = {
	monarch = {
 		name = "Ralrakral"
		dynasty = "Edzu-Shackle"
		birth_date = 691.1.1
		adm = 0
		dip = 6
		mil = 6
    }
}

757.1.1 = {
	monarch = {
 		name = "Evelbal"
		dynasty = "Edzu-Lana"
		birth_date = 704.1.1
		adm = 2
		dip = 0
		mil = 1
    }
}

837.1.1 = {
	monarch = {
 		name = "Yegusned"
		dynasty = "Edzu-Natume"
		birth_date = 806.1.1
		adm = 0
		dip = 6
		mil = 4
    }
}

935.1.1 = {
	monarch = {
 		name = "Norvazos"
		dynasty = "Edzu-Vothamea"
		birth_date = 894.1.1
		adm = 5
		dip = 5
		mil = 1
		female = yes
    }
}

1001.1.1 = {
	monarch = {
 		name = "Zulvurlis"
		dynasty = "Edzu-Narie"
		birth_date = 953.1.1
		adm = 4
		dip = 5
		mil = 5
    }
}

1092.1.1 = {
	monarch = {
 		name = "Unnekran"
		dynasty = "Edzu-Amunea"
		birth_date = 1050.1.1
		adm = 2
		dip = 4
		mil = 1
    }
}

1163.1.1 = {
	monarch = {
 		name = "Shindannoh"
		dynasty = "Edzu-Zona"
		birth_date = 1132.1.1
		adm = 0
		dip = 3
		mil = 5
		female = yes
    }
}

1220.1.1 = {
	monarch = {
 		name = "Norvazos"
		dynasty = "Edzu-Ipholie"
		birth_date = 1191.1.1
		adm = 5
		dip = 2
		mil = 1
		female = yes
    }
}

1272.1.1 = {
	monarch = {
 		name = "Lungaknam"
		dynasty = "Edzu-Nove"
		birth_date = 1226.1.1
		adm = 4
		dip = 1
		mil = 5
    }
}

1324.1.1 = {
	monarch = {
 		name = "Lugrurved"
		dynasty = "Edzu-Amunea"
		birth_date = 1271.1.1
		adm = 5
		dip = 3
		mil = 0
    }
}

1368.1.1 = {
	monarch = {
 		name = "Shindannoh"
		dynasty = "Edzu-Ozulia"
		birth_date = 1345.1.1
		adm = 4
		dip = 2
		mil = 3
		female = yes
    }
}

1404.1.1 = {
	monarch = {
 		name = "Felahlot"
		dynasty = "Edzu-Maphevea"
		birth_date = 1369.1.1
		adm = 2
		dip = 1
		mil = 0
		female = yes
    }
}

1474.1.1 = {
	monarch = {
 		name = "Yamirgad"
		dynasty = "Edzu-Nari"
		birth_date = 1433.1.1
		adm = 0
		dip = 0
		mil = 4
    }
}

1566.1.1 = {
	monarch = {
 		name = "Hanvictol"
		dynasty = "Edzu-Rania"
		birth_date = 1540.1.1
		adm = 5
		dip = 6
		mil = 0
		female = yes
    }
}

1642.1.1 = {
	monarch = {
 		name = "Filnagno"
		dynasty = "Edzu-Netheva"
		birth_date = 1590.1.1
		adm = 4
		dip = 6
		mil = 4
		female = yes
    }
}

1739.1.1 = {
	monarch = {
 		name = "Zonilbal"
		dynasty = "Edzu-Ogila"
		birth_date = 1698.1.1
		adm = 2
		dip = 5
		mil = 1
    }
}

1809.1.1 = {
	monarch = {
 		name = "Nepremol"
		dynasty = "Edzu-Dene"
		birth_date = 1780.1.1
		adm = 0
		dip = 4
		mil = 4
		female = yes
    }
}

1876.1.1 = {
	monarch = {
 		name = "Hanvictol"
		dynasty = "Edzu-Mani"
		birth_date = 1841.1.1
		adm = 2
		dip = 5
		mil = 6
		female = yes
    }
}

1932.1.1 = {
	monarch = {
 		name = "Laglirnul"
		dynasty = "Edzu-Revea"
		birth_date = 1891.1.1
		adm = 0
		dip = 5
		mil = 2
    }
}

2013.1.1 = {
	monarch = {
 		name = "Valruldal"
		dynasty = "Edzu-Rathane"
		birth_date = 1977.1.1
		adm = 6
		dip = 4
		mil = 6
    }
}

2098.1.1 = {
	monarch = {
 		name = "Zuvinkal"
		dynasty = "Edzu-Vishoma"
		birth_date = 2059.1.1
		adm = 4
		dip = 3
		mil = 3
    }
}

2166.1.1 = {
	monarch = {
 		name = "Naglestas"
		dynasty = "Edzu-Mophere"
		birth_date = 2142.1.1
		adm = 2
		dip = 2
		mil = 6
    }
}

2265.1.1 = {
	monarch = {
 		name = "Ulvirlem"
		dynasty = "Edzu-Dovula"
		birth_date = 2240.1.1
		adm = 0
		dip = 1
		mil = 3
    }
}

2326.1.1 = {
	monarch = {
 		name = "Nivlivah"
		dynasty = "Edzu-Amia"
		birth_date = 2300.1.1
		adm = 5
		dip = 1
		mil = 0
		female = yes
    }
}

2398.1.1 = {
	monarch = {
 		name = "Zuvinkal"
		dynasty = "Edzu-Nuthona"
		birth_date = 2352.1.1
		adm = 4
		dip = 0
		mil = 3
    }
}

2494.1.1 = {
	monarch = {
 		name = "Lerimmoh"
		dynasty = "			Edzu-Adomie"
		birth_date = 2460.1.1
		adm = 6
		dip = 1
		mil = 5
		female = yes
    }
}

