government = republic
government_rank = 3
mercantilism = 1
technology_group = dwemer_tg
religion = reason_and_logic_cult
primary_culture = dwemer
capital = 4273

54.1.1 = {
	monarch = {
 		name = "Rhzomrond"
		dynasty = "Aq'Banrynn"
		birth_date = 23.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

125.1.1 = {
	monarch = {
 		name = "Nedac"
		dynasty = "Aq'Chrotwern"
		birth_date = 78.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

175.1.1 = {
	monarch = {
 		name = "Jravarn"
		dynasty = "Aq'Cfradrys"
		birth_date = 132.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

231.1.1 = {
	monarch = {
 		name = "Kamzlin"
		dynasty = "Av'Tromgunch"
		birth_date = 178.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

306.1.1 = {
	monarch = {
 		name = "Inratarn"
		dynasty = "Af'Sthord"
		birth_date = 275.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

391.1.1 = {
	monarch = {
 		name = "Nedac"
		dynasty = "Aq'Kamzlin"
		birth_date = 347.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

443.1.1 = {
	monarch = {
 		name = "Dzredras"
		dynasty = "Az'Ylrefwinn"
		birth_date = 413.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

490.1.1 = {
	monarch = {
 		name = "Jnathunch"
		dynasty = "Af'Alnogwetch"
		birth_date = 458.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

525.1.1 = {
	monarch = {
 		name = "Shtrorlis"
		dynasty = "Aq'Ararbira"
		birth_date = 479.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

562.1.1 = {
	monarch = {
 		name = "Yhnazdir"
		dynasty = "Az'Shtrorlis"
		birth_date = 543.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

598.1.1 = {
	monarch = {
 		name = "Snebchasz"
		dynasty = "Az'Nhetchatz"
		birth_date = 572.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

672.1.1 = {
	monarch = {
 		name = "Azsamchin"
		dynasty = "Af'Nchynac"
		birth_date = 646.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

715.1.1 = {
	monarch = {
 		name = "Chzevragch"
		dynasty = "Af'Choalchanf"
		birth_date = 683.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

772.1.1 = {
	monarch = {
 		name = "Yhnazdir"
		dynasty = "Af'Jnavraz"
		birth_date = 754.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

826.1.1 = {
	monarch = {
 		name = "Czadlin"
		dynasty = "Av'Yhnazzefk"
		birth_date = 780.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

892.1.1 = {
	monarch = {
 		name = "Tronruz"
		dynasty = "Aq'Rlorlis"
		birth_date = 854.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

970.1.1 = {
	monarch = {
 		name = "Nromgunch"
		dynasty = "Aq'Ilzeglan"
		birth_date = 947.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1052.1.1 = {
	monarch = {
 		name = "Cheftris"
		dynasty = "Az'Yzranrida"
		birth_date = 1028.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1139.1.1 = {
	monarch = {
 		name = "Mherbira"
		dynasty = "Af'Kolzarf"
		birth_date = 1102.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1210.1.1 = {
	monarch = {
 		name = "Snelarn"
		dynasty = "Av'Tnarlac"
		birth_date = 1190.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1264.1.1 = {
	monarch = {
 		name = "Tnarlac"
		dynasty = "Av'Jlarerhunch"
		birth_date = 1222.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1315.1.1 = {
	monarch = {
 		name = "Asramrumhz"
		dynasty = "Av'Tromgunch"
		birth_date = 1293.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1405.1.1 = {
	monarch = {
 		name = "Cuohrek"
		dynasty = "Af'Alnogwetch"
		birth_date = 1385.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1462.1.1 = {
	monarch = {
 		name = "Chzegrenz"
		dynasty = "Aq'Ararbira"
		birth_date = 1417.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1535.1.1 = {
	monarch = {
 		name = "Klalzrak"
		dynasty = "Aq'Gonwess"
		birth_date = 1506.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1602.1.1 = {
	monarch = {
 		name = "Mrotchatz"
		dynasty = "Af'Grabnanch"
		birth_date = 1553.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1671.1.1 = {
	monarch = {
 		name = "Nchuzalf"
		dynasty = "Aq'Snebchasz"
		birth_date = 1650.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1741.1.1 = {
	monarch = {
 		name = "Asrahuanch"
		dynasty = "Af'Agahuanch"
		birth_date = 1695.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1829.1.1 = {
	monarch = {
 		name = "Mrobwarn"
		dynasty = "Af'Chzemgunch"
		birth_date = 1783.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1866.1.1 = {
	monarch = {
 		name = "Djubchasz"
		dynasty = "Af'Ghafuan"
		birth_date = 1827.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1917.1.1 = {
	monarch = {
 		name = "Ralen"
		dynasty = "Af'Izvumratz"
		birth_date = 1893.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2001.1.1 = {
	monarch = {
 		name = "Asrahuanch"
		dynasty = "Az'Nrotchzan"
		birth_date = 1957.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2081.1.1 = {
	monarch = {
 		name = "Rkudit"
		dynasty = "Az'Nhezril"
		birth_date = 2056.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2177.1.1 = {
	monarch = {
 		name = "Batvar"
		dynasty = "Aq'Byrahken"
		birth_date = 2124.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2224.1.1 = {
	monarch = {
 		name = "Izvuvretch"
		dynasty = "Av'Bzraban"
		birth_date = 2206.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

2290.1.1 = {
	monarch = {
 		name = "Chruhnch"
		dynasty = "Aq'Snenard"
		birth_date = 2270.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2330.1.1 = {
	monarch = {
 		name = "Brehron"
		dynasty = "Av'Choatchatz"
		birth_date = 2301.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2403.1.1 = {
	monarch = {
 		name = "Asrahuanch"
		dynasty = "Aq'Chzegrenz"
		birth_date = 2361.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2441.1.1 = {
	monarch = {
 		name = "Ychogarn"
		dynasty = "Av'Mzaglan"
		birth_date = 2390.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2477.1.1 = {
	monarch = {
 		name = "Aravlen"
		dynasty = "Av'Czadlin"
		birth_date = 2436.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

