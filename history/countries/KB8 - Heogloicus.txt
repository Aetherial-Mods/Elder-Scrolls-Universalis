government = theocracy
government_rank = 1
mercantilism = 1
technology_group = kamal_tg
religion = kamal_pantheon
primary_culture = kamal
capital = 503
# = 503
religious_school = tortoise_school

54.1.1 = {
	monarch = {
 		name = "Neurdeas"
		dynasty = "Rolion"
		birth_date = 32.1.1
		adm = 0
		dip = 0
		mil = 3
    }
}

116.1.1 = {
	monarch = {
 		name = "Theggy"
		dynasty = "Chezetheus"
		birth_date = 71.1.1
		adm = 2
		dip = 2
		mil = 4
		female = yes
    }
}

167.1.1 = {
	monarch = {
 		name = "Chutholo"
		dynasty = "Vorlos"
		birth_date = 128.1.1
		adm = 0
		dip = 1
		mil = 1
		female = yes
    }
}

266.1.1 = {
	monarch = {
 		name = "Noktelea"
		dynasty = "Voxeidon"
		birth_date = 242.1.1
		adm = 6
		dip = 0
		mil = 5
		female = yes
    }
}

308.1.1 = {
	monarch = {
 		name = "Xosis"
		dynasty = "Phoranes"
		birth_date = 259.1.1
		adm = 6
		dip = 4
		mil = 3
    }
}

359.1.1 = {
	monarch = {
 		name = "Dyzonos"
		dynasty = "Xomanos"
		birth_date = 318.1.1
		adm = 5
		dip = 3
		mil = 6
    }
}

409.1.1 = {
	monarch = {
 		name = "Chonaeus"
		dynasty = "Vimion"
		birth_date = 368.1.1
		adm = 3
		dip = 3
		mil = 3
    }
}

486.1.1 = {
	monarch = {
 		name = "Veuggiton"
		dynasty = "Kustaemon"
		birth_date = 445.1.1
		adm = 4
		dip = 0
		mil = 1
    }
}

585.1.1 = {
	monarch = {
 		name = "Chrostrerus"
		dynasty = "Rombras"
		birth_date = 565.1.1
		adm = 2
		dip = 6
		mil = 4
    }
}

630.1.1 = {
	monarch = {
 		name = "Nivytion"
		dynasty = "Kylaestus"
		birth_date = 588.1.1
		adm = 0
		dip = 5
		mil = 1
    }
}

705.1.1 = {
	monarch = {
 		name = "Chasias"
		dynasty = "Viggetus"
		birth_date = 677.1.1
		adm = 5
		dip = 4
		mil = 5
    }
}

749.1.1 = {
	monarch = {
 		name = "Rembrotus"
		dynasty = "Rembrotus"
		birth_date = 703.1.1
		adm = 4
		dip = 3
		mil = 1
    }
}

811.1.1 = {
	monarch = {
 		name = "Kanthagos"
		dynasty = "Madon"
		birth_date = 759.1.1
		adm = 2
		dip = 3
		mil = 5
    }
}

869.1.1 = {
	monarch = {
 		name = "Nivytion"
		dynasty = "Zeurdarus"
		birth_date = 835.1.1
		adm = 0
		dip = 2
		mil = 2
    }
}

914.1.1 = {
	monarch = {
 		name = "Volo"
		dynasty = "Nivytion"
		birth_date = 875.1.1
		adm = 5
		dip = 1
		mil = 5
		female = yes
    }
}

1004.1.1 = {
	monarch = {
 		name = "Hembrates"
		dynasty = "Mosonos"
		birth_date = 957.1.1
		adm = 0
		dip = 2
		mil = 0
    }
}

1077.1.1 = {
	monarch = {
 		name = "Xoludre"
		dynasty = "Chreusdaon"
		birth_date = 1048.1.1
		adm = 5
		dip = 2
		mil = 3
		female = yes
    }
}

1141.1.1 = {
	monarch = {
 		name = "Thaxo"
		dynasty = "Kanthagos"
		birth_date = 1112.1.1
		adm = 3
		dip = 4
		mil = 4
    }
}

1185.1.1 = {
	monarch = {
 		name = "Xastrida"
		dynasty = "Chrostrerus"
		birth_date = 1140.1.1
		adm = 4
		dip = 5
		mil = 5
		female = yes
    }
}

1236.1.1 = {
	monarch = {
 		name = "Ruthagos"
		dynasty = "Cheggatos"
		birth_date = 1203.1.1
		adm = 3
		dip = 4
		mil = 2
    }
}

1314.1.1 = {
	monarch = {
 		name = "Kole"
		dynasty = "Pheistrion"
		birth_date = 1275.1.1
		adm = 0
		dip = 6
		mil = 2
		female = yes
    }
}

1392.1.1 = {
	monarch = {
 		name = "Kelce"
		dynasty = "Phores"
		birth_date = 1370.1.1
		adm = 6
		dip = 2
		mil = 2
		female = yes
    }
}

1439.1.1 = {
	monarch = {
 		name = "Kasoeus"
		dynasty = "Theldaeon"
		birth_date = 1418.1.1
		adm = 4
		dip = 2
		mil = 6
    }
}

1516.1.1 = {
	monarch = {
 		name = "Xosis"
		dynasty = "Phoranes"
		birth_date = 1479.1.1
		adm = 3
		dip = 1
		mil = 2
    }
}

1552.1.1 = {
	monarch = {
 		name = "Phottibos"
		dynasty = "Theldaeon"
		birth_date = 1529.1.1
		adm = 3
		dip = 5
		mil = 0
    }
}

1592.1.1 = {
	monarch = {
 		name = "Kelce"
		dynasty = "Phoranes"
		birth_date = 1549.1.1
		adm = 2
		dip = 4
		mil = 4
		female = yes
    }
}

1668.1.1 = {
	monarch = {
 		name = "Nistuse"
		dynasty = "Chiggos"
		birth_date = 1616.1.1
		adm = 0
		dip = 3
		mil = 1
		female = yes
    }
}

1749.1.1 = {
	monarch = {
 		name = "Chrostrerus"
		dynasty = "Thovaumas"
		birth_date = 1717.1.1
		adm = 5
		dip = 3
		mil = 4
    }
}

1795.1.1 = {
	monarch = {
 		name = "Chaso"
		dynasty = "Nozios"
		birth_date = 1768.1.1
		adm = 3
		dip = 2
		mil = 1
		female = yes
    }
}

1844.1.1 = {
	monarch = {
 		name = "Mosonos"
		dynasty = "Chroxus"
		birth_date = 1820.1.1
		adm = 5
		dip = 3
		mil = 2
    }
}

1890.1.1 = {
	monarch = {
 		name = "Sthulea"
		dynasty = "Voxeidon"
		birth_date = 1868.1.1
		adm = 4
		dip = 2
		mil = 6
		female = yes
    }
}

1927.1.1 = {
	monarch = {
 		name = "Kanthagos"
		dynasty = "Voloeus"
		birth_date = 1894.1.1
		adm = 2
		dip = 1
		mil = 3
    }
}

1966.1.1 = {
	monarch = {
 		name = "Nivytion"
		dynasty = "Deisditon"
		birth_date = 1926.1.1
		adm = 3
		dip = 5
		mil = 1
    }
}

2022.1.1 = {
	monarch = {
 		name = "Vimion"
		dynasty = "Dinaemon"
		birth_date = 1997.1.1
		adm = 1
		dip = 5
		mil = 4
    }
}

2058.1.1 = {
	monarch = {
 		name = "Ruthagos"
		dynasty = "Xuxoeus"
		birth_date = 2037.1.1
		adm = 5
		dip = 0
		mil = 4
    }
}

2104.1.1 = {
	monarch = {
 		name = "Voloeus"
		dynasty = "Hespyrus"
		birth_date = 2062.1.1
		adm = 6
		dip = 4
		mil = 2
    }
}

2170.1.1 = {
	monarch = {
 		name = "Phores"
		dynasty = "Veuggiton"
		birth_date = 2143.1.1
		adm = 4
		dip = 3
		mil = 6
    }
}

2257.1.1 = {
	monarch = {
 		name = "Phondris"
		dynasty = "Neurdeas"
		birth_date = 2204.1.1
		adm = 2
		dip = 2
		mil = 2
    }
}

2350.1.1 = {
	monarch = {
 		name = "Chryzontho"
		dynasty = "Sthezetheus"
		birth_date = 2308.1.1
		adm = 4
		dip = 3
		mil = 4
		female = yes
    }
}

2427.1.1 = {
	monarch = {
 		name = "Viggetus"
		dynasty = "Nivytion"
		birth_date = 2377.1.1
		adm = 0
		dip = 5
		mil = 6
    }
}

