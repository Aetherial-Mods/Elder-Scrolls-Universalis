government = tribal
government_rank = 1
mercantilism = 1
technology_group = elven_tg
religion = nocturnal_cult
primary_culture = lilmothiit
capital = 6794

54.1.1 = {
	monarch = {
 		name = "Vyavyughan"
		dynasty = "Nodru-Wituluty"
		birth_date = 29.1.1
		adm = 0
		dip = 0
		mil = 3
    }
}

151.1.1 = {
	monarch = {
 		name = "Rupoi"
		dynasty = "Nodru-Viwe"
		birth_date = 113.1.1
		adm = 5
		dip = 0
		mil = 0
    }
}

237.1.1 = {
	monarch = {
 		name = "Ngkasyoghan"
		dynasty = "Nedre-Ghoro"
		birth_date = 208.1.1
		adm = 3
		dip = 6
		mil = 4
    }
}

296.1.1 = {
	monarch = {
 		name = "Giraka"
		dynasty = "Nudra-Ngompemp"
		birth_date = 246.1.1
		adm = 5
		dip = 0
		mil = 5
    }
}

378.1.1 = {
	monarch = {
 		name = "Vyovyovuar"
		dynasty = "Nedre-Gha"
		birth_date = 337.1.1
		adm = 4
		dip = 6
		mil = 2
		female = yes
    }
}

449.1.1 = {
	monarch = {
 		name = "Gumun"
		dynasty = "Nedre-Nato"
		birth_date = 431.1.1
		adm = 2
		dip = 5
		mil = 5
    }
}

516.1.1 = {
	monarch = {
 		name = "Pevadun"
		dynasty = "Nedre-Gati"
		birth_date = 463.1.1
		adm = 4
		dip = 0
		mil = 1
		female = yes
    }
}

593.1.1 = {
	monarch = {
 		name = "Myodun"
		dynasty = "Nudra-Ngkavyel"
		birth_date = 571.1.1
		adm = 3
		dip = 6
		mil = 4
		female = yes
    }
}

662.1.1 = {
	monarch = {
 		name = "Ganenighan"
		dynasty = "Nadro-Tiny"
		birth_date = 623.1.1
		adm = 5
		dip = 0
		mil = 6
    }
}

727.1.1 = {
	monarch = {
 		name = "Tyonyoredun"
		dynasty = "Nodru-Yumy"
		birth_date = 693.1.1
		adm = 3
		dip = 6
		mil = 3
		female = yes
    }
}

825.1.1 = {
	monarch = {
 		name = "Gesyighan"
		dynasty = "Nodru-Tyor"
		birth_date = 780.1.1
		adm = 1
		dip = 6
		mil = 6
    }
}

909.1.1 = {
	monarch = {
 		name = "Vagaghan"
		dynasty = "Nodru-Tyor"
		birth_date = 875.1.1
		adm = 6
		dip = 5
		mil = 3
    }
}

960.1.1 = {
	monarch = {
 		name = "Panganupoi"
		dynasty = "Nadro-Pogu"
		birth_date = 917.1.1
		adm = 5
		dip = 4
		mil = 0
    }
}

1025.1.1 = {
	monarch = {
 		name = "Myamyayeghan"
		dynasty = "Nadro-Pongk"
		birth_date = 1007.1.1
		adm = 3
		dip = 3
		mil = 3
    }
}

1087.1.1 = {
	monarch = {
 		name = "Geghan"
		dynasty = "Nadro-Syenar"
		birth_date = 1039.1.1
		adm = 1
		dip = 2
		mil = 0
    }
}

1149.1.1 = {
	monarch = {
 		name = "Yampangkaghan"
		dynasty = "Nodru-We"
		birth_date = 1113.1.1
		adm = 0
		dip = 6
		mil = 0
    }
}

1213.1.1 = {
	monarch = {
 		name = "Syonyeka"
		dynasty = "Nodru-Tyor"
		birth_date = 1167.1.1
		adm = 6
		dip = 5
		mil = 4
    }
}

1299.1.1 = {
	monarch = {
 		name = "Ntengusaghan"
		dynasty = "Nudra-Nutyuvy"
		birth_date = 1258.1.1
		adm = 4
		dip = 4
		mil = 0
    }
}

1354.1.1 = {
	monarch = {
 		name = "Matoghan"
		dynasty = "Nadro-Sum"
		birth_date = 1303.1.1
		adm = 2
		dip = 3
		mil = 4
    }
}

1447.1.1 = {
	monarch = {
 		name = "Wunumyabya"
		dynasty = "Nudra-Nayogh"
		birth_date = 1411.1.1
		adm = 0
		dip = 3
		mil = 1
    }
}

1501.1.1 = {
	monarch = {
 		name = "Syoka"
		dynasty = "Nadro-Tavyo"
		birth_date = 1483.1.1
		adm = 6
		dip = 2
		mil = 4
    }
}

1585.1.1 = {
	monarch = {
 		name = "Noweghan"
		dynasty = "Nadro-Sinyal"
		birth_date = 1545.1.1
		adm = 1
		dip = 3
		mil = 6
    }
}

1622.1.1 = {
	monarch = {
 		name = "Matebya"
		dynasty = "Nedre-Mpogha"
		birth_date = 1584.1.1
		adm = 6
		dip = 2
		mil = 3
    }
}

1681.1.1 = {
	monarch = {
 		name = "Lomyitun"
		dynasty = "Nedre-Geghal"
		birth_date = 1659.1.1
		adm = 2
		dip = 4
		mil = 5
		female = yes
    }
}

1763.1.1 = {
	monarch = {
 		name = "Vyonatyar"
		dynasty = "Nudra-Ngkugha"
		birth_date = 1717.1.1
		adm = 0
		dip = 3
		mil = 2
		female = yes
    }
}

1809.1.1 = {
	monarch = {
 		name = "Sodun"
		dynasty = "Nadro-Sive"
		birth_date = 1774.1.1
		adm = 5
		dip = 2
		mil = 5
		female = yes
    }
}

1857.1.1 = {
	monarch = {
 		name = "Ngkamumun"
		dynasty = "Nudra-Nimyu"
		birth_date = 1808.1.1
		adm = 0
		dip = 3
		mil = 0
    }
}

1906.1.1 = {
	monarch = {
 		name = "Ghuka"
		dynasty = "Nedre-Lasov"
		birth_date = 1854.1.1
		adm = 5
		dip = 3
		mil = 3
    }
}

1982.1.1 = {
	monarch = {
 		name = "Ngkaginyutun"
		dynasty = "Nadro-Puga"
		birth_date = 1934.1.1
		adm = 3
		dip = 2
		mil = 0
		female = yes
    }
}

2024.1.1 = {
	monarch = {
 		name = "Govyopoi"
		dynasty = "Nadro-Tata"
		birth_date = 1975.1.1
		adm = 2
		dip = 1
		mil = 4
    }
}

2079.1.1 = {
	monarch = {
 		name = "Vyunotun"
		dynasty = "Nadro-Taya"
		birth_date = 2060.1.1
		adm = 0
		dip = 0
		mil = 0
		female = yes
    }
}

2152.1.1 = {
	monarch = {
 		name = "Sasyamun"
		dynasty = "Nudra-Nevivus"
		birth_date = 2128.1.1
		adm = 6
		dip = 4
		mil = 1
    }
}

2223.1.1 = {
	monarch = {
 		name = "Ganaghan"
		dynasty = "Nedre-Mpu"
		birth_date = 2184.1.1
		adm = 5
		dip = 3
		mil = 4
    }
}

2267.1.1 = {
	monarch = {
 		name = "Mulika"
		dynasty = "Nudra-Ngo"
		birth_date = 2239.1.1
		adm = 3
		dip = 2
		mil = 1
    }
}

2361.1.1 = {
	monarch = {
 		name = "Ghayompidun"
		dynasty = "Nodru-Va"
		birth_date = 2312.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

2408.1.1 = {
	monarch = {
 		name = "Tyumyatompoi"
		dynasty = "Nudra-Nimyu"
		birth_date = 2375.1.1
		adm = 6
		dip = 0
		mil = 1
    }
}

2484.1.1 = {
	monarch = {
 		name = "Pungkutyar"
		dynasty = "Nodru-Wituluty"
		birth_date = 2459.1.1
		adm = 5
		dip = 0
		mil = 5
		female = yes
    }
}

