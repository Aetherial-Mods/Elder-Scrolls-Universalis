government = theocracy
government_rank = 1
mercantilism = 1
technology_group = elven_tg
religion = altmeri_pantheon
primary_culture = altmer
capital = 4734

54.1.1 = {
	monarch = {
 		name = "Tanulnyon"
		dynasty = "Ael-Nalcrand"
		birth_date = 33.1.1
		adm = 0
		dip = 5
		mil = 2
    }
}

147.1.1 = {
	monarch = {
 		name = "Naryon"
		dynasty = "Bel-Shimmerene"
		birth_date = 125.1.1
		adm = 5
		dip = 4
		mil = 6
    }
}

203.1.1 = {
	monarch = {
 		name = "Galondorn"
		dynasty = "Ael-Isque"
		birth_date = 164.1.1
		adm = 0
		dip = 5
		mil = 1
    }
}

290.1.1 = {
	monarch = {
 		name = "Bayenor"
		dynasty = "Bel-Valia"
		birth_date = 250.1.1
		adm = 5
		dip = 5
		mil = 4
    }
}

387.1.1 = {
	monarch = {
 		name = "Errannie"
		dynasty = "Wel-Elanesse"
		birth_date = 350.1.1
		adm = 3
		dip = 4
		mil = 1
		female = yes
    }
}

472.1.1 = {
	monarch = {
 		name = "Valion"
		dynasty = "Wel-Ardende"
		birth_date = 440.1.1
		adm = 4
		dip = 1
		mil = 6
    }
}

507.1.1 = {
	monarch = {
 		name = "Pelimo"
		dynasty = "Bel-Termrine"
		birth_date = 471.1.1
		adm = 2
		dip = 0
		mil = 3
    }
}

576.1.1 = {
	monarch = {
 		name = "Varafalmil"
		dynasty = "Bel-Ronassa"
		birth_date = 544.1.1
		adm = 1
		dip = 6
		mil = 6
    }
}

664.1.1 = {
	monarch = {
 		name = "Quaranon"
		dynasty = "Bel-Valia"
		birth_date = 618.1.1
		adm = 6
		dip = 6
		mil = 3
    }
}

705.1.1 = {
	monarch = {
 		name = "Lernuvaril"
		dynasty = "Wel-Cinrie"
		birth_date = 666.1.1
		adm = 4
		dip = 5
		mil = 6
    }
}

758.1.1 = {
	monarch = {
 		name = "Ellidoril"
		dynasty = "Wel-Faeire"
		birth_date = 724.1.1
		adm = 2
		dip = 4
		mil = 3
    }
}

856.1.1 = {
	monarch = {
 		name = "Varulae"
		dynasty = "Ael-Nalcrand"
		birth_date = 821.1.1
		adm = 1
		dip = 3
		mil = 0
		female = yes
    }
}

921.1.1 = {
	monarch = {
 		name = "Pamolinwe"
		dynasty = "Wel-Cinrie"
		birth_date = 878.1.1
		adm = 6
		dip = 2
		mil = 3
		female = yes
    }
}

957.1.1 = {
	monarch = {
 		name = "Lerinyon"
		dynasty = "Wel-Angaelle"
		birth_date = 939.1.1
		adm = 1
		dip = 4
		mil = 5
    }
}

1024.1.1 = {
	monarch = {
 		name = "Catannie"
		dynasty = "Wel-Alkinosin"
		birth_date = 999.1.1
		adm = 6
		dip = 3
		mil = 2
		female = yes
    }
}

1112.1.1 = {
	monarch = {
 		name = "Imare"
		dynasty = "Ael-Nenurmend"
		birth_date = 1071.1.1
		adm = 4
		dip = 2
		mil = 5
		female = yes
    }
}

1147.1.1 = {
	monarch = {
 		name = "Cirunra"
		dynasty = "Bel-Silsailen"
		birth_date = 1114.1.1
		adm = 2
		dip = 1
		mil = 2
		female = yes
    }
}

1222.1.1 = {
	monarch = {
 		name = "Sorfinilon"
		dynasty = "Ael-Lorusse"
		birth_date = 1169.1.1
		adm = 3
		dip = 5
		mil = 0
		female = yes
    }
}

1260.1.1 = {
	monarch = {
 		name = "Bayenor"
		dynasty = "Wel-Croddlehurst"
		birth_date = 1229.1.1
		adm = 1
		dip = 5
		mil = 4
    }
}

1320.1.1 = {
	monarch = {
 		name = "Tanorian"
		dynasty = "Ael-Jaerfhaer"
		birth_date = 1277.1.1
		adm = 0
		dip = 4
		mil = 0
    }
}

1361.1.1 = {
	monarch = {
 		name = "Narandor"
		dynasty = "Wel-Celae"
		birth_date = 1327.1.1
		adm = 0
		dip = 1
		mil = 5
    }
}

1458.1.1 = {
	monarch = {
 		name = "Pelimo"
		dynasty = "Bel-Termrine"
		birth_date = 1431.1.1
		adm = 6
		dip = 0
		mil = 2
    }
}

1555.1.1 = {
	monarch = {
 		name = "Haenelisse"
		dynasty = "Bel-Tanorne"
		birth_date = 1506.1.1
		adm = 4
		dip = 6
		mil = 5
		female = yes
    }
}

1591.1.1 = {
	monarch = {
 		name = "Earos"
		dynasty = "Bel-Sondnae"
		birth_date = 1548.1.1
		adm = 5
		dip = 3
		mil = 3
    }
}

1655.1.1 = {
	monarch = {
 		name = "Fasendil"
		dynasty = "Ael-Lillandril"
		birth_date = 1632.1.1
		adm = 3
		dip = 3
		mil = 0
    }
}

1746.1.1 = {
	monarch = {
 		name = "Aminore"
		dynasty = "Bel-Siriginia"
		birth_date = 1720.1.1
		adm = 1
		dip = 2
		mil = 4
		female = yes
    }
}

1824.1.1 = {
	monarch = {
 		name = "Siniaritaale"
		dynasty = "Ael-Ohterane"
		birth_date = 1781.1.1
		adm = 6
		dip = 1
		mil = 0
		female = yes
    }
}

1898.1.1 = {
	monarch = {
 		name = "Earil"
		dynasty = "Bel-Shamia"
		birth_date = 1867.1.1
		adm = 6
		dip = 4
		mil = 1
    }
}

1939.1.1 = {
	monarch = {
 		name = "Vairalmil"
		dynasty = "Wel-Celrandil"
		birth_date = 1907.1.1
		adm = 4
		dip = 4
		mil = 4
    }
}

1985.1.1 = {
	monarch = {
 		name = "Catannie"
		dynasty = "Ael-Niria"
		birth_date = 1937.1.1
		adm = 1
		dip = 6
		mil = 4
		female = yes
    }
}

2030.1.1 = {
	monarch = {
 		name = "Sorangarion"
		dynasty = "Bel-Taanye"
		birth_date = 1990.1.1
		adm = 3
		dip = 0
		mil = 6
    }
}

2071.1.1 = {
	monarch = {
 		name = "Linaanque"
		dynasty = "Bel-Termrine"
		birth_date = 2045.1.1
		adm = 1
		dip = 6
		mil = 2
		female = yes
    }
}

2144.1.1 = {
	monarch = {
 		name = "Suurelbrim"
		dynasty = "Wel-Cloudrest"
		birth_date = 2109.1.1
		adm = 0
		dip = 5
		mil = 6
    }
}

2191.1.1 = {
	monarch = {
 		name = "Muril"
		dynasty = "Ael-Minoirdalin"
		birth_date = 2147.1.1
		adm = 5
		dip = 4
		mil = 3
    }
}

2252.1.1 = {
	monarch = {
 		name = "Elendarie"
		dynasty = "Wel-Eleaninde"
		birth_date = 2233.1.1
		adm = 3
		dip = 4
		mil = 6
		female = yes
    }
}

2304.1.1 = {
	monarch = {
 		name = "Ardorin"
		dynasty = "Wel-Faeire"
		birth_date = 2269.1.1
		adm = 1
		dip = 3
		mil = 3
    }
}

2350.1.1 = {
	monarch = {
 		name = "Sununturil"
		dynasty = "Bel-Terna"
		birth_date = 2319.1.1
		adm = 0
		dip = 2
		mil = 0
    }
}

2437.1.1 = {
	monarch = {
 		name = "Muramil"
		dynasty = "Ael-Lorusse"
		birth_date = 2405.1.1
		adm = 5
		dip = 1
		mil = 3
    }
}

2493.1.1 = {
	monarch = {
 		name = "Feredir"
		dynasty = "		"
		birth_date = 2468.1.1
		adm = 0
		dip = 3
		mil = 5
    }
}

