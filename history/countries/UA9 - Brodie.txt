government = monarchy
government_rank = 1
mercantilism = 1
technology_group = akavir_tg
religion = po_tun_pantheon
primary_culture = po_tun
capital = 3543

54.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Tiar"
		monarch_name = "Tiar I"
		dynasty = "Qachil'wo"
		birth_date = 40.1.1
		death_date = 58.1.1
		claim = 100
		adm = 4
		dip = 0
		mil = 0
		female = yes
    }
}

58.1.1 = {
	monarch = {
 		name = "Lihya"
		dynasty = "Rondez'su"
		birth_date = 16.1.1
		adm = 4
		dip = 3
		mil = 0
    }
}

136.1.1 = {
	monarch = {
 		name = "Doknog"
		dynasty = "Curcu'la"
		birth_date = 95.1.1
		adm = 2
		dip = 2
		mil = 4
    }
}

192.1.1 = {
	monarch = {
 		name = "Piruhus"
		dynasty = "Tus'su"
		birth_date = 148.1.1
		adm = 0
		dip = 2
		mil = 1
    }
	queen = {
 		name = "Karken"
		dynasty = "Tus'su"
		birth_date = 141.1.1
		adm = 4
		dip = 0
		mil = 0
		female = yes
    }
}

246.1.1 = {
	monarch = {
 		name = "Thom"
		dynasty = "Macmecnol'wo"
		birth_date = 206.1.1
		adm = 2
		dip = 6
		mil = 3
    }
	queen = {
 		name = "Thuhelnum"
		dynasty = "Macmecnol'wo"
		birth_date = 208.1.1
		adm = 6
		dip = 5
		mil = 2
		female = yes
    }
}

320.1.1 = {
	monarch = {
 		name = "Tid"
		dynasty = "Feg'ri"
		birth_date = 296.1.1
		adm = 3
		dip = 0
		mil = 2
    }
}

384.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Fog"
		monarch_name = "Fog I"
		dynasty = "Lihya'su"
		birth_date = 369.1.1
		death_date = 387.1.1
		claim = 100
		adm = 4
		dip = 4
		mil = 0
    }
}

387.1.1 = {
	monarch = {
 		name = "Shreyl"
		dynasty = "Den'su"
		birth_date = 334.1.1
		adm = 3
		dip = 0
		mil = 0
		female = yes
    }
	queen = {
 		name = "Rozzeg"
		dynasty = "Den'su"
		birth_date = 359.1.1
		adm = 0
		dip = 6
		mil = 6
    }
}

429.1.1 = {
	monarch = {
 		name = "Bororr"
		dynasty = "Wolcor'la"
		birth_date = 391.1.1
		adm = 6
		dip = 5
		mil = 3
		female = yes
    }
	queen = {
 		name = "Par"
		dynasty = "Wolcor'la"
		birth_date = 396.1.1
		adm = 2
		dip = 4
		mil = 2
    }
}

471.1.1 = {
	monarch = {
 		name = "Krer"
		dynasty = "Munok'wo"
		birth_date = 421.1.1
		adm = 1
		dip = 3
		mil = 6
    }
}

529.1.1 = {
	monarch = {
 		name = "Chehlon"
		dynasty = "For'la"
		birth_date = 491.1.1
		adm = 6
		dip = 2
		mil = 2
    }
}

593.1.1 = {
	monarch = {
 		name = "Vag"
		dynasty = "Sorove'su"
		birth_date = 569.1.1
		adm = 4
		dip = 1
		mil = 6
    }
}

675.1.1 = {
	monarch = {
 		name = "Gaduyd"
		dynasty = "Tosdul'wo"
		birth_date = 625.1.1
		adm = 2
		dip = 0
		mil = 3
    }
	queen = {
 		name = "Klirkugots"
		dynasty = "Tosdul'wo"
		birth_date = 655.1.1
		adm = 6
		dip = 6
		mil = 2
		female = yes
    }
	heir = {
 		name = "Voldar"
		monarch_name = "Voldar I"
		dynasty = "Tosdul'wo"
		birth_date = 674.1.1
		death_date = 761.1.1
		claim = 100
		adm = 3
		dip = 5
		mil = 1
    }
}

761.1.1 = {
	monarch = {
 		name = "Chehlon"
		dynasty = "Nontrae'la"
		birth_date = 736.1.1
		adm = 6
		dip = 6
		mil = 3
    }
}

817.1.1 = {
	monarch = {
 		name = "Vag"
		dynasty = "Tiar'la"
		birth_date = 794.1.1
		adm = 4
		dip = 5
		mil = 0
    }
	queen = {
 		name = "Lisurr"
		dynasty = "Tiar'la"
		birth_date = 781.1.1
		adm = 1
		dip = 4
		mil = 6
		female = yes
    }
	heir = {
 		name = "Dowentem"
		monarch_name = "Dowentem I"
		dynasty = "Tiar'la"
		birth_date = 815.1.1
		death_date = 892.1.1
		claim = 100
		adm = 5
		dip = 2
		mil = 4
		female = yes
    }
}

892.1.1 = {
	monarch = {
 		name = "Gelnar"
		dynasty = "Wolcor'la"
		birth_date = 867.1.1
		adm = 4
		dip = 6
		mil = 5
    }
	queen = {
 		name = "Luttesso"
		dynasty = "Wolcor'la"
		birth_date = 849.1.1
		adm = 1
		dip = 4
		mil = 4
		female = yes
    }
	heir = {
 		name = "Mur"
		monarch_name = "Mur I"
		dynasty = "Wolcor'la"
		birth_date = 885.1.1
		death_date = 958.1.1
		claim = 100
		adm = 1
		dip = 1
		mil = 5
    }
}

958.1.1 = {
	monarch = {
 		name = "Dochecmea"
		dynasty = "Thewud'ri"
		birth_date = 905.1.1
		adm = 1
		dip = 4
		mil = 5
		female = yes
    }
}

1006.1.1 = {
	monarch = {
 		name = "Remoz"
		dynasty = "Qevor'wo"
		birth_date = 954.1.1
		adm = 6
		dip = 3
		mil = 2
    }
	queen = {
 		name = "Luhu"
		dynasty = "Qevor'wo"
		birth_date = 978.1.1
		adm = 0
		dip = 4
		mil = 6
		female = yes
    }
}

1091.1.1 = {
	monarch = {
 		name = "Zud"
		dynasty = "For'la"
		birth_date = 1070.1.1
		adm = 6
		dip = 3
		mil = 3
    }
	queen = {
 		name = "Wegichuk"
		dynasty = "For'la"
		birth_date = 1057.1.1
		adm = 2
		dip = 2
		mil = 2
		female = yes
    }
	heir = {
 		name = "Derreho"
		monarch_name = "Derreho I"
		dynasty = "For'la"
		birth_date = 1081.1.1
		death_date = 1146.1.1
		claim = 100
		adm = 6
		dip = 0
		mil = 1
    }
}

1146.1.1 = {
	monarch = {
 		name = "Ramoz"
		dynasty = "Rondez'su"
		birth_date = 1112.1.1
		adm = 6
		dip = 4
		mil = 1
    }
	queen = {
 		name = "Susdevu"
		dynasty = "Rondez'su"
		birth_date = 1127.1.1
		adm = 3
		dip = 2
		mil = 0
		female = yes
    }
	heir = {
 		name = "Gur"
		monarch_name = "Gur I"
		dynasty = "Rondez'su"
		birth_date = 1133.1.1
		death_date = 1222.1.1
		claim = 100
		adm = 3
		dip = 6
		mil = 1
    }
}

1222.1.1 = {
	monarch = {
 		name = "Qokug"
		dynasty = "De'la"
		birth_date = 1180.1.1
		adm = 2
		dip = 2
		mil = 1
    }
}

1261.1.1 = {
	monarch = {
 		name = "Chadhak"
		dynasty = "Tehortes'la"
		birth_date = 1225.1.1
		adm = 0
		dip = 1
		mil = 5
    }
}

1319.1.1 = {
	monarch = {
 		name = "Fon"
		dynasty = "Pon'su"
		birth_date = 1298.1.1
		adm = 6
		dip = 0
		mil = 2
    }
	queen = {
 		name = "Qe"
		dynasty = "Pon'su"
		birth_date = 1270.1.1
		adm = 3
		dip = 6
		mil = 1
		female = yes
    }
	heir = {
 		name = "Kil"
		monarch_name = "Kil I"
		dynasty = "Pon'su"
		birth_date = 1309.1.1
		death_date = 1367.1.1
		claim = 100
		adm = 6
		dip = 4
		mil = 0
		female = yes
    }
}

1367.1.1 = {
	monarch = {
 		name = "Demusdoor"
		dynasty = "Moyd'su"
		birth_date = 1314.1.1
		adm = 5
		dip = 4
		mil = 3
    }
}

1418.1.1 = {
	monarch = {
 		name = "Thusdu"
		dynasty = "Lisurr'la"
		birth_date = 1373.1.1
		adm = 3
		dip = 3
		mil = 0
		female = yes
    }
	queen = {
 		name = "Fon"
		dynasty = "Lisurr'la"
		birth_date = 1395.1.1
		adm = 4
		dip = 4
		mil = 5
    }
}

1509.1.1 = {
	monarch = {
 		name = "Livosu"
		dynasty = "Domdondrul'su"
		birth_date = 1478.1.1
		adm = 6
		dip = 5
		mil = 6
    }
}

1590.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Se"
		monarch_name = "Se I"
		dynasty = "Rusdevyal'wo"
		birth_date = 1589.1.1
		death_date = 1607.1.1
		claim = 100
		adm = 3
		dip = 0
		mil = 6
		female = yes
    }
}

1607.1.1 = {
	monarch = {
 		name = "De"
		dynasty = "Shrinetuk'la"
		birth_date = 1587.1.1
		adm = 3
		dip = 3
		mil = 0
		female = yes
    }
}

1652.1.1 = {
	monarch = {
 		name = "Kev"
		dynasty = "Thewud'ri"
		birth_date = 1599.1.1
		adm = 3
		dip = 0
		mil = 4
    }
	queen = {
 		name = "Droryrko"
		dynasty = "Thewud'ri"
		birth_date = 1610.1.1
		adm = 4
		dip = 4
		mil = 6
		female = yes
    }
	heir = {
 		name = "Rowendurr"
		monarch_name = "Rowendurr I"
		dynasty = "Thewud'ri"
		birth_date = 1639.1.1
		death_date = 1727.1.1
		claim = 100
		adm = 1
		dip = 2
		mil = 4
    }
}

1727.1.1 = {
	monarch = {
 		name = "Ven"
		dynasty = "Win'la"
		birth_date = 1686.1.1
		adm = 0
		dip = 6
		mil = 5
    }
	queen = {
 		name = "De"
		dynasty = "Win'la"
		birth_date = 1706.1.1
		adm = 4
		dip = 4
		mil = 4
		female = yes
    }
	heir = {
 		name = "Ked"
		monarch_name = "Ked I"
		dynasty = "Win'la"
		birth_date = 1720.1.1
		death_date = 1775.1.1
		claim = 100
		adm = 1
		dip = 3
		mil = 3
    }
}

1775.1.1 = {
	monarch = {
 		name = "Zohlad"
		dynasty = "Rolum'ri"
		birth_date = 1730.1.1
		adm = 3
		dip = 4
		mil = 5
    }
}

1811.1.1 = {
	monarch = {
 		name = "Gon"
		dynasty = "Kev'ri"
		birth_date = 1770.1.1
		adm = 2
		dip = 3
		mil = 2
    }
}

1886.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "So"
		monarch_name = "So I"
		dynasty = "Fiestemo'ri"
		birth_date = 1878.1.1
		death_date = 1896.1.1
		claim = 100
		adm = 2
		dip = 0
		mil = 0
		female = yes
    }
}

1896.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Pin"
		monarch_name = "Pin I"
		dynasty = "Suhorrur'ri"
		birth_date = 1895.1.1
		death_date = 1913.1.1
		claim = 100
		adm = 3
		dip = 4
		mil = 5
		female = yes
    }
}

1913.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Dalnog"
		monarch_name = "Dalnog I"
		dynasty = "Lisurr'la"
		birth_date = 1913.1.1
		death_date = 1931.1.1
		claim = 100
		adm = 1
		dip = 4
		mil = 1
    }
}

1931.1.1 = {
	monarch = {
 		name = "Rolum"
		dynasty = "Kusdere'su"
		birth_date = 1880.1.1
		adm = 4
		dip = 5
		mil = 4
    }
}

2015.1.1 = {
	monarch = {
 		name = "Settu"
		dynasty = "Sen'ri"
		birth_date = 1986.1.1
		adm = 2
		dip = 4
		mil = 0
		female = yes
    }
}

2081.1.1 = {
	monarch = {
 		name = "Tehuntu"
		dynasty = "Shracmur'wo"
		birth_date = 2056.1.1
		adm = 4
		dip = 5
		mil = 2
    }
	queen = {
 		name = "Shracmur"
		dynasty = "Shracmur'wo"
		birth_date = 2052.1.1
		adm = 5
		dip = 2
		mil = 3
		female = yes
    }
	heir = {
 		name = "Bo"
		monarch_name = "Bo I"
		dynasty = "Shracmur'wo"
		birth_date = 2072.1.1
		death_date = 2117.1.1
		claim = 100
		adm = 1
		dip = 0
		mil = 2
		female = yes
    }
}

2117.1.1 = {
	monarch = {
 		name = "Fiandok"
		dynasty = "Nontrae'la"
		birth_date = 2071.1.1
		adm = 1
		dip = 4
		mil = 2
    }
}

2212.1.1 = {
	monarch = {
 		name = "Ruvo"
		dynasty = "Gur'su"
		birth_date = 2172.1.1
		adm = 6
		dip = 3
		mil = 6
    }
}

2264.1.1 = {
	monarch = {
 		name = "Sehondek"
		dynasty = "Pakhak'su"
		birth_date = 2244.1.1
		adm = 4
		dip = 2
		mil = 2
    }
	queen = {
 		name = "Shracmur"
		dynasty = "Pakhak'su"
		birth_date = 2218.1.1
		adm = 1
		dip = 1
		mil = 1
		female = yes
    }
}

2324.1.1 = {
	monarch = {
 		name = "Gakuveets"
		dynasty = "Bundrumu'ri"
		birth_date = 2284.1.1
		adm = 6
		dip = 0
		mil = 5
		female = yes
    }
	queen = {
 		name = "Pu"
		dynasty = "Bundrumu'ri"
		birth_date = 2274.1.1
		adm = 1
		dip = 1
		mil = 3
    }
	heir = {
 		name = "Neyak"
		monarch_name = "Neyak I"
		dynasty = "Bundrumu'ri"
		birth_date = 2311.1.1
		death_date = 2370.1.1
		claim = 100
		adm = 5
		dip = 6
		mil = 2
    }
}

2370.1.1 = {
	monarch = {
 		name = "Den"
		dynasty = "Sagecnar'wo"
		birth_date = 2319.1.1
		adm = 4
		dip = 3
		mil = 2
    }
	queen = {
 		name = "Dur"
		dynasty = "Sagecnar'wo"
		birth_date = 2321.1.1
		adm = 1
		dip = 1
		mil = 1
		female = yes
    }
}

2438.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Dusturo"
		monarch_name = "Dusturo I"
		dynasty = "Ciar'la"
		birth_date = 2425.1.1
		death_date = 2443.1.1
		claim = 100
		adm = 5
		dip = 3
		mil = 1
		female = yes
    }
}

2443.1.1 = {
	monarch = {
 		name = "Nontrae"
		dynasty = "Demusdoor'su"
		birth_date = 2425.1.1
		adm = 4
		dip = 0
		mil = 1
		female = yes
    }
	queen = {
 		name = "Piruhus"
		dynasty = "Demusdoor'su"
		birth_date = 2422.1.1
		adm = 1
		dip = 5
		mil = 0
    }
	heir = {
 		name = "Sierrosde"
		monarch_name = "Sierrosde I"
		dynasty = "Demusdoor'su"
		birth_date = 2439.1.1
		death_date = 2523.1.1
		claim = 100
		adm = 5
		dip = 4
		mil = 6
		female = yes
    }
}

