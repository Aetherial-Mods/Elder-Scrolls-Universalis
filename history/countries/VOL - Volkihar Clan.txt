government = monarchy
government_rank = 5
mercantilism = 1
technology_group = northern_tg
religion = molag_bal_cult
primary_culture = vampire
capital = 1344

54.1.1 = {
	monarch = {
 		name = "Quentin"
		dynasty = "Naaneen"
		birth_date = 36.1.1
		adm = 6
		dip = 4
		mil = 2
    }
	add_ruler_personality = immortal_personality
	queen = {
 		name = "Aventina"
		dynasty = "Naaneen"
		birth_date = 16.1.1
		adm = 3
		dip = 2
		mil = 1
		female = yes
    }
	add_queen_personality = immortal_personality
}

89.1.1 = {
	monarch = {
 		name = "Aemilia"
		dynasty = "Tyks"
		birth_date = 59.1.1
		adm = 1
		dip = 2
		mil = 5
		female = yes
    }
	add_ruler_personality = immortal_personality
	queen = {
 		name = "Imus"
		dynasty = "Tyks"
		birth_date = 41.1.1
		adm = 5
		dip = 0
		mil = 4
    }
	add_queen_personality = immortal_personality
	heir = {
 		name = "Viator"
		monarch_name = "Viator I"
		dynasty = "Tyks"
		birth_date = 77.1.1
		death_date = 165.1.1
		claim = 100
		adm = 2
		dip = 6
		mil = 3
    }
	add_heir_personality = immortal_personality
}

165.1.1 = {
	monarch = {
 		name = "Luven"
		dynasty = "Adanaa"
		birth_date = 137.1.1
		adm = 4
		dip = 0
		mil = 5
    }
	add_ruler_personality = immortal_personality
}

211.1.1 = {
	monarch = {
		name = "Harkon"
        dynasty = "Volhkihar"
		culture = vampire
		birth_date = 181.1.1
		adm = 3
		dip = 5
		mil = 7
	}
	add_ruler_personality = immortal_personality
}

211.1.1 = {
	heir = {
		name = "Serana"
		monarch_name = "Serana I"
        dynasty = "Volhkihar"
		culture = vampire
		birth_date = 197.1.1
		death_date = 9999.1.1
		claim = 100
		adm = 5
		dip = 7
		mil = 3
		female = yes
	}
	add_heir_personality = immortal_personality
}

211.1.1 = {
	queen = {
		name = "Valerica"
        dynasty = "Volhkihar"
		culture = vampire
		birth_date = 185.1.1
		death_date = 9999.1.1
		country_of_origin = VOL
		adm = 7
		dip = 3
		mil = 5
		female = yes
	}
	add_queen_personality = immortal_personality
}