government = tribal
government_rank = 1
mercantilism = 1
technology_group = orsimer_tg
religion = malacath_cult
primary_culture = iron_orc
capital = 6253

54.1.1 = {
	monarch = {
 		name = "Gezdak"
		dynasty = "gro-Lothdush"
		birth_date = 21.1.1
		adm = 2
		dip = 1
		mil = 2
    }
}

150.1.1 = {
	monarch = {
 		name = "Yashnarz"
		dynasty = "gro-Magrol"
		birth_date = 103.1.1
		adm = 1
		dip = 0
		mil = 5
    }
}

222.1.1 = {
	monarch = {
 		name = "Rogdul"
		dynasty = "gro-Mauhoth"
		birth_date = 177.1.1
		adm = 6
		dip = 6
		mil = 2
    }
}

295.1.1 = {
	monarch = {
 		name = "Lozotusk"
		dynasty = "gro-Ghamborz"
		birth_date = 275.1.1
		adm = 4
		dip = 6
		mil = 5
    }
}

376.1.1 = {
	monarch = {
 		name = "Gashzug"
		dynasty = "gro-Nabshuq"
		birth_date = 332.1.1
		adm = 2
		dip = 5
		mil = 2
    }
}

439.1.1 = {
	monarch = {
 		name = "Yarnag"
		dynasty = "gro-Graalug"
		birth_date = 406.1.1
		adm = 1
		dip = 4
		mil = 6
    }
}

513.1.1 = {
	monarch = {
 		name = "Razgurug"
		dynasty = "gro-Skulzak"
		birth_date = 460.1.1
		adm = 2
		dip = 5
		mil = 0
    }
}

610.1.1 = {
	monarch = {
 		name = "Gulza"
		dynasty = "gro-Zhasim"
		birth_date = 570.1.1
		adm = 1
		dip = 5
		mil = 4
		female = yes
    }
}

651.1.1 = {
	monarch = {
 		name = "Rolfikha"
		dynasty = "gro-Garnikh"
		birth_date = 625.1.1
		adm = 6
		dip = 4
		mil = 1
		female = yes
    }
}

697.1.1 = {
	monarch = {
 		name = "Lugulg"
		dynasty = "gro-Orgush"
		birth_date = 662.1.1
		adm = 4
		dip = 3
		mil = 4
    }
}

755.1.1 = {
	monarch = {
 		name = "Ghamulg"
		dynasty = "gro-Ramorgol"
		birth_date = 716.1.1
		adm = 2
		dip = 2
		mil = 1
    }
}

802.1.1 = {
	monarch = {
 		name = "Zagrugh"
		dynasty = "gro-Agrobal"
		birth_date = 765.1.1
		adm = 1
		dip = 1
		mil = 5
    }
}

891.1.1 = {
	monarch = {
 		name = "Roku"
		dynasty = "gro-Snikhbat"
		birth_date = 866.1.1
		adm = 6
		dip = 1
		mil = 1
		female = yes
    }
}

982.1.1 = {
	monarch = {
 		name = "Lugrub"
		dynasty = "gro-Zagrugh"
		birth_date = 942.1.1
		adm = 4
		dip = 0
		mil = 5
    }
}

1052.1.1 = {
	monarch = {
 		name = "Azulga"
		dynasty = "gro-Ugorz"
		birth_date = 999.1.1
		adm = 6
		dip = 1
		mil = 6
		female = yes
    }
}

1088.1.1 = {
	monarch = {
 		name = "Zagh"
		dynasty = "gro-Umurn"
		birth_date = 1059.1.1
		adm = 4
		dip = 0
		mil = 3
    }
}

1151.1.1 = {
	monarch = {
 		name = "Ghornugag"
		dynasty = "gro-Othohoth"
		birth_date = 1117.1.1
		adm = 3
		dip = 6
		mil = 0
    }
}

1230.1.1 = {
	monarch = {
 		name = "Zulgukh"
		dynasty = "gro-Vulmon"
		birth_date = 1203.1.1
		adm = 1
		dip = 6
		mil = 3
    }
}

1318.1.1 = {
	monarch = {
 		name = "Rulfuna"
		dynasty = "gro-Ilthag"
		birth_date = 1283.1.1
		adm = 6
		dip = 5
		mil = 0
		female = yes
    }
}

1409.1.1 = {
	monarch = {
 		name = "Luz"
		dynasty = "gro-Gladba"
		birth_date = 1368.1.1
		adm = 4
		dip = 4
		mil = 4
    }
}

1505.1.1 = {
	monarch = {
 		name = "Bashgara"
		dynasty = "gro-Dragom"
		birth_date = 1460.1.1
		adm = 3
		dip = 3
		mil = 0
		female = yes
    }
}

1571.1.1 = {
	monarch = {
 		name = "Zulbek"
		dynasty = "gro-Snagh"
		birth_date = 1532.1.1
		adm = 1
		dip = 3
		mil = 4
    }
}

1619.1.1 = {
	monarch = {
 		name = "Shakharg"
		dynasty = "gro-Rogurog"
		birth_date = 1595.1.1
		adm = 3
		dip = 4
		mil = 5
    }
}

1694.1.1 = {
	monarch = {
 		name = "Lurgonash"
		dynasty = "gro-Luzmash"
		birth_date = 1645.1.1
		adm = 1
		dip = 3
		mil = 2
    }
}

1735.1.1 = {
	monarch = {
 		name = "Shabon"
		dynasty = "gro-Dur"
		birth_date = 1706.1.1
		adm = 6
		dip = 2
		mil = 6
		female = yes
    }
}

1790.1.1 = {
	monarch = {
 		name = "Margog"
		dynasty = "gro-Ulmamug"
		birth_date = 1742.1.1
		adm = 4
		dip = 1
		mil = 2
    }
}

1846.1.1 = {
	monarch = {
 		name = "Glaz"
		dynasty = "gro-Kharag"
		birth_date = 1799.1.1
		adm = 3
		dip = 1
		mil = 6
    }
}

1889.1.1 = {
	monarch = {
 		name = "Agrobal"
		dynasty = "gro-Snakh"
		birth_date = 1841.1.1
		adm = 1
		dip = 0
		mil = 3
    }
}

1973.1.1 = {
	monarch = {
 		name = "Shabeg"
		dynasty = "gro-Balzag"
		birth_date = 1926.1.1
		adm = 6
		dip = 6
		mil = 6
		female = yes
    }
}

2031.1.1 = {
	monarch = {
 		name = "Kora"
		dynasty = "gro-Uggnath"
		birth_date = 1987.1.1
		adm = 4
		dip = 5
		mil = 3
		female = yes
    }
}

2073.1.1 = {
	monarch = {
 		name = "Bizra"
		dynasty = "gro-Ghromrash"
		birth_date = 2051.1.1
		adm = 6
		dip = 6
		mil = 5
		female = yes
    }
}

2127.1.1 = {
	monarch = {
 		name = "Aghurz"
		dynasty = "gro-Ordooth"
		birth_date = 2103.1.1
		adm = 5
		dip = 6
		mil = 1
    }
}

2200.1.1 = {
	monarch = {
 		name = "Glurdag"
		dynasty = "gro-Kirgut"
		birth_date = 2164.1.1
		adm = 3
		dip = 5
		mil = 5
    }
}

2269.1.1 = {
	monarch = {
 		name = "Azrath"
		dynasty = "gro-Gruluk"
		birth_date = 2238.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2327.1.1 = {
	monarch = {
 		name = "Shaldagan"
		dynasty = "gro-Lozruth"
		birth_date = 2275.1.1
		adm = 6
		dip = 3
		mil = 5
		female = yes
    }
}

2383.1.1 = {
	monarch = {
 		name = "Megorz"
		dynasty = "gro-Lurbozog"
		birth_date = 2337.1.1
		adm = 5
		dip = 3
		mil = 2
    }
}

2470.1.1 = {
	monarch = {
 		name = "Glundeg"
		dynasty = "gro-Kurz"
		birth_date = 2437.1.1
		adm = 3
		dip = 2
		mil = 5
    }
}

