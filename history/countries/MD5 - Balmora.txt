government = tribal
government_rank = 3
mercantilism = 1
technology_group = exiled_tg
religion = chimer_pantheon
primary_culture = ashlander_chimer
capital = 1045

54.1.1 = {
	monarch = {
 		name = "Tunipy"
		dynasty = "Zansatanit"
		birth_date = 2.1.1
		adm = 0
		dip = 3
		mil = 6
		female = yes
    }
}

89.1.1 = {
	monarch = {
 		name = "Patababi"
		dynasty = "Assarnuridan"
		birth_date = 50.1.1
		adm = 5
		dip = 2
		mil = 3
    }
}

176.1.1 = {
	monarch = {
 		name = "Hemus"
		dynasty = "Kaushmamanu"
		birth_date = 135.1.1
		adm = 3
		dip = 1
		mil = 0
    }
}

211.1.1 = {
	monarch = {
 		name = "Ahanabi"
		dynasty = "Assarbeberib"
		birth_date = 175.1.1
		adm = 1
		dip = 0
		mil = 3
		female = yes
    }
}

310.1.1 = {
	monarch = {
 		name = "Ulabael"
		dynasty = "Assirnarari"
		birth_date = 257.1.1
		adm = 0
		dip = 6
		mil = 0
    }
}

365.1.1 = {
	monarch = {
 		name = "Patababi"
		dynasty = "Urshummarnamus"
		birth_date = 340.1.1
		adm = 5
		dip = 6
		mil = 4
    }
}

434.1.1 = {
	monarch = {
 		name = "Ibanammu"
		dynasty = "Massitisun"
		birth_date = 382.1.1
		adm = 0
		dip = 0
		mil = 5
		female = yes
    }
}

475.1.1 = {
	monarch = {
 		name = "Elitlaya"
		dynasty = "Addaribantes"
		birth_date = 440.1.1
		adm = 5
		dip = 6
		mil = 2
		female = yes
    }
}

520.1.1 = {
	monarch = {
 		name = "Zelay"
		dynasty = "Anurnudai"
		birth_date = 471.1.1
		adm = 1
		dip = 0
		mil = 4
    }
}

568.1.1 = {
	monarch = {
 		name = "Shin"
		dynasty = "Santumatus"
		birth_date = 533.1.1
		adm = 6
		dip = 0
		mil = 1
    }
}

655.1.1 = {
	monarch = {
 		name = "Massarapal"
		dynasty = "Matluberib"
		birth_date = 619.1.1
		adm = 4
		dip = 6
		mil = 4
    }
}

731.1.1 = {
	monarch = {
 		name = "Dakin"
		dynasty = "Ashun-Idantus"
		birth_date = 682.1.1
		adm = 6
		dip = 0
		mil = 6
    }
}

782.1.1 = {
	monarch = {
 		name = "Zelay"
		dynasty = "Ashalkimallit"
		birth_date = 756.1.1
		adm = 4
		dip = 6
		mil = 3
    }
}

845.1.1 = {
	monarch = {
 		name = "Dunsalipal"
		dynasty = "Odin-Ahhe"
		birth_date = 800.1.1
		adm = 3
		dip = 5
		mil = 6
    }
}

894.1.1 = {
	monarch = {
 		name = "Zennammu"
		dynasty = "Erushara"
		birth_date = 841.1.1
		adm = 1
		dip = 5
		mil = 3
    }
}

949.1.1 = {
	monarch = {
 		name = "Shishi"
		dynasty = "Kaushad"
		birth_date = 905.1.1
		adm = 6
		dip = 4
		mil = 0
		female = yes
    }
}

1032.1.1 = {
	monarch = {
 		name = "Mimanu"
		dynasty = "Ilurnubishpal"
		birth_date = 1009.1.1
		adm = 4
		dip = 3
		mil = 3
		female = yes
    }
}

1080.1.1 = {
	monarch = {
 		name = "Elumabi"
		dynasty = "Assullinbanud"
		birth_date = 1057.1.1
		adm = 3
		dip = 2
		mil = 0
		female = yes
    }
}

1179.1.1 = {
	monarch = {
 		name = "Zennammu"
		dynasty = "Ashunbabi"
		birth_date = 1140.1.1
		adm = 1
		dip = 2
		mil = 4
    }
}

1274.1.1 = {
	monarch = {
 		name = "Shirerib"
		dynasty = "Massitisun"
		birth_date = 1222.1.1
		adm = 3
		dip = 3
		mil = 5
    }
}

1363.1.1 = {
	monarch = {
 		name = "Minassour"
		dynasty = "Samma"
		birth_date = 1341.1.1
		adm = 1
		dip = 2
		mil = 2
    }
}

1462.1.1 = {
	monarch = {
 		name = "Sul-Matuul"
		dynasty = "Addarari"
		birth_date = 1432.1.1
		adm = 6
		dip = 1
		mil = 5
    }
}

1512.1.1 = {
	monarch = {
 		name = "Missamsi"
		dynasty = "Pudashara"
		birth_date = 1478.1.1
		adm = 4
		dip = 0
		mil = 2
		female = yes
    }
}

1573.1.1 = {
	monarch = {
 		name = "Emul-Ran"
		dynasty = "Assalatammis"
		birth_date = 1536.1.1
		adm = 3
		dip = 0
		mil = 6
    }
}

1623.1.1 = {
	monarch = {
 		name = "Truan"
		dynasty = "Shand"
		birth_date = 1584.1.1
		adm = 3
		dip = 4
		mil = 4
    }
}

1662.1.1 = {
	monarch = {
 		name = "Asharapli"
		dynasty = "Saddarnuran"
		birth_date = 1611.1.1
		adm = 2
		dip = 3
		mil = 0
		female = yes
    }
}

1741.1.1 = {
	monarch = {
 		name = "Yapal"
		dynasty = "Asserrumusa"
		birth_date = 1700.1.1
		adm = 0
		dip = 2
		mil = 4
    }
}

1817.1.1 = {
	monarch = {
 		name = "Rasamsi"
		dynasty = "Subaddamael"
		birth_date = 1769.1.1
		adm = 5
		dip = 1
		mil = 1
		female = yes
    }
}

1862.1.1 = {
	monarch = {
 		name = "Ilasour"
		dynasty = "Ashunbabi"
		birth_date = 1822.1.1
		adm = 3
		dip = 1
		mil = 4
    }
}

1934.1.1 = {
	monarch = {
 		name = "Anit"
		dynasty = "Serdimapal"
		birth_date = 1883.1.1
		adm = 2
		dip = 0
		mil = 1
    }
}

2033.1.1 = {
	monarch = {
 		name = "Ulisamsi"
		dynasty = "Yassabisun"
		birth_date = 1983.1.1
		adm = 4
		dip = 1
		mil = 2
		female = yes
    }
}

2112.1.1 = {
	monarch = {
 		name = "Sal"
		dynasty = "Serimilk"
		birth_date = 2085.1.1
		adm = 2
		dip = 0
		mil = 6
    }
}

2186.1.1 = {
	monarch = {
 		name = "Yen"
		dynasty = "Atinsabia"
		birth_date = 2166.1.1
		adm = 0
		dip = 6
		mil = 3
    }
}

2259.1.1 = {
	monarch = {
 		name = "Salmat"
		dynasty = "Yansurnummu"
		birth_date = 2212.1.1
		adm = 5
		dip = 6
		mil = 6
    }
}

2295.1.1 = {
	monarch = {
 		name = "Kanud"
		dynasty = "Esatliballit"
		birth_date = 2275.1.1
		adm = 4
		dip = 5
		mil = 3
    }
}

2365.1.1 = {
	monarch = {
 		name = "Ashibaal"
		dynasty = "Santumatus"
		birth_date = 2341.1.1
		adm = 2
		dip = 4
		mil = 0
    }
}

2431.1.1 = {
	monarch = {
 		name = "Yeherradad"
		dynasty = "Assunbahanammu"
		birth_date = 2379.1.1
		adm = 0
		dip = 3
		mil = 3
    }
}

