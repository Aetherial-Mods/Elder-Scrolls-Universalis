government = monarchy
government_rank = 1
mercantilism = 1
technology_group = elven_tg
religion = altmeri_pantheon
primary_culture = altmer
capital = 4746
# = 4746

54.1.1 = {
	monarch = {
 		name = "Cironire"
		dynasty = "Bel-Shattered"
		birth_date = 8.1.1
		adm = 2
		dip = 3
		mil = 4
    }
}

121.1.1 = {
	monarch = {
 		name = "Trallilrion"
		dynasty = "Bel-Vareya"
		birth_date = 101.1.1
		adm = 0
		dip = 2
		mil = 1
    }
	queen = {
 		name = "Eilelmil"
		dynasty = "Bel-Vareya"
		birth_date = 79.1.1
		adm = 4
		dip = 1
		mil = 0
		female = yes
    }
	heir = {
 		name = "Taredinion"
		monarch_name = "Taredinion I"
		dynasty = "Bel-Vareya"
		birth_date = 109.1.1
		death_date = 189.1.1
		claim = 100
		adm = 1
		dip = 0
		mil = 5
    }
}

189.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Nedoril"
		monarch_name = "Nedoril I"
		dynasty = "Ael-Relnnarre"
		birth_date = 188.1.1
		death_date = 206.1.1
		claim = 100
		adm = 6
		dip = 6
		mil = 2
    }
}

206.1.1 = {
	monarch = {
 		name = "Cirimion"
		dynasty = "Ael-Minoirdalin"
		birth_date = 188.1.1
		adm = 2
		dip = 0
		mil = 5
    }
}

280.1.1 = {
	monarch = {
 		name = "Tinaducil"
		dynasty = "Wel-Adiane"
		birth_date = 232.1.1
		adm = 0
		dip = 6
		mil = 1
    }
}

371.1.1 = {
	monarch = {
 		name = "Numithir"
		dynasty = "Wel-Celrith"
		birth_date = 346.1.1
		adm = 2
		dip = 1
		mil = 3
    }
	queen = {
 		name = "Lireore"
		dynasty = "Wel-Celrith"
		birth_date = 348.1.1
		adm = 2
		dip = 4
		mil = 4
		female = yes
    }
}

436.1.1 = {
	monarch = {
 		name = "Valmir"
		dynasty = "Wel-Celae"
		birth_date = 389.1.1
		adm = 4
		dip = 5
		mil = 5
    }
	queen = {
 		name = "Telarniel"
		dynasty = "Wel-Celae"
		birth_date = 384.1.1
		adm = 4
		dip = 2
		mil = 6
		female = yes
    }
	heir = {
 		name = "Indure"
		monarch_name = "Indure I"
		dynasty = "Wel-Celae"
		birth_date = 432.1.1
		death_date = 519.1.1
		claim = 100
		adm = 1
		dip = 0
		mil = 5
    }
}

519.1.1 = {
	monarch = {
 		name = "Vareldur"
		dynasty = "Wel-Aryria"
		birth_date = 470.1.1
		adm = 1
		dip = 4
		mil = 6
    }
	queen = {
 		name = "Erranil"
		dynasty = "Wel-Aryria"
		birth_date = 486.1.1
		adm = 5
		dip = 2
		mil = 5
		female = yes
    }
	heir = {
 		name = "Tyermaillin"
		monarch_name = "Tyermaillin I"
		dynasty = "Wel-Aryria"
		birth_date = 514.1.1
		death_date = 614.1.1
		claim = 100
		adm = 1
		dip = 1
		mil = 4
    }
}

614.1.1 = {
	monarch = {
 		name = "Lernyelel"
		dynasty = "Ael-Relnnarre"
		birth_date = 588.1.1
		adm = 0
		dip = 0
		mil = 0
    }
}

659.1.1 = {
	monarch = {
 		name = "Nafarion"
		dynasty = "Ael-Miriath"
		birth_date = 607.1.1
		adm = 5
		dip = 6
		mil = 4
    }
}

742.1.1 = {
	monarch = {
 		name = "Findarninur"
		dynasty = "Wel-Aryria"
		birth_date = 702.1.1
		adm = 3
		dip = 5
		mil = 0
    }
}

793.1.1 = {
	monarch = {
 		name = "Aringoth"
		dynasty = "Ael-Relen"
		birth_date = 766.1.1
		adm = 1
		dip = 5
		mil = 4
    }
}

863.1.1 = {
	monarch = {
 		name = "Talerion"
		dynasty = "Ael-Nirae"
		birth_date = 813.1.1
		adm = 0
		dip = 4
		mil = 1
    }
}

961.1.1 = {
	monarch = {
 		name = "Naarwe"
		dynasty = "Ael-Lillandril"
		birth_date = 913.1.1
		adm = 5
		dip = 3
		mil = 4
    }
}

1055.1.1 = {
	monarch = {
 		name = "Fiiriel"
		dynasty = "Ael-Mirlenya"
		birth_date = 1017.1.1
		adm = 3
		dip = 2
		mil = 1
    }
}

1114.1.1 = {
	monarch = {
 		name = "Arfanel"
		dynasty = "Wel-Anaedan"
		birth_date = 1068.1.1
		adm = 5
		dip = 4
		mil = 3
    }
}

1206.1.1 = {
	monarch = {
 		name = "Sortarione"
		dynasty = "Ael-Kingshaven"
		birth_date = 1182.1.1
		adm = 3
		dip = 3
		mil = 6
		female = yes
    }
}

1280.1.1 = {
	monarch = {
 		name = "Aramil"
		dynasty = "Bel-Slaughter"
		birth_date = 1258.1.1
		adm = 1
		dip = 2
		mil = 3
		female = yes
    }
}

1345.1.1 = {
	monarch = {
 		name = "Tanulad"
		dynasty = "Wel-Elate"
		birth_date = 1306.1.1
		adm = 0
		dip = 1
		mil = 0
    }
	queen = {
 		name = "Cirtelcare"
		dynasty = "Wel-Elate"
		birth_date = 1323.1.1
		adm = 4
		dip = 0
		mil = 6
		female = yes
    }
}

1432.1.1 = {
	monarch = {
 		name = "Astanya"
		dynasty = "Wel-Alkinosin"
		birth_date = 1409.1.1
		adm = 2
		dip = 6
		mil = 2
		female = yes
    }
}

1474.1.1 = {
	monarch = {
 		name = "Tilanfire"
		dynasty = "Bel-Saelinoth"
		birth_date = 1444.1.1
		adm = 0
		dip = 5
		mil = 6
		female = yes
    }
}

1570.1.1 = {
	monarch = {
 		name = "Nulion"
		dynasty = "Ael-Felballin"
		birth_date = 1517.1.1
		adm = 5
		dip = 4
		mil = 2
    }
	queen = {
 		name = "Alerume"
		dynasty = "Ael-Felballin"
		birth_date = 1520.1.1
		adm = 2
		dip = 3
		mil = 1
		female = yes
    }
	heir = {
 		name = "Naramin"
		monarch_name = "Naramin I"
		dynasty = "Ael-Felballin"
		birth_date = 1569.1.1
		death_date = 1642.1.1
		claim = 100
		adm = 6
		dip = 1
		mil = 0
    }
}

1642.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Foloril"
		monarch_name = "Foloril I"
		dynasty = "Wel-Angaelle"
		birth_date = 1641.1.1
		death_date = 1659.1.1
		claim = 100
		adm = 4
		dip = 1
		mil = 4
    }
}

1659.1.1 = {
	monarch = {
 		name = "Tildulon"
		dynasty = "Bel-Saelinoth"
		birth_date = 1631.1.1
		adm = 4
		dip = 4
		mil = 4
    }
	queen = {
 		name = "Sintilfalion"
		dynasty = "Bel-Saelinoth"
		birth_date = 1630.1.1
		adm = 4
		dip = 0
		mil = 5
		female = yes
    }
	heir = {
 		name = "Ernandel"
		monarch_name = "Ernandel I"
		dynasty = "Bel-Saelinoth"
		birth_date = 1652.1.1
		death_date = 1698.1.1
		claim = 100
		adm = 1
		dip = 6
		mil = 4
		female = yes
    }
}

1698.1.1 = {
	monarch = {
 		name = "Turalla"
		dynasty = "Bel-Ronassa"
		birth_date = 1668.1.1
		adm = 0
		dip = 2
		mil = 5
		female = yes
    }
}

1763.1.1 = {
	monarch = {
 		name = "Ohtimbar"
		dynasty = "Bel-Valaena"
		birth_date = 1717.1.1
		adm = 5
		dip = 2
		mil = 1
    }
	queen = {
 		name = "Amilya"
		dynasty = "Bel-Valaena"
		birth_date = 1724.1.1
		adm = 2
		dip = 0
		mil = 0
		female = yes
    }
}

1818.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Condalin"
		monarch_name = "Condalin I"
		dynasty = "Bel-Terna"
		birth_date = 1818.1.1
		death_date = 1836.1.1
		claim = 100
		adm = 3
		dip = 4
		mil = 5
    }
}

1836.1.1 = {
	monarch = {
 		name = "Qamtir"
		dynasty = "Wel-Alinor"
		birth_date = 1808.1.1
		adm = 6
		dip = 6
		mil = 0
    }
}

1917.1.1 = {
	monarch = {
 		name = "Henenme"
		dynasty = "Ael-Felballin"
		birth_date = 1883.1.1
		adm = 4
		dip = 5
		mil = 4
		female = yes
    }
}

1955.1.1 = {
	monarch = {
 		name = "Castewe"
		dynasty = "Wel-Alkinosin"
		birth_date = 1911.1.1
		adm = 2
		dip = 4
		mil = 1
		female = yes
    }
	queen = {
 		name = "Carecalmo"
		dynasty = "Wel-Alkinosin"
		birth_date = 1914.1.1
		adm = 6
		dip = 2
		mil = 0
    }
	heir = {
 		name = "Colmorion"
		monarch_name = "Colmorion I"
		dynasty = "Wel-Alkinosin"
		birth_date = 1953.1.1
		death_date = 2043.1.1
		claim = 100
		adm = 3
		dip = 1
		mil = 6
    }
}

2043.1.1 = {
	monarch = {
 		name = "Ornuune"
		dynasty = "Wel-Ardatha"
		birth_date = 1998.1.1
		adm = 2
		dip = 4
		mil = 6
		female = yes
    }
	queen = {
 		name = "Nirulmo"
		dynasty = "Wel-Ardatha"
		birth_date = 2016.1.1
		adm = 3
		dip = 1
		mil = 0
    }
	heir = {
 		name = "Camandil"
		monarch_name = "Camandil I"
		dynasty = "Wel-Ardatha"
		birth_date = 2040.1.1
		death_date = 2084.1.1
		claim = 100
		adm = 0
		dip = 6
		mil = 6
		female = yes
    }
}

2084.1.1 = {
	monarch = {
 		name = "Relequen"
		dynasty = "Wel-Eleaninde"
		birth_date = 2033.1.1
		adm = 6
		dip = 3
		mil = 6
    }
	queen = {
 		name = "Arnarra"
		dynasty = "Wel-Eleaninde"
		birth_date = 2035.1.1
		adm = 3
		dip = 1
		mil = 5
		female = yes
    }
}

2144.1.1 = {
	monarch = {
 		name = "Ancarive"
		dynasty = "Wel-Camiana"
		birth_date = 2108.1.1
		adm = 1
		dip = 1
		mil = 2
    }
}

2206.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Veraye"
		monarch_name = "Veraye I"
		dynasty = "Ael-Lillandril"
		birth_date = 2199.1.1
		death_date = 2217.1.1
		claim = 100
		adm = 2
		dip = 5
		mil = 0
		female = yes
    }
}

2217.1.1 = {
	monarch = {
 		name = "Mercano"
		dynasty = "Bel-Terna"
		birth_date = 2182.1.1
		adm = 5
		dip = 6
		mil = 2
    }
	queen = {
 		name = "Undwelda"
		dynasty = "Bel-Terna"
		birth_date = 2184.1.1
		adm = 1
		dip = 5
		mil = 1
		female = yes
    }
}

2306.1.1 = {
	monarch = {
 		name = "Tandil"
		dynasty = "Ael-Relte"
		birth_date = 2285.1.1
		adm = 0
		dip = 4
		mil = 5
    }
}

2361.1.1 = {
	monarch = {
 		name = "Medora"
		dynasty = "Bel-Sontarya"
		birth_date = 2337.1.1
		adm = 5
		dip = 3
		mil = 1
		female = yes
    }
}

2450.1.1 = {
	monarch = {
 		name = "Firisar"
		dynasty = "Bel-Sondnwe"
		birth_date = 2425.1.1
		adm = 3
		dip = 2
		mil = 5
    }
	queen = {
 		name = "Corelanya"
		dynasty = "Bel-Sondnwe"
		birth_date = 2407.1.1
		adm = 0
		dip = 1
		mil = 4
		female = yes
    }
	heir = {
 		name = "Faladil"
		monarch_name = "Faladil I"
		dynasty = "Bel-Sondnwe"
		birth_date = 2441.1.1
		death_date = 2526.1.1
		claim = 100
		adm = 4
		dip = 6
		mil = 3
    }
}

