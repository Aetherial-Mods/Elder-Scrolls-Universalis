government = tribal
government_rank = 5
mercantilism = 1
technology_group = northern_tg
religion = old_gods_cult
primary_culture = kreathmen
capital = 1291

54.1.1 = {
	monarch = {
 		name = "Bauvmith"
		dynasty = "Tudaarin"
		birth_date = 22.1.1
		adm = 6
		dip = 3
		mil = 3
    }
}

129.1.1 = {
	monarch = {
 		name = "Heva"
		dynasty = "Letan"
		birth_date = 103.1.1
		adm = 4
		dip = 3
		mil = 6
		female = yes
    }
}

204.1.1 = {
	monarch = {
 		name = "Runhyrac"
		dynasty = "Ludec"
		birth_date = 167.1.1
		adm = 2
		dip = 2
		mil = 3
    }
}

290.1.1 = {
	monarch = {
 		name = "Shelren"
		dynasty = "Desylis"
		birth_date = 251.1.1
		adm = 1
		dip = 1
		mil = 0
    }
}

348.1.1 = {
	monarch = {
 		name = "Lukaazon"
		dynasty = "Dauhrak"
		birth_date = 312.1.1
		adm = 6
		dip = 0
		mil = 3
    }
}

419.1.1 = {
	monarch = {
 		name = "Heva"
		dynasty = "Ralreloc"
		birth_date = 383.1.1
		adm = 0
		dip = 4
		mil = 1
		female = yes
    }
}

484.1.1 = {
	monarch = {
 		name = "Levadaeh"
		dynasty = "Shelren"
		birth_date = 444.1.1
		adm = 5
		dip = 4
		mil = 5
		female = yes
    }
}

579.1.1 = {
	monarch = {
 		name = "Tirlaasir"
		dynasty = "Zyrdusor"
		birth_date = 536.1.1
		adm = 3
		dip = 3
		mil = 1
    }
}

628.1.1 = {
	monarch = {
 		name = "Zathi"
		dynasty = "Hetok"
		birth_date = 603.1.1
		adm = 1
		dip = 2
		mil = 5
		female = yes
    }
}

676.1.1 = {
	monarch = {
 		name = "Viladuh"
		dynasty = "Nharmok"
		birth_date = 630.1.1
		adm = 0
		dip = 1
		mil = 2
		female = yes
    }
}

726.1.1 = {
	monarch = {
 		name = "Nathael"
		dynasty = "Likoth"
		birth_date = 704.1.1
		adm = 1
		dip = 2
		mil = 3
		female = yes
    }
}

817.1.1 = {
	monarch = {
 		name = "Mimohu"
		dynasty = "Hetok"
		birth_date = 770.1.1
		adm = 0
		dip = 2
		mil = 0
		female = yes
    }
}

901.1.1 = {
	monarch = {
 		name = "Zosev"
		dynasty = "Demralis"
		birth_date = 855.1.1
		adm = 5
		dip = 1
		mil = 4
		female = yes
    }
}

979.1.1 = {
	monarch = {
 		name = "Lotemu"
		dynasty = "Ridravin"
		birth_date = 927.1.1
		adm = 3
		dip = 0
		mil = 0
		female = yes
    }
}

1062.1.1 = {
	monarch = {
 		name = "Tudaarin"
		dynasty = "Tykisok"
		birth_date = 1044.1.1
		adm = 1
		dip = 6
		mil = 4
    }
}

1122.1.1 = {
	monarch = {
 		name = "Dyrmec"
		dynasty = "Gaohen"
		birth_date = 1077.1.1
		adm = 0
		dip = 5
		mil = 1
    }
}

1178.1.1 = {
	monarch = {
 		name = "Virdanath"
		dynasty = "Bakumoth"
		birth_date = 1133.1.1
		adm = 5
		dip = 5
		mil = 4
    }
}

1273.1.1 = {
	monarch = {
 		name = "Herlir"
		dynasty = "Remaasok"
		birth_date = 1227.1.1
		adm = 3
		dip = 4
		mil = 1
    }
}

1323.1.1 = {
	monarch = {
 		name = "Tudaarin"
		dynasty = "Haonuvic"
		birth_date = 1303.1.1
		adm = 5
		dip = 5
		mil = 2
    }
}

1373.1.1 = {
	monarch = {
 		name = "Sonev"
		dynasty = "Vrilak"
		birth_date = 1342.1.1
		adm = 3
		dip = 4
		mil = 6
		female = yes
    }
}

1442.1.1 = {
	monarch = {
 		name = "Sarul"
		dynasty = "Vurmesoth"
		birth_date = 1395.1.1
		adm = 2
		dip = 4
		mil = 3
		female = yes
    }
}

1511.1.1 = {
	monarch = {
 		name = "Vurmesoth"
		dynasty = "Zerlec"
		birth_date = 1490.1.1
		adm = 0
		dip = 3
		mil = 6
    }
}

1601.1.1 = {
	monarch = {
 		name = "Bardek"
		dynasty = "Nhekhedin"
		birth_date = 1572.1.1
		adm = 5
		dip = 2
		mil = 3
    }
}

1690.1.1 = {
	monarch = {
 		name = "Hotedi"
		dynasty = "Gikamik"
		birth_date = 1650.1.1
		adm = 3
		dip = 1
		mil = 0
		female = yes
    }
}

1750.1.1 = {
	monarch = {
 		name = "Sarul"
		dynasty = "Malrasoc"
		birth_date = 1731.1.1
		adm = 2
		dip = 0
		mil = 3
		female = yes
    }
}

1818.1.1 = {
	monarch = {
 		name = "Retobil"
		dynasty = "Riralath"
		birth_date = 1787.1.1
		adm = 0
		dip = 0
		mil = 0
		female = yes
    }
}

1916.1.1 = {
	monarch = {
 		name = "Bardek"
		dynasty = "Naanuzar"
		birth_date = 1864.1.1
		adm = 2
		dip = 1
		mil = 1
    }
}

2003.1.1 = {
	monarch = {
 		name = "Hotedi"
		dynasty = "Lukaazon"
		birth_date = 1966.1.1
		adm = 0
		dip = 0
		mil = 5
		female = yes
    }
}

2052.1.1 = {
	monarch = {
 		name = "Myhros"
		dynasty = "Dauhrak"
		birth_date = 2026.1.1
		adm = 5
		dip = 6
		mil = 2
    }
}

2136.1.1 = {
	monarch = {
 		name = "Vrakir"
		dynasty = "Vinhylak"
		birth_date = 2094.1.1
		adm = 3
		dip = 5
		mil = 5
    }
}

2221.1.1 = {
	monarch = {
 		name = "Vredraonir"
		dynasty = "Tylak"
		birth_date = 2200.1.1
		adm = 2
		dip = 5
		mil = 2
    }
}

2315.1.1 = {
	monarch = {
 		name = "Nhystareth"
		dynasty = "Tadak"
		birth_date = 2262.1.1
		adm = 0
		dip = 4
		mil = 6
    }
}

2370.1.1 = {
	monarch = {
 		name = "Myhros"
		dynasty = "Haukos"
		birth_date = 2345.1.1
		adm = 5
		dip = 3
		mil = 2
    }
}

2431.1.1 = {
	monarch = {
 		name = "Letan"
		dynasty = "Nestith"
		birth_date = 2396.1.1
		adm = 3
		dip = 2
		mil = 6
    }
}

2482.1.1 = {
	monarch = {
 		name = "Lysteth"
		dynasty = "Vristidok"
		birth_date = 2431.1.1
		adm = 5
		dip = 4
		mil = 1
    }
}

