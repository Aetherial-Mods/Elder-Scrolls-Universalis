government = theocracy
government_rank = 1
mercantilism = 1
technology_group = akavir_tg
religion = tsaesci_pantheon
primary_culture = tsaesci
capital = 715
# = 715

54.1.1 = {
	monarch = {
 		name = "Zokaaxax"
		dynasty = "Tson'Uzho"
		birth_date = 25.1.1
		adm = 0
		dip = 4
		mil = 4
    }
}

127.1.1 = {
	monarch = {
 		name = "Rehnesh"
		dynasty = "Tzon'Ghihzaz"
		birth_date = 84.1.1
		adm = 5
		dip = 3
		mil = 1
    }
}

163.1.1 = {
	monarch = {
 		name = "Chuxou"
		dynasty = "Tson'Nakhe"
		birth_date = 137.1.1
		adm = 0
		dip = 5
		mil = 2
    }
}

204.1.1 = {
	monarch = {
 		name = "Thuacahsoq"
		dynasty = "Tzon'Evekku"
		birth_date = 155.1.1
		adm = 5
		dip = 4
		mil = 6
    }
}

259.1.1 = {
	monarch = {
 		name = "Oqakix"
		dynasty = "Tzon'Juhuke"
		birth_date = 230.1.1
		adm = 4
		dip = 3
		mil = 3
    }
}

303.1.1 = {
	monarch = {
 		name = "Nunnu"
		dynasty = "Tzon'Imerai"
		birth_date = 254.1.1
		adm = 2
		dip = 2
		mil = 6
		female = yes
    }
}

367.1.1 = {
	monarch = {
 		name = "Chuskashki"
		dynasty = "Tzon'Ivono"
		birth_date = 322.1.1
		adm = 0
		dip = 1
		mil = 3
		female = yes
    }
}

440.1.1 = {
	monarch = {
 		name = "Uxoz"
		dynasty = "Tzon'Ogzego"
		birth_date = 391.1.1
		adm = 5
		dip = 1
		mil = 0
    }
}

480.1.1 = {
	monarch = {
 		name = "Oqakix"
		dynasty = "Sson'Xogyauz"
		birth_date = 432.1.1
		adm = 4
		dip = 0
		mil = 3
    }
}

572.1.1 = {
	monarch = {
 		name = "Aakuc"
		dynasty = "Zson'Juvhahzihz"
		birth_date = 540.1.1
		adm = 2
		dip = 6
		mil = 0
    }
}

636.1.1 = {
	monarch = {
 		name = "Trezus"
		dynasty = "Sson'Chuxou"
		birth_date = 584.1.1
		adm = 3
		dip = 3
		mil = 5
		female = yes
    }
}

726.1.1 = {
	monarch = {
 		name = "Zehsicraut"
		dynasty = "Tzon'Nraullegrahs"
		birth_date = 685.1.1
		adm = 2
		dip = 0
		mil = 5
    }
}

810.1.1 = {
	monarch = {
 		name = "Yogshuth"
		dynasty = "Tson'Onezo"
		birth_date = 792.1.1
		adm = 0
		dip = 6
		mil = 2
    }
}

906.1.1 = {
	monarch = {
 		name = "Asuqhus"
		dynasty = "Zson'Jekki"
		birth_date = 866.1.1
		adm = 1
		dip = 3
		mil = 0
    }
}

950.1.1 = {
	monarch = {
 		name = "Xaaggas"
		dynasty = "Zson'Khukuhz"
		birth_date = 915.1.1
		adm = 6
		dip = 2
		mil = 3
    }
}

1043.1.1 = {
	monarch = {
 		name = "Thaxugret"
		dynasty = "Sson'Hikuashuth"
		birth_date = 1020.1.1
		adm = 4
		dip = 1
		mil = 0
    }
}

1119.1.1 = {
	monarch = {
 		name = "Ajout"
		dynasty = "Sson'Uaguzoq"
		birth_date = 1080.1.1
		adm = 3
		dip = 0
		mil = 4
    }
}

1215.1.1 = {
	monarch = {
 		name = "Thihnus"
		dynasty = "Zson'Kuhshammo"
		birth_date = 1184.1.1
		adm = 3
		dip = 5
		mil = 1
    }
}

1281.1.1 = {
	monarch = {
 		name = "Vouggignez"
		dynasty = "Zson'Ingrossi"
		birth_date = 1228.1.1
		adm = 2
		dip = 4
		mil = 5
		female = yes
    }
}

1376.1.1 = {
	monarch = {
 		name = "Zokaaxax"
		dynasty = "Sson'Asec"
		birth_date = 1333.1.1
		adm = 0
		dip = 3
		mil = 2
    }
}

1434.1.1 = {
	monarch = {
 		name = "Goman"
		dynasty = "Sson'Asec"
		birth_date = 1416.1.1
		adm = 5
		dip = 2
		mil = 5
		female = yes
    }
}

1502.1.1 = {
	monarch = {
 		name = "Chuxou"
		dynasty = "Zson'Ougunnuhz"
		birth_date = 1479.1.1
		adm = 3
		dip = 1
		mil = 2
    }
}

1571.1.1 = {
	monarch = {
 		name = "Thuacahsoq"
		dynasty = "Tzon'Zerish"
		birth_date = 1543.1.1
		adm = 2
		dip = 1
		mil = 6
    }
}

1611.1.1 = {
	monarch = {
 		name = "Zokaaxax"
		dynasty = "Tson'Anaza"
		birth_date = 1573.1.1
		adm = 0
		dip = 0
		mil = 2
    }
}

1650.1.1 = {
	monarch = {
 		name = "Goman"
		dynasty = "Tson'Chakah"
		birth_date = 1623.1.1
		adm = 2
		dip = 1
		mil = 4
		female = yes
    }
}

1724.1.1 = {
	monarch = {
 		name = "Krakhuakij"
		dynasty = "Tzon'Chizozro"
		birth_date = 1704.1.1
		adm = 0
		dip = 0
		mil = 1
		female = yes
    }
}

1798.1.1 = {
	monarch = {
 		name = "Uxoz"
		dynasty = "Tzon'Chinnia"
		birth_date = 1757.1.1
		adm = 5
		dip = 6
		mil = 4
    }
}

1837.1.1 = {
	monarch = {
 		name = "Oqakix"
		dynasty = "Tzon'Chizozro"
		birth_date = 1799.1.1
		adm = 4
		dip = 6
		mil = 1
    }
}

1910.1.1 = {
	monarch = {
 		name = "Nunnu"
		dynasty = "Sson'Xaaggas"
		birth_date = 1864.1.1
		adm = 2
		dip = 5
		mil = 5
		female = yes
    }
}

2002.1.1 = {
	monarch = {
 		name = "Chuskashki"
		dynasty = "Tzon'Uzshuhs"
		birth_date = 1952.1.1
		adm = 0
		dip = 4
		mil = 1
		female = yes
    }
}

2078.1.1 = {
	monarch = {
 		name = "Mrecho"
		dynasty = "Sson'Aakuc"
		birth_date = 2041.1.1
		adm = 5
		dip = 3
		mil = 5
		female = yes
    }
}

2175.1.1 = {
	monarch = {
 		name = "Hehsuaqus"
		dynasty = "Tson'Chihxeze"
		birth_date = 2155.1.1
		adm = 4
		dip = 2
		mil = 1
    }
}

2261.1.1 = {
	monarch = {
 		name = "Ghokizsha"
		dynasty = "Zson'Nrekij"
		birth_date = 2212.1.1
		adm = 5
		dip = 4
		mil = 3
		female = yes
    }
}

2313.1.1 = {
	monarch = {
 		name = "Shocaja"
		dynasty = "Sson'Nujaceq"
		birth_date = 2288.1.1
		adm = 4
		dip = 3
		mil = 0
    }
}

2375.1.1 = {
	monarch = {
 		name = "Azaa"
		dynasty = "Tson'Jekrigeh"
		birth_date = 2338.1.1
		adm = 2
		dip = 2
		mil = 3
    }
}

2423.1.1 = {
	monarch = {
 		name = "Mgongivu"
		dynasty = "Sson'Yucre"
		birth_date = 2386.1.1
		adm = 0
		dip = 1
		mil = 0
		female = yes
    }
}

