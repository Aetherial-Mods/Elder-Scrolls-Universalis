government = republic
government_rank = 1
mercantilism = 1
technology_group = elven_tg
religion = altmeri_pantheon
primary_culture = sinistral
capital = 3443

54.1.1 = {
	monarch = {
 		name = "Vingalmo"
		dynasty = "Spelluseus'enas"
		birth_date = 26.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

94.1.1 = {
	monarch = {
 		name = "Peregrine"
		dynasty = "Karnstern'aeas"
		birth_date = 53.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

163.1.1 = {
	monarch = {
 		name = "Linwirmion"
		dynasty = "Silsailen'enas"
		birth_date = 121.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

247.1.1 = {
	monarch = {
 		name = "Narandor"
		dynasty = "Ardende'avas"
		birth_date = 220.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

316.1.1 = {
	monarch = {
 		name = "Endannie"
		dynasty = "Highael'aeas"
		birth_date = 284.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

401.1.1 = {
	monarch = {
 		name = "Ateldil"
		dynasty = "Felballin'aeas"
		birth_date = 362.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

444.1.1 = {
	monarch = {
 		name = "Talirinde"
		dynasty = "Arannnarre'avas"
		birth_date = 397.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

487.1.1 = {
	monarch = {
 		name = "Meleril"
		dynasty = "Sondnwe'enas"
		birth_date = 451.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

583.1.1 = {
	monarch = {
 		name = "Telacar"
		dynasty = "Faeire'avas"
		birth_date = 563.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

677.1.1 = {
	monarch = {
 		name = "Minwolhil"
		dynasty = "Siriginia'enas"
		birth_date = 652.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

720.1.1 = {
	monarch = {
 		name = "Erissare"
		dynasty = "Taarine'enas"
		birth_date = 677.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

786.1.1 = {
	monarch = {
 		name = "Arcimil"
		dynasty = "Elsinthaer'avas"
		birth_date = 737.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

884.1.1 = {
	monarch = {
 		name = "Tarie"
		dynasty = "Erresse'avas"
		birth_date = 842.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

929.1.1 = {
	monarch = {
 		name = "Minorne"
		dynasty = "Belport'avas"
		birth_date = 883.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

982.1.1 = {
	monarch = {
 		name = "Erelin"
		dynasty = "Celae'avas"
		birth_date = 964.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1020.1.1 = {
	monarch = {
 		name = "Calbalion"
		dynasty = "Lorusse'aeas"
		birth_date = 984.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1117.1.1 = {
	monarch = {
 		name = "Alandil"
		dynasty = "Shimmerene'enas"
		birth_date = 1089.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1172.1.1 = {
	monarch = {
 		name = "Arnurdurwe"
		dynasty = "Graddun'aeas"
		birth_date = 1144.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1222.1.1 = {
	monarch = {
 		name = "Telyasawen"
		dynasty = "Elate'avas"
		birth_date = 1185.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1257.1.1 = {
	monarch = {
 		name = "Eslendore"
		dynasty = "Aryria'avas"
		birth_date = 1205.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1343.1.1 = {
	monarch = {
 		name = "Alanaire"
		dynasty = "Spelluseus'enas"
		birth_date = 1322.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1425.1.1 = {
	monarch = {
 		name = "Earlarume"
		dynasty = "Firsthold'aeas"
		birth_date = 1377.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1481.1.1 = {
	monarch = {
 		name = "Ancano"
		dynasty = "Sondnae'enas"
		birth_date = 1440.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1520.1.1 = {
	monarch = {
 		name = "Seridur"
		dynasty = "Caminalda'avas"
		birth_date = 1484.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1602.1.1 = {
	monarch = {
 		name = "Menuldhel"
		dynasty = "Shimmerene'enas"
		birth_date = 1576.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1665.1.1 = {
	monarch = {
 		name = "Falanaamo"
		dynasty = "Muulinde'aeas"
		birth_date = 1613.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1743.1.1 = {
	monarch = {
 		name = "Anarenen"
		dynasty = "Karnstern'aeas"
		birth_date = 1690.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1831.1.1 = {
	monarch = {
 		name = "Seanwen"
		dynasty = "Slaughter'enas"
		birth_date = 1790.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1874.1.1 = {
	monarch = {
 		name = "Lantelcare"
		dynasty = "Earrand'avas"
		birth_date = 1826.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1959.1.1 = {
	monarch = {
 		name = "Norarubel"
		dynasty = "Silsailen'enas"
		birth_date = 1911.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2053.1.1 = {
	monarch = {
 		name = "Heratir"
		dynasty = "Valia'enas"
		birth_date = 2008.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2103.1.1 = {
	monarch = {
 		name = "Celarus"
		dynasty = "Edala'avas"
		birth_date = 2068.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2140.1.1 = {
	monarch = {
 		name = "Ilaatysar"
		dynasty = "Sondsara'enas"
		birth_date = 2100.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2233.1.1 = {
	monarch = {
 		name = "Ciryarel"
		dynasty = "Lorusse'aeas"
		birth_date = 2187.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2310.1.1 = {
	monarch = {
 		name = "Tuinorion"
		dynasty = "Kardnar'aeas"
		birth_date = 2266.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2389.1.1 = {
	monarch = {
 		name = "Nuulehtel"
		dynasty = "Alinor'avas"
		birth_date = 2345.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2473.1.1 = {
	monarch = {
 		name = "Farwelayne"
		dynasty = "Mathiisen'aeas"
		birth_date = 2425.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

