government = native
government_rank = 1
mercantilism = 1
technology_group = marshan_tg
religion = hist
primary_culture = naga
capital = 6587

54.1.1 = {
	monarch = {
 		name = "Uazee"
		dynasty = "Otumei"
		birth_date = 26.1.1
		adm = 0
		dip = 4
		mil = 2
		female = yes
    }
}

153.1.1 = {
	monarch = {
 		name = "Nosaleeth"
		dynasty = "Nagdeseer"
		birth_date = 110.1.1
		adm = 5
		dip = 3
		mil = 6
    }
}

237.1.1 = {
	monarch = {
 		name = "Heir-Marza"
		dynasty = "Yelei"
		birth_date = 209.1.1
		adm = 3
		dip = 2
		mil = 2
    }
}

291.1.1 = {
	monarch = {
 		name = "Beshnus"
		dynasty = "Lafnaresh"
		birth_date = 251.1.1
		adm = 2
		dip = 1
		mil = 6
    }
}

358.1.1 = {
	monarch = {
 		name = "Tunbam-Na"
		dynasty = "Yelei"
		birth_date = 320.1.1
		adm = 0
		dip = 1
		mil = 2
		female = yes
    }
}

411.1.1 = {
	monarch = {
 		name = "Nodeeus"
		dynasty = "Caligeepa"
		birth_date = 393.1.1
		adm = 2
		dip = 2
		mil = 4
    }
}

483.1.1 = {
	monarch = {
 		name = "Heetzasi"
		dynasty = "Tibersifon"
		birth_date = 461.1.1
		adm = 0
		dip = 1
		mil = 1
    }
}

573.1.1 = {
	monarch = {
 		name = "Oleed-Meen"
		dynasty = "Antigoneeixth"
		birth_date = 538.1.1
		adm = 5
		dip = 0
		mil = 4
		female = yes
    }
}

644.1.1 = {
	monarch = {
 		name = "Im-Kajin"
		dynasty = "Casdorus"
		birth_date = 620.1.1
		adm = 4
		dip = 0
		mil = 1
    }
}

690.1.1 = {
	monarch = {
 		name = "Simeesh"
		dynasty = "Tikeerdeseer"
		birth_date = 649.1.1
		adm = 6
		dip = 1
		mil = 3
		female = yes
    }
}

740.1.1 = {
	monarch = {
 		name = "Mahei-Tei"
		dynasty = "Canimesh"
		birth_date = 709.1.1
		adm = 1
		dip = 2
		mil = 5
    }
}

780.1.1 = {
	monarch = {
 		name = "Dreevureesh"
		dynasty = "Cascalees"
		birth_date = 739.1.1
		adm = 6
		dip = 1
		mil = 1
    }
}

832.1.1 = {
	monarch = {
 		name = "Meenosh"
		dynasty = "Talen-Lan"
		birth_date = 810.1.1
		adm = 5
		dip = 1
		mil = 5
    }
}

874.1.1 = {
	monarch = {
 		name = "Ei-Etaku"
		dynasty = "Im-Kajin"
		birth_date = 831.1.1
		adm = 3
		dip = 0
		mil = 2
    }
}

932.1.1 = {
	monarch = {
 		name = "An-Dar"
		dynasty = "Nileesh"
		birth_date = 909.1.1
		adm = 1
		dip = 6
		mil = 5
		female = yes
    }
}

1004.1.1 = {
	monarch = {
 		name = "Skalasha"
		dynasty = "Meerhaj"
		birth_date = 968.1.1
		adm = 6
		dip = 5
		mil = 2
    }
}

1070.1.1 = {
	monarch = {
 		name = "Meenjee"
		dynasty = "Talen-Lan"
		birth_date = 1032.1.1
		adm = 5
		dip = 4
		mil = 6
    }
}

1147.1.1 = {
	monarch = {
 		name = "Ei-Ei"
		dynasty = "Mereemesh"
		birth_date = 1113.1.1
		adm = 3
		dip = 4
		mil = 2
    }
}

1224.1.1 = {
	monarch = {
 		name = "Anash"
		dynasty = "Casdorus"
		birth_date = 1190.1.1
		adm = 5
		dip = 5
		mil = 4
    }
}

1284.1.1 = {
	monarch = {
 		name = "Shuvu"
		dynasty = "Taierleesh"
		birth_date = 1262.1.1
		adm = 3
		dip = 4
		mil = 1
    }
}

1338.1.1 = {
	monarch = {
 		name = "Az-Maxath"
		dynasty = "Meerhaj"
		birth_date = 1316.1.1
		adm = 1
		dip = 3
		mil = 4
		female = yes
    }
}

1409.1.1 = {
	monarch = {
 		name = "Tar-Lurash"
		dynasty = "Saliz'k"
		birth_date = 1370.1.1
		adm = 0
		dip = 2
		mil = 1
		female = yes
    }
}

1493.1.1 = {
	monarch = {
 		name = "Mobareed"
		dynasty = "Pehrsar"
		birth_date = 1474.1.1
		adm = 5
		dip = 2
		mil = 5
    }
}

1548.1.1 = {
	monarch = {
 		name = "Heed-Meeus"
		dynasty = "Meeros"
		birth_date = 1521.1.1
		adm = 3
		dip = 1
		mil = 1
		female = yes
    }
}

1583.1.1 = {
	monarch = {
 		name = "Aseepa"
		dynasty = "Madeian"
		birth_date = 1559.1.1
		adm = 1
		dip = 0
		mil = 5
    }
}

1666.1.1 = {
	monarch = {
 		name = "Taleel-Bex"
		dynasty = "Im-Kajin"
		birth_date = 1623.1.1
		adm = 0
		dip = 6
		mil = 1
    }
}

1718.1.1 = {
	monarch = {
 		name = "Muz-Muz"
		dynasty = "Peteus"
		birth_date = 1686.1.1
		adm = 1
		dip = 1
		mil = 3
		female = yes
    }
}

1795.1.1 = {
	monarch = {
 		name = "Ereel-Dum"
		dynasty = "Antigoneeixth"
		birth_date = 1744.1.1
		adm = 0
		dip = 0
		mil = 0
    }
}

1843.1.1 = {
	monarch = {
 		name = "Muz-Shah"
		dynasty = "Xeirtius"
		birth_date = 1816.1.1
		adm = 5
		dip = 6
		mil = 3
    }
}

1886.1.1 = {
	monarch = {
 		name = "Geel-Ma"
		dynasty = "Tee-Zeeus"
		birth_date = 1857.1.1
		adm = 3
		dip = 5
		mil = 0
    }
}

1956.1.1 = {
	monarch = {
 		name = "Beehuna"
		dynasty = "Endoredeseer"
		birth_date = 1937.1.1
		adm = 1
		dip = 4
		mil = 4
		female = yes
    }
}

2038.1.1 = {
	monarch = {
 		name = "Terezeeus"
		dynasty = "Ah-Jee"
		birth_date = 2005.1.1
		adm = 0
		dip = 4
		mil = 0
    }
}

2114.1.1 = {
	monarch = {
 		name = "Nathrasa"
		dynasty = "Effe-Shei"
		birth_date = 2073.1.1
		adm = 5
		dip = 3
		mil = 4
		female = yes
    }
}

2183.1.1 = {
	monarch = {
 		name = "Geel-Gei"
		dynasty = "Tikeerdeseer"
		birth_date = 2154.1.1
		adm = 3
		dip = 2
		mil = 1
    }
}

2239.1.1 = {
	monarch = {
 		name = "Baseenar"
		dynasty = "Huzdeek"
		birth_date = 2210.1.1
		adm = 5
		dip = 3
		mil = 2
		female = yes
    }
}

2318.1.1 = {
	monarch = {
 		name = "Teelawei"
		dynasty = "Tee-Zeeus"
		birth_date = 2293.1.1
		adm = 3
		dip = 2
		mil = 6
    }
}

2372.1.1 = {
	monarch = {
 		name = "Bar-Goh"
		dynasty = "Meerhaj"
		birth_date = 2329.1.1
		adm = 2
		dip = 2
		mil = 2
    }
}

2430.1.1 = {
	monarch = {
 		name = "Tsatheitepa"
		dynasty = "Caseus"
		birth_date = 2402.1.1
		adm = 0
		dip = 1
		mil = 6
    }
}

