government = monarchy
government_rank = 1
mercantilism = 1
technology_group = pyandonea_tg
religion = serpant_king
primary_culture = maormer
capital = 2050

54.1.1 = {
	monarch = {
 		name = "Chryinius"
		dynasty = "Na-Ydo"
		birth_date = 15.1.1
		adm = 3
		dip = 4
		mil = 3
    }
	queen = {
 		name = "Ilyria"
		dynasty = "Na-Ydo"
		birth_date = 6.1.1
		adm = 2
		dip = 0
		mil = 3
		female = yes
    }
	heir = {
 		name = "Isda"
		monarch_name = "Isda I"
		dynasty = "Na-Ydo"
		birth_date = 49.1.1
		death_date = 126.1.1
		claim = 100
		adm = 3
		dip = 4
		mil = 4
		female = yes
    }
}

126.1.1 = {
	monarch = {
 		name = "Arslone"
		dynasty = "Na-Pydislo"
		birth_date = 77.1.1
		adm = 2
		dip = 0
		mil = 4
		female = yes
    }
	queen = {
 		name = "Mannum"
		dynasty = "Na-Pydislo"
		birth_date = 77.1.1
		adm = 6
		dip = 6
		mil = 3
    }
	heir = {
 		name = "Carnum"
		monarch_name = "Carnum I"
		dynasty = "Na-Pydislo"
		birth_date = 119.1.1
		death_date = 221.1.1
		claim = 100
		adm = 0
		dip = 6
		mil = 1
    }
}

221.1.1 = {
	monarch = {
 		name = "Ilral"
		dynasty = "Na-Yorhasri"
		birth_date = 172.1.1
		adm = 0
		dip = 3
		mil = 1
    }
	queen = {
 		name = "Paror"
		dynasty = "Na-Yorhasri"
		birth_date = 190.1.1
		adm = 0
		dip = 6
		mil = 2
		female = yes
    }
	heir = {
 		name = "Rirlel"
		monarch_name = "Rirlel I"
		dynasty = "Na-Yorhasri"
		birth_date = 214.1.1
		death_date = 295.1.1
		claim = 100
		adm = 4
		dip = 5
		mil = 1
    }
}

295.1.1 = {
	monarch = {
 		name = "Iphilinmanius"
		dynasty = "Ne-Arrion"
		birth_date = 245.1.1
		adm = 3
		dip = 1
		mil = 1
    }
}

336.1.1 = {
	monarch = {
 		name = "Arumondros"
		dynasty = "Na-Junuth"
		birth_date = 311.1.1
		adm = 1
		dip = 0
		mil = 5
    }
}

380.1.1 = {
	monarch = {
 		name = "Ohaevda"
		dynasty = "Na-Jasmo"
		birth_date = 346.1.1
		adm = 0
		dip = 0
		mil = 2
		female = yes
    }
	queen = {
 		name = "Daildor"
		dynasty = "Na-Jasmo"
		birth_date = 348.1.1
		adm = 3
		dip = 5
		mil = 1
    }
}

419.1.1 = {
	monarch = {
 		name = "Terirwen"
		dynasty = "Nu-Qrymlite"
		birth_date = 387.1.1
		adm = 2
		dip = 4
		mil = 4
		female = yes
    }
}

492.1.1 = {
	monarch = {
 		name = "Unmenmo"
		dynasty = "Na-Yarlosh"
		birth_date = 462.1.1
		adm = 0
		dip = 4
		mil = 1
    }
}

567.1.1 = {
	monarch = {
 		name = "Nordo"
		dynasty = "Ne-Yohnysh"
		birth_date = 523.1.1
		adm = 5
		dip = 3
		mil = 5
    }
}

602.1.1 = {
	monarch = {
 		name = "Isqraasar"
		dynasty = "Nu-Momneh"
		birth_date = 566.1.1
		adm = 3
		dip = 2
		mil = 1
    }
	queen = {
 		name = "Ondcareis"
		dynasty = "Nu-Momneh"
		birth_date = 572.1.1
		adm = 0
		dip = 1
		mil = 0
		female = yes
    }
}

657.1.1 = {
	monarch = {
 		name = "Ohavislel"
		dynasty = "Na-Pydislo"
		birth_date = 630.1.1
		adm = 6
		dip = 0
		mil = 4
    }
	queen = {
 		name = "Vaeht"
		dynasty = "Na-Pydislo"
		birth_date = 614.1.1
		adm = 2
		dip = 5
		mil = 3
		female = yes
    }
	heir = {
 		name = "Chryinius"
		monarch_name = "Chryinius I"
		dynasty = "Na-Pydislo"
		birth_date = 650.1.1
		death_date = 711.1.1
		claim = 100
		adm = 6
		dip = 4
		mil = 2
    }
}

711.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Uueildor"
		monarch_name = "Uueildor I"
		dynasty = "Ni-Vylrul"
		birth_date = 699.1.1
		death_date = 717.1.1
		claim = 100
		adm = 5
		dip = 3
		mil = 6
    }
}

717.1.1 = {
	monarch = {
 		name = "Larnil"
		dynasty = "		"
		birth_date = 698.1.1
		adm = 4
		dip = 6
		mil = 6
    }
	queen = {
 		name = "Desnorwen"
		dynasty = "		"
		birth_date = 691.1.1
		adm = 1
		dip = 5
		mil = 5
		female = yes
    }
	heir = {
 		name = "Jykisvisdo"
		monarch_name = "Jykisvisdo I"
		dynasty = "		"
		birth_date = 711.1.1
		death_date = 813.1.1
		claim = 100
		adm = 5
		dip = 3
		mil = 4
    }
}

813.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Cecrasnil"
		monarch_name = "Cecrasnil I"
		dynasty = "Ni-Jydromdis"
		birth_date = 810.1.1
		death_date = 828.1.1
		claim = 100
		adm = 3
		dip = 3
		mil = 0
    }
}

828.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Unwe"
		monarch_name = "Unwe I"
		dynasty = "Na-Arhylwassen"
		birth_date = 818.1.1
		death_date = 836.1.1
		claim = 100
		adm = 1
		dip = 2
		mil = 4
    }
}

836.1.1 = {
	monarch = {
 		name = "Ladesmerdo"
		dynasty = "Ni-Hassoldonmes"
		birth_date = 816.1.1
		adm = 4
		dip = 3
		mil = 6
    }
	queen = {
 		name = "Pelrasanse"
		dynasty = "Ni-Hassoldonmes"
		birth_date = 814.1.1
		adm = 1
		dip = 2
		mil = 5
		female = yes
    }
	heir = {
 		name = "Jykion"
		monarch_name = "Jykion I"
		dynasty = "Ni-Hassoldonmes"
		birth_date = 831.1.1
		death_date = 892.1.1
		claim = 100
		adm = 5
		dip = 0
		mil = 4
    }
}

892.1.1 = {
	monarch = {
 		name = "Samsamsha"
		dynasty = "Na-Sulnuht"
		birth_date = 859.1.1
		adm = 4
		dip = 4
		mil = 5
		female = yes
    }
}

934.1.1 = {
	monarch = {
 		name = "Oonil"
		dynasty = "Ni-Esluth"
		birth_date = 892.1.1
		adm = 2
		dip = 3
		mil = 1
    }
}

1014.1.1 = {
	monarch = {
 		name = "Yaacooth"
		dynasty = "Ni-Keinweroan"
		birth_date = 994.1.1
		adm = 1
		dip = 2
		mil = 5
    }
	queen = {
 		name = "Chryiphidi"
		dynasty = "Ni-Keinweroan"
		birth_date = 996.1.1
		adm = 4
		dip = 1
		mil = 4
		female = yes
    }
}

1092.1.1 = {
	monarch = {
 		name = "Heculoa"
		dynasty = "Na-Dihisresh"
		birth_date = 1069.1.1
		adm = 3
		dip = 0
		mil = 0
    }
}

1132.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Culqraauloth"
		monarch_name = "Culqraauloth I"
		dynasty = "Ne-Pesnos"
		birth_date = 1129.1.1
		death_date = 1147.1.1
		claim = 100
		adm = 3
		dip = 4
		mil = 5
    }
}

1147.1.1 = {
	monarch = {
 		name = "Rassene"
		dynasty = "Ni-Cenvah"
		birth_date = 1128.1.1
		adm = 6
		dip = 5
		mil = 1
    }
	queen = {
 		name = "Ieda"
		dynasty = "Ni-Cenvah"
		birth_date = 1126.1.1
		adm = 3
		dip = 4
		mil = 0
		female = yes
    }
	heir = {
 		name = "Anpanne"
		monarch_name = "Anpanne I"
		dynasty = "Ni-Cenvah"
		birth_date = 1134.1.1
		death_date = 1224.1.1
		claim = 100
		adm = 0
		dip = 2
		mil = 6
		female = yes
    }
}

1224.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Lordi"
		monarch_name = "Lordi I"
		dynasty = "Ni-Kahmu"
		birth_date = 1214.1.1
		death_date = 1232.1.1
		claim = 100
		adm = 5
		dip = 2
		mil = 2
		female = yes
    }
}

1232.1.1 = {
	monarch = {
 		name = "Menmion"
		dynasty = "Ne-Zysmossunel"
		birth_date = 1212.1.1
		adm = 3
		dip = 1
		mil = 6
		female = yes
    }
	queen = {
 		name = "Rassene"
		dynasty = "Ne-Zysmossunel"
		birth_date = 1199.1.1
		adm = 0
		dip = 6
		mil = 5
    }
	heir = {
 		name = "Iphidamas"
		monarch_name = "Iphidamas I"
		dynasty = "Ne-Zysmossunel"
		birth_date = 1229.1.1
		death_date = 1278.1.1
		claim = 100
		adm = 4
		dip = 5
		mil = 4
    }
}

1278.1.1 = {
	monarch = {
 		name = "Ungun"
		dynasty = "Ne-Ela"
		birth_date = 1252.1.1
		adm = 0
		dip = 6
		mil = 6
    }
	queen = {
 		name = "Arsrodsri"
		dynasty = "Ne-Ela"
		birth_date = 1243.1.1
		adm = 4
		dip = 5
		mil = 5
		female = yes
    }
	heir = {
 		name = "Gulsha"
		monarch_name = "Gulsha I"
		dynasty = "Ne-Ela"
		birth_date = 1272.1.1
		death_date = 1357.1.1
		claim = 100
		adm = 1
		dip = 3
		mil = 4
		female = yes
    }
}

1357.1.1 = {
	monarch = {
 		name = "Isooniril"
		dynasty = "Ne-Zospa"
		birth_date = 1333.1.1
		adm = 0
		dip = 0
		mil = 4
    }
}

1435.1.1 = {
	monarch = {
 		name = "Sameht"
		dynasty = "Ni-Imneldi"
		birth_date = 1389.1.1
		adm = 5
		dip = 6
		mil = 1
		female = yes
    }
	queen = {
 		name = "Alwe"
		dynasty = "Ni-Imneldi"
		birth_date = 1393.1.1
		adm = 2
		dip = 4
		mil = 0
    }
	heir = {
 		name = "Desheneior"
		monarch_name = "Desheneior I"
		dynasty = "Ni-Imneldi"
		birth_date = 1428.1.1
		death_date = 1471.1.1
		claim = 100
		adm = 2
		dip = 1
		mil = 1
		female = yes
    }
}

1471.1.1 = {
	monarch = {
 		name = "Cecrasnil"
		dynasty = "Ni-Ylniom"
		birth_date = 1444.1.1
		adm = 2
		dip = 4
		mil = 1
    }
	queen = {
 		name = "Norania"
		dynasty = "Ni-Ylniom"
		birth_date = 1450.1.1
		adm = 3
		dip = 5
		mil = 6
		female = yes
    }
	heir = {
 		name = "Pannenvissar"
		monarch_name = "Pannenvissar I"
		dynasty = "Ni-Ylniom"
		birth_date = 1460.1.1
		death_date = 1544.1.1
		claim = 100
		adm = 0
		dip = 3
		mil = 5
    }
}

1544.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Yrdron"
		monarch_name = "Yrdron I"
		dynasty = "Ne-Yithinveth"
		birth_date = 1534.1.1
		death_date = 1552.1.1
		claim = 100
		adm = 1
		dip = 1
		mil = 3
    }
}

1552.1.1 = {
	monarch = {
 		name = "Vilaroonmion"
		dynasty = "Nu-Qryrgiul"
		birth_date = 1501.1.1
		adm = 4
		dip = 2
		mil = 5
		female = yes
    }
	queen = {
 		name = "Ilral"
		dynasty = "Nu-Qryrgiul"
		birth_date = 1533.1.1
		adm = 0
		dip = 0
		mil = 4
    }
	heir = {
 		name = "Niohpelna"
		monarch_name = "Niohpelna I"
		dynasty = "Nu-Qryrgiul"
		birth_date = 1537.1.1
		death_date = 1611.1.1
		claim = 100
		adm = 2
		dip = 1
		mil = 2
		female = yes
    }
}

1611.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Nenleroonoth"
		monarch_name = "Nenleroonoth I"
		dynasty = "Ne-Chomnath"
		birth_date = 1596.1.1
		death_date = 1614.1.1
		claim = 100
		adm = 3
		dip = 5
		mil = 0
    }
}

1614.1.1 = {
	monarch = {
 		name = "Ircecsri"
		dynasty = "Nu-Ranvu"
		birth_date = 1578.1.1
		adm = 6
		dip = 4
		mil = 6
		female = yes
    }
	queen = {
 		name = "Ciyrdar"
		dynasty = "Nu-Ranvu"
		birth_date = 1568.1.1
		adm = 3
		dip = 2
		mil = 5
    }
	heir = {
 		name = "Pelcodar"
		monarch_name = "Pelcodar I"
		dynasty = "Nu-Ranvu"
		birth_date = 1600.1.1
		death_date = 1658.1.1
		claim = 100
		adm = 0
		dip = 1
		mil = 4
    }
}

1658.1.1 = {
	monarch = {
 		name = "Noryaaius"
		dynasty = "Na-Chralernos"
		birth_date = 1640.1.1
		adm = 3
		dip = 2
		mil = 6
    }
}

1742.1.1 = {
	monarch = {
 		name = "Ararcinlos"
		dynasty = "Ni-Ynri"
		birth_date = 1712.1.1
		adm = 1
		dip = 1
		mil = 3
    }
}

1800.1.1 = {
	monarch = {
 		name = "Orgcarsri"
		dynasty = "Ne-Dhyshislosh"
		birth_date = 1766.1.1
		adm = 3
		dip = 3
		mil = 4
		female = yes
    }
}

1882.1.1 = {
	monarch = {
 		name = "Locounna"
		dynasty = "Ni-Croma"
		birth_date = 1863.1.1
		adm = 1
		dip = 2
		mil = 1
		female = yes
    }
}

1967.1.1 = {
	monarch = {
 		name = "Sewe"
		dynasty = "Ni-Ymlim"
		birth_date = 1931.1.1
		adm = 0
		dip = 1
		mil = 5
    }
	queen = {
 		name = "Ohlayrwen"
		dynasty = "Ni-Ymlim"
		birth_date = 1933.1.1
		adm = 3
		dip = 6
		mil = 4
		female = yes
    }
	heir = {
 		name = "Qraalel"
		monarch_name = "Qraalel I"
		dynasty = "Ni-Ymlim"
		birth_date = 1960.1.1
		death_date = 2034.1.1
		claim = 100
		adm = 0
		dip = 5
		mil = 3
    }
}

2034.1.1 = {
	monarch = {
 		name = "Iphiculdar"
		dynasty = "Nu-Vuunvolrislu"
		birth_date = 2004.1.1
		adm = 3
		dip = 6
		mil = 5
    }
	queen = {
 		name = "Neinendessri"
		dynasty = "Nu-Vuunvolrislu"
		birth_date = 2005.1.1
		adm = 0
		dip = 5
		mil = 4
		female = yes
    }
}

2084.1.1 = {
	monarch = {
 		name = "Nitil"
		dynasty = "Ne-Calath"
		birth_date = 2045.1.1
		adm = 5
		dip = 4
		mil = 1
    }
}

2130.1.1 = {
	monarch = {
 		name = "Isooniril"
		dynasty = "Nu-Ranvu"
		birth_date = 2102.1.1
		adm = 3
		dip = 3
		mil = 4
    }
	queen = {
 		name = "Nise"
		dynasty = "Nu-Ranvu"
		birth_date = 2081.1.1
		adm = 0
		dip = 2
		mil = 3
		female = yes
    }
}

2229.1.1 = {
	monarch = {
 		name = "Ieeis"
		dynasty = "Nu-Kuuldendol"
		birth_date = 2179.1.1
		adm = 5
		dip = 1
		mil = 0
		female = yes
    }
}

2279.1.1 = {
	monarch = {
 		name = "Jyktil"
		dynasty = "Ni-Jydromdis"
		birth_date = 2227.1.1
		adm = 4
		dip = 0
		mil = 4
    }
}

2378.1.1 = {
	monarch = {
 		name = "Arslone"
		dynasty = "Ni-Pyhmonalner"
		birth_date = 2340.1.1
		adm = 2
		dip = 6
		mil = 0
		female = yes
    }
	queen = {
 		name = "Arumondros"
		dynasty = "Ni-Pyhmonalner"
		birth_date = 2356.1.1
		adm = 6
		dip = 5
		mil = 6
    }
	heir = {
 		name = "Norevalion"
		monarch_name = "Norevalion I"
		dynasty = "Ni-Pyhmonalner"
		birth_date = 2374.1.1
		death_date = 2435.1.1
		claim = 100
		adm = 3
		dip = 4
		mil = 5
    }
}

2435.1.1 = {
	monarch = {
 		name = "Ieeis"
		dynasty = "Ni-Uurguulniuth"
		birth_date = 2408.1.1
		adm = 2
		dip = 0
		mil = 5
		female = yes
    }
	queen = {
 		name = "Monwe"
		dynasty = "Ni-Uurguulniuth"
		birth_date = 2390.1.1
		adm = 6
		dip = 5
		mil = 4
    }
	heir = {
 		name = "Cecnya"
		monarch_name = "Cecnya I"
		dynasty = "Ni-Uurguulniuth"
		birth_date = 2425.1.1
		death_date = 2471.1.1
		claim = 100
		adm = 6
		dip = 2
		mil = 5
    }
}

2471.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Unmenmo"
		monarch_name = "Unmenmo I"
		dynasty = "Ne-Arrion"
		birth_date = 2456.1.1
		death_date = 2474.1.1
		claim = 100
		adm = 1
		dip = 3
		mil = 0
    }
}

2474.1.1 = {
	monarch = {
 		name = "Laaviil"
		dynasty = "Ni-Qanralre"
		birth_date = 2423.1.1
		adm = 4
		dip = 5
		mil = 2
    }
}

