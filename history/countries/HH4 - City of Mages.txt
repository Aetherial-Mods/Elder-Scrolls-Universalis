government = monarchy
government_rank = 3
mercantilism = 1
technology_group = cyrodiil_tg
religion = celestials
primary_culture = keptu
capital = 6178

54.1.1 = {
	monarch = {
 		name = "Emah"
		dynasty = "Ytas"
		birth_date = 36.1.1
		adm = 3
		dip = 5
		mil = 2
		female = yes
    }
	queen = {
 		name = "Ridravin"
		dynasty = "Ytas"
		birth_date = 34.1.1
		adm = 6
		dip = 3
		mil = 1
    }
	heir = {
 		name = "Bauvmith"
		monarch_name = "Bauvmith I"
		dynasty = "Ytas"
		birth_date = 43.1.1
		death_date = 96.1.1
		claim = 100
		adm = 3
		dip = 2
		mil = 0
    }
}

96.1.1 = {
	monarch = {
 		name = "Daolo"
		dynasty = "Lysteth"
		birth_date = 57.1.1
		adm = 3
		dip = 5
		mil = 0
    }
}

157.1.1 = {
	monarch = {
 		name = "Varin"
		dynasty = "Tudaarin"
		birth_date = 120.1.1
		adm = 1
		dip = 4
		mil = 4
		female = yes
    }
	queen = {
 		name = "Gikamik"
		dynasty = "Tudaarin"
		birth_date = 107.1.1
		adm = 5
		dip = 3
		mil = 3
    }
	heir = {
 		name = "Shelren"
		monarch_name = "Shelren I"
		dynasty = "Tudaarin"
		birth_date = 146.1.1
		death_date = 234.1.1
		claim = 100
		adm = 5
		dip = 6
		mil = 4
    }
}

234.1.1 = {
	monarch = {
 		name = "Vrurlauleth"
		dynasty = "Ridravin"
		birth_date = 181.1.1
		adm = 4
		dip = 3
		mil = 4
    }
}

303.1.1 = {
	monarch = {
 		name = "Zurduzok"
		dynasty = "Haukos"
		birth_date = 255.1.1
		adm = 3
		dip = 2
		mil = 1
    }
}

379.1.1 = {
	monarch = {
 		name = "Menhadeth"
		dynasty = "Haukos"
		birth_date = 347.1.1
		adm = 3
		dip = 6
		mil = 6
    }
}

436.1.1 = {
	monarch = {
 		name = "Kaomrar"
		dynasty = "Ralreloc"
		birth_date = 390.1.1
		adm = 2
		dip = 5
		mil = 2
    }
}

472.1.1 = {
	monarch = {
 		name = "Nhystareth"
		dynasty = "Kaomrar"
		birth_date = 437.1.1
		adm = 0
		dip = 4
		mil = 6
    }
}

526.1.1 = {
	monarch = {
 		name = "Myhros"
		dynasty = "Nuvner"
		birth_date = 479.1.1
		adm = 5
		dip = 4
		mil = 3
    }
}

611.1.1 = {
	monarch = {
 		name = "Lerrimiv"
		dynasty = "Dadrades"
		birth_date = 586.1.1
		adm = 3
		dip = 3
		mil = 6
		female = yes
    }
}

682.1.1 = {
	monarch = {
 		name = "Vredraonir"
		dynasty = "Larduve"
		birth_date = 629.1.1
		adm = 2
		dip = 2
		mil = 3
    }
}

743.1.1 = {
	monarch = {
 		name = "Nhystareth"
		dynasty = "Nirdoc"
		birth_date = 722.1.1
		adm = 4
		dip = 3
		mil = 4
    }
	queen = {
 		name = "Doshir"
		dynasty = "Nirdoc"
		birth_date = 692.1.1
		adm = 4
		dip = 0
		mil = 5
		female = yes
    }
	heir = {
 		name = "Dannota"
		monarch_name = "Dannota I"
		dynasty = "Nirdoc"
		birth_date = 742.1.1
		death_date = 828.1.1
		claim = 100
		adm = 1
		dip = 5
		mil = 4
		female = yes
    }
}

828.1.1 = {
	monarch = {
 		name = "Issaeh"
		dynasty = "Gahlaras"
		birth_date = 794.1.1
		adm = 0
		dip = 2
		mil = 5
		female = yes
    }
	queen = {
 		name = "Nuvnizo"
		dynasty = "Gahlaras"
		birth_date = 805.1.1
		adm = 4
		dip = 0
		mil = 4
    }
}

888.1.1 = {
	monarch = {
 		name = "Disseh"
		dynasty = "Yhin"
		birth_date = 838.1.1
		adm = 2
		dip = 6
		mil = 0
		female = yes
    }
	queen = {
 		name = "Virdanath"
		dynasty = "Yhin"
		birth_date = 856.1.1
		adm = 6
		dip = 5
		mil = 6
    }
}

962.1.1 = {
	monarch = {
 		name = "Rardoc"
		dynasty = "Nhystareth"
		birth_date = 912.1.1
		adm = 4
		dip = 4
		mil = 3
    }
	queen = {
 		name = "Issaeh"
		dynasty = "Nhystareth"
		birth_date = 919.1.1
		adm = 1
		dip = 3
		mil = 2
		female = yes
    }
	heir = {
 		name = "Zahoti"
		monarch_name = "Zahoti I"
		dynasty = "Nhystareth"
		birth_date = 955.1.1
		death_date = 1056.1.1
		claim = 100
		adm = 5
		dip = 1
		mil = 1
		female = yes
    }
}

1056.1.1 = {
	monarch = {
 		name = "Tirdor"
		dynasty = "Ralreloc"
		birth_date = 1004.1.1
		adm = 1
		dip = 3
		mil = 3
    }
}

1120.1.1 = {
	monarch = {
 		name = "Daolo"
		dynasty = "Vredraonir"
		birth_date = 1096.1.1
		adm = 6
		dip = 2
		mil = 0
    }
}

1169.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Resi"
		monarch_name = "Resi I"
		dynasty = "Amedoc"
		birth_date = 1167.1.1
		death_date = 1185.1.1
		claim = 100
		adm = 0
		dip = 6
		mil = 5
		female = yes
    }
}

1185.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Zyrdusor"
		monarch_name = "Zyrdusor I"
		dynasty = "Halaanis"
		birth_date = 1170.1.1
		death_date = 1188.1.1
		claim = 100
		adm = 5
		dip = 5
		mil = 1
    }
}

1188.1.1 = {
	monarch = {
 		name = "Tirdor"
		dynasty = "Aalezos"
		birth_date = 1163.1.1
		adm = 4
		dip = 2
		mil = 2
    }
	queen = {
 		name = "Izaev"
		dynasty = "Aalezos"
		birth_date = 1146.1.1
		adm = 5
		dip = 5
		mil = 3
		female = yes
    }
}

1234.1.1 = {
	monarch = {
 		name = "Dadrades"
		dynasty = "Gakaazin"
		birth_date = 1191.1.1
		adm = 0
		dip = 6
		mil = 4
    }
}

1330.1.1 = {
	monarch = {
 		name = "Zakysor"
		dynasty = "Ihrasek"
		birth_date = 1285.1.1
		adm = 5
		dip = 5
		mil = 1
    }
}

1420.1.1 = {
	monarch = {
 		name = "Tirlaasir"
		dynasty = "Dyrmec"
		birth_date = 1390.1.1
		adm = 3
		dip = 5
		mil = 5
    }
}

1519.1.1 = {
	monarch = {
 		name = "Aalezos"
		dynasty = "Gukhedas"
		birth_date = 1476.1.1
		adm = 1
		dip = 4
		mil = 1
    }
	queen = {
 		name = "Oshan"
		dynasty = "Gukhedas"
		birth_date = 1486.1.1
		adm = 5
		dip = 2
		mil = 0
		female = yes
    }
	heir = {
 		name = "Vrurlauleth"
		monarch_name = "Vrurlauleth I"
		dynasty = "Gukhedas"
		birth_date = 1513.1.1
		death_date = 1570.1.1
		claim = 100
		adm = 1
		dip = 4
		mil = 3
    }
}

1570.1.1 = {
	monarch = {
 		name = "Lyrmerek"
		dynasty = "Zakysor"
		birth_date = 1519.1.1
		adm = 0
		dip = 0
		mil = 3
    }
	queen = {
 		name = "Mimohu"
		dynasty = "Zakysor"
		birth_date = 1539.1.1
		adm = 4
		dip = 6
		mil = 2
		female = yes
    }
	heir = {
 		name = "Nhystareth"
		monarch_name = "Nhystareth I"
		dynasty = "Zakysor"
		birth_date = 1561.1.1
		death_date = 1612.1.1
		claim = 100
		adm = 1
		dip = 4
		mil = 1
    }
}

1612.1.1 = {
	monarch = {
 		name = "Horremel"
		dynasty = "Rardoc"
		birth_date = 1583.1.1
		adm = 4
		dip = 6
		mil = 3
		female = yes
    }
}

1686.1.1 = {
	monarch = {
 		name = "Soshemeh"
		dynasty = "Tirlaasir"
		birth_date = 1657.1.1
		adm = 2
		dip = 5
		mil = 0
		female = yes
    }
}

1766.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Mahodae"
		monarch_name = "Mahodae I"
		dynasty = "Vrakir"
		birth_date = 1752.1.1
		death_date = 1770.1.1
		claim = 100
		adm = 3
		dip = 2
		mil = 5
		female = yes
    }
}

1770.1.1 = {
	monarch = {
 		name = "Naonhaudes"
		dynasty = "Ihrasek"
		birth_date = 1738.1.1
		adm = 6
		dip = 3
		mil = 0
    }
	queen = {
 		name = "Sarri"
		dynasty = "Ihrasek"
		birth_date = 1724.1.1
		adm = 2
		dip = 2
		mil = 6
		female = yes
    }
	heir = {
 		name = "Menhadeth"
		monarch_name = "Menhadeth I"
		dynasty = "Ihrasek"
		birth_date = 1759.1.1
		death_date = 1812.1.1
		claim = 100
		adm = 6
		dip = 0
		mil = 5
    }
}

1812.1.1 = {
	monarch = {
 		name = "Ridravin"
		dynasty = "Bauvmith"
		birth_date = 1778.1.1
		adm = 6
		dip = 4
		mil = 5
    }
	queen = {
 		name = "Nemehul"
		dynasty = "Bauvmith"
		birth_date = 1771.1.1
		adm = 3
		dip = 2
		mil = 4
		female = yes
    }
	heir = {
 		name = "Havu"
		monarch_name = "Havu I"
		dynasty = "Bauvmith"
		birth_date = 1797.1.1
		death_date = 1895.1.1
		claim = 100
		adm = 3
		dip = 6
		mil = 5
		female = yes
    }
}

1895.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Halaanis"
		monarch_name = "Halaanis I"
		dynasty = "Taaner"
		birth_date = 1883.1.1
		death_date = 1901.1.1
		claim = 100
		adm = 5
		dip = 0
		mil = 0
    }
}

1901.1.1 = {
	monarch = {
 		name = "Tadak"
		dynasty = "Lukaazon"
		birth_date = 1872.1.1
		adm = 0
		dip = 1
		mil = 2
    }
	queen = {
 		name = "Lotemu"
		dynasty = "Lukaazon"
		birth_date = 1873.1.1
		adm = 4
		dip = 0
		mil = 1
		female = yes
    }
	heir = {
 		name = "Riralath"
		monarch_name = "Riralath I"
		dynasty = "Lukaazon"
		birth_date = 1894.1.1
		death_date = 1996.1.1
		claim = 100
		adm = 1
		dip = 5
		mil = 0
    }
}

1996.1.1 = {
	monarch = {
 		name = "Ilirah"
		dynasty = "Ihrasek"
		birth_date = 1965.1.1
		adm = 4
		dip = 0
		mil = 3
		female = yes
    }
}

2090.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Halaanis"
		monarch_name = "Halaanis I"
		dynasty = "Dauhrak"
		birth_date = 2076.1.1
		death_date = 2094.1.1
		claim = 100
		adm = 5
		dip = 4
		mil = 0
    }
}

2094.1.1 = {
	monarch = {
 		name = "Tadak"
		dynasty = "Herlir"
		birth_date = 2051.1.1
		adm = 4
		dip = 0
		mil = 1
    }
	queen = {
 		name = "Retobil"
		dynasty = "Herlir"
		birth_date = 2046.1.1
		adm = 4
		dip = 4
		mil = 2
		female = yes
    }
	heir = {
 		name = "Gahlaras"
		monarch_name = "Gahlaras I"
		dynasty = "Herlir"
		birth_date = 2090.1.1
		death_date = 2184.1.1
		claim = 100
		adm = 1
		dip = 2
		mil = 1
    }
}

2184.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Remaasok"
		monarch_name = "Remaasok I"
		dynasty = "Ralreloc"
		birth_date = 2169.1.1
		death_date = 2187.1.1
		claim = 100
		adm = 6
		dip = 1
		mil = 4
    }
}

2187.1.1 = {
	monarch = {
 		name = "Tamedir"
		dynasty = "Nhekhedin"
		birth_date = 2137.1.1
		adm = 6
		dip = 5
		mil = 5
    }
	queen = {
 		name = "Atholur"
		dynasty = "Nhekhedin"
		birth_date = 2163.1.1
		adm = 3
		dip = 3
		mil = 4
		female = yes
    }
	heir = {
 		name = "Gilraoles"
		monarch_name = "Gilraoles I"
		dynasty = "Nhekhedin"
		birth_date = 2182.1.1
		death_date = 2275.1.1
		claim = 100
		adm = 0
		dip = 2
		mil = 3
    }
}

2275.1.1 = {
	monarch = {
 		name = "Hiherev"
		dynasty = "Ridravin"
		birth_date = 2244.1.1
		adm = 2
		dip = 3
		mil = 5
		female = yes
    }
}

2325.1.1 = {
	monarch = {
 		name = "Vaorylan"
		dynasty = "Vredraudin"
		birth_date = 2281.1.1
		adm = 1
		dip = 2
		mil = 2
    }
	queen = {
 		name = "Vena"
		dynasty = "Vredraudin"
		birth_date = 2302.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
	heir = {
 		name = "Tylak"
		monarch_name = "Tylak I"
		dynasty = "Vredraudin"
		birth_date = 2313.1.1
		death_date = 2415.1.1
		claim = 100
		adm = 1
		dip = 6
		mil = 0
    }
}

2415.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Soshemeh"
		monarch_name = "Soshemeh I"
		dynasty = "Tylak"
		birth_date = 2402.1.1
		death_date = 2420.1.1
		claim = 100
		adm = 1
		dip = 3
		mil = 0
		female = yes
    }
}

2420.1.1 = {
	monarch = {
 		name = "Bauvmith"
		dynasty = "Gumrok"
		birth_date = 2377.1.1
		adm = 3
		dip = 4
		mil = 2
    }
	queen = {
 		name = "Tome"
		dynasty = "Gumrok"
		birth_date = 2399.1.1
		adm = 0
		dip = 3
		mil = 1
		female = yes
    }
	heir = {
 		name = "Linne"
		monarch_name = "Linne I"
		dynasty = "Gumrok"
		birth_date = 2416.1.1
		death_date = 2470.1.1
		claim = 100
		adm = 4
		dip = 1
		mil = 0
		female = yes
    }
}

2470.1.1 = {
	monarch = {
 		name = "Nestith"
		dynasty = "Vrurlauleth"
		birth_date = 2451.1.1
		adm = 0
		dip = 3
		mil = 2
    }
	queen = {
 		name = "Lassoba"
		dynasty = "Vrurlauleth"
		birth_date = 2450.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
	heir = {
 		name = "Hetok"
		monarch_name = "Hetok I"
		dynasty = "Vrurlauleth"
		birth_date = 2455.1.1
		death_date = 2554.1.1
		claim = 100
		adm = 1
		dip = 0
		mil = 0
    }
}

