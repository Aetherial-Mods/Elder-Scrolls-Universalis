government = tribal
government_rank = 1
mercantilism = 1
technology_group = daedric_tg
religion = molag_bal_cult
primary_culture = soul_shriven
capital = 3147

54.1.1 = {
	monarch = {
 		name = "Tirdor"
		dynasty = "Vindele"
		birth_date = 35.1.1
		adm = 1
		dip = 1
		mil = 1
    }
	add_ruler_personality = immortal_personality
}

148.1.1 = {
	monarch = {
 		name = "Nomuh"
		dynasty = "Vothinda"
		birth_date = 95.1.1
		adm = 2
		dip = 5
		mil = 6
		female = yes
    }
	add_ruler_personality = immortal_personality
}

204.1.1 = {
	monarch = {
 		name = "Retobil"
		dynasty = "Zifenne"
		birth_date = 158.1.1
		adm = 0
		dip = 4
		mil = 3
		female = yes
    }
	add_ruler_personality = immortal_personality
}

266.1.1 = {
	monarch = {
 		name = "Lirimiv"
		dynasty = "Vindele"
		birth_date = 224.1.1
		adm = 5
		dip = 4
		mil = 0
		female = yes
    }
	add_ruler_personality = immortal_personality
}

344.1.1 = {
	monarch = {
 		name = "Dastith"
		dynasty = "Manlazo"
		birth_date = 294.1.1
		adm = 3
		dip = 3
		mil = 3
    }
	add_ruler_personality = immortal_personality
}

425.1.1 = {
	monarch = {
 		name = "Kaahlec"
		dynasty = "Aslelle"
		birth_date = 386.1.1
		adm = 2
		dip = 2
		mil = 0
    }
	add_ruler_personality = immortal_personality
}

522.1.1 = {
	monarch = {
 		name = "Hiherev"
		dynasty = "Endale"
		birth_date = 474.1.1
		adm = 3
		dip = 3
		mil = 1
		female = yes
    }
	add_ruler_personality = immortal_personality
}

602.1.1 = {
	monarch = {
 		name = "Vaorylan"
		dynasty = "Sanimala"
		birth_date = 579.1.1
		adm = 1
		dip = 5
		mil = 1
    }
	add_ruler_personality = immortal_personality
}

663.1.1 = {
	monarch = {
 		name = "Nizae"
		dynasty = "Ralmehay"
		birth_date = 616.1.1
		adm = 0
		dip = 2
		mil = 2
		female = yes
    }
	add_ruler_personality = immortal_personality
}

744.1.1 = {
	monarch = {
 		name = "Myhros"
		dynasty = "Zifenne"
		birth_date = 697.1.1
		adm = 5
		dip = 1
		mil = 5
    }
	add_ruler_personality = immortal_personality
}

796.1.1 = {
	monarch = {
 		name = "Vrakir"
		dynasty = "Zifenne"
		birth_date = 753.1.1
		adm = 3
		dip = 0
		mil = 2
    }
	add_ruler_personality = immortal_personality
}

876.1.1 = {
	monarch = {
 		name = "Mahodae"
		dynasty = "Linyiva"
		birth_date = 842.1.1
		adm = 2
		dip = 6
		mil = 6
		female = yes
    }
	add_ruler_personality = immortal_personality
}

930.1.1 = {
	monarch = {
 		name = "Vena"
		dynasty = "Cesella"
		birth_date = 891.1.1
		adm = 0
		dip = 5
		mil = 2
		female = yes
    }
	add_ruler_personality = immortal_personality
}

967.1.1 = {
	monarch = {
 		name = "Hynhaolen"
		dynasty = "Isnosa"
		birth_date = 930.1.1
		adm = 5
		dip = 5
		mil = 6
    }
	add_ruler_personality = immortal_personality
}

1045.1.1 = {
	monarch = {
 		name = "Letan"
		dynasty = "Irpase"
		birth_date = 1022.1.1
		adm = 0
		dip = 6
		mil = 1
    }
	add_ruler_personality = immortal_personality
}

1098.1.1 = {
	monarch = {
 		name = "Nennabu"
		dynasty = "Nathanno"
		birth_date = 1048.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
	add_ruler_personality = immortal_personality
}

1157.1.1 = {
	monarch = {
 		name = "Runhyrac"
		dynasty = "Melfezita"
		birth_date = 1132.1.1
		adm = 4
		dip = 4
		mil = 1
    }
	add_ruler_personality = immortal_personality
}

1225.1.1 = {
	monarch = {
 		name = "Dennae"
		dynasty = "Hanera"
		birth_date = 1199.1.1
		adm = 2
		dip = 4
		mil = 4
		female = yes
    }
	add_ruler_personality = immortal_personality
}

1312.1.1 = {
	monarch = {
 		name = "Demer"
		dynasty = "Enkeyile"
		birth_date = 1293.1.1
		adm = 0
		dip = 3
		mil = 1
		female = yes
    }
	add_ruler_personality = immortal_personality
}

1397.1.1 = {
	monarch = {
 		name = "Vaulic"
		dynasty = "Invitte"
		birth_date = 1346.1.1
		adm = 5
		dip = 2
		mil = 5
    }
	add_ruler_personality = immortal_personality
}

1445.1.1 = {
	monarch = {
 		name = "Teni"
		dynasty = "Mophahe"
		birth_date = 1414.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
	add_ruler_personality = immortal_personality
}

1534.1.1 = {
	monarch = {
 		name = "Dennae"
		dynasty = "Adrela"
		birth_date = 1515.1.1
		adm = 2
		dip = 0
		mil = 5
		female = yes
    }
	add_ruler_personality = immortal_personality
}

1577.1.1 = {
	monarch = {
 		name = "Demer"
		dynasty = "Egile"
		birth_date = 1549.1.1
		adm = 4
		dip = 2
		mil = 0
		female = yes
    }
	add_ruler_personality = immortal_personality
}

1643.1.1 = {
	monarch = {
 		name = "Vaulic"
		dynasty = "Mophahe"
		birth_date = 1617.1.1
		adm = 2
		dip = 1
		mil = 3
    }
	add_ruler_personality = immortal_personality
}

1702.1.1 = {
	monarch = {
 		name = "Tylak"
		dynasty = "Gerlenno"
		birth_date = 1679.1.1
		adm = 0
		dip = 0
		mil = 0
    }
	add_ruler_personality = immortal_personality
}

1779.1.1 = {
	monarch = {
 		name = "Gilraoles"
		dynasty = "Vindele"
		birth_date = 1755.1.1
		adm = 5
		dip = 6
		mil = 4
    }
	add_ruler_personality = immortal_personality
}

1837.1.1 = {
	monarch = {
 		name = "Gahlaras"
		dynasty = "Killing"
		birth_date = 1819.1.1
		adm = 4
		dip = 5
		mil = 0
    }
	add_ruler_personality = immortal_personality
}

1920.1.1 = {
	monarch = {
 		name = "Zonemaer"
		dynasty = "Killing"
		birth_date = 1899.1.1
		adm = 2
		dip = 5
		mil = 4
		female = yes
    }
	add_ruler_personality = immortal_personality
}

1989.1.1 = {
	monarch = {
 		name = "Hosir"
		dynasty = "Hanera"
		birth_date = 1936.1.1
		adm = 0
		dip = 4
		mil = 0
		female = yes
    }
	add_ruler_personality = immortal_personality
}

2055.1.1 = {
	monarch = {
 		name = "Gilraoles"
		dynasty = "Alromoli"
		birth_date = 2036.1.1
		adm = 5
		dip = 3
		mil = 4
    }
	add_ruler_personality = immortal_personality
}

2118.1.1 = {
	monarch = {
 		name = "Gahlaras"
		dynasty = "Endivese"
		birth_date = 2087.1.1
		adm = 0
		dip = 4
		mil = 6
    }
	add_ruler_personality = immortal_personality
}

2214.1.1 = {
	monarch = {
 		name = "Menhadeth"
		dynasty = "Larsemay"
		birth_date = 2187.1.1
		adm = 6
		dip = 4
		mil = 2
    }
	add_ruler_personality = immortal_personality
}

2279.1.1 = {
	monarch = {
 		name = "Runhath"
		dynasty = "Rondifito"
		birth_date = 2227.1.1
		adm = 4
		dip = 3
		mil = 6
    }
	add_ruler_personality = immortal_personality
}

2329.1.1 = {
	monarch = {
 		name = "Ridravin"
		dynasty = "Yelmendo"
		birth_date = 2290.1.1
		adm = 2
		dip = 2
		mil = 3
    }
	add_ruler_personality = immortal_personality
}

2387.1.1 = {
	monarch = {
 		name = "Nhihreseth"
		dynasty = "Lisaroma"
		birth_date = 2349.1.1
		adm = 3
		dip = 6
		mil = 1
    }
	add_ruler_personality = immortal_personality
}

2428.1.1 = {
	monarch = {
 		name = "Dirrinuh"
		dynasty = "Enkala"
		birth_date = 2397.1.1
		adm = 0
		dip = 1
		mil = 1
		female = yes
    }
	add_ruler_personality = immortal_personality
}

2466.1.1 = {
	monarch = {
 		name = "Hynhaolen"
		dynasty = "Civali"
		birth_date = 2428.1.1
		adm = 2
		dip = 2
		mil = 2
    }
	add_ruler_personality = immortal_personality
}

