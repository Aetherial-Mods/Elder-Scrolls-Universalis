government = theocracy
government_rank = 3
mercantilism = 1
technology_group = elven_tg
religion = bosmer_pantheon
primary_culture = bosmer
capital = 4950
secondary_religion = hircine_cult

54.1.1 = {
	monarch = {
 		name = "Cirthile"
		dynasty = "Or'Naearil"
		birth_date = 24.1.1
		adm = 2
		dip = 6
		mil = 4
    }
}

134.1.1 = {
	monarch = {
 		name = "Sethel"
		dynasty = "Ur'Cormair"
		birth_date = 86.1.1
		adm = 4
		dip = 0
		mil = 6
		female = yes
    }
}

174.1.1 = {
	monarch = {
 		name = "Lothiel"
		dynasty = "Ca'Elsesse"
		birth_date = 131.1.1
		adm = 2
		dip = 6
		mil = 2
		female = yes
    }
}

271.1.1 = {
	monarch = {
 		name = "Thaenaneth"
		dynasty = "Ur'Andsse"
		birth_date = 238.1.1
		adm = 0
		dip = 6
		mil = 6
		female = yes
    }
}

339.1.1 = {
	monarch = {
 		name = "Melbethil"
		dynasty = "Or'Southpoint"
		birth_date = 301.1.1
		adm = 6
		dip = 5
		mil = 3
		female = yes
    }
}

385.1.1 = {
	monarch = {
 		name = "Forelfin"
		dynasty = "Ca'Heaven"
		birth_date = 338.1.1
		adm = 4
		dip = 4
		mil = 6
    }
}

475.1.1 = {
	monarch = {
 		name = "Dairaegal"
		dynasty = "Ur'Donae"
		birth_date = 423.1.1
		adm = 2
		dip = 3
		mil = 3
		female = yes
    }
}

556.1.1 = {
	monarch = {
 		name = "Saldaer"
		dynasty = "Ur'Areanragil"
		birth_date = 513.1.1
		adm = 0
		dip = 2
		mil = 0
    }
}

593.1.1 = {
	monarch = {
 		name = "Madruin"
		dynasty = "Ur'Bluewind"
		birth_date = 563.1.1
		adm = 6
		dip = 2
		mil = 3
    }
}

656.1.1 = {
	monarch = {
 		name = "Galethril"
		dynasty = "Ca'Heaven"
		birth_date = 625.1.1
		adm = 1
		dip = 3
		mil = 5
		female = yes
    }
}

718.1.1 = {
	monarch = {
 		name = "Danrithri"
		dynasty = "Or'Naearil"
		birth_date = 689.1.1
		adm = 6
		dip = 2
		mil = 1
    }
}

770.1.1 = {
	monarch = {
 		name = "Gaerthgorn"
		dynasty = "Or'Naerilin"
		birth_date = 730.1.1
		adm = 4
		dip = 1
		mil = 5
    }
}

823.1.1 = {
	monarch = {
 		name = "Dirchanor"
		dynasty = "Ur'Applerun"
		birth_date = 770.1.1
		adm = 2
		dip = 1
		mil = 2
    }
}

894.1.1 = {
	monarch = {
 		name = "Tholdegil"
		dynasty = "Ca'Galrchel"
		birth_date = 849.1.1
		adm = 1
		dip = 0
		mil = 5
    }
}

958.1.1 = {
	monarch = {
 		name = "Malionar"
		dynasty = "Ca'Greenheart"
		birth_date = 922.1.1
		adm = 6
		dip = 6
		mil = 2
    }
}

1044.1.1 = {
	monarch = {
 		name = "Garel"
		dynasty = "Ur'Areanragil"
		birth_date = 1015.1.1
		adm = 4
		dip = 5
		mil = 6
		female = yes
    }
}

1139.1.1 = {
	monarch = {
 		name = "Gwingelruin"
		dynasty = "Ca'Meneia"
		birth_date = 1120.1.1
		adm = 5
		dip = 2
		mil = 4
    }
}

1233.1.1 = {
	monarch = {
 		name = "Enthoras"
		dynasty = "Or'Vulkwasten"
		birth_date = 1199.1.1
		adm = 3
		dip = 1
		mil = 0
    }
}

1307.1.1 = {
	monarch = {
 		name = "Andrilion"
		dynasty = "Ur'Arednor"
		birth_date = 1279.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1395.1.1 = {
	monarch = {
 		name = "Nistel"
		dynasty = "Ca'Iingenyl"
		birth_date = 1365.1.1
		adm = 6
		dip = 0
		mil = 1
		female = yes
    }
}

1459.1.1 = {
	monarch = {
 		name = "Gwindir"
		dynasty = "Ca'Meneia"
		birth_date = 1433.1.1
		adm = 1
		dip = 1
		mil = 2
    }
}

1538.1.1 = {
	monarch = {
 		name = "Eraneth"
		dynasty = "Ur'Arenthia"
		birth_date = 1497.1.1
		adm = 0
		dip = 0
		mil = 6
		female = yes
    }
}

1603.1.1 = {
	monarch = {
 		name = "Halalin"
		dynasty = "Or'Southpoint"
		birth_date = 1567.1.1
		adm = 5
		dip = 0
		mil = 2
    }
}

1675.1.1 = {
	monarch = {
 		name = "Erothel"
		dynasty = "Ca'Laenoniel"
		birth_date = 1652.1.1
		adm = 3
		dip = 6
		mil = 6
    }
}

1759.1.1 = {
	monarch = {
 		name = "Arenion"
		dynasty = "Ca'Estrin"
		birth_date = 1720.1.1
		adm = 1
		dip = 5
		mil = 3
    }
}

1847.1.1 = {
	monarch = {
 		name = "Ninglenel"
		dynasty = "Or'Mossmire"
		birth_date = 1809.1.1
		adm = 0
		dip = 4
		mil = 6
    }
}

1914.1.1 = {
	monarch = {
 		name = "Hadras"
		dynasty = "Ca'Elsyon"
		birth_date = 1895.1.1
		adm = 5
		dip = 3
		mil = 3
    }
}

1964.1.1 = {
	monarch = {
 		name = "Essonarth"
		dynasty = "Or'Thamnel"
		birth_date = 1937.1.1
		adm = 3
		dip = 3
		mil = 0
		female = yes
    }
}

2023.1.1 = {
	monarch = {
 		name = "Angrineth"
		dynasty = "Ur'Andsse"
		birth_date = 2000.1.1
		adm = 5
		dip = 4
		mil = 1
		female = yes
    }
}

2096.1.1 = {
	monarch = {
 		name = "Nilding"
		dynasty = "Ca'Mandae"
		birth_date = 2050.1.1
		adm = 3
		dip = 3
		mil = 5
    }
}

2165.1.1 = {
	monarch = {
 		name = "Atheval"
		dynasty = "Ca'Laenoniel"
		birth_date = 2129.1.1
		adm = 1
		dip = 2
		mil = 1
		female = yes
    }
}

2254.1.1 = {
	monarch = {
 		name = "Obenion"
		dynasty = "Or'Southpoint"
		birth_date = 2214.1.1
		adm = 0
		dip = 1
		mil = 5
    }
}

2350.1.1 = {
	monarch = {
 		name = "Hareglor"
		dynasty = "Ca'Iingenyl"
		birth_date = 2318.1.1
		adm = 5
		dip = 1
		mil = 2
    }
}

2435.1.1 = {
	monarch = {
 		name = "Esunil"
		dynasty = "Ca'Heimdar"
		birth_date = 2390.1.1
		adm = 3
		dip = 0
		mil = 5
    }
}

