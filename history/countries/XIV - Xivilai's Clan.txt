government = native
government_rank = 1
mercantilism = 1
technology_group = daedric_tg
religion = mehrunes_dagon_cult
primary_culture = xivilai
capital = 4075

54.1.1 = {
	monarch = {
 		name = "Miamys"
		dynasty = "Iarpeiros"
		birth_date = 33.1.1
		adm = 0
		dip = 6
		mil = 0
		female = yes
    }
}

89.1.1 = {
	monarch = {
 		name = "Moryarus"
		dynasty = "Xilhorn"
		birth_date = 51.1.1
		adm = 6
		dip = 5
		mil = 4
    }
}

185.1.1 = {
	monarch = {
 		name = "Aexina"
		dynasty = "Glynven"
		birth_date = 134.1.1
		adm = 4
		dip = 4
		mil = 1
		female = yes
    }
}

262.1.1 = {
	monarch = {
 		name = "Umefaren"
		dynasty = "Iarxalim"
		birth_date = 237.1.1
		adm = 2
		dip = 4
		mil = 4
    }
}

319.1.1 = {
	monarch = {
 		name = "Faeceran"
		dynasty = "Tracoril"
		birth_date = 282.1.1
		adm = 0
		dip = 3
		mil = 1
		female = yes
    }
}

405.1.1 = {
	monarch = {
 		name = "Moryarus"
		dynasty = "Gralion"
		birth_date = 357.1.1
		adm = 6
		dip = 2
		mil = 5
    }
}

445.1.1 = {
	monarch = {
 		name = "Keynan"
		dynasty = "Yorron"
		birth_date = 426.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

542.1.1 = {
	monarch = {
 		name = "Mordan"
		dynasty = "Quonor"
		birth_date = 499.1.1
		adm = 6
		dip = 2
		mil = 3
		female = yes
    }
}

613.1.1 = {
	monarch = {
 		name = "Corladon"
		dynasty = "Stephtorin"
		birth_date = 585.1.1
		adm = 4
		dip = 2
		mil = 0
    }
}

698.1.1 = {
	monarch = {
 		name = "Quilana"
		dynasty = "Faepetor"
		birth_date = 670.1.1
		adm = 2
		dip = 1
		mil = 3
		female = yes
    }
}

746.1.1 = {
	monarch = {
 		name = "Holavaris"
		dynasty = "Tracoril"
		birth_date = 725.1.1
		adm = 1
		dip = 0
		mil = 0
    }
}

790.1.1 = {
	monarch = {
 		name = "Malcutorin"
		dynasty = "Moryarus"
		birth_date = 752.1.1
		adm = 1
		dip = 4
		mil = 5
    }
}

840.1.1 = {
	monarch = {
 		name = "Ralothyra"
		dynasty = "Grexina"
		birth_date = 808.1.1
		adm = 0
		dip = 3
		mil = 1
		female = yes
    }
}

880.1.1 = {
	monarch = {
 		name = "Donred"
		dynasty = "Wynlar"
		birth_date = 842.1.1
		adm = 2
		dip = 5
		mil = 4
    }
}

946.1.1 = {
	monarch = {
 		name = "Xilcan"
		dynasty = "Tracoril"
		birth_date = 904.1.1
		adm = 6
		dip = 0
		mil = 3
    }
}

1000.1.1 = {
	monarch = {
 		name = "Umdorr"
		dynasty = "Glynven"
		birth_date = 963.1.1
		adm = 4
		dip = 6
		mil = 0
    }
}

1050.1.1 = {
	monarch = {
 		name = "Olafir"
		dynasty = "Wynlar"
		birth_date = 1026.1.1
		adm = 2
		dip = 5
		mil = 3
    }
}

1130.1.1 = {
	monarch = {
 		name = "Havekoris"
		dynasty = "Beikas"
		birth_date = 1087.1.1
		adm = 0
		dip = 4
		mil = 0
    }
}

1181.1.1 = {
	monarch = {
 		name = "Omakian"
		dynasty = "Lorastina"
		birth_date = 1130.1.1
		adm = 6
		dip = 3
		mil = 4
		female = yes
    }
}

1258.1.1 = {
	monarch = {
 		name = "Jefmeron"
		dynasty = "Remluin"
		birth_date = 1212.1.1
		adm = 4
		dip = 3
		mil = 0
    }
}

1318.1.1 = {
	monarch = {
 		name = "Frandorr"
		dynasty = "Inasalor"
		birth_date = 1269.1.1
		adm = 2
		dip = 2
		mil = 4
    }
}

1369.1.1 = {
	monarch = {
 		name = "Havekoris"
		dynasty = "Reykian"
		birth_date = 1324.1.1
		adm = 1
		dip = 5
		mil = 4
    }
}

1454.1.1 = {
	monarch = {
 		name = "Lamlaeron"
		dynasty = "Jarthorn"
		birth_date = 1435.1.1
		adm = 0
		dip = 4
		mil = 1
    }
}

1492.1.1 = {
	monarch = {
 		name = "Corladon"
		dynasty = "Shapetor"
		birth_date = 1471.1.1
		adm = 0
		dip = 2
		mil = 6
    }
}

1567.1.1 = {
	monarch = {
 		name = "Quonor"
		dynasty = "Havekoris"
		birth_date = 1540.1.1
		adm = 6
		dip = 1
		mil = 2
    }
}

1649.1.1 = {
	monarch = {
 		name = "Jamekath"
		dynasty = "Tracoril"
		birth_date = 1608.1.1
		adm = 4
		dip = 0
		mil = 6
    }
}

1740.1.1 = {
	monarch = {
 		name = "Galered"
		dynasty = "Magstina"
		birth_date = 1704.1.1
		adm = 2
		dip = 6
		mil = 3
    }
}

1831.1.1 = {
	monarch = {
 		name = "Corladon"
		dynasty = "Hanlael"
		birth_date = 1782.1.1
		adm = 0
		dip = 5
		mil = 6
    }
}

1901.1.1 = {
	monarch = {
 		name = "Quonor"
		dynasty = "Nicparin"
		birth_date = 1865.1.1
		adm = 6
		dip = 5
		mil = 3
    }
}

1986.1.1 = {
	monarch = {
 		name = "Kyslaema"
		dynasty = "Beikas"
		birth_date = 1946.1.1
		adm = 1
		dip = 6
		mil = 5
		female = yes
    }
}

2061.1.1 = {
	monarch = {
 		name = "Heilleth"
		dynasty = "Keynan"
		birth_date = 2011.1.1
		adm = 6
		dip = 5
		mil = 1
		female = yes
    }
}

2113.1.1 = {
	monarch = {
 		name = "Ralothyra"
		dynasty = "Zumkalyn"
		birth_date = 2073.1.1
		adm = 2
		dip = 6
		mil = 4
		female = yes
    }
}

2174.1.1 = {
	monarch = {
 		name = "Hanlael"
		dynasty = "Alxiron"
		birth_date = 2153.1.1
		adm = 0
		dip = 6
		mil = 0
    }
}

2260.1.1 = {
	monarch = {
 		name = "Qiydark"
		dynasty = "Jarfarin"
		birth_date = 2242.1.1
		adm = 5
		dip = 5
		mil = 4
		female = yes
    }
}

2311.1.1 = {
	monarch = {
 		name = "Elahana"
		dynasty = "Farris"
		birth_date = 2274.1.1
		adm = 0
		dip = 6
		mil = 5
		female = yes
    }
}

2396.1.1 = {
	monarch = {
 		name = "Mirayarus"
		dynasty = "Gabqinor"
		birth_date = 2374.1.1
		adm = 5
		dip = 5
		mil = 2
		female = yes
    }
}

2493.1.1 = {
	monarch = {
 		name = "Keylee"
		dynasty = "Faraalath"
		birth_date = 2465.1.1
		adm = 2
		dip = 0
		mil = 2
		female = yes
    }
}

