government = monarchy
government_rank = 1
mercantilism = 1
technology_group = akavir_tg
religion = po_tun_pantheon
primary_culture = po_tun
capital = 662

54.1.1 = {
	monarch = {
 		name = "Pi"
		dynasty = "Tehortes'la"
		birth_date = 9.1.1
		adm = 6
		dip = 1
		mil = 5
		female = yes
    }
	queen = {
 		name = "Gon"
		dynasty = "Tehortes'la"
		birth_date = 18.1.1
		adm = 3
		dip = 6
		mil = 4
    }
}

140.1.1 = {
	monarch = {
 		name = "Quv"
		dynasty = "Ramoz'ri"
		birth_date = 107.1.1
		adm = 1
		dip = 5
		mil = 1
    }
	queen = {
 		name = "Sieherr"
		dynasty = "Ramoz'ri"
		birth_date = 120.1.1
		adm = 5
		dip = 4
		mil = 0
		female = yes
    }
	heir = {
 		name = "Tehuntu"
		monarch_name = "Tehuntu I"
		dynasty = "Ramoz'ri"
		birth_date = 131.1.1
		death_date = 222.1.1
		claim = 100
		adm = 5
		dip = 0
		mil = 1
    }
}

222.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Nuyul"
		monarch_name = "Nuyul I"
		dynasty = "Gaduyd'ri"
		birth_date = 221.1.1
		death_date = 239.1.1
		claim = 100
		adm = 0
		dip = 2
		mil = 2
		female = yes
    }
}

239.1.1 = {
	monarch = {
 		name = "Pad"
		dynasty = "Susthae'la"
		birth_date = 210.1.1
		adm = 3
		dip = 3
		mil = 5
    }
}

291.1.1 = {
	monarch = {
 		name = "Lun"
		dynasty = "Somdok'wo"
		birth_date = 239.1.1
		adm = 4
		dip = 0
		mil = 3
    }
	queen = {
 		name = "Thud"
		dynasty = "Somdok'wo"
		birth_date = 238.1.1
		adm = 0
		dip = 6
		mil = 2
		female = yes
    }
	heir = {
 		name = "Drustho"
		monarch_name = "Drustho I"
		dynasty = "Somdok'wo"
		birth_date = 280.1.1
		death_date = 340.1.1
		claim = 100
		adm = 4
		dip = 4
		mil = 1
		female = yes
    }
}

340.1.1 = {
	monarch = {
 		name = "Shracmur"
		dynasty = "Sagecnar'wo"
		birth_date = 322.1.1
		adm = 0
		dip = 5
		mil = 3
		female = yes
    }
	queen = {
 		name = "Pad"
		dynasty = "Sagecnar'wo"
		birth_date = 316.1.1
		adm = 4
		dip = 4
		mil = 2
    }
	heir = {
 		name = "Somdok"
		monarch_name = "Somdok I"
		dynasty = "Sagecnar'wo"
		birth_date = 339.1.1
		death_date = 422.1.1
		claim = 100
		adm = 1
		dip = 3
		mil = 1
		female = yes
    }
}

422.1.1 = {
	monarch = {
 		name = "Sen"
		dynasty = "Krer'ri"
		birth_date = 400.1.1
		adm = 4
		dip = 4
		mil = 3
    }
}

462.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Fuzzaz"
		monarch_name = "Fuzzaz I"
		dynasty = "Tiar'la"
		birth_date = 454.1.1
		death_date = 472.1.1
		claim = 100
		adm = 4
		dip = 1
		mil = 1
    }
}

472.1.1 = {
	monarch = {
 		name = "Lihya"
		dynasty = "Quv'ri"
		birth_date = 436.1.1
		adm = 3
		dip = 0
		mil = 5
    }
}

515.1.1 = {
	monarch = {
 		name = "Doknog"
		dynasty = "Kontessoor'su"
		birth_date = 466.1.1
		adm = 1
		dip = 6
		mil = 1
    }
}

552.1.1 = {
	monarch = {
 		name = "Remoz"
		dynasty = "Fen'ri"
		birth_date = 533.1.1
		adm = 6
		dip = 6
		mil = 5
    }
	queen = {
 		name = "Sohemool"
		dynasty = "Fen'ri"
		birth_date = 514.1.1
		adm = 3
		dip = 4
		mil = 4
		female = yes
    }
	heir = {
 		name = "Pi"
		monarch_name = "Pi I"
		dynasty = "Fen'ri"
		birth_date = 546.1.1
		death_date = 620.1.1
		claim = 100
		adm = 0
		dip = 3
		mil = 3
		female = yes
    }
}

620.1.1 = {
	monarch = {
 		name = "Chaz"
		dynasty = "Luwume'wo"
		birth_date = 584.1.1
		adm = 6
		dip = 6
		mil = 3
    }
}

673.1.1 = {
	monarch = {
 		name = "Nontrae"
		dynasty = "Pon'su"
		birth_date = 651.1.1
		adm = 4
		dip = 5
		mil = 0
		female = yes
    }
	queen = {
 		name = "Piruhus"
		dynasty = "Pon'su"
		birth_date = 655.1.1
		adm = 1
		dip = 4
		mil = 6
    }
}

759.1.1 = {
	monarch = {
 		name = "Sovoosum"
		dynasty = "Tilnen'la"
		birth_date = 739.1.1
		adm = 0
		dip = 3
		mil = 3
		female = yes
    }
}

824.1.1 = {
	monarch = {
 		name = "Puhak"
		dynasty = "Gakuveets'wo"
		birth_date = 803.1.1
		adm = 5
		dip = 2
		mil = 6
    }
}

899.1.1 = {
	monarch = {
 		name = "Kev"
		dynasty = "Qadun'su"
		birth_date = 855.1.1
		adm = 3
		dip = 1
		mil = 3
    }
}

961.1.1 = {
	monarch = {
 		name = "Par"
		dynasty = "Pi'la"
		birth_date = 920.1.1
		adm = 1
		dip = 1
		mil = 0
    }
}

1059.1.1 = {
	monarch = {
 		name = "Fog"
		dynasty = "Gakuveets'wo"
		birth_date = 1038.1.1
		adm = 0
		dip = 0
		mil = 3
    }
	queen = {
 		name = "Nuyul"
		dynasty = "Gakuveets'wo"
		birth_date = 1006.1.1
		adm = 3
		dip = 5
		mil = 2
		female = yes
    }
	heir = {
 		name = "Rusdevyal"
		monarch_name = "Rusdevyal I"
		dynasty = "Gakuveets'wo"
		birth_date = 1058.1.1
		death_date = 1107.1.1
		claim = 100
		adm = 0
		dip = 4
		mil = 1
		female = yes
    }
}

1107.1.1 = {
	monarch = {
 		name = "Thusdu"
		dynasty = "Sohemool'wo"
		birth_date = 1075.1.1
		adm = 0
		dip = 0
		mil = 1
		female = yes
    }
}

1159.1.1 = {
	monarch = {
 		name = "Gaduyd"
		dynasty = "Therr'la"
		birth_date = 1116.1.1
		adm = 2
		dip = 2
		mil = 4
    }
	queen = {
 		name = "Cisaclol"
		dynasty = "Therr'la"
		birth_date = 1124.1.1
		adm = 6
		dip = 0
		mil = 3
		female = yes
    }
	heir = {
 		name = "Su"
		monarch_name = "Su I"
		dynasty = "Therr'la"
		birth_date = 1152.1.1
		death_date = 1252.1.1
		claim = 100
		adm = 3
		dip = 6
		mil = 2
    }
}

1252.1.1 = {
	monarch = {
 		name = "Se"
		dynasty = "Dusturo'wo"
		birth_date = 1214.1.1
		adm = 6
		dip = 0
		mil = 4
		female = yes
    }
}

1305.1.1 = {
	monarch = {
 		name = "Vag"
		dynasty = "Piruhus'su"
		birth_date = 1281.1.1
		adm = 4
		dip = 6
		mil = 1
    }
}

1374.1.1 = {
	monarch = {
 		name = "Gaduyd"
		dynasty = "Nontrae'la"
		birth_date = 1332.1.1
		adm = 6
		dip = 0
		mil = 2
    }
	queen = {
 		name = "Sentyam"
		dynasty = "Nontrae'la"
		birth_date = 1353.1.1
		adm = 6
		dip = 4
		mil = 3
		female = yes
    }
}

1464.1.1 = {
	monarch = {
 		name = "Lu"
		dynasty = "Susthae'la"
		birth_date = 1427.1.1
		adm = 1
		dip = 5
		mil = 5
		female = yes
    }
}

1543.1.1 = {
	monarch = {
 		name = "De"
		dynasty = "Livosu'ri"
		birth_date = 1500.1.1
		adm = 6
		dip = 4
		mil = 1
		female = yes
    }
	queen = {
 		name = "Ramoz"
		dynasty = "Livosu'ri"
		birth_date = 1493.1.1
		adm = 3
		dip = 3
		mil = 0
    }
	heir = {
 		name = "Beme"
		monarch_name = "Beme I"
		dynasty = "Livosu'ri"
		birth_date = 1543.1.1
		death_date = 1605.1.1
		claim = 100
		adm = 4
		dip = 6
		mil = 1
		female = yes
    }
}

1605.1.1 = {
	monarch = {
 		name = "Nuk"
		dynasty = "De'la"
		birth_date = 1559.1.1
		adm = 3
		dip = 3
		mil = 2
    }
}

1648.1.1 = {
	monarch = {
 		name = "Funded"
		dynasty = "Qusoky'la"
		birth_date = 1629.1.1
		adm = 4
		dip = 0
		mil = 0
    }
}

1683.1.1 = {
	monarch = {
 		name = "Bororr"
		dynasty = "Luhu'la"
		birth_date = 1647.1.1
		adm = 2
		dip = 6
		mil = 3
		female = yes
    }
}

1780.1.1 = {
	monarch = {
 		name = "Fulnod"
		dynasty = "Cin'la"
		birth_date = 1746.1.1
		adm = 0
		dip = 5
		mil = 0
		female = yes
    }
	queen = {
 		name = "Lun"
		dynasty = "Cin'la"
		birth_date = 1745.1.1
		adm = 4
		dip = 4
		mil = 6
    }
	heir = {
 		name = "Pundem"
		monarch_name = "Pundem I"
		dynasty = "Cin'la"
		birth_date = 1766.1.1
		death_date = 1862.1.1
		claim = 100
		adm = 1
		dip = 2
		mil = 5
    }
}

1862.1.1 = {
	monarch = {
 		name = "Delolcok"
		dynasty = "Domdondrul'su"
		birth_date = 1842.1.1
		adm = 4
		dip = 4
		mil = 0
		female = yes
    }
}

1941.1.1 = {
	monarch = {
 		name = "Krakug"
		dynasty = "Krer'ri"
		birth_date = 1906.1.1
		adm = 2
		dip = 3
		mil = 4
    }
	queen = {
 		name = "Dume"
		dynasty = "Krer'ri"
		birth_date = 1921.1.1
		adm = 6
		dip = 1
		mil = 3
		female = yes
    }
}

2026.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Gur"
		monarch_name = "Gur I"
		dynasty = "Tuwurtus'la"
		birth_date = 2025.1.1
		death_date = 2043.1.1
		claim = 100
		adm = 0
		dip = 6
		mil = 1
    }
}

2043.1.1 = {
	monarch = {
 		name = "Qokug"
		dynasty = "Krer'ri"
		birth_date = 2023.1.1
		adm = 2
		dip = 0
		mil = 3
    }
	queen = {
 		name = "Bim"
		dynasty = "Krer'ri"
		birth_date = 2005.1.1
		adm = 6
		dip = 5
		mil = 2
		female = yes
    }
	heir = {
 		name = "Chav"
		monarch_name = "Chav I"
		dynasty = "Krer'ri"
		birth_date = 2036.1.1
		death_date = 2113.1.1
		claim = 100
		adm = 3
		dip = 4
		mil = 1
    }
}

2113.1.1 = {
	monarch = {
 		name = "Listus"
		dynasty = "Bim'la"
		birth_date = 2089.1.1
		adm = 2
		dip = 0
		mil = 1
		female = yes
    }
}

2184.1.1 = {
	monarch = {
 		name = "Gelnar"
		dynasty = "Pan'ri"
		birth_date = 2157.1.1
		adm = 1
		dip = 0
		mil = 5
    }
	queen = {
 		name = "Wolcor"
		dynasty = "Pan'ri"
		birth_date = 2142.1.1
		adm = 5
		dip = 5
		mil = 4
		female = yes
    }
	heir = {
 		name = "Neyak"
		monarch_name = "Neyak I"
		dynasty = "Pan'ri"
		birth_date = 2170.1.1
		death_date = 2231.1.1
		claim = 100
		adm = 1
		dip = 4
		mil = 3
    }
}

2231.1.1 = {
	monarch = {
 		name = "Vag"
		dynasty = "Win'la"
		birth_date = 2186.1.1
		adm = 4
		dip = 5
		mil = 5
    }
	queen = {
 		name = "Dur"
		dynasty = "Win'la"
		birth_date = 2191.1.1
		adm = 1
		dip = 3
		mil = 4
		female = yes
    }
	heir = {
 		name = "Qusoky"
		monarch_name = "Qusoky I"
		dynasty = "Win'la"
		birth_date = 2217.1.1
		death_date = 2290.1.1
		claim = 100
		adm = 5
		dip = 2
		mil = 3
		female = yes
    }
}

2290.1.1 = {
	monarch = {
 		name = "Gelnar"
		dynasty = "Nuyul'wo"
		birth_date = 2258.1.1
		adm = 1
		dip = 3
		mil = 6
    }
	queen = {
 		name = "Luttesso"
		dynasty = "Nuyul'wo"
		birth_date = 2261.1.1
		adm = 4
		dip = 2
		mil = 5
		female = yes
    }
	heir = {
 		name = "Shrugi"
		monarch_name = "Shrugi I"
		dynasty = "Nuyul'wo"
		birth_date = 2287.1.1
		death_date = 2331.1.1
		claim = 100
		adm = 1
		dip = 0
		mil = 4
		female = yes
    }
}

2331.1.1 = {
	monarch = {
 		name = "Sierrosde"
		dynasty = "Piak'su"
		birth_date = 2304.1.1
		adm = 1
		dip = 4
		mil = 4
		female = yes
    }
}

2421.1.1 = {
	monarch = {
 		name = "Thom"
		dynasty = "Krez'su"
		birth_date = 2372.1.1
		adm = 6
		dip = 3
		mil = 0
    }
}

2491.1.1 = {
	monarch = {
 		name = "Tuwurtus"
		dynasty = "Susthae'la"
		birth_date = 2442.1.1
		adm = 4
		dip = 2
		mil = 4
		female = yes
    }
}

