government = native
government_rank = 1
mercantilism = 1
technology_group = atmora_tg
religion = cult_of_ancestors
primary_culture = sea_giant
capital = 2228

54.1.1 = {
	monarch = {
 		name = "Chynis"
		dynasty = "Lagryl"
		birth_date = 14.1.1
		adm = 0
		dip = 5
		mil = 6
    }
}

139.1.1 = {
	monarch = {
 		name = "Theribos"
		dynasty = "Scostem"
		birth_date = 111.1.1
		adm = 6
		dip = 4
		mil = 3
    }
}

190.1.1 = {
	monarch = {
 		name = "Mivaumas"
		dynasty = "Krugneg"
		birth_date = 151.1.1
		adm = 0
		dip = 5
		mil = 4
    }
}

288.1.1 = {
	monarch = {
 		name = "Hilaexo"
		dynasty = "Mallid"
		birth_date = 247.1.1
		adm = 6
		dip = 5
		mil = 1
		female = yes
    }
}

335.1.1 = {
	monarch = {
 		name = "Xaino"
		dynasty = "Byshish"
		birth_date = 295.1.1
		adm = 4
		dip = 4
		mil = 5
		female = yes
    }
}

430.1.1 = {
	monarch = {
 		name = "Nirys"
		dynasty = "Latho"
		birth_date = 392.1.1
		adm = 2
		dip = 3
		mil = 1
    }
}

524.1.1 = {
	monarch = {
 		name = "Thusneus"
		dynasty = "Goklin"
		birth_date = 478.1.1
		adm = 0
		dip = 2
		mil = 5
    }
}

597.1.1 = {
	monarch = {
 		name = "Hules"
		dynasty = "Goklin"
		birth_date = 569.1.1
		adm = 6
		dip = 1
		mil = 2
    }
}

677.1.1 = {
	monarch = {
 		name = "Xaino"
		dynasty = "Subdawr"
		birth_date = 657.1.1
		adm = 4
		dip = 1
		mil = 5
		female = yes
    }
}

762.1.1 = {
	monarch = {
 		name = "Chintos"
		dynasty = "Jaskos"
		birth_date = 729.1.1
		adm = 2
		dip = 0
		mil = 2
    }
}

825.1.1 = {
	monarch = {
 		name = "Thusneus"
		dynasty = "Hobdis"
		birth_date = 794.1.1
		adm = 4
		dip = 1
		mil = 3
    }
}

910.1.1 = {
	monarch = {
 		name = "Thunthuna"
		dynasty = "Lidrur"
		birth_date = 891.1.1
		adm = 2
		dip = 0
		mil = 0
		female = yes
    }
}

974.1.1 = {
	monarch = {
 		name = "Chedyrus"
		dynasty = "Blagmed"
		birth_date = 942.1.1
		adm = 1
		dip = 6
		mil = 4
    }
}

1013.1.1 = {
	monarch = {
 		name = "Thanthaenon"
		dynasty = "Kiruwr"
		birth_date = 979.1.1
		adm = 6
		dip = 6
		mil = 0
    }
}

1049.1.1 = {
	monarch = {
 		name = "Phavose"
		dynasty = "Lazal"
		birth_date = 997.1.1
		adm = 4
		dip = 5
		mil = 4
		female = yes
    }
}

1132.1.1 = {
	monarch = {
 		name = "Vaiktaios"
		dynasty = "Lazal"
		birth_date = 1083.1.1
		adm = 2
		dip = 4
		mil = 1
    }
}

1219.1.1 = {
	monarch = {
 		name = "Kevantos"
		dynasty = "Lidrur"
		birth_date = 1167.1.1
		adm = 1
		dip = 3
		mil = 4
    }
}

1260.1.1 = {
	monarch = {
 		name = "Dimos"
		dynasty = "Cheglyl"
		birth_date = 1215.1.1
		adm = 6
		dip = 3
		mil = 1
    }
}

1312.1.1 = {
	monarch = {
 		name = "Derespo"
		dynasty = "Lazur"
		birth_date = 1293.1.1
		adm = 1
		dip = 4
		mil = 3
		female = yes
    }
}

1375.1.1 = {
	monarch = {
 		name = "Vyala"
		dynasty = "Lebsok"
		birth_date = 1326.1.1
		adm = 6
		dip = 3
		mil = 6
		female = yes
    }
}

1470.1.1 = {
	monarch = {
 		name = "Riserus"
		dynasty = "Large"
		birth_date = 1444.1.1
		adm = 4
		dip = 2
		mil = 3
    }
}

1527.1.1 = {
	monarch = {
 		name = "Zaria"
		dynasty = "Leklyl"
		birth_date = 1500.1.1
		adm = 2
		dip = 1
		mil = 6
		female = yes
    }
}

1602.1.1 = {
	monarch = {
 		name = "Zovor"
		dynasty = "Blagmed"
		birth_date = 1557.1.1
		adm = 1
		dip = 1
		mil = 3
    }
}

1683.1.1 = {
	monarch = {
 		name = "Chrincor"
		dynasty = "Gezon"
		birth_date = 1643.1.1
		adm = 6
		dip = 0
		mil = 0
    }
}

1729.1.1 = {
	monarch = {
 		name = "Riserus"
		dynasty = "Lagryl"
		birth_date = 1678.1.1
		adm = 4
		dip = 6
		mil = 3
    }
}

1772.1.1 = {
	monarch = {
 		name = "Xulotos"
		dynasty = "Trerym"
		birth_date = 1735.1.1
		adm = 2
		dip = 5
		mil = 0
    }
}

1837.1.1 = {
	monarch = {
 		name = "Vagnome"
		dynasty = "Subdawr"
		birth_date = 1819.1.1
		adm = 4
		dip = 6
		mil = 2
		female = yes
    }
}

1888.1.1 = {
	monarch = {
 		name = "Chrincor"
		dynasty = "Gugnak"
		birth_date = 1845.1.1
		adm = 0
		dip = 1
		mil = 4
    }
}

1935.1.1 = {
	monarch = {
 		name = "Haisolus"
		dynasty = "Gugnak"
		birth_date = 1915.1.1
		adm = 5
		dip = 0
		mil = 1
    }
}

1999.1.1 = {
	monarch = {
 		name = "Zistros"
		dynasty = "Kagram"
		birth_date = 1965.1.1
		adm = 4
		dip = 6
		mil = 4
    }
}

2075.1.1 = {
	monarch = {
 		name = "Phidaeus"
		dynasty = "Logmuwr"
		birth_date = 2042.1.1
		adm = 2
		dip = 5
		mil = 1
    }
}

2166.1.1 = {
	monarch = {
 		name = "Kosarda"
		dynasty = "Jabsan"
		birth_date = 2131.1.1
		adm = 4
		dip = 0
		mil = 2
		female = yes
    }
}

2224.1.1 = {
	monarch = {
 		name = "Haisolus"
		dynasty = "Challor"
		birth_date = 2172.1.1
		adm = 2
		dip = 6
		mil = 6
    }
}

2307.1.1 = {
	monarch = {
 		name = "Chetteidon"
		dynasty = "Jaskos"
		birth_date = 2255.1.1
		adm = 0
		dip = 5
		mil = 3
    }
}

2390.1.1 = {
	monarch = {
 		name = "Xispeidon"
		dynasty = "Lykowr"
		birth_date = 2370.1.1
		adm = 5
		dip = 4
		mil = 6
    }
}

2438.1.1 = {
	monarch = {
 		name = "Vekte"
		dynasty = "Grorkywr"
		birth_date = 2412.1.1
		adm = 4
		dip = 4
		mil = 3
		female = yes
    }
}

