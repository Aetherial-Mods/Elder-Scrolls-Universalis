government = republic
government_rank = 5
mercantilism = 1
technology_group = cyrodiil_tg
religion = namira_cult
primary_culture = ayleid
capital = 1194

54.1.1 = {
	monarch = {
 		name = "Djel"
		dynasty = "Ula-Rusifus"
		birth_date = 17.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

125.1.1 = {
	monarch = {
 		name = "Cunylne"
		dynasty = "Ula-Plalusius"
		birth_date = 74.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

223.1.1 = {
	monarch = {
 		name = "Lymma"
		dynasty = "Mea-Corannus"
		birth_date = 178.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

316.1.1 = {
	monarch = {
 		name = "Nilind"
		dynasty = "Mea-Brina"
		birth_date = 284.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

357.1.1 = {
	monarch = {
 		name = "Gand"
		dynasty = "Vae-Statain"
		birth_date = 339.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

432.1.1 = {
	monarch = {
 		name = "Nivi"
		dynasty = "Vae-Tasaso"
		birth_date = 400.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

482.1.1 = {
	monarch = {
 		name = "Lida"
		dynasty = "Vae-Sardavar"
		birth_date = 443.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

569.1.1 = {
	monarch = {
 		name = "Varondil"
		dynasty = "Vae-Weyandawik"
		birth_date = 530.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

621.1.1 = {
	monarch = {
 		name = "Ironwymu"
		dynasty = "Ula-Nornalhorst"
		birth_date = 588.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

663.1.1 = {
	monarch = {
 		name = "Eradhiho"
		dynasty = "Ula-Kvatch"
		birth_date = 644.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

716.1.1 = {
	monarch = {
 		name = "Djamduusceges"
		dynasty = "Mea-Corannus"
		birth_date = 663.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

786.1.1 = {
	monarch = {
 		name = "Ostarand"
		dynasty = "Ula-Kvatch"
		birth_date = 754.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

885.1.1 = {
	monarch = {
 		name = "Ammas"
		dynasty = "Mea-Corannus"
		birth_date = 846.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

974.1.1 = {
	monarch = {
 		name = "Pallush"
		dynasty = "Ula-Nonungalo"
		birth_date = 932.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1026.1.1 = {
	monarch = {
 		name = "Puvusadya"
		dynasty = "Vae-Weyandawik"
		birth_date = 991.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1063.1.1 = {
	monarch = {
 		name = "Nanalne"
		dynasty = "Ula-Homestead"
		birth_date = 1037.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1113.1.1 = {
	monarch = {
 		name = "Synarlysush"
		dynasty = "Ula-Plalusius"
		birth_date = 1076.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1185.1.1 = {
	monarch = {
 		name = "Tjuunrerhen"
		dynasty = "Vae-Waelori"
		birth_date = 1150.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1243.1.1 = {
	monarch = {
 		name = "Cumhul"
		dynasty = "Mea-Harm's"
		birth_date = 1223.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1317.1.1 = {
	monarch = {
 		name = "Enyrrolum"
		dynasty = "Vae-Sardavar"
		birth_date = 1271.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1367.1.1 = {
	monarch = {
 		name = "Liva"
		dynasty = "Vae-Stirk"
		birth_date = 1338.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1453.1.1 = {
	monarch = {
 		name = "Qystur"
		dynasty = "Vae-Sialerius"
		birth_date = 1426.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1534.1.1 = {
	monarch = {
 		name = "Varondil"
		dynasty = "Mea-Brina"
		birth_date = 1487.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1582.1.1 = {
	monarch = {
 		name = "Irgogol"
		dynasty = "Vae-Weyandawik"
		birth_date = 1559.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1657.1.1 = {
	monarch = {
 		name = "Femde"
		dynasty = "Vae-Sena"
		birth_date = 1625.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1701.1.1 = {
	monarch = {
 		name = "Lida"
		dynasty = "Vae-Sylolvia"
		birth_date = 1682.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1755.1.1 = {
	monarch = {
 		name = "Hunro"
		dynasty = "Mea-Correllia"
		birth_date = 1707.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1834.1.1 = {
	monarch = {
 		name = "Fenlord"
		dynasty = "Mea-Allevar"
		birth_date = 1787.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1908.1.1 = {
	monarch = {
 		name = "Lunnu"
		dynasty = "Mea-Corannus"
		birth_date = 1859.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1962.1.1 = {
	monarch = {
 		name = "Helydu"
		dynasty = "Ula-Julidonea"
		birth_date = 1914.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

2021.1.1 = {
	monarch = {
 		name = "Lurdon"
		dynasty = "Vae-Sylolvia"
		birth_date = 1979.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2107.1.1 = {
	monarch = {
 		name = "Curano"
		dynasty = "Mea-Egnagia"
		birth_date = 2076.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

2206.1.1 = {
	monarch = {
 		name = "Lunnu"
		dynasty = "Ula-Kvatch"
		birth_date = 2167.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

2286.1.1 = {
	monarch = {
 		name = "Nula"
		dynasty = "Ula-Renne"
		birth_date = 2256.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

2343.1.1 = {
	monarch = {
 		name = "Cusdar"
		dynasty = "Mea-Adrarcella"
		birth_date = 2320.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2437.1.1 = {
	monarch = {
 		name = "Vanhur"
		dynasty = "Mea-Caumana"
		birth_date = 2390.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2494.1.1 = {
	monarch = {
 		name = "Pemu"
		dynasty = "		"
		birth_date = 2450.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

