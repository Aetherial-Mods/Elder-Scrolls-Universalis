government = monarchy
government_rank = 5
mercantilism = 1
technology_group = elven_tg
religion = hermeus_mora_cult
primary_culture = corelanyan
capital = 1510

54.1.1 = {
	monarch = {
 		name = "Naoi"
		dynasty = "Ousiaann'lac"
		birth_date = 3.1.1
		adm = 0
		dip = 2
		mil = 5
    }
}

141.1.1 = {
	monarch = {
 		name = "Rehilla"
		dynasty = "Vael'tec"
		birth_date = 120.1.1
		adm = 5
		dip = 1
		mil = 2
    }
	queen = {
 		name = "Nessrereeneli"
		dynasty = "Vael'tec"
		birth_date = 120.1.1
		adm = 2
		dip = 6
		mil = 1
		female = yes
    }
}

237.1.1 = {
	monarch = {
 		name = "Nessonnessreaann"
		dynasty = "Vaen'ac"
		birth_date = 195.1.1
		adm = 0
		dip = 6
		mil = 4
    }
}

292.1.1 = {
	monarch = {
 		name = "Naressonen"
		dynasty = "Rescyncaeneli'ac"
		birth_date = 252.1.1
		adm = 0
		dip = 5
		mil = 6
		female = yes
    }
}

369.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Ishall'ui"
		monarch_name = "Ishall'ui I"
		dynasty = "Eousanneli'lac"
		birth_date = 360.1.1
		death_date = 378.1.1
		claim = 100
		adm = 1
		dip = 2
		mil = 3
		female = yes
    }
}

378.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Malla"
		monarch_name = "Malla I"
		dynasty = "Brialu-la'tec"
		birth_date = 364.1.1
		death_date = 382.1.1
		claim = 100
		adm = 6
		dip = 2
		mil = 0
    }
}

382.1.1 = {
	monarch = {
 		name = "D'eaann"
		dynasty = "Teud-shall'sonel'tec"
		birth_date = 354.1.1
		adm = 5
		dip = 5
		mil = 0
    }
}

432.1.1 = {
	monarch = {
 		name = "Aelnalanneli"
		dynasty = "Brialu-la'tec"
		birth_date = 403.1.1
		adm = 4
		dip = 4
		mil = 4
    }
}

526.1.1 = {
	monarch = {
 		name = "Cyui"
		dynasty = "Ousncaa'ar"
		birth_date = 506.1.1
		adm = 2
		dip = 4
		mil = 1
		female = yes
    }
}

592.1.1 = {
	monarch = {
 		name = "Desui"
		dynasty = "Tesi'ac"
		birth_date = 566.1.1
		adm = 0
		dip = 3
		mil = 4
		female = yes
    }
}

672.1.1 = {
	monarch = {
 		name = "Isreel"
		dynasty = "Vaoussrenesui'ac"
		birth_date = 651.1.1
		adm = 5
		dip = 2
		mil = 1
    }
	queen = {
 		name = "Ereeli"
		dynasty = "Vaoussrenesui'ac"
		birth_date = 637.1.1
		adm = 2
		dip = 0
		mil = 0
		female = yes
    }
	heir = {
 		name = "Srehilla"
		monarch_name = "Srehilla I"
		dynasty = "Vaoussrenesui'ac"
		birth_date = 666.1.1
		death_date = 766.1.1
		claim = 100
		adm = 6
		dip = 6
		mil = 6
    }
}

766.1.1 = {
	monarch = {
 		name = "Cyui"
		dynasty = "Evausla'tec"
		birth_date = 723.1.1
		adm = 2
		dip = 0
		mil = 1
		female = yes
    }
}

848.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Athsaanneli"
		monarch_name = "Athsaanneli I"
		dynasty = "Rethalu-ea'ar"
		birth_date = 845.1.1
		death_date = 863.1.1
		claim = 100
		adm = 3
		dip = 4
		mil = 6
    }
}

863.1.1 = {
	monarch = {
 		name = "Teshilvausui"
		dynasty = "Fhaervaanneli'lac"
		birth_date = 826.1.1
		adm = 2
		dip = 1
		mil = 6
		female = yes
    }
	queen = {
 		name = "Briathiousla"
		dynasty = "Fhaervaanneli'lac"
		birth_date = 815.1.1
		adm = 6
		dip = 6
		mil = 5
    }
	heir = {
 		name = "Nala"
		monarch_name = "Nala I"
		dynasty = "Fhaervaanneli'lac"
		birth_date = 850.1.1
		death_date = 939.1.1
		claim = 100
		adm = 6
		dip = 3
		mil = 6
    }
}

939.1.1 = {
	monarch = {
 		name = "Rilvaasae"
		dynasty = "Fhaerle'ar"
		birth_date = 908.1.1
		adm = 6
		dip = 6
		mil = 0
    }
	queen = {
 		name = "Fhaernaaeleneli"
		dynasty = "Fhaerle'ar"
		birth_date = 914.1.1
		adm = 2
		dip = 5
		mil = 6
		female = yes
    }
	heir = {
 		name = "Athsaanneli"
		monarch_name = "Athsaanneli I"
		dynasty = "Fhaerle'ar"
		birth_date = 934.1.1
		death_date = 1025.1.1
		claim = 100
		adm = 6
		dip = 3
		mil = 5
    }
}

1025.1.1 = {
	monarch = {
 		name = "Metesil"
		dynasty = "Aela'ar"
		birth_date = 983.1.1
		adm = 2
		dip = 5
		mil = 0
		female = yes
    }
}

1112.1.1 = {
	monarch = {
 		name = "Usa"
		dynasty = "Srebrisaioa'lac"
		birth_date = 1094.1.1
		adm = 0
		dip = 4
		mil = 4
		female = yes
    }
}

1165.1.1 = {
	monarch = {
 		name = "Iteud-ee"
		dynasty = "Reteud-eli'ar"
		birth_date = 1116.1.1
		adm = 6
		dip = 3
		mil = 0
    }
}

1259.1.1 = {
	monarch = {
 		name = "Ivojhilla"
		dynasty = "Vala'tec"
		birth_date = 1214.1.1
		adm = 0
		dip = 4
		mil = 2
    }
	queen = {
 		name = "Athil"
		dynasty = "Vala'tec"
		birth_date = 1212.1.1
		adm = 1
		dip = 1
		mil = 3
		female = yes
    }
	heir = {
 		name = "Reeshall'u"
		monarch_name = "Reeshall'u I"
		dynasty = "Vala'tec"
		birth_date = 1253.1.1
		death_date = 1346.1.1
		claim = 100
		adm = 5
		dip = 6
		mil = 2
		female = yes
    }
}

1346.1.1 = {
	monarch = {
 		name = "Rethrela"
		dynasty = "Maleli'ar"
		birth_date = 1317.1.1
		adm = 4
		dip = 3
		mil = 2
    }
	queen = {
 		name = "Sausmeeli"
		dynasty = "Maleli'ar"
		birth_date = 1325.1.1
		adm = 1
		dip = 1
		mil = 1
		female = yes
    }
	heir = {
 		name = "Ivojelui"
		monarch_name = "Ivojelui I"
		dynasty = "Maleli'ar"
		birth_date = 1336.1.1
		death_date = 1401.1.1
		claim = 100
		adm = 5
		dip = 0
		mil = 0
    }
}

1401.1.1 = {
	monarch = {
 		name = "Hilvadese"
		dynasty = "Cyasaasaeli'ac"
		birth_date = 1377.1.1
		adm = 0
		dip = 1
		mil = 2
    }
}

1469.1.1 = {
	monarch = {
 		name = "Alu-usouselui"
		dynasty = "Cyasaasaeli'ac"
		birth_date = 1446.1.1
		adm = 6
		dip = 0
		mil = 6
    }
	queen = {
 		name = "Icyeli"
		dynasty = "Cyasaasaeli'ac"
		birth_date = 1436.1.1
		adm = 3
		dip = 6
		mil = 5
		female = yes
    }
	heir = {
 		name = "Resasai"
		monarch_name = "Resasai I"
		dynasty = "Cyasaasaeli'ac"
		birth_date = 1458.1.1
		death_date = 1532.1.1
		claim = 100
		adm = 6
		dip = 4
		mil = 4
		female = yes
    }
}

1532.1.1 = {
	monarch = {
 		name = "Cydesielui"
		dynasty = "Asashall'o'tec"
		birth_date = 1487.1.1
		adm = 2
		dip = 6
		mil = 6
    }
}

1570.1.1 = {
	monarch = {
 		name = "Nesathoi"
		dynasty = "Laieli'ac"
		birth_date = 1533.1.1
		adm = 4
		dip = 0
		mil = 1
    }
}

1631.1.1 = {
	monarch = {
 		name = "Briasaousil"
		dynasty = "Cyioa'lac"
		birth_date = 1590.1.1
		adm = 2
		dip = 6
		mil = 5
		female = yes
    }
	queen = {
 		name = "Srehilla"
		dynasty = "Cyioa'lac"
		birth_date = 1612.1.1
		adm = 6
		dip = 5
		mil = 4
    }
}

1673.1.1 = {
	monarch = {
 		name = "Fhaerianneli"
		dynasty = "Rethd'eee'lac"
		birth_date = 1641.1.1
		adm = 4
		dip = 4
		mil = 0
    }
}

1749.1.1 = {
	monarch = {
 		name = "Sreousteud-ui"
		dynasty = "Fhaera'ar"
		birth_date = 1696.1.1
		adm = 3
		dip = 3
		mil = 4
		female = yes
    }
}

1795.1.1 = {
	monarch = {
 		name = "Hilteseli"
		dynasty = "Brivashall'en'ar"
		birth_date = 1759.1.1
		adm = 1
		dip = 2
		mil = 0
		female = yes
    }
	queen = {
 		name = "Mallaaann"
		dynasty = "Brivashall'en'ar"
		birth_date = 1755.1.1
		adm = 5
		dip = 1
		mil = 6
    }
}

1875.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Athioa"
		monarch_name = "Athioa I"
		dynasty = "Lael'lac"
		birth_date = 1861.1.1
		death_date = 1879.1.1
		claim = 100
		adm = 6
		dip = 5
		mil = 4
    }
}

1879.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Emeelui"
		monarch_name = "Emeelui I"
		dynasty = "Merauie'ac"
		birth_date = 1876.1.1
		death_date = 1894.1.1
		claim = 100
		adm = 4
		dip = 4
		mil = 1
    }
}

1894.1.1 = {
	monarch = {
 		name = "Namalanneli"
		dynasty = "Usu'ar"
		birth_date = 1848.1.1
		adm = 0
		dip = 6
		mil = 3
    }
}

1980.1.1 = {
	monarch = {
 		name = "Athrefhaerol"
		dynasty = "Athsreel'tec"
		birth_date = 1942.1.1
		adm = 5
		dip = 5
		mil = 0
    }
}

2068.1.1 = {
	monarch = {
 		name = "Malathreie"
		dynasty = "Ousncaa'ar"
		birth_date = 2036.1.1
		adm = 3
		dip = 4
		mil = 4
		female = yes
    }
}

2151.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Cyasao"
		monarch_name = "Cyasao I"
		dynasty = "Nesie'ac"
		birth_date = 2143.1.1
		death_date = 2161.1.1
		claim = 100
		adm = 4
		dip = 1
		mil = 2
    }
}

2161.1.1 = {
	monarch = {
 		name = "Namalanneli"
		dynasty = "Evalae'tec"
		birth_date = 2129.1.1
		adm = 3
		dip = 4
		mil = 2
    }
	queen = {
 		name = "Rethmala"
		dynasty = "Evalae'tec"
		birth_date = 2117.1.1
		adm = 0
		dip = 3
		mil = 1
		female = yes
    }
}

2222.1.1 = {
	monarch = {
 		name = "Sateud-sreaann"
		dynasty = "Rescyncaeneli'ac"
		birth_date = 2202.1.1
		adm = 5
		dip = 2
		mil = 4
    }
	queen = {
 		name = "Retha"
		dynasty = "Rescyncaeneli'ac"
		birth_date = 2201.1.1
		adm = 2
		dip = 1
		mil = 3
		female = yes
    }
}

2308.1.1 = {
	monarch = {
 		name = "Vail"
		dynasty = "Varethil'ar"
		birth_date = 2277.1.1
		adm = 0
		dip = 0
		mil = 0
		female = yes
    }
	queen = {
 		name = "Maloi"
		dynasty = "Varethil'ar"
		birth_date = 2256.1.1
		adm = 4
		dip = 5
		mil = 6
    }
}

2379.1.1 = {
	monarch = {
 		name = "Rethouselui"
		dynasty = "Malle'ac"
		birth_date = 2356.1.1
		adm = 3
		dip = 5
		mil = 3
    }
}

2431.1.1 = {
	monarch = {
 		name = "Fhaerianneli"
		dynasty = "Resteud-oi'lac"
		birth_date = 2412.1.1
		adm = 1
		dip = 4
		mil = 6
    }
}

