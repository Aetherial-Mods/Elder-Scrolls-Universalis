government = monarchy
government_rank = 1
mercantilism = 1
technology_group = yokudan_tg
religion = redguard_pantheon
primary_culture = redguard
capital = 1468

54.1.1 = {
	monarch = {
 		name = "Boldon"
		dynasty = "Farmas"
		birth_date = 13.1.1
		adm = 2
		dip = 2
		mil = 4
    }
}

127.1.1 = {
	monarch = {
 		name = "Thahisal"
		dynasty = "Khez-E"
		birth_date = 88.1.1
		adm = 0
		dip = 1
		mil = 1
    }
}

175.1.1 = {
	monarch = {
 		name = "Nanimir"
		dynasty = "Gancasur"
		birth_date = 130.1.1
		adm = 5
		dip = 1
		mil = 5
    }
	queen = {
 		name = "Afadi"
		dynasty = "Gancasur"
		birth_date = 130.1.1
		adm = 2
		dip = 6
		mil = 4
		female = yes
    }
	heir = {
 		name = "Mehdbeq"
		monarch_name = "Mehdbeq I"
		dynasty = "Gancasur"
		birth_date = 170.1.1
		death_date = 220.1.1
		claim = 100
		adm = 6
		dip = 5
		mil = 2
    }
}

220.1.1 = {
	monarch = {
 		name = "Burwa"
		dynasty = "Jartnado"
		birth_date = 184.1.1
		adm = 2
		dip = 6
		mil = 5
		female = yes
    }
	queen = {
 		name = "Ahobi"
		dynasty = "Jartnado"
		birth_date = 172.1.1
		adm = 6
		dip = 4
		mil = 4
    }
	heir = {
 		name = "Mizrayda"
		monarch_name = "Mizrayda I"
		dynasty = "Jartnado"
		birth_date = 213.1.1
		death_date = 312.1.1
		claim = 100
		adm = 3
		dip = 3
		mil = 3
		female = yes
    }
}

312.1.1 = {
	monarch = {
 		name = "Namvar"
		dynasty = "Rhajtha"
		birth_date = 294.1.1
		adm = 2
		dip = 6
		mil = 3
    }
	queen = {
 		name = "Malakeh"
		dynasty = "Rhajtha"
		birth_date = 291.1.1
		adm = 6
		dip = 5
		mil = 2
		female = yes
    }
	heir = {
 		name = "Azad"
		monarch_name = "Azad I"
		dynasty = "Rhajtha"
		birth_date = 309.1.1
		death_date = 348.1.1
		claim = 100
		adm = 6
		dip = 1
		mil = 3
    }
}

348.1.1 = {
	monarch = {
 		name = "Nebzez"
		dynasty = "Greelan"
		birth_date = 314.1.1
		adm = 5
		dip = 5
		mil = 3
    }
}

412.1.1 = {
	monarch = {
 		name = "Jart"
		dynasty = "Nistolm"
		birth_date = 377.1.1
		adm = 4
		dip = 4
		mil = 0
    }
}

461.1.1 = {
	monarch = {
 		name = "Casnar"
		dynasty = "Proanan"
		birth_date = 420.1.1
		adm = 2
		dip = 3
		mil = 4
    }
}

556.1.1 = {
	monarch = {
 		name = "Torson"
		dynasty = "Endotias"
		birth_date = 528.1.1
		adm = 0
		dip = 2
		mil = 0
    }
}

647.1.1 = {
	monarch = {
 		name = "Nayyer"
		dynasty = "Rlogtha"
		birth_date = 623.1.1
		adm = 5
		dip = 2
		mil = 4
		female = yes
    }
	queen = {
 		name = "Marimah"
		dynasty = "Rlogtha"
		birth_date = 616.1.1
		adm = 2
		dip = 0
		mil = 3
    }
	heir = {
 		name = "Bamalen"
		monarch_name = "Bamalen I"
		dynasty = "Rlogtha"
		birth_date = 636.1.1
		death_date = 704.1.1
		claim = 100
		adm = 6
		dip = 6
		mil = 2
    }
}

704.1.1 = {
	monarch = {
 		name = "Carrod"
		dynasty = "Boistim"
		birth_date = 651.1.1
		adm = 6
		dip = 2
		mil = 2
    }
	queen = {
 		name = "Anora"
		dynasty = "Boistim"
		birth_date = 659.1.1
		adm = 2
		dip = 1
		mil = 1
		female = yes
    }
	heir = {
 		name = "Mulimah"
		monarch_name = "Mulimah I"
		dynasty = "Boistim"
		birth_date = 696.1.1
		death_date = 755.1.1
		claim = 100
		adm = 3
		dip = 4
		mil = 2
		female = yes
    }
}

755.1.1 = {
	monarch = {
 		name = "Delia"
		dynasty = "Chref"
		birth_date = 733.1.1
		adm = 2
		dip = 1
		mil = 2
		female = yes
    }
}

844.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Bail"
		monarch_name = "Bail I"
		dynasty = "Manielan"
		birth_date = 832.1.1
		death_date = 850.1.1
		claim = 100
		adm = 3
		dip = 5
		mil = 0
    }
}

850.1.1 = {
	monarch = {
 		name = "Nudbahil"
		dynasty = "Fairgki"
		birth_date = 814.1.1
		adm = 5
		dip = 6
		mil = 3
    }
	queen = {
 		name = "Esmee"
		dynasty = "Fairgki"
		birth_date = 823.1.1
		adm = 2
		dip = 4
		mil = 2
		female = yes
    }
	heir = {
 		name = "Muheh"
		monarch_name = "Muheh I"
		dynasty = "Fairgki"
		birth_date = 839.1.1
		death_date = 920.1.1
		claim = 100
		adm = 6
		dip = 3
		mil = 1
		female = yes
    }
}

920.1.1 = {
	monarch = {
 		name = "Dajeh"
		dynasty = "Fortum"
		birth_date = 884.1.1
		adm = 2
		dip = 4
		mil = 3
    }
}

967.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Namvar"
		monarch_name = "Namvar I"
		dynasty = "Delmin"
		birth_date = 963.1.1
		death_date = 981.1.1
		claim = 100
		adm = 3
		dip = 1
		mil = 1
    }
}

981.1.1 = {
	monarch = {
 		name = "Nistacey"
		dynasty = "Jinrgel"
		birth_date = 949.1.1
		adm = 2
		dip = 5
		mil = 1
    }
}

1045.1.1 = {
	monarch = {
 		name = "Toura"
		dynasty = "Stifyli"
		birth_date = 994.1.1
		adm = 0
		dip = 4
		mil = 5
		female = yes
    }
}

1131.1.1 = {
	monarch = {
 		name = "Paldezh"
		dynasty = "Nathe"
		birth_date = 1098.1.1
		adm = 6
		dip = 3
		mil = 1
    }
	queen = {
 		name = "Atefah"
		dynasty = "Nathe"
		birth_date = 1113.1.1
		adm = 2
		dip = 2
		mil = 0
		female = yes
    }
}

1204.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Dhul"
		monarch_name = "Dhul I"
		dynasty = "Kithithon"
		birth_date = 1201.1.1
		death_date = 1219.1.1
		claim = 100
		adm = 3
		dip = 6
		mil = 5
    }
}

1219.1.1 = {
	monarch = {
 		name = "Rajelar"
		dynasty = "M'urcti"
		birth_date = 1181.1.1
		adm = 6
		dip = 0
		mil = 1
    }
}

1271.1.1 = {
	monarch = {
 		name = "Khayr"
		dynasty = "Dindal"
		birth_date = 1248.1.1
		adm = 4
		dip = 6
		mil = 4
    }
	queen = {
 		name = "Sinfay"
		dynasty = "Dindal"
		birth_date = 1222.1.1
		adm = 1
		dip = 5
		mil = 3
		female = yes
    }
	heir = {
 		name = "Karayyah"
		monarch_name = "Karayyah I"
		dynasty = "Dindal"
		birth_date = 1271.1.1
		death_date = 1339.1.1
		claim = 100
		adm = 5
		dip = 3
		mil = 2
		female = yes
    }
}

1339.1.1 = {
	monarch = {
 		name = "Zakavit"
		dynasty = "Shartta"
		birth_date = 1314.1.1
		adm = 1
		dip = 5
		mil = 5
    }
}

1374.1.1 = {
	monarch = {
 		name = "Raifa"
		dynasty = "Trateve"
		birth_date = 1321.1.1
		adm = 3
		dip = 6
		mil = 6
    }
}

1410.1.1 = {
	monarch = {
 		name = "Khammo"
		dynasty = "Dindal"
		birth_date = 1360.1.1
		adm = 1
		dip = 5
		mil = 3
    }
	queen = {
 		name = "Jaeloreh"
		dynasty = "Dindal"
		birth_date = 1362.1.1
		adm = 5
		dip = 4
		mil = 2
		female = yes
    }
}

1499.1.1 = {
	monarch = {
 		name = "Sabhell"
		dynasty = "Nisir"
		birth_date = 1469.1.1
		adm = 3
		dip = 3
		mil = 5
    }
}

1586.1.1 = {
	monarch = {
 		name = "Sundurah"
		dynasty = "Nissan"
		birth_date = 1539.1.1
		adm = 1
		dip = 2
		mil = 2
		female = yes
    }
	queen = {
 		name = "Salyazh"
		dynasty = "Nissan"
		birth_date = 1551.1.1
		adm = 5
		dip = 1
		mil = 1
    }
	heir = {
 		name = "Zilu"
		monarch_name = "Zilu I"
		dynasty = "Nissan"
		birth_date = 1572.1.1
		death_date = 1658.1.1
		claim = 100
		adm = 2
		dip = 6
		mil = 0
    }
}

1658.1.1 = {
	monarch = {
 		name = "Mani"
		dynasty = "Jateif"
		birth_date = 1634.1.1
		adm = 5
		dip = 1
		mil = 2
    }
	queen = {
 		name = "Umana"
		dynasty = "Jateif"
		birth_date = 1640.1.1
		adm = 2
		dip = 6
		mil = 1
		female = yes
    }
}

1730.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Alburz"
		monarch_name = "Alburz I"
		dynasty = "Fortum"
		birth_date = 1726.1.1
		death_date = 1744.1.1
		claim = 100
		adm = 2
		dip = 3
		mil = 6
    }
}

1744.1.1 = {
	monarch = {
 		name = "Mujwadeen"
		dynasty = "Endotias"
		birth_date = 1694.1.1
		adm = 5
		dip = 5
		mil = 2
    }
}

1806.1.1 = {
	monarch = {
 		name = "Hurien"
		dynasty = "Irgher"
		birth_date = 1755.1.1
		adm = 3
		dip = 4
		mil = 5
    }
}

1891.1.1 = {
	monarch = {
 		name = "Azzan"
		dynasty = "Khirdrn"
		birth_date = 1843.1.1
		adm = 2
		dip = 3
		mil = 2
    }
}

1958.1.1 = {
	monarch = {
 		name = "Sukelan"
		dynasty = "Nissan"
		birth_date = 1929.1.1
		adm = 3
		dip = 4
		mil = 4
    }
	queen = {
 		name = "Rithleen"
		dynasty = "Nissan"
		birth_date = 1929.1.1
		adm = 4
		dip = 1
		mil = 5
		female = yes
    }
	heir = {
 		name = "Harukar"
		monarch_name = "Harukar I"
		dynasty = "Nissan"
		birth_date = 1951.1.1
		death_date = 1996.1.1
		claim = 100
		adm = 1
		dip = 6
		mil = 4
    }
}

1996.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Amren"
		monarch_name = "Amren I"
		dynasty = "Curorter"
		birth_date = 1988.1.1
		death_date = 2006.1.1
		claim = 100
		adm = 6
		dip = 5
		mil = 0
    }
}

2006.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Shabhehm"
		monarch_name = "Shabhehm I"
		dynasty = "Flargir"
		birth_date = 1994.1.1
		death_date = 2012.1.1
		claim = 100
		adm = 1
		dip = 0
		mil = 2
    }
}

2012.1.1 = {
	monarch = {
 		name = "Inaya"
		dynasty = "Endotias"
		birth_date = 1968.1.1
		adm = 3
		dip = 1
		mil = 4
		female = yes
    }
	queen = {
 		name = "Rhadshathir"
		dynasty = "Endotias"
		birth_date = 1968.1.1
		adm = 0
		dip = 0
		mil = 3
    }
	heir = {
 		name = "Hanneh"
		monarch_name = "Hanneh I"
		dynasty = "Endotias"
		birth_date = 2011.1.1
		death_date = 2109.1.1
		claim = 100
		adm = 3
		dip = 1
		mil = 5
		female = yes
    }
}

2109.1.1 = {
	monarch = {
 		name = "Fabien"
		dynasty = "Owyada"
		birth_date = 2067.1.1
		adm = 2
		dip = 4
		mil = 6
    }
	queen = {
 		name = "Nadwa"
		dynasty = "Owyada"
		birth_date = 2072.1.1
		adm = 6
		dip = 3
		mil = 5
		female = yes
    }
}

2204.1.1 = {
	monarch = {
 		name = "Mahtab"
		dynasty = "Sticert"
		birth_date = 2153.1.1
		adm = 5
		dip = 2
		mil = 1
		female = yes
    }
	queen = {
 		name = "Varnado"
		dynasty = "Sticert"
		birth_date = 2186.1.1
		adm = 1
		dip = 1
		mil = 0
    }
}

2261.1.1 = {
	monarch = {
 		name = "Saymimah"
		dynasty = "Fafyler"
		birth_date = 2214.1.1
		adm = 0
		dip = 0
		mil = 4
		female = yes
    }
	queen = {
 		name = "Eshundir"
		dynasty = "Fafyler"
		birth_date = 2216.1.1
		adm = 4
		dip = 5
		mil = 3
    }
	heir = {
 		name = "Runid"
		monarch_name = "Runid I"
		dynasty = "Fafyler"
		birth_date = 2250.1.1
		death_date = 2323.1.1
		claim = 100
		adm = 0
		dip = 4
		mil = 2
    }
}

2323.1.1 = {
	monarch = {
 		name = "Haythehdi"
		dynasty = "Trateve"
		birth_date = 2300.1.1
		adm = 3
		dip = 5
		mil = 4
    }
}

2398.1.1 = {
	monarch = {
 		name = "Raccan"
		dynasty = "Curtyvond"
		birth_date = 2368.1.1
		adm = 3
		dip = 2
		mil = 4
    }
}

2455.1.1 = {
	monarch = {
 		name = "Zerh"
		dynasty = "Jeleadal"
		birth_date = 2423.1.1
		adm = 1
		dip = 1
		mil = 1
    }
}

