government = monarchy
government_rank = 5
mercantilism = 1
technology_group = cyrodiil_tg
religion = hircine_cult
primary_culture = nedic
capital = 5875

54.1.1 = {
	monarch = {
 		name = "Emah"
		dynasty = "Menhadeth"
		birth_date = 28.1.1
		adm = 3
		dip = 2
		mil = 3
		female = yes
    }
}

123.1.1 = {
	monarch = {
 		name = "Tirdor"
		dynasty = "Ralreloc"
		birth_date = 89.1.1
		adm = 1
		dip = 1
		mil = 0
    }
	queen = {
 		name = "Dame"
		dynasty = "Ralreloc"
		birth_date = 91.1.1
		adm = 5
		dip = 0
		mil = 6
		female = yes
    }
	heir = {
 		name = "Nhekhedin"
		monarch_name = "Nhekhedin I"
		dynasty = "Ralreloc"
		birth_date = 109.1.1
		death_date = 194.1.1
		claim = 100
		adm = 2
		dip = 5
		mil = 5
    }
}

194.1.1 = {
	monarch = {
 		name = "Rardoc"
		dynasty = "Gilraoles"
		birth_date = 176.1.1
		adm = 1
		dip = 1
		mil = 5
    }
	queen = {
 		name = "Terenu"
		dynasty = "Gilraoles"
		birth_date = 160.1.1
		adm = 5
		dip = 0
		mil = 4
		female = yes
    }
	heir = {
 		name = "Dezone"
		monarch_name = "Dezone I"
		dynasty = "Gilraoles"
		birth_date = 189.1.1
		death_date = 246.1.1
		claim = 100
		adm = 5
		dip = 3
		mil = 5
		female = yes
    }
}

246.1.1 = {
	monarch = {
 		name = "Zeveduv"
		dynasty = "Zerlec"
		birth_date = 198.1.1
		adm = 5
		dip = 0
		mil = 6
		female = yes
    }
	queen = {
 		name = "Gakaazin"
		dynasty = "Zerlec"
		birth_date = 203.1.1
		adm = 1
		dip = 5
		mil = 5
    }
	heir = {
 		name = "Nhekhedin"
		monarch_name = "Nhekhedin I"
		dynasty = "Zerlec"
		birth_date = 240.1.1
		death_date = 321.1.1
		claim = 100
		adm = 5
		dip = 4
		mil = 4
    }
}

321.1.1 = {
	monarch = {
 		name = "Desylis"
		dynasty = "Bauvmith"
		birth_date = 273.1.1
		adm = 4
		dip = 3
		mil = 0
    }
	queen = {
 		name = "Ossonel"
		dynasty = "Bauvmith"
		birth_date = 294.1.1
		adm = 0
		dip = 2
		mil = 6
		female = yes
    }
	heir = {
 		name = "Vallal"
		monarch_name = "Vallal I"
		dynasty = "Bauvmith"
		birth_date = 310.1.1
		death_date = 408.1.1
		claim = 100
		adm = 4
		dip = 0
		mil = 5
		female = yes
    }
}

408.1.1 = {
	monarch = {
 		name = "Demer"
		dynasty = "Nuvnizo"
		birth_date = 370.1.1
		adm = 0
		dip = 2
		mil = 0
		female = yes
    }
}

482.1.1 = {
	monarch = {
 		name = "Vaulic"
		dynasty = "Muvmec"
		birth_date = 438.1.1
		adm = 5
		dip = 1
		mil = 4
    }
	queen = {
 		name = "Zeli"
		dynasty = "Muvmec"
		birth_date = 430.1.1
		adm = 5
		dip = 4
		mil = 4
		female = yes
    }
	heir = {
 		name = "Lyrmerek"
		monarch_name = "Lyrmerek I"
		dynasty = "Muvmec"
		birth_date = 473.1.1
		death_date = 560.1.1
		claim = 100
		adm = 2
		dip = 3
		mil = 3
    }
}

560.1.1 = {
	monarch = {
 		name = "Dauhrak"
		dynasty = "Vinhylak"
		birth_date = 521.1.1
		adm = 4
		dip = 4
		mil = 6
    }
	queen = {
 		name = "Romale"
		dynasty = "Vinhylak"
		birth_date = 513.1.1
		adm = 1
		dip = 3
		mil = 5
		female = yes
    }
	heir = {
 		name = "Amedoc"
		monarch_name = "Amedoc I"
		dynasty = "Vinhylak"
		birth_date = 554.1.1
		death_date = 632.1.1
		claim = 100
		adm = 5
		dip = 1
		mil = 4
    }
}

632.1.1 = {
	monarch = {
 		name = "Tirleros"
		dynasty = "Halaanis"
		birth_date = 597.1.1
		adm = 1
		dip = 2
		mil = 6
    }
}

678.1.1 = {
	monarch = {
 		name = "Nuvner"
		dynasty = "Haonuvic"
		birth_date = 654.1.1
		adm = 6
		dip = 2
		mil = 3
    }
}

744.1.1 = {
	monarch = {
 		name = "Kynhesac"
		dynasty = "Shelren"
		birth_date = 699.1.1
		adm = 1
		dip = 3
		mil = 4
    }
	queen = {
 		name = "Norri"
		dynasty = "Shelren"
		birth_date = 700.1.1
		adm = 1
		dip = 6
		mil = 5
		female = yes
    }
	heir = {
 		name = "Lethadu"
		monarch_name = "Lethadu I"
		dynasty = "Shelren"
		birth_date = 734.1.1
		death_date = 810.1.1
		claim = 100
		adm = 5
		dip = 5
		mil = 4
		female = yes
    }
}

810.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Lihareh"
		monarch_name = "Lihareh I"
		dynasty = "Nhudren"
		birth_date = 795.1.1
		death_date = 813.1.1
		claim = 100
		adm = 3
		dip = 4
		mil = 1
		female = yes
    }
}

813.1.1 = {
	monarch = {
 		name = "Osibin"
		dynasty = "Nhekhedin"
		birth_date = 793.1.1
		adm = 5
		dip = 5
		mil = 2
		female = yes
    }
}

860.1.1 = {
	monarch = {
 		name = "Sonev"
		dynasty = "Dadrades"
		birth_date = 814.1.1
		adm = 3
		dip = 5
		mil = 6
		female = yes
    }
	queen = {
 		name = "Daolo"
		dynasty = "Dadrades"
		birth_date = 819.1.1
		adm = 0
		dip = 3
		mil = 5
    }
}

948.1.1 = {
	monarch = {
 		name = "Menhadeth"
		dynasty = "Menhadeth"
		birth_date = 897.1.1
		adm = 5
		dip = 2
		mil = 2
    }
	queen = {
 		name = "Resa"
		dynasty = "Menhadeth"
		birth_date = 907.1.1
		adm = 2
		dip = 1
		mil = 1
		female = yes
    }
	heir = {
 		name = "Bardek"
		monarch_name = "Bardek I"
		dynasty = "Menhadeth"
		birth_date = 936.1.1
		death_date = 1024.1.1
		claim = 100
		adm = 3
		dip = 4
		mil = 2
    }
}

1024.1.1 = {
	monarch = {
 		name = "Ronah"
		dynasty = "Nhudren"
		birth_date = 1002.1.1
		adm = 2
		dip = 1
		mil = 2
		female = yes
    }
	queen = {
 		name = "Naanuzar"
		dynasty = "Nhudren"
		birth_date = 1001.1.1
		adm = 6
		dip = 6
		mil = 1
    }
}

1078.1.1 = {
	monarch = {
 		name = "Vauleso"
		dynasty = "Gumrok"
		birth_date = 1055.1.1
		adm = 4
		dip = 6
		mil = 5
    }
}

1140.1.1 = {
	monarch = {
 		name = "Nirlyloth"
		dynasty = "Kaomrar"
		birth_date = 1104.1.1
		adm = 2
		dip = 5
		mil = 1
    }
}

1219.1.1 = {
	monarch = {
 		name = "Vaorylan"
		dynasty = "Naamaulec"
		birth_date = 1180.1.1
		adm = 1
		dip = 4
		mil = 5
    }
	queen = {
 		name = "Hotedi"
		dynasty = "Naamaulec"
		birth_date = 1170.1.1
		adm = 4
		dip = 2
		mil = 4
		female = yes
    }
	heir = {
 		name = "Gakaazin"
		monarch_name = "Gakaazin I"
		dynasty = "Naamaulec"
		birth_date = 1211.1.1
		death_date = 1263.1.1
		claim = 100
		adm = 1
		dip = 1
		mil = 3
    }
}

1263.1.1 = {
	monarch = {
 		name = "Vauleso"
		dynasty = "Tirlaasir"
		birth_date = 1221.1.1
		adm = 4
		dip = 2
		mil = 5
    }
	queen = {
 		name = "Lerrimiv"
		dynasty = "Tirlaasir"
		birth_date = 1219.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1341.1.1 = {
	monarch = {
 		name = "Ludec"
		dynasty = "Vaulic"
		birth_date = 1313.1.1
		adm = 6
		dip = 0
		mil = 1
    }
	queen = {
 		name = "Masehur"
		dynasty = "Vaulic"
		birth_date = 1293.1.1
		adm = 3
		dip = 6
		mil = 0
		female = yes
    }
	heir = {
 		name = "Vinhylak"
		monarch_name = "Vinhylak I"
		dynasty = "Vaulic"
		birth_date = 1339.1.1
		death_date = 1397.1.1
		claim = 100
		adm = 0
		dip = 4
		mil = 6
    }
}

1397.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Tirleros"
		monarch_name = "Tirleros I"
		dynasty = "Hyker"
		birth_date = 1395.1.1
		death_date = 1413.1.1
		claim = 100
		adm = 5
		dip = 3
		mil = 2
    }
}

1413.1.1 = {
	monarch = {
 		name = "Taaner"
		dynasty = "Nirlyloth"
		birth_date = 1375.1.1
		adm = 5
		dip = 0
		mil = 3
    }
	queen = {
 		name = "Dame"
		dynasty = "Nirlyloth"
		birth_date = 1386.1.1
		adm = 1
		dip = 5
		mil = 2
		female = yes
    }
	heir = {
 		name = "Kynhesac"
		monarch_name = "Kynhesac I"
		dynasty = "Nirlyloth"
		birth_date = 1407.1.1
		death_date = 1469.1.1
		claim = 100
		adm = 5
		dip = 4
		mil = 1
    }
}

1469.1.1 = {
	monarch = {
 		name = "Lukaazon"
		dynasty = "Dyrmec"
		birth_date = 1422.1.1
		adm = 4
		dip = 3
		mil = 4
    }
	queen = {
 		name = "Lassoba"
		dynasty = "Dyrmec"
		birth_date = 1437.1.1
		adm = 0
		dip = 2
		mil = 3
		female = yes
    }
	heir = {
 		name = "Herlir"
		monarch_name = "Herlir I"
		dynasty = "Dyrmec"
		birth_date = 1469.1.1
		death_date = 1509.1.1
		claim = 100
		adm = 1
		dip = 5
		mil = 4
    }
}

1509.1.1 = {
	monarch = {
 		name = "Issaeh"
		dynasty = "Gakaazin"
		birth_date = 1469.1.1
		adm = 0
		dip = 1
		mil = 5
		female = yes
    }
}

1598.1.1 = {
	monarch = {
 		name = "Havu"
		dynasty = "Tamedir"
		birth_date = 1570.1.1
		adm = 5
		dip = 1
		mil = 1
		female = yes
    }
	queen = {
 		name = "Zurduzok"
		dynasty = "Tamedir"
		birth_date = 1569.1.1
		adm = 5
		dip = 4
		mil = 1
    }
	heir = {
 		name = "Tadak"
		monarch_name = "Tadak I"
		dynasty = "Tamedir"
		birth_date = 1591.1.1
		death_date = 1677.1.1
		claim = 100
		adm = 5
		dip = 1
		mil = 2
    }
}

1677.1.1 = {
	monarch = {
 		name = "Rardoc"
		dynasty = "Nuvnizo"
		birth_date = 1643.1.1
		adm = 4
		dip = 4
		mil = 3
    }
}

1736.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Zyrdusor"
		monarch_name = "Zyrdusor I"
		dynasty = "Tylak"
		birth_date = 1728.1.1
		death_date = 1746.1.1
		claim = 100
		adm = 5
		dip = 1
		mil = 1
    }
}

1746.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Gumrok"
		monarch_name = "Gumrok I"
		dynasty = "Gaohen"
		birth_date = 1731.1.1
		death_date = 1749.1.1
		claim = 100
		adm = 3
		dip = 0
		mil = 4
    }
}

1749.1.1 = {
	monarch = {
 		name = "Daolo"
		dynasty = "Vaorylan"
		birth_date = 1711.1.1
		adm = 6
		dip = 2
		mil = 0
    }
	queen = {
 		name = "Mazan"
		dynasty = "Vaorylan"
		birth_date = 1699.1.1
		adm = 3
		dip = 0
		mil = 6
		female = yes
    }
	heir = {
 		name = "Gaohen"
		monarch_name = "Gaohen I"
		dynasty = "Vaorylan"
		birth_date = 1740.1.1
		death_date = 1822.1.1
		claim = 100
		adm = 0
		dip = 6
		mil = 5
    }
}

1822.1.1 = {
	monarch = {
 		name = "Ihrasek"
		dynasty = "Nuvnizo"
		birth_date = 1786.1.1
		adm = 6
		dip = 2
		mil = 5
    }
}

1907.1.1 = {
	monarch = {
 		name = "Serrive"
		dynasty = "Vredraonir"
		birth_date = 1880.1.1
		adm = 4
		dip = 1
		mil = 2
		female = yes
    }
}

1986.1.1 = {
	monarch = {
 		name = "Zurduzok"
		dynasty = "Tamedir"
		birth_date = 1953.1.1
		adm = 3
		dip = 0
		mil = 5
    }
}

2053.1.1 = {
	monarch = {
 		name = "Tirdor"
		dynasty = "Bardek"
		birth_date = 2008.1.1
		adm = 1
		dip = 0
		mil = 2
    }
	queen = {
 		name = "Dennida"
		dynasty = "Bardek"
		birth_date = 2013.1.1
		adm = 5
		dip = 5
		mil = 1
		female = yes
    }
	heir = {
 		name = "Vaorylan"
		monarch_name = "Vaorylan I"
		dynasty = "Bardek"
		birth_date = 2053.1.1
		death_date = 2092.1.1
		claim = 100
		adm = 2
		dip = 4
		mil = 0
    }
}

2092.1.1 = {
	monarch = {
 		name = "Hyker"
		dynasty = "Nirdoc"
		birth_date = 2045.1.1
		adm = 0
		dip = 3
		mil = 3
    }
	queen = {
 		name = "Emah"
		dynasty = "Nirdoc"
		birth_date = 2059.1.1
		adm = 4
		dip = 1
		mil = 2
		female = yes
    }
	heir = {
 		name = "Viladuh"
		monarch_name = "Viladuh I"
		dynasty = "Nirdoc"
		birth_date = 2080.1.1
		death_date = 2189.1.1
		claim = 100
		adm = 1
		dip = 0
		mil = 1
		female = yes
    }
}

2189.1.1 = {
	monarch = {
 		name = "Letan"
		dynasty = "Naonhaudes"
		birth_date = 2136.1.1
		adm = 3
		dip = 1
		mil = 4
    }
	queen = {
 		name = "Tome"
		dynasty = "Naonhaudes"
		birth_date = 2164.1.1
		adm = 0
		dip = 0
		mil = 3
		female = yes
    }
	heir = {
 		name = "Haonuvic"
		monarch_name = "Haonuvic I"
		dynasty = "Naonhaudes"
		birth_date = 2174.1.1
		death_date = 2254.1.1
		claim = 100
		adm = 4
		dip = 5
		mil = 2
    }
}

2254.1.1 = {
	monarch = {
 		name = "Nestith"
		dynasty = "Nhihreseth"
		birth_date = 2232.1.1
		adm = 0
		dip = 0
		mil = 4
    }
}

2339.1.1 = {
	monarch = {
 		name = "Omidiv"
		dynasty = "Vinhylak"
		birth_date = 2317.1.1
		adm = 5
		dip = 6
		mil = 1
		female = yes
    }
}

2396.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Tudaarin"
		monarch_name = "Tudaarin I"
		dynasty = "Amedoc"
		birth_date = 2389.1.1
		death_date = 2407.1.1
		claim = 100
		adm = 6
		dip = 3
		mil = 6
    }
}

2407.1.1 = {
	monarch = {
 		name = "Lysteth"
		dynasty = "Taaner"
		birth_date = 2389.1.1
		adm = 5
		dip = 6
		mil = 6
    }
	queen = {
 		name = "Nemehul"
		dynasty = "Taaner"
		birth_date = 2384.1.1
		adm = 1
		dip = 1
		mil = 1
		female = yes
    }
}

2467.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Shelren"
		monarch_name = "Shelren I"
		dynasty = "Vrilak"
		birth_date = 2454.1.1
		death_date = 2472.1.1
		claim = 100
		adm = 2
		dip = 5
		mil = 6
    }
}

2472.1.1 = {
	monarch = {
 		name = "Zeveduv"
		dynasty = "Nhihreseth"
		birth_date = 2451.1.1
		adm = 0
		dip = 4
		mil = 3
		female = yes
    }
	queen = {
 		name = "Yhin"
		dynasty = "Nhihreseth"
		birth_date = 2449.1.1
		adm = 4
		dip = 3
		mil = 2
    }
}

