government = monarchy
government_rank = 5
mercantilism = 1
technology_group = akavir_tg
religion = dragon_cult
primary_culture = akaviri
capital = 750

54.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Rhaenosia"
		monarch_name = "Rhaenosia I"
		dynasty = "Vi-Savariphei"
		birth_date = 49.1.1
		death_date = 67.1.1
		claim = 100
		adm = 5
		dip = 3
		mil = 5
		female = yes
    }
}

67.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Typhaus"
		monarch_name = "Typhaus I"
		dynasty = "Vi-Theanionis"
		birth_date = 66.1.1
		death_date = 84.1.1
		claim = 100
		adm = 3
		dip = 2
		mil = 2
    }
}

84.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Antiphoebus"
		monarch_name = "Antiphoebus I"
		dynasty = "Vi-Rhaeniophai"
		birth_date = 83.1.1
		death_date = 101.1.1
		claim = 100
		adm = 3
		dip = 5
		mil = 2
    }
}

101.1.1 = {
	monarch = {
 		name = "Ashataus"
		dynasty = "Di-Athixus"
		birth_date = 61.1.1
		adm = 6
		dip = 0
		mil = 4
    }
}

164.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Zalisthus"
		monarch_name = "Zalisthus I"
		dynasty = "Vi-Othrithoe"
		birth_date = 154.1.1
		death_date = 172.1.1
		claim = 100
		adm = 6
		dip = 4
		mil = 2
    }
}

172.1.1 = {
	monarch = {
 		name = "Paphopheu"
		dynasty = "Du-Proteserios"
		birth_date = 142.1.1
		adm = 5
		dip = 3
		mil = 6
		female = yes
    }
	queen = {
 		name = "Eustolos"
		dynasty = "Du-Proteserios"
		birth_date = 133.1.1
		adm = 1
		dip = 1
		mil = 5
    }
	heir = {
 		name = "Antiphoebus"
		monarch_name = "Antiphoebus I"
		dynasty = "Du-Proteserios"
		birth_date = 168.1.1
		death_date = 263.1.1
		claim = 100
		adm = 5
		dip = 0
		mil = 4
    }
}

263.1.1 = {
	monarch = {
 		name = "Pherishae"
		dynasty = "Di-Basos"
		birth_date = 221.1.1
		adm = 1
		dip = 1
		mil = 6
		female = yes
    }
}

325.1.1 = {
	monarch = {
 		name = "Xerxelix"
		dynasty = "Vi-Crepheasi"
		birth_date = 304.1.1
		adm = 6
		dip = 1
		mil = 3
    }
	queen = {
 		name = "Ophiali"
		dynasty = "Vi-Crepheasi"
		birth_date = 285.1.1
		adm = 3
		dip = 6
		mil = 2
		female = yes
    }
}

375.1.1 = {
	monarch = {
 		name = "Oriphephia"
		dynasty = "Vi-Rhaenosia"
		birth_date = 353.1.1
		adm = 1
		dip = 5
		mil = 6
		female = yes
    }
}

435.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Anasztochus"
		monarch_name = "Anasztochus I"
		dynasty = "Du-Caesanthus"
		birth_date = 425.1.1
		death_date = 443.1.1
		claim = 100
		adm = 2
		dip = 2
		mil = 4
    }
}

443.1.1 = {
	monarch = {
 		name = "Proteserios"
		dynasty = "Vi-Nethice"
		birth_date = 394.1.1
		adm = 2
		dip = 6
		mil = 4
    }
}

512.1.1 = {
	monarch = {
 		name = "Akisyxio"
		dynasty = "Du-Thitous"
		birth_date = 467.1.1
		adm = 0
		dip = 5
		mil = 0
		female = yes
    }
	queen = {
 		name = "Demithaeus"
		dynasty = "Du-Thitous"
		birth_date = 491.1.1
		adm = 4
		dip = 3
		mil = 6
    }
	heir = {
 		name = "Arsax"
		monarch_name = "Arsax I"
		dynasty = "Du-Thitous"
		birth_date = 510.1.1
		death_date = 589.1.1
		claim = 100
		adm = 4
		dip = 0
		mil = 0
    }
}

589.1.1 = {
	monarch = {
 		name = "Absyristhus"
		dynasty = "Vi-Mystymes"
		birth_date = 555.1.1
		adm = 3
		dip = 3
		mil = 1
    }
}

668.1.1 = {
	monarch = {
 		name = "Erasmys"
		dynasty = "Di-Iphitelous"
		birth_date = 615.1.1
		adm = 2
		dip = 3
		mil = 4
    }
	queen = {
 		name = "Theanionis"
		dynasty = "Di-Iphitelous"
		birth_date = 620.1.1
		adm = 5
		dip = 1
		mil = 3
		female = yes
    }
}

713.1.1 = {
	monarch = {
 		name = "Siliphi"
		dynasty = "Vi-Mystophi"
		birth_date = 674.1.1
		adm = 4
		dip = 0
		mil = 0
		female = yes
    }
	queen = {
 		name = "Arsax"
		dynasty = "Vi-Mystophi"
		birth_date = 666.1.1
		adm = 1
		dip = 6
		mil = 6
    }
	heir = {
 		name = "Absyristhus"
		monarch_name = "Absyristhus I"
		dynasty = "Vi-Mystophi"
		birth_date = 707.1.1
		death_date = 761.1.1
		claim = 100
		adm = 4
		dip = 4
		mil = 5
    }
}

761.1.1 = {
	monarch = {
 		name = "Isocrerios"
		dynasty = "Du-Thimithous"
		birth_date = 742.1.1
		adm = 0
		dip = 6
		mil = 0
    }
	queen = {
 		name = "Lisiphe"
		dynasty = "Du-Thimithous"
		birth_date = 719.1.1
		adm = 4
		dip = 4
		mil = 6
		female = yes
    }
	heir = {
 		name = "Xenades"
		monarch_name = "Xenades I"
		dynasty = "Du-Thimithous"
		birth_date = 757.1.1
		death_date = 801.1.1
		claim = 100
		adm = 1
		dip = 3
		mil = 5
    }
}

801.1.1 = {
	monarch = {
 		name = "Dionysanos"
		dynasty = "Du-Spyrersis"
		birth_date = 759.1.1
		adm = 0
		dip = 6
		mil = 5
    }
	queen = {
 		name = "Pethalise"
		dynasty = "Du-Spyrersis"
		birth_date = 773.1.1
		adm = 1
		dip = 3
		mil = 0
		female = yes
    }
}

900.1.1 = {
	monarch = {
 		name = "Tharasiaraus"
		dynasty = "Vi-Crepheasi"
		birth_date = 858.1.1
		adm = 2
		dip = 4
		mil = 1
    }
	queen = {
 		name = "Akisisha"
		dynasty = "Vi-Crepheasi"
		birth_date = 870.1.1
		adm = 3
		dip = 0
		mil = 2
		female = yes
    }
	heir = {
 		name = "Therrastos"
		monarch_name = "Therrastos I"
		dynasty = "Vi-Crepheasi"
		birth_date = 895.1.1
		death_date = 966.1.1
		claim = 100
		adm = 0
		dip = 6
		mil = 1
    }
}

966.1.1 = {
	monarch = {
 		name = "Parthaus"
		dynasty = "Vu-Silishia"
		birth_date = 935.1.1
		adm = 6
		dip = 2
		mil = 1
    }
	queen = {
 		name = "Kisseosis"
		dynasty = "Vu-Silishia"
		birth_date = 946.1.1
		adm = 3
		dip = 1
		mil = 0
		female = yes
    }
	heir = {
 		name = "Ashataus"
		monarch_name = "Ashataus I"
		dynasty = "Vu-Silishia"
		birth_date = 957.1.1
		death_date = 1056.1.1
		claim = 100
		adm = 0
		dip = 6
		mil = 6
    }
}

1056.1.1 = {
	monarch = {
 		name = "Thanistae"
		dynasty = "Vu-Meniphelia"
		birth_date = 1033.1.1
		adm = 2
		dip = 1
		mil = 2
		female = yes
    }
}

1119.1.1 = {
	monarch = {
 		name = "Daphnaia"
		dynasty = "Vi-Athiliphis"
		birth_date = 1070.1.1
		adm = 1
		dip = 0
		mil = 5
		female = yes
    }
}

1203.1.1 = {
	monarch = {
 		name = "Parthaus"
		dynasty = "Di-Salmysios"
		birth_date = 1173.1.1
		adm = 6
		dip = 6
		mil = 2
    }
}

1283.1.1 = {
	monarch = {
 		name = "Lichophoros"
		dynasty = "Di-Anasztochus"
		birth_date = 1250.1.1
		adm = 4
		dip = 5
		mil = 6
    }
	queen = {
 		name = "Ethemilis"
		dynasty = "Di-Anasztochus"
		birth_date = 1256.1.1
		adm = 1
		dip = 4
		mil = 5
		female = yes
    }
	heir = {
 		name = "Spyrersis"
		monarch_name = "Spyrersis I"
		dynasty = "Di-Anasztochus"
		birth_date = 1278.1.1
		death_date = 1352.1.1
		claim = 100
		adm = 5
		dip = 2
		mil = 4
    }
}

1352.1.1 = {
	monarch = {
 		name = "Orthralus"
		dynasty = "Vi-Thanistae"
		birth_date = 1310.1.1
		adm = 4
		dip = 6
		mil = 4
    }
}

1435.1.1 = {
	monarch = {
 		name = "Brasas"
		dynasty = "Du-Laestithous"
		birth_date = 1398.1.1
		adm = 3
		dip = 5
		mil = 0
    }
}

1521.1.1 = {
	monarch = {
 		name = "Alcelix"
		dynasty = "Vi-Crepheasi"
		birth_date = 1496.1.1
		adm = 1
		dip = 4
		mil = 4
    }
	queen = {
 		name = "Cnassisa"
		dynasty = "Vi-Crepheasi"
		birth_date = 1495.1.1
		adm = 5
		dip = 3
		mil = 3
		female = yes
    }
	heir = {
 		name = "Spyrersis"
		monarch_name = "Spyrersis I"
		dynasty = "Vi-Crepheasi"
		birth_date = 1508.1.1
		death_date = 1593.1.1
		claim = 100
		adm = 2
		dip = 1
		mil = 2
    }
}

1593.1.1 = {
	monarch = {
 		name = "Adrates"
		dynasty = "Du-Helix"
		birth_date = 1554.1.1
		adm = 4
		dip = 3
		mil = 4
    }
	queen = {
 		name = "Meniphelia"
		dynasty = "Du-Helix"
		birth_date = 1561.1.1
		adm = 1
		dip = 1
		mil = 3
		female = yes
    }
}

1636.1.1 = {
	monarch = {
 		name = "Theamephia"
		dynasty = "Vi-Cnassisa"
		birth_date = 1595.1.1
		adm = 6
		dip = 0
		mil = 0
		female = yes
    }
	queen = {
 		name = "Hespice"
		dynasty = "Vi-Cnassisa"
		birth_date = 1613.1.1
		adm = 3
		dip = 6
		mil = 6
    }
	heir = {
 		name = "Anthesi"
		monarch_name = "Anthesi I"
		dynasty = "Vi-Cnassisa"
		birth_date = 1621.1.1
		death_date = 1673.1.1
		claim = 100
		adm = 0
		dip = 4
		mil = 5
		female = yes
    }
}

1673.1.1 = {
	monarch = {
 		name = "Antipherios"
		dynasty = "Vu-Leuciphia"
		birth_date = 1639.1.1
		adm = 3
		dip = 6
		mil = 0
    }
}

1766.1.1 = {
	monarch = {
 		name = "Pherishae"
		dynasty = "Vi-Thesassea"
		birth_date = 1720.1.1
		adm = 5
		dip = 0
		mil = 2
		female = yes
    }
}

1850.1.1 = {
	monarch = {
 		name = "Xerxelix"
		dynasty = "Vi-Adrasaxaura"
		birth_date = 1816.1.1
		adm = 3
		dip = 6
		mil = 5
    }
	queen = {
 		name = "Anthia"
		dynasty = "Vi-Adrasaxaura"
		birth_date = 1832.1.1
		adm = 0
		dip = 5
		mil = 4
		female = yes
    }
	heir = {
 		name = "Basos"
		monarch_name = "Basos I"
		dynasty = "Vi-Adrasaxaura"
		birth_date = 1840.1.1
		death_date = 1902.1.1
		claim = 100
		adm = 0
		dip = 1
		mil = 6
    }
}

1902.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Nyxophi"
		monarch_name = "Nyxophi I"
		dynasty = "Vi-Cnassisa"
		birth_date = 1889.1.1
		death_date = 1907.1.1
		claim = 100
		adm = 1
		dip = 5
		mil = 3
		female = yes
    }
}

1907.1.1 = {
	monarch = {
 		name = "Oediphose"
		dynasty = "Vu-Taphiphe"
		birth_date = 1870.1.1
		adm = 0
		dip = 2
		mil = 4
    }
	queen = {
 		name = "Athiliphis"
		dynasty = "Vu-Taphiphe"
		birth_date = 1856.1.1
		adm = 4
		dip = 0
		mil = 3
		female = yes
    }
}

1958.1.1 = {
	monarch = {
 		name = "Phrixice"
		dynasty = "Vu-Theamephia"
		birth_date = 1923.1.1
		adm = 2
		dip = 0
		mil = 6
    }
	queen = {
 		name = "Physacia"
		dynasty = "Vu-Theamephia"
		birth_date = 1931.1.1
		adm = 6
		dip = 5
		mil = 5
		female = yes
    }
	heir = {
 		name = "Sabixa"
		monarch_name = "Sabixa I"
		dynasty = "Vu-Theamephia"
		birth_date = 1952.1.1
		death_date = 2005.1.1
		claim = 100
		adm = 0
		dip = 2
		mil = 6
		female = yes
    }
}

2005.1.1 = {
	monarch = {
 		name = "Theanaeia"
		dynasty = "Du-Isocrerios"
		birth_date = 1978.1.1
		adm = 1
		dip = 3
		mil = 1
		female = yes
    }
	queen = {
 		name = "Lichophoros"
		dynasty = "Du-Isocrerios"
		birth_date = 1953.1.1
		adm = 2
		dip = 6
		mil = 2
    }
	heir = {
 		name = "Anasztochus"
		monarch_name = "Anasztochus I"
		dynasty = "Du-Isocrerios"
		birth_date = 1992.1.1
		death_date = 2095.1.1
		claim = 100
		adm = 6
		dip = 5
		mil = 1
    }
}

2095.1.1 = {
	monarch = {
 		name = "Zalothius"
		dynasty = "Du-Parthaus"
		birth_date = 2049.1.1
		adm = 5
		dip = 1
		mil = 1
    }
	queen = {
 		name = "Ashertes"
		dynasty = "Du-Parthaus"
		birth_date = 2048.1.1
		adm = 2
		dip = 0
		mil = 0
		female = yes
    }
	heir = {
 		name = "Hespocia"
		monarch_name = "Hespocia I"
		dynasty = "Du-Parthaus"
		birth_date = 2087.1.1
		death_date = 2158.1.1
		claim = 100
		adm = 6
		dip = 5
		mil = 6
    }
}

2158.1.1 = {
	monarch = {
 		name = "Prosenis"
		dynasty = "Vi-Nethoesa"
		birth_date = 2123.1.1
		adm = 1
		dip = 0
		mil = 2
		female = yes
    }
}

2216.1.1 = {
	monarch = {
 		name = "Akiseris"
		dynasty = "Di-Krathisthus"
		birth_date = 2190.1.1
		adm = 0
		dip = 6
		mil = 5
		female = yes
    }
}

2280.1.1 = {
	monarch = {
 		name = "Zalothius"
		dynasty = "Vu-Theamephia"
		birth_date = 2253.1.1
		adm = 5
		dip = 5
		mil = 2
    }
}

2339.1.1 = {
	monarch = {
 		name = "Ephiste"
		dynasty = "Di-Ansteas"
		birth_date = 2311.1.1
		adm = 3
		dip = 4
		mil = 5
    }
	queen = {
 		name = "Adrasaxaura"
		dynasty = "Di-Ansteas"
		birth_date = 2298.1.1
		adm = 0
		dip = 3
		mil = 4
		female = yes
    }
	heir = {
 		name = "Xerxelix"
		monarch_name = "Xerxelix I"
		dynasty = "Di-Ansteas"
		birth_date = 2338.1.1
		death_date = 2390.1.1
		claim = 100
		adm = 4
		dip = 1
		mil = 3
    }
}

2390.1.1 = {
	monarch = {
 		name = "Athanersis"
		dynasty = "Du-Thimithous"
		birth_date = 2357.1.1
		adm = 3
		dip = 5
		mil = 4
    }
}

2471.1.1 = {
	monarch = {
 		name = "Salmysios"
		dynasty = "Du-Orthrosyne"
		birth_date = 2419.1.1
		adm = 2
		dip = 4
		mil = 0
    }
	queen = {
 		name = "Anthophila"
		dynasty = "Du-Orthrosyne"
		birth_date = 2423.1.1
		adm = 5
		dip = 2
		mil = 6
		female = yes
    }
}

