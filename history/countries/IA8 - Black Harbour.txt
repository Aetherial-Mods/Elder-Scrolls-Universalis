government = republic
government_rank = 1
mercantilism = 1
technology_group = akavir_tg
religion = dragon_cult
primary_culture = islander
capital = 800

54.1.1 = {
	monarch = {
 		name = "Kedtaica"
		dynasty = "I'Gethecac"
		birth_date = 13.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

112.1.1 = {
	monarch = {
 		name = "Honqaeh"
		dynasty = "U'Ukzaq"
		birth_date = 76.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

190.1.1 = {
	monarch = {
 		name = "Grurqadox"
		dynasty = "I'Vunqiakki"
		birth_date = 163.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

243.1.1 = {
	monarch = {
 		name = "Qenge"
		dynasty = "E'Tarvuicre"
		birth_date = 219.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

308.1.1 = {
	monarch = {
 		name = "Qendriqeq"
		dynasty = "I'Khrazi"
		birth_date = 283.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

357.1.1 = {
	monarch = {
 		name = "Grejukoz"
		dynasty = "I'Kathud"
		birth_date = 328.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

394.1.1 = {
	monarch = {
 		name = "Xanzaz"
		dynasty = "U'Javruk"
		birth_date = 348.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

474.1.1 = {
	monarch = {
 		name = "Droktid"
		dynasty = "E'Quaqqaazzuaq"
		birth_date = 438.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

552.1.1 = {
	monarch = {
 		name = "Grizzig"
		dynasty = "E'Uqroz"
		birth_date = 520.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

641.1.1 = {
	monarch = {
 		name = "Kruidgekrex"
		dynasty = "U'Grurqadox"
		birth_date = 607.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

717.1.1 = {
	monarch = {
 		name = "Kiqqaz"
		dynasty = "O'Knorchih"
		birth_date = 670.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

800.1.1 = {
	monarch = {
 		name = "Droktid"
		dynasty = "U'Iakzic"
		birth_date = 753.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

844.1.1 = {
	monarch = {
 		name = "Grizzig"
		dynasty = "I'Gethecac"
		birth_date = 796.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

891.1.1 = {
	monarch = {
 		name = "Kruidgekrex"
		dynasty = "I'Gethecac"
		birth_date = 862.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

968.1.1 = {
	monarch = {
 		name = "Kiqqaz"
		dynasty = "I'Ugnegad"
		birth_date = 917.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1061.1.1 = {
	monarch = {
 		name = "Ziaktag"
		dynasty = "O'Gnarvih"
		birth_date = 1038.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1153.1.1 = {
	monarch = {
 		name = "Kevruzze"
		dynasty = "I'Kututh"
		birth_date = 1103.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1247.1.1 = {
	monarch = {
 		name = "Nujeq"
		dynasty = "U'Trangod"
		birth_date = 1194.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1292.1.1 = {
	monarch = {
 		name = "Hazzuq"
		dynasty = "I'Thraerra"
		birth_date = 1260.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1363.1.1 = {
	monarch = {
 		name = "Ziaktag"
		dynasty = "I'Kugnuh"
		birth_date = 1337.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1461.1.1 = {
	monarch = {
 		name = "Dradgiccek"
		dynasty = "I'Tirgeza"
		birth_date = 1438.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1545.1.1 = {
	monarch = {
 		name = "Knorchih"
		dynasty = "U'Nujeq"
		birth_date = 1523.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1620.1.1 = {
	monarch = {
 		name = "Guvit"
		dynasty = "I'Evo"
		birth_date = 1587.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1711.1.1 = {
	monarch = {
 		name = "Ondruc"
		dynasty = "I'Khrazi"
		birth_date = 1693.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1746.1.1 = {
	monarch = {
 		name = "Orvikkec"
		dynasty = "O'Iaqran"
		birth_date = 1706.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1795.1.1 = {
	monarch = {
 		name = "Crusqauz"
		dynasty = "O'Gherreqaot"
		birth_date = 1763.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1843.1.1 = {
	monarch = {
 		name = "Guqqerix"
		dynasty = "I'Ghinqizuq"
		birth_date = 1822.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1941.1.1 = {
	monarch = {
 		name = "Chitren"
		dynasty = "O'Gadde"
		birth_date = 1894.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2030.1.1 = {
	monarch = {
 		name = "Vatakkiag"
		dynasty = "O'Kevruzze"
		birth_date = 1997.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2128.1.1 = {
	monarch = {
 		name = "Crusqauz"
		dynasty = "U'Craajot"
		birth_date = 2088.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2214.1.1 = {
	monarch = {
 		name = "Guqqerix"
		dynasty = "U'Traisqiq"
		birth_date = 2192.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2313.1.1 = {
	monarch = {
 		name = "Eqriz"
		dynasty = "E'Quaqqaazzuaq"
		birth_date = 2275.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2384.1.1 = {
	monarch = {
 		name = "Kogruc"
		dynasty = "U'Raagrud"
		birth_date = 2334.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2477.1.1 = {
	monarch = {
 		name = "Trangod"
		dynasty = "U'Oqiandrex"
		birth_date = 2439.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

