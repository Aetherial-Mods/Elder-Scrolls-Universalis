government = monarchy
government_rank = 3
mercantilism = 1
technology_group = cyrodiil_tg
religion = nedic_pantheon
primary_culture = nedic
capital = 5634

54.1.1 = {
	monarch = {
 		name = "Osibin"
		dynasty = "Desylis"
		birth_date = 26.1.1
		adm = 3
		dip = 6
		mil = 4
		female = yes
    }
	queen = {
 		name = "Vaulic"
		dynasty = "Desylis"
		birth_date = 5.1.1
		adm = 6
		dip = 5
		mil = 3
    }
}

99.1.1 = {
	monarch = {
 		name = "Hella"
		dynasty = "Dulros"
		birth_date = 48.1.1
		adm = 0
		dip = 2
		mil = 1
		female = yes
    }
}

193.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Vurmesoth"
		monarch_name = "Vurmesoth I"
		dynasty = "Vrilak"
		birth_date = 193.1.1
		death_date = 211.1.1
		claim = 100
		adm = 4
		dip = 4
		mil = 1
    }
}

211.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Bardek"
		monarch_name = "Bardek I"
		dynasty = "Gahlaras"
		birth_date = 207.1.1
		death_date = 225.1.1
		claim = 100
		adm = 6
		dip = 5
		mil = 2
    }
}

225.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Dastith"
		monarch_name = "Dastith I"
		dynasty = "Gahlaras"
		birth_date = 219.1.1
		death_date = 237.1.1
		claim = 100
		adm = 4
		dip = 4
		mil = 6
    }
}

237.1.1 = {
	monarch = {
 		name = "Tylak"
		dynasty = "Nirdoc"
		birth_date = 214.1.1
		adm = 3
		dip = 3
		mil = 3
    }
	queen = {
 		name = "Mazan"
		dynasty = "Nirdoc"
		birth_date = 194.1.1
		adm = 0
		dip = 2
		mil = 2
		female = yes
    }
	heir = {
 		name = "Nirlyloth"
		monarch_name = "Nirlyloth I"
		dynasty = "Nirdoc"
		birth_date = 235.1.1
		death_date = 293.1.1
		claim = 100
		adm = 3
		dip = 0
		mil = 1
    }
}

293.1.1 = {
	monarch = {
 		name = "Vredraudin"
		dynasty = "Gakaazin"
		birth_date = 274.1.1
		adm = 6
		dip = 2
		mil = 3
    }
}

330.1.1 = {
	monarch = {
 		name = "Hyker"
		dynasty = "Vredraonir"
		birth_date = 288.1.1
		adm = 0
		dip = 6
		mil = 1
    }
	queen = {
 		name = "Emah"
		dynasty = "Vredraonir"
		birth_date = 290.1.1
		adm = 4
		dip = 4
		mil = 0
		female = yes
    }
	heir = {
 		name = "Shahaulec"
		monarch_name = "Shahaulec I"
		dynasty = "Vredraonir"
		birth_date = 317.1.1
		death_date = 424.1.1
		claim = 100
		adm = 1
		dip = 3
		mil = 6
    }
}

424.1.1 = {
	monarch = {
 		name = "Lonnev"
		dynasty = "Vrurlauleth"
		birth_date = 386.1.1
		adm = 3
		dip = 4
		mil = 1
		female = yes
    }
}

496.1.1 = {
	monarch = {
 		name = "Lysteth"
		dynasty = "Idith"
		birth_date = 450.1.1
		adm = 2
		dip = 3
		mil = 5
    }
	queen = {
 		name = "Zollehin"
		dynasty = "Idith"
		birth_date = 455.1.1
		adm = 6
		dip = 2
		mil = 4
		female = yes
    }
	heir = {
 		name = "Zerlec"
		monarch_name = "Zerlec I"
		dynasty = "Idith"
		birth_date = 485.1.1
		death_date = 577.1.1
		claim = 100
		adm = 2
		dip = 1
		mil = 3
    }
}

577.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Ridravin"
		monarch_name = "Ridravin I"
		dynasty = "Kaahlec"
		birth_date = 566.1.1
		death_date = 584.1.1
		claim = 100
		adm = 3
		dip = 5
		mil = 1
    }
}

584.1.1 = {
	monarch = {
 		name = "Lukaazon"
		dynasty = "Gahlaras"
		birth_date = 541.1.1
		adm = 1
		dip = 4
		mil = 4
    }
	queen = {
 		name = "Heva"
		dynasty = "Gahlaras"
		birth_date = 547.1.1
		adm = 5
		dip = 2
		mil = 3
		female = yes
    }
	heir = {
 		name = "Rardoc"
		monarch_name = "Rardoc I"
		dynasty = "Gahlaras"
		birth_date = 575.1.1
		death_date = 625.1.1
		claim = 100
		adm = 1
		dip = 4
		mil = 6
    }
}

625.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Dennae"
		monarch_name = "Dennae I"
		dynasty = "Vredraudin"
		birth_date = 619.1.1
		death_date = 637.1.1
		claim = 100
		adm = 6
		dip = 3
		mil = 2
		female = yes
    }
}

637.1.1 = {
	monarch = {
 		name = "Resi"
		dynasty = "Yhin"
		birth_date = 611.1.1
		adm = 6
		dip = 6
		mil = 3
		female = yes
    }
}

736.1.1 = {
	monarch = {
 		name = "Zyrdusor"
		dynasty = "Amedoc"
		birth_date = 683.1.1
		adm = 4
		dip = 5
		mil = 6
    }
}

775.1.1 = {
	monarch = {
 		name = "Gumrok"
		dynasty = "Haonuvic"
		birth_date = 729.1.1
		adm = 2
		dip = 5
		mil = 3
    }
}

863.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Kaomrar"
		monarch_name = "Kaomrar I"
		dynasty = "Nhudren"
		birth_date = 858.1.1
		death_date = 876.1.1
		claim = 100
		adm = 3
		dip = 2
		mil = 1
    }
}

876.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Riralath"
		monarch_name = "Riralath I"
		dynasty = "Vrakir"
		birth_date = 861.1.1
		death_date = 879.1.1
		claim = 100
		adm = 1
		dip = 1
		mil = 4
    }
}

879.1.1 = {
	monarch = {
 		name = "Amedoc"
		dynasty = "Vredraonir"
		birth_date = 826.1.1
		adm = 4
		dip = 2
		mil = 0
    }
}

928.1.1 = {
	monarch = {
 		name = "Nhudren"
		dynasty = "Vredraudin"
		birth_date = 875.1.1
		adm = 6
		dip = 4
		mil = 1
    }
	queen = {
 		name = "Atholur"
		dynasty = "Vredraudin"
		birth_date = 881.1.1
		adm = 6
		dip = 0
		mil = 2
		female = yes
    }
}

1023.1.1 = {
	monarch = {
 		name = "Dauhrak"
		dynasty = "Lyrmerek"
		birth_date = 975.1.1
		adm = 1
		dip = 1
		mil = 4
    }
	queen = {
 		name = "Dennida"
		dynasty = "Lyrmerek"
		birth_date = 972.1.1
		adm = 1
		dip = 5
		mil = 5
		female = yes
    }
	heir = {
 		name = "Lethadu"
		monarch_name = "Lethadu I"
		dynasty = "Lyrmerek"
		birth_date = 1009.1.1
		death_date = 1109.1.1
		claim = 100
		adm = 5
		dip = 3
		mil = 4
		female = yes
    }
}

1109.1.1 = {
	monarch = {
 		name = "Hyker"
		dynasty = "Vredraonir"
		birth_date = 1074.1.1
		adm = 4
		dip = 0
		mil = 4
    }
}

1188.1.1 = {
	monarch = {
 		name = "Emah"
		dynasty = "Halaanis"
		birth_date = 1145.1.1
		adm = 3
		dip = 6
		mil = 1
		female = yes
    }
}

1245.1.1 = {
	monarch = {
 		name = "Tirdor"
		dynasty = "Ludec"
		birth_date = 1224.1.1
		adm = 1
		dip = 5
		mil = 5
    }
	queen = {
 		name = "Norri"
		dynasty = "Ludec"
		birth_date = 1206.1.1
		adm = 5
		dip = 4
		mil = 3
		female = yes
    }
	heir = {
 		name = "Lethadu"
		monarch_name = "Lethadu I"
		dynasty = "Ludec"
		birth_date = 1236.1.1
		death_date = 1282.1.1
		claim = 100
		adm = 2
		dip = 2
		mil = 2
		female = yes
    }
}

1282.1.1 = {
	monarch = {
 		name = "Zollehin"
		dynasty = "Riralath"
		birth_date = 1232.1.1
		adm = 4
		dip = 4
		mil = 5
		female = yes
    }
	queen = {
 		name = "Gikamik"
		dynasty = "Riralath"
		birth_date = 1254.1.1
		adm = 1
		dip = 2
		mil = 4
    }
	heir = {
 		name = "Zahoti"
		monarch_name = "Zahoti I"
		dynasty = "Riralath"
		birth_date = 1274.1.1
		death_date = 1351.1.1
		claim = 100
		adm = 5
		dip = 1
		mil = 3
		female = yes
    }
}

1351.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Dirrinuh"
		monarch_name = "Dirrinuh I"
		dynasty = "Kaahlec"
		birth_date = 1343.1.1
		death_date = 1361.1.1
		claim = 100
		adm = 3
		dip = 0
		mil = 6
		female = yes
    }
}

1361.1.1 = {
	monarch = {
 		name = "Nomuh"
		dynasty = "Dastith"
		birth_date = 1338.1.1
		adm = 3
		dip = 3
		mil = 0
		female = yes
    }
}

1431.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Resa"
		monarch_name = "Resa I"
		dynasty = "Kalrin"
		birth_date = 1419.1.1
		death_date = 1437.1.1
		claim = 100
		adm = 2
		dip = 3
		mil = 1
		female = yes
    }
}

1437.1.1 = {
	monarch = {
 		name = "Lirimiv"
		dynasty = "Nestith"
		birth_date = 1395.1.1
		adm = 2
		dip = 6
		mil = 1
		female = yes
    }
}

1513.1.1 = {
	monarch = {
 		name = "Dastith"
		dynasty = "Larduve"
		birth_date = 1495.1.1
		adm = 0
		dip = 6
		mil = 5
    }
}

1554.1.1 = {
	monarch = {
 		name = "Tazamaev"
		dynasty = "Zerlec"
		birth_date = 1503.1.1
		adm = 5
		dip = 5
		mil = 2
		female = yes
    }
}

1653.1.1 = {
	monarch = {
 		name = "Vrakir"
		dynasty = "Shelren"
		birth_date = 1612.1.1
		adm = 3
		dip = 4
		mil = 5
    }
	queen = {
 		name = "Ossonel"
		dynasty = "Shelren"
		birth_date = 1615.1.1
		adm = 0
		dip = 3
		mil = 4
		female = yes
    }
	heir = {
 		name = "Vallal"
		monarch_name = "Vallal I"
		dynasty = "Shelren"
		birth_date = 1647.1.1
		death_date = 1751.1.1
		claim = 100
		adm = 4
		dip = 1
		mil = 3
		female = yes
    }
}

1751.1.1 = {
	monarch = {
 		name = "Romale"
		dynasty = "Kalrin"
		birth_date = 1698.1.1
		adm = 0
		dip = 3
		mil = 5
		female = yes
    }
	queen = {
 		name = "Zurduzok"
		dynasty = "Kalrin"
		birth_date = 1710.1.1
		adm = 4
		dip = 1
		mil = 4
    }
	heir = {
 		name = "Nirdoc"
		monarch_name = "Nirdoc I"
		dynasty = "Kalrin"
		birth_date = 1743.1.1
		death_date = 1795.1.1
		claim = 100
		adm = 1
		dip = 0
		mil = 3
    }
}

1795.1.1 = {
	monarch = {
 		name = "Vrakir"
		dynasty = "Bauvmith"
		birth_date = 1770.1.1
		adm = 3
		dip = 1
		mil = 6
    }
}

1863.1.1 = {
	monarch = {
 		name = "Mahodae"
		dynasty = "Amedoc"
		birth_date = 1828.1.1
		adm = 5
		dip = 2
		mil = 0
		female = yes
    }
}

1957.1.1 = {
	monarch = {
 		name = "Nhystareth"
		dynasty = "Shahaulec"
		birth_date = 1923.1.1
		adm = 4
		dip = 1
		mil = 4
    }
	queen = {
 		name = "Doshir"
		dynasty = "Shahaulec"
		birth_date = 1907.1.1
		adm = 0
		dip = 0
		mil = 3
		female = yes
    }
	heir = {
 		name = "Ralreloc"
		monarch_name = "Ralreloc I"
		dynasty = "Shahaulec"
		birth_date = 1942.1.1
		death_date = 2001.1.1
		claim = 100
		adm = 1
		dip = 3
		mil = 4
    }
}

2001.1.1 = {
	monarch = {
 		name = "Idith"
		dynasty = "Rardoc"
		birth_date = 1974.1.1
		adm = 0
		dip = 0
		mil = 4
    }
}

2061.1.1 = {
	monarch = {
 		name = "Taatysoth"
		dynasty = "Virdanath"
		birth_date = 2029.1.1
		adm = 5
		dip = 6
		mil = 1
    }
	queen = {
 		name = "Romale"
		dynasty = "Virdanath"
		birth_date = 2042.1.1
		adm = 4
		dip = 5
		mil = 5
		female = yes
    }
	heir = {
 		name = "Likoth"
		monarch_name = "Likoth I"
		dynasty = "Virdanath"
		birth_date = 2046.1.1
		death_date = 2154.1.1
		claim = 100
		adm = 0
		dip = 4
		mil = 4
    }
}

2154.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Resa"
		monarch_name = "Resa I"
		dynasty = "Zurduzok"
		birth_date = 2146.1.1
		death_date = 2164.1.1
		claim = 100
		adm = 6
		dip = 3
		mil = 0
		female = yes
    }
}

2164.1.1 = {
	monarch = {
 		name = "Tudaarin"
		dynasty = "Naanuzar"
		birth_date = 2146.1.1
		adm = 5
		dip = 6
		mil = 0
    }
}

2262.1.1 = {
	monarch = {
 		name = "Hotedi"
		dynasty = "Remaasok"
		birth_date = 2234.1.1
		adm = 3
		dip = 6
		mil = 4
		female = yes
    }
}

2339.1.1 = {
	monarch = {
 		name = "Sarul"
		dynasty = "Tamedir"
		birth_date = 2306.1.1
		adm = 2
		dip = 5
		mil = 1
		female = yes
    }
}

2395.1.1 = {
	monarch = {
 		name = "Vurmesoth"
		dynasty = "Riralath"
		birth_date = 2374.1.1
		adm = 0
		dip = 4
		mil = 4
    }
}

2486.1.1 = {
	monarch = {
 		name = "Bardek"
		dynasty = "Aroc"
		birth_date = 2451.1.1
		adm = 5
		dip = 3
		mil = 1
    }
}

