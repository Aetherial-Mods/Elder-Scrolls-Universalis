government = monarchy
government_rank = 1
mercantilism = 1
technology_group = cyrodiil_tg
religion = ayleid_pantheon
primary_culture = ayleid
capital = 1227

54.1.1 = {
	monarch = {
 		name = "Cossin"
		dynasty = "Ula-Restidina"
		birth_date = 16.1.1
		adm = 4
		dip = 0
		mil = 4
    }
	queen = {
 		name = "Cavannorel"
		dynasty = "Ula-Restidina"
		birth_date = 24.1.1
		adm = 1
		dip = 6
		mil = 3
		female = yes
    }
	heir = {
 		name = "Luudagarund"
		monarch_name = "Luudagarund I"
		dynasty = "Ula-Restidina"
		birth_date = 53.1.1
		death_date = 149.1.1
		claim = 100
		adm = 5
		dip = 4
		mil = 2
    }
}

149.1.1 = {
	monarch = {
 		name = "Vem"
		dynasty = "Vae-Weatherleah"
		birth_date = 121.1.1
		adm = 1
		dip = 6
		mil = 4
    }
}

207.1.1 = {
	monarch = {
 		name = "Vingiant"
		dynasty = "Mea-Andrulusus"
		birth_date = 170.1.1
		adm = 6
		dip = 5
		mil = 1
    }
	queen = {
 		name = "Urenenya"
		dynasty = "Mea-Andrulusus"
		birth_date = 180.1.1
		adm = 3
		dip = 4
		mil = 0
		female = yes
    }
	heir = {
 		name = "Rorguuramis"
		monarch_name = "Rorguuramis I"
		dynasty = "Mea-Andrulusus"
		birth_date = 198.1.1
		death_date = 250.1.1
		claim = 100
		adm = 0
		dip = 2
		mil = 6
    }
}

250.1.1 = {
	monarch = {
 		name = "Lymma"
		dynasty = "Vae-Sepades"
		birth_date = 224.1.1
		adm = 2
		dip = 3
		mil = 1
		female = yes
    }
}

320.1.1 = {
	monarch = {
 		name = "Vem"
		dynasty = "Vae-Tirerius"
		birth_date = 272.1.1
		adm = 4
		dip = 5
		mil = 3
    }
	queen = {
 		name = "Poman"
		dynasty = "Vae-Tirerius"
		birth_date = 296.1.1
		adm = 5
		dip = 1
		mil = 4
		female = yes
    }
	heir = {
 		name = "Vunhualdadont"
		monarch_name = "Vunhualdadont I"
		dynasty = "Vae-Tirerius"
		birth_date = 316.1.1
		death_date = 363.1.1
		claim = 100
		adm = 1
		dip = 0
		mil = 3
    }
}

363.1.1 = {
	monarch = {
 		name = "Qessem"
		dynasty = "Vae-Sena"
		birth_date = 310.1.1
		adm = 1
		dip = 3
		mil = 3
    }
	queen = {
 		name = "Cimase"
		dynasty = "Vae-Sena"
		birth_date = 313.1.1
		adm = 5
		dip = 2
		mil = 2
		female = yes
    }
	heir = {
 		name = "Wommi"
		monarch_name = "Wommi I"
		dynasty = "Vae-Sena"
		birth_date = 359.1.1
		death_date = 443.1.1
		claim = 100
		adm = 2
		dip = 0
		mil = 1
		female = yes
    }
}

443.1.1 = {
	monarch = {
 		name = "Tuanum"
		dynasty = "Vae-Vesagrius"
		birth_date = 416.1.1
		adm = 4
		dip = 1
		mil = 3
    }
	queen = {
 		name = "Poman"
		dynasty = "Vae-Vesagrius"
		birth_date = 420.1.1
		adm = 1
		dip = 0
		mil = 2
		female = yes
    }
	heir = {
 		name = "Hunne"
		monarch_name = "Hunne I"
		dynasty = "Vae-Vesagrius"
		birth_date = 434.1.1
		death_date = 502.1.1
		claim = 100
		adm = 5
		dip = 6
		mil = 1
		female = yes
    }
}

502.1.1 = {
	monarch = {
 		name = "Eradhiho"
		dynasty = "Vae-Vandiand"
		birth_date = 465.1.1
		adm = 1
		dip = 0
		mil = 3
		female = yes
    }
	queen = {
 		name = "Hasind"
		dynasty = "Vae-Vandiand"
		birth_date = 480.1.1
		adm = 5
		dip = 5
		mil = 2
    }
	heir = {
 		name = "Inhuagadom"
		monarch_name = "Inhuagadom I"
		dynasty = "Vae-Vandiand"
		birth_date = 489.1.1
		death_date = 549.1.1
		claim = 100
		adm = 2
		dip = 4
		mil = 1
    }
}

549.1.1 = {
	monarch = {
 		name = "Tuanum"
		dynasty = "Vae-Silelia"
		birth_date = 499.1.1
		adm = 1
		dip = 0
		mil = 2
    }
}

600.1.1 = {
	monarch = {
 		name = "Ynnylsasul"
		dynasty = "Vae-Weatherleah"
		birth_date = 549.1.1
		adm = 6
		dip = 0
		mil = 5
		female = yes
    }
}

689.1.1 = {
	monarch = {
 		name = "Tjuunrerhen"
		dynasty = "Mea-Etirina"
		birth_date = 670.1.1
		adm = 4
		dip = 6
		mil = 2
    }
	queen = {
 		name = "Nulynhi"
		dynasty = "Mea-Etirina"
		birth_date = 641.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

761.1.1 = {
	monarch = {
 		name = "Fidhi"
		dynasty = "Vae-Vanua"
		birth_date = 724.1.1
		adm = 0
		dip = 4
		mil = 5
    }
}

814.1.1 = {
	monarch = {
 		name = "Dini"
		dynasty = "Mea-Correllia"
		birth_date = 787.1.1
		adm = 5
		dip = 3
		mil = 1
		female = yes
    }
}

913.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Dynnyan"
		monarch_name = "Dynnyan I"
		dynasty = "Mea-Caumana"
		birth_date = 906.1.1
		death_date = 924.1.1
		claim = 100
		adm = 6
		dip = 0
		mil = 6
		female = yes
    }
}

924.1.1 = {
	monarch = {
 		name = "Hergor"
		dynasty = "Vae-Trumbe"
		birth_date = 873.1.1
		adm = 1
		dip = 1
		mil = 1
    }
	queen = {
 		name = "Hunnul"
		dynasty = "Vae-Trumbe"
		birth_date = 878.1.1
		adm = 5
		dip = 0
		mil = 0
		female = yes
    }
	heir = {
 		name = "Nanalne"
		monarch_name = "Nanalne I"
		dynasty = "Vae-Trumbe"
		birth_date = 921.1.1
		death_date = 984.1.1
		claim = 100
		adm = 2
		dip = 5
		mil = 6
		female = yes
    }
}

984.1.1 = {
	monarch = {
 		name = "Vanhur"
		dynasty = "Mea-Garlas"
		birth_date = 935.1.1
		adm = 5
		dip = 0
		mil = 2
    }
}

1035.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Haromir"
		monarch_name = "Haromir I"
		dynasty = "Mea-Aderina"
		birth_date = 1030.1.1
		death_date = 1048.1.1
		claim = 100
		adm = 1
		dip = 1
		mil = 1
    }
}

1048.1.1 = {
	monarch = {
 		name = "Ymmu"
		dynasty = "Mea-Allevar"
		birth_date = 1004.1.1
		adm = 4
		dip = 3
		mil = 3
		female = yes
    }
	queen = {
 		name = "Tjan"
		dynasty = "Mea-Allevar"
		birth_date = 1021.1.1
		adm = 1
		dip = 1
		mil = 2
    }
	heir = {
 		name = "Vossyduron"
		monarch_name = "Vossyduron I"
		dynasty = "Mea-Allevar"
		birth_date = 1034.1.1
		death_date = 1146.1.1
		claim = 100
		adm = 4
		dip = 0
		mil = 1
    }
}

1146.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Femde"
		monarch_name = "Femde I"
		dynasty = "Mea-Bawn"
		birth_date = 1135.1.1
		death_date = 1153.1.1
		claim = 100
		adm = 4
		dip = 3
		mil = 2
    }
}

1153.1.1 = {
	monarch = {
 		name = "Hegint"
		dynasty = "Mea-Garlas"
		birth_date = 1100.1.1
		adm = 2
		dip = 3
		mil = 5
    }
}

1242.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Talkynd"
		monarch_name = "Talkynd I"
		dynasty = "Vae-Vlastarus"
		birth_date = 1227.1.1
		death_date = 1245.1.1
		claim = 100
		adm = 6
		dip = 5
		mil = 5
    }
}

1245.1.1 = {
	monarch = {
 		name = "Lonnosson"
		dynasty = "Mea-Egnagia"
		birth_date = 1196.1.1
		adm = 3
		dip = 3
		mil = 4
		female = yes
    }
	queen = {
 		name = "Nym"
		dynasty = "Mea-Egnagia"
		birth_date = 1209.1.1
		adm = 0
		dip = 2
		mil = 3
    }
}

1337.1.1 = {
	monarch = {
 		name = "Orhualmuunint"
		dynasty = "Vae-Sena"
		birth_date = 1310.1.1
		adm = 5
		dip = 1
		mil = 0
    }
	queen = {
 		name = "Nula"
		dynasty = "Vae-Sena"
		birth_date = 1310.1.1
		adm = 2
		dip = 6
		mil = 6
		female = yes
    }
}

1424.1.1 = {
	monarch = {
 		name = "Ussamuul"
		dynasty = "Vae-Sena"
		birth_date = 1397.1.1
		adm = 0
		dip = 6
		mil = 2
    }
	queen = {
 		name = "Mezol"
		dynasty = "Vae-Sena"
		birth_date = 1387.1.1
		adm = 4
		dip = 4
		mil = 1
		female = yes
    }
}

1509.1.1 = {
	monarch = {
 		name = "Ynnylsasul"
		dynasty = "Mea-Herillius"
		birth_date = 1474.1.1
		adm = 2
		dip = 3
		mil = 5
		female = yes
    }
}

1579.1.1 = {
	monarch = {
 		name = "Harrylnes"
		dynasty = "Vae-Vortia"
		birth_date = 1542.1.1
		adm = 1
		dip = 2
		mil = 2
		female = yes
    }
}

1635.1.1 = {
	monarch = {
 		name = "Sylanwesh"
		dynasty = "Vae-Vortia"
		birth_date = 1608.1.1
		adm = 3
		dip = 4
		mil = 3
		female = yes
    }
	queen = {
 		name = "Nant"
		dynasty = "Vae-Vortia"
		birth_date = 1606.1.1
		adm = 3
		dip = 0
		mil = 4
    }
}

1700.1.1 = {
	monarch = {
 		name = "Nulynhi"
		dynasty = "Mea-Correllia"
		birth_date = 1668.1.1
		adm = 4
		dip = 4
		mil = 2
		female = yes
    }
}

1780.1.1 = {
	monarch = {
 		name = "Anumaril"
		dynasty = "Ula-Runata"
		birth_date = 1736.1.1
		adm = 2
		dip = 4
		mil = 6
    }
	queen = {
 		name = "Elanwe"
		dynasty = "Ula-Runata"
		birth_date = 1736.1.1
		adm = 6
		dip = 2
		mil = 5
		female = yes
    }
}

1825.1.1 = {
	monarch = {
 		name = "Undorrau"
		dynasty = "Ula-Messiena"
		birth_date = 1786.1.1
		adm = 4
		dip = 1
		mil = 2
    }
}

1879.1.1 = {
	monarch = {
 		name = "Haromir"
		dynasty = "Mea-Essassius"
		birth_date = 1854.1.1
		adm = 2
		dip = 0
		mil = 5
    }
}

1924.1.1 = {
	monarch = {
 		name = "Omashaul"
		dynasty = "Ula-Lildana"
		birth_date = 1890.1.1
		adm = 4
		dip = 2
		mil = 0
    }
}

2010.1.1 = {
	monarch = {
 		name = "Narges"
		dynasty = "Vae-Silorn"
		birth_date = 1961.1.1
		adm = 2
		dip = 1
		mil = 3
    }
	queen = {
 		name = "Lomaldydaa"
		dynasty = "Vae-Silorn"
		birth_date = 1968.1.1
		adm = 6
		dip = 6
		mil = 2
		female = yes
    }
}

2107.1.1 = {
	monarch = {
 		name = "Eradhiho"
		dynasty = "Ula-Ontus"
		birth_date = 2082.1.1
		adm = 4
		dip = 6
		mil = 6
		female = yes
    }
	queen = {
 		name = "Nugant"
		dynasty = "Ula-Ontus"
		birth_date = 2055.1.1
		adm = 1
		dip = 4
		mil = 5
    }
	heir = {
 		name = "Hennynlyho"
		monarch_name = "Hennynlyho I"
		dynasty = "Ula-Ontus"
		birth_date = 2096.1.1
		death_date = 2152.1.1
		claim = 100
		adm = 2
		dip = 1
		mil = 6
		female = yes
    }
}

2152.1.1 = {
	monarch = {
 		name = "Cyrheganund"
		dynasty = "Mea-Gince"
		birth_date = 2106.1.1
		adm = 3
		dip = 2
		mil = 1
    }
}

2206.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Djus"
		monarch_name = "Djus I"
		dynasty = "Vae-Vesagrius"
		birth_date = 2206.1.1
		death_date = 2224.1.1
		claim = 100
		adm = 1
		dip = 4
		mil = 1
    }
}

2224.1.1 = {
	monarch = {
 		name = "Ymmilune"
		dynasty = "Mea-Aerienna"
		birth_date = 2189.1.1
		adm = 0
		dip = 0
		mil = 1
		female = yes
    }
}

2271.1.1 = {
	monarch = {
 		name = "Soreshi"
		dynasty = "Vae-Statain"
		birth_date = 2233.1.1
		adm = 5
		dip = 0
		mil = 4
		female = yes
    }
	queen = {
 		name = "Odilon"
		dynasty = "Vae-Statain"
		birth_date = 2241.1.1
		adm = 1
		dip = 1
		mil = 0
    }
}

2337.1.1 = {
	monarch = {
 		name = "Synarlysush"
		dynasty = "Vae-Vilverin"
		birth_date = 2308.1.1
		adm = 3
		dip = 2
		mil = 1
		female = yes
    }
}

2411.1.1 = {
	monarch = {
 		name = "Erridhish"
		dynasty = "Vae-Sepades"
		birth_date = 2369.1.1
		adm = 1
		dip = 1
		mil = 5
		female = yes
    }
	queen = {
 		name = "Luudagarund"
		dynasty = "Vae-Sepades"
		birth_date = 2376.1.1
		adm = 5
		dip = 0
		mil = 4
    }
	heir = {
 		name = "Cur"
		monarch_name = "Cur I"
		dynasty = "Vae-Sepades"
		birth_date = 2411.1.1
		death_date = 2502.1.1
		claim = 100
		adm = 5
		dip = 3
		mil = 5
    }
}

