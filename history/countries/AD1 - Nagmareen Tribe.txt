government = native
government_rank = 1
mercantilism = 1
technology_group = marshan_tg
religion = hist
primary_culture = hapsleet
capital = 6766

54.1.1 = {
	monarch = {
 		name = "Obaxith"
		dynasty = "Er-Tei"
		birth_date = 14.1.1
		adm = 2
		dip = 1
		mil = 4
    }
}

140.1.1 = {
	monarch = {
 		name = "Villa"
		dynasty = "Meeros"
		birth_date = 102.1.1
		adm = 0
		dip = 0
		mil = 1
		female = yes
    }
}

184.1.1 = {
	monarch = {
 		name = "Oleed-Tah"
		dynasty = "Herneen"
		birth_date = 160.1.1
		adm = 5
		dip = 6
		mil = 4
    }
}

273.1.1 = {
	monarch = {
 		name = "Jee-Tul"
		dynasty = "Augussareth"
		birth_date = 235.1.1
		adm = 4
		dip = 5
		mil = 1
    }
}

331.1.1 = {
	monarch = {
 		name = "Chalish"
		dynasty = "Sakeides"
		birth_date = 303.1.1
		adm = 2
		dip = 5
		mil = 5
    }
}

368.1.1 = {
	monarch = {
 		name = "Veth-Veidal"
		dynasty = "Theodesh"
		birth_date = 349.1.1
		adm = 0
		dip = 4
		mil = 1
		female = yes
    }
}

411.1.1 = {
	monarch = {
 		name = "Pad-Maxath"
		dynasty = "Nagdeseer"
		birth_date = 370.1.1
		adm = 5
		dip = 3
		mil = 5
		female = yes
    }
}

507.1.1 = {
	monarch = {
 		name = "Jee-Lar"
		dynasty = "Ah-Jee"
		birth_date = 465.1.1
		adm = 0
		dip = 4
		mil = 0
    }
}

602.1.1 = {
	monarch = {
 		name = "Chaleed-Wan"
		dynasty = "Huzdeek"
		birth_date = 565.1.1
		adm = 6
		dip = 3
		mil = 3
    }
}

684.1.1 = {
	monarch = {
 		name = "Jilux"
		dynasty = "Madeian"
		birth_date = 646.1.1
		adm = 4
		dip = 3
		mil = 0
    }
}

751.1.1 = {
	monarch = {
 		name = "Cheedal-Jeen"
		dynasty = "Androteus"
		birth_date = 708.1.1
		adm = 2
		dip = 2
		mil = 4
    }
}

837.1.1 = {
	monarch = {
 		name = "Vureiem"
		dynasty = "Caylus"
		birth_date = 802.1.1
		adm = 0
		dip = 1
		mil = 0
		female = yes
    }
}

897.1.1 = {
	monarch = {
 		name = "Opatieel"
		dynasty = "Yelei"
		birth_date = 857.1.1
		adm = 6
		dip = 0
		mil = 4
    }
}

944.1.1 = {
	monarch = {
 		name = "Jeetum-Tulm"
		dynasty = "Agiussilus"
		birth_date = 903.1.1
		adm = 4
		dip = 0
		mil = 1
    }
}

988.1.1 = {
	monarch = {
 		name = "Cheedal-Gah"
		dynasty = "Taierlures"
		birth_date = 950.1.1
		adm = 2
		dip = 6
		mil = 4
    }
}

1084.1.1 = {
	monarch = {
 		name = "Vistha-Jush"
		dynasty = "Gallures"
		birth_date = 1055.1.1
		adm = 4
		dip = 0
		mil = 6
    }
}

1149.1.1 = {
	monarch = {
 		name = "Onurai-Betu"
		dynasty = "Eleedal-Teeus"
		birth_date = 1115.1.1
		adm = 2
		dip = 6
		mil = 2
    }
}

1230.1.1 = {
	monarch = {
 		name = "Weewish"
		dynasty = "Pehrsar"
		birth_date = 1190.1.1
		adm = 0
		dip = 5
		mil = 6
		female = yes
    }
}

1315.1.1 = {
	monarch = {
 		name = "Pujeerish"
		dynasty = "Neethcalees"
		birth_date = 1281.1.1
		adm = 6
		dip = 5
		mil = 3
		female = yes
    }
}

1364.1.1 = {
	monarch = {
 		name = "Kamatpa"
		dynasty = "Canimesh"
		birth_date = 1317.1.1
		adm = 4
		dip = 4
		mil = 6
    }
}

1403.1.1 = {
	monarch = {
 		name = "Chukka-Jekka"
		dynasty = "Jeeraz"
		birth_date = 1366.1.1
		adm = 2
		dip = 3
		mil = 3
    }
}

1467.1.1 = {
	monarch = {
 		name = "Vushtulm"
		dynasty = "Taierlures"
		birth_date = 1444.1.1
		adm = 0
		dip = 2
		mil = 0
    }
}

1548.1.1 = {
	monarch = {
 		name = "Pinooh"
		dynasty = "Nefeseus"
		birth_date = 1527.1.1
		adm = 6
		dip = 1
		mil = 3
		female = yes
    }
}

1598.1.1 = {
	monarch = {
 		name = "Kulusi"
		dynasty = "Gallures"
		birth_date = 1578.1.1
		adm = 1
		dip = 3
		mil = 5
		female = yes
    }
}

1695.1.1 = {
	monarch = {
 		name = "Chosumeel"
		dynasty = "Pethees"
		birth_date = 1672.1.1
		adm = 6
		dip = 2
		mil = 1
    }
}

1735.1.1 = {
	monarch = {
 		name = "Keema-Ru"
		dynasty = "Neethcalees"
		birth_date = 1704.1.1
		adm = 4
		dip = 1
		mil = 5
    }
}

1808.1.1 = {
	monarch = {
 		name = "Dar-Jasa"
		dynasty = "Meerhaj"
		birth_date = 1786.1.1
		adm = 2
		dip = 0
		mil = 2
    }
}

1907.1.1 = {
	monarch = {
 		name = "Wud-Weska"
		dynasty = "Effe-Shei"
		birth_date = 1870.1.1
		adm = 1
		dip = 0
		mil = 5
		female = yes
    }
}

1968.1.1 = {
	monarch = {
 		name = "Pekai-Jee"
		dynasty = "Wanan-Dar"
		birth_date = 1938.1.1
		adm = 6
		dip = 6
		mil = 2
    }
}

2013.1.1 = {
	monarch = {
 		name = "Leel-Vata"
		dynasty = "Huzdeek"
		birth_date = 1962.1.1
		adm = 4
		dip = 5
		mil = 6
		female = yes
    }
}

2057.1.1 = {
	monarch = {
 		name = "Dar-Zish"
		dynasty = "Silm-Na"
		birth_date = 2022.1.1
		adm = 2
		dip = 4
		mil = 2
		female = yes
    }
}

2132.1.1 = {
	monarch = {
 		name = "Wideem-Voh"
		dynasty = "Nefeseus"
		birth_date = 2099.1.1
		adm = 4
		dip = 5
		mil = 4
    }
}

2196.1.1 = {
	monarch = {
 		name = "Pejureel"
		dynasty = "Taierlures"
		birth_date = 2156.1.1
		adm = 2
		dip = 5
		mil = 0
    }
}

2254.1.1 = {
	monarch = {
 		name = "Xeejalish"
		dynasty = "Talen-Lan"
		birth_date = 2223.1.1
		adm = 1
		dip = 4
		mil = 4
		female = yes
    }
}

2342.1.1 = {
	monarch = {
 		name = "Powaj"
		dynasty = "Tee-Zeeus"
		birth_date = 2299.1.1
		adm = 6
		dip = 3
		mil = 1
    }
}

2422.1.1 = {
	monarch = {
 		name = "Kepanuu"
		dynasty = "Neethsareeth"
		birth_date = 2387.1.1
		adm = 4
		dip = 2
		mil = 4
    }
}

