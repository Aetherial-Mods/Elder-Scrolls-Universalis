government = monarchy
government_rank = 3
mercantilism = 1
technology_group = yokudan_tg
religion = redguard_pantheon
primary_culture = redguard
capital = 1482

54.1.1 = {
	monarch = {
 		name = "Sayda"
		dynasty = "Faavba"
		birth_date = 33.1.1
		adm = 0
		dip = 6
		mil = 0
		female = yes
    }
}

153.1.1 = {
	monarch = {
 		name = "Merric"
		dynasty = "Nathe"
		birth_date = 119.1.1
		adm = 5
		dip = 5
		mil = 4
    }
	queen = {
 		name = "Yisraza"
		dynasty = "Nathe"
		birth_date = 132.1.1
		adm = 2
		dip = 4
		mil = 3
		female = yes
    }
}

213.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Ayma"
		monarch_name = "Ayma I"
		dynasty = "Athh"
		birth_date = 199.1.1
		death_date = 217.1.1
		claim = 100
		adm = 3
		dip = 1
		mil = 1
		female = yes
    }
}

217.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Sharz"
		monarch_name = "Sharz I"
		dynasty = "Fafyler"
		birth_date = 208.1.1
		death_date = 226.1.1
		claim = 100
		adm = 1
		dip = 0
		mil = 5
    }
}

226.1.1 = {
	monarch = {
 		name = "Jahan"
		dynasty = "Shartta"
		birth_date = 180.1.1
		adm = 4
		dip = 1
		mil = 0
    }
	queen = {
 		name = "Hadineh"
		dynasty = "Shartta"
		birth_date = 188.1.1
		adm = 0
		dip = 0
		mil = 6
		female = yes
    }
	heir = {
 		name = "Hazanah"
		monarch_name = "Hazanah I"
		dynasty = "Shartta"
		birth_date = 223.1.1
		death_date = 315.1.1
		claim = 100
		adm = 4
		dip = 5
		mil = 5
		female = yes
    }
}

315.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Muazel"
		monarch_name = "Muazel I"
		dynasty = "Traah"
		birth_date = 300.1.1
		death_date = 318.1.1
		claim = 100
		adm = 3
		dip = 5
		mil = 2
    }
}

318.1.1 = {
	monarch = {
 		name = "Nalannah"
		dynasty = "Kithithon"
		birth_date = 265.1.1
		adm = 2
		dip = 1
		mil = 2
		female = yes
    }
}

383.1.1 = {
	monarch = {
 		name = "Taqiyat"
		dynasty = "Dudlim"
		birth_date = 361.1.1
		adm = 0
		dip = 0
		mil = 5
		female = yes
    }
	queen = {
 		name = "Gudarz"
		dynasty = "Dudlim"
		birth_date = 354.1.1
		adm = 4
		dip = 6
		mil = 4
    }
	heir = {
 		name = "Stenet"
		monarch_name = "Stenet I"
		dynasty = "Dudlim"
		birth_date = 383.1.1
		death_date = 461.1.1
		claim = 100
		adm = 1
		dip = 4
		mil = 3
    }
}

461.1.1 = {
	monarch = {
 		name = "Jawan"
		dynasty = "Nust-Si"
		birth_date = 434.1.1
		adm = 4
		dip = 6
		mil = 6
    }
}

528.1.1 = {
	monarch = {
 		name = "Dahlia"
		dynasty = "Trateve"
		birth_date = 493.1.1
		adm = 2
		dip = 5
		mil = 2
		female = yes
    }
	queen = {
 		name = "Majdazh"
		dynasty = "Trateve"
		birth_date = 479.1.1
		adm = 6
		dip = 3
		mil = 1
    }
	heir = {
 		name = "Azad"
		monarch_name = "Azad I"
		dynasty = "Trateve"
		birth_date = 528.1.1
		death_date = 617.1.1
		claim = 100
		adm = 3
		dip = 2
		mil = 0
    }
}

617.1.1 = {
	monarch = {
 		name = "Nebuin"
		dynasty = "Dudldr"
		birth_date = 566.1.1
		adm = 5
		dip = 3
		mil = 3
    }
}

657.1.1 = {
	monarch = {
 		name = "Jalienna"
		dynasty = "Jateif"
		birth_date = 604.1.1
		adm = 0
		dip = 5
		mil = 4
		female = yes
    }
	queen = {
 		name = "Hallan"
		dynasty = "Jateif"
		birth_date = 626.1.1
		adm = 0
		dip = 1
		mil = 5
    }
	heir = {
 		name = "Shelmar"
		monarch_name = "Shelmar I"
		dynasty = "Jateif"
		birth_date = 642.1.1
		death_date = 731.1.1
		claim = 100
		adm = 4
		dip = 0
		mil = 4
		female = yes
    }
}

731.1.1 = {
	monarch = {
 		name = "Jon"
		dynasty = "Manig"
		birth_date = 694.1.1
		adm = 4
		dip = 3
		mil = 5
    }
}

817.1.1 = {
	monarch = {
 		name = "Daron"
		dynasty = "Birgi"
		birth_date = 785.1.1
		adm = 2
		dip = 2
		mil = 1
    }
}

871.1.1 = {
	monarch = {
 		name = "Unar"
		dynasty = "Dindal"
		birth_date = 836.1.1
		adm = 0
		dip = 1
		mil = 5
    }
}

915.1.1 = {
	monarch = {
 		name = "Nuzbawir"
		dynasty = "Nazld"
		birth_date = 889.1.1
		adm = 5
		dip = 1
		mil = 1
    }
}

1011.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Mulaha"
		monarch_name = "Mulaha I"
		dynasty = "B'ollka"
		birth_date = 1011.1.1
		death_date = 1029.1.1
		claim = 100
		adm = 6
		dip = 5
		mil = 6
		female = yes
    }
}

1029.1.1 = {
	monarch = {
 		name = "Damien"
		dynasty = "Provmin"
		birth_date = 990.1.1
		adm = 2
		dip = 6
		mil = 2
    }
	queen = {
 		name = "Aubatha"
		dynasty = "Provmin"
		birth_date = 1005.1.1
		adm = 6
		dip = 5
		mil = 1
		female = yes
    }
	heir = {
 		name = "Nalannah"
		monarch_name = "Nalannah I"
		dynasty = "Provmin"
		birth_date = 1019.1.1
		death_date = 1086.1.1
		claim = 100
		adm = 3
		dip = 3
		mil = 0
		female = yes
    }
}

1086.1.1 = {
	monarch = {
 		name = "Noshzandar"
		dynasty = "Endotias"
		birth_date = 1062.1.1
		adm = 2
		dip = 0
		mil = 0
    }
	queen = {
 		name = "Mehry"
		dynasty = "Endotias"
		birth_date = 1039.1.1
		adm = 6
		dip = 5
		mil = 6
		female = yes
    }
}

1158.1.1 = {
	monarch = {
 		name = "Yncan"
		dynasty = "Talhik"
		birth_date = 1110.1.1
		adm = 4
		dip = 4
		mil = 3
    }
}

1231.1.1 = {
	monarch = {
 		name = "Farasad"
		dynasty = "Cyrcy"
		birth_date = 1188.1.1
		adm = 1
		dip = 6
		mil = 3
    }
	queen = {
 		name = "Zarida"
		dynasty = "Cyrcy"
		birth_date = 1200.1.1
		adm = 5
		dip = 5
		mil = 2
		female = yes
    }
	heir = {
 		name = "Majdazh"
		monarch_name = "Majdazh I"
		dynasty = "Cyrcy"
		birth_date = 1220.1.1
		death_date = 1278.1.1
		claim = 100
		adm = 2
		dip = 3
		mil = 1
    }
}

1278.1.1 = {
	monarch = {
 		name = "Mehfa"
		dynasty = "Stifyli"
		birth_date = 1260.1.1
		adm = 1
		dip = 0
		mil = 1
		female = yes
    }
	queen = {
 		name = "Khemzarq"
		dynasty = "Stifyli"
		birth_date = 1246.1.1
		adm = 5
		dip = 5
		mil = 0
    }
	heir = {
 		name = "Aishie"
		monarch_name = "Aishie I"
		dynasty = "Stifyli"
		birth_date = 1272.1.1
		death_date = 1345.1.1
		claim = 100
		adm = 6
		dip = 2
		mil = 1
		female = yes
    }
}

1345.1.1 = {
	monarch = {
 		name = "Mizbawaz"
		dynasty = "Endotias"
		birth_date = 1309.1.1
		adm = 5
		dip = 5
		mil = 1
    }
}

1441.1.1 = {
	monarch = {
 		name = "Hinther"
		dynasty = "Gorkrn"
		birth_date = 1390.1.1
		adm = 3
		dip = 4
		mil = 5
    }
}

1487.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Gonnison"
		monarch_name = "Gonnison I"
		dynasty = "Chararon"
		birth_date = 1486.1.1
		death_date = 1504.1.1
		claim = 100
		adm = 4
		dip = 1
		mil = 3
    }
}

1504.1.1 = {
	monarch = {
 		name = "Sermenh"
		dynasty = "Traah"
		birth_date = 1474.1.1
		adm = 0
		dip = 3
		mil = 5
		female = yes
    }
}

1592.1.1 = {
	monarch = {
 		name = "Mirasal"
		dynasty = "Stifyli"
		birth_date = 1545.1.1
		adm = 5
		dip = 2
		mil = 2
    }
}

1655.1.1 = {
	monarch = {
 		name = "Hillemir"
		dynasty = "Dudlim"
		birth_date = 1619.1.1
		adm = 0
		dip = 3
		mil = 3
    }
	queen = {
 		name = "Fayna"
		dynasty = "Dudlim"
		birth_date = 1623.1.1
		adm = 0
		dip = 0
		mil = 4
		female = yes
    }
	heir = {
 		name = "Sameq"
		monarch_name = "Sameq I"
		dynasty = "Dudlim"
		birth_date = 1649.1.1
		death_date = 1697.1.1
		claim = 100
		adm = 4
		dip = 5
		mil = 3
    }
}

1697.1.1 = {
	monarch = {
 		name = "Ikran"
		dynasty = "Traah"
		birth_date = 1670.1.1
		adm = 3
		dip = 2
		mil = 4
    }
}

1767.1.1 = {
	monarch = {
 		name = "Bail"
		dynasty = "Thanet"
		birth_date = 1735.1.1
		adm = 2
		dip = 1
		mil = 0
    }
	queen = {
 		name = "Lette"
		dynasty = "Thanet"
		birth_date = 1737.1.1
		adm = 5
		dip = 6
		mil = 6
		female = yes
    }
}

1852.1.1 = {
	monarch = {
 		name = "Jossai"
		dynasty = "Manielan"
		birth_date = 1806.1.1
		adm = 1
		dip = 1
		mil = 2
		female = yes
    }
}

1940.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Zihlijdel"
		monarch_name = "Zihlijdel I"
		dynasty = "Tralnthal"
		birth_date = 1927.1.1
		death_date = 1945.1.1
		claim = 100
		adm = 2
		dip = 5
		mil = 6
    }
}

1945.1.1 = {
	monarch = {
 		name = "Tavriya"
		dynasty = "Chref"
		birth_date = 1895.1.1
		adm = 0
		dip = 4
		mil = 3
		female = yes
    }
	queen = {
 		name = "Hahdavir"
		dynasty = "Chref"
		birth_date = 1905.1.1
		adm = 4
		dip = 3
		mil = 2
    }
	heir = {
 		name = "Erla"
		monarch_name = "Erla I"
		dynasty = "Chref"
		birth_date = 1943.1.1
		death_date = 1981.1.1
		claim = 100
		adm = 3
		dip = 6
		mil = 2
		female = yes
    }
}

1981.1.1 = {
	monarch = {
 		name = "Jelanan"
		dynasty = "Gorkrn"
		birth_date = 1933.1.1
		adm = 4
		dip = 2
		mil = 3
    }
}

2076.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Tavaid"
		monarch_name = "Tavaid I"
		dynasty = "Gorkrn"
		birth_date = 2068.1.1
		death_date = 2086.1.1
		claim = 100
		adm = 4
		dip = 6
		mil = 1
    }
}

2086.1.1 = {
	monarch = {
 		name = "Ubrabih"
		dynasty = "Sadssean"
		birth_date = 2048.1.1
		adm = 4
		dip = 3
		mil = 2
    }
	queen = {
 		name = "Samila"
		dynasty = "Sadssean"
		birth_date = 2050.1.1
		adm = 1
		dip = 1
		mil = 1
		female = yes
    }
	heir = {
 		name = "Isaadiah"
		monarch_name = "Isaadiah I"
		dynasty = "Sadssean"
		birth_date = 2080.1.1
		death_date = 2181.1.1
		claim = 100
		adm = 1
		dip = 5
		mil = 2
		female = yes
    }
}

2181.1.1 = {
	monarch = {
 		name = "Vhos"
		dynasty = "Jeleadal"
		birth_date = 2145.1.1
		adm = 0
		dip = 1
		mil = 2
    }
}

2249.1.1 = {
	monarch = {
 		name = "Owynok"
		dynasty = "Flargir"
		birth_date = 2206.1.1
		adm = 6
		dip = 1
		mil = 6
    }
	queen = {
 		name = "Ashiyana"
		dynasty = "Flargir"
		birth_date = 2227.1.1
		adm = 2
		dip = 6
		mil = 4
		female = yes
    }
	heir = {
 		name = "Nahzhem"
		monarch_name = "Nahzhem I"
		dynasty = "Flargir"
		birth_date = 2247.1.1
		death_date = 2342.1.1
		claim = 100
		adm = 6
		dip = 5
		mil = 3
    }
}

2342.1.1 = {
	monarch = {
 		name = "Dhemireh"
		dynasty = "Slurgti"
		birth_date = 2300.1.1
		adm = 2
		dip = 6
		mil = 6
		female = yes
    }
}

2418.1.1 = {
	monarch = {
 		name = "Varshab"
		dynasty = "Kithithon"
		birth_date = 2398.1.1
		adm = 0
		dip = 5
		mil = 2
    }
	queen = {
 		name = "Sehmasah"
		dynasty = "Kithithon"
		birth_date = 2385.1.1
		adm = 4
		dip = 4
		mil = 1
		female = yes
    }
	heir = {
 		name = "Jalbert"
		monarch_name = "Jalbert I"
		dynasty = "Kithithon"
		birth_date = 2409.1.1
		death_date = 2460.1.1
		claim = 100
		adm = 1
		dip = 2
		mil = 0
    }
}

2460.1.1 = {
	monarch = {
 		name = "K'avar"
		dynasty = "Cilp'kern"
		birth_date = 2427.1.1
		adm = 0
		dip = 6
		mil = 1
    }
	queen = {
 		name = "Parvaia"
		dynasty = "Cilp'kern"
		birth_date = 2432.1.1
		adm = 4
		dip = 4
		mil = 0
		female = yes
    }
	heir = {
 		name = "Therdon"
		monarch_name = "Therdon I"
		dynasty = "Cilp'kern"
		birth_date = 2458.1.1
		death_date = 2501.1.1
		claim = 100
		adm = 5
		dip = 1
		mil = 1
    }
}

