government = republic
government_rank = 1
mercantilism = 1
technology_group = elven_tg
religion = bosmer_pantheon
primary_culture = bosmer
capital = 816
secondary_religion = khajiiti_pantheon

54.1.1 = {
	monarch = {
 		name = "Anriel"
		dynasty = "Ca'Greenheart"
		birth_date = 3.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

116.1.1 = {
	monarch = {
 		name = "Farenlith"
		dynasty = "Ca'Meneia"
		birth_date = 78.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

191.1.1 = {
	monarch = {
 		name = "Belwinan"
		dynasty = "		"
		birth_date = 153.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

284.1.1 = {
	monarch = {
 		name = "Melbethil"
		dynasty = "Ca'Greenheart"
		birth_date = 250.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

373.1.1 = {
	monarch = {
 		name = "Thollunil"
		dynasty = "Ca'Lichenrock"
		birth_date = 326.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

454.1.1 = {
	monarch = {
 		name = "Manalmir"
		dynasty = "Or'Naearil"
		birth_date = 431.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

494.1.1 = {
	monarch = {
 		name = "Garimmir"
		dynasty = "Or'Stonesquare"
		birth_date = 441.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

544.1.1 = {
	monarch = {
 		name = "Dolian"
		dynasty = "Ca'Heaven"
		birth_date = 519.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

595.1.1 = {
	monarch = {
 		name = "Tholerthorn"
		dynasty = "Ca'Laenoniel"
		birth_date = 566.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

648.1.1 = {
	monarch = {
 		name = "Malthor"
		dynasty = "Ca'Green"
		birth_date = 619.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

724.1.1 = {
	monarch = {
 		name = "Gaerthgorn"
		dynasty = "Ca'Lynpar"
		birth_date = 674.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

760.1.1 = {
	monarch = {
 		name = "Dirwendel"
		dynasty = "Or'Stonesquare"
		birth_date = 732.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

825.1.1 = {
	monarch = {
 		name = "Ganolon"
		dynasty = "Or'Seledra"
		birth_date = 805.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

896.1.1 = {
	monarch = {
 		name = "Draugorin"
		dynasty = "Ur'Cori"
		birth_date = 858.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

964.1.1 = {
	monarch = {
 		name = "Thariel"
		dynasty = "Or'Run"
		birth_date = 932.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1017.1.1 = {
	monarch = {
 		name = "Marilrin"
		dynasty = "Ur'Eineneth"
		birth_date = 985.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1086.1.1 = {
	monarch = {
 		name = "Galthonor"
		dynasty = "Ur'Ebon"
		birth_date = 1068.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1152.1.1 = {
	monarch = {
 		name = "Donnaelain"
		dynasty = "Ur'Bluewood"
		birth_date = 1126.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1194.1.1 = {
	monarch = {
 		name = "Thanael"
		dynasty = "Ur'Bluewood"
		birth_date = 1172.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1276.1.1 = {
	monarch = {
 		name = "Manroth"
		dynasty = "Ur'Arednor"
		birth_date = 1226.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1318.1.1 = {
	monarch = {
 		name = "Ulwemir"
		dynasty = "Ca'Heimdar"
		birth_date = 1279.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1375.1.1 = {
	monarch = {
 		name = "Mendros"
		dynasty = "Ca'Manthia"
		birth_date = 1330.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1453.1.1 = {
	monarch = {
 		name = "Gerethel"
		dynasty = "Or'Parndra"
		birth_date = 1401.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1537.1.1 = {
	monarch = {
 		name = "Earnaana"
		dynasty = "Ur'Adaesse"
		birth_date = 1485.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1588.1.1 = {
	monarch = {
 		name = "Ulladhel"
		dynasty = "Ca'Estiynia"
		birth_date = 1563.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1679.1.1 = {
	monarch = {
 		name = "Melleron"
		dynasty = "Or'Oakenwood"
		birth_date = 1651.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1744.1.1 = {
	monarch = {
 		name = "Gilragaith"
		dynasty = "Or'Timberstone"
		birth_date = 1716.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1798.1.1 = {
	monarch = {
 		name = "Dothriel"
		dynasty = "Or'Riverwood"
		birth_date = 1755.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1846.1.1 = {
	monarch = {
 		name = "Glaurolin"
		dynasty = "Ur'Dra'bul"
		birth_date = 1805.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1891.1.1 = {
	monarch = {
 		name = "Bendralas"
		dynasty = "Ur'Andsse"
		birth_date = 1870.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1937.1.1 = {
	monarch = {
 		name = "Nothrolion"
		dynasty = "Ur'Eldenroot"
		birth_date = 1903.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1996.1.1 = {
	monarch = {
 		name = "Borwaegal"
		dynasty = "Ca'Green"
		birth_date = 1976.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

2045.1.1 = {
	monarch = {
 		name = "Ralai"
		dynasty = "Or'Silvenar"
		birth_date = 2008.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

2107.1.1 = {
	monarch = {
 		name = "Heralbor"
		dynasty = "Ur'Cormair"
		birth_date = 2060.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2157.1.1 = {
	monarch = {
 		name = "Minaelion"
		dynasty = "Or'Mossmire"
		birth_date = 2123.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

2214.1.1 = {
	monarch = {
 		name = "Galolion"
		dynasty = "Ca'Laenoniel"
		birth_date = 2190.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2255.1.1 = {
	monarch = {
 		name = "Dorinlas"
		dynasty = "Or'Oakwood"
		birth_date = 2210.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2314.1.1 = {
	monarch = {
 		name = "Thongonor"
		dynasty = "Ca'Heimdar"
		birth_date = 2292.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2367.1.1 = {
	monarch = {
 		name = "Millenith"
		dynasty = "Ur'Ebon"
		birth_date = 2330.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

2422.1.1 = {
	monarch = {
 		name = "Galfinar"
		dynasty = "Ur'Diss"
		birth_date = 2398.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

