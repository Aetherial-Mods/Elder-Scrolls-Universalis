government = monarchy
government_rank = 1
mercantilism = 1
technology_group = pyandonea_tg
religion = serpant_king
primary_culture = maormer
capital = 218

54.1.1 = {
	monarch = {
 		name = "Achenum"
		dynasty = "Nu-Pysnur"
		birth_date = 11.1.1
		adm = 1
		dip = 2
		mil = 0
    }
}

128.1.1 = {
	monarch = {
 		name = "Rassene"
		dynasty = "Na-Yorhasri"
		birth_date = 109.1.1
		adm = 6
		dip = 1
		mil = 4
    }
	queen = {
 		name = "Rinyaondlindir"
		dynasty = "Na-Yorhasri"
		birth_date = 90.1.1
		adm = 3
		dip = 0
		mil = 2
		female = yes
    }
}

190.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Heculoa"
		monarch_name = "Heculoa I"
		dynasty = "Nu-Pysnur"
		birth_date = 176.1.1
		death_date = 194.1.1
		claim = 100
		adm = 4
		dip = 4
		mil = 0
    }
}

194.1.1 = {
	monarch = {
 		name = "Cardir"
		dynasty = "Ni-Esluth"
		birth_date = 169.1.1
		adm = 0
		dip = 5
		mil = 3
		female = yes
    }
}

249.1.1 = {
	monarch = {
 		name = "Monmaldor"
		dynasty = "Nu-Qremrevil"
		birth_date = 213.1.1
		adm = 1
		dip = 6
		mil = 4
    }
	queen = {
 		name = "Ohasri"
		dynasty = "Nu-Qremrevil"
		birth_date = 217.1.1
		adm = 2
		dip = 3
		mil = 5
		female = yes
    }
	heir = {
 		name = "Aranlel"
		monarch_name = "Aranlel I"
		dynasty = "Nu-Qremrevil"
		birth_date = 239.1.1
		death_date = 295.1.1
		claim = 100
		adm = 6
		dip = 1
		mil = 4
    }
}

295.1.1 = {
	monarch = {
 		name = "Vaeht"
		dynasty = "Nu-Qremrevil"
		birth_date = 268.1.1
		adm = 5
		dip = 5
		mil = 5
		female = yes
    }
	queen = {
 		name = "Yealros"
		dynasty = "Nu-Qremrevil"
		birth_date = 266.1.1
		adm = 2
		dip = 3
		mil = 4
    }
}

381.1.1 = {
	monarch = {
 		name = "Oharasyeda"
		dynasty = "Ne-Dyspirlyrwun"
		birth_date = 335.1.1
		adm = 0
		dip = 3
		mil = 0
		female = yes
    }
}

459.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Tyisral"
		monarch_name = "Tyisral I"
		dynasty = "Ni-Jydromdis"
		birth_date = 452.1.1
		death_date = 470.1.1
		claim = 100
		adm = 1
		dip = 0
		mil = 5
    }
}

470.1.1 = {
	monarch = {
 		name = "Jykion"
		dynasty = "Ni-Eclalras"
		birth_date = 452.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

558.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Isanmetil"
		monarch_name = "Isanmetil I"
		dynasty = "Na-Vallirly"
		birth_date = 551.1.1
		death_date = 569.1.1
		claim = 100
		adm = 4
		dip = 5
		mil = 5
    }
}

569.1.1 = {
	monarch = {
 		name = "Panohalamion"
		dynasty = "Na-Puhlynvurnia"
		birth_date = 522.1.1
		adm = 0
		dip = 6
		mil = 1
		female = yes
    }
}

658.1.1 = {
	monarch = {
 		name = "Parevdil"
		dynasty = "Nu-Vykhydir"
		birth_date = 640.1.1
		adm = 5
		dip = 6
		mil = 4
		female = yes
    }
}

707.1.1 = {
	monarch = {
 		name = "Jykion"
		dynasty = "Nu-Qakhasa"
		birth_date = 671.1.1
		adm = 0
		dip = 0
		mil = 6
    }
}

742.1.1 = {
	monarch = {
 		name = "Virindi"
		dynasty = "Ni-Peislyrliur"
		birth_date = 710.1.1
		adm = 5
		dip = 6
		mil = 3
		female = yes
    }
}

821.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Ninordor"
		monarch_name = "Ninordor I"
		dynasty = "Ni-Hamondor"
		birth_date = 813.1.1
		death_date = 831.1.1
		claim = 100
		adm = 3
		dip = 1
		mil = 3
    }
}

831.1.1 = {
	monarch = {
 		name = "Ieda"
		dynasty = "Na-Vucydal"
		birth_date = 778.1.1
		adm = 2
		dip = 4
		mil = 3
		female = yes
    }
}

903.1.1 = {
	monarch = {
 		name = "Uulos"
		dynasty = "Na-Uhnilvy"
		birth_date = 870.1.1
		adm = 0
		dip = 4
		mil = 0
    }
}

1002.1.1 = {
	monarch = {
 		name = "Noryaaius"
		dynasty = "Ne-Jorlos"
		birth_date = 977.1.1
		adm = 5
		dip = 3
		mil = 3
    }
}

1099.1.1 = {
	monarch = {
 		name = "Ienurdil"
		dynasty = "Ni-Hassoldonmes"
		birth_date = 1053.1.1
		adm = 4
		dip = 2
		mil = 0
		female = yes
    }
	queen = {
 		name = "Iphidamas"
		dynasty = "Ni-Hassoldonmes"
		birth_date = 1078.1.1
		adm = 1
		dip = 1
		mil = 6
    }
	heir = {
 		name = "Chryiphidi"
		monarch_name = "Chryiphidi I"
		dynasty = "Ni-Hassoldonmes"
		birth_date = 1085.1.1
		death_date = 1141.1.1
		claim = 100
		adm = 4
		dip = 6
		mil = 5
		female = yes
    }
}

1141.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Nordo"
		monarch_name = "Nordo I"
		dynasty = "Ni-Nernath"
		birth_date = 1133.1.1
		death_date = 1151.1.1
		claim = 100
		adm = 3
		dip = 5
		mil = 1
    }
}

1151.1.1 = {
	monarch = {
 		name = "Noryaaius"
		dynasty = "Ni-Peislyrliur"
		birth_date = 1110.1.1
		adm = 2
		dip = 2
		mil = 2
    }
}

1221.1.1 = {
	monarch = {
 		name = "Rodjahohawen"
		dynasty = "Nu-Qakhasa"
		birth_date = 1187.1.1
		adm = 0
		dip = 1
		mil = 5
		female = yes
    }
}

1275.1.1 = {
	monarch = {
 		name = "Nurdron"
		dynasty = "Nu-Remrel"
		birth_date = 1232.1.1
		adm = 6
		dip = 0
		mil = 2
    }
	queen = {
 		name = "Irinwe"
		dynasty = "Nu-Remrel"
		birth_date = 1231.1.1
		adm = 2
		dip = 6
		mil = 1
		female = yes
    }
	heir = {
 		name = "Nitil"
		monarch_name = "Nitil I"
		dynasty = "Nu-Remrel"
		birth_date = 1264.1.1
		death_date = 1336.1.1
		claim = 100
		adm = 6
		dip = 4
		mil = 0
    }
}

1336.1.1 = {
	monarch = {
 		name = "Cinulisloa"
		dynasty = "Ne-Vemnosh"
		birth_date = 1312.1.1
		adm = 2
		dip = 6
		mil = 2
    }
}

1400.1.1 = {
	monarch = {
 		name = "Merlolos"
		dynasty = "Na-Seirrarmir"
		birth_date = 1360.1.1
		adm = 0
		dip = 5
		mil = 6
    }
}

1454.1.1 = {
	monarch = {
 		name = "Nurdron"
		dynasty = "Na-Dycendunu"
		birth_date = 1411.1.1
		adm = 5
		dip = 4
		mil = 3
    }
	queen = {
 		name = "Desheneior"
		dynasty = "Na-Dycendunu"
		birth_date = 1429.1.1
		adm = 4
		dip = 0
		mil = 5
		female = yes
    }
	heir = {
 		name = "Virindi"
		monarch_name = "Virindi I"
		dynasty = "Na-Dycendunu"
		birth_date = 1445.1.1
		death_date = 1498.1.1
		claim = 100
		adm = 6
		dip = 1
		mil = 1
		female = yes
    }
}

1498.1.1 = {
	monarch = {
 		name = "Arslone"
		dynasty = "Ni-Kahmu"
		birth_date = 1450.1.1
		adm = 6
		dip = 4
		mil = 1
		female = yes
    }
	queen = {
 		name = "Aryaamo"
		dynasty = "Ni-Kahmu"
		birth_date = 1478.1.1
		adm = 3
		dip = 3
		mil = 0
    }
	heir = {
 		name = "Heyrral"
		monarch_name = "Heyrral I"
		dynasty = "Ni-Kahmu"
		birth_date = 1495.1.1
		death_date = 1545.1.1
		claim = 100
		adm = 3
		dip = 6
		mil = 1
    }
}

1545.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Jahuuyetil"
		monarch_name = "Jahuuyetil I"
		dynasty = "Ni-Celgeth"
		birth_date = 1534.1.1
		death_date = 1552.1.1
		claim = 100
		adm = 5
		dip = 1
		mil = 2
    }
}

1552.1.1 = {
	monarch = {
 		name = "Eiltylaeht"
		dynasty = "Ni-Nihmolen"
		birth_date = 1520.1.1
		adm = 0
		dip = 2
		mil = 5
		female = yes
    }
	queen = {
 		name = "Iphilinmanius"
		dynasty = "Ni-Nihmolen"
		birth_date = 1519.1.1
		adm = 4
		dip = 1
		mil = 4
    }
}

1644.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Chrylana"
		monarch_name = "Chrylana I"
		dynasty = "Na-Ymuo"
		birth_date = 1641.1.1
		death_date = 1659.1.1
		claim = 100
		adm = 2
		dip = 0
		mil = 0
		female = yes
    }
}

1659.1.1 = {
	monarch = {
 		name = "Norevalion"
		dynasty = "Nu-Qaalru"
		birth_date = 1624.1.1
		adm = 1
		dip = 6
		mil = 4
    }
}

1726.1.1 = {
	monarch = {
 		name = "Jahuuyetil"
		dynasty = "Ni-Emnih"
		birth_date = 1686.1.1
		adm = 0
		dip = 2
		mil = 4
    }
}

1820.1.1 = {
	monarch = {
 		name = "Noryaaius"
		dynasty = "Nu-Vinimdul"
		birth_date = 1795.1.1
		adm = 5
		dip = 2
		mil = 1
    }
}

1883.1.1 = {
	monarch = {
 		name = "Jyklos"
		dynasty = "Ni-Jykheh"
		birth_date = 1860.1.1
		adm = 4
		dip = 1
		mil = 5
    }
}

1933.1.1 = {
	monarch = {
 		name = "Chryloa"
		dynasty = "Ne-Cynhis"
		birth_date = 1882.1.1
		adm = 2
		dip = 0
		mil = 1
    }
	queen = {
 		name = "Locounna"
		dynasty = "Ne-Cynhis"
		birth_date = 1907.1.1
		adm = 6
		dip = 6
		mil = 0
		female = yes
    }
}

2015.1.1 = {
	monarch = {
 		name = "Larnil"
		dynasty = "Nu-Himlaalniu"
		birth_date = 1989.1.1
		adm = 4
		dip = 5
		mil = 4
    }
	queen = {
 		name = "Lermenwen"
		dynasty = "Nu-Himlaalniu"
		birth_date = 1978.1.1
		adm = 1
		dip = 3
		mil = 3
		female = yes
    }
	heir = {
 		name = "Jykisvisdo"
		monarch_name = "Jykisvisdo I"
		dynasty = "Nu-Himlaalniu"
		birth_date = 2014.1.1
		death_date = 2103.1.1
		claim = 100
		adm = 5
		dip = 2
		mil = 2
    }
}

2103.1.1 = {
	monarch = {
 		name = "Viscultil"
		dynasty = "Nu-Puunrindor"
		birth_date = 2065.1.1
		adm = 0
		dip = 3
		mil = 4
    }
	queen = {
 		name = "Caorgia"
		dynasty = "Nu-Puunrindor"
		birth_date = 2061.1.1
		adm = 4
		dip = 2
		mil = 3
		female = yes
    }
	heir = {
 		name = "Uueildor"
		monarch_name = "Uueildor I"
		dynasty = "Nu-Puunrindor"
		birth_date = 2097.1.1
		death_date = 2193.1.1
		claim = 100
		adm = 1
		dip = 0
		mil = 2
    }
}

2193.1.1 = {
	monarch = {
 		name = "Camyrda"
		dynasty = "Ne-Ynlath"
		birth_date = 2172.1.1
		adm = 1
		dip = 4
		mil = 2
		female = yes
    }
}

2282.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Vagun"
		monarch_name = "Vagun I"
		dynasty = "Na-Jytshysiaht"
		birth_date = 2279.1.1
		death_date = 2297.1.1
		claim = 100
		adm = 5
		dip = 6
		mil = 2
    }
}

2297.1.1 = {
	monarch = {
 		name = "Acarsha"
		dynasty = "Na-Ohlo"
		birth_date = 2265.1.1
		adm = 4
		dip = 2
		mil = 3
		female = yes
    }
}

2388.1.1 = {
	monarch = {
 		name = "Dairtil"
		dynasty = "Nu-Miphodraaslo"
		birth_date = 2366.1.1
		adm = 2
		dip = 1
		mil = 6
    }
	queen = {
 		name = "Isda"
		dynasty = "Nu-Miphodraaslo"
		birth_date = 2337.1.1
		adm = 6
		dip = 0
		mil = 5
		female = yes
    }
	heir = {
 		name = "Arslone"
		monarch_name = "Arslone I"
		dynasty = "Nu-Miphodraaslo"
		birth_date = 2375.1.1
		death_date = 2471.1.1
		claim = 100
		adm = 3
		dip = 5
		mil = 4
		female = yes
    }
}

2471.1.1 = {
	monarch = {
 		name = "Panlaane"
		dynasty = "Ne-Jorlos"
		birth_date = 2443.1.1
		adm = 6
		dip = 0
		mil = 0
    }
}

