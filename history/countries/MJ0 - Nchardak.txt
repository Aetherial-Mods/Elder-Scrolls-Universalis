government = republic
government_rank = 3
mercantilism = 1
technology_group = dwemer_tg
religion = reason_and_logic_cult
primary_culture = dwemer
capital = 3320

54.1.1 = {
	monarch = {
 		name = "Szoglynsh"
		dynasty = "Az'Izvuvretch"
		birth_date = 19.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

96.1.1 = {
	monarch = {
 		name = "Talchanf"
		dynasty = "Af'Rhzorhunch"
		birth_date = 49.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

166.1.1 = {
	monarch = {
 		name = "Gonwess"
		dynasty = "Af'Chiuvnak"
		birth_date = 129.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

256.1.1 = {
	monarch = {
 		name = "Tnadrak"
		dynasty = "Az'Asratchzan"
		birth_date = 226.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

334.1.1 = {
	monarch = {
 		name = "Asramrumhz"
		dynasty = "Az'Gzonrynn"
		birth_date = 307.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

388.1.1 = {
	monarch = {
 		name = "Cfradrys"
		dynasty = "Af'Tzedrunz"
		birth_date = 340.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

443.1.1 = {
	monarch = {
 		name = "Ksrefurn"
		dynasty = "Av'Bzrazgar"
		birth_date = 396.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

494.1.1 = {
	monarch = {
 		name = "Somzlin"
		dynasty = "Af'Nchynac"
		birth_date = 459.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

562.1.1 = {
	monarch = {
 		name = "Rafnyg"
		dynasty = "Aq'Byrahken"
		birth_date = 524.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

635.1.1 = {
	monarch = {
 		name = "Churd"
		dynasty = "Af'Mchavin"
		birth_date = 584.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

710.1.1 = {
	monarch = {
 		name = "Chzetvar"
		dynasty = "Af'Chzevragch"
		birth_date = 669.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

769.1.1 = {
	monarch = {
 		name = "Miban"
		dynasty = "Av'Brehron"
		birth_date = 744.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

862.1.1 = {
	monarch = {
 		name = "Rafnyg"
		dynasty = "Av'Jlarerhunch"
		birth_date = 836.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

946.1.1 = {
	monarch = {
 		name = "Churd"
		dynasty = "Af'Rhzorhunch"
		birth_date = 916.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1044.1.1 = {
	monarch = {
 		name = "Snenard"
		dynasty = "Av'Czavzyrn"
		birth_date = 997.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1098.1.1 = {
	monarch = {
 		name = "Irhadac"
		dynasty = "Aq'Snenard"
		birth_date = 1062.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1182.1.1 = {
	monarch = {
 		name = "Izvulzarf"
		dynasty = "Af'Sthord"
		birth_date = 1158.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1242.1.1 = {
	monarch = {
 		name = "Ythamac"
		dynasty = "Az'Shtrorlis"
		birth_date = 1199.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1333.1.1 = {
	monarch = {
 		name = "Chruhnch"
		dynasty = "Aq'Mravlara"
		birth_date = 1287.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1409.1.1 = {
	monarch = {
 		name = "Brehron"
		dynasty = "Aq'Asranrynn"
		birth_date = 1386.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1499.1.1 = {
	monarch = {
 		name = "Asrahuanch"
		dynasty = "Af'Asrahuanch"
		birth_date = 1452.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1549.1.1 = {
	monarch = {
 		name = "Ychogarn"
		dynasty = "Aq'Ilzeglan"
		birth_date = 1529.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1598.1.1 = {
	monarch = {
 		name = "Jhouvin"
		dynasty = "Aq'Ararbira"
		birth_date = 1557.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1668.1.1 = {
	monarch = {
 		name = "Brehron"
		dynasty = "Az'Ylrefwinn"
		birth_date = 1650.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1758.1.1 = {
	monarch = {
 		name = "Asrahuanch"
		dynasty = "Af'Alnomratz"
		birth_date = 1717.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1849.1.1 = {
	monarch = {
 		name = "Czoudrys"
		dynasty = "Af'Achyggo"
		birth_date = 1796.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1907.1.1 = {
	monarch = {
 		name = "Mhuzgar"
		dynasty = "Az'Aknanwess"
		birth_date = 1883.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1993.1.1 = {
	monarch = {
 		name = "Crafrysz"
		dynasty = "Af'Chzetvar"
		birth_date = 1955.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2062.1.1 = {
	monarch = {
 		name = "Sthord"
		dynasty = "Af'Agahuanch"
		birth_date = 2016.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2100.1.1 = {
	monarch = {
 		name = "Mzamrumhz"
		dynasty = "Aq'Mebchasz"
		birth_date = 2074.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2156.1.1 = {
	monarch = {
 		name = "Sohner"
		dynasty = "Az'Nhezril"
		birth_date = 2110.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

2205.1.1 = {
	monarch = {
 		name = "Jlethurzch"
		dynasty = "Av'Czavzyrn"
		birth_date = 2179.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

2285.1.1 = {
	monarch = {
 		name = "Agahuanch"
		dynasty = "Az'Dzreglan"
		birth_date = 2240.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2322.1.1 = {
	monarch = {
 		name = "Jnavraz"
		dynasty = "Af'Chruhnch"
		birth_date = 2275.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2382.1.1 = {
	monarch = {
 		name = "Talchanf"
		dynasty = "Af'Churd"
		birth_date = 2339.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2470.1.1 = {
	monarch = {
 		name = "Ghaznak"
		dynasty = "Af'Goundam"
		birth_date = 2428.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

