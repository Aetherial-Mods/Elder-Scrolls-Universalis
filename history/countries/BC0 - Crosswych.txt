government = monarchy
government_rank = 1
mercantilism = 1
technology_group = northern_tg
religion = druidism
primary_culture = breton
capital = 6969

54.1.1 = {
	monarch = {
 		name = "Callice"
		dynasty = "Serien"
		birth_date = 1.1.1
		adm = 2
		dip = 2
		mil = 0
		female = yes
    }
	queen = {
 		name = "Archimbert"
		dynasty = "Serien"
		birth_date = 13.1.1
		adm = 6
		dip = 1
		mil = 6
    }
}

144.1.1 = {
	monarch = {
 		name = "Isabeth"
		dynasty = "Quirin"
		birth_date = 119.1.1
		adm = 4
		dip = 0
		mil = 3
		female = yes
    }
	queen = {
 		name = "Frederic"
		dynasty = "Quirin"
		birth_date = 119.1.1
		adm = 1
		dip = 5
		mil = 2
    }
	heir = {
 		name = "Velica"
		monarch_name = "Velica I"
		dynasty = "Quirin"
		birth_date = 133.1.1
		death_date = 228.1.1
		claim = 100
		adm = 5
		dip = 4
		mil = 1
		female = yes
    }
}

228.1.1 = {
	monarch = {
 		name = "Victraud"
		dynasty = "Franc"
		birth_date = 198.1.1
		adm = 4
		dip = 0
		mil = 1
    }
}

317.1.1 = {
	monarch = {
 		name = "Daniel"
		dynasty = "Geves"
		birth_date = 281.1.1
		adm = 3
		dip = 6
		mil = 5
    }
	queen = {
 		name = "Marcellyne"
		dynasty = "Geves"
		birth_date = 286.1.1
		adm = 6
		dip = 5
		mil = 4
		female = yes
    }
	heir = {
 		name = "Charbriel"
		monarch_name = "Charbriel I"
		dynasty = "Geves"
		birth_date = 313.1.1
		death_date = 400.1.1
		claim = 100
		adm = 3
		dip = 4
		mil = 3
    }
}

400.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Vanisande"
		monarch_name = "Vanisande I"
		dynasty = "Levys"
		birth_date = 393.1.1
		death_date = 411.1.1
		claim = 100
		adm = 2
		dip = 3
		mil = 0
		female = yes
    }
}

411.1.1 = {
	monarch = {
 		name = "Jeanne"
		dynasty = "Hearthfield"
		birth_date = 374.1.1
		adm = 4
		dip = 4
		mil = 2
		female = yes
    }
}

509.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Gerard"
		monarch_name = "Gerard I"
		dynasty = "Leraud"
		birth_date = 501.1.1
		death_date = 519.1.1
		claim = 100
		adm = 5
		dip = 1
		mil = 0
    }
}

519.1.1 = {
	monarch = {
 		name = "Yveline"
		dynasty = "Brigette"
		birth_date = 488.1.1
		adm = 1
		dip = 2
		mil = 2
		female = yes
    }
	queen = {
 		name = "Tetra"
		dynasty = "Brigette"
		birth_date = 495.1.1
		adm = 5
		dip = 1
		mil = 1
    }
	heir = {
 		name = "Gordon"
		monarch_name = "Gordon I"
		dynasty = "Brigette"
		birth_date = 517.1.1
		death_date = 555.1.1
		claim = 100
		adm = 1
		dip = 0
		mil = 0
    }
}

555.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Claumin"
		monarch_name = "Claumin I"
		dynasty = "Trebane"
		birth_date = 548.1.1
		death_date = 566.1.1
		claim = 100
		adm = 0
		dip = 6
		mil = 4
    }
}

566.1.1 = {
	monarch = {
 		name = "Nyrona"
		dynasty = "Mastien"
		birth_date = 518.1.1
		adm = 6
		dip = 2
		mil = 4
		female = yes
    }
}

642.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Morven"
		monarch_name = "Morven I"
		dynasty = "Sylbenitte"
		birth_date = 629.1.1
		death_date = 647.1.1
		claim = 100
		adm = 0
		dip = 6
		mil = 2
    }
}

647.1.1 = {
	monarch = {
 		name = "Darron"
		dynasty = "Vervins"
		birth_date = 625.1.1
		adm = 3
		dip = 1
		mil = 4
    }
}

739.1.1 = {
	monarch = {
 		name = "Zibaran"
		dynasty = "Longis"
		birth_date = 687.1.1
		adm = 1
		dip = 0
		mil = 1
    }
}

808.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Valentois"
		monarch_name = "Valentois I"
		dynasty = "Marigny"
		birth_date = 799.1.1
		death_date = 817.1.1
		claim = 100
		adm = 2
		dip = 4
		mil = 6
    }
}

817.1.1 = {
	monarch = {
 		name = "Joslin"
		dynasty = "Rocque"
		birth_date = 769.1.1
		adm = 4
		dip = 5
		mil = 1
		female = yes
    }
}

876.1.1 = {
	monarch = {
 		name = "Delphine"
		dynasty = "Erelle"
		birth_date = 826.1.1
		adm = 6
		dip = 6
		mil = 3
		female = yes
    }
	queen = {
 		name = "Brucetus"
		dynasty = "Erelle"
		birth_date = 833.1.1
		adm = 6
		dip = 3
		mil = 4
    }
}

973.1.1 = {
	monarch = {
 		name = "Jollivet"
		dynasty = "Frinck"
		birth_date = 953.1.1
		adm = 1
		dip = 4
		mil = 5
    }
	queen = {
 		name = "Isabeau"
		dynasty = "Frinck"
		birth_date = 954.1.1
		adm = 2
		dip = 1
		mil = 6
		female = yes
    }
	heir = {
 		name = "Adwig"
		monarch_name = "Adwig I"
		dynasty = "Frinck"
		birth_date = 968.1.1
		death_date = 1072.1.1
		claim = 100
		adm = 5
		dip = 6
		mil = 5
    }
}

1072.1.1 = {
	monarch = {
 		name = "Kauric"
		dynasty = "Nalskin"
		birth_date = 1051.1.1
		adm = 5
		dip = 3
		mil = 6
    }
	queen = {
 		name = "Vyctarine"
		dynasty = "Nalskin"
		birth_date = 1049.1.1
		adm = 2
		dip = 1
		mil = 5
		female = yes
    }
	heir = {
 		name = "Jacques"
		monarch_name = "Jacques I"
		dynasty = "Nalskin"
		birth_date = 1066.1.1
		death_date = 1132.1.1
		claim = 100
		adm = 6
		dip = 0
		mil = 4
    }
}

1132.1.1 = {
	monarch = {
 		name = "Antoine"
		dynasty = "Renaudin"
		birth_date = 1079.1.1
		adm = 1
		dip = 1
		mil = 6
    }
	queen = {
 		name = "Iona"
		dynasty = "Renaudin"
		birth_date = 1104.1.1
		adm = 5
		dip = 0
		mil = 5
		female = yes
    }
	heir = {
 		name = "Adnot"
		monarch_name = "Adnot I"
		dynasty = "Renaudin"
		birth_date = 1128.1.1
		death_date = 1204.1.1
		claim = 100
		adm = 2
		dip = 5
		mil = 4
    }
}

1204.1.1 = {
	monarch = {
 		name = "Kastus"
		dynasty = "Jenseric"
		birth_date = 1159.1.1
		adm = 5
		dip = 6
		mil = 6
    }
	queen = {
 		name = "Javierre"
		dynasty = "Jenseric"
		birth_date = 1166.1.1
		adm = 2
		dip = 5
		mil = 5
		female = yes
    }
}

1252.1.1 = {
	monarch = {
 		name = "Severine"
		dynasty = "Urbyn"
		birth_date = 1214.1.1
		adm = 0
		dip = 4
		mil = 2
		female = yes
    }
	queen = {
 		name = "Piper"
		dynasty = "Urbyn"
		birth_date = 1232.1.1
		adm = 4
		dip = 3
		mil = 1
    }
	heir = {
 		name = "Reynald"
		monarch_name = "Reynald I"
		dynasty = "Urbyn"
		birth_date = 1247.1.1
		death_date = 1314.1.1
		claim = 100
		adm = 1
		dip = 1
		mil = 0
    }
}

1314.1.1 = {
	monarch = {
 		name = "Florenot"
		dynasty = "Stoine"
		birth_date = 1287.1.1
		adm = 0
		dip = 5
		mil = 0
    }
	queen = {
 		name = "Edelinne"
		dynasty = "Stoine"
		birth_date = 1296.1.1
		adm = 0
		dip = 1
		mil = 1
		female = yes
    }
}

1410.1.1 = {
	monarch = {
 		name = "Mederic"
		dynasty = "Delatte"
		birth_date = 1385.1.1
		adm = 2
		dip = 2
		mil = 3
    }
	queen = {
 		name = "Luciana"
		dynasty = "Delatte"
		birth_date = 1369.1.1
		adm = 2
		dip = 6
		mil = 4
		female = yes
    }
	heir = {
 		name = "Berjac"
		monarch_name = "Berjac I"
		dynasty = "Delatte"
		birth_date = 1410.1.1
		death_date = 1454.1.1
		claim = 100
		adm = 6
		dip = 4
		mil = 3
    }
}

1454.1.1 = {
	monarch = {
 		name = "Morten"
		dynasty = "Henyra"
		birth_date = 1411.1.1
		adm = 6
		dip = 1
		mil = 3
    }
	queen = {
 		name = "Aren"
		dynasty = "Henyra"
		birth_date = 1434.1.1
		adm = 3
		dip = 6
		mil = 2
		female = yes
    }
	heir = {
 		name = "Malkoran"
		monarch_name = "Malkoran I"
		dynasty = "Henyra"
		birth_date = 1451.1.1
		death_date = 1516.1.1
		claim = 100
		adm = 6
		dip = 5
		mil = 1
    }
}

1516.1.1 = {
	monarch = {
 		name = "Christoph"
		dynasty = "Korine"
		birth_date = 1471.1.1
		adm = 2
		dip = 6
		mil = 3
    }
}

1600.1.1 = {
	monarch = {
 		name = "Vachel"
		dynasty = "Antienne"
		birth_date = 1582.1.1
		adm = 0
		dip = 5
		mil = 0
    }
	queen = {
 		name = "Esmie"
		dynasty = "Antienne"
		birth_date = 1559.1.1
		adm = 4
		dip = 4
		mil = 6
		female = yes
    }
	heir = {
 		name = "Sovelle"
		monarch_name = "Sovelle I"
		dynasty = "Antienne"
		birth_date = 1595.1.1
		death_date = 1669.1.1
		claim = 100
		adm = 1
		dip = 2
		mil = 5
		female = yes
    }
}

1669.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Margaux"
		monarch_name = "Margaux I"
		dynasty = "Hemmet"
		birth_date = 1665.1.1
		death_date = 1683.1.1
		claim = 100
		adm = 6
		dip = 2
		mil = 2
		female = yes
    }
}

1683.1.1 = {
	monarch = {
 		name = "Chedric"
		dynasty = "Arthe"
		birth_date = 1640.1.1
		adm = 6
		dip = 5
		mil = 2
    }
}

1761.1.1 = {
	monarch = {
 		name = "Usquebald"
		dynasty = "Aurilie"
		birth_date = 1716.1.1
		adm = 4
		dip = 4
		mil = 5
    }
}

1822.1.1 = {
	monarch = {
 		name = "Condier"
		dynasty = "Sette"
		birth_date = 1787.1.1
		adm = 2
		dip = 3
		mil = 2
    }
	queen = {
 		name = "Lysse"
		dynasty = "Sette"
		birth_date = 1776.1.1
		adm = 6
		dip = 2
		mil = 1
		female = yes
    }
	heir = {
 		name = "Brenlynne"
		monarch_name = "Brenlynne I"
		dynasty = "Sette"
		birth_date = 1807.1.1
		death_date = 1912.1.1
		claim = 100
		adm = 3
		dip = 1
		mil = 0
		female = yes
    }
}

1912.1.1 = {
	monarch = {
 		name = "Nepos"
		dynasty = "Hoger"
		birth_date = 1865.1.1
		adm = 6
		dip = 2
		mil = 2
    }
	queen = {
 		name = "Arzhela"
		dynasty = "Hoger"
		birth_date = 1878.1.1
		adm = 3
		dip = 0
		mil = 1
		female = yes
    }
	heir = {
 		name = "Marq"
		monarch_name = "Marq I"
		dynasty = "Hoger"
		birth_date = 1900.1.1
		death_date = 2003.1.1
		claim = 100
		adm = 0
		dip = 6
		mil = 0
    }
}

2003.1.1 = {
	monarch = {
 		name = "Charmela"
		dynasty = "Berene"
		birth_date = 1958.1.1
		adm = 2
		dip = 0
		mil = 3
		female = yes
    }
}

2052.1.1 = {
	monarch = {
 		name = "Vivian"
		dynasty = "Montieu"
		birth_date = 2014.1.1
		adm = 1
		dip = 0
		mil = 6
		female = yes
    }
	queen = {
 		name = "Socucius"
		dynasty = "Montieu"
		birth_date = 2031.1.1
		adm = 4
		dip = 5
		mil = 5
    }
}

2087.1.1 = {
	monarch = {
 		name = "Degaratien"
		dynasty = "Cottret"
		birth_date = 2038.1.1
		adm = 3
		dip = 4
		mil = 2
    }
}

2161.1.1 = {
	monarch = {
 		name = "Adiel"
		dynasty = "Gamache"
		birth_date = 2136.1.1
		adm = 5
		dip = 6
		mil = 3
		female = yes
    }
	queen = {
 		name = "Tolwin"
		dynasty = "Gamache"
		birth_date = 2131.1.1
		adm = 5
		dip = 2
		mil = 5
    }
	heir = {
 		name = "Janeve"
		monarch_name = "Janeve I"
		dynasty = "Gamache"
		birth_date = 2155.1.1
		death_date = 2232.1.1
		claim = 100
		adm = 2
		dip = 1
		mil = 4
		female = yes
    }
}

2232.1.1 = {
	monarch = {
 		name = "Alard"
		dynasty = "Cergend"
		birth_date = 2192.1.1
		adm = 1
		dip = 4
		mil = 4
    }
	queen = {
 		name = "Gwenaelle"
		dynasty = "Cergend"
		birth_date = 2211.1.1
		adm = 5
		dip = 2
		mil = 3
		female = yes
    }
}

2307.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Laranette"
		monarch_name = "Laranette I"
		dynasty = "Dugot"
		birth_date = 2305.1.1
		death_date = 2323.1.1
		claim = 100
		adm = 2
		dip = 4
		mil = 3
		female = yes
    }
}

2323.1.1 = {
	monarch = {
 		name = "Armel"
		dynasty = "Fenandre"
		birth_date = 2292.1.1
		adm = 1
		dip = 1
		mil = 3
    }
}

2398.1.1 = {
	monarch = {
 		name = "Ronerelie"
		dynasty = "Admand"
		birth_date = 2380.1.1
		adm = 0
		dip = 0
		mil = 0
		female = yes
    }
}

2462.1.1 = {
	monarch = {
 		name = "Krag"
		dynasty = "Vyau"
		birth_date = 2439.1.1
		adm = 5
		dip = 6
		mil = 3
    }
	queen = {
 		name = "Ylanie"
		dynasty = "Vyau"
		birth_date = 2423.1.1
		adm = 2
		dip = 5
		mil = 2
		female = yes
    }
	heir = {
 		name = "Jascien"
		monarch_name = "Jascien I"
		dynasty = "Vyau"
		birth_date = 2449.1.1
		death_date = 2511.1.1
		claim = 100
		adm = 6
		dip = 3
		mil = 1
    }
}

