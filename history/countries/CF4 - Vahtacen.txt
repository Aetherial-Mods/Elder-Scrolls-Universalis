government = theocracy
government_rank = 1
mercantilism = 1
technology_group = cyrodiil_tg
religion = vaermina_cult
primary_culture = ayleid
capital = 1254

54.1.1 = {
	monarch = {
 		name = "Nogo"
		dynasty = "Mea-Brina"
		birth_date = 18.1.1
		adm = 6
		dip = 2
		mil = 6
    }
}

116.1.1 = {
	monarch = {
 		name = "Vanhur"
		dynasty = "Vae-Vanua"
		birth_date = 77.1.1
		adm = 5
		dip = 1
		mil = 2
    }
}

156.1.1 = {
	monarch = {
 		name = "Irreshyva"
		dynasty = "Ula-Macelius"
		birth_date = 126.1.1
		adm = 3
		dip = 0
		mil = 6
		female = yes
    }
}

227.1.1 = {
	monarch = {
 		name = "Hadhuul"
		dynasty = "Vae-Weatherleah"
		birth_date = 178.1.1
		adm = 5
		dip = 2
		mil = 0
    }
}

301.1.1 = {
	monarch = {
 		name = "Nogo"
		dynasty = "Vae-Tasaso"
		birth_date = 262.1.1
		adm = 3
		dip = 1
		mil = 4
    }
}

341.1.1 = {
	monarch = {
 		name = "Laloriaran"
		dynasty = "Mea-Crownhaven"
		birth_date = 304.1.1
		adm = 1
		dip = 0
		mil = 1
    }
}

416.1.1 = {
	monarch = {
 		name = "Tedygedo"
		dynasty = "Ula-Horuria"
		birth_date = 375.1.1
		adm = 0
		dip = 6
		mil = 4
    }
}

487.1.1 = {
	monarch = {
 		name = "Qegylomo"
		dynasty = "Vae-Stirk"
		birth_date = 448.1.1
		adm = 5
		dip = 6
		mil = 1
    }
}

545.1.1 = {
	monarch = {
 		name = "Lonnosson"
		dynasty = "Vae-Vietia"
		birth_date = 521.1.1
		adm = 3
		dip = 5
		mil = 5
		female = yes
    }
}

607.1.1 = {
	monarch = {
 		name = "Faranwe"
		dynasty = "Vae-Vanua"
		birth_date = 570.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

662.1.1 = {
	monarch = {
 		name = "Tedygedo"
		dynasty = "Ula-Latara"
		birth_date = 611.1.1
		adm = 0
		dip = 3
		mil = 5
    }
}

705.1.1 = {
	monarch = {
 		name = "Cyrheganund"
		dynasty = "Mea-Calabolis"
		birth_date = 665.1.1
		adm = 1
		dip = 4
		mil = 6
    }
}

799.1.1 = {
	monarch = {
 		name = "Djus"
		dynasty = "Mea-Caumana"
		birth_date = 751.1.1
		adm = 0
		dip = 4
		mil = 3
    }
}

851.1.1 = {
	monarch = {
 		name = "Dondont"
		dynasty = "Vae-Sardavar"
		birth_date = 798.1.1
		adm = 5
		dip = 3
		mil = 0
    }
}

909.1.1 = {
	monarch = {
 		name = "Heystuadhisind"
		dynasty = "Vae-Vassorman"
		birth_date = 867.1.1
		adm = 3
		dip = 2
		mil = 3
    }
}

962.1.1 = {
	monarch = {
 		name = "Morilye"
		dynasty = "Ula-Ryndenyse"
		birth_date = 936.1.1
		adm = 4
		dip = 6
		mil = 1
		female = yes
    }
}

1055.1.1 = {
	monarch = {
 		name = "Haromir"
		dynasty = "Mea-Crownhaven"
		birth_date = 1008.1.1
		adm = 2
		dip = 5
		mil = 5
    }
}

1142.1.1 = {
	monarch = {
 		name = "Rorhemeras"
		dynasty = "Ula-Lildana"
		birth_date = 1096.1.1
		adm = 0
		dip = 4
		mil = 2
    }
}

1195.1.1 = {
	monarch = {
 		name = "Myzas"
		dynasty = "Mea-Egnagia"
		birth_date = 1166.1.1
		adm = 6
		dip = 4
		mil = 5
		female = yes
    }
}

1250.1.1 = {
	monarch = {
 		name = "Undorrau"
		dynasty = "Mea-Aderina"
		birth_date = 1212.1.1
		adm = 4
		dip = 3
		mil = 2
    }
}

1333.1.1 = {
	monarch = {
 		name = "Gordhaur"
		dynasty = "Mea-Allidenius"
		birth_date = 1308.1.1
		adm = 2
		dip = 2
		mil = 6
    }
}

1431.1.1 = {
	monarch = {
 		name = "Danasso"
		dynasty = "Ula-Plalusius"
		birth_date = 1391.1.1
		adm = 0
		dip = 1
		mil = 2
		female = yes
    }
}

1526.1.1 = {
	monarch = {
 		name = "Narges"
		dynasty = "Vae-Skingrad"
		birth_date = 1500.1.1
		adm = 2
		dip = 3
		mil = 4
    }
}

1574.1.1 = {
	monarch = {
 		name = "Muzos"
		dynasty = "Mea-Amane"
		birth_date = 1529.1.1
		adm = 1
		dip = 2
		mil = 0
		female = yes
    }
}

1661.1.1 = {
	monarch = {
 		name = "Gemhel"
		dynasty = "Mea-Corannus"
		birth_date = 1622.1.1
		adm = 6
		dip = 1
		mil = 4
    }
}

1757.1.1 = {
	monarch = {
 		name = "Lymher"
		dynasty = "Mea-Andrulusus"
		birth_date = 1739.1.1
		adm = 4
		dip = 0
		mil = 1
    }
}

1838.1.1 = {
	monarch = {
 		name = "Ymme"
		dynasty = "Vae-Weatherleah"
		birth_date = 1804.1.1
		adm = 2
		dip = 6
		mil = 4
		female = yes
    }
}

1899.1.1 = {
	monarch = {
 		name = "Djador"
		dynasty = "Mea-Conoa"
		birth_date = 1851.1.1
		adm = 3
		dip = 3
		mil = 2
    }
}

1941.1.1 = {
	monarch = {
 		name = "Houtern"
		dynasty = "Ula-Larilatia"
		birth_date = 1890.1.1
		adm = 1
		dip = 3
		mil = 6
    }
}

2005.1.1 = {
	monarch = {
 		name = "Azu"
		dynasty = "Ula-Nenyond"
		birth_date = 1974.1.1
		adm = 0
		dip = 2
		mil = 3
		female = yes
    }
}

2081.1.1 = {
	monarch = {
 		name = "Cyrheganund"
		dynasty = "Vae-Waelori"
		birth_date = 2040.1.1
		adm = 5
		dip = 1
		mil = 6
    }
}

2132.1.1 = {
	monarch = {
 		name = "Djus"
		dynasty = "Mea-Bravil"
		birth_date = 2096.1.1
		adm = 6
		dip = 5
		mil = 4
    }
}

2200.1.1 = {
	monarch = {
 		name = "Luudagarund"
		dynasty = "Mea-Bromma"
		birth_date = 2166.1.1
		adm = 4
		dip = 4
		mil = 1
    }
}

2275.1.1 = {
	monarch = {
 		name = "Filestis"
		dynasty = "Ula-Renne"
		birth_date = 2255.1.1
		adm = 2
		dip = 4
		mil = 4
    }
}

2342.1.1 = {
	monarch = {
 		name = "Leydel"
		dynasty = "Mea-Hastrel"
		birth_date = 2319.1.1
		adm = 0
		dip = 3
		mil = 1
    }
}

2389.1.1 = {
	monarch = {
 		name = "Hilli"
		dynasty = "Vae-Silorn"
		birth_date = 2351.1.1
		adm = 6
		dip = 2
		mil = 5
		female = yes
    }
}

2448.1.1 = {
	monarch = {
 		name = "Immol"
		dynasty = "Vae-Sepades"
		birth_date = 2408.1.1
		adm = 0
		dip = 3
		mil = 6
		female = yes
    }
}

