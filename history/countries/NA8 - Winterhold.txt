government = monarchy
government_rank = 3
mercantilism = 1
technology_group = northern_tg
religion = nordic_pantheon
primary_culture = nord
capital = 1299
elector = yes

historical_friend = WIT

54.1.1 = {
    monarch = {
 		name = "Geilir"
		dynasty = "Ysgramor"
		birth_date = 33.1.1
		adm = 5
		dip = 5
		mil = 4
    }
	heir = {
 		name = "Barirrid"
		monarch_name = "Barirrid I"
		dynasty = "Ysgramor"
		birth_date = 54.1.1
		death_date = 72.1.1
		claim = 100
		adm = 3
		dip = 4
		mil = 3
    }
}

72.1.1 = {
	monarch = {
 		name = "Merkyllian"
		dynasty = "Ysgramor"
		birth_date = 41.1.1
		adm = 5
		dip = 6
		mil = 6
    }
}

168.1.1 = {
	monarch = {
 		name = "Borgas"
		dynasty = "Ysgramor"
		birth_date = 147.1.1
		adm = 4
		dip = 5
		mil = 2
		female = yes
    }
}

241.1.1 = {
	monarch = {
 		name = "Braning"
		dynasty = "Winterhold"
		birth_date = 212.1.1
		adm = 3
		dip = 1
		mil = 3
		female = yes
    }
}

299.1.1 = {
	monarch = {
 		name = "Angbjar"
		dynasty = "Winterhold"
		birth_date = 269.1.1
		adm = 1
		dip = 1
		mil = 6
    }
}

348.1.1 = {
	monarch = {
 		name = "Restla"
		dynasty = "Winterhold"
		birth_date = 321.1.1
		adm = 6
		dip = 0
		mil = 3
		female = yes
    }
	queen = {
 		name = "Dryngheid"
		dynasty = "Hleirjin"
		birth_date = 325.1.1
		adm = 3
		dip = 5
		mil = 2
    }
	heir = {
 		name = "Odana"
		monarch_name = "Odana I"
		dynasty = "Winterhold"
		birth_date = 333.1.1
		death_date = 447.1.1
		claim = 100
		adm = 0
		dip = 4
		mil = 1
		female = yes
    }
}

447.1.1 = {
	monarch = {
 		name = "Gorm"
		dynasty = "Winterhold"
		birth_date = 396.1.1
		adm = 3
		dip = 5
		mil = 3
    }
	queen = {
 		name = "Elda"
		dynasty = "Bor"
		birth_date = 425.1.1
		adm = 0
		dip = 4
		mil = 2
		female = yes
    }
}

487.1.1 = {
	monarch = {
 		name = "Karita"
		dynasty = "Winterhold"
		birth_date = 450.1.1
		adm = 5
		dip = 3
		mil = 6
		female = yes
    }
}

578.1.1 = {
	monarch = {
 		name = "Greta"
		dynasty = "Winterhold"
		birth_date = 546.1.1
		adm = 0
		dip = 4
		mil = 0
		female = yes
    }
}

619.1.1 = {
	monarch = {
 		name = "Belgrod"
		dynasty = "Winterhold"
		birth_date = 581.1.1
		adm = 5
		dip = 3
		mil = 4
    }
	queen = {
 		name = "Adla"
		dynasty = "Storgath"
		birth_date = 586.1.1
		adm = 2
		dip = 2
		mil = 3
		female = yes
    }
	heir = {
 		name = "Jakidi"
		monarch_name = "Jakidi I"
		dynasty = "Winterhold"
		birth_date = 619.1.1
		death_date = 672.1.1
		claim = 100
		adm = 2
		dip = 5
		mil = 4
		female = yes
    }
}

672.1.1 = {
	monarch = {
 		name = "Bjartur"
		dynasty = "Winterhold"
		birth_date = 635.1.1
		adm = 2
		dip = 2
		mil = 4
		female = yes
    }
	queen = {
 		name = "Jolf"
		dynasty = "Dusnanl"
		birth_date = 639.1.1
		adm = 6
		dip = 0
		mil = 3
    }
	heir = {
 		name = "Aslfur"
		monarch_name = "Aslfur I"
		dynasty = "Winterhold"
		birth_date = 658.1.1
		death_date = 754.1.1
		claim = 100
		adm = 2
		dip = 6
		mil = 2
    }
}

754.1.1 = {
	monarch = {
 		name = "Knutrida"
		dynasty = "Winterhold"
		birth_date = 728.1.1
		adm = 5
		dip = 0
		mil = 5
		female = yes
    }
	queen = {
 		name = "Adding"
		dynasty = "Sirlannen"
		birth_date = 711.1.1
		adm = 2
		dip = 6
		mil = 3
    }
	heir = {
 		name = "Kott"
		monarch_name = "Kott I"
		dynasty = "Winterhold"
		birth_date = 744.1.1
		death_date = 802.1.1
		claim = 100
		adm = 6
		dip = 4
		mil = 2
    }
}

802.1.1 = {
	monarch = {
 		name = "Bjornolfr"
		dynasty = "Winterhold"
		birth_date = 772.1.1
		adm = 2
		dip = 6
		mil = 5
    }
	queen = {
 		name = "Agnenor"
		dynasty = "Svedryl"
		birth_date = 763.1.1
		adm = 6
		dip = 4
		mil = 4
		female = yes
    }
	heir = {
 		name = "Lenne"
		monarch_name = "Lenne I"
		dynasty = "Winterhold"
		birth_date = 791.1.1
		death_date = 855.1.1
		claim = 100
		adm = 2
		dip = 3
		mil = 3
    }
}

855.1.1 = {
	monarch = {
 		name = "Lynoit"
		dynasty = "Winterhold"
		birth_date = 804.1.1
		adm = 2
		dip = 6
		mil = 3
    }
	queen = {
 		name = "Imwyn"
		dynasty = "Feegnerr"
		birth_date = 834.1.1
		adm = 6
		dip = 5
		mil = 2
		female = yes
    }
	heir = {
 		name = "Barfel"
		monarch_name = "Barfel I"
		dynasty = "Winterhold"
		birth_date = 854.1.1
		death_date = 912.1.1
		claim = 100
		adm = 6
		dip = 1
		mil = 3
    }
}

912.1.1 = {
	monarch = {
 		name = "Meksim"
		dynasty = "Winterhold"
		birth_date = 869.1.1
		adm = 5
		dip = 5
		mil = 3
    }
	queen = {
 		name = "Agka"
		dynasty = "Veydef"
		birth_date = 859.1.1
		adm = 2
		dip = 3
		mil = 2
		female = yes
    }
	heir = {
 		name = "Lemkil"
		monarch_name = "Lemkil I"
		dynasty = "Winterhold"
		birth_date = 899.1.1
		death_date = 964.1.1
		claim = 100
		adm = 6
		dip = 2
		mil = 1
    }
}

964.1.1 = {
	monarch = {
 		name = "Bragur"
		dynasty = "Winterhold"
		birth_date = 937.1.1
		adm = 2
		dip = 3
		mil = 4
    }
	queen = {
 		name = "Ilfhild"
		dynasty = "Gildaldr"
		birth_date = 920.1.1
		adm = 6
		dip = 2
		mil = 3
		female = yes
    }
}

1027.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Larsa"
		monarch_name = "Larsa I"
		dynasty = "Winterhold"
		birth_date = 1013.1.1
		death_date = 1031.1.1
		claim = 100
		adm = 6
		dip = 6
		mil = 0
		female = yes
    }
}

1031.1.1 = {
	monarch = {
 		name = "Drod"
		dynasty = "Winterhold"
		birth_date = 998.1.1
		adm = 2
		dip = 0
		mil = 3
		female = yes
    }
}

1078.1.1 = {
	monarch = {
 		name = "Tirora"
		dynasty = "Winterhold"
		birth_date = 1027.1.1
		adm = 0
		dip = 6
		mil = 6
		female = yes
    }
}

1161.1.1 = {
	monarch = {
 		name = "Maren"
		dynasty = "Winterhold"
		birth_date = 1126.1.1
		adm = 2
		dip = 0
		mil = 1
		female = yes
    }
}

1217.1.1 = {
	monarch = {
 		name = "Horkvir"
		dynasty = "Winterhold"
		birth_date = 1197.1.1
		adm = 1
		dip = 0
		mil = 5
    }
}

1263.1.1 = {
	monarch = {
 		name = "Ottglund"
		dynasty = "Winterhold"
		birth_date = 1221.1.1
		adm = 6
		dip = 6
		mil = 1
    }
	queen = {
 		name = "Balwyn"
		dynasty = "Svedryl"
		birth_date = 1224.1.1
		adm = 3
		dip = 4
		mil = 0
		female = yes
    }
}

1321.1.1 = {
	monarch = {
 		name = "Adding"
		dynasty = "Winterhold"
		birth_date = 1268.1.1
		adm = 1
		dip = 4
		mil = 4
    }
	queen = {
 		name = "Haltafi"
		dynasty = "Valdubild"
		birth_date = 1273.1.1
		adm = 5
		dip = 2
		mil = 3
		female = yes
    }
	heir = {
 		name = "Lodvar"
		monarch_name = "Lodvar I"
		dynasty = "Winterhold"
		birth_date = 1315.1.1
		death_date = 1412.1.1
		claim = 100
		adm = 2
		dip = 1
		mil = 2
    }
}

1412.1.1 = {
	monarch = {
 		name = "Jofstrom"
		dynasty = "Winterhold"
		birth_date = 1371.1.1
		adm = 4
		dip = 2
		mil = 4
    }
}

1469.1.1 = {
	monarch = {
 		name = "Feiri"
		dynasty = "Winterhold"
		birth_date = 1449.1.1
		adm = 3
		dip = 1
		mil = 1
		female = yes
    }
}

1518.1.1 = {
	monarch = {
 		name = "Yust"
		dynasty = "Winterhold"
		birth_date = 1478.1.1
		adm = 2
		dip = 5
		mil = 1
    }
	queen = {
 		name = "Jafelma"
		dynasty = "Keld"
		birth_date = 1498.1.1
		adm = 6
		dip = 3
		mil = 0
		female = yes
    }
	heir = {
 		name = "Bjaron"
		monarch_name = "Bjaron I"
		dynasty = "Winterhold"
		birth_date = 1510.1.1
		death_date = 1561.1.1
		claim = 100
		adm = 6
		dip = 0
		mil = 1
    }
}

1561.1.1 = {
	monarch = {
 		name = "Njordunn"
		dynasty = "Winterhold"
		birth_date = 1525.1.1
		adm = 6
		dip = 3
		mil = 1
    }
	queen = {
 		name = "Hel"
		dynasty = "Hjemuld"
		birth_date = 1540.1.1
		adm = 2
		dip = 2
		mil = 0
		female = yes
    }
	heir = {
 		name = "Lungird"
		monarch_name = "Lungird I"
		dynasty = "Winterhold"
		birth_date = 1558.1.1
		death_date = 1605.1.1
		claim = 100
		adm = 6
		dip = 0
		mil = 6
    }
}

1605.1.1 = {
	monarch = {
 		name = "Cathja"
		dynasty = "Winterhold"
		birth_date = 1581.1.1
		adm = 2
		dip = 1
		mil = 2
		female = yes
    }
	queen = {
 		name = "Kodrir"
		dynasty = "Vodrold"
		birth_date = 1569.1.1
		adm = 6
		dip = 0
		mil = 1
    }
	heir = {
 		name = "Birkir"
		monarch_name = "Birkir I"
		dynasty = "Winterhold"
		birth_date = 1602.1.1
		death_date = 1694.1.1
		claim = 100
		adm = 3
		dip = 5
		mil = 0
    }
}

1694.1.1 = {
	monarch = {
 		name = "Lyris"
		dynasty = "Winterhold"
		birth_date = 1656.1.1
		adm = 5
		dip = 0
		mil = 2
		female = yes
    }
}

1792.1.1 = {
	monarch = {
 		name = "Hofgrir"
		dynasty = "Winterhold"
		birth_date = 1742.1.1
		adm = 0
		dip = 1
		mil = 4
    }
	queen = {
 		name = "Geinarre"
		dynasty = "Kigrildr"
		birth_date = 1755.1.1
		adm = 1
		dip = 5
		mil = 5
		female = yes
    }
	heir = {
 		name = "Suwenna"
		monarch_name = "Suwenna I"
		dynasty = "Winterhold"
		birth_date = 1781.1.1
		death_date = 1865.1.1
		claim = 100
		adm = 4
		dip = 3
		mil = 4
		female = yes
    }
}

1865.1.1 = {
	monarch = {
 		name = "Horkvir"
		dynasty = "Winterhold"
		birth_date = 1831.1.1
		adm = 4
		dip = 6
		mil = 4
    }
}

1903.1.1 = {
	monarch = {
 		name = "Didrika"
		dynasty = "Winterhold"
		birth_date = 1860.1.1
		adm = 2
		dip = 6
		mil = 1
		female = yes
    }
	queen = {
 		name = "Lars"
		dynasty = "Deesmal"
		birth_date = 1882.1.1
		adm = 6
		dip = 4
		mil = 6
    }
}

1947.1.1 = {
	monarch = {
 		name = "Itar"
		dynasty = "Winterhold"
		birth_date = 1927.1.1
		adm = 4
		dip = 3
		mil = 3
    }
	queen = {
 		name = "Suretta"
		dynasty = "Tholnofeldr"
		birth_date = 1916.1.1
		adm = 1
		dip = 2
		mil = 2
		female = yes
    }
}

1996.1.1 = {
	monarch = {
 		name = "Skaldir"
		dynasty = "Winterhold"
		birth_date = 1972.1.1
		adm = 6
		dip = 1
		mil = 6
    }
	queen = {
 		name = "Deldwine"
		dynasty = "Vilskemild"
		birth_date = 1956.1.1
		adm = 3
		dip = 0
		mil = 5
		female = yes
    }
	heir = {
 		name = "Nikolvara"
		monarch_name = "Nikolvara I"
		dynasty = "Winterhold"
		birth_date = 1981.1.1
		death_date = 2057.1.1
		claim = 100
		adm = 0
		dip = 5
		mil = 4
		female = yes
    }
}

2057.1.1 = {
	monarch = {
 		name = "Fresmara"
		dynasty = "Winterhold"
		birth_date = 2028.1.1
		adm = 3
		dip = 0
		mil = 6
		female = yes
    }
}

2131.1.1 = {
	monarch = {
 		name = "Aldi"
		dynasty = "Winterhold"
		birth_date = 2098.1.1
		adm = 1
		dip = 6
		mil = 3
		female = yes
    }
}

2226.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Wilmuth"
		monarch_name = "Wilmuth I"
		dynasty = "Winterhold"
		birth_date = 2217.1.1
		death_date = 2235.1.1
		claim = 100
		adm = 2
		dip = 3
		mil = 1
    }
}

2235.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Farvild"
		monarch_name = "Farvild I"
		dynasty = "Winterhold"
		birth_date = 2233.1.1
		death_date = 2251.1.1
		claim = 100
		adm = 0
		dip = 2
		mil = 4
		female = yes
    }
}

2251.1.1 = {
	monarch = {
 		name = "Gjalund"
		dynasty = "Winterhold"
		birth_date = 2229.1.1
		adm = 6
		dip = 5
		mil = 5
    }
	queen = {
 		name = "Eila"
		dynasty = "Svednun"
		birth_date = 2219.1.1
		adm = 3
		dip = 4
		mil = 3
		female = yes
    }
	heir = {
 		name = "Ogra"
		monarch_name = "Ogra I"
		dynasty = "Winterhold"
		birth_date = 2249.1.1
		death_date = 2308.1.1
		claim = 100
		adm = 4
		dip = 0
		mil = 5
		female = yes
    }
}

2308.1.1 = {
	monarch = {
 		name = "Grisvar"
		dynasty = "Winterhold"
		birth_date = 2273.1.1
		adm = 3
		dip = 4
		mil = 5
    }
	queen = {
 		name = "Maylde"
		dynasty = "Hjangris"
		birth_date = 2273.1.1
		adm = 0
		dip = 2
		mil = 4
		female = yes
    }
	heir = {
 		name = "Fultheim"
		monarch_name = "Fultheim I"
		dynasty = "Winterhold"
		birth_date = 2295.1.1
		death_date = 2368.1.1
		claim = 100
		adm = 4
		dip = 1
		mil = 3
    }
}

2368.1.1 = {
	monarch = {
 		name = "Slonn"
		dynasty = "Winterhold"
		birth_date = 2315.1.1
		adm = 6
		dip = 2
		mil = 5
    }
	queen = {
 		name = "Eepa"
		dynasty = "Vif"
		birth_date = 2331.1.1
		adm = 3
		dip = 1
		mil = 4
		female = yes
    }
	heir = {
 		name = "Odvar"
		monarch_name = "Odvar I"
		dynasty = "Winterhold"
		birth_date = 2365.1.1
		death_date = 2421.1.1
		claim = 100
		adm = 0
		dip = 6
		mil = 3
		female = yes
    }
}

2421.1.1 = {
	monarch = {
 		name = "Grimissar"
		dynasty = "Winterhold"
		birth_date = 2375.1.1
		adm = 3
		dip = 1
		mil = 5
    }
}

