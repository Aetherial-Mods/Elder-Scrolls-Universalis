government = native
government_rank = 1
mercantilism = 1
technology_group = marshan_tg
religion = hist
primary_culture = naga
capital = 983

54.1.1 = {
	monarch = {
 		name = "Vureiem"
		dynasty = "Casdorus"
		birth_date = 4.1.1
		adm = 4
		dip = 0
		mil = 5
		female = yes
    }
}

100.1.1 = {
	monarch = {
 		name = "Peek-Ereel"
		dynasty = "Cascalees"
		birth_date = 58.1.1
		adm = 2
		dip = 6
		mil = 1
		female = yes
    }
}

175.1.1 = {
	monarch = {
 		name = "Wanum-Neeus"
		dynasty = "Jeeraz"
		birth_date = 136.1.1
		adm = 0
		dip = 6
		mil = 5
    }
}

210.1.1 = {
	monarch = {
 		name = "Peek-Shah"
		dynasty = "Silm-Na"
		birth_date = 184.1.1
		adm = 6
		dip = 5
		mil = 2
    }
}

298.1.1 = {
	monarch = {
 		name = "Kankeeus"
		dynasty = "Talen-Ei"
		birth_date = 247.1.1
		adm = 4
		dip = 4
		mil = 5
    }
}

384.1.1 = {
	monarch = {
 		name = "Chuxu"
		dynasty = "Endoremus"
		birth_date = 340.1.1
		adm = 2
		dip = 3
		mil = 2
    }
}

477.1.1 = {
	monarch = {
 		name = "Weewish"
		dynasty = "Jeeraz"
		birth_date = 438.1.1
		adm = 0
		dip = 2
		mil = 6
		female = yes
    }
}

550.1.1 = {
	monarch = {
 		name = "Paxalt"
		dynasty = "Nagdeseer"
		birth_date = 508.1.1
		adm = 6
		dip = 2
		mil = 2
    }
}

587.1.1 = {
	monarch = {
 		name = "Kamatpa"
		dynasty = "Sakeides"
		birth_date = 541.1.1
		adm = 1
		dip = 3
		mil = 4
    }
}

635.1.1 = {
	monarch = {
 		name = "Chukka-Jekka"
		dynasty = "Derkeeja"
		birth_date = 608.1.1
		adm = 6
		dip = 2
		mil = 0
    }
}

722.1.1 = {
	monarch = {
 		name = "Keemeen"
		dynasty = "Huzdeek"
		birth_date = 702.1.1
		adm = 4
		dip = 1
		mil = 4
    }
}

769.1.1 = {
	monarch = {
 		name = "Dashnu"
		dynasty = "Jeetum-Lei"
		birth_date = 726.1.1
		adm = 2
		dip = 1
		mil = 1
    }
}

829.1.1 = {
	monarch = {
 		name = "Wuja-Meenus"
		dynasty = "Neethcalees"
		birth_date = 800.1.1
		adm = 1
		dip = 0
		mil = 4
		female = yes
    }
}

887.1.1 = {
	monarch = {
 		name = "Pideelus"
		dynasty = "Reeeepa"
		birth_date = 840.1.1
		adm = 6
		dip = 6
		mil = 1
    }
}

979.1.1 = {
	monarch = {
 		name = "Keema-Ru"
		dynasty = "Androdes"
		birth_date = 931.1.1
		adm = 4
		dip = 5
		mil = 5
    }
}

1067.1.1 = {
	monarch = {
 		name = "Dar-Jasa"
		dynasty = "Pergoulus"
		birth_date = 1018.1.1
		adm = 2
		dip = 4
		mil = 1
    }
}

1136.1.1 = {
	monarch = {
 		name = "Wixil"
		dynasty = "Augussareth"
		birth_date = 1102.1.1
		adm = 4
		dip = 6
		mil = 3
    }
}

1183.1.1 = {
	monarch = {
 		name = "Pekai-Jee"
		dynasty = "Reeeepa"
		birth_date = 1132.1.1
		adm = 2
		dip = 5
		mil = 0
    }
}

1240.1.1 = {
	monarch = {
 		name = "Xil-Jekka"
		dynasty = "Otumei"
		birth_date = 1192.1.1
		adm = 1
		dip = 4
		mil = 3
    }
}

1323.1.1 = {
	monarch = {
 		name = "Rupaheel"
		dynasty = "Teegeixth"
		birth_date = 1289.1.1
		adm = 6
		dip = 3
		mil = 0
		female = yes
    }
}

1401.1.1 = {
	monarch = {
 		name = "Kiameed"
		dynasty = "Endoremus"
		birth_date = 1371.1.1
		adm = 4
		dip = 2
		mil = 4
    }
}

1498.1.1 = {
	monarch = {
 		name = "Deejeeta"
		dynasty = "Theodesh"
		birth_date = 1469.1.1
		adm = 2
		dip = 2
		mil = 0
    }
}

1566.1.1 = {
	monarch = {
 		name = "Xazar"
		dynasty = "Androteus"
		birth_date = 1526.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1658.1.1 = {
	monarch = {
 		name = "Ruja-Wan"
		dynasty = "Mereemesh"
		birth_date = 1609.1.1
		adm = 6
		dip = 0
		mil = 0
		female = yes
    }
}

1750.1.1 = {
	monarch = {
 		name = "Kepanuu"
		dynasty = "Theermarush"
		birth_date = 1711.1.1
		adm = 1
		dip = 1
		mil = 2
    }
}

1840.1.1 = {
	monarch = {
 		name = "Deemat"
		dynasty = "Taierlures"
		birth_date = 1806.1.1
		adm = 6
		dip = 1
		mil = 6
		female = yes
    }
}

1889.1.1 = {
	monarch = {
 		name = "Kud-Lan"
		dynasty = "Theotius"
		birth_date = 1855.1.1
		adm = 4
		dip = 0
		mil = 2
    }
}

1961.1.1 = {
	monarch = {
 		name = "Dosa"
		dynasty = "Pergoulus"
		birth_date = 1916.1.1
		adm = 3
		dip = 6
		mil = 6
		female = yes
    }
}

1999.1.1 = {
	monarch = {
 		name = "Yelus"
		dynasty = "Neethsareeth"
		birth_date = 1972.1.1
		adm = 1
		dip = 5
		mil = 3
    }
}

2081.1.1 = {
	monarch = {
 		name = "Redieeus"
		dynasty = "Taierleesh"
		birth_date = 2036.1.1
		adm = 6
		dip = 4
		mil = 6
    }
}

2134.1.1 = {
	monarch = {
 		name = "Marz-Loh"
		dynasty = "Tee-Zeeus"
		birth_date = 2103.1.1
		adm = 4
		dip = 4
		mil = 3
		female = yes
    }
}

2171.1.1 = {
	monarch = {
 		name = "Deetum-Gih"
		dynasty = "Caetius"
		birth_date = 2150.1.1
		adm = 3
		dip = 3
		mil = 0
    }
}

2236.1.1 = {
	monarch = {
 		name = "Xokomar"
		dynasty = "Tibersifon"
		birth_date = 2191.1.1
		adm = 4
		dip = 4
		mil = 1
		female = yes
    }
}

2308.1.1 = {
	monarch = {
 		name = "Ratulmdutsei"
		dynasty = "Xemclesh"
		birth_date = 2274.1.1
		adm = 3
		dip = 3
		mil = 5
    }
}

2346.1.1 = {
	monarch = {
 		name = "Ahaht-Kilaya"
		dynasty = "Otumei"
		birth_date = 2325.1.1
		adm = 1
		dip = 2
		mil = 1
    }
}

2431.1.1 = {
	monarch = {
 		name = "Rultkath"
		dynasty = "Silm-Na"
		birth_date = 2402.1.1
		adm = 6
		dip = 2
		mil = 5
    }
}

