government = monarchy
add_government_reform = elective_monarchy_reform
government_rank = 7
mercantilism = 1
technology_group = northern_tg
religion = nordic_pantheon
primary_culture = nord
capital = 1275

54.1.1 = {
	monarch = {
 		name = "Poguf"
		dynasty = "Delsgoll"
		birth_date = 31.1.1
		adm = 6
		dip = 6
		mil = 1
    }
	queen = {
 		name = "Bekwine"
		dynasty = "Delsgoll"
		birth_date = 11.1.1
		adm = 3
		dip = 4
		mil = 0
		female = yes
    }
	heir = {
 		name = "Lisylde"
		monarch_name = "Lisylde I"
		dynasty = "Delsgoll"
		birth_date = 42.1.1
		death_date = 151.1.1
		claim = 100
		adm = 0
		dip = 3
		mil = 6
		female = yes
    }
}

151.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Hanmaer"
		monarch_name = "Hanmaer I"
		dynasty = "Told"
		birth_date = 139.1.1
		death_date = 157.1.1
		claim = 100
		adm = 5
		dip = 2
		mil = 2
		female = yes
    }
}

157.1.1 = {
	monarch = {
 		name = "Vilod"
		dynasty = "Gendodr"
		birth_date = 133.1.1
		adm = 1
		dip = 3
		mil = 5
    }
	queen = {
 		name = "Sorga"
		dynasty = "Gendodr"
		birth_date = 118.1.1
		adm = 4
		dip = 2
		mil = 4
		female = yes
    }
}

216.1.1 = {
	monarch = {
 		name = "Gardalgar"
		dynasty = "Valdubild"
		birth_date = 179.1.1
		adm = 3
		dip = 1
		mil = 0
    }
	queen = {
 		name = "Daljari"
		dynasty = "Valdubild"
		birth_date = 185.1.1
		adm = 0
		dip = 0
		mil = 6
		female = yes
    }
	heir = {
 		name = "Eirul"
		monarch_name = "Eirul I"
		dynasty = "Valdubild"
		birth_date = 216.1.1
		death_date = 298.1.1
		claim = 100
		adm = 3
		dip = 5
		mil = 5
		female = yes
    }
}

298.1.1 = {
	monarch = {
 		name = "Olfrid"
		dynasty = "Steedeth"
		birth_date = 255.1.1
		adm = 3
		dip = 1
		mil = 6
		female = yes
    }
}

356.1.1 = {
	monarch = {
 		name = "Huolde"
		dynasty = "Kirkom"
		birth_date = 334.1.1
		adm = 1
		dip = 1
		mil = 2
		female = yes
    }
	queen = {
 		name = "Holgeir"
		dynasty = "Kirkom"
		birth_date = 309.1.1
		adm = 5
		dip = 6
		mil = 1
    }
	heir = {
 		name = "Vari"
		monarch_name = "Vari I"
		dynasty = "Kirkom"
		birth_date = 356.1.1
		death_date = 423.1.1
		claim = 100
		adm = 5
		dip = 3
		mil = 2
		female = yes
    }
}

423.1.1 = {
	monarch = {
 		name = "Inge"
		dynasty = "Keld"
		birth_date = 403.1.1
		adm = 5
		dip = 6
		mil = 3
		female = yes
    }
}

495.1.1 = {
	monarch = {
 		name = "Girunn"
		dynasty = "Frowunnirr"
		birth_date = 446.1.1
		adm = 3
		dip = 5
		mil = 6
    }
}

561.1.1 = {
	monarch = {
 		name = "Akovor"
		dynasty = "Dusnanl"
		birth_date = 528.1.1
		adm = 1
		dip = 5
		mil = 3
    }
	queen = {
 		name = "Hela"
		dynasty = "Dusnanl"
		birth_date = 508.1.1
		adm = 5
		dip = 3
		mil = 2
		female = yes
    }
	heir = {
 		name = "Vaoldelf"
		monarch_name = "Vaoldelf I"
		dynasty = "Dusnanl"
		birth_date = 552.1.1
		death_date = 648.1.1
		claim = 100
		adm = 2
		dip = 2
		mil = 1
		female = yes
    }
}

648.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Narana"
		monarch_name = "Narana I"
		dynasty = "Mesmannath"
		birth_date = 647.1.1
		death_date = 665.1.1
		claim = 100
		adm = 0
		dip = 1
		mil = 4
		female = yes
    }
}

665.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Ylgar"
		monarch_name = "Ylgar I"
		dynasty = "Hledrind"
		birth_date = 655.1.1
		death_date = 673.1.1
		claim = 100
		adm = 5
		dip = 0
		mil = 1
    }
}

673.1.1 = {
	monarch = {
 		name = "Agning"
		dynasty = "Bretgolak"
		birth_date = 643.1.1
		adm = 5
		dip = 3
		mil = 1
    }
}

736.1.1 = {
	monarch = {
 		name = "Gomund"
		dynasty = "Rerlok"
		birth_date = 707.1.1
		adm = 3
		dip = 3
		mil = 5
    }
}

788.1.1 = {
	monarch = {
 		name = "Alvring"
		dynasty = "Foermeskr"
		birth_date = 769.1.1
		adm = 1
		dip = 2
		mil = 2
    }
	queen = {
 		name = "Hestla"
		dynasty = "Foermeskr"
		birth_date = 751.1.1
		adm = 5
		dip = 0
		mil = 1
		female = yes
    }
	heir = {
 		name = "Mathies"
		monarch_name = "Mathies I"
		dynasty = "Foermeskr"
		birth_date = 780.1.1
		death_date = 847.1.1
		claim = 100
		adm = 6
		dip = 1
		mil = 5
    }
}

847.1.1 = {
	monarch = {
 		name = "Dratholar"
		dynasty = "Hledrind"
		birth_date = 801.1.1
		adm = 5
		dip = 0
		mil = 2
    }
}

900.1.1 = {
	monarch = {
 		name = "Gogvir"
		dynasty = "Brungolg"
		birth_date = 876.1.1
		adm = 4
		dip = 4
		mil = 2
    }
}

974.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Hjornskar"
		monarch_name = "Hjornskar I"
		dynasty = "Stisgreskr"
		birth_date = 968.1.1
		death_date = 986.1.1
		claim = 100
		adm = 1
		dip = 6
		mil = 2
    }
}

986.1.1 = {
	monarch = {
 		name = "Trynhild"
		dynasty = "Kadref"
		birth_date = 959.1.1
		adm = 1
		dip = 2
		mil = 3
		female = yes
    }
	queen = {
 		name = "Halof"
		dynasty = "Kadref"
		birth_date = 960.1.1
		adm = 4
		dip = 1
		mil = 1
    }
	heir = {
 		name = "Temgrid"
		monarch_name = "Temgrid I"
		dynasty = "Kadref"
		birth_date = 978.1.1
		death_date = 1030.1.1
		claim = 100
		adm = 1
		dip = 6
		mil = 0
		female = yes
    }
}

1030.1.1 = {
	monarch = {
 		name = "Hjalari"
		dynasty = "Kamen"
		birth_date = 995.1.1
		adm = 4
		dip = 0
		mil = 3
		female = yes
    }
	queen = {
 		name = "Talsold"
		dynasty = "Kamen"
		birth_date = 1006.1.1
		adm = 1
		dip = 6
		mil = 2
    }
	heir = {
 		name = "Halmaera"
		monarch_name = "Halmaera I"
		dynasty = "Kamen"
		birth_date = 1016.1.1
		death_date = 1109.1.1
		claim = 100
		adm = 5
		dip = 5
		mil = 1
		female = yes
    }
}

1109.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Brey"
		monarch_name = "Brey I"
		dynasty = "Hregjaf"
		birth_date = 1097.1.1
		death_date = 1115.1.1
		claim = 100
		adm = 3
		dip = 4
		mil = 4
		female = yes
    }
}

1115.1.1 = {
	monarch = {
 		name = "Ortis"
		dynasty = "Hregjaf"
		birth_date = 1066.1.1
		adm = 2
		dip = 0
		mil = 5
    }
	queen = {
 		name = "Kirsten"
		dynasty = "Hregjaf"
		birth_date = 1082.1.1
		adm = 3
		dip = 4
		mil = 6
		female = yes
    }
	heir = {
 		name = "Dagimar"
		monarch_name = "Dagimar I"
		dynasty = "Hregjaf"
		birth_date = 1110.1.1
		death_date = 1189.1.1
		claim = 100
		adm = 6
		dip = 2
		mil = 5
    }
}

1189.1.1 = {
	monarch = {
 		name = "Narana"
		dynasty = "Hledrind"
		birth_date = 1153.1.1
		adm = 6
		dip = 6
		mil = 5
		female = yes
    }
	queen = {
 		name = "Bjadmund"
		dynasty = "Hledrind"
		birth_date = 1149.1.1
		adm = 3
		dip = 4
		mil = 4
    }
	heir = {
 		name = "Njial"
		monarch_name = "Njial I"
		dynasty = "Hledrind"
		birth_date = 1180.1.1
		death_date = 1244.1.1
		claim = 100
		adm = 0
		dip = 3
		mil = 3
    }
}

1244.1.1 = {
	monarch = {
 		name = "Falfinar"
		dynasty = "Stegath"
		birth_date = 1220.1.1
		adm = 2
		dip = 4
		mil = 5
    }
}

1292.1.1 = {
	monarch = {
 		name = "Valerica"
		dynasty = "Fudemth"
		birth_date = 1271.1.1
		adm = 1
		dip = 3
		mil = 2
		female = yes
    }
	queen = {
 		name = "Harnwulf"
		dynasty = "Fudemth"
		birth_date = 1268.1.1
		adm = 4
		dip = 2
		mil = 1
    }
	heir = {
 		name = "Thonlynn"
		monarch_name = "Thonlynn I"
		dynasty = "Fudemth"
		birth_date = 1288.1.1
		death_date = 1330.1.1
		claim = 100
		adm = 1
		dip = 0
		mil = 0
		female = yes
    }
}

1330.1.1 = {
	monarch = {
 		name = "Idofrid"
		dynasty = "Dasgron"
		birth_date = 1280.1.1
		adm = 4
		dip = 2
		mil = 2
    }
	queen = {
 		name = "Gurilda"
		dynasty = "Dasgron"
		birth_date = 1277.1.1
		adm = 1
		dip = 0
		mil = 1
		female = yes
    }
	heir = {
 		name = "Tiredir"
		monarch_name = "Tiredir I"
		dynasty = "Dasgron"
		birth_date = 1322.1.1
		death_date = 1372.1.1
		claim = 100
		adm = 5
		dip = 6
		mil = 0
		female = yes
    }
}

1372.1.1 = {
	monarch = {
 		name = "Vaereid"
		dynasty = "Rhyrrerik"
		birth_date = 1333.1.1
		adm = 4
		dip = 2
		mil = 0
		female = yes
    }
	queen = {
 		name = "Thrihild"
		dynasty = "Rhyrrerik"
		birth_date = 1349.1.1
		adm = 1
		dip = 1
		mil = 6
    }
	heir = {
 		name = "Horgeir"
		monarch_name = "Horgeir I"
		dynasty = "Rhyrrerik"
		birth_date = 1359.1.1
		death_date = 1427.1.1
		claim = 100
		adm = 1
		dip = 4
		mil = 0
    }
}

1427.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Dogun"
		monarch_name = "Dogun I"
		dynasty = "Ferleld"
		birth_date = 1422.1.1
		death_date = 1440.1.1
		claim = 100
		adm = 3
		dip = 5
		mil = 2
    }
}

1440.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Valdar"
		monarch_name = "Valdar I"
		dynasty = "Fosmil"
		birth_date = 1438.1.1
		death_date = 1456.1.1
		claim = 100
		adm = 1
		dip = 5
		mil = 6
    }
}

1456.1.1 = {
	monarch = {
 		name = "Istler"
		dynasty = "Finremth"
		birth_date = 1415.1.1
		adm = 4
		dip = 6
		mil = 1
    }
	queen = {
 		name = "Stodlod"
		dynasty = "Finremth"
		birth_date = 1421.1.1
		adm = 1
		dip = 4
		mil = 0
		female = yes
    }
	heir = {
 		name = "Hengeki"
		monarch_name = "Hengeki I"
		dynasty = "Finremth"
		birth_date = 1443.1.1
		death_date = 1518.1.1
		claim = 100
		adm = 5
		dip = 3
		mil = 6
		female = yes
    }
}

1518.1.1 = {
	monarch = {
 		name = "Wilvor"
		dynasty = "Svugjis"
		birth_date = 1487.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1584.1.1 = {
	monarch = {
 		name = "Rigvar"
		dynasty = "Rolrifodr"
		birth_date = 1531.1.1
		adm = 3
		dip = 6
		mil = 3
    }
	queen = {
 		name = "Liezl"
		dynasty = "Rolrifodr"
		birth_date = 1547.1.1
		adm = 3
		dip = 2
		mil = 4
		female = yes
    }
	heir = {
 		name = "Eldjar"
		monarch_name = "Eldjar I"
		dynasty = "Rolrifodr"
		birth_date = 1571.1.1
		death_date = 1652.1.1
		claim = 100
		adm = 0
		dip = 1
		mil = 3
    }
}

1652.1.1 = {
	monarch = {
 		name = "Rorygg"
		dynasty = "Thedrildr"
		birth_date = 1625.1.1
		adm = 6
		dip = 4
		mil = 3
    }
}

1751.1.1 = {
	monarch = {
 		name = "Hrosaa"
		dynasty = "Snake-eye"
		birth_date = 1707.1.1
		adm = 4
		dip = 3
		mil = 0
		female = yes
    }
	queen = {
 		name = "Torkalgar"
		dynasty = "Snake-eye"
		birth_date = 1704.1.1
		adm = 1
		dip = 2
		mil = 6
    }
	heir = {
 		name = "Hroggi"
		monarch_name = "Hroggi I"
		dynasty = "Snake-eye"
		birth_date = 1749.1.1
		death_date = 1825.1.1
		claim = 100
		adm = 5
		dip = 0
		mil = 5
    }
}

1825.1.1 = {
	monarch = {
 		name = "Yringor"
		dynasty = "Hjialdr"
		birth_date = 1793.1.1
		adm = 1
		dip = 2
		mil = 0
    }
}

1903.1.1 = {
	monarch = {
 		name = "Rorngrek"
		dynasty = "Brelskald"
		birth_date = 1870.1.1
		adm = 6
		dip = 1
		mil = 4
    }
}

1946.1.1 = {
	monarch = {
 		name = "Jegnorr"
		dynasty = "Skitgerr"
		birth_date = 1911.1.1
		adm = 4
		dip = 0
		mil = 0
    }
}

2001.1.1 = {
	monarch = {
 		name = "Fruth"
		dynasty = "Thudubyldr"
		birth_date = 1969.1.1
		adm = 6
		dip = 1
		mil = 2
    }
}

2093.1.1 = {
	monarch = {
 		name = "Yngold"
		dynasty = "Skoetgogam"
		birth_date = 2042.1.1
		adm = 4
		dip = 0
		mil = 6
    }
}

2142.1.1 = {
	monarch = {
 		name = "Fjurdora"
		dynasty = "Thrialder"
		birth_date = 2094.1.1
		adm = 3
		dip = 0
		mil = 2
		female = yes
    }
}

2219.1.1 = {
	monarch = {
 		name = "Advir"
		dynasty = "Sudinl"
		birth_date = 2195.1.1
		adm = 1
		dip = 6
		mil = 6
    }
	queen = {
 		name = "Hauti"
		dynasty = "Sudinl"
		birth_date = 2191.1.1
		adm = 5
		dip = 4
		mil = 5
		female = yes
    }
	heir = {
 		name = "Vkor"
		monarch_name = "Vkor I"
		dynasty = "Sudinl"
		birth_date = 2214.1.1
		death_date = 2310.1.1
		claim = 100
		adm = 2
		dip = 3
		mil = 4
    }
}

2310.1.1 = {
	monarch = {
 		name = "Idgrod"
		dynasty = "Frem"
		birth_date = 2290.1.1
		adm = 4
		dip = 4
		mil = 6
		female = yes
    }
	queen = {
 		name = "Uche"
		dynasty = "Frem"
		birth_date = 2270.1.1
		adm = 1
		dip = 3
		mil = 5
    }
	heir = {
 		name = "Hulgarth"
		monarch_name = "Hulgarth I"
		dynasty = "Frem"
		birth_date = 2307.1.1
		death_date = 2394.1.1
		claim = 100
		adm = 5
		dip = 1
		mil = 4
    }
}

2394.1.1 = {
	monarch = {
 		name = "Adord"
		dynasty = "Veydef"
		birth_date = 2372.1.1
		adm = 1
		dip = 3
		mil = 6
    }
}

2477.1.1 = {
	monarch = {
 		name = "Sarjoll"
		dynasty = "Hlalmadrun"
		birth_date = 2441.1.1
		adm = 3
		dip = 4
		mil = 1
    }
	queen = {
 		name = "Maeva"
		dynasty = "Hlalmadrun"
		birth_date = 2437.1.1
		adm = 3
		dip = 0
		mil = 2
		female = yes
    }
}

