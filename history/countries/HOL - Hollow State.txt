government = tribal
government_rank = 5
mercantilism = 1
technology_group = elven_tg
religion = bosmer_pantheon
primary_culture = hollow
capital = 5029

54.1.1 = {
	monarch = {
 		name = "Gullohl"
		dynasty = "Kash-Kaalsandra"
		birth_date = 16.1.1
		adm = 5
		dip = 3
		mil = 6
    }
}

122.1.1 = {
	monarch = {
 		name = "Nirrars"
		dynasty = "Kash-Rosepool"
		birth_date = 84.1.1
		adm = 6
		dip = 0
		mil = 4
    }
}

195.1.1 = {
	monarch = {
 		name = "Yobesa"
		dynasty = "Kash-Aerhel"
		birth_date = 142.1.1
		adm = 2
		dip = 2
		mil = 6
		female = yes
    }
}

290.1.1 = {
	monarch = {
 		name = "Kyolel"
		dynasty = "Kash-Nightlock"
		birth_date = 263.1.1
		adm = 5
		dip = 4
		mil = 5
		female = yes
    }
}

380.1.1 = {
	monarch = {
 		name = "Atallong"
		dynasty = "Kash-Riverhollow"
		birth_date = 358.1.1
		adm = 1
		dip = 5
		mil = 1
    }
}

474.1.1 = {
	monarch = {
 		name = "Reweh"
		dynasty = "Kash-Eindaeneth"
		birth_date = 423.1.1
		adm = 1
		dip = 2
		mil = 6
		female = yes
    }
}

534.1.1 = {
	monarch = {
 		name = "Noddurs"
		dynasty = "Kash-Appledale"
		birth_date = 512.1.1
		adm = 0
		dip = 1
		mil = 2
    }
}

607.1.1 = {
	monarch = {
 		name = "Uhitish"
		dynasty = "Kash-Naaenenor"
		birth_date = 554.1.1
		adm = 0
		dip = 5
		mil = 0
    }
}

677.1.1 = {
	monarch = {
 		name = "Sokilar"
		dynasty = "Kash-Filael"
		birth_date = 642.1.1
		adm = 3
		dip = 0
		mil = 3
    }
}

746.1.1 = {
	monarch = {
 		name = "Thinom"
		dynasty = "Kash-Willowmire"
		birth_date = 719.1.1
		adm = 4
		dip = 4
		mil = 1
    }
}

840.1.1 = {
	monarch = {
 		name = "Meki"
		dynasty = "Kash-Aradrwen"
		birth_date = 806.1.1
		adm = 3
		dip = 0
		mil = 1
		female = yes
    }
}

888.1.1 = {
	monarch = {
 		name = "Olezahl"
		dynasty = "Kash-Acorngrass"
		birth_date = 841.1.1
		adm = 0
		dip = 2
		mil = 1
    }
}

982.1.1 = {
	monarch = {
 		name = "Ozerri"
		dynasty = "Kash-Githuin"
		birth_date = 955.1.1
		adm = 6
		dip = 1
		mil = 4
		female = yes
    }
}

1050.1.1 = {
	monarch = {
 		name = "Arleh"
		dynasty = "Kash-Ferngrove"
		birth_date = 1012.1.1
		adm = 1
		dip = 3
		mil = 6
		female = yes
    }
}

1094.1.1 = {
	monarch = {
 		name = "Muryan"
		dynasty = "Kash-Uedir"
		birth_date = 1065.1.1
		adm = 6
		dip = 2
		mil = 3
		female = yes
    }
}

1159.1.1 = {
	monarch = {
 		name = "Djelli"
		dynasty = "Kash-Willowmire"
		birth_date = 1138.1.1
		adm = 4
		dip = 1
		mil = 6
    }
}

1254.1.1 = {
	monarch = {
 		name = "Chalruten"
		dynasty = "Kash-Githuin"
		birth_date = 1230.1.1
		adm = 2
		dip = 0
		mil = 3
    }
}

1339.1.1 = {
	monarch = {
 		name = "Nebi"
		dynasty = "Kash-Celeae"
		birth_date = 1290.1.1
		adm = 3
		dip = 4
		mil = 1
		female = yes
    }
}

1438.1.1 = {
	monarch = {
 		name = "Molang"
		dynasty = "Kash-Willowmire"
		birth_date = 1403.1.1
		adm = 1
		dip = 4
		mil = 5
    }
}

1484.1.1 = {
	monarch = {
 		name = "Kumo"
		dynasty = "		"
		birth_date = 1445.1.1
		adm = 0
		dip = 3
		mil = 1
		female = yes
    }
}

1538.1.1 = {
	monarch = {
 		name = "Gyakkos"
		dynasty = "Kash-Riverhollow"
		birth_date = 1506.1.1
		adm = 5
		dip = 2
		mil = 5
    }
}

1621.1.1 = {
	monarch = {
 		name = "Kunarye"
		dynasty = "Kash-Rosepool"
		birth_date = 1569.1.1
		adm = 3
		dip = 1
		mil = 1
		female = yes
    }
}

1715.1.1 = {
	monarch = {
 		name = "Ushos"
		dynasty = "Kash-Uedir"
		birth_date = 1681.1.1
		adm = 1
		dip = 0
		mil = 5
    }
}

1809.1.1 = {
	monarch = {
 		name = "Hotsahn"
		dynasty = "Kash-Celeae"
		birth_date = 1785.1.1
		adm = 0
		dip = 0
		mil = 2
    }
}

1865.1.1 = {
	monarch = {
 		name = "Yerya"
		dynasty = "Kash-Aradrwen"
		birth_date = 1826.1.1
		adm = 1
		dip = 1
		mil = 3
		female = yes
    }
}

1928.1.1 = {
	monarch = {
 		name = "Kunarye"
		dynasty = "Kash-Camowing"
		birth_date = 1879.1.1
		adm = 0
		dip = 0
		mil = 0
		female = yes
    }
}

2017.1.1 = {
	monarch = {
 		name = "Olezahl"
		dynasty = "Kash-Githuin"
		birth_date = 1996.1.1
		adm = 5
		dip = 6
		mil = 4
    }
}

2057.1.1 = {
	monarch = {
 		name = "Ishehn"
		dynasty = "Kash-Aradrwen"
		birth_date = 2034.1.1
		adm = 3
		dip = 6
		mil = 0
		female = yes
    }
}

2109.1.1 = {
	monarch = {
 		name = "Soyam"
		dynasty = "Kash-Aradrwen"
		birth_date = 2079.1.1
		adm = 4
		dip = 3
		mil = 5
    }
}

2190.1.1 = {
	monarch = {
 		name = "Muryan"
		dynasty = "Kash-Samril"
		birth_date = 2171.1.1
		adm = 2
		dip = 2
		mil = 2
		female = yes
    }
}

2238.1.1 = {
	monarch = {
 		name = "Mohon"
		dynasty = "Kash-Shadybranch"
		birth_date = 2192.1.1
		adm = 0
		dip = 1
		mil = 5
    }
}

2284.1.1 = {
	monarch = {
 		name = "Hirroza"
		dynasty = "Kash-Samril"
		birth_date = 2254.1.1
		adm = 6
		dip = 0
		mil = 2
    }
}

2356.1.1 = {
	monarch = {
 		name = "Arleh"
		dynasty = "Kash-Naaenenor"
		birth_date = 2332.1.1
		adm = 4
		dip = 6
		mil = 6
		female = yes
    }
}

2418.1.1 = {
	monarch = {
 		name = "Muryan"
		dynasty = "Kash-Appledale"
		birth_date = 2398.1.1
		adm = 2
		dip = 6
		mil = 2
		female = yes
    }
}

2488.1.1 = {
	monarch = {
 		name = "Mohon"
		dynasty = "Kash-Kaalsandra"
		birth_date = 2452.1.1
		adm = 0
		dip = 5
		mil = 6
    }
}

