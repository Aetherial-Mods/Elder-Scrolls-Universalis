government = monarchy
government_rank = 1
mercantilism = 1
technology_group = kamal_tg
religion = dremora_pantheon
primary_culture = al_dremoran
capital = 519
# = 517

54.1.1 = {
	monarch = {
 		name = "Urannear"
		dynasty = "Vyl-Gnuallyrath"
		birth_date = 4.1.1
		adm = 6
		dip = 5
		mil = 6
    }
}

139.1.1 = {
	monarch = {
 		name = "Xylney"
		dynasty = "Vyl-Teubal"
		birth_date = 121.1.1
		adm = 4
		dip = 4
		mil = 3
		female = yes
    }
}

192.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Laran"
		monarch_name = "Laran I"
		dynasty = "Vyl-Haecyunes"
		birth_date = 192.1.1
		death_date = 210.1.1
		claim = 100
		adm = 5
		dip = 1
		mil = 1
    }
}

210.1.1 = {
	monarch = {
 		name = "Dyntar"
		dynasty = "Vyl-Plystius"
		birth_date = 190.1.1
		adm = 0
		dip = 3
		mil = 3
    }
}

290.1.1 = {
	monarch = {
 		name = "Najyx"
		dynasty = "Vyl-Oustaal"
		birth_date = 244.1.1
		adm = 6
		dip = 2
		mil = 0
    }
	queen = {
 		name = "Tinaith"
		dynasty = "Vyl-Oustaal"
		birth_date = 245.1.1
		adm = 3
		dip = 0
		mil = 6
		female = yes
    }
}

343.1.1 = {
	monarch = {
 		name = "Unoranin"
		dynasty = "Vyl-Truor'us"
		birth_date = 305.1.1
		adm = 1
		dip = 6
		mil = 2
    }
}

404.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Xinear"
		monarch_name = "Xinear I"
		dynasty = "Vyl-Cupudea"
		birth_date = 402.1.1
		death_date = 420.1.1
		claim = 100
		adm = 2
		dip = 4
		mil = 0
    }
}

420.1.1 = {
	monarch = {
 		name = "Pamnex"
		dynasty = "Vyl-Luna"
		birth_date = 377.1.1
		adm = 4
		dip = 5
		mil = 3
    }
}

458.1.1 = {
	monarch = {
 		name = "Eorynad"
		dynasty = "Vyl-Haecyunes"
		birth_date = 414.1.1
		adm = 6
		dip = 6
		mil = 4
    }
	queen = {
 		name = "Phisthine"
		dynasty = "Vyl-Haecyunes"
		birth_date = 419.1.1
		adm = 6
		dip = 3
		mil = 5
		female = yes
    }
	heir = {
 		name = "Ohirvyn"
		monarch_name = "Ohirvyn I"
		dynasty = "Vyl-Haecyunes"
		birth_date = 456.1.1
		death_date = 501.1.1
		claim = 100
		adm = 3
		dip = 1
		mil = 4
		female = yes
    }
}

501.1.1 = {
	monarch = {
 		name = "Uhryera"
		dynasty = "Vyl-Oustaal"
		birth_date = 461.1.1
		adm = 3
		dip = 5
		mil = 4
		female = yes
    }
	queen = {
 		name = "Fyzelon"
		dynasty = "Vyl-Oustaal"
		birth_date = 451.1.1
		adm = 5
		dip = 6
		mil = 0
    }
	heir = {
 		name = "Saloth"
		monarch_name = "Saloth I"
		dynasty = "Vyl-Oustaal"
		birth_date = 499.1.1
		death_date = 579.1.1
		claim = 100
		adm = 2
		dip = 4
		mil = 6
    }
}

579.1.1 = {
	monarch = {
 		name = "Najix"
		dynasty = "Vyl-Wouus"
		birth_date = 558.1.1
		adm = 2
		dip = 1
		mil = 6
    }
	queen = {
 		name = "Cahrvyra"
		dynasty = "Vyl-Wouus"
		birth_date = 528.1.1
		adm = 6
		dip = 6
		mil = 5
		female = yes
    }
	heir = {
 		name = "Braxlyx"
		monarch_name = "Braxlyx I"
		dynasty = "Vyl-Wouus"
		birth_date = 566.1.1
		death_date = 648.1.1
		claim = 100
		adm = 4
		dip = 6
		mil = 2
    }
}

648.1.1 = {
	monarch = {
 		name = "Dyntiran"
		dynasty = "Vyl-Kmeiine"
		birth_date = 609.1.1
		adm = 0
		dip = 0
		mil = 4
    }
	queen = {
 		name = "Grynera"
		dynasty = "Vyl-Kmeiine"
		birth_date = 629.1.1
		adm = 3
		dip = 5
		mil = 3
		female = yes
    }
	heir = {
 		name = "Xalsahr"
		monarch_name = "Xalsahr I"
		dynasty = "Vyl-Kmeiine"
		birth_date = 646.1.1
		death_date = 729.1.1
		claim = 100
		adm = 0
		dip = 4
		mil = 2
    }
}

729.1.1 = {
	monarch = {
 		name = "Azerel"
		dynasty = "Vyl-Austere"
		birth_date = 698.1.1
		adm = 3
		dip = 5
		mil = 4
    }
}

824.1.1 = {
	monarch = {
 		name = "Crynlynx"
		dynasty = "Vyl-Ruyura"
		birth_date = 789.1.1
		adm = 4
		dip = 2
		mil = 2
    }
	queen = {
 		name = "Cattia"
		dynasty = "Vyl-Ruyura"
		birth_date = 787.1.1
		adm = 1
		dip = 1
		mil = 1
		female = yes
    }
}

902.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Borenorin"
		monarch_name = "Borenorin I"
		dynasty = "Vyl-Knakroura"
		birth_date = 900.1.1
		death_date = 918.1.1
		claim = 100
		adm = 1
		dip = 5
		mil = 6
    }
}

918.1.1 = {
	monarch = {
 		name = "Braxlax"
		dynasty = "Vyl-Calamity"
		birth_date = 874.1.1
		adm = 4
		dip = 6
		mil = 2
    }
}

969.1.1 = {
	monarch = {
 		name = "Berinona"
		dynasty = "Vyl-Cluepeal"
		birth_date = 948.1.1
		adm = 2
		dip = 6
		mil = 5
		female = yes
    }
}

1033.1.1 = {
	monarch = {
 		name = "Xintrax"
		dynasty = "Vyl-Pliglyura"
		birth_date = 1002.1.1
		adm = 4
		dip = 0
		mil = 0
    }
	queen = {
 		name = "Jaenesh"
		dynasty = "Vyl-Pliglyura"
		birth_date = 992.1.1
		adm = 5
		dip = 3
		mil = 1
		female = yes
    }
}

1107.1.1 = {
	monarch = {
 		name = "Qrinnar"
		dynasty = "Vyl-Cupudea"
		birth_date = 1088.1.1
		adm = 6
		dip = 5
		mil = 2
    }
}

1153.1.1 = {
	monarch = {
 		name = "Nyrtar"
		dynasty = "Vyl-Qleahmymina"
		birth_date = 1119.1.1
		adm = 4
		dip = 0
		mil = 2
    }
}

1196.1.1 = {
	monarch = {
 		name = "Ohmvyce"
		dynasty = "Vyl-Pliglyura"
		birth_date = 1157.1.1
		adm = 2
		dip = 6
		mil = 6
    }
	queen = {
 		name = "Zrixira"
		dynasty = "Vyl-Pliglyura"
		birth_date = 1160.1.1
		adm = 6
		dip = 4
		mil = 5
		female = yes
    }
	heir = {
 		name = "Najix"
		monarch_name = "Najix I"
		dynasty = "Vyl-Pliglyura"
		birth_date = 1191.1.1
		death_date = 1250.1.1
		claim = 100
		adm = 3
		dip = 3
		mil = 4
    }
}

1250.1.1 = {
	monarch = {
 		name = "Crynlynx"
		dynasty = "Vyl-Flycymina"
		birth_date = 1199.1.1
		adm = 2
		dip = 6
		mil = 4
    }
}

1307.1.1 = {
	monarch = {
 		name = "Wraxox"
		dynasty = "Vyl-Kmeiine"
		birth_date = 1257.1.1
		adm = 0
		dip = 5
		mil = 1
    }
	queen = {
 		name = "Dhyseli"
		dynasty = "Vyl-Kmeiine"
		birth_date = 1269.1.1
		adm = 4
		dip = 4
		mil = 0
		female = yes
    }
	heir = {
 		name = "Carilyss"
		monarch_name = "Carilyss I"
		dynasty = "Vyl-Kmeiine"
		birth_date = 1300.1.1
		death_date = 1373.1.1
		claim = 100
		adm = 1
		dip = 3
		mil = 6
		female = yes
    }
}

1373.1.1 = {
	monarch = {
 		name = "Tryxlith"
		dynasty = "Vyl-Knabrivus"
		birth_date = 1327.1.1
		adm = 4
		dip = 4
		mil = 1
		female = yes
    }
}

1449.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Phistia"
		monarch_name = "Phistia I"
		dynasty = "Vyl-Krisebal"
		birth_date = 1440.1.1
		death_date = 1458.1.1
		claim = 100
		adm = 4
		dip = 1
		mil = 6
		female = yes
    }
}

1458.1.1 = {
	monarch = {
 		name = "Cahrielle"
		dynasty = "Vyl-Knetsiag"
		birth_date = 1412.1.1
		adm = 0
		dip = 2
		mil = 1
		female = yes
    }
	queen = {
 		name = "Dyntiran"
		dynasty = "Vyl-Knetsiag"
		birth_date = 1426.1.1
		adm = 4
		dip = 1
		mil = 0
    }
}

1520.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Tryxlith"
		monarch_name = "Tryxlith I"
		dynasty = "Vyl-Gretaura"
		birth_date = 1505.1.1
		death_date = 1523.1.1
		claim = 100
		adm = 5
		dip = 5
		mil = 5
		female = yes
    }
}

1523.1.1 = {
	monarch = {
 		name = "Harelvyra"
		dynasty = "Vyl-Duiala"
		birth_date = 1491.1.1
		adm = 0
		dip = 6
		mil = 1
		female = yes
    }
	queen = {
 		name = "Fyzied"
		dynasty = "Vyl-Duiala"
		birth_date = 1479.1.1
		adm = 4
		dip = 5
		mil = 0
    }
}

1565.1.1 = {
	monarch = {
 		name = "Garanlan"
		dynasty = "Vyl-Austere"
		birth_date = 1532.1.1
		adm = 3
		dip = 4
		mil = 3
    }
}

1645.1.1 = {
	monarch = {
 		name = "Iphisysha"
		dynasty = "Vyl-Haecyunes"
		birth_date = 1605.1.1
		adm = 5
		dip = 5
		mil = 5
		female = yes
    }
}

1731.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Braxlax"
		monarch_name = "Braxlax I"
		dynasty = "Vyl-Haecyunes"
		birth_date = 1726.1.1
		death_date = 1744.1.1
		claim = 100
		adm = 2
		dip = 0
		mil = 5
    }
}

1744.1.1 = {
	monarch = {
 		name = "Qyriana"
		dynasty = "Vyl-Eiv'aicus"
		birth_date = 1723.1.1
		adm = 0
		dip = 6
		mil = 2
		female = yes
    }
}

1782.1.1 = {
	monarch = {
 		name = "Bwynvyra"
		dynasty = "Vyl-Plaequnes"
		birth_date = 1743.1.1
		adm = 2
		dip = 1
		mil = 3
		female = yes
    }
}

1826.1.1 = {
	monarch = {
 		name = "Jaroriad"
		dynasty = "Vyl-Sturaoth"
		birth_date = 1777.1.1
		adm = 0
		dip = 0
		mil = 0
    }
}

1896.1.1 = {
	monarch = {
 		name = "Lilesh"
		dynasty = "Vyl-Duiala"
		birth_date = 1860.1.1
		adm = 5
		dip = 6
		mil = 3
		female = yes
    }
}

1977.1.1 = {
	monarch = {
 		name = "Lanarrenar"
		dynasty = "Vyl-Duiala"
		birth_date = 1949.1.1
		adm = 3
		dip = 5
		mil = 0
    }
}

2067.1.1 = {
	monarch = {
 		name = "Tryxana"
		dynasty = "Vyl-Ieflaina"
		birth_date = 2041.1.1
		adm = 2
		dip = 5
		mil = 4
		female = yes
    }
}

2113.1.1 = {
	monarch = {
 		name = "Larsahr"
		dynasty = "Vyl-Elmemaeus"
		birth_date = 2060.1.1
		adm = 2
		dip = 2
		mil = 2
    }
	queen = {
 		name = "Phisvienne"
		dynasty = "Vyl-Elmemaeus"
		birth_date = 2087.1.1
		adm = 6
		dip = 0
		mil = 1
		female = yes
    }
	heir = {
 		name = "Zarinoris"
		monarch_name = "Zarinoris I"
		dynasty = "Vyl-Elmemaeus"
		birth_date = 2103.1.1
		death_date = 2160.1.1
		claim = 100
		adm = 3
		dip = 6
		mil = 0
		female = yes
    }
}

2160.1.1 = {
	monarch = {
 		name = "Glysvyra"
		dynasty = "Vyl-Sturaoth"
		birth_date = 2142.1.1
		adm = 6
		dip = 0
		mil = 2
		female = yes
    }
	queen = {
 		name = "Ohmvyce"
		dynasty = "Vyl-Sturaoth"
		birth_date = 2142.1.1
		adm = 3
		dip = 6
		mil = 1
    }
}

2248.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Irinynore"
		monarch_name = "Irinynore I"
		dynasty = "Vyl-Kamori"
		birth_date = 2234.1.1
		death_date = 2252.1.1
		claim = 100
		adm = 2
		dip = 5
		mil = 2
		female = yes
    }
}

2252.1.1 = {
	monarch = {
 		name = "Colorad"
		dynasty = "Vyl-Qlucuina"
		birth_date = 2205.1.1
		adm = 2
		dip = 2
		mil = 2
    }
}

2347.1.1 = {
	monarch = {
 		name = "Dharyx"
		dynasty = "Vyl-Elmemaeus"
		birth_date = 2304.1.1
		adm = 0
		dip = 1
		mil = 6
    }
	queen = {
 		name = "Daemrelle"
		dynasty = "Vyl-Elmemaeus"
		birth_date = 2302.1.1
		adm = 4
		dip = 0
		mil = 5
		female = yes
    }
	heir = {
 		name = "Gnarvile"
		monarch_name = "Gnarvile I"
		dynasty = "Vyl-Elmemaeus"
		birth_date = 2340.1.1
		death_date = 2428.1.1
		claim = 100
		adm = 1
		dip = 5
		mil = 4
    }
}

2428.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Jhorvix"
		monarch_name = "Jhorvix I"
		dynasty = "Vyl-Krisebal"
		birth_date = 2422.1.1
		death_date = 2440.1.1
		claim = 100
		adm = 6
		dip = 4
		mil = 1
    }
}

2440.1.1 = {
	monarch = {
 		name = "Krynyra"
		dynasty = "Vyl-Duiala"
		birth_date = 2418.1.1
		adm = 2
		dip = 6
		mil = 3
		female = yes
    }
	queen = {
 		name = "Qrinnar"
		dynasty = "Vyl-Duiala"
		birth_date = 2388.1.1
		adm = 1
		dip = 2
		mil = 3
    }
}

2499.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Iphisysha"
		monarch_name = "Iphisysha I"
		dynasty = "Vyl-Oustaal"
		birth_date = 2492.1.1
		death_date = 2510.1.1
		claim = 100
		adm = 2
		dip = 6
		mil = 1
		female = yes
    }
}

