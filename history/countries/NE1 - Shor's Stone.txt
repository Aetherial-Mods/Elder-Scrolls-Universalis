government = monarchy
government_rank = 1
mercantilism = 1
technology_group = northern_tg
religion = nordic_pantheon
primary_culture = nord
capital = 1271

54.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Denskar"
		monarch_name = "Denskar I"
		dynasty = "Frem"
		birth_date = 45.1.1
		death_date = 63.1.1
		claim = 100
		adm = 0
		dip = 4
		mil = 1
    }
}

63.1.1 = {
	monarch = {
 		name = "Rigvar"
		dynasty = "Hjir"
		birth_date = 18.1.1
		adm = 6
		dip = 1
		mil = 1
    }
	queen = {
 		name = "Bodil"
		dynasty = "Hjir"
		birth_date = 19.1.1
		adm = 3
		dip = 6
		mil = 0
		female = yes
    }
}

121.1.1 = {
	monarch = {
 		name = "Alforunn"
		dynasty = "Gol"
		birth_date = 99.1.1
		adm = 1
		dip = 5
		mil = 4
    }
	queen = {
 		name = "Helgreir"
		dynasty = "Gol"
		birth_date = 96.1.1
		adm = 5
		dip = 4
		mil = 3
		female = yes
    }
	heir = {
 		name = "Vigdis"
		monarch_name = "Vigdis I"
		dynasty = "Gol"
		birth_date = 116.1.1
		death_date = 172.1.1
		claim = 100
		adm = 2
		dip = 2
		mil = 2
		female = yes
    }
}

172.1.1 = {
	monarch = {
 		name = "Kaleb"
		dynasty = "Hagrudr"
		birth_date = 143.1.1
		adm = 5
		dip = 4
		mil = 4
    }
	queen = {
 		name = "Tidave"
		dynasty = "Hagrudr"
		birth_date = 129.1.1
		adm = 1
		dip = 2
		mil = 3
		female = yes
    }
	heir = {
 		name = "Holdilf"
		monarch_name = "Holdilf I"
		dynasty = "Hagrudr"
		birth_date = 162.1.1
		death_date = 260.1.1
		claim = 100
		adm = 5
		dip = 1
		mil = 2
		female = yes
    }
}

260.1.1 = {
	monarch = {
 		name = "Aldrig"
		dynasty = "Thrivedraldr"
		birth_date = 235.1.1
		adm = 1
		dip = 2
		mil = 4
    }
}

320.1.1 = {
	monarch = {
 		name = "Sinos"
		dynasty = "Thovohadr"
		birth_date = 275.1.1
		adm = 3
		dip = 3
		mil = 6
    }
	queen = {
 		name = "Maven"
		dynasty = "Thovohadr"
		birth_date = 277.1.1
		adm = 3
		dip = 0
		mil = 0
		female = yes
    }
	heir = {
 		name = "Fruth"
		monarch_name = "Fruth I"
		dynasty = "Thovohadr"
		birth_date = 311.1.1
		death_date = 409.1.1
		claim = 100
		adm = 0
		dip = 5
		mil = 6
    }
}

409.1.1 = {
	monarch = {
 		name = "Rigel"
		dynasty = "Frechom"
		birth_date = 375.1.1
		adm = 6
		dip = 2
		mil = 6
		female = yes
    }
}

501.1.1 = {
	monarch = {
 		name = "Knarstygg"
		dynasty = "Rotmalleskr"
		birth_date = 458.1.1
		adm = 5
		dip = 1
		mil = 3
    }
	queen = {
 		name = "Tolgredda"
		dynasty = "Rotmalleskr"
		birth_date = 449.1.1
		adm = 2
		dip = 0
		mil = 2
		female = yes
    }
	heir = {
 		name = "Hroa"
		monarch_name = "Hroa I"
		dynasty = "Rotmalleskr"
		birth_date = 490.1.1
		death_date = 554.1.1
		claim = 100
		adm = 5
		dip = 5
		mil = 1
		female = yes
    }
}

554.1.1 = {
	monarch = {
 		name = "Angbjar"
		dynasty = "Jetmenl"
		birth_date = 530.1.1
		adm = 1
		dip = 6
		mil = 3
    }
}

595.1.1 = {
	monarch = {
 		name = "Resrytte"
		dynasty = "Bralg"
		birth_date = 557.1.1
		adm = 6
		dip = 6
		mil = 0
		female = yes
    }
	queen = {
 		name = "Povaren"
		dynasty = "Bralg"
		birth_date = 559.1.1
		adm = 3
		dip = 4
		mil = 6
    }
	heir = {
 		name = "Oda"
		monarch_name = "Oda I"
		dynasty = "Bralg"
		birth_date = 581.1.1
		death_date = 690.1.1
		claim = 100
		adm = 0
		dip = 3
		mil = 5
		female = yes
    }
}

690.1.1 = {
	monarch = {
 		name = "Goren"
		dynasty = "Storgath"
		birth_date = 671.1.1
		adm = 0
		dip = 6
		mil = 5
    }
}

736.1.1 = {
	monarch = {
 		name = "Andrei"
		dynasty = "Brodrunlyn"
		birth_date = 712.1.1
		adm = 5
		dip = 5
		mil = 2
    }
}

805.1.1 = {
	monarch = {
 		name = "Gundlar"
		dynasty = "Veladrys"
		birth_date = 753.1.1
		adm = 3
		dip = 5
		mil = 5
    }
	queen = {
 		name = "Merta"
		dynasty = "Veladrys"
		birth_date = 774.1.1
		adm = 0
		dip = 3
		mil = 4
		female = yes
    }
	heir = {
 		name = "Fjori"
		monarch_name = "Fjori I"
		dynasty = "Veladrys"
		birth_date = 790.1.1
		death_date = 879.1.1
		claim = 100
		adm = 4
		dip = 2
		mil = 3
		female = yes
    }
}

879.1.1 = {
	monarch = {
 		name = "Sond"
		dynasty = "Frowunnirr"
		birth_date = 852.1.1
		adm = 0
		dip = 3
		mil = 6
    }
}

916.1.1 = {
	monarch = {
 		name = "Kralald"
		dynasty = "Hrund"
		birth_date = 863.1.1
		adm = 5
		dip = 2
		mil = 2
    }
	queen = {
 		name = "Tyrnea"
		dynasty = "Hrund"
		birth_date = 887.1.1
		adm = 2
		dip = 1
		mil = 1
		female = yes
    }
	heir = {
 		name = "Jokorn"
		monarch_name = "Jokorn I"
		dynasty = "Hrund"
		birth_date = 904.1.1
		death_date = 995.1.1
		claim = 100
		adm = 6
		dip = 6
		mil = 0
    }
}

995.1.1 = {
	monarch = {
 		name = "Ashvorn"
		dynasty = "Hlolmof"
		birth_date = 967.1.1
		adm = 1
		dip = 1
		mil = 3
    }
	queen = {
 		name = "Veranir"
		dynasty = "Hlolmof"
		birth_date = 965.1.1
		adm = 5
		dip = 6
		mil = 2
		female = yes
    }
	heir = {
 		name = "Kadord"
		monarch_name = "Kadord I"
		dynasty = "Hlolmof"
		birth_date = 982.1.1
		death_date = 1044.1.1
		claim = 100
		adm = 2
		dip = 5
		mil = 0
    }
}

1044.1.1 = {
	monarch = {
 		name = "Jafrera"
		dynasty = "Hrialmuhon"
		birth_date = 999.1.1
		adm = 1
		dip = 1
		mil = 1
		female = yes
    }
}

1142.1.1 = {
	monarch = {
 		name = "Sulrengar"
		dynasty = "Fruveld"
		birth_date = 1098.1.1
		adm = 0
		dip = 0
		mil = 4
    }
	queen = {
 		name = "Enlaf"
		dynasty = "Fruveld"
		birth_date = 1099.1.1
		adm = 4
		dip = 6
		mil = 3
		female = yes
    }
	heir = {
 		name = "Sigyrr"
		monarch_name = "Sigyrr I"
		dynasty = "Fruveld"
		birth_date = 1139.1.1
		death_date = 1183.1.1
		claim = 100
		adm = 0
		dip = 4
		mil = 2
    }
}

1183.1.1 = {
	monarch = {
 		name = "Haening"
		dynasty = "Kleelim"
		birth_date = 1135.1.1
		adm = 3
		dip = 6
		mil = 5
    }
	queen = {
 		name = "Narri"
		dynasty = "Kleelim"
		birth_date = 1141.1.1
		adm = 0
		dip = 4
		mil = 4
		female = yes
    }
}

1243.1.1 = {
	monarch = {
 		name = "Maulkyr"
		dynasty = "Dormam"
		birth_date = 1213.1.1
		adm = 5
		dip = 3
		mil = 0
    }
	queen = {
 		name = "Agila"
		dynasty = "Dormam"
		birth_date = 1192.1.1
		adm = 2
		dip = 2
		mil = 6
		female = yes
    }
}

1327.1.1 = {
	monarch = {
 		name = "Vanskyr"
		dynasty = "Bor"
		birth_date = 1301.1.1
		adm = 0
		dip = 1
		mil = 3
    }
}

1374.1.1 = {
	monarch = {
 		name = "Ollfar"
		dynasty = "Rondirek"
		birth_date = 1331.1.1
		adm = 6
		dip = 0
		mil = 0
    }
}

1472.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Mathon"
		monarch_name = "Mathon I"
		dynasty = "Vudmuldr"
		birth_date = 1472.1.1
		death_date = 1490.1.1
		claim = 100
		adm = 6
		dip = 4
		mil = 4
    }
}

1490.1.1 = {
	monarch = {
 		name = "Drengr"
		dynasty = "Stindurilg"
		birth_date = 1448.1.1
		adm = 2
		dip = 6
		mil = 0
    }
	queen = {
 		name = "Beitild"
		dynasty = "Stindurilg"
		birth_date = 1471.1.1
		adm = 6
		dip = 4
		mil = 6
		female = yes
    }
}

1555.1.1 = {
	monarch = {
 		name = "Jagyr"
		dynasty = "Sogjudrildr"
		birth_date = 1531.1.1
		adm = 4
		dip = 3
		mil = 2
    }
}

1598.1.1 = {
	monarch = {
 		name = "Forbrand"
		dynasty = "Valben"
		birth_date = 1563.1.1
		adm = 6
		dip = 5
		mil = 4
    }
	queen = {
 		name = "Brimfja"
		dynasty = "Valben"
		birth_date = 1580.1.1
		adm = 6
		dip = 1
		mil = 5
		female = yes
    }
	heir = {
 		name = "Otrovor"
		monarch_name = "Otrovor I"
		dynasty = "Valben"
		birth_date = 1587.1.1
		death_date = 1695.1.1
		claim = 100
		adm = 3
		dip = 0
		mil = 4
    }
}

1695.1.1 = {
	monarch = {
 		name = "Ganeryk"
		dynasty = "Radr"
		birth_date = 1668.1.1
		adm = 3
		dip = 3
		mil = 4
    }
	queen = {
 		name = "Linlia"
		dynasty = "Radr"
		birth_date = 1673.1.1
		adm = 0
		dip = 2
		mil = 3
		female = yes
    }
	heir = {
 		name = "Embry"
		monarch_name = "Embry I"
		dynasty = "Radr"
		birth_date = 1683.1.1
		death_date = 1741.1.1
		claim = 100
		adm = 3
		dip = 0
		mil = 2
    }
}

1741.1.1 = {
	monarch = {
 		name = "Saborm"
		dynasty = "Hrialmuhon"
		birth_date = 1720.1.1
		adm = 6
		dip = 2
		mil = 5
    }
	queen = {
 		name = "Breya"
		dynasty = "Hrialmuhon"
		birth_date = 1716.1.1
		adm = 3
		dip = 0
		mil = 4
		female = yes
    }
	heir = {
 		name = "Oslaf"
		monarch_name = "Oslaf I"
		dynasty = "Hrialmuhon"
		birth_date = 1728.1.1
		death_date = 1821.1.1
		claim = 100
		adm = 0
		dip = 6
		mil = 3
    }
}

1821.1.1 = {
	monarch = {
 		name = "Gamirth"
		dynasty = "Gendodr"
		birth_date = 1793.1.1
		adm = 3
		dip = 0
		mil = 5
    }
	queen = {
 		name = "Chalwyn"
		dynasty = "Gendodr"
		birth_date = 1803.1.1
		adm = 6
		dip = 6
		mil = 4
		female = yes
    }
	heir = {
 		name = "Eiraki"
		monarch_name = "Eiraki I"
		dynasty = "Gendodr"
		birth_date = 1816.1.1
		death_date = 1912.1.1
		claim = 100
		adm = 3
		dip = 4
		mil = 3
		female = yes
    }
}

1912.1.1 = {
	monarch = {
 		name = "Olava"
		dynasty = "Brundeskr"
		birth_date = 1878.1.1
		adm = 3
		dip = 0
		mil = 3
		female = yes
    }
}

1998.1.1 = {
	monarch = {
 		name = "Jesper"
		dynasty = "Ferleld"
		birth_date = 1953.1.1
		adm = 1
		dip = 0
		mil = 0
    }
}

2061.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Valgritta"
		monarch_name = "Valgritta I"
		dynasty = "Skoetgogam"
		birth_date = 2056.1.1
		death_date = 2074.1.1
		claim = 100
		adm = 5
		dip = 2
		mil = 0
		female = yes
    }
}

2074.1.1 = {
	monarch = {
 		name = "Jorleif"
		dynasty = "Threidiodr"
		birth_date = 2047.1.1
		adm = 4
		dip = 5
		mil = 0
    }
}

2134.1.1 = {
	monarch = {
 		name = "Ger"
		dynasty = "Hjir"
		birth_date = 2093.1.1
		adm = 3
		dip = 4
		mil = 4
    }
}

2174.1.1 = {
	monarch = {
 		name = "Afnhi"
		dynasty = "Skylskak"
		birth_date = 2150.1.1
		adm = 1
		dip = 3
		mil = 0
		female = yes
    }
}

2257.1.1 = {
	monarch = {
 		name = "Orolo"
		dynasty = "Gugnorr"
		birth_date = 2226.1.1
		adm = 6
		dip = 3
		mil = 4
		female = yes
    }
	queen = {
 		name = "Olfand"
		dynasty = "Gugnorr"
		birth_date = 2221.1.1
		adm = 3
		dip = 1
		mil = 3
    }
	heir = {
 		name = "Thromgar"
		monarch_name = "Thromgar I"
		dynasty = "Gugnorr"
		birth_date = 2256.1.1
		death_date = 2310.1.1
		claim = 100
		adm = 0
		dip = 0
		mil = 2
    }
}

2310.1.1 = {
	monarch = {
 		name = "Geirlund"
		dynasty = "Surjafald"
		birth_date = 2285.1.1
		adm = 4
		dip = 5
		mil = 1
    }
}

2348.1.1 = {
	monarch = {
 		name = "Drahff"
		dynasty = "Gilsganl"
		birth_date = 2328.1.1
		adm = 2
		dip = 4
		mil = 5
    }
}

2388.1.1 = {
	monarch = {
 		name = "Tilma"
		dynasty = "Finremth"
		birth_date = 2340.1.1
		adm = 0
		dip = 4
		mil = 1
		female = yes
    }
}

2487.1.1 = {
	monarch = {
 		name = "Ogondar"
		dynasty = "Segald"
		birth_date = 2444.1.1
		adm = 6
		dip = 3
		mil = 5
    }
	queen = {
 		name = "Julni"
		dynasty = "Segald"
		birth_date = 2440.1.1
		adm = 2
		dip = 1
		mil = 4
		female = yes
    }
	heir = {
 		name = "Manwe"
		monarch_name = "Manwe I"
		dynasty = "Segald"
		birth_date = 2477.1.1
		death_date = 2562.1.1
		claim = 100
		adm = 6
		dip = 0
		mil = 3
    }
}

