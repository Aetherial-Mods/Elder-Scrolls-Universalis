government = monarchy
government_rank = 10
mercantilism = 1
technology_group = akavir_tg
religion = dragon_cult
primary_culture = akaviri
capital = 724

54.1.1 = {
	monarch = {
 		name = "Thimithous"
		dynasty = "Di-Antipherios"
		birth_date = 32.1.1
		adm = 5
		dip = 4
		mil = 1
    }
	queen = {
 		name = "Galixaia"
		dynasty = "Di-Antipherios"
		birth_date = 27.1.1
		adm = 2
		dip = 3
		mil = 0
		female = yes
    }
}

98.1.1 = {
	monarch = {
 		name = "Astrisei"
		dynasty = "Vu-Nyshassa"
		birth_date = 49.1.1
		adm = 0
		dip = 2
		mil = 3
		female = yes
    }
}

144.1.1 = {
	monarch = {
 		name = "Soterixus"
		dynasty = "Vi-Oriphephia"
		birth_date = 119.1.1
		adm = 5
		dip = 1
		mil = 0
    }
	queen = {
 		name = "Acaliphelia"
		dynasty = "Vi-Oriphephia"
		birth_date = 92.1.1
		adm = 6
		dip = 2
		mil = 5
		female = yes
    }
}

208.1.1 = {
	monarch = {
 		name = "Isocrerios"
		dynasty = "Du-Eustolos"
		birth_date = 176.1.1
		adm = 0
		dip = 6
		mil = 3
    }
	queen = {
 		name = "Nephiphi"
		dynasty = "Du-Eustolos"
		birth_date = 185.1.1
		adm = 2
		dip = 6
		mil = 0
		female = yes
    }
	heir = {
 		name = "Salmoebus"
		monarch_name = "Salmoebus I"
		dynasty = "Du-Eustolos"
		birth_date = 202.1.1
		death_date = 248.1.1
		claim = 100
		adm = 5
		dip = 5
		mil = 6
    }
}

248.1.1 = {
	monarch = {
 		name = "Dionysanos"
		dynasty = "Di-Ansteas"
		birth_date = 206.1.1
		adm = 5
		dip = 1
		mil = 6
    }
	queen = {
 		name = "Rhaeneis"
		dynasty = "Di-Ansteas"
		birth_date = 218.1.1
		adm = 1
		dip = 3
		mil = 2
		female = yes
    }
}

300.1.1 = {
	monarch = {
 		name = "Anthia"
		dynasty = "Vu-Rhaeneis"
		birth_date = 255.1.1
		adm = 6
		dip = 2
		mil = 5
		female = yes
    }
	queen = {
 		name = "Zalisthus"
		dynasty = "Vu-Rhaeneis"
		birth_date = 270.1.1
		adm = 3
		dip = 0
		mil = 4
    }
	heir = {
 		name = "Orthrosyne"
		monarch_name = "Orthrosyne I"
		dynasty = "Vu-Rhaeneis"
		birth_date = 296.1.1
		death_date = 372.1.1
		claim = 100
		adm = 0
		dip = 6
		mil = 3
    }
}

372.1.1 = {
	monarch = {
 		name = "Typhaus"
		dynasty = "Vi-Prisenis"
		birth_date = 347.1.1
		adm = 6
		dip = 2
		mil = 4
    }
	queen = {
 		name = "Kisseosis"
		dynasty = "Vi-Prisenis"
		birth_date = 331.1.1
		adm = 6
		dip = 6
		mil = 5
		female = yes
    }
	heir = {
 		name = "Misypso"
		monarch_name = "Misypso I"
		dynasty = "Vi-Prisenis"
		birth_date = 369.1.1
		death_date = 420.1.1
		claim = 100
		adm = 4
		dip = 2
		mil = 0
		female = yes
    }
}

420.1.1 = {
	monarch = {
 		name = "Jaselphos"
		dynasty = "Vu-Orethaeia"
		birth_date = 377.1.1
		adm = 2
		dip = 1
		mil = 4
    }
	queen = {
 		name = "Akisisha"
		dynasty = "Vu-Orethaeia"
		birth_date = 387.1.1
		adm = 6
		dip = 6
		mil = 3
		female = yes
    }
}

461.1.1 = {
	monarch = {
 		name = "Sabixa"
		dynasty = "Vi-Kisseosis"
		birth_date = 433.1.1
		adm = 2
		dip = 1
		mil = 5
		female = yes
    }
}

559.1.1 = {
	monarch = {
 		name = "Galixeilla"
		dynasty = "Vu-Galixaia"
		birth_date = 540.1.1
		adm = 0
		dip = 0
		mil = 2
		female = yes
    }
	queen = {
 		name = "Niseseus"
		dynasty = "Vu-Galixaia"
		birth_date = 521.1.1
		adm = 4
		dip = 5
		mil = 1
    }
	heir = {
 		name = "Jaselphos"
		monarch_name = "Jaselphos I"
		dynasty = "Vu-Galixaia"
		birth_date = 546.1.1
		death_date = 651.1.1
		claim = 100
		adm = 4
		dip = 2
		mil = 1
    }
}

651.1.1 = {
	monarch = {
 		name = "Thaleros"
		dynasty = "Di-Brasas"
		birth_date = 625.1.1
		adm = 0
		dip = 0
		mil = 0
    }
}

687.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Parthaus"
		monarch_name = "Parthaus I"
		dynasty = "Vi-Oriphosa"
		birth_date = 684.1.1
		death_date = 702.1.1
		claim = 100
		adm = 0
		dip = 0
		mil = 1
    }
}

702.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Lichophoros"
		monarch_name = "Lichophoros I"
		dynasty = "Du-Hespocia"
		birth_date = 696.1.1
		death_date = 714.1.1
		claim = 100
		adm = 3
		dip = 2
		mil = 4
    }
}

714.1.1 = {
	monarch = {
 		name = "Anasztochus"
		dynasty = "Di-Demithaeus"
		birth_date = 696.1.1
		adm = 5
		dip = 3
		mil = 5
    }
}

800.1.1 = {
	monarch = {
 		name = "Vasios"
		dynasty = "Di-Stephophor"
		birth_date = 747.1.1
		adm = 0
		dip = 4
		mil = 1
    }
}

884.1.1 = {
	monarch = {
 		name = "Prophios"
		dynasty = "Du-Laestithous"
		birth_date = 863.1.1
		adm = 1
		dip = 1
		mil = 6
    }
}

960.1.1 = {
	monarch = {
 		name = "Theamephia"
		dynasty = "Vi-Prisenis"
		birth_date = 910.1.1
		adm = 4
		dip = 3
		mil = 1
		female = yes
    }
}

1055.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Amycades"
		monarch_name = "Amycades I"
		dynasty = "Vu-Theanaeia"
		birth_date = 1043.1.1
		death_date = 1061.1.1
		claim = 100
		adm = 5
		dip = 0
		mil = 6
    }
}

1061.1.1 = {
	monarch = {
 		name = "Galixaia"
		dynasty = "Vi-Thesassea"
		birth_date = 1032.1.1
		adm = 4
		dip = 3
		mil = 6
		female = yes
    }
}

1102.1.1 = {
	monarch = {
 		name = "Prophios"
		dynasty = "Di-Aphrixi"
		birth_date = 1072.1.1
		adm = 2
		dip = 2
		mil = 3
    }
}

1186.1.1 = {
	monarch = {
 		name = "Sotalus"
		dynasty = "Du-Heryrtus"
		birth_date = 1138.1.1
		adm = 1
		dip = 1
		mil = 6
    }
	queen = {
 		name = "Nethoesa"
		dynasty = "Du-Heryrtus"
		birth_date = 1163.1.1
		adm = 4
		dip = 0
		mil = 5
		female = yes
    }
	heir = {
 		name = "Basos"
		monarch_name = "Basos I"
		dynasty = "Du-Heryrtus"
		birth_date = 1185.1.1
		death_date = 1248.1.1
		claim = 100
		adm = 0
		dip = 1
		mil = 1
    }
}

1248.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Nyxophi"
		monarch_name = "Nyxophi I"
		dynasty = "Di-Salmysios"
		birth_date = 1243.1.1
		death_date = 1261.1.1
		claim = 100
		adm = 5
		dip = 1
		mil = 4
		female = yes
    }
}

1261.1.1 = {
	monarch = {
 		name = "Othrithoe"
		dynasty = "Vu-Iasephia"
		birth_date = 1239.1.1
		adm = 2
		dip = 6
		mil = 3
		female = yes
    }
	queen = {
 		name = "Nisolas"
		dynasty = "Vu-Iasephia"
		birth_date = 1243.1.1
		adm = 6
		dip = 5
		mil = 2
    }
	heir = {
 		name = "Krathisthus"
		monarch_name = "Krathisthus I"
		dynasty = "Vu-Iasephia"
		birth_date = 1253.1.1
		death_date = 1332.1.1
		claim = 100
		adm = 3
		dip = 3
		mil = 1
    }
}

1332.1.1 = {
	monarch = {
 		name = "Ophiali"
		dynasty = "Du-Orthralus"
		birth_date = 1312.1.1
		adm = 6
		dip = 4
		mil = 4
		female = yes
    }
}

1407.1.1 = {
	monarch = {
 		name = "Acsais"
		dynasty = "Di-Typhaus"
		birth_date = 1362.1.1
		adm = 1
		dip = 6
		mil = 5
		female = yes
    }
	queen = {
 		name = "Prophilles"
		dynasty = "Di-Typhaus"
		birth_date = 1374.1.1
		adm = 1
		dip = 2
		mil = 6
    }
	heir = {
 		name = "Caesanthus"
		monarch_name = "Caesanthus I"
		dynasty = "Di-Typhaus"
		birth_date = 1403.1.1
		death_date = 1495.1.1
		claim = 100
		adm = 5
		dip = 1
		mil = 5
    }
}

1495.1.1 = {
	monarch = {
 		name = "Rhaeniophai"
		dynasty = "Di-Oediphose"
		birth_date = 1448.1.1
		adm = 4
		dip = 4
		mil = 6
		female = yes
    }
	queen = {
 		name = "Laestithous"
		dynasty = "Di-Oediphose"
		birth_date = 1464.1.1
		adm = 1
		dip = 3
		mil = 4
    }
	heir = {
 		name = "Theamiophai"
		monarch_name = "Theamiophai I"
		dynasty = "Di-Oediphose"
		birth_date = 1489.1.1
		death_date = 1573.1.1
		claim = 100
		adm = 5
		dip = 1
		mil = 3
		female = yes
    }
}

1573.1.1 = {
	monarch = {
 		name = "Orthralus"
		dynasty = "Vu-Brethiophai"
		birth_date = 1547.1.1
		adm = 1
		dip = 3
		mil = 6
    }
}

1627.1.1 = {
	monarch = {
 		name = "Parthaus"
		dynasty = "Vu-Leuciphia"
		birth_date = 1604.1.1
		adm = 6
		dip = 2
		mil = 2
    }
	queen = {
 		name = "Nyshise"
		dynasty = "Vu-Leuciphia"
		birth_date = 1593.1.1
		adm = 3
		dip = 0
		mil = 1
		female = yes
    }
	heir = {
 		name = "Acsiphei"
		monarch_name = "Acsiphei I"
		dynasty = "Vu-Leuciphia"
		birth_date = 1622.1.1
		death_date = 1685.1.1
		claim = 100
		adm = 0
		dip = 6
		mil = 0
		female = yes
    }
}

1685.1.1 = {
	monarch = {
 		name = "Oriphosa"
		dynasty = "Vi-Misais"
		birth_date = 1648.1.1
		adm = 2
		dip = 0
		mil = 3
		female = yes
    }
	queen = {
 		name = "Aphrixi"
		dynasty = "Vi-Misais"
		birth_date = 1636.1.1
		adm = 6
		dip = 6
		mil = 2
    }
	heir = {
 		name = "Leuceshi"
		monarch_name = "Leuceshi I"
		dynasty = "Vi-Misais"
		birth_date = 1672.1.1
		death_date = 1751.1.1
		claim = 100
		adm = 3
		dip = 4
		mil = 1
		female = yes
    }
}

1751.1.1 = {
	monarch = {
 		name = "Theanaeia"
		dynasty = "Di-Ancochus"
		birth_date = 1703.1.1
		adm = 3
		dip = 1
		mil = 1
		female = yes
    }
}

1793.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Hespice"
		monarch_name = "Hespice I"
		dynasty = "Di-Prophilles"
		birth_date = 1788.1.1
		death_date = 1806.1.1
		claim = 100
		adm = 0
		dip = 3
		mil = 1
    }
}

1806.1.1 = {
	monarch = {
 		name = "Anthesi"
		dynasty = "Vu-Rhaeneis"
		birth_date = 1772.1.1
		adm = 6
		dip = 6
		mil = 1
		female = yes
    }
	queen = {
 		name = "Aphrixi"
		dynasty = "Vu-Rhaeneis"
		birth_date = 1756.1.1
		adm = 3
		dip = 5
		mil = 0
    }
	heir = {
 		name = "Prophios"
		monarch_name = "Prophios I"
		dynasty = "Vu-Rhaeneis"
		birth_date = 1798.1.1
		death_date = 1901.1.1
		claim = 100
		adm = 0
		dip = 3
		mil = 6
    }
}

1901.1.1 = {
	monarch = {
 		name = "Adrasaxaura"
		dynasty = "Du-Demithios"
		birth_date = 1855.1.1
		adm = 2
		dip = 4
		mil = 2
		female = yes
    }
	queen = {
 		name = "Zalisthus"
		dynasty = "Du-Demithios"
		birth_date = 1850.1.1
		adm = 6
		dip = 3
		mil = 0
    }
	heir = {
 		name = "Orphishia"
		monarch_name = "Orphishia I"
		dynasty = "Du-Demithios"
		birth_date = 1886.1.1
		death_date = 1996.1.1
		claim = 100
		adm = 3
		dip = 2
		mil = 6
		female = yes
    }
}

1996.1.1 = {
	monarch = {
 		name = "Anthesi"
		dynasty = "Du-Spyrersis"
		birth_date = 1956.1.1
		adm = 6
		dip = 3
		mil = 2
		female = yes
    }
}

2034.1.1 = {
	monarch = {
 		name = "Antiphoebus"
		dynasty = "Vi-Prisenis"
		birth_date = 1997.1.1
		adm = 1
		dip = 4
		mil = 3
    }
}

2133.1.1 = {
	monarch = {
 		name = "Jaselphos"
		dynasty = "Vu-Silishia"
		birth_date = 2105.1.1
		adm = 6
		dip = 3
		mil = 0
    }
}

2185.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Ophiali"
		monarch_name = "Ophiali I"
		dynasty = "Vi-Nysyse"
		birth_date = 2175.1.1
		death_date = 2193.1.1
		claim = 100
		adm = 3
		dip = 5
		mil = 0
		female = yes
    }
}

2193.1.1 = {
	monarch = {
 		name = "Adrasephia"
		dynasty = "Vi-Mystophi"
		birth_date = 2158.1.1
		adm = 3
		dip = 2
		mil = 0
		female = yes
    }
	queen = {
 		name = "Thitous"
		dynasty = "Vi-Mystophi"
		birth_date = 2166.1.1
		adm = 6
		dip = 0
		mil = 6
    }
}

2271.1.1 = {
	monarch = {
 		name = "Taphithe"
		dynasty = "Vu-Brethiophai"
		birth_date = 2218.1.1
		adm = 5
		dip = 6
		mil = 3
		female = yes
    }
}

2328.1.1 = {
	monarch = {
 		name = "Alpheas"
		dynasty = "Di-Caphaneithes"
		birth_date = 2283.1.1
		adm = 3
		dip = 6
		mil = 0
    }
	queen = {
 		name = "Ophiali"
		dynasty = "Di-Caphaneithes"
		birth_date = 2290.1.1
		adm = 0
		dip = 4
		mil = 6
		female = yes
    }
	heir = {
 		name = "Demithaeus"
		monarch_name = "Demithaeus I"
		dynasty = "Di-Caphaneithes"
		birth_date = 2315.1.1
		death_date = 2397.1.1
		claim = 100
		adm = 4
		dip = 3
		mil = 4
    }
}

2397.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Nyxophi"
		monarch_name = "Nyxophi I"
		dynasty = "Du-Parthaus"
		birth_date = 2390.1.1
		death_date = 2408.1.1
		claim = 100
		adm = 2
		dip = 2
		mil = 1
		female = yes
    }
}

2408.1.1 = {
	monarch = {
 		name = "Thimithous"
		dynasty = "Du-Orthrosyne"
		birth_date = 2376.1.1
		adm = 5
		dip = 3
		mil = 4
    }
	queen = {
 		name = "Rhaeniophai"
		dynasty = "Du-Orthrosyne"
		birth_date = 2377.1.1
		adm = 2
		dip = 2
		mil = 2
		female = yes
    }
	heir = {
 		name = "Brethilis"
		monarch_name = "Brethilis I"
		dynasty = "Du-Orthrosyne"
		birth_date = 2403.1.1
		death_date = 2478.1.1
		claim = 100
		adm = 5
		dip = 0
		mil = 1
		female = yes
    }
}

2478.1.1 = {
	monarch = {
 		name = "Mystymes"
		dynasty = "Di-Amycepios"
		birth_date = 2425.1.1
		adm = 5
		dip = 4
		mil = 2
		female = yes
    }
	queen = {
 		name = "Orthralus"
		dynasty = "Di-Amycepios"
		birth_date = 2453.1.1
		adm = 2
		dip = 2
		mil = 1
    }
	heir = {
 		name = "Hespocia"
		monarch_name = "Hespocia I"
		dynasty = "Di-Amycepios"
		birth_date = 2478.1.1
		death_date = 2576.1.1
		claim = 100
		adm = 2
		dip = 6
		mil = 2
    }
}

