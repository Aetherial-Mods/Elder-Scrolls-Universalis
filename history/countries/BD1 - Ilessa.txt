government = monarchy
government_rank = 1
mercantilism = 1
technology_group = northern_tg
religion = druidism
primary_culture = breton
capital = 1376

54.1.1 = {
	monarch = {
 		name = "Gyrard"
		dynasty = "Maurard"
		birth_date = 14.1.1
		adm = 1
		dip = 3
		mil = 3
    }
}

91.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Veronique"
		monarch_name = "Veronique I"
		dynasty = "Vallet"
		birth_date = 76.1.1
		death_date = 94.1.1
		claim = 100
		adm = 5
		dip = 5
		mil = 3
		female = yes
    }
}

94.1.1 = {
	monarch = {
 		name = "Hierot"
		dynasty = "Marigny"
		birth_date = 72.1.1
		adm = 4
		dip = 1
		mil = 3
    }
}

164.1.1 = {
	monarch = {
 		name = "Darene"
		dynasty = "Pathierry"
		birth_date = 122.1.1
		adm = 3
		dip = 0
		mil = 0
		female = yes
    }
}

205.1.1 = {
	monarch = {
 		name = "Zinedine"
		dynasty = "Seychelle"
		birth_date = 179.1.1
		adm = 1
		dip = 6
		mil = 3
		female = yes
    }
}

285.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Verene"
		monarch_name = "Verene I"
		dynasty = "Paumier"
		birth_date = 274.1.1
		death_date = 292.1.1
		claim = 100
		adm = 2
		dip = 4
		mil = 1
		female = yes
    }
}

292.1.1 = {
	monarch = {
 		name = "Jehannette"
		dynasty = "Tibbin"
		birth_date = 252.1.1
		adm = 4
		dip = 5
		mil = 4
		female = yes
    }
}

348.1.1 = {
	monarch = {
 		name = "Danwyche"
		dynasty = "Dubois"
		birth_date = 327.1.1
		adm = 3
		dip = 4
		mil = 0
    }
}

404.1.1 = {
	monarch = {
 		name = "Yanis"
		dynasty = "Ascent"
		birth_date = 365.1.1
		adm = 4
		dip = 5
		mil = 2
    }
	queen = {
 		name = "Thais"
		dynasty = "Ascent"
		birth_date = 365.1.1
		adm = 5
		dip = 2
		mil = 3
		female = yes
    }
	heir = {
 		name = "Granier"
		monarch_name = "Granier I"
		dynasty = "Ascent"
		birth_date = 390.1.1
		death_date = 503.1.1
		claim = 100
		adm = 2
		dip = 0
		mil = 2
    }
}

503.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Charmel"
		monarch_name = "Charmel I"
		dynasty = "Channitte"
		birth_date = 488.1.1
		death_date = 506.1.1
		claim = 100
		adm = 0
		dip = 0
		mil = 6
		female = yes
    }
}

506.1.1 = {
	monarch = {
 		name = "Percy"
		dynasty = "Jerine"
		birth_date = 455.1.1
		adm = 6
		dip = 3
		mil = 6
    }
	queen = {
 		name = "Bovkinna"
		dynasty = "Jerine"
		birth_date = 465.1.1
		adm = 3
		dip = 1
		mil = 5
		female = yes
    }
	heir = {
 		name = "Nak'tah"
		monarch_name = "Nak'tah I"
		dynasty = "Jerine"
		birth_date = 498.1.1
		death_date = 563.1.1
		claim = 100
		adm = 0
		dip = 0
		mil = 4
    }
}

563.1.1 = {
	monarch = {
 		name = "Debentien"
		dynasty = "Missonnier"
		birth_date = 532.1.1
		adm = 3
		dip = 1
		mil = 6
    }
}

638.1.1 = {
	monarch = {
 		name = "Achibert"
		dynasty = "Hedier"
		birth_date = 620.1.1
		adm = 1
		dip = 1
		mil = 3
    }
}

677.1.1 = {
	monarch = {
 		name = "Paulin"
		dynasty = "Thierry"
		birth_date = 642.1.1
		adm = 6
		dip = 0
		mil = 6
    }
	queen = {
 		name = "Maurrie"
		dynasty = "Thierry"
		birth_date = 631.1.1
		adm = 3
		dip = 5
		mil = 5
		female = yes
    }
	heir = {
 		name = "Crendal"
		monarch_name = "Crendal I"
		dynasty = "Thierry"
		birth_date = 673.1.1
		death_date = 723.1.1
		claim = 100
		adm = 0
		dip = 4
		mil = 4
    }
}

723.1.1 = {
	monarch = {
 		name = "Davide"
		dynasty = "Rye"
		birth_date = 679.1.1
		adm = 6
		dip = 0
		mil = 5
    }
	queen = {
 		name = "Camille"
		dynasty = "Rye"
		birth_date = 690.1.1
		adm = 3
		dip = 6
		mil = 4
		female = yes
    }
	heir = {
 		name = "Norbert"
		monarch_name = "Norbert I"
		dynasty = "Rye"
		birth_date = 708.1.1
		death_date = 778.1.1
		claim = 100
		adm = 3
		dip = 2
		mil = 5
    }
}

778.1.1 = {
	monarch = {
 		name = "Dimitri"
		dynasty = "Hawrond"
		birth_date = 737.1.1
		adm = 3
		dip = 6
		mil = 5
    }
}

816.1.1 = {
	monarch = {
 		name = "Alexia"
		dynasty = "Cassel"
		birth_date = 776.1.1
		adm = 1
		dip = 5
		mil = 2
		female = yes
    }
}

853.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Vilhim"
		monarch_name = "Vilhim I"
		dynasty = "Retene"
		birth_date = 845.1.1
		death_date = 863.1.1
		claim = 100
		adm = 2
		dip = 2
		mil = 0
    }
}

863.1.1 = {
	monarch = {
 		name = "Laetitia"
		dynasty = "Justal"
		birth_date = 816.1.1
		adm = 5
		dip = 3
		mil = 2
		female = yes
    }
	queen = {
 		name = "Gessure"
		dynasty = "Justal"
		birth_date = 812.1.1
		adm = 1
		dip = 2
		mil = 1
    }
	heir = {
 		name = "Isabeth"
		monarch_name = "Isabeth I"
		dynasty = "Justal"
		birth_date = 863.1.1
		death_date = 941.1.1
		claim = 100
		adm = 5
		dip = 0
		mil = 0
		female = yes
    }
}

941.1.1 = {
	monarch = {
 		name = "Ailex"
		dynasty = "Labouche"
		birth_date = 923.1.1
		adm = 5
		dip = 4
		mil = 0
    }
	queen = {
 		name = "Vaudrie"
		dynasty = "Labouche"
		birth_date = 892.1.1
		adm = 5
		dip = 0
		mil = 1
		female = yes
    }
	heir = {
 		name = "Jeanylle"
		monarch_name = "Jeanylle I"
		dynasty = "Labouche"
		birth_date = 932.1.1
		death_date = 992.1.1
		claim = 100
		adm = 2
		dip = 6
		mil = 0
		female = yes
    }
}

992.1.1 = {
	monarch = {
 		name = "Alinon"
		dynasty = "Herrick"
		birth_date = 944.1.1
		adm = 1
		dip = 2
		mil = 0
    }
}

1079.1.1 = {
	monarch = {
 		name = "Prolyssa"
		dynasty = "Capron"
		birth_date = 1028.1.1
		adm = 6
		dip = 1
		mil = 4
		female = yes
    }
	queen = {
 		name = "Charbriel"
		dynasty = "Capron"
		birth_date = 1053.1.1
		adm = 3
		dip = 0
		mil = 3
    }
}

1172.1.1 = {
	monarch = {
 		name = "Armandine"
		dynasty = "Stroud"
		birth_date = 1149.1.1
		adm = 2
		dip = 6
		mil = 0
		female = yes
    }
}

1257.1.1 = {
	monarch = {
 		name = "Rubyn"
		dynasty = "Lan"
		birth_date = 1218.1.1
		adm = 0
		dip = 5
		mil = 3
    }
	queen = {
 		name = "Constance"
		dynasty = "Lan"
		birth_date = 1232.1.1
		adm = 4
		dip = 4
		mil = 2
		female = yes
    }
	heir = {
 		name = "Piernette"
		monarch_name = "Piernette I"
		dynasty = "Lan"
		birth_date = 1244.1.1
		death_date = 1324.1.1
		claim = 100
		adm = 1
		dip = 2
		mil = 1
		female = yes
    }
}

1324.1.1 = {
	monarch = {
 		name = "Esteve"
		dynasty = "Gelves"
		birth_date = 1280.1.1
		adm = 3
		dip = 4
		mil = 4
    }
}

1405.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Draven"
		monarch_name = "Draven I"
		dynasty = "Charchere"
		birth_date = 1393.1.1
		death_date = 1411.1.1
		claim = 100
		adm = 4
		dip = 1
		mil = 2
    }
}

1411.1.1 = {
	monarch = {
 		name = "Rolbert"
		dynasty = "Chauvry"
		birth_date = 1389.1.1
		adm = 3
		dip = 4
		mil = 2
    }
	queen = {
 		name = "Nyrona"
		dynasty = "Chauvry"
		birth_date = 1387.1.1
		adm = 4
		dip = 1
		mil = 3
		female = yes
    }
	heir = {
 		name = "Edweg"
		monarch_name = "Edweg I"
		dynasty = "Chauvry"
		birth_date = 1403.1.1
		death_date = 1464.1.1
		claim = 100
		adm = 0
		dip = 6
		mil = 2
    }
}

1464.1.1 = {
	monarch = {
 		name = "Serenarth"
		dynasty = "Pathierry"
		birth_date = 1425.1.1
		adm = 0
		dip = 3
		mil = 2
    }
}

1554.1.1 = {
	monarch = {
 		name = "Maelle"
		dynasty = "Montarbault"
		birth_date = 1520.1.1
		adm = 5
		dip = 2
		mil = 6
		female = yes
    }
	queen = {
 		name = "Zavour"
		dynasty = "Montarbault"
		birth_date = 1502.1.1
		adm = 2
		dip = 0
		mil = 5
    }
	heir = {
 		name = "Jonne"
		monarch_name = "Jonne I"
		dynasty = "Montarbault"
		birth_date = 1554.1.1
		death_date = 1641.1.1
		claim = 100
		adm = 6
		dip = 6
		mil = 4
    }
}

1641.1.1 = {
	monarch = {
 		name = "Augustina"
		dynasty = "Landreau"
		birth_date = 1614.1.1
		adm = 2
		dip = 0
		mil = 6
		female = yes
    }
}

1718.1.1 = {
	monarch = {
 		name = "Sarvith"
		dynasty = "Cirges"
		birth_date = 1667.1.1
		adm = 0
		dip = 6
		mil = 3
    }
	queen = {
 		name = "Osanna"
		dynasty = "Cirges"
		birth_date = 1671.1.1
		adm = 4
		dip = 5
		mil = 2
		female = yes
    }
	heir = {
 		name = "Raylen"
		monarch_name = "Raylen I"
		dynasty = "Cirges"
		birth_date = 1703.1.1
		death_date = 1763.1.1
		claim = 100
		adm = 1
		dip = 3
		mil = 1
    }
}

1763.1.1 = {
	monarch = {
 		name = "Faolchu"
		dynasty = "Gevette"
		birth_date = 1715.1.1
		adm = 0
		dip = 0
		mil = 1
    }
	queen = {
 		name = "Dominique"
		dynasty = "Gevette"
		birth_date = 1712.1.1
		adm = 0
		dip = 3
		mil = 2
		female = yes
    }
	heir = {
 		name = "Rianne"
		monarch_name = "Rianne I"
		dynasty = "Gevette"
		birth_date = 1748.1.1
		death_date = 1838.1.1
		claim = 100
		adm = 4
		dip = 2
		mil = 1
		female = yes
    }
}

1838.1.1 = {
	monarch = {
 		name = "Franck"
		dynasty = "Chrirnis"
		birth_date = 1785.1.1
		adm = 3
		dip = 5
		mil = 1
    }
}

1886.1.1 = {
	monarch = {
 		name = "Beaunois"
		dynasty = "Dupare"
		birth_date = 1837.1.1
		adm = 2
		dip = 5
		mil = 5
    }
}

1927.1.1 = {
	monarch = {
 		name = "Staubin"
		dynasty = "Rane"
		birth_date = 1904.1.1
		adm = 0
		dip = 4
		mil = 1
    }
	queen = {
 		name = "Dianette"
		dynasty = "Rane"
		birth_date = 1894.1.1
		adm = 4
		dip = 2
		mil = 0
		female = yes
    }
}

1984.1.1 = {
	monarch = {
 		name = "Celnard"
		dynasty = "Favret"
		birth_date = 1955.1.1
		adm = 2
		dip = 1
		mil = 4
    }
}

2035.1.1 = {
	monarch = {
 		name = "Treves"
		dynasty = "Macien"
		birth_date = 2004.1.1
		adm = 0
		dip = 1
		mil = 1
    }
	queen = {
 		name = "Endyra"
		dynasty = "Macien"
		birth_date = 1983.1.1
		adm = 4
		dip = 6
		mil = 0
		female = yes
    }
	heir = {
 		name = "Severine"
		monarch_name = "Severine I"
		dynasty = "Macien"
		birth_date = 2029.1.1
		death_date = 2109.1.1
		claim = 100
		adm = 6
		dip = 0
		mil = 4
		female = yes
    }
}

2109.1.1 = {
	monarch = {
 		name = "Lucienne"
		dynasty = "Hurier"
		birth_date = 2084.1.1
		adm = 5
		dip = 3
		mil = 5
		female = yes
    }
}

2167.1.1 = {
	monarch = {
 		name = "Etty"
		dynasty = "Niert"
		birth_date = 2117.1.1
		adm = 3
		dip = 2
		mil = 1
		female = yes
    }
	queen = {
 		name = "Olivier"
		dynasty = "Niert"
		birth_date = 2120.1.1
		adm = 0
		dip = 1
		mil = 0
    }
	heir = {
 		name = "Edana"
		monarch_name = "Edana I"
		dynasty = "Niert"
		birth_date = 2160.1.1
		death_date = 2236.1.1
		claim = 100
		adm = 4
		dip = 0
		mil = 6
		female = yes
    }
}

2236.1.1 = {
	monarch = {
 		name = "Rogolphe"
		dynasty = "Aubertin"
		birth_date = 2214.1.1
		adm = 0
		dip = 1
		mil = 2
    }
	queen = {
 		name = "Clotylda"
		dynasty = "Aubertin"
		birth_date = 2211.1.1
		adm = 4
		dip = 6
		mil = 1
		female = yes
    }
	heir = {
 		name = "Pierre"
		monarch_name = "Pierre I"
		dynasty = "Aubertin"
		birth_date = 2233.1.1
		death_date = 2328.1.1
		claim = 100
		adm = 0
		dip = 5
		mil = 6
    }
}

2328.1.1 = {
	monarch = {
 		name = "Ernirus"
		dynasty = "Benele"
		birth_date = 2309.1.1
		adm = 3
		dip = 6
		mil = 2
    }
	queen = {
 		name = "Delphine"
		dynasty = "Benele"
		birth_date = 2288.1.1
		adm = 0
		dip = 5
		mil = 1
		female = yes
    }
	heir = {
 		name = "Raelynn"
		monarch_name = "Raelynn I"
		dynasty = "Benele"
		birth_date = 2315.1.1
		death_date = 2427.1.1
		claim = 100
		adm = 6
		dip = 1
		mil = 1
		female = yes
    }
}

2427.1.1 = {
	monarch = {
 		name = "Rosaldine"
		dynasty = "Silane"
		birth_date = 2387.1.1
		adm = 3
		dip = 0
		mil = 0
		female = yes
    }
}

2469.1.1 = {
	monarch = {
 		name = "Avrippe"
		dynasty = "Erelle"
		birth_date = 2451.1.1
		adm = 2
		dip = 6
		mil = 4
    }
	queen = {
 		name = "Joelle"
		dynasty = "Erelle"
		birth_date = 2427.1.1
		adm = 5
		dip = 5
		mil = 3
		female = yes
    }
	heir = {
 		name = "Alphaury"
		monarch_name = "Alphaury I"
		dynasty = "Erelle"
		birth_date = 2455.1.1
		death_date = 2553.1.1
		claim = 100
		adm = 2
		dip = 3
		mil = 2
    }
}

