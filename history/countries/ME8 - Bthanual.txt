government = republic
government_rank = 3
mercantilism = 1
technology_group = dwemer_tg
religion = reason_and_logic_cult
primary_culture = dwemer
capital = 3832

54.1.1 = {
	monarch = {
 		name = "Ilzeglan"
		dynasty = "Af'Izvulzarf"
		birth_date = 17.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

107.1.1 = {
	monarch = {
 		name = "Rafnyg"
		dynasty = "Af'Ychogarn"
		birth_date = 54.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

169.1.1 = {
	monarch = {
 		name = "Agahuanch"
		dynasty = "Af'Chruhnch"
		birth_date = 122.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

226.1.1 = {
	monarch = {
 		name = "Jnavraz"
		dynasty = "Aq'Krilnif"
		birth_date = 189.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

269.1.1 = {
	monarch = {
 		name = "Alnodrunz"
		dynasty = "Af'Szogarn"
		birth_date = 239.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

336.1.1 = {
	monarch = {
 		name = "Nebgar"
		dynasty = "Av'Chrudras"
		birth_date = 317.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

415.1.1 = {
	monarch = {
 		name = "Tadlin"
		dynasty = "Av'Gzozchyn"
		birth_date = 391.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

465.1.1 = {
	monarch = {
 		name = "Tugradac"
		dynasty = "Af'Mhanrazg"
		birth_date = 430.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

513.1.1 = {
	monarch = {
 		name = "Alnodrunz"
		dynasty = "Af'Jrudit"
		birth_date = 472.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

599.1.1 = {
	monarch = {
 		name = "Ksrefurn"
		dynasty = "Aq'Chrotwern"
		birth_date = 551.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

640.1.1 = {
	monarch = {
 		name = "Tadlin"
		dynasty = "Af'Ychogarn"
		birth_date = 618.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

739.1.1 = {
	monarch = {
 		name = "Tugradac"
		dynasty = "Az'Nromratz"
		birth_date = 695.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

809.1.1 = {
	monarch = {
 		name = "Ishevnorz"
		dynasty = "Az'Jravarn"
		birth_date = 778.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

873.1.1 = {
	monarch = {
 		name = "Yhnazzefk"
		dynasty = "Av'Tugradac"
		birth_date = 852.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

911.1.1 = {
	monarch = {
 		name = "Nromratz"
		dynasty = "Az'Doudhis"
		birth_date = 862.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

994.1.1 = {
	monarch = {
 		name = "Nrotchzan"
		dynasty = "Aq'Chzegrenz"
		birth_date = 946.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1052.1.1 = {
	monarch = {
 		name = "Ishevnorz"
		dynasty = "Aq'Nromgunch"
		birth_date = 1019.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1112.1.1 = {
	monarch = {
 		name = "Mlirloar"
		dynasty = "Aq'Aravlen"
		birth_date = 1064.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1161.1.1 = {
	monarch = {
 		name = "Ynzatarn"
		dynasty = "Av'Snelarn"
		birth_date = 1109.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1233.1.1 = {
	monarch = {
 		name = "Ruerbira"
		dynasty = "Av'Gzozchyn"
		birth_date = 1182.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1296.1.1 = {
	monarch = {
 		name = "Asratchzan"
		dynasty = "Aq'Ylregriln"
		birth_date = 1252.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1377.1.1 = {
	monarch = {
 		name = "Djuhnch"
		dynasty = "Av'Davlar"
		birth_date = 1329.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1465.1.1 = {
	monarch = {
 		name = "Mravlara"
		dynasty = "Aq'Chrotwern"
		birth_date = 1430.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1528.1.1 = {
	monarch = {
 		name = "Bhazchyn"
		dynasty = "Av'Inratarn"
		birth_date = 1478.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1615.1.1 = {
	monarch = {
 		name = "Nchynac"
		dynasty = "Aq'Bluvzal"
		birth_date = 1590.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1658.1.1 = {
	monarch = {
 		name = "Rhzomrond"
		dynasty = "Af'Chzetvar"
		birth_date = 1621.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1741.1.1 = {
	monarch = {
 		name = "Ynzarlakch"
		dynasty = "Av'Mzamrumhz"
		birth_date = 1688.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1783.1.1 = {
	monarch = {
 		name = "Choadzach"
		dynasty = "Aq'Jhonzcharn"
		birth_date = 1764.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1833.1.1 = {
	monarch = {
 		name = "Bzrazgar"
		dynasty = "Af'Agahuanch"
		birth_date = 1801.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1904.1.1 = {
	monarch = {
 		name = "Nhetchatz"
		dynasty = "Av'Tnarlac"
		birth_date = 1876.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1987.1.1 = {
	monarch = {
 		name = "Ghafuan"
		dynasty = "Aq'Rlorlis"
		birth_date = 1964.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2048.1.1 = {
	monarch = {
 		name = "Choadzach"
		dynasty = "Af'Ychogarn"
		birth_date = 2012.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

2105.1.1 = {
	monarch = {
 		name = "Bzrazgar"
		dynasty = "Av'Mzamrumhz"
		birth_date = 2086.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2155.1.1 = {
	monarch = {
 		name = "Jholzarf"
		dynasty = "Az'Gzonrynn"
		birth_date = 2105.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2254.1.1 = {
	monarch = {
 		name = "Ararbira"
		dynasty = "Az'Azsamchin"
		birth_date = 2211.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

2348.1.1 = {
	monarch = {
 		name = "Drunrynn"
		dynasty = "Aq'Chzefrach"
		birth_date = 2306.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

2429.1.1 = {
	monarch = {
 		name = "Jhourlac"
		dynasty = "Av'Yhnazdir"
		birth_date = 2392.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2481.1.1 = {
	monarch = {
 		name = "Jholzarf"
		dynasty = "Af'Jnavraz"
		birth_date = 2435.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

