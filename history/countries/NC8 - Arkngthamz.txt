government = republic
government_rank = 3
mercantilism = 1
technology_group = dwemer_tg
religion = reason_and_logic_cult
primary_culture = dwemer
capital = 7245

54.1.1 = {
	monarch = {
 		name = "Nebgar"
		dynasty = "Av'Tadlin"
		birth_date = 36.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

144.1.1 = {
	monarch = {
 		name = "Tadlin"
		dynasty = "Aq'Choadzach"
		birth_date = 123.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

199.1.1 = {
	monarch = {
 		name = "Byrahken"
		dynasty = "Av'Jholzarf"
		birth_date = 161.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

262.1.1 = {
	monarch = {
 		name = "Ishevnorz"
		dynasty = "Av'Davlar"
		birth_date = 222.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

320.1.1 = {
	monarch = {
 		name = "Yhnazzefk"
		dynasty = "Az'Tronruz"
		birth_date = 270.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

386.1.1 = {
	monarch = {
 		name = "Nromratz"
		dynasty = "Az'Ryumorn"
		birth_date = 340.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

432.1.1 = {
	monarch = {
 		name = "Jrudit"
		dynasty = "Az'Choahretz"
		birth_date = 388.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

485.1.1 = {
	monarch = {
 		name = "Banrynn"
		dynasty = "Af'Chiuvnak"
		birth_date = 464.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

560.1.1 = {
	monarch = {
 		name = "Yhnazzefk"
		dynasty = "Aq'Jlethurzch"
		birth_date = 526.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

654.1.1 = {
	monarch = {
 		name = "Nromratz"
		dynasty = "Av'Inratarn"
		birth_date = 622.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

704.1.1 = {
	monarch = {
 		name = "Jrudit"
		dynasty = "Aq'Grigarn"
		birth_date = 683.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

785.1.1 = {
	monarch = {
 		name = "Asratchzan"
		dynasty = "Av'Inratarn"
		birth_date = 736.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

852.1.1 = {
	monarch = {
 		name = "Djuhnch"
		dynasty = "Az'Chzebchasz"
		birth_date = 832.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

924.1.1 = {
	monarch = {
 		name = "Ynzarlatz"
		dynasty = "Aq'Nromgunch"
		birth_date = 899.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

995.1.1 = {
	monarch = {
 		name = "Dark"
		dynasty = "Az'Kridhis"
		birth_date = 944.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1036.1.1 = {
	monarch = {
 		name = "Izvumratz"
		dynasty = "Az'Ithorves"
		birth_date = 984.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1120.1.1 = {
	monarch = {
 		name = "Nhetchatz"
		dynasty = "Az'Chzebchasz"
		birth_date = 1082.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1173.1.1 = {
	monarch = {
 		name = "Szogarn"
		dynasty = "Aq'Jhomrumhz"
		birth_date = 1142.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1233.1.1 = {
	monarch = {
 		name = "Dzragvin"
		dynasty = "Aq'Choadzach"
		birth_date = 1188.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1297.1.1 = {
	monarch = {
 		name = "Ychogrenz"
		dynasty = "Av'Ghaznak"
		birth_date = 1274.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1384.1.1 = {
	monarch = {
 		name = "Inratarn"
		dynasty = "Av'Grubond"
		birth_date = 1358.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1444.1.1 = {
	monarch = {
 		name = "Ynzarlakch"
		dynasty = "Aq'Rlovrin"
		birth_date = 1397.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1495.1.1 = {
	monarch = {
 		name = "Tahron"
		dynasty = "Az'Ryumorn"
		birth_date = 1460.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1541.1.1 = {
	monarch = {
 		name = "Mlirtes"
		dynasty = "Af'Ghafuan"
		birth_date = 1512.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1636.1.1 = {
	monarch = {
 		name = "Jholzarf"
		dynasty = "Af'Jhourlac"
		birth_date = 1597.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1675.1.1 = {
	monarch = {
 		name = "Mhunac"
		dynasty = "Aq'Blulnmer"
		birth_date = 1625.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1732.1.1 = {
	monarch = {
 		name = "Mhanrazg"
		dynasty = "Aq'Jhomrumhz"
		birth_date = 1679.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1775.1.1 = {
	monarch = {
 		name = "Jhourlac"
		dynasty = "Aq'Mrotchatz"
		birth_date = 1731.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1832.1.1 = {
	monarch = {
 		name = "Jholzarf"
		dynasty = "Av'Ihlefuan"
		birth_date = 1807.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1913.1.1 = {
	monarch = {
 		name = "Mhunac"
		dynasty = "Aq'Snenard"
		birth_date = 1880.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1986.1.1 = {
	monarch = {
 		name = "Drunrynn"
		dynasty = "Af'Tzenruz"
		birth_date = 1935.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

2066.1.1 = {
	monarch = {
 		name = "Ithorves"
		dynasty = "Av'Jholzarf"
		birth_date = 2041.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

2118.1.1 = {
	monarch = {
 		name = "Agahuanch"
		dynasty = "Az'Doudrys"
		birth_date = 2070.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2185.1.1 = {
	monarch = {
 		name = "Jnavraz"
		dynasty = "Av'Mzaglan"
		birth_date = 2158.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2240.1.1 = {
	monarch = {
 		name = "Ilzeglan"
		dynasty = "Az'Nohnch"
		birth_date = 2193.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

2316.1.1 = {
	monarch = {
 		name = "Ghaznak"
		dynasty = "Aq'Somzlin"
		birth_date = 2284.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2390.1.1 = {
	monarch = {
 		name = "Ylrefwinn"
		dynasty = "Av'Ralen"
		birth_date = 2355.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

2429.1.1 = {
	monarch = {
 		name = "Szoglynsh"
		dynasty = "Aq'Bluhzis"
		birth_date = 2398.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

2466.1.1 = {
	monarch = {
 		name = "Achygrac"
		dynasty = "Az'Nhezril"
		birth_date = 2441.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

