government = republic
government_rank = 3
mercantilism = 1
technology_group = elven_tg
religion = bosmer_pantheon
primary_culture = bosmer
capital = 4941

54.1.1 = {
	monarch = {
 		name = "Cendalor"
		dynasty = "Ur'Balfstone"
		birth_date = 7.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

89.1.1 = {
	monarch = {
 		name = "Rilion"
		dynasty = "Or'Vulkwasten"
		birth_date = 69.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

170.1.1 = {
	monarch = {
 		name = "Little"
		dynasty = "Ca'Glien"
		birth_date = 139.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

230.1.1 = {
	monarch = {
 		name = "Fingor"
		dynasty = "Ca'Lynpar"
		birth_date = 209.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

304.1.1 = {
	monarch = {
 		name = "Cenrathil"
		dynasty = "Ur'Eineneth"
		birth_date = 270.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

352.1.1 = {
	monarch = {
 		name = "Sarrothiel"
		dynasty = "Or'Wasten"
		birth_date = 300.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

430.1.1 = {
	monarch = {
 		name = "Cunodir"
		dynasty = "Ur'Arenthia"
		birth_date = 402.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

504.1.1 = {
	monarch = {
 		name = "Rolon"
		dynasty = "Or'Niveneth"
		birth_date = 482.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

581.1.1 = {
	monarch = {
 		name = "Malareth"
		dynasty = "Or'Nileneth"
		birth_date = 531.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

641.1.1 = {
	monarch = {
 		name = "Firthaedal"
		dynasty = "Or'Stonesquare"
		birth_date = 612.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

693.1.1 = {
	monarch = {
 		name = "Clenalgor"
		dynasty = "Or'Vulkwasten"
		birth_date = 642.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

732.1.1 = {
	monarch = {
 		name = "Sorvayel"
		dynasty = "Or'Riverwood"
		birth_date = 680.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

768.1.1 = {
	monarch = {
 		name = "Lenranas"
		dynasty = "Or'Southpoint"
		birth_date = 744.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

864.1.1 = {
	monarch = {
 		name = "Firolmoth"
		dynasty = "Ca'Lichenrock"
		birth_date = 844.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

961.1.1 = {
	monarch = {
 		name = "Mell"
		dynasty = "Ur'Arednor"
		birth_date = 936.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1016.1.1 = {
	monarch = {
 		name = "Forolon"
		dynasty = "Ca'Glien"
		birth_date = 986.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1083.1.1 = {
	monarch = {
 		name = "Dandra"
		dynasty = "Or'Naerilin"
		birth_date = 1064.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1130.1.1 = {
	monarch = {
 		name = "Salgaer"
		dynasty = "Ca'Moran"
		birth_date = 1084.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1172.1.1 = {
	monarch = {
 		name = "Melbethil"
		dynasty = "Ur'Bluewind"
		birth_date = 1133.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1224.1.1 = {
	monarch = {
 		name = "Galthel"
		dynasty = "Or'Tarlain"
		birth_date = 1189.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1322.1.1 = {
	monarch = {
 		name = "Dairaegal"
		dynasty = "Ur'Donraenriel"
		birth_date = 1276.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1388.1.1 = {
	monarch = {
 		name = "Saldaer"
		dynasty = "Ur'Bluewood"
		birth_date = 1337.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1471.1.1 = {
	monarch = {
 		name = "Dolilas"
		dynasty = "Or'Thamnel"
		birth_date = 1446.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1549.1.1 = {
	monarch = {
 		name = "Tholerthorn"
		dynasty = "		"
		birth_date = 1510.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1594.1.1 = {
	monarch = {
 		name = "Gwaerinbor"
		dynasty = "Ca'Gelwen"
		birth_date = 1550.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1658.1.1 = {
	monarch = {
 		name = "Neronnir"
		dynasty = "Or'Nileneth"
		birth_date = 1629.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1701.1.1 = {
	monarch = {
 		name = "Hasiwen"
		dynasty = "Ur'Applegrass"
		birth_date = 1650.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1771.1.1 = {
	monarch = {
 		name = "Erilthel"
		dynasty = "Or'Parndra"
		birth_date = 1720.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1824.1.1 = {
	monarch = {
 		name = "Angeron"
		dynasty = "Ur'Cormair"
		birth_date = 1802.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1885.1.1 = {
	monarch = {
 		name = "Nendilir"
		dynasty = "Or'Run"
		birth_date = 1835.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1929.1.1 = {
	monarch = {
 		name = "Gwingelruin"
		dynasty = "Ur'Eagle"
		birth_date = 1894.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2013.1.1 = {
	monarch = {
 		name = "Enthoras"
		dynasty = "Ur'Areanragil"
		birth_date = 1980.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2048.1.1 = {
	monarch = {
 		name = "Andranon"
		dynasty = "Ca'Filen"
		birth_date = 2008.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2121.1.1 = {
	monarch = {
 		name = "Ethrandora"
		dynasty = "Ur'Bothenandriah"
		birth_date = 2089.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

2218.1.1 = {
	monarch = {
 		name = "Rodaegaer"
		dynasty = "Ca'Estiynia"
		birth_date = 2182.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2269.1.1 = {
	monarch = {
 		name = "Lengaer"
		dynasty = "Or'Naerilin"
		birth_date = 2219.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2305.1.1 = {
	monarch = {
 		name = "Saldaer"
		dynasty = "Ca'Glien"
		birth_date = 2256.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2351.1.1 = {
	monarch = {
 		name = "Glothor"
		dynasty = "Or'Niveneth"
		birth_date = 2314.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2423.1.1 = {
	monarch = {
 		name = "Eguroth"
		dynasty = "Ca'Green"
		birth_date = 2395.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2494.1.1 = {
	monarch = {
 		name = "Gwagoth"
		dynasty = "Or'Velyn"
		birth_date = 2453.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

