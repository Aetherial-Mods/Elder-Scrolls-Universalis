government = monarchy
government_rank = 1
mercantilism = 1
technology_group = akavir_tg
religion = po_tun_pantheon
primary_culture = po_tun
capital = 623

54.1.1 = {
	monarch = {
 		name = "Milin"
		dynasty = "Luwume'wo"
		birth_date = 7.1.1
		adm = 6
		dip = 1
		mil = 5
		female = yes
    }
}

136.1.1 = {
	monarch = {
 		name = "Kruyd"
		dynasty = "Tusserr'su"
		birth_date = 103.1.1
		adm = 4
		dip = 1
		mil = 2
    }
	queen = {
 		name = "Luttesso"
		dynasty = "Tusserr'su"
		birth_date = 116.1.1
		adm = 1
		dip = 6
		mil = 1
		female = yes
    }
	heir = {
 		name = "Mur"
		monarch_name = "Mur I"
		dynasty = "Tusserr'su"
		birth_date = 126.1.1
		death_date = 210.1.1
		claim = 100
		adm = 5
		dip = 5
		mil = 0
    }
}

210.1.1 = {
	monarch = {
 		name = "Doknog"
		dynasty = "Sohemool'wo"
		birth_date = 179.1.1
		adm = 1
		dip = 6
		mil = 2
    }
	queen = {
 		name = "Fulnod"
		dynasty = "Sohemool'wo"
		birth_date = 181.1.1
		adm = 5
		dip = 5
		mil = 1
		female = yes
    }
	heir = {
 		name = "Dalnog"
		monarch_name = "Dalnog I"
		dynasty = "Sohemool'wo"
		birth_date = 196.1.1
		death_date = 268.1.1
		claim = 100
		adm = 2
		dip = 3
		mil = 0
    }
}

268.1.1 = {
	monarch = {
 		name = "Tuwurtus"
		dynasty = "Kusdere'su"
		birth_date = 242.1.1
		adm = 1
		dip = 0
		mil = 1
		female = yes
    }
	queen = {
 		name = "Tho"
		dynasty = "Kusdere'su"
		birth_date = 230.1.1
		adm = 1
		dip = 3
		mil = 2
    }
	heir = {
 		name = "Chadhak"
		monarch_name = "Chadhak I"
		dynasty = "Kusdere'su"
		birth_date = 266.1.1
		death_date = 352.1.1
		claim = 100
		adm = 5
		dip = 2
		mil = 1
    }
}

352.1.1 = {
	monarch = {
 		name = "Quv"
		dynasty = "Kumolen'ri"
		birth_date = 307.1.1
		adm = 4
		dip = 5
		mil = 1
    }
}

423.1.1 = {
	monarch = {
 		name = "Cin"
		dynasty = "De'la"
		birth_date = 378.1.1
		adm = 3
		dip = 4
		mil = 5
		female = yes
    }
}

514.1.1 = {
	monarch = {
 		name = "Fiandok"
		dynasty = "La'wo"
		birth_date = 481.1.1
		adm = 1
		dip = 3
		mil = 1
    }
	queen = {
 		name = "Delolcok"
		dynasty = "La'wo"
		birth_date = 490.1.1
		adm = 5
		dip = 2
		mil = 0
		female = yes
    }
}

560.1.1 = {
	monarch = {
 		name = "Fiestemo"
		dynasty = "Lud'la"
		birth_date = 533.1.1
		adm = 3
		dip = 1
		mil = 4
    }
	queen = {
 		name = "Listus"
		dynasty = "Lud'la"
		birth_date = 539.1.1
		adm = 0
		dip = 0
		mil = 3
		female = yes
    }
}

615.1.1 = {
	monarch = {
 		name = "Tusserr"
		dynasty = "Pad'ri"
		birth_date = 565.1.1
		adm = 5
		dip = 6
		mil = 6
    }
	queen = {
 		name = "Ther"
		dynasty = "Pad'ri"
		birth_date = 569.1.1
		adm = 2
		dip = 4
		mil = 5
		female = yes
    }
	heir = {
 		name = "Rilu"
		monarch_name = "Rilu I"
		dynasty = "Pad'ri"
		birth_date = 600.1.1
		death_date = 705.1.1
		claim = 100
		adm = 6
		dip = 3
		mil = 4
		female = yes
    }
}

705.1.1 = {
	monarch = {
 		name = "Bim"
		dynasty = "Fiandok'su"
		birth_date = 687.1.1
		adm = 2
		dip = 4
		mil = 0
		female = yes
    }
}

770.1.1 = {
	monarch = {
 		name = "Le"
		dynasty = "Tehortes'la"
		birth_date = 732.1.1
		adm = 0
		dip = 3
		mil = 3
		female = yes
    }
	queen = {
 		name = "Chuhleyd"
		dynasty = "Tehortes'la"
		birth_date = 742.1.1
		adm = 4
		dip = 2
		mil = 2
    }
	heir = {
 		name = "Tiar"
		monarch_name = "Tiar I"
		dynasty = "Tehortes'la"
		birth_date = 759.1.1
		death_date = 817.1.1
		claim = 100
		adm = 1
		dip = 1
		mil = 1
		female = yes
    }
}

817.1.1 = {
	monarch = {
 		name = "Ruldov"
		dynasty = "Kruyd'ri"
		birth_date = 764.1.1
		adm = 0
		dip = 4
		mil = 2
    }
	queen = {
 		name = "De"
		dynasty = "Kruyd'ri"
		birth_date = 772.1.1
		adm = 4
		dip = 2
		mil = 0
		female = yes
    }
	heir = {
 		name = "La"
		monarch_name = "La I"
		dynasty = "Kruyd'ri"
		birth_date = 808.1.1
		death_date = 857.1.1
		claim = 100
		adm = 4
		dip = 6
		mil = 2
		female = yes
    }
}

857.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Neta"
		monarch_name = "Neta I"
		dynasty = "Chen'la"
		birth_date = 851.1.1
		death_date = 869.1.1
		claim = 100
		adm = 5
		dip = 3
		mil = 0
		female = yes
    }
}

869.1.1 = {
	monarch = {
 		name = "Cisaclol"
		dynasty = "Drustho'la"
		birth_date = 824.1.1
		adm = 3
		dip = 2
		mil = 3
		female = yes
    }
	queen = {
 		name = "Chaz"
		dynasty = "Drustho'la"
		birth_date = 824.1.1
		adm = 0
		dip = 1
		mil = 2
    }
	heir = {
 		name = "Funded"
		monarch_name = "Funded I"
		dynasty = "Drustho'la"
		birth_date = 857.1.1
		death_date = 912.1.1
		claim = 100
		adm = 4
		dip = 6
		mil = 1
    }
}

912.1.1 = {
	monarch = {
 		name = "Ked"
		dynasty = "Zohlad'ri"
		birth_date = 864.1.1
		adm = 1
		dip = 5
		mil = 0
    }
	queen = {
 		name = "Shrinetuk"
		dynasty = "Zohlad'ri"
		birth_date = 866.1.1
		adm = 5
		dip = 3
		mil = 6
		female = yes
    }
	heir = {
 		name = "Neta"
		monarch_name = "Neta I"
		dynasty = "Zohlad'ri"
		birth_date = 911.1.1
		death_date = 1001.1.1
		claim = 100
		adm = 1
		dip = 2
		mil = 5
		female = yes
    }
}

1001.1.1 = {
	monarch = {
 		name = "Rolum"
		dynasty = "Neta'wo"
		birth_date = 957.1.1
		adm = 4
		dip = 3
		mil = 0
    }
}

1071.1.1 = {
	monarch = {
 		name = "Settu"
		dynasty = "Dustu'ri"
		birth_date = 1020.1.1
		adm = 2
		dip = 2
		mil = 4
		female = yes
    }
}

1119.1.1 = {
	monarch = {
 		name = "Tehuntu"
		dynasty = "Gaduyd'ri"
		birth_date = 1089.1.1
		adm = 1
		dip = 2
		mil = 1
    }
}

1216.1.1 = {
	monarch = {
 		name = "Beme"
		dynasty = "De'la"
		birth_date = 1191.1.1
		adm = 3
		dip = 3
		mil = 2
		female = yes
    }
	queen = {
 		name = "Gur"
		dynasty = "De'la"
		birth_date = 1171.1.1
		adm = 3
		dip = 6
		mil = 3
    }
}

1283.1.1 = {
	monarch = {
 		name = "Chuhleyd"
		dynasty = "Lun'ri"
		birth_date = 1259.1.1
		adm = 4
		dip = 3
		mil = 1
    }
}

1350.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Fuzzaz"
		monarch_name = "Fuzzaz I"
		dynasty = "Sorove'su"
		birth_date = 1348.1.1
		death_date = 1366.1.1
		claim = 100
		adm = 4
		dip = 1
		mil = 6
    }
}

1366.1.1 = {
	monarch = {
 		name = "Shracmur"
		dynasty = "Qadun'su"
		birth_date = 1343.1.1
		adm = 0
		dip = 2
		mil = 1
		female = yes
    }
	queen = {
 		name = "Piak"
		dynasty = "Qadun'su"
		birth_date = 1328.1.1
		adm = 4
		dip = 0
		mil = 0
    }
	heir = {
 		name = "Wolcor"
		monarch_name = "Wolcor I"
		dynasty = "Qadun'su"
		birth_date = 1355.1.1
		death_date = 1446.1.1
		claim = 100
		adm = 1
		dip = 6
		mil = 6
		female = yes
    }
}

1446.1.1 = {
	monarch = {
 		name = "Lisurr"
		dynasty = "Dalnog'su"
		birth_date = 1423.1.1
		adm = 0
		dip = 2
		mil = 0
		female = yes
    }
	queen = {
 		name = "Fiestemo"
		dynasty = "Dalnog'su"
		birth_date = 1395.1.1
		adm = 4
		dip = 1
		mil = 6
    }
	heir = {
 		name = "Mag"
		monarch_name = "Mag I"
		dynasty = "Dalnog'su"
		birth_date = 1435.1.1
		death_date = 1508.1.1
		claim = 100
		adm = 4
		dip = 4
		mil = 0
    }
}

1508.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Mekaz"
		monarch_name = "Mekaz I"
		dynasty = "Shogi'la"
		birth_date = 1503.1.1
		death_date = 1521.1.1
		claim = 100
		adm = 6
		dip = 6
		mil = 1
    }
}

1521.1.1 = {
	monarch = {
 		name = "Voldar"
		dynasty = "Luhu'la"
		birth_date = 1478.1.1
		adm = 2
		dip = 0
		mil = 4
    }
	queen = {
 		name = "Munok"
		dynasty = "Luhu'la"
		birth_date = 1487.1.1
		adm = 6
		dip = 5
		mil = 3
		female = yes
    }
}

1609.1.1 = {
	monarch = {
 		name = "Bundrumu"
		dynasty = "Qachil'wo"
		birth_date = 1575.1.1
		adm = 4
		dip = 5
		mil = 6
    }
	queen = {
 		name = "Dur"
		dynasty = "Qachil'wo"
		birth_date = 1575.1.1
		adm = 1
		dip = 3
		mil = 5
		female = yes
    }
	heir = {
 		name = "Nahag"
		monarch_name = "Nahag I"
		dynasty = "Qachil'wo"
		birth_date = 1609.1.1
		death_date = 1692.1.1
		claim = 100
		adm = 5
		dip = 2
		mil = 4
    }
}

1692.1.1 = {
	monarch = {
 		name = "Domdondrul"
		dynasty = "Lisurr'la"
		birth_date = 1658.1.1
		adm = 1
		dip = 3
		mil = 0
    }
}

1736.1.1 = {
	monarch = {
 		name = "De"
		dynasty = "Kil'wo"
		birth_date = 1698.1.1
		adm = 6
		dip = 2
		mil = 3
    }
}

1789.1.1 = {
	monarch = {
 		name = "Sierrosde"
		dynasty = "Gaduyd'ri"
		birth_date = 1741.1.1
		adm = 1
		dip = 4
		mil = 5
		female = yes
    }
}

1867.1.1 = {
	monarch = {
 		name = "Pi"
		dynasty = "Kumdem'wo"
		birth_date = 1817.1.1
		adm = 6
		dip = 3
		mil = 1
		female = yes
    }
	queen = {
 		name = "Gon"
		dynasty = "Kumdem'wo"
		birth_date = 1845.1.1
		adm = 3
		dip = 1
		mil = 0
    }
	heir = {
 		name = "Lis"
		monarch_name = "Lis I"
		dynasty = "Kumdem'wo"
		birth_date = 1860.1.1
		death_date = 1904.1.1
		claim = 100
		adm = 3
		dip = 5
		mil = 1
		female = yes
    }
}

1904.1.1 = {
	monarch = {
 		name = "Suhorrur"
		dynasty = "Piak'su"
		birth_date = 1876.1.1
		adm = 2
		dip = 1
		mil = 2
    }
	queen = {
 		name = "Dohortus"
		dynasty = "Piak'su"
		birth_date = 1881.1.1
		adm = 6
		dip = 0
		mil = 1
		female = yes
    }
	heir = {
 		name = "Su"
		monarch_name = "Su I"
		dynasty = "Piak'su"
		birth_date = 1903.1.1
		death_date = 2000.1.1
		claim = 100
		adm = 3
		dip = 5
		mil = 0
    }
}

2000.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Tus"
		monarch_name = "Tus I"
		dynasty = "Les'ri"
		birth_date = 1996.1.1
		death_date = 2014.1.1
		claim = 100
		adm = 1
		dip = 4
		mil = 3
    }
}

2014.1.1 = {
	monarch = {
 		name = "Rolum"
		dynasty = "Sok'ri"
		birth_date = 1973.1.1
		adm = 4
		dip = 6
		mil = 6
    }
	queen = {
 		name = "Lisurr"
		dynasty = "Sok'ri"
		birth_date = 1982.1.1
		adm = 1
		dip = 4
		mil = 5
		female = yes
    }
	heir = {
 		name = "Sieherr"
		monarch_name = "Sieherr I"
		dynasty = "Sok'ri"
		birth_date = 2001.1.1
		death_date = 2090.1.1
		claim = 100
		adm = 5
		dip = 3
		mil = 4
		female = yes
    }
}

2090.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Pon"
		monarch_name = "Pon I"
		dynasty = "Dowentem'wo"
		birth_date = 2081.1.1
		death_date = 2099.1.1
		claim = 100
		adm = 3
		dip = 2
		mil = 0
    }
}

2099.1.1 = {
	monarch = {
 		name = "Pek"
		dynasty = "Dur'wo"
		birth_date = 2078.1.1
		adm = 3
		dip = 5
		mil = 0
    }
	queen = {
 		name = "Dis"
		dynasty = "Dur'wo"
		birth_date = 2056.1.1
		adm = 6
		dip = 4
		mil = 6
		female = yes
    }
	heir = {
 		name = "Pu"
		monarch_name = "Pu I"
		dynasty = "Dur'wo"
		birth_date = 2089.1.1
		death_date = 2193.1.1
		claim = 100
		adm = 0
		dip = 0
		mil = 0
    }
}

2193.1.1 = {
	monarch = {
 		name = "Ruvo"
		dynasty = "Susdevu'la"
		birth_date = 2160.1.1
		adm = 6
		dip = 4
		mil = 1
    }
}

2265.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Pon"
		monarch_name = "Pon I"
		dynasty = "Rilu'wo"
		birth_date = 2255.1.1
		death_date = 2273.1.1
		claim = 100
		adm = 0
		dip = 1
		mil = 6
    }
}

2273.1.1 = {
	monarch = {
 		name = "Livosu"
		dynasty = "Kusdere'su"
		birth_date = 2255.1.1
		adm = 3
		dip = 2
		mil = 1
    }
	queen = {
 		name = "Dis"
		dynasty = "Kusdere'su"
		birth_date = 2222.1.1
		adm = 6
		dip = 1
		mil = 0
		female = yes
    }
	heir = {
 		name = "Pu"
		monarch_name = "Pu I"
		dynasty = "Kusdere'su"
		birth_date = 2262.1.1
		death_date = 2358.1.1
		claim = 100
		adm = 3
		dip = 6
		mil = 6
    }
}

2358.1.1 = {
	monarch = {
 		name = "Remoz"
		dynasty = "Gakuveets'wo"
		birth_date = 2327.1.1
		adm = 6
		dip = 1
		mil = 1
    }
}

2451.1.1 = {
	monarch = {
 		name = "Dirskyts"
		dynasty = "Gon'ri"
		birth_date = 2421.1.1
		adm = 1
		dip = 2
		mil = 3
		female = yes
    }
}

