government = monarchy
government_rank = 1
mercantilism = 1
technology_group = northern_tg
religion = druidism
primary_culture = breton
capital = 1389

54.1.1 = {
	monarch = {
 		name = "Madanach"
		dynasty = "Laussac"
		birth_date = 6.1.1
		adm = 5
		dip = 0
		mil = 6
    }
	queen = {
 		name = "Alexia"
		dynasty = "Laussac"
		birth_date = 27.1.1
		adm = 0
		dip = 0
		mil = 3
		female = yes
    }
	heir = {
 		name = "Coyne"
		monarch_name = "Coyne I"
		dynasty = "Laussac"
		birth_date = 45.1.1
		death_date = 129.1.1
		claim = 100
		adm = 4
		dip = 6
		mil = 2
    }
}

129.1.1 = {
	monarch = {
 		name = "Ouida"
		dynasty = "Ashton"
		birth_date = 76.1.1
		adm = 3
		dip = 2
		mil = 3
		female = yes
    }
}

221.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Daighre"
		monarch_name = "Daighre I"
		dynasty = "Derre"
		birth_date = 214.1.1
		death_date = 232.1.1
		claim = 100
		adm = 0
		dip = 4
		mil = 3
		female = yes
    }
}

232.1.1 = {
	monarch = {
 		name = "Pyn"
		dynasty = "Brousseau"
		birth_date = 214.1.1
		adm = 6
		dip = 1
		mil = 3
		female = yes
    }
}

278.1.1 = {
	monarch = {
 		name = "Jesper"
		dynasty = "Menoit"
		birth_date = 234.1.1
		adm = 5
		dip = 0
		mil = 0
    }
}

315.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Jeanylle"
		monarch_name = "Jeanylle I"
		dynasty = "Brolus"
		birth_date = 315.1.1
		death_date = 333.1.1
		claim = 100
		adm = 5
		dip = 4
		mil = 4
		female = yes
    }
}

333.1.1 = {
	monarch = {
 		name = "Alinon"
		dynasty = "Ergend"
		birth_date = 305.1.1
		adm = 1
		dip = 5
		mil = 0
    }
	queen = {
 		name = "Helese"
		dynasty = "Ergend"
		birth_date = 287.1.1
		adm = 5
		dip = 4
		mil = 6
		female = yes
    }
	heir = {
 		name = "Yvon"
		monarch_name = "Yvon I"
		dynasty = "Ergend"
		birth_date = 329.1.1
		death_date = 406.1.1
		claim = 100
		adm = 2
		dip = 2
		mil = 5
		female = yes
    }
}

406.1.1 = {
	monarch = {
 		name = "Jernald"
		dynasty = "Agnan"
		birth_date = 370.1.1
		adm = 5
		dip = 4
		mil = 0
    }
	queen = {
 		name = "Ileana"
		dynasty = "Agnan"
		birth_date = 370.1.1
		adm = 2
		dip = 2
		mil = 6
		female = yes
    }
	heir = {
 		name = "Achibert"
		monarch_name = "Achibert I"
		dynasty = "Agnan"
		birth_date = 401.1.1
		death_date = 477.1.1
		claim = 100
		adm = 5
		dip = 1
		mil = 5
    }
}

477.1.1 = {
	monarch = {
 		name = "Alyze"
		dynasty = "Garscroft"
		birth_date = 435.1.1
		adm = 5
		dip = 4
		mil = 5
		female = yes
    }
}

568.1.1 = {
	monarch = {
 		name = "Elvyrie"
		dynasty = "Douare"
		birth_date = 543.1.1
		adm = 3
		dip = 3
		mil = 2
		female = yes
    }
	queen = {
 		name = "Mylo"
		dynasty = "Douare"
		birth_date = 543.1.1
		adm = 0
		dip = 2
		mil = 1
    }
}

633.1.1 = {
	monarch = {
 		name = "Magali"
		dynasty = "Thal"
		birth_date = 609.1.1
		adm = 5
		dip = 1
		mil = 5
		female = yes
    }
}

703.1.1 = {
	monarch = {
 		name = "Fenas"
		dynasty = "Augier"
		birth_date = 662.1.1
		adm = 3
		dip = 0
		mil = 1
    }
}

798.1.1 = {
	monarch = {
 		name = "Aurelie"
		dynasty = "Orinth"
		birth_date = 774.1.1
		adm = 2
		dip = 0
		mil = 5
		female = yes
    }
}

873.1.1 = {
	monarch = {
 		name = "Serge"
		dynasty = "Nalskin"
		birth_date = 820.1.1
		adm = 0
		dip = 6
		mil = 2
    }
}

949.1.1 = {
	monarch = {
 		name = "Leseph"
		dynasty = "Rye"
		birth_date = 929.1.1
		adm = 5
		dip = 5
		mil = 5
    }
	queen = {
 		name = "Laetitia"
		dynasty = "Rye"
		birth_date = 903.1.1
		adm = 2
		dip = 3
		mil = 4
		female = yes
    }
	heir = {
 		name = "Jorden"
		monarch_name = "Jorden I"
		dynasty = "Rye"
		birth_date = 939.1.1
		death_date = 1043.1.1
		claim = 100
		adm = 6
		dip = 2
		mil = 3
    }
}

1043.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Rochelle"
		monarch_name = "Rochelle I"
		dynasty = "Marquad"
		birth_date = 1037.1.1
		death_date = 1055.1.1
		claim = 100
		adm = 4
		dip = 1
		mil = 0
		female = yes
    }
}

1055.1.1 = {
	monarch = {
 		name = "Sarvith"
		dynasty = "Amadour"
		birth_date = 1011.1.1
		adm = 4
		dip = 5
		mil = 0
    }
	queen = {
 		name = "Osanna"
		dynasty = "Amadour"
		birth_date = 1012.1.1
		adm = 0
		dip = 3
		mil = 6
		female = yes
    }
	heir = {
 		name = "Erisia"
		monarch_name = "Erisia I"
		dynasty = "Amadour"
		birth_date = 1055.1.1
		death_date = 1115.1.1
		claim = 100
		adm = 1
		dip = 0
		mil = 0
		female = yes
    }
}

1115.1.1 = {
	monarch = {
 		name = "Sibellec"
		dynasty = "Stieve"
		birth_date = 1084.1.1
		adm = 0
		dip = 3
		mil = 0
		female = yes
    }
}

1206.1.1 = {
	monarch = {
 		name = "Marcellyne"
		dynasty = "Crowe"
		birth_date = 1158.1.1
		adm = 5
		dip = 2
		mil = 4
		female = yes
    }
}

1296.1.1 = {
	monarch = {
 		name = "Frelene"
		dynasty = "Carsitien"
		birth_date = 1254.1.1
		adm = 3
		dip = 1
		mil = 1
		female = yes
    }
	queen = {
 		name = "Peverel"
		dynasty = "Carsitien"
		birth_date = 1252.1.1
		adm = 0
		dip = 0
		mil = 0
    }
	heir = {
 		name = "Emoryan"
		monarch_name = "Emoryan I"
		dynasty = "Carsitien"
		birth_date = 1287.1.1
		death_date = 1390.1.1
		claim = 100
		adm = 4
		dip = 6
		mil = 6
    }
}

1390.1.1 = {
	monarch = {
 		name = "Staubin"
		dynasty = "Fenandre"
		birth_date = 1341.1.1
		adm = 0
		dip = 0
		mil = 1
    }
	queen = {
 		name = "Piernette"
		dynasty = "Fenandre"
		birth_date = 1352.1.1
		adm = 4
		dip = 5
		mil = 0
		female = yes
    }
	heir = {
 		name = "Eugenie"
		monarch_name = "Eugenie I"
		dynasty = "Fenandre"
		birth_date = 1378.1.1
		death_date = 1470.1.1
		claim = 100
		adm = 1
		dip = 4
		mil = 6
		female = yes
    }
}

1470.1.1 = {
	monarch = {
 		name = "Fonten"
		dynasty = "Talnarith"
		birth_date = 1440.1.1
		adm = 0
		dip = 0
		mil = 6
    }
	queen = {
 		name = "Edilitte"
		dynasty = "Talnarith"
		birth_date = 1424.1.1
		adm = 4
		dip = 6
		mil = 5
		female = yes
    }
	heir = {
 		name = "Rolbert"
		monarch_name = "Rolbert I"
		dynasty = "Talnarith"
		birth_date = 1470.1.1
		death_date = 1518.1.1
		claim = 100
		adm = 4
		dip = 2
		mil = 6
    }
}

1518.1.1 = {
	monarch = {
 		name = "Genvieve"
		dynasty = "Jeline"
		birth_date = 1480.1.1
		adm = 4
		dip = 6
		mil = 6
		female = yes
    }
}

1610.1.1 = {
	monarch = {
 		name = "Bernard"
		dynasty = "Antieve"
		birth_date = 1584.1.1
		adm = 2
		dip = 5
		mil = 3
    }
}

1678.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Arnoth"
		monarch_name = "Arnoth I"
		dynasty = "Lafont"
		birth_date = 1664.1.1
		death_date = 1682.1.1
		claim = 100
		adm = 3
		dip = 2
		mil = 1
    }
}

1682.1.1 = {
	monarch = {
 		name = "Marguerite"
		dynasty = "Sansone"
		birth_date = 1642.1.1
		adm = 5
		dip = 3
		mil = 3
		female = yes
    }
	queen = {
 		name = "Jorden"
		dynasty = "Sansone"
		birth_date = 1646.1.1
		adm = 2
		dip = 2
		mil = 2
    }
	heir = {
 		name = "Landal"
		monarch_name = "Landal I"
		dynasty = "Sansone"
		birth_date = 1670.1.1
		death_date = 1752.1.1
		claim = 100
		adm = 6
		dip = 0
		mil = 1
    }
}

1752.1.1 = {
	monarch = {
 		name = "Bernique"
		dynasty = "Luillier"
		birth_date = 1713.1.1
		adm = 5
		dip = 4
		mil = 2
		female = yes
    }
}

1798.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Madena"
		monarch_name = "Madena I"
		dynasty = "Teague"
		birth_date = 1797.1.1
		death_date = 1815.1.1
		claim = 100
		adm = 3
		dip = 6
		mil = 2
		female = yes
    }
}

1815.1.1 = {
	monarch = {
 		name = "Brucetus"
		dynasty = "Lelles"
		birth_date = 1787.1.1
		adm = 2
		dip = 2
		mil = 2
    }
}

1868.1.1 = {
	monarch = {
 		name = "Thibierry"
		dynasty = "Roreles"
		birth_date = 1836.1.1
		adm = 0
		dip = 1
		mil = 5
    }
}

1950.1.1 = {
	monarch = {
 		name = "Martinne"
		dynasty = "Justal"
		birth_date = 1918.1.1
		adm = 5
		dip = 1
		mil = 2
		female = yes
    }
}

2045.1.1 = {
	monarch = {
 		name = "Gastarge"
		dynasty = "Maviniele"
		birth_date = 1999.1.1
		adm = 4
		dip = 0
		mil = 6
    }
}

2137.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Falisse"
		monarch_name = "Falisse I"
		dynasty = "Vienne"
		birth_date = 2128.1.1
		death_date = 2146.1.1
		claim = 100
		adm = 4
		dip = 4
		mil = 4
		female = yes
    }
}

2146.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Auguste"
		monarch_name = "Auguste I"
		dynasty = "Lozieres"
		birth_date = 2145.1.1
		death_date = 2163.1.1
		claim = 100
		adm = 3
		dip = 3
		mil = 0
    }
}

2163.1.1 = {
	monarch = {
 		name = "Marthe"
		dynasty = "Mornardl"
		birth_date = 2118.1.1
		adm = 2
		dip = 0
		mil = 1
		female = yes
    }
}

2222.1.1 = {
	monarch = {
 		name = "Gascone"
		dynasty = "Urbyn"
		birth_date = 2174.1.1
		adm = 0
		dip = 6
		mil = 4
    }
}

2290.1.1 = {
	monarch = {
 		name = "Mercer"
		dynasty = "Litte"
		birth_date = 2237.1.1
		adm = 6
		dip = 5
		mil = 1
    }
	queen = {
 		name = "Annaïg"
		dynasty = "Litte"
		birth_date = 2261.1.1
		adm = 2
		dip = 3
		mil = 0
		female = yes
    }
	heir = {
 		name = "Lort"
		monarch_name = "Lort I"
		dynasty = "Litte"
		birth_date = 2280.1.1
		death_date = 2368.1.1
		claim = 100
		adm = 6
		dip = 2
		mil = 6
    }
}

2368.1.1 = {
	monarch = {
 		name = "Carlier"
		dynasty = "Ernarde"
		birth_date = 2332.1.1
		adm = 2
		dip = 3
		mil = 1
		female = yes
    }
}

2451.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Basilien"
		monarch_name = "Basilien I"
		dynasty = "Carme"
		birth_date = 2447.1.1
		death_date = 2465.1.1
		claim = 100
		adm = 3
		dip = 0
		mil = 6
    }
}

2465.1.1 = {
	monarch = {
 		name = "Mederic"
		dynasty = "Davaux"
		birth_date = 2421.1.1
		adm = 6
		dip = 2
		mil = 1
    }
}

