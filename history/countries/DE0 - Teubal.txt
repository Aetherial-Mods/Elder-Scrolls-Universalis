government = native
government_rank = 1
mercantilism = 1
technology_group = atmora_tg
religion = cult_of_ancestors
primary_culture = sea_giant
capital = 2506

54.1.1 = {
	monarch = {
 		name = "Sthurantos"
		dynasty = "Zoglag"
		birth_date = 15.1.1
		adm = 6
		dip = 6
		mil = 6
    }
}

120.1.1 = {
	monarch = {
 		name = "Risneidon"
		dynasty = "Kagram"
		birth_date = 69.1.1
		adm = 6
		dip = 2
		mil = 6
    }
}

185.1.1 = {
	monarch = {
 		name = "Chrorda"
		dynasty = "Luklox"
		birth_date = 148.1.1
		adm = 4
		dip = 2
		mil = 3
		female = yes
    }
}

277.1.1 = {
	monarch = {
 		name = "Hivius"
		dynasty = "Cratrog"
		birth_date = 229.1.1
		adm = 2
		dip = 1
		mil = 0
    }
}

354.1.1 = {
	monarch = {
 		name = "Stherea"
		dynasty = "Blashan"
		birth_date = 304.1.1
		adm = 0
		dip = 0
		mil = 3
		female = yes
    }
}

443.1.1 = {
	monarch = {
 		name = "Zonoeus"
		dynasty = "Sokros"
		birth_date = 420.1.1
		adm = 6
		dip = 6
		mil = 0
    }
}

503.1.1 = {
	monarch = {
 		name = "Doscias"
		dynasty = "Grizar"
		birth_date = 460.1.1
		adm = 4
		dip = 5
		mil = 4
    }
}

563.1.1 = {
	monarch = {
 		name = "Thesemia"
		dynasty = "Glerkosh"
		birth_date = 511.1.1
		adm = 6
		dip = 0
		mil = 5
		female = yes
    }
}

636.1.1 = {
	monarch = {
 		name = "Chunios"
		dynasty = "Lidrur"
		birth_date = 590.1.1
		adm = 4
		dip = 6
		mil = 2
    }
}

686.1.1 = {
	monarch = {
 		name = "Riderion"
		dynasty = "Jastes"
		birth_date = 655.1.1
		adm = 2
		dip = 5
		mil = 5
    }
}

772.1.1 = {
	monarch = {
 		name = "Chredno"
		dynasty = "Kagram"
		birth_date = 725.1.1
		adm = 0
		dip = 4
		mil = 2
		female = yes
    }
}

843.1.1 = {
	monarch = {
 		name = "Noser"
		dynasty = "Genras"
		birth_date = 816.1.1
		adm = 6
		dip = 3
		mil = 6
    }
}

942.1.1 = {
	monarch = {
 		name = "Sthava"
		dynasty = "Chutrix"
		birth_date = 891.1.1
		adm = 4
		dip = 3
		mil = 2
		female = yes
    }
}

1014.1.1 = {
	monarch = {
 		name = "Nedytion"
		dynasty = "Challor"
		birth_date = 989.1.1
		adm = 2
		dip = 2
		mil = 6
    }
}

1111.1.1 = {
	monarch = {
 		name = "Chynis"
		dynasty = "Markal"
		birth_date = 1065.1.1
		adm = 0
		dip = 1
		mil = 3
    }
}

1158.1.1 = {
	monarch = {
 		name = "Theribos"
		dynasty = "Groklus"
		birth_date = 1126.1.1
		adm = 2
		dip = 2
		mil = 4
    }
}

1208.1.1 = {
	monarch = {
 		name = "Mivaumas"
		dynasty = "Mendan"
		birth_date = 1177.1.1
		adm = 1
		dip = 2
		mil = 1
    }
}

1245.1.1 = {
	monarch = {
 		name = "Chistoeis"
		dynasty = "Glubdys"
		birth_date = 1199.1.1
		adm = 6
		dip = 1
		mil = 4
    }
}

1283.1.1 = {
	monarch = {
 		name = "Xaino"
		dynasty = "Zetril"
		birth_date = 1248.1.1
		adm = 4
		dip = 0
		mil = 1
		female = yes
    }
}

1330.1.1 = {
	monarch = {
 		name = "Nirys"
		dynasty = "Sokros"
		birth_date = 1297.1.1
		adm = 2
		dip = 6
		mil = 5
    }
}

1409.1.1 = {
	monarch = {
 		name = "Thusneus"
		dynasty = "Latho"
		birth_date = 1384.1.1
		adm = 0
		dip = 5
		mil = 1
    }
}

1453.1.1 = {
	monarch = {
 		name = "Thunthuna"
		dynasty = "Cratrog"
		birth_date = 1400.1.1
		adm = 6
		dip = 5
		mil = 5
		female = yes
    }
}

1528.1.1 = {
	monarch = {
 		name = "Dyamyrus"
		dynasty = "Hokuk"
		birth_date = 1487.1.1
		adm = 4
		dip = 4
		mil = 2
    }
}

1605.1.1 = {
	monarch = {
 		name = "Xascula"
		dynasty = "Lazur"
		birth_date = 1553.1.1
		adm = 6
		dip = 5
		mil = 3
		female = yes
    }
}

1692.1.1 = {
	monarch = {
 		name = "Hesy"
		dynasty = "Goki"
		birth_date = 1670.1.1
		adm = 4
		dip = 4
		mil = 0
		female = yes
    }
}

1791.1.1 = {
	monarch = {
 		name = "Phosdeas"
		dynasty = "Bleglil"
		birth_date = 1759.1.1
		adm = 2
		dip = 3
		mil = 4
    }
}

1862.1.1 = {
	monarch = {
 		name = "Chramenia"
		dynasty = "Challor"
		birth_date = 1827.1.1
		adm = 1
		dip = 3
		mil = 0
		female = yes
    }
}

1929.1.1 = {
	monarch = {
 		name = "Thanthaenon"
		dynasty = "Genras"
		birth_date = 1905.1.1
		adm = 6
		dip = 2
		mil = 4
    }
}

2027.1.1 = {
	monarch = {
 		name = "Phezytion"
		dynasty = "Kugnig"
		birth_date = 2001.1.1
		adm = 4
		dip = 1
		mil = 0
    }
}

2070.1.1 = {
	monarch = {
 		name = "Thusde"
		dynasty = "Miscik"
		birth_date = 2050.1.1
		adm = 2
		dip = 0
		mil = 4
		female = yes
    }
}

2108.1.1 = {
	monarch = {
 		name = "Kevantos"
		dynasty = "Gyngan"
		birth_date = 2078.1.1
		adm = 1
		dip = 6
		mil = 1
    }
}

2192.1.1 = {
	monarch = {
 		name = "Dimos"
		dynasty = "Jakral"
		birth_date = 2146.1.1
		adm = 2
		dip = 1
		mil = 2
    }
}

2285.1.1 = {
	monarch = {
 		name = "Derespo"
		dynasty = "Gezon"
		birth_date = 2265.1.1
		adm = 1
		dip = 0
		mil = 6
		female = yes
    }
}

2334.1.1 = {
	monarch = {
 		name = "Chrincor"
		dynasty = "Lotrin"
		birth_date = 2315.1.1
		adm = 6
		dip = 6
		mil = 3
    }
}

2417.1.1 = {
	monarch = {
 		name = "Zumbrimia"
		dynasty = "Sarak"
		birth_date = 2399.1.1
		adm = 4
		dip = 5
		mil = 6
		female = yes
    }
}

2470.1.1 = {
	monarch = {
 		name = "Xulotos"
		dynasty = "Gognosh"
		birth_date = 2443.1.1
		adm = 2
		dip = 5
		mil = 3
    }
}

