government = republic
government_rank = 1
mercantilism = 1
technology_group = cyrodiil_tg
religion = meridia_cult
primary_culture = nedic
capital = 6034

54.1.1 = {
	monarch = {
 		name = "Desylis"
		dynasty = "Nirdoc"
		birth_date = 19.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

92.1.1 = {
	monarch = {
 		name = "Dauhrak"
		dynasty = "Zunhe"
		birth_date = 68.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

187.1.1 = {
	monarch = {
 		name = "Ihrasek"
		dynasty = "Runhath"
		birth_date = 147.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

258.1.1 = {
	monarch = {
 		name = "Vrurlauleth"
		dynasty = "Ludec"
		birth_date = 214.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

314.1.1 = {
	monarch = {
 		name = "Nharmok"
		dynasty = "Tirleros"
		birth_date = 296.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

360.1.1 = {
	monarch = {
 		name = "Tirdor"
		dynasty = "Gumrok"
		birth_date = 314.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

441.1.1 = {
	monarch = {
 		name = "Vredraudin"
		dynasty = "Runhyrac"
		birth_date = 398.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

499.1.1 = {
	monarch = {
 		name = "Hyker"
		dynasty = "Amedoc"
		birth_date = 466.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

593.1.1 = {
	monarch = {
 		name = "Emah"
		dynasty = "Nauhrimak"
		birth_date = 559.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

643.1.1 = {
	monarch = {
 		name = "Tirdor"
		dynasty = "Vaulic"
		birth_date = 601.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

686.1.1 = {
	monarch = {
 		name = "Tome"
		dynasty = "Halaanis"
		birth_date = 654.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

761.1.1 = {
	monarch = {
 		name = "Zeli"
		dynasty = "Hetok"
		birth_date = 728.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

849.1.1 = {
	monarch = {
 		name = "Hynhaolen"
		dynasty = "Haonuvic"
		birth_date = 814.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

925.1.1 = {
	monarch = {
 		name = "Letan"
		dynasty = "Naanuzar"
		birth_date = 897.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

983.1.1 = {
	monarch = {
 		name = "Tazamaev"
		dynasty = "Ludec"
		birth_date = 941.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1067.1.1 = {
	monarch = {
 		name = "Teni"
		dynasty = "Shahaulec"
		birth_date = 1030.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1166.1.1 = {
	monarch = {
 		name = "Dennae"
		dynasty = "Zunhe"
		birth_date = 1136.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1238.1.1 = {
	monarch = {
 		name = "Demer"
		dynasty = "Hyker"
		birth_date = 1206.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1283.1.1 = {
	monarch = {
 		name = "Tazamaev"
		dynasty = "Nestith"
		birth_date = 1261.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1319.1.1 = {
	monarch = {
 		name = "Halaanis"
		dynasty = "Ridravin"
		birth_date = 1281.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1390.1.1 = {
	monarch = {
 		name = "Kaomrar"
		dynasty = "Nestith"
		birth_date = 1349.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1429.1.1 = {
	monarch = {
 		name = "Riralath"
		dynasty = "Muvmec"
		birth_date = 1393.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1507.1.1 = {
	monarch = {
 		name = "Zonemaer"
		dynasty = "Lysteth"
		birth_date = 1483.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1551.1.1 = {
	monarch = {
 		name = "Tylak"
		dynasty = "Tylak"
		birth_date = 1529.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1600.1.1 = {
	monarch = {
 		name = "Gilraoles"
		dynasty = "Gahlaras"
		birth_date = 1561.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1663.1.1 = {
	monarch = {
 		name = "Gahlaras"
		dynasty = "Dyrmec"
		birth_date = 1627.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1711.1.1 = {
	monarch = {
 		name = "Remaasok"
		dynasty = "Tirlaasir"
		birth_date = 1682.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1780.1.1 = {
	monarch = {
 		name = "Idith"
		dynasty = "Zurduzok"
		birth_date = 1742.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1872.1.1 = {
	monarch = {
 		name = "Havu"
		dynasty = "Menhadeth"
		birth_date = 1820.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1915.1.1 = {
	monarch = {
 		name = "Norri"
		dynasty = "Nhekhedin"
		birth_date = 1896.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1964.1.1 = {
	monarch = {
 		name = "Iran"
		dynasty = "Vrilak"
		birth_date = 1926.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

2012.1.1 = {
	monarch = {
 		name = "Terenu"
		dynasty = "Nharmok"
		birth_date = 1987.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

2073.1.1 = {
	monarch = {
 		name = "Tykisok"
		dynasty = "Idith"
		birth_date = 2049.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2132.1.1 = {
	monarch = {
 		name = "Gakaazin"
		dynasty = "Vredraonir"
		birth_date = 2112.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2228.1.1 = {
	monarch = {
 		name = "Vrilak"
		dynasty = "Kalrin"
		birth_date = 2205.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2303.1.1 = {
	monarch = {
 		name = "Gikamik"
		dynasty = "Tirleros"
		birth_date = 2277.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2387.1.1 = {
	monarch = {
 		name = "Tykisok"
		dynasty = "Dadrades"
		birth_date = 2335.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2474.1.1 = {
	monarch = {
 		name = "Gakaazin"
		dynasty = "Zevmin"
		birth_date = 2454.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

