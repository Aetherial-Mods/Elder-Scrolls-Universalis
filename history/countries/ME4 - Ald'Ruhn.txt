government = tribal
government_rank = 3
mercantilism = 1
technology_group = exiled_tg
religion = chimer_pantheon
primary_culture = ashlander_chimer
capital = 4263

54.1.1 = {
	monarch = {
 		name = "Kushishi"
		dynasty = "Yanumibaal"
		birth_date = 23.1.1
		adm = 4
		dip = 0
		mil = 5
		female = yes
    }
}

91.1.1 = {
	monarch = {
 		name = "Ashibaal"
		dynasty = "Shamirbasour"
		birth_date = 59.1.1
		adm = 2
		dip = 6
		mil = 1
    }
}

149.1.1 = {
	monarch = {
 		name = "Yassour"
		dynasty = "Yessur-Disadon"
		birth_date = 120.1.1
		adm = 0
		dip = 6
		mil = 5
    }
}

194.1.1 = {
	monarch = {
 		name = "Salay"
		dynasty = "Kummimmidan"
		birth_date = 152.1.1
		adm = 5
		dip = 5
		mil = 1
    }
}

267.1.1 = {
	monarch = {
 		name = "Kushishi"
		dynasty = "Mirpal"
		birth_date = 248.1.1
		adm = 0
		dip = 6
		mil = 3
		female = yes
    }
}

330.1.1 = {
	monarch = {
 		name = "Ashumanu"
		dynasty = "Ashummi-Ammus"
		birth_date = 281.1.1
		adm = 5
		dip = 5
		mil = 0
		female = yes
    }
}

388.1.1 = {
	monarch = {
 		name = "Kaushad"
		dynasty = "Assunudadnud"
		birth_date = 341.1.1
		adm = 4
		dip = 4
		mil = 3
    }
}

434.1.1 = {
	monarch = {
 		name = "Ashuma-Nud"
		dynasty = "Subaddamael"
		birth_date = 414.1.1
		adm = 2
		dip = 4
		mil = 0
    }
}

474.1.1 = {
	monarch = {
 		name = "Yan-Ahhe"
		dynasty = "Ududnabia"
		birth_date = 444.1.1
		adm = 0
		dip = 3
		mil = 4
		female = yes
    }
}

561.1.1 = {
	monarch = {
 		name = "Sannit"
		dynasty = "Saladnius"
		birth_date = 510.1.1
		adm = 5
		dip = 2
		mil = 0
    }
}

650.1.1 = {
	monarch = {
 		name = "Kashtes"
		dynasty = "Niladon"
		birth_date = 627.1.1
		adm = 4
		dip = 1
		mil = 4
    }
}

728.1.1 = {
	monarch = {
 		name = "Ashuma-Nud"
		dynasty = "Adur-Dan"
		birth_date = 709.1.1
		adm = 2
		dip = 0
		mil = 1
    }
}

764.1.1 = {
	monarch = {
 		name = "Yen"
		dynasty = "Assebiriddan"
		birth_date = 741.1.1
		adm = 4
		dip = 2
		mil = 2
    }
}

825.1.1 = {
	monarch = {
 		name = "Salmus"
		dynasty = "Kummimmidan"
		birth_date = 801.1.1
		adm = 2
		dip = 1
		mil = 6
    }
}

862.1.1 = {
	monarch = {
 		name = "Yanabani"
		dynasty = "Kaushad"
		birth_date = 841.1.1
		adm = 6
		dip = 3
		mil = 6
		female = yes
    }
}

958.1.1 = {
	monarch = {
 		name = "Mimanu"
		dynasty = "Dun-Ahhe"
		birth_date = 940.1.1
		adm = 1
		dip = 4
		mil = 0
		female = yes
    }
}

1049.1.1 = {
	monarch = {
 		name = "Hainab"
		dynasty = "Zabynatus"
		birth_date = 1028.1.1
		adm = 6
		dip = 3
		mil = 4
		female = yes
    }
}

1095.1.1 = {
	monarch = {
 		name = "Assallit"
		dynasty = "Niladon"
		birth_date = 1065.1.1
		adm = 2
		dip = 5
		mil = 6
    }
}

1173.1.1 = {
	monarch = {
 		name = "Yanabani"
		dynasty = "Akin"
		birth_date = 1142.1.1
		adm = 0
		dip = 4
		mil = 3
		female = yes
    }
}

1208.1.1 = {
	monarch = {
 		name = "Samsi"
		dynasty = "Yansurnummu"
		birth_date = 1173.1.1
		adm = 6
		dip = 3
		mil = 0
		female = yes
    }
}

1260.1.1 = {
	monarch = {
 		name = "Kausi"
		dynasty = "Sonnerralit"
		birth_date = 1216.1.1
		adm = 0
		dip = 4
		mil = 1
    }
}

1339.1.1 = {
	monarch = {
 		name = "Assaba-Bentus"
		dynasty = "Kaushad"
		birth_date = 1296.1.1
		adm = 6
		dip = 4
		mil = 5
    }
}

1418.1.1 = {
	monarch = {
 		name = "Mamusa"
		dynasty = "Shashipal"
		birth_date = 1366.1.1
		adm = 4
		dip = 3
		mil = 1
		female = yes
    }
}

1465.1.1 = {
	monarch = {
 		name = "Assamma-Idan"
		dynasty = "Varnan-Adda"
		birth_date = 1443.1.1
		adm = 2
		dip = 2
		mil = 5
    }
}

1515.1.1 = {
	monarch = {
 		name = "Yenabi"
		dynasty = "Shinirbael"
		birth_date = 1488.1.1
		adm = 0
		dip = 1
		mil = 2
		female = yes
    }
}

1558.1.1 = {
	monarch = {
 		name = "Shabinbael"
		dynasty = "Timmiriran"
		birth_date = 1523.1.1
		adm = 6
		dip = 0
		mil = 5
    }
}

1633.1.1 = {
	monarch = {
 		name = "Kummi-Namus"
		dynasty = "Selkin-Adda"
		birth_date = 1602.1.1
		adm = 4
		dip = 0
		mil = 2
    }
}

1696.1.1 = {
	monarch = {
 		name = "Assamma-Idan"
		dynasty = "Abelmawia"
		birth_date = 1649.1.1
		adm = 2
		dip = 6
		mil = 6
    }
}

1738.1.1 = {
	monarch = {
 		name = "Yanibi"
		dynasty = "Sobdishapal"
		birth_date = 1715.1.1
		adm = 4
		dip = 0
		mil = 0
		female = yes
    }
}

1799.1.1 = {
	monarch = {
 		name = "Selkirnemus"
		dynasty = "Assurnarairan"
		birth_date = 1774.1.1
		adm = 2
		dip = 6
		mil = 4
    }
}

1881.1.1 = {
	monarch = {
 		name = "Zaba"
		dynasty = "Eramarellaku"
		birth_date = 1832.1.1
		adm = 1
		dip = 6
		mil = 1
		female = yes
    }
}

1922.1.1 = {
	monarch = {
 		name = "Shanit"
		dynasty = "Ashun-Idantus"
		birth_date = 1874.1.1
		adm = 6
		dip = 5
		mil = 4
    }
}

1977.1.1 = {
	monarch = {
 		name = "Mabarrabael"
		dynasty = "Ashummi-Ammus"
		birth_date = 1929.1.1
		adm = 4
		dip = 4
		mil = 1
    }
}

2038.1.1 = {
	monarch = {
 		name = "Assemmus"
		dynasty = "Bael"
		birth_date = 2009.1.1
		adm = 2
		dip = 3
		mil = 4
    }
}

2134.1.1 = {
	monarch = {
 		name = "Zaba"
		dynasty = "Ashar-Don"
		birth_date = 2084.1.1
		adm = 0
		dip = 2
		mil = 1
		female = yes
    }
}

2228.1.1 = {
	monarch = {
 		name = "Sen"
		dynasty = "Assintashiran"
		birth_date = 2178.1.1
		adm = 6
		dip = 2
		mil = 5
		female = yes
    }
}

2280.1.1 = {
	monarch = {
 		name = "Lassour"
		dynasty = "Tibashipal"
		birth_date = 2256.1.1
		adm = 1
		dip = 3
		mil = 6
    }
}

2315.1.1 = {
	monarch = {
 		name = "Assatlit"
		dynasty = "Sehabani"
		birth_date = 2276.1.1
		adm = 6
		dip = 2
		mil = 3
    }
}

2403.1.1 = {
	monarch = {
 		name = "Malay"
		dynasty = "Kutebani"
		birth_date = 2353.1.1
		adm = 4
		dip = 1
		mil = 0
    }
}

2473.1.1 = {
	monarch = {
 		name = "Balur"
		dynasty = "Dunsamsi"
		birth_date = 2421.1.1
		adm = 2
		dip = 0
		mil = 3
    }
}

