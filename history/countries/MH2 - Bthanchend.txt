government = republic
government_rank = 1
mercantilism = 1
technology_group = dwemer_tg
religion = reason_and_logic_cult
primary_culture = dwemer
capital = 4304

54.1.1 = {
	monarch = {
 		name = "Jholzarf"
		dynasty = "Av'Mhunac"
		birth_date = 8.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

98.1.1 = {
	monarch = {
 		name = "Mhunac"
		dynasty = "Az'Ksrefurn"
		birth_date = 46.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

186.1.1 = {
	monarch = {
 		name = "Mhanrazg"
		dynasty = "Av'Sthomin"
		birth_date = 138.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

279.1.1 = {
	monarch = {
 		name = "Ghaznak"
		dynasty = "Az'Yzranrida"
		birth_date = 228.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

335.1.1 = {
	monarch = {
 		name = "Dzreglan"
		dynasty = "Av'Dhamuard"
		birth_date = 315.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

384.1.1 = {
	monarch = {
 		name = "Szoglynsh"
		dynasty = "Aq'Badzach"
		birth_date = 352.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

464.1.1 = {
	monarch = {
 		name = "Cfradrys"
		dynasty = "Af'Kolzarf"
		birth_date = 429.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

516.1.1 = {
	monarch = {
 		name = "Ghaznak"
		dynasty = "Af'Chruhnch"
		birth_date = 483.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

587.1.1 = {
	monarch = {
 		name = "Agahuanch"
		dynasty = "Aq'Czoudrys"
		birth_date = 560.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

638.1.1 = {
	monarch = {
 		name = "Tzenruz"
		dynasty = "Az'Jozlen"
		birth_date = 603.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

676.1.1 = {
	monarch = {
 		name = "Achygrac"
		dynasty = "Av'Snelarn"
		birth_date = 644.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

771.1.1 = {
	monarch = {
 		name = "Ksrefurn"
		dynasty = "Af'Nebgar"
		birth_date = 743.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

822.1.1 = {
	monarch = {
 		name = "Somzlin"
		dynasty = "Af'Szogarn"
		birth_date = 804.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

907.1.1 = {
	monarch = {
 		name = "Byrahken"
		dynasty = "Av'Chrudras"
		birth_date = 869.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

991.1.1 = {
	monarch = {
 		name = "Churd"
		dynasty = "Av'Rafnyg"
		birth_date = 941.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1048.1.1 = {
	monarch = {
 		name = "Cfranhatch"
		dynasty = "Aq'Cfradrys"
		birth_date = 1020.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1107.1.1 = {
	monarch = {
 		name = "Miban"
		dynasty = "Af'Ralarn"
		birth_date = 1072.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1163.1.1 = {
	monarch = {
 		name = "Rafnyg"
		dynasty = "Aq'Szozchyn"
		birth_date = 1114.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1232.1.1 = {
	monarch = {
 		name = "Churd"
		dynasty = "Av'Jnathunch"
		birth_date = 1214.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1289.1.1 = {
	monarch = {
 		name = "Asrahuanch"
		dynasty = "Af'Agahuanch"
		birth_date = 1239.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1328.1.1 = {
	monarch = {
 		name = "Mrobwarn"
		dynasty = "Av'Talchanf"
		birth_date = 1302.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1379.1.1 = {
	monarch = {
 		name = "Djubchasz"
		dynasty = "Aq'Rlovrin"
		birth_date = 1352.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1418.1.1 = {
	monarch = {
 		name = "Ythamac"
		dynasty = "Aq'Asradlin"
		birth_date = 1387.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1509.1.1 = {
	monarch = {
 		name = "Badzach"
		dynasty = "Aq'Kamzlin"
		birth_date = 1480.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1607.1.1 = {
	monarch = {
 		name = "Drertes"
		dynasty = "Av'Yhnazdir"
		birth_date = 1554.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1696.1.1 = {
	monarch = {
 		name = "Batvar"
		dynasty = "Aq'Jlethurzch"
		birth_date = 1664.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1759.1.1 = {
	monarch = {
 		name = "Ychogarn"
		dynasty = "Av'Ishevnorz"
		birth_date = 1740.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1852.1.1 = {
	monarch = {
 		name = "Jhouvin"
		dynasty = "Af'Inrarlakch"
		birth_date = 1827.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1928.1.1 = {
	monarch = {
 		name = "Brehron"
		dynasty = "Af'Sthord"
		birth_date = 1907.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1994.1.1 = {
	monarch = {
 		name = "Grigarn"
		dynasty = "Av'Ihlefuan"
		birth_date = 1975.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

2078.1.1 = {
	monarch = {
 		name = "Nromratz"
		dynasty = "Av'Djubchasz"
		birth_date = 2054.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

2117.1.1 = {
	monarch = {
 		name = "Jrudit"
		dynasty = "Aq'Szozchyn"
		birth_date = 2084.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2170.1.1 = {
	monarch = {
 		name = "Ishevnorz"
		dynasty = "Av'Tarlatz"
		birth_date = 2141.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2219.1.1 = {
	monarch = {
 		name = "Yhnazzefk"
		dynasty = "Av'Mzaglan"
		birth_date = 2198.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2297.1.1 = {
	monarch = {
 		name = "Ynzatarn"
		dynasty = "Az'Gzovnorz"
		birth_date = 2252.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2359.1.1 = {
	monarch = {
 		name = "Sthomin"
		dynasty = "Af'Chzevragch"
		birth_date = 2324.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2400.1.1 = {
	monarch = {
 		name = "Chzemgunch"
		dynasty = "Aq'Chzegrenz"
		birth_date = 2350.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2491.1.1 = {
	monarch = {
 		name = "Rafk"
		dynasty = "Az'Kridhis"
		birth_date = 2440.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

