government = monarchy
government_rank = 1
mercantilism = 1
technology_group = northern_tg
religion = nordic_pantheon
primary_culture = nord
capital = 7288
elector = yes

54.1.1 = {
	monarch = {
 		name = "Nelkir"
		dynasty = "Gagrer"
		birth_date = 27.1.1
		adm = 2
		dip = 1
		mil = 2
    }
}

136.1.1 = {
	monarch = {
 		name = "Vald"
		dynasty = "Kosner"
		birth_date = 113.1.1
		adm = 0
		dip = 0
		mil = 5
    }
	queen = {
 		name = "Garfrost"
		dynasty = "Kosner"
		birth_date = 109.1.1
		adm = 4
		dip = 5
		mil = 4
		female = yes
    }
	heir = {
 		name = "Thorygg"
		monarch_name = "Thorygg I"
		dynasty = "Kosner"
		birth_date = 123.1.1
		death_date = 232.1.1
		claim = 100
		adm = 1
		dip = 4
		mil = 3
    }
}

232.1.1 = {
	monarch = {
 		name = "Holvur"
		dynasty = "Klolsgonl"
		birth_date = 189.1.1
		adm = 4
		dip = 5
		mil = 6
    }
	queen = {
 		name = "Rudagalfa"
		dynasty = "Klolsgonl"
		birth_date = 197.1.1
		adm = 1
		dip = 4
		mil = 4
		female = yes
    }
}

307.1.1 = {
	monarch = {
 		name = "Rigurt"
		dynasty = "Thelmyn"
		birth_date = 284.1.1
		adm = 6
		dip = 3
		mil = 1
    }
	queen = {
 		name = "Bodil"
		dynasty = "Thelmyn"
		birth_date = 264.1.1
		adm = 3
		dip = 1
		mil = 0
		female = yes
    }
	heir = {
 		name = "Odmi"
		monarch_name = "Odmi I"
		dynasty = "Thelmyn"
		birth_date = 303.1.1
		death_date = 362.1.1
		claim = 100
		adm = 0
		dip = 0
		mil = 6
    }
}

362.1.1 = {
	monarch = {
 		name = "Fens"
		dynasty = "Frargil"
		birth_date = 342.1.1
		adm = 2
		dip = 1
		mil = 1
    }
	queen = {
 		name = "Brecir"
		dynasty = "Frargil"
		birth_date = 315.1.1
		adm = 6
		dip = 0
		mil = 0
		female = yes
    }
	heir = {
 		name = "Daric"
		monarch_name = "Daric I"
		dynasty = "Frargil"
		birth_date = 355.1.1
		death_date = 418.1.1
		claim = 100
		adm = 3
		dip = 5
		mil = 6
    }
}

418.1.1 = {
	monarch = {
 		name = "Nelmryn"
		dynasty = "Frethold"
		birth_date = 373.1.1
		adm = 3
		dip = 2
		mil = 0
		female = yes
    }
}

476.1.1 = {
	monarch = {
 		name = "Holdilf"
		dynasty = "Tiwon"
		birth_date = 435.1.1
		adm = 1
		dip = 1
		mil = 3
		female = yes
    }
}

555.1.1 = {
	monarch = {
 		name = "Odvar"
		dynasty = "Valben"
		birth_date = 511.1.1
		adm = 6
		dip = 0
		mil = 0
		female = yes
    }
	queen = {
 		name = "Bren"
		dynasty = "Valben"
		birth_date = 504.1.1
		adm = 3
		dip = 6
		mil = 6
    }
}

614.1.1 = {
	monarch = {
 		name = "Amihild"
		dynasty = "Svugjis"
		birth_date = 565.1.1
		adm = 1
		dip = 5
		mil = 3
		female = yes
    }
	queen = {
 		name = "Hroar"
		dynasty = "Svugjis"
		birth_date = 569.1.1
		adm = 5
		dip = 4
		mil = 1
    }
}

701.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Kluwe"
		monarch_name = "Kluwe I"
		dynasty = "Dormam"
		birth_date = 701.1.1
		death_date = 719.1.1
		claim = 100
		adm = 2
		dip = 6
		mil = 2
    }
}

719.1.1 = {
	monarch = {
 		name = "Berfar"
		dynasty = "Bralg"
		birth_date = 699.1.1
		adm = 2
		dip = 2
		mil = 2
    }
}

758.1.1 = {
	monarch = {
 		name = "Thanethen"
		dynasty = "Klytmor"
		birth_date = 719.1.1
		adm = 0
		dip = 1
		mil = 5
    }
	queen = {
 		name = "Fairynn"
		dynasty = "Klytmor"
		birth_date = 739.1.1
		adm = 4
		dip = 0
		mil = 4
		female = yes
    }
	heir = {
 		name = "Resrytte"
		monarch_name = "Resrytte I"
		dynasty = "Klytmor"
		birth_date = 751.1.1
		death_date = 833.1.1
		claim = 100
		adm = 1
		dip = 5
		mil = 3
		female = yes
    }
}

833.1.1 = {
	monarch = {
 		name = "Hanse"
		dynasty = "Brodrunlyn"
		birth_date = 794.1.1
		adm = 3
		dip = 0
		mil = 6
    }
	queen = {
 		name = "Fjorna"
		dynasty = "Brodrunlyn"
		birth_date = 789.1.1
		adm = 0
		dip = 5
		mil = 5
		female = yes
    }
}

880.1.1 = {
	monarch = {
 		name = "Nedasilf"
		dynasty = "Ratmam"
		birth_date = 862.1.1
		adm = 5
		dip = 4
		mil = 1
    }
	queen = {
 		name = "Jakild"
		dynasty = "Ratmam"
		birth_date = 851.1.1
		adm = 2
		dip = 3
		mil = 0
		female = yes
    }
	heir = {
 		name = "Kari"
		monarch_name = "Kari I"
		dynasty = "Ratmam"
		birth_date = 878.1.1
		death_date = 941.1.1
		claim = 100
		adm = 6
		dip = 1
		mil = 6
		female = yes
    }
}

941.1.1 = {
	monarch = {
 		name = "Bryngrim"
		dynasty = "Bolbein"
		birth_date = 894.1.1
		adm = 6
		dip = 5
		mil = 0
    }
	queen = {
 		name = "Araki"
		dynasty = "Bolbein"
		birth_date = 919.1.1
		adm = 6
		dip = 1
		mil = 1
		female = yes
    }
}

979.1.1 = {
	monarch = {
 		name = "Hjaralda"
		dynasty = "Thangianl"
		birth_date = 949.1.1
		adm = 1
		dip = 3
		mil = 2
		female = yes
    }
	queen = {
 		name = "Addvar"
		dynasty = "Thangianl"
		birth_date = 941.1.1
		adm = 2
		dip = 3
		mil = 0
    }
	heir = {
 		name = "Kragfarr"
		monarch_name = "Kragfarr I"
		dynasty = "Thangianl"
		birth_date = 964.1.1
		death_date = 1030.1.1
		claim = 100
		adm = 6
		dip = 2
		mil = 6
    }
}

1030.1.1 = {
	monarch = {
 		name = "Bofesar"
		dynasty = "Sarynd"
		birth_date = 983.1.1
		adm = 2
		dip = 3
		mil = 1
    }
	queen = {
 		name = "Hyfta"
		dynasty = "Sarynd"
		birth_date = 978.1.1
		adm = 6
		dip = 2
		mil = 0
		female = yes
    }
	heir = {
 		name = "Asgeir"
		monarch_name = "Asgeir I"
		dynasty = "Sarynd"
		birth_date = 1025.1.1
		death_date = 1102.1.1
		claim = 100
		adm = 2
		dip = 0
		mil = 6
    }
}

1102.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Rolunda"
		monarch_name = "Rolunda I"
		dynasty = "Kirkom"
		birth_date = 1089.1.1
		death_date = 1107.1.1
		claim = 100
		adm = 1
		dip = 6
		mil = 3
		female = yes
    }
}

1107.1.1 = {
	monarch = {
 		name = "Grunirra"
		dynasty = "Svedryl"
		birth_date = 1067.1.1
		adm = 0
		dip = 3
		mil = 3
		female = yes
    }
	queen = {
 		name = "Girunn"
		dynasty = "Svedryl"
		birth_date = 1066.1.1
		adm = 0
		dip = 6
		mil = 4
    }
	heir = {
 		name = "Shannia"
		monarch_name = "Shannia I"
		dynasty = "Svedryl"
		birth_date = 1103.1.1
		death_date = 1183.1.1
		claim = 100
		adm = 4
		dip = 5
		mil = 3
		female = yes
    }
}

1183.1.1 = {
	monarch = {
 		name = "Hil"
		dynasty = "Keld"
		birth_date = 1136.1.1
		adm = 4
		dip = 1
		mil = 3
    }
}

1278.1.1 = {
	monarch = {
 		name = "Brauti"
		dynasty = "Dyrulleim"
		birth_date = 1259.1.1
		adm = 2
		dip = 0
		mil = 0
    }
	queen = {
 		name = "Imsin"
		dynasty = "Dyrulleim"
		birth_date = 1230.1.1
		adm = 6
		dip = 6
		mil = 6
		female = yes
    }
	heir = {
 		name = "Banthor"
		monarch_name = "Banthor I"
		dynasty = "Dyrulleim"
		birth_date = 1278.1.1
		death_date = 1337.1.1
		claim = 100
		adm = 3
		dip = 4
		mil = 5
    }
}

1337.1.1 = {
	monarch = {
 		name = "Maul"
		dynasty = "Frildiafol"
		birth_date = 1288.1.1
		adm = 5
		dip = 6
		mil = 0
    }
	queen = {
 		name = "Agila"
		dynasty = "Frildiafol"
		birth_date = 1315.1.1
		adm = 2
		dip = 4
		mil = 6
		female = yes
    }
	heir = {
 		name = "Leifur"
		monarch_name = "Leifur I"
		dynasty = "Frildiafol"
		birth_date = 1326.1.1
		death_date = 1395.1.1
		claim = 100
		adm = 6
		dip = 3
		mil = 5
    }
}

1395.1.1 = {
	monarch = {
 		name = "Bottild"
		dynasty = "Kosner"
		birth_date = 1366.1.1
		adm = 2
		dip = 4
		mil = 1
    }
}

1452.1.1 = {
	monarch = {
 		name = "Svanhildr"
		dynasty = "Kosnorr"
		birth_date = 1404.1.1
		adm = 4
		dip = 5
		mil = 2
		female = yes
    }
}

1499.1.1 = {
	monarch = {
 		name = "Mathmyar"
		dynasty = "Tewam"
		birth_date = 1462.1.1
		adm = 2
		dip = 5
		mil = 6
    }
}

1598.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Beirand"
		monarch_name = "Beirand I"
		dynasty = "Tewam"
		birth_date = 1589.1.1
		death_date = 1607.1.1
		claim = 100
		adm = 6
		dip = 0
		mil = 6
    }
}

1607.1.1 = {
	monarch = {
 		name = "Lis"
		dynasty = "Skodnild"
		birth_date = 1574.1.1
		adm = 5
		dip = 3
		mil = 6
		female = yes
    }
	queen = {
 		name = "Alofnorr"
		dynasty = "Skodnild"
		birth_date = 1583.1.1
		adm = 2
		dip = 2
		mil = 5
    }
	heir = {
 		name = "Kar"
		monarch_name = "Kar I"
		dynasty = "Skodnild"
		birth_date = 1604.1.1
		death_date = 1701.1.1
		claim = 100
		adm = 6
		dip = 0
		mil = 4
		female = yes
    }
}

1701.1.1 = {
	monarch = {
 		name = "Brima"
		dynasty = "Vunheldys"
		birth_date = 1682.1.1
		adm = 2
		dip = 1
		mil = 6
		female = yes
    }
}

1794.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Bedraflod"
		monarch_name = "Bedraflod I"
		dynasty = "Vunheldys"
		birth_date = 1789.1.1
		death_date = 1807.1.1
		claim = 100
		adm = 3
		dip = 6
		mil = 4
    }
}

1807.1.1 = {
	monarch = {
 		name = "Msirae"
		dynasty = "Hagrudr"
		birth_date = 1782.1.1
		adm = 5
		dip = 0
		mil = 0
    }
	queen = {
 		name = "Jafola"
		dynasty = "Hagrudr"
		birth_date = 1757.1.1
		adm = 2
		dip = 5
		mil = 6
		female = yes
    }
}

1862.1.1 = {
	monarch = {
 		name = "Vigge"
		dynasty = "Gildaldr"
		birth_date = 1817.1.1
		adm = 2
		dip = 2
		mil = 6
    }
	queen = {
 		name = "Huki"
		dynasty = "Gildaldr"
		birth_date = 1842.1.1
		adm = 6
		dip = 0
		mil = 5
		female = yes
    }
	heir = {
 		name = "Aron"
		monarch_name = "Aron I"
		dynasty = "Gildaldr"
		birth_date = 1851.1.1
		death_date = 1961.1.1
		claim = 100
		adm = 2
		dip = 6
		mil = 4
    }
}

1961.1.1 = {
	monarch = {
 		name = "Lygrleid"
		dynasty = "Hlogren"
		birth_date = 1927.1.1
		adm = 5
		dip = 0
		mil = 6
    }
}

2052.1.1 = {
	monarch = {
 		name = "Hatunn"
		dynasty = "Feld"
		birth_date = 2031.1.1
		adm = 3
		dip = 6
		mil = 3
    }
	queen = {
 		name = "Okula"
		dynasty = "Feld"
		birth_date = 2019.1.1
		adm = 0
		dip = 5
		mil = 2
		female = yes
    }
	heir = {
 		name = "Gromjolf"
		monarch_name = "Gromjolf I"
		dynasty = "Feld"
		birth_date = 2037.1.1
		death_date = 2151.1.1
		claim = 100
		adm = 4
		dip = 4
		mil = 1
    }
}

2151.1.1 = {
	monarch = {
 		name = "Thonar"
		dynasty = "Vunheldys"
		birth_date = 2121.1.1
		adm = 0
		dip = 5
		mil = 3
    }
	queen = {
 		name = "Othvild"
		dynasty = "Vunheldys"
		birth_date = 2116.1.1
		adm = 4
		dip = 3
		mil = 2
		female = yes
    }
	heir = {
 		name = "Hadmal"
		monarch_name = "Hadmal I"
		dynasty = "Vunheldys"
		birth_date = 2144.1.1
		death_date = 2240.1.1
		claim = 100
		adm = 1
		dip = 2
		mil = 1
    }
}

2240.1.1 = {
	monarch = {
 		name = "Harrald"
		dynasty = "Tiwon"
		birth_date = 2196.1.1
		adm = 0
		dip = 5
		mil = 1
    }
	queen = {
 		name = "Fralia"
		dynasty = "Tiwon"
		birth_date = 2216.1.1
		adm = 4
		dip = 4
		mil = 0
		female = yes
    }
}

2326.1.1 = {
	monarch = {
 		name = "Nielstig"
		dynasty = "Told"
		birth_date = 2277.1.1
		adm = 2
		dip = 3
		mil = 4
    }
	queen = {
 		name = "Jareen"
		dynasty = "Told"
		birth_date = 2276.1.1
		adm = 6
		dip = 2
		mil = 3
		female = yes
    }
	heir = {
 		name = "Bonorvir"
		monarch_name = "Bonorvir I"
		dynasty = "Told"
		birth_date = 2322.1.1
		death_date = 2361.1.1
		claim = 100
		adm = 6
		dip = 5
		mil = 4
		female = yes
    }
}

2361.1.1 = {
	monarch = {
 		name = "Olav"
		dynasty = "Skitgerr"
		birth_date = 2334.1.1
		adm = 6
		dip = 1
		mil = 4
    }
	queen = {
 		name = "Arva"
		dynasty = "Skitgerr"
		birth_date = 2309.1.1
		adm = 2
		dip = 0
		mil = 3
		female = yes
    }
	heir = {
 		name = "Lafka"
		monarch_name = "Lafka I"
		dynasty = "Skitgerr"
		birth_date = 2360.1.1
		death_date = 2396.1.1
		claim = 100
		adm = 6
		dip = 6
		mil = 2
		female = yes
    }
}

2396.1.1 = {
	monarch = {
 		name = "Dogun"
		dynasty = "Feegnerr"
		birth_date = 2368.1.1
		adm = 2
		dip = 0
		mil = 4
    }
	queen = {
 		name = "Jalrana"
		dynasty = "Feegnerr"
		birth_date = 2371.1.1
		adm = 6
		dip = 5
		mil = 3
		female = yes
    }
	heir = {
 		name = "Borjs"
		monarch_name = "Borjs I"
		dynasty = "Feegnerr"
		birth_date = 2386.1.1
		death_date = 2475.1.1
		claim = 100
		adm = 3
		dip = 4
		mil = 2
    }
}

2475.1.1 = {
	monarch = {
 		name = "Ogmund"
		dynasty = "Thrivedraldr"
		birth_date = 2448.1.1
		adm = 6
		dip = 5
		mil = 5
    }
}

