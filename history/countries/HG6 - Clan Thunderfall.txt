government = tribal
government_rank = 1
mercantilism = 1
technology_group = orsimer_tg
religion = malacath_cult
primary_culture = iron_orc
capital = 6251

54.1.1 = {
	monarch = {
 		name = "Ogumalg"
		dynasty = "gro-Grashbag"
		birth_date = 15.1.1
		adm = 5
		dip = 5
		mil = 4
    }
}

133.1.1 = {
	monarch = {
 		name = "Durgat"
		dynasty = "gro-Farbalg"
		birth_date = 115.1.1
		adm = 1
		dip = 6
		mil = 6
		female = yes
    }
}

186.1.1 = {
	monarch = {
 		name = "Sharbarga"
		dynasty = "gro-Goorgul"
		birth_date = 146.1.1
		adm = 2
		dip = 3
		mil = 4
		female = yes
    }
}

278.1.1 = {
	monarch = {
 		name = "Ulghesh"
		dynasty = "gro-Mogrul"
		birth_date = 253.1.1
		adm = 0
		dip = 3
		mil = 1
    }
}

344.1.1 = {
	monarch = {
 		name = "Murboga"
		dynasty = "gro-Bashnag"
		birth_date = 297.1.1
		adm = 5
		dip = 2
		mil = 5
		female = yes
    }
}

416.1.1 = {
	monarch = {
 		name = "Lazdutha"
		dynasty = "gro-Usnagikh"
		birth_date = 368.1.1
		adm = 5
		dip = 5
		mil = 5
		female = yes
    }
}

507.1.1 = {
	monarch = {
 		name = "Carzog"
		dynasty = "gro-Murdodosh"
		birth_date = 487.1.1
		adm = 3
		dip = 4
		mil = 2
    }
}

592.1.1 = {
	monarch = {
 		name = "Bazgulub"
		dynasty = "gro-Yggnast"
		birth_date = 564.1.1
		adm = 1
		dip = 4
		mil = 5
    }
}

657.1.1 = {
	monarch = {
 		name = "Snarbugag"
		dynasty = "gro-Nakhul"
		birth_date = 609.1.1
		adm = 6
		dip = 3
		mil = 2
    }
}

719.1.1 = {
	monarch = {
 		name = "Lashgurgol"
		dynasty = "gro-Oodeg"
		birth_date = 701.1.1
		adm = 5
		dip = 2
		mil = 5
		female = yes
    }
}

770.1.1 = {
	monarch = {
 		name = "Goragol"
		dynasty = "gro-Monru"
		birth_date = 732.1.1
		adm = 3
		dip = 1
		mil = 2
    }
}

865.1.1 = {
	monarch = {
 		name = "Bashnag"
		dynasty = "gro-Ghorlorz"
		birth_date = 840.1.1
		adm = 5
		dip = 3
		mil = 4
    }
}

922.1.1 = {
	monarch = {
 		name = "Sharuk"
		dynasty = "gro-Bulugbek"
		birth_date = 894.1.1
		adm = 3
		dip = 2
		mil = 0
		female = yes
    }
}

998.1.1 = {
	monarch = {
 		name = "Bognash"
		dynasty = "gro-Yozth"
		birth_date = 971.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1036.1.1 = {
	monarch = {
 		name = "Snushbat"
		dynasty = "gro-Smagg"
		birth_date = 993.1.1
		adm = 0
		dip = 0
		mil = 1
    }
}

1117.1.1 = {
	monarch = {
 		name = "Lokra"
		dynasty = "gro-Ghorbash"
		birth_date = 1079.1.1
		adm = 5
		dip = 6
		mil = 4
		female = yes
    }
}

1166.1.1 = {
	monarch = {
 		name = "Gorzesh"
		dynasty = "gro-Dumag"
		birth_date = 1143.1.1
		adm = 3
		dip = 6
		mil = 1
    }
}

1227.1.1 = {
	monarch = {
 		name = "Bogakh"
		dynasty = "gro-Thagbruth"
		birth_date = 1199.1.1
		adm = 1
		dip = 5
		mil = 5
    }
}

1296.1.1 = {
	monarch = {
 		name = "Shufgrut"
		dynasty = "gro-Murdodosh"
		birth_date = 1253.1.1
		adm = 0
		dip = 4
		mil = 1
		female = yes
    }
}

1376.1.1 = {
	monarch = {
 		name = "Loglorag"
		dynasty = "gro-Yggnast"
		birth_date = 1325.1.1
		adm = 1
		dip = 5
		mil = 3
		female = yes
    }
}

1459.1.1 = {
	monarch = {
 		name = "Dumoga"
		dynasty = "gro-Shuzug"
		birth_date = 1423.1.1
		adm = 0
		dip = 4
		mil = 6
		female = yes
    }
}

1533.1.1 = {
	monarch = {
 		name = "Murzog"
		dynasty = "gro-Osgulug"
		birth_date = 1490.1.1
		adm = 5
		dip = 4
		mil = 3
    }
}

1620.1.1 = {
	monarch = {
 		name = "Gahgra"
		dynasty = "gro-Thagam"
		birth_date = 1568.1.1
		adm = 3
		dip = 3
		mil = 0
		female = yes
    }
}

1683.1.1 = {
	monarch = {
 		name = "Borgh"
		dynasty = "gro-Gorrath"
		birth_date = 1660.1.1
		adm = 1
		dip = 2
		mil = 3
    }
}

1758.1.1 = {
	monarch = {
 		name = "Thagam"
		dynasty = "gro-Gruzgob"
		birth_date = 1707.1.1
		adm = 0
		dip = 1
		mil = 0
    }
}

1842.1.1 = {
	monarch = {
 		name = "Lulgra"
		dynasty = "gro-Thoogh"
		birth_date = 1800.1.1
		adm = 5
		dip = 1
		mil = 4
		female = yes
    }
}

1907.1.1 = {
	monarch = {
 		name = "Gravik"
		dynasty = "gro-Snakha"
		birth_date = 1868.1.1
		adm = 3
		dip = 0
		mil = 0
    }
}

1956.1.1 = {
	monarch = {
 		name = "Shelur"
		dynasty = "gro-Nagrul"
		birth_date = 1912.1.1
		adm = 5
		dip = 1
		mil = 2
		female = yes
    }
}

2026.1.1 = {
	monarch = {
 		name = "Tazgol"
		dynasty = "gro-Muzkul"
		birth_date = 1995.1.1
		adm = 3
		dip = 0
		mil = 5
    }
}

2084.1.1 = {
	monarch = {
 		name = "Brogdul"
		dynasty = "gro-Bargrag"
		birth_date = 2045.1.1
		adm = 2
		dip = 6
		mil = 2
    }
}

2141.1.1 = {
	monarch = {
 		name = "Thorkh"
		dynasty = "gro-Gurag"
		birth_date = 2120.1.1
		adm = 0
		dip = 6
		mil = 6
    }
}

2232.1.1 = {
	monarch = {
 		name = "Nagoth"
		dynasty = "gro-Ushamph"
		birth_date = 2193.1.1
		adm = 5
		dip = 5
		mil = 2
    }
}

2315.1.1 = {
	monarch = {
 		name = "Grugnur"
		dynasty = "gro-Borth"
		birth_date = 2270.1.1
		adm = 3
		dip = 4
		mil = 6
    }
}

2412.1.1 = {
	monarch = {
 		name = "Brag"
		dynasty = "gro-Durz"
		birth_date = 2370.1.1
		adm = 2
		dip = 3
		mil = 3
    }
}

2481.1.1 = {
	monarch = {
 		name = "Snoogh"
		dynasty = "gro-Boagog"
		birth_date = 2435.1.1
		adm = 0
		dip = 2
		mil = 6
		female = yes
    }
}

