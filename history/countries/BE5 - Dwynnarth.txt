government = monarchy
government_rank = 1
mercantilism = 1
technology_group = elven_tg
religion = altmeri_pantheon
primary_culture = altmer
capital = 1381

54.1.1 = {
	monarch = {
 		name = "Nemendoril"
		dynasty = "Ael-Nenurmend"
		birth_date = 20.1.1
		adm = 5
		dip = 5
		mil = 5
    }
}

132.1.1 = {
	monarch = {
 		name = "Glamran"
		dynasty = "Ael-Greenwater"
		birth_date = 98.1.1
		adm = 3
		dip = 4
		mil = 2
    }
}

191.1.1 = {
	monarch = {
 		name = "Aranwen"
		dynasty = "Ael-Kaefhaer"
		birth_date = 146.1.1
		adm = 2
		dip = 3
		mil = 5
		female = yes
    }
	queen = {
 		name = "Aldarnen"
		dynasty = "Ael-Kaefhaer"
		birth_date = 161.1.1
		adm = 5
		dip = 2
		mil = 4
    }
}

271.1.1 = {
	monarch = {
 		name = "Iachesis"
		dynasty = "Bel-Shimmerene"
		birth_date = 242.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

328.1.1 = {
	monarch = {
 		name = "Atildel"
		dynasty = "Ael-Nenurmend"
		birth_date = 302.1.1
		adm = 6
		dip = 3
		mil = 3
		female = yes
    }
}

367.1.1 = {
	monarch = {
 		name = "Trelano"
		dynasty = "Ael-Nirae"
		birth_date = 334.1.1
		adm = 4
		dip = 2
		mil = 6
    }
}

437.1.1 = {
	monarch = {
 		name = "Curandil"
		dynasty = "Ael-Miriath"
		birth_date = 409.1.1
		adm = 2
		dip = 1
		mil = 3
    }
}

535.1.1 = {
	monarch = {
 		name = "Umbacano"
		dynasty = "Ael-Greenwater"
		birth_date = 503.1.1
		adm = 0
		dip = 0
		mil = 0
    }
}

602.1.1 = {
	monarch = {
 		name = "Ondolemar"
		dynasty = "Ael-Relen"
		birth_date = 579.1.1
		adm = 6
		dip = 6
		mil = 3
    }
}

684.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Nerien'eth"
		monarch_name = "Nerien'eth I"
		dynasty = "Ael-Muulinde"
		birth_date = 683.1.1
		death_date = 701.1.1
		claim = 100
		adm = 6
		dip = 3
		mil = 1
    }
}

701.1.1 = {
	monarch = {
 		name = "Calirririe"
		dynasty = "Bel-Shattered"
		birth_date = 668.1.1
		adm = 2
		dip = 5
		mil = 3
		female = yes
    }
}

774.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Arfire"
		monarch_name = "Arfire I"
		dynasty = "Wel-Belport"
		birth_date = 762.1.1
		death_date = 780.1.1
		claim = 100
		adm = 3
		dip = 2
		mil = 1
		female = yes
    }
}

780.1.1 = {
	monarch = {
 		name = "Ondendil"
		dynasty = "Bel-Taanye"
		birth_date = 735.1.1
		adm = 2
		dip = 5
		mil = 2
    }
}

822.1.1 = {
	monarch = {
 		name = "Firien"
		dynasty = "Ael-Kingdom"
		birth_date = 801.1.1
		adm = 0
		dip = 4
		mil = 5
		female = yes
    }
	queen = {
 		name = "Finyedion"
		dynasty = "Ael-Kingdom"
		birth_date = 785.1.1
		adm = 4
		dip = 3
		mil = 4
    }
	heir = {
 		name = "Tholbor"
		monarch_name = "Tholbor I"
		dynasty = "Ael-Kingdom"
		birth_date = 810.1.1
		death_date = 909.1.1
		claim = 100
		adm = 5
		dip = 6
		mil = 5
    }
}

909.1.1 = {
	monarch = {
 		name = "Freyvene"
		dynasty = "Bel-Valnoore"
		birth_date = 888.1.1
		adm = 4
		dip = 3
		mil = 6
		female = yes
    }
	queen = {
 		name = "Talonir"
		dynasty = "Bel-Valnoore"
		birth_date = 875.1.1
		adm = 1
		dip = 1
		mil = 5
    }
	heir = {
 		name = "Henlandon"
		monarch_name = "Henlandon I"
		dynasty = "Bel-Valnoore"
		birth_date = 908.1.1
		death_date = 1002.1.1
		claim = 100
		adm = 5
		dip = 0
		mil = 4
    }
}

1002.1.1 = {
	monarch = {
 		name = "Uulafalmil"
		dynasty = "Bel-Slaughter"
		birth_date = 958.1.1
		adm = 0
		dip = 1
		mil = 6
    }
}

1053.1.1 = {
	monarch = {
 		name = "Palomir"
		dynasty = "Ael-Felballin"
		birth_date = 1035.1.1
		adm = 6
		dip = 0
		mil = 3
    }
}

1144.1.1 = {
	monarch = {
 		name = "Itaalel"
		dynasty = "Ael-Nenyfire"
		birth_date = 1092.1.1
		adm = 4
		dip = 0
		mil = 6
    }
}

1243.1.1 = {
	monarch = {
 		name = "Eancar"
		dynasty = "Wel-Ardende"
		birth_date = 1216.1.1
		adm = 5
		dip = 4
		mil = 4
    }
}

1294.1.1 = {
	monarch = {
 		name = "Fanorne"
		dynasty = "Bel-Termrine"
		birth_date = 1260.1.1
		adm = 3
		dip = 3
		mil = 1
    }
	queen = {
 		name = "Carterril"
		dynasty = "Bel-Termrine"
		birth_date = 1257.1.1
		adm = 0
		dip = 1
		mil = 0
		female = yes
    }
}

1353.1.1 = {
	monarch = {
 		name = "Merenya"
		dynasty = "Wel-Angerane"
		birth_date = 1323.1.1
		adm = 5
		dip = 1
		mil = 3
		female = yes
    }
	queen = {
 		name = "Cirion"
		dynasty = "Wel-Angerane"
		birth_date = 1302.1.1
		adm = 3
		dip = 3
		mil = 6
    }
}

1437.1.1 = {
	monarch = {
 		name = "Lembien"
		dynasty = "Wel-Endrae"
		birth_date = 1418.1.1
		adm = 1
		dip = 3
		mil = 3
		female = yes
    }
}

1497.1.1 = {
	monarch = {
 		name = "Sott"
		dynasty = "Bel-Shamia"
		birth_date = 1454.1.1
		adm = 0
		dip = 2
		mil = 6
    }
	queen = {
 		name = "Lireore"
		dynasty = "Bel-Shamia"
		birth_date = 1462.1.1
		adm = 3
		dip = 0
		mil = 5
		female = yes
    }
}

1584.1.1 = {
	monarch = {
 		name = "Calpelion"
		dynasty = "Wel-Alkinosin"
		birth_date = 1549.1.1
		adm = 2
		dip = 0
		mil = 2
    }
}

1670.1.1 = {
	monarch = {
 		name = "Teldrelwe"
		dynasty = "Ael-Kingshaven"
		birth_date = 1630.1.1
		adm = 0
		dip = 6
		mil = 6
		female = yes
    }
	queen = {
 		name = "Erudil"
		dynasty = "Ael-Kingshaven"
		birth_date = 1637.1.1
		adm = 4
		dip = 4
		mil = 4
    }
	heir = {
 		name = "Sorriel"
		monarch_name = "Sorriel I"
		dynasty = "Ael-Kingshaven"
		birth_date = 1663.1.1
		death_date = 1727.1.1
		claim = 100
		adm = 1
		dip = 3
		mil = 3
    }
}

1727.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Morelrubel"
		monarch_name = "Morelrubel I"
		dynasty = "Ael-Firsthold"
		birth_date = 1713.1.1
		death_date = 1731.1.1
		claim = 100
		adm = 6
		dip = 2
		mil = 0
    }
}

1731.1.1 = {
	monarch = {
 		name = "Calibar"
		dynasty = "Wel-Faeire"
		birth_date = 1697.1.1
		adm = 2
		dip = 3
		mil = 2
    }
}

1794.1.1 = {
	monarch = {
 		name = "Telandil"
		dynasty = "Wel-Calmssare"
		birth_date = 1754.1.1
		adm = 0
		dip = 3
		mil = 6
		female = yes
    }
	queen = {
 		name = "Sanyon"
		dynasty = "Wel-Calmssare"
		birth_date = 1757.1.1
		adm = 4
		dip = 1
		mil = 5
    }
	heir = {
 		name = "Emdolye"
		monarch_name = "Emdolye I"
		dynasty = "Wel-Calmssare"
		birth_date = 1790.1.1
		death_date = 1851.1.1
		claim = 100
		adm = 1
		dip = 0
		mil = 4
		female = yes
    }
}

1851.1.1 = {
	monarch = {
 		name = "Halimorion"
		dynasty = "Bel-Virea"
		birth_date = 1802.1.1
		adm = 0
		dip = 3
		mil = 4
    }
}

1931.1.1 = {
	monarch = {
 		name = "Nolontar"
		dynasty = "Wel-Eleaninde"
		birth_date = 1911.1.1
		adm = 5
		dip = 2
		mil = 1
    }
	queen = {
 		name = "Alcildilwe"
		dynasty = "Wel-Eleaninde"
		birth_date = 1879.1.1
		adm = 2
		dip = 1
		mil = 0
		female = yes
    }
	heir = {
 		name = "Malangwen"
		monarch_name = "Malangwen I"
		dynasty = "Wel-Eleaninde"
		birth_date = 1925.1.1
		death_date = 1973.1.1
		claim = 100
		adm = 6
		dip = 6
		mil = 6
		female = yes
    }
}

1973.1.1 = {
	monarch = {
 		name = "Caryarel"
		dynasty = "Ael-Ohterane"
		birth_date = 1940.1.1
		adm = 2
		dip = 1
		mil = 1
    }
}

2011.1.1 = {
	monarch = {
 		name = "Tesenmolin"
		dynasty = "Wel-Erresse"
		birth_date = 1982.1.1
		adm = 0
		dip = 0
		mil = 5
    }
	queen = {
 		name = "Curorne"
		dynasty = "Wel-Erresse"
		birth_date = 1967.1.1
		adm = 4
		dip = 5
		mil = 4
		female = yes
    }
	heir = {
 		name = "Talambarel"
		monarch_name = "Talambarel I"
		dynasty = "Wel-Erresse"
		birth_date = 2006.1.1
		death_date = 2098.1.1
		claim = 100
		adm = 1
		dip = 4
		mil = 3
    }
}

2098.1.1 = {
	monarch = {
 		name = "Estirdalin"
		dynasty = "Bel-Vareya"
		birth_date = 2048.1.1
		adm = 3
		dip = 5
		mil = 5
		female = yes
    }
	queen = {
 		name = "Fanildil"
		dynasty = "Bel-Vareya"
		birth_date = 2072.1.1
		adm = 0
		dip = 4
		mil = 4
    }
	heir = {
 		name = "Tanulldel"
		monarch_name = "Tanulldel I"
		dynasty = "Bel-Vareya"
		birth_date = 2096.1.1
		death_date = 2148.1.1
		claim = 100
		adm = 4
		dip = 2
		mil = 3
    }
}

2148.1.1 = {
	monarch = {
 		name = "Tenirtel"
		dynasty = "Ael-Jaerfhaer"
		birth_date = 2109.1.1
		adm = 4
		dip = 6
		mil = 3
    }
	queen = {
 		name = "Sawen"
		dynasty = "Ael-Jaerfhaer"
		birth_date = 2098.1.1
		adm = 0
		dip = 4
		mil = 2
		female = yes
    }
}

2196.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Tilinarie"
		monarch_name = "Tilinarie I"
		dynasty = "Bel-Tanena"
		birth_date = 2186.1.1
		death_date = 2204.1.1
		claim = 100
		adm = 5
		dip = 6
		mil = 2
		female = yes
    }
}

2204.1.1 = {
	monarch = {
 		name = "Lanilian"
		dynasty = "Ael-Mathiisen"
		birth_date = 2172.1.1
		adm = 4
		dip = 3
		mil = 3
    }
	queen = {
 		name = "Taltaarie"
		dynasty = "Ael-Mathiisen"
		birth_date = 2166.1.1
		adm = 1
		dip = 1
		mil = 2
		female = yes
    }
	heir = {
 		name = "Faranya"
		monarch_name = "Faranya I"
		dynasty = "Ael-Mathiisen"
		birth_date = 2193.1.1
		death_date = 2291.1.1
		claim = 100
		adm = 5
		dip = 0
		mil = 1
		female = yes
    }
}

2291.1.1 = {
	monarch = {
 		name = "Vaerwyae"
		dynasty = "Bel-Vareya"
		birth_date = 2254.1.1
		adm = 0
		dip = 1
		mil = 3
		female = yes
    }
}

2335.1.1 = {
	monarch = {
 		name = "Pelidil"
		dynasty = "Ael-Graddun"
		birth_date = 2304.1.1
		adm = 6
		dip = 0
		mil = 0
    }
	queen = {
 		name = "Antomine"
		dynasty = "Ael-Graddun"
		birth_date = 2291.1.1
		adm = 3
		dip = 6
		mil = 6
		female = yes
    }
	heir = {
 		name = "Nelanya"
		monarch_name = "Nelanya I"
		dynasty = "Ael-Graddun"
		birth_date = 2325.1.1
		death_date = 2430.1.1
		claim = 100
		adm = 6
		dip = 4
		mil = 4
		female = yes
    }
}

2430.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Hinaamo"
		monarch_name = "Hinaamo I"
		dynasty = "Bel-Silalaure"
		birth_date = 2428.1.1
		death_date = 2446.1.1
		claim = 100
		adm = 5
		dip = 4
		mil = 1
    }
}

2446.1.1 = {
	monarch = {
 		name = "Valano"
		dynasty = "Wel-Croddlehurst"
		birth_date = 2410.1.1
		adm = 4
		dip = 0
		mil = 1
    }
}

