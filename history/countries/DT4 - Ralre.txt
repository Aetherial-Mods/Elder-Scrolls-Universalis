government = native
government_rank = 1
mercantilism = 1
technology_group = daedric_tg
religion = molag_bal_cult
primary_culture = clanfear
capital = 3891

54.1.1 = {
	monarch = {
 		name = "Cevaid"
		dynasty = "Jul-Moutti"
		birth_date = 32.1.1
		adm = 2
		dip = 5
		mil = 4
    }
}

139.1.1 = {
	monarch = {
 		name = "Guhro"
		dynasty = "Jul-Muotaso"
		birth_date = 105.1.1
		adm = 2
		dip = 2
		mil = 4
		female = yes
    }
}

181.1.1 = {
	monarch = {
 		name = "Cilgru"
		dynasty = "Jul-Ceh"
		birth_date = 162.1.1
		adm = 0
		dip = 1
		mil = 1
    }
}

230.1.1 = {
	monarch = {
 		name = "Alto"
		dynasty = "Jul-Vox"
		birth_date = 203.1.1
		adm = 5
		dip = 0
		mil = 5
		female = yes
    }
}

283.1.1 = {
	monarch = {
 		name = "Drazri"
		dynasty = "Jul-Tavi"
		birth_date = 235.1.1
		adm = 4
		dip = 6
		mil = 1
    }
}

354.1.1 = {
	monarch = {
 		name = "Hizzovul"
		dynasty = "Jul-Fince"
		birth_date = 316.1.1
		adm = 2
		dip = 5
		mil = 5
    }
}

433.1.1 = {
	monarch = {
 		name = "Cilgru"
		dynasty = "Jul-Fidlezeth"
		birth_date = 385.1.1
		adm = 0
		dip = 5
		mil = 2
    }
}

472.1.1 = {
	monarch = {
 		name = "Gnigorm"
		dynasty = "Jul-Pututh"
		birth_date = 432.1.1
		adm = 5
		dip = 4
		mil = 5
    }
}

563.1.1 = {
	monarch = {
 		name = "Drazri"
		dynasty = "Jul-Rodlal"
		birth_date = 531.1.1
		adm = 0
		dip = 5
		mil = 0
    }
}

649.1.1 = {
	monarch = {
 		name = "Hizzovul"
		dynasty = "Jul-Nimeclo"
		birth_date = 617.1.1
		adm = 5
		dip = 4
		mil = 3
    }
}

737.1.1 = {
	monarch = {
 		name = "Hraizzos"
		dynasty = "Jul-Figli"
		birth_date = 705.1.1
		adm = 4
		dip = 4
		mil = 0
    }
}

799.1.1 = {
	monarch = {
 		name = "Dazungat"
		dynasty = "Jul-Bromrix"
		birth_date = 765.1.1
		adm = 2
		dip = 3
		mil = 4
    }
}

842.1.1 = {
	monarch = {
 		name = "Ders"
		dynasty = "Jul-Gizromol"
		birth_date = 807.1.1
		adm = 0
		dip = 2
		mil = 0
    }
}

879.1.1 = {
	monarch = {
 		name = "Neraibors"
		dynasty = "Jul-Dash"
		birth_date = 858.1.1
		adm = 5
		dip = 1
		mil = 4
    }
}

952.1.1 = {
	monarch = {
 		name = "Hraizzos"
		dynasty = "Jul-Cosron"
		birth_date = 901.1.1
		adm = 4
		dip = 0
		mil = 1
    }
}

1025.1.1 = {
	monarch = {
 		name = "Kish"
		dynasty = "Jul-Gogrol"
		birth_date = 993.1.1
		adm = 2
		dip = 0
		mil = 4
		female = yes
    }
}

1071.1.1 = {
	monarch = {
 		name = "Hax"
		dynasty = "Jul-Hata"
		birth_date = 1032.1.1
		adm = 4
		dip = 1
		mil = 6
		female = yes
    }
}

1114.1.1 = {
	monarch = {
 		name = "Griazuurag"
		dynasty = "Jul-Keth"
		birth_date = 1063.1.1
		adm = 2
		dip = 0
		mil = 2
    }
}

1191.1.1 = {
	monarch = {
 		name = "Zall"
		dynasty = "Jul-Idash"
		birth_date = 1145.1.1
		adm = 6
		dip = 2
		mil = 2
    }
}

1269.1.1 = {
	monarch = {
 		name = "Ferin"
		dynasty = "Jul-Bromrix"
		birth_date = 1228.1.1
		adm = 1
		dip = 3
		mil = 4
		female = yes
    }
}

1306.1.1 = {
	monarch = {
 		name = "Crir"
		dynasty = "Jul-Voh"
		birth_date = 1274.1.1
		adm = 6
		dip = 3
		mil = 1
    }
}

1400.1.1 = {
	monarch = {
 		name = "Throlraars"
		dynasty = "Jul-Catrush"
		birth_date = 1368.1.1
		adm = 5
		dip = 2
		mil = 4
    }
}

1499.1.1 = {
	monarch = {
 		name = "Sar"
		dynasty = "Jul-Zun"
		birth_date = 1449.1.1
		adm = 3
		dip = 1
		mil = 1
    }
}

1598.1.1 = {
	monarch = {
 		name = "Adzad"
		dynasty = "Jul-Vox"
		birth_date = 1568.1.1
		adm = 4
		dip = 5
		mil = 6
    }
}

1682.1.1 = {
	monarch = {
 		name = "Hash"
		dynasty = "Jul-Dash"
		birth_date = 1653.1.1
		adm = 2
		dip = 4
		mil = 3
		female = yes
    }
}

1756.1.1 = {
	monarch = {
 		name = "Tocrai"
		dynasty = "Jul-Ren"
		birth_date = 1705.1.1
		adm = 0
		dip = 3
		mil = 6
		female = yes
    }
}

1817.1.1 = {
	monarch = {
 		name = "Cevaid"
		dynasty = "Jul-Nabrou"
		birth_date = 1773.1.1
		adm = 5
		dip = 3
		mil = 3
    }
}

1878.1.1 = {
	monarch = {
 		name = "Daicri"
		dynasty = "Jul-Bittegho"
		birth_date = 1854.1.1
		adm = 4
		dip = 2
		mil = 6
		female = yes
    }
}

1929.1.1 = {
	monarch = {
 		name = "Hash"
		dynasty = "Jul-Sehka"
		birth_date = 1901.1.1
		adm = 2
		dip = 1
		mil = 3
		female = yes
    }
}

2010.1.1 = {
	monarch = {
 		name = "Khastra"
		dynasty = "Jul-Maroga"
		birth_date = 1971.1.1
		adm = 0
		dip = 0
		mil = 0
    }
}

2050.1.1 = {
	monarch = {
 		name = "Simlax"
		dynasty = "Jul-Vible"
		birth_date = 2011.1.1
		adm = 2
		dip = 2
		mil = 1
		female = yes
    }
}

2085.1.1 = {
	monarch = {
 		name = "Iljkola"
		dynasty = "Jul-Faighoullo"
		birth_date = 2034.1.1
		adm = 0
		dip = 1
		mil = 5
		female = yes
    }
}

2135.1.1 = {
	monarch = {
 		name = "Griazuurag"
		dynasty = "Jul-Nesh"
		birth_date = 2084.1.1
		adm = 4
		dip = 3
		mil = 5
    }
}

2227.1.1 = {
	monarch = {
 		name = "Ilra"
		dynasty = "Jul-Ren"
		birth_date = 2174.1.1
		adm = 6
		dip = 4
		mil = 0
		female = yes
    }
}

2309.1.1 = {
	monarch = {
 		name = "Kharogesh"
		dynasty = "Jul-Fince"
		birth_date = 2282.1.1
		adm = 4
		dip = 3
		mil = 3
    }
}

2367.1.1 = {
	monarch = {
 		name = "Crir"
		dynasty = "Jul-Rax"
		birth_date = 2318.1.1
		adm = 3
		dip = 2
		mil = 0
    }
}

2418.1.1 = {
	monarch = {
 		name = "Adzad"
		dynasty = "Jul-Ren"
		birth_date = 2391.1.1
		adm = 1
		dip = 2
		mil = 3
    }
}

2485.1.1 = {
	monarch = {
 		name = "Gork"
		dynasty = "Jul-Ruoni"
		birth_date = 2432.1.1
		adm = 6
		dip = 1
		mil = 0
    }
}

