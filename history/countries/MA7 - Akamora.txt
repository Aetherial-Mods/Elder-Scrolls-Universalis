government = republic
government_rank = 3
mercantilism = 1
technology_group = dwemer_tg
religion = reason_and_logic_cult
primary_culture = dwemer
capital = 4032

54.1.1 = {
	monarch = {
 		name = "Nromgunch"
		dynasty = "Aq'Tchazchyn"
		birth_date = 6.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

90.1.1 = {
	monarch = {
 		name = "Achyggo"
		dynasty = "Av'Batvar"
		birth_date = 72.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

135.1.1 = {
	monarch = {
 		name = "Mherbira"
		dynasty = "Af'Chzetvar"
		birth_date = 116.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

182.1.1 = {
	monarch = {
 		name = "Klazgar"
		dynasty = "Az'Melchanf"
		birth_date = 152.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

276.1.1 = {
	monarch = {
 		name = "Nromgunch"
		dynasty = "Av'Brazzedit"
		birth_date = 249.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

342.1.1 = {
	monarch = {
 		name = "Grabnanch"
		dynasty = "Av'Sthomin"
		birth_date = 300.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

395.1.1 = {
	monarch = {
 		name = "Brehron"
		dynasty = "Az'Kridhis"
		birth_date = 349.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

484.1.1 = {
	monarch = {
 		name = "Asrahuanch"
		dynasty = "Av'Miban"
		birth_date = 452.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

577.1.1 = {
	monarch = {
 		name = "Sohner"
		dynasty = "Af'Sthord"
		birth_date = 556.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

667.1.1 = {
	monarch = {
 		name = "Mrotchatz"
		dynasty = "Az'Sohner"
		birth_date = 625.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

703.1.1 = {
	monarch = {
 		name = "Ralen"
		dynasty = "Aq'Banrynn"
		birth_date = 681.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

775.1.1 = {
	monarch = {
 		name = "Asrahuanch"
		dynasty = "Az'Azsamchin"
		birth_date = 735.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

852.1.1 = {
	monarch = {
 		name = "Klalzrak"
		dynasty = "Aq'Choadzach"
		birth_date = 818.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

904.1.1 = {
	monarch = {
 		name = "Izvulzarf"
		dynasty = "Az'Melchanf"
		birth_date = 875.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

999.1.1 = {
	monarch = {
 		name = "Ythamac"
		dynasty = "Aq'Byrahken"
		birth_date = 968.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1098.1.1 = {
	monarch = {
 		name = "Badzach"
		dynasty = "Av'Dzragvin"
		birth_date = 1056.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1135.1.1 = {
	monarch = {
 		name = "Rkudit"
		dynasty = "Av'Ralen"
		birth_date = 1111.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1202.1.1 = {
	monarch = {
 		name = "Izvulzarf"
		dynasty = "Af'Chzemgunch"
		birth_date = 1156.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1251.1.1 = {
	monarch = {
 		name = "Jlarerhunch"
		dynasty = "Av'Ynzarlatz"
		birth_date = 1202.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1334.1.1 = {
	monarch = {
 		name = "Kolzarf"
		dynasty = "Av'Ihlefuan"
		birth_date = 1289.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1411.1.1 = {
	monarch = {
 		name = "Drertes"
		dynasty = "Av'Ralen"
		birth_date = 1385.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1499.1.1 = {
	monarch = {
 		name = "Bhazchyn"
		dynasty = "Av'Brehron"
		birth_date = 1468.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1549.1.1 = {
	monarch = {
 		name = "Nchynac"
		dynasty = "Av'Chiuhld"
		birth_date = 1499.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1620.1.1 = {
	monarch = {
 		name = "Kolzarf"
		dynasty = "Aq'Ilzeglan"
		birth_date = 1574.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1681.1.1 = {
	monarch = {
 		name = "Mravlara"
		dynasty = "Av'Chragrenz"
		birth_date = 1640.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1740.1.1 = {
	monarch = {
 		name = "Ralarn"
		dynasty = "Av'Tnarlac"
		birth_date = 1687.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1782.1.1 = {
	monarch = {
 		name = "Nchynac"
		dynasty = "Av'Grubond"
		birth_date = 1737.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1836.1.1 = {
	monarch = {
 		name = "Inratarn"
		dynasty = "Aq'Ksredlin"
		birth_date = 1784.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1924.1.1 = {
	monarch = {
 		name = "Nedac"
		dynasty = "Aq'Chzefrach"
		birth_date = 1878.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1964.1.1 = {
	monarch = {
 		name = "Sthord"
		dynasty = "Az'Djuhnch"
		birth_date = 1937.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2049.1.1 = {
	monarch = {
 		name = "Jnathunch"
		dynasty = "Az'Jribwyr"
		birth_date = 2012.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2116.1.1 = {
	monarch = {
 		name = "Sthomin"
		dynasty = "Av'Jnathunch"
		birth_date = 2073.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2203.1.1 = {
	monarch = {
 		name = "Chzemgunch"
		dynasty = "Af'Nchynac"
		birth_date = 2160.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2295.1.1 = {
	monarch = {
 		name = "Rafk"
		dynasty = "Af'Klalzrak"
		birth_date = 2247.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2335.1.1 = {
	monarch = {
 		name = "Chzefrach"
		dynasty = "Av'Inratarn"
		birth_date = 2283.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

2404.1.1 = {
	monarch = {
 		name = "Mchavin"
		dynasty = "Av'Mzaglan"
		birth_date = 2374.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2466.1.1 = {
	monarch = {
 		name = "Rhothurzch"
		dynasty = "Av'Tugradac"
		birth_date = 2421.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

