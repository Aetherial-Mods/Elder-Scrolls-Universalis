government = theocracy
government_rank = 1
mercantilism = 1
technology_group = northern_tg
religion = dragon_cult
primary_culture = atmoran
capital = 3191

54.1.1 = {
	monarch = {
 		name = "Folsgold"
		dynasty = "Hoksild"
		birth_date = 33.1.1
		adm = 5
		dip = 3
		mil = 3
    }
}

132.1.1 = {
	monarch = {
 		name = "Klolsgonl"
		dynasty = "Rhanwall"
		birth_date = 112.1.1
		adm = 4
		dip = 2
		mil = 6
    }
}

226.1.1 = {
	monarch = {
 		name = "Ferleld"
		dynasty = "Fosmil"
		birth_date = 180.1.1
		adm = 4
		dip = 6
		mil = 4
    }
}

312.1.1 = {
	monarch = {
 		name = "Rulur"
		dynasty = "Thirmoskr"
		birth_date = 289.1.1
		adm = 3
		dip = 5
		mil = 1
		female = yes
    }
}

397.1.1 = {
	monarch = {
 		name = "Frodmeldof"
		dynasty = "Bralg"
		birth_date = 370.1.1
		adm = 3
		dip = 2
		mil = 6
		female = yes
    }
}

439.1.1 = {
	monarch = {
 		name = "Ratmam"
		dynasty = "Dorkalg"
		birth_date = 417.1.1
		adm = 3
		dip = 6
		mil = 6
    }
}

514.1.1 = {
	monarch = {
 		name = "Jochin"
		dynasty = "Brelskald"
		birth_date = 475.1.1
		adm = 0
		dip = 1
		mil = 6
    }
}

571.1.1 = {
	monarch = {
 		name = "Gol"
		dynasty = "Dilg"
		birth_date = 531.1.1
		adm = 5
		dip = 0
		mil = 3
    }
}

607.1.1 = {
	monarch = {
 		name = "Stindurilg"
		dynasty = "Frelsgel"
		birth_date = 562.1.1
		adm = 0
		dip = 1
		mil = 4
    }
}

660.1.1 = {
	monarch = {
 		name = "Jetmenl"
		dynasty = "Gugnorr"
		birth_date = 631.1.1
		adm = 4
		dip = 3
		mil = 4
    }
}

738.1.1 = {
	monarch = {
 		name = "Hoksild"
		dynasty = "Gechem"
		birth_date = 685.1.1
		adm = 2
		dip = 2
		mil = 1
    }
}

794.1.1 = {
	monarch = {
 		name = "Budek"
		dynasty = "Finwoskr"
		birth_date = 766.1.1
		adm = 4
		dip = 4
		mil = 2
    }
}

890.1.1 = {
	monarch = {
 		name = "Geldr"
		dynasty = "Rhyrrerik"
		birth_date = 850.1.1
		adm = 3
		dip = 3
		mil = 6
		female = yes
    }
}

947.1.1 = {
	monarch = {
 		name = "Skechilg"
		dynasty = "Finwoskr"
		birth_date = 910.1.1
		adm = 1
		dip = 2
		mil = 3
    }
}

1029.1.1 = {
	monarch = {
 		name = "Kurmok"
		dynasty = "Keld"
		birth_date = 1000.1.1
		adm = 6
		dip = 1
		mil = 6
    }
}

1067.1.1 = {
	monarch = {
 		name = "Thelbell"
		dynasty = "Sungiaskr"
		birth_date = 1047.1.1
		adm = 4
		dip = 0
		mil = 3
    }
}

1113.1.1 = {
	monarch = {
 		name = "Hormeld"
		dynasty = "Dormam"
		birth_date = 1070.1.1
		adm = 2
		dip = 0
		mil = 0
		female = yes
    }
}

1148.1.1 = {
	monarch = {
 		name = "Skechilg"
		dynasty = "Rotgild"
		birth_date = 1110.1.1
		adm = 1
		dip = 6
		mil = 3
    }
}

1184.1.1 = {
	monarch = {
 		name = "Kurmok"
		dynasty = "Bryngeemir"
		birth_date = 1157.1.1
		adm = 6
		dip = 5
		mil = 0
    }
}

1267.1.1 = {
	monarch = {
 		name = "Thelbell"
		dynasty = "Tilgeskr"
		birth_date = 1220.1.1
		adm = 1
		dip = 6
		mil = 1
    }
}

1349.1.1 = {
	monarch = {
 		name = "Hlihuldan"
		dynasty = "Skitgerr"
		birth_date = 1299.1.1
		adm = 6
		dip = 5
		mil = 5
		female = yes
    }
}

1388.1.1 = {
	monarch = {
 		name = "Thedrildr"
		dynasty = "Rotgild"
		birth_date = 1360.1.1
		adm = 4
		dip = 5
		mil = 2
		female = yes
    }
}

1460.1.1 = {
	monarch = {
 		name = "Rulur"
		dynasty = "Skitgerr"
		birth_date = 1411.1.1
		adm = 3
		dip = 4
		mil = 5
		female = yes
    }
}

1521.1.1 = {
	monarch = {
 		name = "Rotmalleskr"
		dynasty = "Tilgeskr"
		birth_date = 1498.1.1
		adm = 1
		dip = 3
		mil = 2
    }
}

1584.1.1 = {
	monarch = {
 		name = "Thrialder"
		dynasty = "Klytmor"
		birth_date = 1560.1.1
		adm = 6
		dip = 2
		mil = 6
		female = yes
    }
}

1672.1.1 = {
	monarch = {
 		name = "Thudubyldr"
		dynasty = "Boechyrom"
		birth_date = 1648.1.1
		adm = 4
		dip = 1
		mil = 2
		female = yes
    }
}

1735.1.1 = {
	monarch = {
 		name = "Frethold"
		dynasty = "Tiwon"
		birth_date = 1698.1.1
		adm = 3
		dip = 1
		mil = 6
		female = yes
    }
}

1796.1.1 = {
	monarch = {
 		name = "Rotmalleskr"
		dynasty = "Delsgoll"
		birth_date = 1775.1.1
		adm = 2
		dip = 4
		mil = 6
    }
}

1845.1.1 = {
	monarch = {
 		name = "Kutgild"
		dynasty = "Bralg"
		birth_date = 1792.1.1
		adm = 0
		dip = 3
		mil = 3
    }
}

1941.1.1 = {
	monarch = {
 		name = "Skendeinl"
		dynasty = "Rotmalleskr"
		birth_date = 1917.1.1
		adm = 5
		dip = 2
		mil = 0
    }
}

2005.1.1 = {
	monarch = {
 		name = "Frelsgel"
		dynasty = "Jetmenl"
		birth_date = 1959.1.1
		adm = 4
		dip = 2
		mil = 3
    }
}

2053.1.1 = {
	monarch = {
 		name = "Tosgril"
		dynasty = "Told"
		birth_date = 2031.1.1
		adm = 2
		dip = 1
		mil = 0
    }
}

2123.1.1 = {
	monarch = {
 		name = "Kutgild"
		dynasty = "Steedeth"
		birth_date = 2089.1.1
		adm = 4
		dip = 2
		mil = 1
    }
}

2211.1.1 = {
	monarch = {
 		name = "Threidiodr"
		dynasty = "Vilskemild"
		birth_date = 2173.1.1
		adm = 2
		dip = 1
		mil = 5
		female = yes
    }
}

2296.1.1 = {
	monarch = {
 		name = "Tusgrirr"
		dynasty = "Storgath"
		birth_date = 2243.1.1
		adm = 0
		dip = 1
		mil = 2
    }
}

2357.1.1 = {
	monarch = {
 		name = "Breyld"
		dynasty = "Jochin"
		birth_date = 2304.1.1
		adm = 6
		dip = 0
		mil = 5
		female = yes
    }
}

2420.1.1 = {
	monarch = {
 		name = "Brungolg"
		dynasty = "Keld"
		birth_date = 2382.1.1
		adm = 4
		dip = 6
		mil = 2
    }
}

2474.1.1 = {
	monarch = {
 		name = "Heyrmin"
		dynasty = "Rholl"
		birth_date = 2427.1.1
		adm = 2
		dip = 5
		mil = 6
		female = yes
    }
}

