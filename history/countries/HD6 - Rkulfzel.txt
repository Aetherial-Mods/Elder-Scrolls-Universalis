government = republic
government_rank = 3
mercantilism = 1
technology_group = dwemer_tg
religion = reason_and_logic_cult
primary_culture = dwemer
capital = 6091

54.1.1 = {
	monarch = {
 		name = "Mrotchatz"
		dynasty = "Av'Grubond"
		birth_date = 29.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

149.1.1 = {
	monarch = {
 		name = "Ralen"
		dynasty = "Az'Ynzarlakch"
		birth_date = 128.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

219.1.1 = {
	monarch = {
 		name = "Asrahuanch"
		dynasty = "Av'Miban"
		birth_date = 167.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

301.1.1 = {
	monarch = {
 		name = "Klalzrak"
		dynasty = "Aq'Asranrynn"
		birth_date = 270.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

364.1.1 = {
	monarch = {
 		name = "Batvar"
		dynasty = "Av'Brazzedit"
		birth_date = 341.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

401.1.1 = {
	monarch = {
 		name = "Izvuvretch"
		dynasty = "Aq'Tchazchyn"
		birth_date = 358.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

447.1.1 = {
	monarch = {
 		name = "Badzach"
		dynasty = "Af'Achyggo"
		birth_date = 405.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

498.1.1 = {
	monarch = {
 		name = "Drertes"
		dynasty = "Af'Nchynac"
		birth_date = 478.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

550.1.1 = {
	monarch = {
 		name = "Shtrorlis"
		dynasty = "Av'Klazgar"
		birth_date = 523.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

638.1.1 = {
	monarch = {
 		name = "Mzaglan"
		dynasty = "Af'Szogarn"
		birth_date = 592.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

695.1.1 = {
	monarch = {
 		name = "Ylrefwinn"
		dynasty = "Aq'Grigarn"
		birth_date = 651.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

733.1.1 = {
	monarch = {
 		name = "Tzenruz"
		dynasty = "Af'Rhzomrond"
		birth_date = 691.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

800.1.1 = {
	monarch = {
 		name = "Cfradrys"
		dynasty = "Av'Sthomin"
		birth_date = 776.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

881.1.1 = {
	monarch = {
 		name = "Ksredlin"
		dynasty = "Af'Jhourlac"
		birth_date = 863.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

959.1.1 = {
	monarch = {
 		name = "Tnadrak"
		dynasty = "Az'Krincha"
		birth_date = 910.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1001.1.1 = {
	monarch = {
 		name = "Grabnanch"
		dynasty = "Af'Chzemgunch"
		birth_date = 979.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1045.1.1 = {
	monarch = {
 		name = "Cuohrek"
		dynasty = "Aq'Bluhzis"
		birth_date = 1011.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1105.1.1 = {
	monarch = {
 		name = "Snelarn"
		dynasty = "Az'Aknanwess"
		birth_date = 1071.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1166.1.1 = {
	monarch = {
 		name = "Klalzrak"
		dynasty = "Az'Krincha"
		birth_date = 1121.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1259.1.1 = {
	monarch = {
 		name = "Djubchasz"
		dynasty = "Af'Chzetvar"
		birth_date = 1220.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1347.1.1 = {
	monarch = {
 		name = "Rlorlis"
		dynasty = "Av'Dzragvin"
		birth_date = 1295.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1400.1.1 = {
	monarch = {
 		name = "Cheftris"
		dynasty = "Av'Jlarerhunch"
		birth_date = 1353.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1450.1.1 = {
	monarch = {
 		name = "Jragrenz"
		dynasty = "Af'Ralarn"
		birth_date = 1413.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1512.1.1 = {
	monarch = {
 		name = "Klazgar"
		dynasty = "Av'Choatchatz"
		birth_date = 1484.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1553.1.1 = {
	monarch = {
 		name = "Chrudras"
		dynasty = "Az'Nohnch"
		birth_date = 1530.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1642.1.1 = {
	monarch = {
 		name = "Cheftris"
		dynasty = "Av'Sthomin"
		birth_date = 1606.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1691.1.1 = {
	monarch = {
 		name = "Brehron"
		dynasty = "Af'Szogarn"
		birth_date = 1664.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1753.1.1 = {
	monarch = {
 		name = "Ychohnch"
		dynasty = "Az'Melchanf"
		birth_date = 1703.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1832.1.1 = {
	monarch = {
 		name = "Izvuvretch"
		dynasty = "Av'Tromgunch"
		birth_date = 1781.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1909.1.1 = {
	monarch = {
 		name = "Jhouvin"
		dynasty = "Az'Doudrys"
		birth_date = 1863.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1974.1.1 = {
	monarch = {
 		name = "Brehron"
		dynasty = "Av'Ishevnorz"
		birth_date = 1927.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2025.1.1 = {
	monarch = {
 		name = "Ychohnch"
		dynasty = "Av'Davlar"
		birth_date = 1990.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

2095.1.1 = {
	monarch = {
 		name = "Ychogarn"
		dynasty = "Aq'Choadzach"
		birth_date = 2067.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2178.1.1 = {
	monarch = {
 		name = "Aravlen"
		dynasty = "Aq'Badzach"
		birth_date = 2139.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

2217.1.1 = {
	monarch = {
 		name = "Crafrysz"
		dynasty = "Aq'Blunrida"
		birth_date = 2180.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2253.1.1 = {
	monarch = {
 		name = "Sthord"
		dynasty = "Af'Jlarenzgar"
		birth_date = 2201.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2341.1.1 = {
	monarch = {
 		name = "Mzamrumhz"
		dynasty = "Az'Shtrorlis"
		birth_date = 2319.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2432.1.1 = {
	monarch = {
 		name = "Doudhis"
		dynasty = "Aq'Tchazchyn"
		birth_date = 2394.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

