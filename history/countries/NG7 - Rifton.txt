government = monarchy
government_rank = 3
mercantilism = 1
technology_group = northern_tg
religion = snow_elves_pantheon
primary_culture = snow_elven
capital = 1268

54.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Heregriath"
		monarch_name = "Heregriath I"
		dynasty = "Tor-Heleselin"
		birth_date = 48.1.1
		death_date = 66.1.1
		claim = 100
		adm = 4
		dip = 5
		mil = 0
    }
}

66.1.1 = {
	monarch = {
 		name = "Kranyrwen"
		dynasty = "Tir-Waidhor"
		birth_date = 43.1.1
		adm = 4
		dip = 2
		mil = 0
		female = yes
    }
	queen = {
 		name = "Yrlor"
		dynasty = "Tir-Waidhor"
		birth_date = 24.1.1
		adm = 1
		dip = 0
		mil = 6
    }
	heir = {
 		name = "Jedhur"
		monarch_name = "Jedhur I"
		dynasty = "Tir-Waidhor"
		birth_date = 57.1.1
		death_date = 165.1.1
		claim = 100
		adm = 4
		dip = 6
		mil = 5
    }
}

165.1.1 = {
	monarch = {
 		name = "Unarrenoth"
		dynasty = "Ter-Cryntaroth"
		birth_date = 116.1.1
		adm = 0
		dip = 0
		mil = 1
    }
}

223.1.1 = {
	monarch = {
 		name = "Sindras"
		dynasty = "Tor-Ronekelor"
		birth_date = 205.1.1
		adm = 5
		dip = 6
		mil = 4
    }
	queen = {
 		name = "Rinenyish"
		dynasty = "Tor-Ronekelor"
		birth_date = 171.1.1
		adm = 2
		dip = 5
		mil = 3
		female = yes
    }
	heir = {
 		name = "Aglen"
		monarch_name = "Aglen I"
		dynasty = "Tor-Ronekelor"
		birth_date = 212.1.1
		death_date = 267.1.1
		claim = 100
		adm = 6
		dip = 3
		mil = 2
    }
}

267.1.1 = {
	monarch = {
 		name = "Shanbath"
		dynasty = "Tor-Norvryn"
		birth_date = 237.1.1
		adm = 2
		dip = 5
		mil = 5
    }
	queen = {
 		name = "Pryslori"
		dynasty = "Tor-Norvryn"
		birth_date = 220.1.1
		adm = 6
		dip = 3
		mil = 3
		female = yes
    }
	heir = {
 		name = "Koryrwen"
		monarch_name = "Koryrwen I"
		dynasty = "Tor-Norvryn"
		birth_date = 267.1.1
		death_date = 308.1.1
		claim = 100
		adm = 3
		dip = 2
		mil = 2
    }
}

308.1.1 = {
	monarch = {
 		name = "Harwe"
		dynasty = "Tyr-Vranhan"
		birth_date = 278.1.1
		adm = 2
		dip = 5
		mil = 3
    }
	queen = {
 		name = "Zarrenoth"
		dynasty = "Tyr-Vranhan"
		birth_date = 288.1.1
		adm = 6
		dip = 4
		mil = 2
		female = yes
    }
	heir = {
 		name = "Uresur"
		monarch_name = "Uresur I"
		dynasty = "Tyr-Vranhan"
		birth_date = 294.1.1
		death_date = 356.1.1
		claim = 100
		adm = 6
		dip = 0
		mil = 3
    }
}

356.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Heleselin"
		monarch_name = "Heleselin I"
		dynasty = "Tyr-Celeher"
		birth_date = 348.1.1
		death_date = 366.1.1
		claim = 100
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

366.1.1 = {
	monarch = {
 		name = "Agparwen"
		dynasty = "		"
		birth_date = 340.1.1
		adm = 4
		dip = 3
		mil = 0
		female = yes
    }
}

433.1.1 = {
	monarch = {
 		name = "Orhorith"
		dynasty = "Ter-Caermyn"
		birth_date = 396.1.1
		adm = 5
		dip = 0
		mil = 5
    }
	queen = {
 		name = "Ganborin"
		dynasty = "Ter-Caermyn"
		birth_date = 399.1.1
		adm = 1
		dip = 5
		mil = 4
		female = yes
    }
	heir = {
 		name = "Haerwen"
		monarch_name = "Haerwen I"
		dynasty = "Ter-Caermyn"
		birth_date = 429.1.1
		death_date = 522.1.1
		claim = 100
		adm = 5
		dip = 4
		mil = 3
		female = yes
    }
}

522.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Agzoth"
		monarch_name = "Agzoth I"
		dynasty = "Tir-Awith"
		birth_date = 512.1.1
		death_date = 530.1.1
		claim = 100
		adm = 2
		dip = 6
		mil = 3
    }
}

530.1.1 = {
	monarch = {
 		name = "Yrerawyn"
		dynasty = "Tyr-Arithune"
		birth_date = 477.1.1
		adm = 2
		dip = 2
		mil = 3
    }
	queen = {
 		name = "Hargoth"
		dynasty = "Tyr-Arithune"
		birth_date = 486.1.1
		adm = 6
		dip = 1
		mil = 2
		female = yes
    }
	heir = {
 		name = "Lynzis"
		monarch_name = "Lynzis I"
		dynasty = "Tyr-Arithune"
		birth_date = 526.1.1
		death_date = 617.1.1
		claim = 100
		adm = 3
		dip = 6
		mil = 1
		female = yes
    }
}

617.1.1 = {
	monarch = {
 		name = "Vyrpath"
		dynasty = "Tyr-Loretaroth"
		birth_date = 587.1.1
		adm = 5
		dip = 1
		mil = 3
		female = yes
    }
}

675.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Agzoth"
		monarch_name = "Agzoth I"
		dynasty = "Tir-Parezhor"
		birth_date = 670.1.1
		death_date = 688.1.1
		claim = 100
		adm = 6
		dip = 5
		mil = 1
    }
}

688.1.1 = {
	monarch = {
 		name = "Yrerawyn"
		dynasty = "Tor-Vrantan"
		birth_date = 668.1.1
		adm = 2
		dip = 6
		mil = 3
    }
}

763.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Shanrawyn"
		monarch_name = "Shanrawyn I"
		dynasty = "Ter-Finbath"
		birth_date = 757.1.1
		death_date = 775.1.1
		claim = 100
		adm = 3
		dip = 3
		mil = 1
    }
}

775.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Jedhur"
		monarch_name = "Jedhur I"
		dynasty = "Tir-Zidadhor"
		birth_date = 762.1.1
		death_date = 780.1.1
		claim = 100
		adm = 1
		dip = 2
		mil = 5
    }
}

780.1.1 = {
	monarch = {
 		name = "Lenenoth"
		dynasty = "Tir-Engriath"
		birth_date = 741.1.1
		adm = 0
		dip = 6
		mil = 5
    }
}

830.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Lenekelor"
		monarch_name = "Lenekelor I"
		dynasty = "Tir-Waidhor"
		birth_date = 819.1.1
		death_date = 837.1.1
		claim = 100
		adm = 4
		dip = 1
		mil = 5
    }
}

837.1.1 = {
	monarch = {
 		name = "Uremeloth"
		dynasty = "Tyr-Jelori"
		birth_date = 787.1.1
		adm = 4
		dip = 4
		mil = 6
    }
	queen = {
 		name = "Kirgeth"
		dynasty = "Tyr-Jelori"
		birth_date = 806.1.1
		adm = 1
		dip = 3
		mil = 4
		female = yes
    }
	heir = {
 		name = "Vranzoth"
		monarch_name = "Vranzoth I"
		dynasty = "Tyr-Jelori"
		birth_date = 837.1.1
		death_date = 884.1.1
		claim = 100
		adm = 4
		dip = 1
		mil = 3
    }
}

884.1.1 = {
	monarch = {
 		name = "Minerenoth"
		dynasty = "Ter-Torsur"
		birth_date = 856.1.1
		adm = 0
		dip = 3
		mil = 6
		female = yes
    }
}

960.1.1 = {
	monarch = {
 		name = "Wanborin"
		dynasty = "Tor-Agvarys"
		birth_date = 928.1.1
		adm = 1
		dip = 0
		mil = 4
    }
}

1035.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Glennaris"
		monarch_name = "Glennaris I"
		dynasty = "Tir-Maszras"
		birth_date = 1033.1.1
		death_date = 1051.1.1
		claim = 100
		adm = 2
		dip = 4
		mil = 2
    }
}

1051.1.1 = {
	monarch = {
 		name = "Athdhor"
		dynasty = "Ter-Jedhur"
		birth_date = 1004.1.1
		adm = 4
		dip = 5
		mil = 4
    }
	queen = {
 		name = "Minerenoth"
		dynasty = "Ter-Jedhur"
		birth_date = 1008.1.1
		adm = 1
		dip = 4
		mil = 3
		female = yes
    }
	heir = {
 		name = "Udhelzius"
		monarch_name = "Udhelzius I"
		dynasty = "Ter-Jedhur"
		birth_date = 1049.1.1
		death_date = 1116.1.1
		claim = 100
		adm = 5
		dip = 2
		mil = 2
    }
}

1116.1.1 = {
	monarch = {
 		name = "Wasparwen"
		dynasty = "Ter-Shanbath"
		birth_date = 1073.1.1
		adm = 1
		dip = 4
		mil = 4
		female = yes
    }
	queen = {
 		name = "Engriath"
		dynasty = "Ter-Shanbath"
		birth_date = 1084.1.1
		adm = 5
		dip = 2
		mil = 3
    }
	heir = {
 		name = "Haerwen"
		monarch_name = "Haerwen I"
		dynasty = "Ter-Shanbath"
		birth_date = 1112.1.1
		death_date = 1175.1.1
		claim = 100
		adm = 2
		dip = 1
		mil = 2
		female = yes
    }
}

1175.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Harkir"
		monarch_name = "Harkir I"
		dynasty = "Ter-Uresur"
		birth_date = 1171.1.1
		death_date = 1189.1.1
		claim = 100
		adm = 0
		dip = 0
		mil = 6
    }
}

1189.1.1 = {
	monarch = {
 		name = "Rynmhes"
		dynasty = "Tir-Zongarwen"
		birth_date = 1137.1.1
		adm = 6
		dip = 3
		mil = 6
		female = yes
    }
	queen = {
 		name = "Famhor"
		dynasty = "Tir-Zongarwen"
		birth_date = 1150.1.1
		adm = 3
		dip = 2
		mil = 5
    }
	heir = {
 		name = "Berelnaris"
		monarch_name = "Berelnaris I"
		dynasty = "Tir-Zongarwen"
		birth_date = 1185.1.1
		death_date = 1255.1.1
		claim = 100
		adm = 0
		dip = 0
		mil = 4
    }
}

1255.1.1 = {
	monarch = {
 		name = "Uretaroth"
		dynasty = "Ter-Wirifaris"
		birth_date = 1218.1.1
		adm = 5
		dip = 0
		mil = 1
		female = yes
    }
	queen = {
 		name = "Tinparis"
		dynasty = "Ter-Wirifaris"
		birth_date = 1223.1.1
		adm = 1
		dip = 1
		mil = 3
    }
	heir = {
 		name = "Teredanyis"
		monarch_name = "Teredanyis I"
		dynasty = "Ter-Wirifaris"
		birth_date = 1250.1.1
		death_date = 1305.1.1
		claim = 100
		adm = 5
		dip = 6
		mil = 2
    }
}

1305.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Wanhan"
		monarch_name = "Wanhan I"
		dynasty = "Ter-Denferys"
		birth_date = 1302.1.1
		death_date = 1320.1.1
		claim = 100
		adm = 4
		dip = 3
		mil = 2
    }
}

1320.1.1 = {
	monarch = {
 		name = "Waidhor"
		dynasty = "Tir-Celebor"
		birth_date = 1272.1.1
		adm = 3
		dip = 2
		mil = 6
    }
}

1363.1.1 = {
	monarch = {
 		name = "Vyrpath"
		dynasty = "Ter-Wanvarys"
		birth_date = 1337.1.1
		adm = 5
		dip = 3
		mil = 1
		female = yes
    }
}

1423.1.1 = {
	monarch = {
 		name = "Lenenoth"
		dynasty = "Ter-Cryntaroth"
		birth_date = 1388.1.1
		adm = 4
		dip = 3
		mil = 5
    }
	queen = {
 		name = "Gangarwen"
		dynasty = "Ter-Cryntaroth"
		birth_date = 1396.1.1
		adm = 0
		dip = 1
		mil = 4
		female = yes
    }
	heir = {
 		name = "Anmeloth"
		monarch_name = "Anmeloth I"
		dynasty = "Ter-Cryntaroth"
		birth_date = 1419.1.1
		death_date = 1496.1.1
		claim = 100
		adm = 0
		dip = 4
		mil = 4
		female = yes
    }
}

1496.1.1 = {
	monarch = {
 		name = "Vranmyn"
		dynasty = "Tyr-Faezhina"
		birth_date = 1449.1.1
		adm = 5
		dip = 4
		mil = 1
		female = yes
    }
}

1553.1.1 = {
	monarch = {
 		name = "Vranzoth"
		dynasty = "Tor-Kirgeth"
		birth_date = 1505.1.1
		adm = 3
		dip = 3
		mil = 5
    }
	queen = {
 		name = "Faefani"
		dynasty = "Tor-Kirgeth"
		birth_date = 1524.1.1
		adm = 0
		dip = 1
		mil = 3
		female = yes
    }
	heir = {
 		name = "Ydhebor"
		monarch_name = "Ydhebor I"
		dynasty = "Tor-Kirgeth"
		birth_date = 1551.1.1
		death_date = 1606.1.1
		claim = 100
		adm = 4
		dip = 0
		mil = 2
    }
}

1606.1.1 = {
	monarch = {
 		name = "Cenlebor"
		dynasty = "Tor-Krankelor"
		birth_date = 1575.1.1
		adm = 0
		dip = 1
		mil = 5
    }
}

1703.1.1 = {
	monarch = {
 		name = "Yrgroth"
		dynasty = "Tyr-Pryslori"
		birth_date = 1667.1.1
		adm = 5
		dip = 0
		mil = 1
    }
}

1756.1.1 = {
	monarch = {
 		name = "Vranzoth"
		dynasty = "Tor-Zarkelor"
		birth_date = 1716.1.1
		adm = 3
		dip = 0
		mil = 5
    }
	queen = {
 		name = "Wanfaroth"
		dynasty = "Tor-Zarkelor"
		birth_date = 1708.1.1
		adm = 0
		dip = 5
		mil = 4
		female = yes
    }
}

1824.1.1 = {
	monarch = {
 		name = "Vrantan"
		dynasty = "Tir-Prynre"
		birth_date = 1773.1.1
		adm = 5
		dip = 4
		mil = 1
		female = yes
    }
	queen = {
 		name = "Vranferys"
		dynasty = "Tir-Prynre"
		birth_date = 1787.1.1
		adm = 2
		dip = 3
		mil = 0
    }
	heir = {
 		name = "Granmyn"
		monarch_name = "Granmyn I"
		dynasty = "Tir-Prynre"
		birth_date = 1813.1.1
		death_date = 1883.1.1
		claim = 100
		adm = 6
		dip = 2
		mil = 6
    }
}

1883.1.1 = {
	monarch = {
 		name = "Elthune"
		dynasty = "Ter-Tereyaris"
		birth_date = 1857.1.1
		adm = 6
		dip = 5
		mil = 6
		female = yes
    }
	queen = {
 		name = "Agarloth"
		dynasty = "Ter-Tereyaris"
		birth_date = 1842.1.1
		adm = 2
		dip = 3
		mil = 5
    }
	heir = {
 		name = "Vorzoth"
		monarch_name = "Vorzoth I"
		dynasty = "Ter-Tereyaris"
		birth_date = 1881.1.1
		death_date = 1970.1.1
		claim = 100
		adm = 3
		dip = 0
		mil = 6
		female = yes
    }
}

1970.1.1 = {
	monarch = {
 		name = "Wasparwen"
		dynasty = "Tir-Agzoth"
		birth_date = 1919.1.1
		adm = 5
		dip = 1
		mil = 0
		female = yes
    }
	queen = {
 		name = "Unarrenoth"
		dynasty = "Tir-Agzoth"
		birth_date = 1936.1.1
		adm = 5
		dip = 5
		mil = 2
    }
}

2020.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Engriath"
		monarch_name = "Engriath I"
		dynasty = "Ter-Cryntaroth"
		birth_date = 2011.1.1
		death_date = 2029.1.1
		claim = 100
		adm = 1
		dip = 0
		mil = 1
    }
}

2029.1.1 = {
	monarch = {
 		name = "Teredanyis"
		dynasty = "Ter-Daneparwen"
		birth_date = 1978.1.1
		adm = 4
		dip = 1
		mil = 3
    }
}

2086.1.1 = {
	monarch = {
 		name = "Cendhora"
		dynasty = "Ter-Prisferys"
		birth_date = 2065.1.1
		adm = 2
		dip = 0
		mil = 0
		female = yes
    }
	queen = {
 		name = "Shanrawyn"
		dynasty = "Ter-Prisferys"
		birth_date = 2054.1.1
		adm = 6
		dip = 6
		mil = 6
    }
	heir = {
 		name = "Roneparwen"
		monarch_name = "Roneparwen I"
		dynasty = "Ter-Prisferys"
		birth_date = 2084.1.1
		death_date = 2165.1.1
		claim = 100
		adm = 3
		dip = 4
		mil = 5
		female = yes
    }
}

2165.1.1 = {
	monarch = {
 		name = "Sindras"
		dynasty = "Tor-Inheselin"
		birth_date = 2144.1.1
		adm = 2
		dip = 1
		mil = 5
    }
	queen = {
 		name = "Heledhora"
		dynasty = "Tor-Inheselin"
		birth_date = 2132.1.1
		adm = 6
		dip = 6
		mil = 4
		female = yes
    }
	heir = {
 		name = "Uresur"
		monarch_name = "Uresur I"
		dynasty = "Tor-Inheselin"
		birth_date = 2163.1.1
		death_date = 2236.1.1
		claim = 100
		adm = 6
		dip = 3
		mil = 5
    }
}

2236.1.1 = {
	monarch = {
 		name = "Uredras"
		dynasty = "Tyr-Kranloth"
		birth_date = 2208.1.1
		adm = 6
		dip = 6
		mil = 5
    }
	queen = {
 		name = "Pryslori"
		dynasty = "Tyr-Kranloth"
		birth_date = 2196.1.1
		adm = 2
		dip = 5
		mil = 4
		female = yes
    }
	heir = {
 		name = "Roneparwen"
		monarch_name = "Roneparwen I"
		dynasty = "Tyr-Kranloth"
		birth_date = 2225.1.1
		death_date = 2325.1.1
		claim = 100
		adm = 6
		dip = 3
		mil = 3
		female = yes
    }
}

2325.1.1 = {
	monarch = {
 		name = "Orhorith"
		dynasty = "Tor-Sinbath"
		birth_date = 2300.1.1
		adm = 2
		dip = 4
		mil = 6
    }
	queen = {
 		name = "Inheselin"
		dynasty = "Tor-Sinbath"
		birth_date = 2301.1.1
		adm = 1
		dip = 1
		mil = 6
		female = yes
    }
	heir = {
 		name = "Finbath"
		monarch_name = "Finbath I"
		dynasty = "Tor-Sinbath"
		birth_date = 2324.1.1
		death_date = 2413.1.1
		claim = 100
		adm = 5
		dip = 6
		mil = 5
    }
}

2413.1.1 = {
	monarch = {
 		name = "Shanrawyn"
		dynasty = "Tor-Minerenoth"
		birth_date = 2362.1.1
		adm = 4
		dip = 6
		mil = 1
    }
}

2484.1.1 = {
	monarch = {
 		name = "Elthune"
		dynasty = "Tor-Vorzoth"
		birth_date = 2466.1.1
		adm = 2
		dip = 5
		mil = 5
		female = yes
    }
	queen = {
 		name = "Zidaring"
		dynasty = "Tor-Vorzoth"
		birth_date = 2456.1.1
		adm = 6
		dip = 3
		mil = 4
    }
	heir = {
 		name = "Niribys"
		monarch_name = "Niribys I"
		dynasty = "Tor-Vorzoth"
		birth_date = 2472.1.1
		death_date = 2562.1.1
		claim = 100
		adm = 3
		dip = 2
		mil = 3
    }
}

