government = monarchy
government_rank = 5
mercantilism = 1
technology_group = atmora_tg
religion = dragon_cult
primary_culture = islander
capital = 407

54.1.1 = {
	monarch = {
 		name = "Knorchih"
		dynasty = "O'Drarhoth"
		birth_date = 7.1.1
		adm = 5
		dip = 4
		mil = 4
		female = yes
    }
}

103.1.1 = {
	monarch = {
 		name = "Guvit"
		dynasty = "U'Craajot"
		birth_date = 52.1.1
		adm = 4
		dip = 3
		mil = 1
    }
}

183.1.1 = {
	monarch = {
 		name = "Rerdosqeg"
		dynasty = "I'Throtid"
		birth_date = 139.1.1
		adm = 2
		dip = 2
		mil = 5
    }
	queen = {
 		name = "Gnahraaq"
		dynasty = "I'Throtid"
		birth_date = 152.1.1
		adm = 6
		dip = 1
		mil = 3
		female = yes
    }
	heir = {
 		name = "Throtid"
		monarch_name = "Throtid I"
		dynasty = "I'Throtid"
		birth_date = 169.1.1
		death_date = 226.1.1
		claim = 100
		adm = 3
		dip = 6
		mil = 2
		female = yes
    }
}

226.1.1 = {
	monarch = {
 		name = "Crodguac"
		dynasty = "O'Irzat"
		birth_date = 185.1.1
		adm = 2
		dip = 3
		mil = 3
    }
}

285.1.1 = {
	monarch = {
 		name = "Throrrit"
		dynasty = "I'Qaervu"
		birth_date = 266.1.1
		adm = 0
		dip = 2
		mil = 6
		female = yes
    }
	queen = {
 		name = "Zusqaat"
		dynasty = "I'Qaervu"
		birth_date = 247.1.1
		adm = 4
		dip = 0
		mil = 5
    }
	heir = {
 		name = "Cukad"
		monarch_name = "Cukad I"
		dynasty = "I'Qaervu"
		birth_date = 282.1.1
		death_date = 354.1.1
		claim = 100
		adm = 4
		dip = 4
		mil = 6
    }
}

354.1.1 = {
	monarch = {
 		name = "Vatakkiag"
		dynasty = "O'Khoqhueq"
		birth_date = 308.1.1
		adm = 4
		dip = 0
		mil = 0
    }
	queen = {
 		name = "Uthuzzai"
		dynasty = "O'Khoqhueq"
		birth_date = 307.1.1
		adm = 1
		dip = 6
		mil = 6
		female = yes
    }
	heir = {
 		name = "Knozud"
		monarch_name = "Knozud I"
		dynasty = "O'Khoqhueq"
		birth_date = 350.1.1
		death_date = 419.1.1
		claim = 100
		adm = 4
		dip = 4
		mil = 5
		female = yes
    }
}

419.1.1 = {
	monarch = {
 		name = "Guqqerix"
		dynasty = "I'Ukrud"
		birth_date = 376.1.1
		adm = 0
		dip = 6
		mil = 0
    }
}

469.1.1 = {
	monarch = {
 		name = "Chitren"
		dynasty = "U'Odgiz"
		birth_date = 437.1.1
		adm = 5
		dip = 5
		mil = 4
    }
}

512.1.1 = {
	monarch = {
 		name = "Vatakkiag"
		dynasty = "I'Driaze"
		birth_date = 481.1.1
		adm = 4
		dip = 4
		mil = 0
    }
}

597.1.1 = {
	monarch = {
 		name = "Crusqauz"
		dynasty = "I'Taeqhaq"
		birth_date = 545.1.1
		adm = 6
		dip = 5
		mil = 2
    }
}

677.1.1 = {
	monarch = {
 		name = "Guqqerix"
		dynasty = "I'Kututh"
		birth_date = 637.1.1
		adm = 4
		dip = 5
		mil = 5
    }
}

740.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Khrihnid"
		monarch_name = "Khrihnid I"
		dynasty = "E'Enzac"
		birth_date = 736.1.1
		death_date = 754.1.1
		claim = 100
		adm = 1
		dip = 0
		mil = 5
		female = yes
    }
}

754.1.1 = {
	monarch = {
 		name = "Kogruc"
		dynasty = "I'Thirchoh"
		birth_date = 712.1.1
		adm = 0
		dip = 3
		mil = 6
    }
	queen = {
 		name = "Vunqiakki"
		dynasty = "I'Thirchoh"
		birth_date = 702.1.1
		adm = 4
		dip = 2
		mil = 5
		female = yes
    }
	heir = {
 		name = "Iaqran"
		monarch_name = "Iaqran I"
		dynasty = "I'Thirchoh"
		birth_date = 743.1.1
		death_date = 814.1.1
		claim = 100
		adm = 1
		dip = 0
		mil = 4
		female = yes
    }
}

814.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Eikgiaraz"
		monarch_name = "Eikgiaraz I"
		dynasty = "E'Drarvaarreit"
		birth_date = 814.1.1
		death_date = 832.1.1
		claim = 100
		adm = 6
		dip = 6
		mil = 0
    }
}

832.1.1 = {
	monarch = {
 		name = "Kututh"
		dynasty = "U'Irchox"
		birth_date = 803.1.1
		adm = 2
		dip = 1
		mil = 3
		female = yes
    }
}

869.1.1 = {
	monarch = {
 		name = "Iakzic"
		dynasty = "I'Tirgeza"
		birth_date = 842.1.1
		adm = 0
		dip = 0
		mil = 6
    }
	queen = {
 		name = "Khrurhi"
		dynasty = "I'Tirgeza"
		birth_date = 818.1.1
		adm = 4
		dip = 5
		mil = 5
		female = yes
    }
}

964.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Orvikkec"
		monarch_name = "Orvikkec I"
		dynasty = "U'Arqirraq"
		birth_date = 962.1.1
		death_date = 980.1.1
		claim = 100
		adm = 5
		dip = 3
		mil = 3
    }
}

980.1.1 = {
	monarch = {
 		name = "Qagnaq"
		dynasty = "I'Evo"
		birth_date = 950.1.1
		adm = 4
		dip = 6
		mil = 3
		female = yes
    }
	queen = {
 		name = "Cizuukuuk"
		dynasty = "I'Evo"
		birth_date = 934.1.1
		adm = 5
		dip = 2
		mil = 5
    }
}

1041.1.1 = {
	monarch = {
 		name = "Khonqe"
		dynasty = "U'Kogruc"
		birth_date = 1022.1.1
		adm = 6
		dip = 4
		mil = 6
		female = yes
    }
}

1092.1.1 = {
	monarch = {
 		name = "Gnudris"
		dynasty = "O'Tiknici"
		birth_date = 1047.1.1
		adm = 4
		dip = 6
		mil = 6
		female = yes
    }
}

1143.1.1 = {
	monarch = {
 		name = "Rerdosqeg"
		dynasty = "E'Chited"
		birth_date = 1112.1.1
		adm = 2
		dip = 5
		mil = 3
    }
}

1195.1.1 = {
	monarch = {
 		name = "Kiqqaz"
		dynasty = "E'Ingroriaq"
		birth_date = 1163.1.1
		adm = 4
		dip = 6
		mil = 4
    }
}

1267.1.1 = {
	monarch = {
 		name = "Droktid"
		dynasty = "I'Khrudtoqu"
		birth_date = 1214.1.1
		adm = 2
		dip = 5
		mil = 1
    }
}

1304.1.1 = {
	monarch = {
 		name = "Chinzekkos"
		dynasty = "O'Rhaoqrukhae"
		birth_date = 1286.1.1
		adm = 0
		dip = 5
		mil = 5
		female = yes
    }
}

1366.1.1 = {
	monarch = {
 		name = "Chitren"
		dynasty = "I'Khrudtoqu"
		birth_date = 1341.1.1
		adm = 5
		dip = 4
		mil = 1
    }
	queen = {
 		name = "Thinnecith"
		dynasty = "I'Khrudtoqu"
		birth_date = 1327.1.1
		adm = 1
		dip = 5
		mil = 4
		female = yes
    }
	heir = {
 		name = "Kraqqa"
		monarch_name = "Kraqqa I"
		dynasty = "I'Khrudtoqu"
		birth_date = 1357.1.1
		death_date = 1447.1.1
		claim = 100
		adm = 5
		dip = 4
		mil = 3
    }
}

1447.1.1 = {
	monarch = {
 		name = "Kheqrani"
		dynasty = "E'Caiqqit"
		birth_date = 1416.1.1
		adm = 4
		dip = 0
		mil = 3
		female = yes
    }
}

1513.1.1 = {
	monarch = {
 		name = "Driaze"
		dynasty = "I'Vaiken"
		birth_date = 1474.1.1
		adm = 3
		dip = 6
		mil = 6
		female = yes
    }
	queen = {
 		name = "Nujeq"
		dynasty = "I'Vaiken"
		birth_date = 1462.1.1
		adm = 0
		dip = 5
		mil = 5
    }
	heir = {
 		name = "Crungud"
		monarch_name = "Crungud I"
		dynasty = "I'Vaiken"
		birth_date = 1500.1.1
		death_date = 1605.1.1
		claim = 100
		adm = 3
		dip = 3
		mil = 4
    }
}

1605.1.1 = {
	monarch = {
 		name = "Raagrud"
		dynasty = "E'Aqre"
		birth_date = 1572.1.1
		adm = 6
		dip = 5
		mil = 0
    }
	queen = {
 		name = "Thraerra"
		dynasty = "E'Aqre"
		birth_date = 1566.1.1
		adm = 3
		dip = 3
		mil = 6
		female = yes
    }
	heir = {
 		name = "Qendriqeq"
		monarch_name = "Qendriqeq I"
		dynasty = "E'Aqre"
		birth_date = 1594.1.1
		death_date = 1703.1.1
		claim = 100
		adm = 0
		dip = 2
		mil = 5
    }
}

1703.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Kijiad"
		monarch_name = "Kijiad I"
		dynasty = "O'Khurrokhua"
		birth_date = 1695.1.1
		death_date = 1713.1.1
		claim = 100
		adm = 5
		dip = 1
		mil = 1
    }
}

1713.1.1 = {
	monarch = {
 		name = "Grurqadox"
		dynasty = "O'Chuerchi"
		birth_date = 1684.1.1
		adm = 5
		dip = 4
		mil = 2
    }
	queen = {
 		name = "Rarchoh"
		dynasty = "O'Chuerchi"
		birth_date = 1693.1.1
		adm = 5
		dip = 1
		mil = 3
		female = yes
    }
	heir = {
 		name = "Nurdun"
		monarch_name = "Nurdun I"
		dynasty = "O'Chuerchi"
		birth_date = 1707.1.1
		death_date = 1809.1.1
		claim = 100
		adm = 2
		dip = 6
		mil = 2
    }
}

1809.1.1 = {
	monarch = {
 		name = "Khrihnid"
		dynasty = "E'Aqre"
		birth_date = 1759.1.1
		adm = 0
		dip = 6
		mil = 5
		female = yes
    }
	queen = {
 		name = "Irchox"
		dynasty = "E'Aqre"
		birth_date = 1757.1.1
		adm = 4
		dip = 4
		mil = 4
    }
}

1883.1.1 = {
	monarch = {
 		name = "Auqet"
		dynasty = "U'Gerrikuk"
		birth_date = 1843.1.1
		adm = 2
		dip = 3
		mil = 1
    }
	queen = {
 		name = "Throtid"
		dynasty = "U'Gerrikuk"
		birth_date = 1850.1.1
		adm = 6
		dip = 2
		mil = 0
		female = yes
    }
}

1959.1.1 = {
	monarch = {
 		name = "Bhirgeh"
		dynasty = "O'Erhiaq"
		birth_date = 1920.1.1
		adm = 4
		dip = 1
		mil = 4
		female = yes
    }
}

1999.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Krivuax"
		monarch_name = "Krivuax I"
		dynasty = "U'Krudrugrox"
		birth_date = 1998.1.1
		death_date = 2016.1.1
		claim = 100
		adm = 5
		dip = 5
		mil = 1
    }
}

2016.1.1 = {
	monarch = {
 		name = "Gujon"
		dynasty = "I'Qungadhed"
		birth_date = 1995.1.1
		adm = 4
		dip = 1
		mil = 2
    }
}

2086.1.1 = {
	monarch = {
 		name = "Cirqishod"
		dynasty = "U'Krivrun"
		birth_date = 2044.1.1
		adm = 2
		dip = 1
		mil = 5
		female = yes
    }
}

2128.1.1 = {
	monarch = {
 		name = "Redronduig"
		dynasty = "I'Tirgeza"
		birth_date = 2086.1.1
		adm = 1
		dip = 0
		mil = 2
    }
	queen = {
 		name = "Xoka"
		dynasty = "I'Tirgeza"
		birth_date = 2104.1.1
		adm = 5
		dip = 5
		mil = 1
		female = yes
    }
	heir = {
 		name = "Krivuax"
		monarch_name = "Krivuax I"
		dynasty = "I'Tirgeza"
		birth_date = 2126.1.1
		death_date = 2221.1.1
		claim = 100
		adm = 1
		dip = 4
		mil = 0
    }
}

2221.1.1 = {
	monarch = {
 		name = "Evo"
		dynasty = "U'Eikgiaraz"
		birth_date = 2182.1.1
		adm = 4
		dip = 5
		mil = 2
		female = yes
    }
}

2278.1.1 = {
	monarch = {
 		name = "Cirqishod"
		dynasty = "I'Gnudris"
		birth_date = 2251.1.1
		adm = 2
		dip = 5
		mil = 6
		female = yes
    }
}

2321.1.1 = {
	monarch = {
 		name = "Redronduig"
		dynasty = "I'Rivraod"
		birth_date = 2293.1.1
		adm = 1
		dip = 4
		mil = 3
    }
	queen = {
 		name = "Kevruzze"
		dynasty = "I'Rivraod"
		birth_date = 2270.1.1
		adm = 5
		dip = 2
		mil = 2
		female = yes
    }
	heir = {
 		name = "Gasquh"
		monarch_name = "Gasquh I"
		dynasty = "I'Rivraod"
		birth_date = 2309.1.1
		death_date = 2360.1.1
		claim = 100
		adm = 1
		dip = 1
		mil = 1
		female = yes
    }
}

2360.1.1 = {
	monarch = {
 		name = "Aivug"
		dynasty = "O'Gneqruh"
		birth_date = 2319.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2448.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Viaqruq"
		monarch_name = "Viaqruq I"
		dynasty = "O'Rhonqoth"
		birth_date = 2436.1.1
		death_date = 2454.1.1
		claim = 100
		adm = 5
		dip = 6
		mil = 1
    }
}

2454.1.1 = {
	monarch = {
 		name = "Gudrucret"
		dynasty = "O'Khoqhueq"
		birth_date = 2414.1.1
		adm = 4
		dip = 3
		mil = 1
    }
	queen = {
 		name = "Kevruzze"
		dynasty = "O'Khoqhueq"
		birth_date = 2434.1.1
		adm = 1
		dip = 1
		mil = 0
		female = yes
    }
	heir = {
 		name = "Nerdok"
		monarch_name = "Nerdok I"
		dynasty = "O'Khoqhueq"
		birth_date = 2445.1.1
		death_date = 2501.1.1
		claim = 100
		adm = 5
		dip = 0
		mil = 6
    }
}

