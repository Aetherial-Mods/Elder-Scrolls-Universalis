government = tribal
government_rank = 3
mercantilism = 1
technology_group = elsweyr_tg
religion = khajiiti_pantheon
primary_culture = khajiiti
capital = 5226

54.1.1 = {
	monarch = {
 		name = "Tarlar"
		dynasty = "Rawinai"
		birth_date = 24.1.1
		adm = 0
		dip = 0
		mil = 1
    }
}

136.1.1 = {
	monarch = {
 		name = "Nakmargo"
		dynasty = "Kimnirn"
		birth_date = 115.1.1
		adm = 5
		dip = 6
		mil = 5
    }
}

180.1.1 = {
	monarch = {
 		name = "Khizuna"
		dynasty = "J'arr"
		birth_date = 127.1.1
		adm = 3
		dip = 5
		mil = 1
		female = yes
    }
}

227.1.1 = {
	monarch = {
 		name = "Bihargo"
		dynasty = "Zoarjhan"
		birth_date = 209.1.1
		adm = 2
		dip = 4
		mil = 5
    }
}

304.1.1 = {
	monarch = {
 		name = "Tarazdarr"
		dynasty = "Dar'tesh"
		birth_date = 271.1.1
		adm = 4
		dip = 6
		mil = 6
    }
}

347.1.1 = {
	monarch = {
 		name = "Nakiri"
		dynasty = "Khavanadi"
		birth_date = 303.1.1
		adm = 2
		dip = 5
		mil = 3
    }
}

430.1.1 = {
	monarch = {
 		name = "Tullar-dra"
		dynasty = "Ja'bar"
		birth_date = 392.1.1
		adm = 0
		dip = 4
		mil = 0
    }
}

496.1.1 = {
	monarch = {
 		name = "Nirajlar"
		dynasty = "Kavandi"
		birth_date = 448.1.1
		adm = 1
		dip = 1
		mil = 5
    }
}

543.1.1 = {
	monarch = {
 		name = "Ghadffer"
		dynasty = "Ranrjo"
		birth_date = 502.1.1
		adm = 6
		dip = 0
		mil = 1
    }
}

598.1.1 = {
	monarch = {
 		name = "Mahargo"
		dynasty = "Kesharsha"
		birth_date = 555.1.1
		adm = 4
		dip = 0
		mil = 5
    }
}

684.1.1 = {
	monarch = {
 		name = "Tsin'roh"
		dynasty = "Ranatasarr"
		birth_date = 634.1.1
		adm = 0
		dip = 1
		mil = 0
    }
}

721.1.1 = {
	monarch = {
 		name = "Rilbirri"
		dynasty = "Ma'siri"
		birth_date = 679.1.1
		adm = 5
		dip = 0
		mil = 4
		female = yes
    }
}

808.1.1 = {
	monarch = {
 		name = "Kalgur"
		dynasty = "Jo'arkhu"
		birth_date = 781.1.1
		adm = 0
		dip = 1
		mil = 5
    }
}

899.1.1 = {
	monarch = {
 		name = "Bulag"
		dynasty = "Roudavi"
		birth_date = 849.1.1
		adm = 5
		dip = 1
		mil = 2
    }
}

970.1.1 = {
	monarch = {
 		name = "Kaszo"
		dynasty = "Zoarjhan"
		birth_date = 924.1.1
		adm = 4
		dip = 0
		mil = 6
    }
}

1036.1.1 = {
	monarch = {
 		name = "Dehdri"
		dynasty = "Ma'jahirr"
		birth_date = 1013.1.1
		adm = 2
		dip = 6
		mil = 2
		female = yes
    }
}

1105.1.1 = {
	monarch = {
 		name = "Unnur"
		dynasty = "Tovikmnirn"
		birth_date = 1060.1.1
		adm = 0
		dip = 5
		mil = 6
    }
}

1145.1.1 = {
	monarch = {
 		name = "Nuzon"
		dynasty = "Havnusopor"
		birth_date = 1097.1.1
		adm = 5
		dip = 4
		mil = 3
    }
}

1236.1.1 = {
	monarch = {
 		name = "Lalimeh"
		dynasty = "Kazasha"
		birth_date = 1199.1.1
		adm = 4
		dip = 4
		mil = 6
		female = yes
    }
}

1313.1.1 = {
	monarch = {
 		name = "Dazreso-jo"
		dynasty = "Ma'tasarr"
		birth_date = 1275.1.1
		adm = 2
		dip = 3
		mil = 3
		female = yes
    }
}

1366.1.1 = {
	monarch = {
 		name = "Ulibaz"
		dynasty = "Shoava"
		birth_date = 1313.1.1
		adm = 4
		dip = 4
		mil = 5
    }
}

1452.1.1 = {
	monarch = {
 		name = "Nurzo"
		dynasty = "Bhijhad"
		birth_date = 1405.1.1
		adm = 2
		dip = 3
		mil = 1
    }
}

1494.1.1 = {
	monarch = {
 		name = "Whickmuz"
		dynasty = "Modumiwa"
		birth_date = 1448.1.1
		adm = 0
		dip = 2
		mil = 5
    }
}

1542.1.1 = {
	monarch = {
 		name = "Pinumur"
		dynasty = "Wadaiq"
		birth_date = 1499.1.1
		adm = 6
		dip = 2
		mil = 1
    }
}

1641.1.1 = {
	monarch = {
 		name = "Magdi"
		dynasty = "Raioni"
		birth_date = 1609.1.1
		adm = 4
		dip = 1
		mil = 5
		female = yes
    }
}

1701.1.1 = {
	monarch = {
 		name = "Dro-Dara"
		dynasty = "Ma'jahirr"
		birth_date = 1653.1.1
		adm = 2
		dip = 0
		mil = 2
    }
}

1788.1.1 = {
	monarch = {
 		name = "Zadaza"
		dynasty = "Baradhari"
		birth_date = 1747.1.1
		adm = 0
		dip = 6
		mil = 5
		female = yes
    }
}

1871.1.1 = {
	monarch = {
 		name = "Pinar"
		dynasty = "Zarhan"
		birth_date = 1834.1.1
		adm = 6
		dip = 6
		mil = 2
    }
}

1958.1.1 = {
	monarch = {
 		name = "Maezalaya"
		dynasty = "Hasssien"
		birth_date = 1915.1.1
		adm = 0
		dip = 0
		mil = 4
		female = yes
    }
}

2013.1.1 = {
	monarch = {
 		name = "Do'mazir"
		dynasty = "Zan'esi"
		birth_date = 1990.1.1
		adm = 6
		dip = 6
		mil = 0
    }
}

2090.1.1 = {
	monarch = {
 		name = "Kirrdul"
		dynasty = "Kavandi"
		birth_date = 2065.1.1
		adm = 4
		dip = 5
		mil = 4
    }
}

2181.1.1 = {
	monarch = {
 		name = "Elzhar"
		dynasty = "Do'randru-Jo"
		birth_date = 2157.1.1
		adm = 2
		dip = 4
		mil = 1
    }
}

2280.1.1 = {
	monarch = {
 		name = "Za'ji"
		dynasty = "M'arkhu"
		birth_date = 2242.1.1
		adm = 0
		dip = 4
		mil = 4
    }
}

2357.1.1 = {
	monarch = {
 		name = "Sezdi-dra"
		dynasty = "Jo'arkhu"
		birth_date = 2325.1.1
		adm = 6
		dip = 3
		mil = 1
		female = yes
    }
}

2401.1.1 = {
	monarch = {
 		name = "Kigum-dar"
		dynasty = "Kitanni"
		birth_date = 2367.1.1
		adm = 4
		dip = 2
		mil = 5
    }
}

2499.1.1 = {
	monarch = {
 		name = "Elazura"
		dynasty = "Jodara"
		birth_date = 2469.1.1
		adm = 2
		dip = 1
		mil = 1
		female = yes
    }
}

