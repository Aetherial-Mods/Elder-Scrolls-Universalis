government = monarchy
government_rank = 3
mercantilism = 1
technology_group = yokudan_tg
religion = redguard_pantheon
primary_culture = redguard
capital = 1474

54.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Harimal"
		monarch_name = "Harimal I"
		dynasty = "M'urcti"
		birth_date = 50.1.1
		death_date = 68.1.1
		claim = 100
		adm = 1
		dip = 0
		mil = 4
    }
}

68.1.1 = {
	monarch = {
 		name = "Horayya"
		dynasty = "Dudlim"
		birth_date = 32.1.1
		adm = 0
		dip = 3
		mil = 5
		female = yes
    }
}

154.1.1 = {
	monarch = {
 		name = "Nafefta"
		dynasty = "Lakir"
		birth_date = 121.1.1
		adm = 5
		dip = 2
		mil = 1
		female = yes
    }
}

196.1.1 = {
	monarch = {
 		name = "Irvalam"
		dynasty = "Toran"
		birth_date = 161.1.1
		adm = 3
		dip = 1
		mil = 5
    }
}

254.1.1 = {
	monarch = {
 		name = "Basri"
		dynasty = "Shelamon"
		birth_date = 208.1.1
		adm = 2
		dip = 1
		mil = 1
    }
}

337.1.1 = {
	monarch = {
 		name = "Shiri"
		dynasty = "Khirdrn"
		birth_date = 316.1.1
		adm = 0
		dip = 0
		mil = 5
		female = yes
    }
	queen = {
 		name = "Frandar"
		dynasty = "Khirdrn"
		birth_date = 303.1.1
		adm = 4
		dip = 5
		mil = 4
    }
	heir = {
 		name = "Seral"
		monarch_name = "Seral I"
		dynasty = "Khirdrn"
		birth_date = 336.1.1
		death_date = 388.1.1
		claim = 100
		adm = 1
		dip = 4
		mil = 3
    }
}

388.1.1 = {
	monarch = {
 		name = "Ilmaha"
		dynasty = "Imindyn"
		birth_date = 362.1.1
		adm = 3
		dip = 5
		mil = 5
		female = yes
    }
}

454.1.1 = {
	monarch = {
 		name = "Bolura"
		dynasty = "Fafyler"
		birth_date = 405.1.1
		adm = 5
		dip = 6
		mil = 0
		female = yes
    }
	queen = {
 		name = "Adruzan"
		dynasty = "Fafyler"
		birth_date = 407.1.1
		adm = 6
		dip = 3
		mil = 1
    }
}

500.1.1 = {
	monarch = {
 		name = "Jonis"
		dynasty = "Jartnado"
		birth_date = 456.1.1
		adm = 0
		dip = 4
		mil = 3
    }
	queen = {
 		name = "Hecua"
		dynasty = "Jartnado"
		birth_date = 464.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

572.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Kamandi"
		monarch_name = "Kamandi I"
		dynasty = "Firink"
		birth_date = 566.1.1
		death_date = 584.1.1
		claim = 100
		adm = 1
		dip = 5
		mil = 2
    }
}

584.1.1 = {
	monarch = {
 		name = "Khabzuur"
		dynasty = "Talhik"
		birth_date = 563.1.1
		adm = 1
		dip = 1
		mil = 2
    }
	queen = {
 		name = "Izdiya"
		dynasty = "Talhik"
		birth_date = 561.1.1
		adm = 5
		dip = 0
		mil = 1
		female = yes
    }
	heir = {
 		name = "Wallace"
		monarch_name = "Wallace I"
		dynasty = "Talhik"
		birth_date = 573.1.1
		death_date = 630.1.1
		claim = 100
		adm = 6
		dip = 0
		mil = 5
    }
}

630.1.1 = {
	monarch = {
 		name = "Cattrice"
		dynasty = "Faliaron"
		birth_date = 609.1.1
		adm = 2
		dip = 2
		mil = 1
		female = yes
    }
	queen = {
 		name = "Aizar"
		dynasty = "Faliaron"
		birth_date = 594.1.1
		adm = 6
		dip = 0
		mil = 0
    }
}

704.1.1 = {
	monarch = {
 		name = "Kalrzud"
		dynasty = "Greelan"
		birth_date = 685.1.1
		adm = 4
		dip = 6
		mil = 3
    }
	queen = {
 		name = "Horayya"
		dynasty = "Greelan"
		birth_date = 685.1.1
		adm = 1
		dip = 5
		mil = 2
		female = yes
    }
	heir = {
 		name = "Ramadad"
		monarch_name = "Ramadad I"
		dynasty = "Greelan"
		birth_date = 697.1.1
		death_date = 772.1.1
		claim = 100
		adm = 5
		dip = 3
		mil = 1
    }
}

772.1.1 = {
	monarch = {
 		name = "Travofia"
		dynasty = "Killba"
		birth_date = 726.1.1
		adm = 4
		dip = 0
		mil = 2
		female = yes
    }
}

815.1.1 = {
	monarch = {
 		name = "Oiarah"
		dynasty = "Fairgki"
		birth_date = 783.1.1
		adm = 2
		dip = 6
		mil = 5
		female = yes
    }
}

909.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Dahnadreel"
		monarch_name = "Dahnadreel I"
		dynasty = "Nazld"
		birth_date = 897.1.1
		death_date = 915.1.1
		claim = 100
		adm = 6
		dip = 1
		mil = 5
		female = yes
    }
}

915.1.1 = {
	monarch = {
 		name = "Qabin"
		dynasty = "Kevsa"
		birth_date = 891.1.1
		adm = 6
		dip = 4
		mil = 6
    }
}

971.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Nebzez"
		monarch_name = "Nebzez I"
		dynasty = "Rinek"
		birth_date = 971.1.1
		death_date = 989.1.1
		claim = 100
		adm = 0
		dip = 2
		mil = 3
    }
}

989.1.1 = {
	monarch = {
 		name = "Ehsan"
		dynasty = "Fairgki"
		birth_date = 946.1.1
		adm = 5
		dip = 1
		mil = 0
    }
}

1044.1.1 = {
	monarch = {
 		name = "Hallin"
		dynasty = "Irganan"
		birth_date = 1018.1.1
		adm = 3
		dip = 0
		mil = 4
    }
}

1137.1.1 = {
	monarch = {
 		name = "Amal"
		dynasty = "Dindal"
		birth_date = 1108.1.1
		adm = 1
		dip = 6
		mil = 0
    }
	queen = {
 		name = "Khezibanh"
		dynasty = "Dindal"
		birth_date = 1101.1.1
		adm = 5
		dip = 5
		mil = 6
		female = yes
    }
}

1231.1.1 = {
	monarch = {
 		name = "Ilafa"
		dynasty = "Wilbo"
		birth_date = 1181.1.1
		adm = 3
		dip = 4
		mil = 3
		female = yes
    }
	queen = {
 		name = "Relan"
		dynasty = "Wilbo"
		birth_date = 1180.1.1
		adm = 0
		dip = 2
		mil = 2
    }
	heir = {
 		name = "Halder"
		monarch_name = "Halder I"
		dynasty = "Wilbo"
		birth_date = 1224.1.1
		death_date = 1327.1.1
		claim = 100
		adm = 4
		dip = 1
		mil = 1
    }
}

1327.1.1 = {
	monarch = {
 		name = "Shehrbah"
		dynasty = "Namanet"
		birth_date = 1297.1.1
		adm = 0
		dip = 2
		mil = 3
		female = yes
    }
}

1419.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Sahima"
		monarch_name = "Sahima I"
		dynasty = "Rhajtha"
		birth_date = 1417.1.1
		death_date = 1435.1.1
		claim = 100
		adm = 1
		dip = 6
		mil = 1
		female = yes
    }
}

1435.1.1 = {
	monarch = {
 		name = "Ianwas"
		dynasty = "Nissan"
		birth_date = 1408.1.1
		adm = 0
		dip = 3
		mil = 2
		female = yes
    }
}

1503.1.1 = {
	monarch = {
 		name = "Balabar"
		dynasty = "Wilbo"
		birth_date = 1469.1.1
		adm = 5
		dip = 2
		mil = 5
    }
	queen = {
 		name = "Afareen"
		dynasty = "Wilbo"
		birth_date = 1478.1.1
		adm = 2
		dip = 1
		mil = 4
		female = yes
    }
	heir = {
 		name = "Mehdbeq"
		monarch_name = "Mehdbeq I"
		dynasty = "Wilbo"
		birth_date = 1496.1.1
		death_date = 1547.1.1
		claim = 100
		adm = 2
		dip = 4
		mil = 5
    }
}

1547.1.1 = {
	monarch = {
 		name = "Caminda"
		dynasty = "Greelan"
		birth_date = 1529.1.1
		adm = 2
		dip = 0
		mil = 5
		female = yes
    }
}

1633.1.1 = {
	monarch = {
 		name = "Temzukar"
		dynasty = "Ghuldka"
		birth_date = 1596.1.1
		adm = 0
		dip = 0
		mil = 2
    }
	queen = {
 		name = "Ghazaleh"
		dynasty = "Ghuldka"
		birth_date = 1595.1.1
		adm = 4
		dip = 5
		mil = 1
		female = yes
    }
	heir = {
 		name = "Shamar"
		monarch_name = "Shamar I"
		dynasty = "Ghuldka"
		birth_date = 1626.1.1
		death_date = 1679.1.1
		claim = 100
		adm = 1
		dip = 4
		mil = 0
    }
}

1679.1.1 = {
	monarch = {
 		name = "Issal"
		dynasty = "Dindal"
		birth_date = 1640.1.1
		adm = 3
		dip = 5
		mil = 2
    }
	queen = {
 		name = "Haballa"
		dynasty = "Dindal"
		birth_date = 1641.1.1
		adm = 0
		dip = 4
		mil = 1
		female = yes
    }
	heir = {
 		name = "Hawafa"
		monarch_name = "Hawafa I"
		dynasty = "Dindal"
		birth_date = 1678.1.1
		death_date = 1714.1.1
		claim = 100
		adm = 4
		dip = 2
		mil = 0
		female = yes
    }
}

1714.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Mizaida"
		monarch_name = "Mizaida I"
		dynasty = "Kaydntias"
		birth_date = 1705.1.1
		death_date = 1723.1.1
		claim = 100
		adm = 2
		dip = 1
		mil = 4
		female = yes
    }
}

1723.1.1 = {
	monarch = {
 		name = "Nahrina"
		dynasty = "Rlogtha"
		birth_date = 1687.1.1
		adm = 2
		dip = 5
		mil = 4
		female = yes
    }
	queen = {
 		name = "Maiko"
		dynasty = "Rlogtha"
		birth_date = 1692.1.1
		adm = 6
		dip = 3
		mil = 3
    }
	heir = {
 		name = "Avik"
		monarch_name = "Avik I"
		dynasty = "Rlogtha"
		birth_date = 1717.1.1
		death_date = 1759.1.1
		claim = 100
		adm = 6
		dip = 0
		mil = 4
    }
}

1759.1.1 = {
	monarch = {
 		name = "Nebezh"
		dynasty = "Khish-E"
		birth_date = 1720.1.1
		adm = 5
		dip = 3
		mil = 5
    }
	queen = {
 		name = "Airena"
		dynasty = "Khish-E"
		birth_date = 1729.1.1
		adm = 2
		dip = 2
		mil = 4
		female = yes
    }
	heir = {
 		name = "Mirdamir"
		monarch_name = "Mirdamir I"
		dynasty = "Khish-E"
		birth_date = 1757.1.1
		death_date = 1805.1.1
		claim = 100
		adm = 6
		dip = 0
		mil = 2
    }
}

1805.1.1 = {
	monarch = {
 		name = "Cartos"
		dynasty = "Ghuldka"
		birth_date = 1772.1.1
		adm = 2
		dip = 2
		mil = 5
    }
}

1884.1.1 = {
	monarch = {
 		name = "Tohrzh"
		dynasty = "Glanna"
		birth_date = 1840.1.1
		adm = 0
		dip = 1
		mil = 1
    }
	queen = {
 		name = "Sahima"
		dynasty = "Glanna"
		birth_date = 1847.1.1
		adm = 4
		dip = 6
		mil = 0
		female = yes
    }
	heir = {
 		name = "Sivash"
		monarch_name = "Sivash I"
		dynasty = "Glanna"
		birth_date = 1884.1.1
		death_date = 1982.1.1
		claim = 100
		adm = 1
		dip = 5
		mil = 6
    }
}

1982.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Balabar"
		monarch_name = "Balabar I"
		dynasty = "Nhoste"
		birth_date = 1979.1.1
		death_date = 1997.1.1
		claim = 100
		adm = 6
		dip = 4
		mil = 3
    }
}

1997.1.1 = {
	monarch = {
 		name = "Camaron"
		dynasty = "Nistolm"
		birth_date = 1960.1.1
		adm = 5
		dip = 0
		mil = 3
    }
}

2068.1.1 = {
	monarch = {
 		name = "Jhiinval"
		dynasty = "M'efylur"
		birth_date = 2022.1.1
		adm = 4
		dip = 0
		mil = 0
    }
}

2139.1.1 = {
	monarch = {
 		name = "Dalamar"
		dynasty = "Provmin"
		birth_date = 2118.1.1
		adm = 2
		dip = 6
		mil = 4
    }
	queen = {
 		name = "Manizheh"
		dynasty = "Provmin"
		birth_date = 2095.1.1
		adm = 6
		dip = 4
		mil = 3
		female = yes
    }
	heir = {
 		name = "Bahbiq"
		monarch_name = "Bahbiq I"
		dynasty = "Provmin"
		birth_date = 2125.1.1
		death_date = 2206.1.1
		claim = 100
		adm = 3
		dip = 3
		mil = 2
    }
}

2206.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Sharideh"
		monarch_name = "Sharideh I"
		dynasty = "Slurgti"
		birth_date = 2204.1.1
		death_date = 2222.1.1
		claim = 100
		adm = 1
		dip = 2
		mil = 5
		female = yes
    }
}

2222.1.1 = {
	monarch = {
 		name = "Jelin"
		dynasty = "Cyrcy"
		birth_date = 2196.1.1
		adm = 4
		dip = 3
		mil = 1
    }
	queen = {
 		name = "Hawafa"
		dynasty = "Cyrcy"
		birth_date = 2193.1.1
		adm = 1
		dip = 2
		mil = 0
		female = yes
    }
}

2285.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Udhush"
		monarch_name = "Udhush I"
		dynasty = "Sticert"
		birth_date = 2284.1.1
		death_date = 2302.1.1
		claim = 100
		adm = 1
		dip = 6
		mil = 4
    }
}

2302.1.1 = {
	monarch = {
 		name = "Kavzodd"
		dynasty = "Faliaron"
		birth_date = 2255.1.1
		adm = 4
		dip = 0
		mil = 0
    }
	queen = {
 		name = "Isalde"
		dynasty = "Faliaron"
		birth_date = 2251.1.1
		adm = 1
		dip = 6
		mil = 6
		female = yes
    }
}

2375.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Yashar"
		monarch_name = "Yashar I"
		dynasty = "Kharus"
		birth_date = 2365.1.1
		death_date = 2383.1.1
		claim = 100
		adm = 2
		dip = 3
		mil = 4
    }
}

2383.1.1 = {
	monarch = {
 		name = "Luahna"
		dynasty = "Fortum"
		birth_date = 2357.1.1
		adm = 1
		dip = 6
		mil = 4
		female = yes
    }
}

2457.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Zakhal"
		monarch_name = "Zakhal I"
		dynasty = "Curtyvond"
		birth_date = 2450.1.1
		death_date = 2468.1.1
		claim = 100
		adm = 5
		dip = 1
		mil = 4
    }
}

2468.1.1 = {
	monarch = {
 		name = "Mahbub"
		dynasty = "Cilp'kern"
		birth_date = 2439.1.1
		adm = 5
		dip = 5
		mil = 4
    }
}

