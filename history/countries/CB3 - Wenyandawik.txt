government = monarchy
government_rank = 1
mercantilism = 1
technology_group = cyrodiil_tg
religion = ayleid_pantheon
primary_culture = ayleid
capital = 5315

54.1.1 = {
	monarch = {
 		name = "Luudagarund"
		dynasty = "Ula-Oressinia"
		birth_date = 6.1.1
		adm = 0
		dip = 2
		mil = 4
    }
}

149.1.1 = {
	monarch = {
 		name = "Filestis"
		dynasty = "Mea-Correllia"
		birth_date = 106.1.1
		adm = 6
		dip = 2
		mil = 1
    }
}

208.1.1 = {
	monarch = {
 		name = "Muzos"
		dynasty = "Ula-Jorabrina"
		birth_date = 156.1.1
		adm = 4
		dip = 1
		mil = 5
		female = yes
    }
	queen = {
 		name = "Dingardigam"
		dynasty = "Ula-Jorabrina"
		birth_date = 172.1.1
		adm = 1
		dip = 6
		mil = 4
    }
	heir = {
 		name = "Cordu"
		monarch_name = "Cordu I"
		dynasty = "Ula-Jorabrina"
		birth_date = 208.1.1
		death_date = 287.1.1
		claim = 100
		adm = 5
		dip = 5
		mil = 3
    }
}

287.1.1 = {
	monarch = {
 		name = "Omashaul"
		dynasty = "Vae-Vesagrius"
		birth_date = 267.1.1
		adm = 0
		dip = 6
		mil = 5
    }
}

351.1.1 = {
	monarch = {
 		name = "Myzas"
		dynasty = "Vae-Vanua"
		birth_date = 305.1.1
		adm = 6
		dip = 5
		mil = 2
		female = yes
    }
}

409.1.1 = {
	monarch = {
 		name = "Undorrau"
		dynasty = "Vae-Sepades"
		birth_date = 365.1.1
		adm = 4
		dip = 5
		mil = 5
    }
	queen = {
 		name = "Mennith"
		dynasty = "Vae-Sepades"
		birth_date = 380.1.1
		adm = 1
		dip = 3
		mil = 4
		female = yes
    }
}

502.1.1 = {
	monarch = {
 		name = "Eguggend"
		dynasty = "Ula-Miscarcand"
		birth_date = 483.1.1
		adm = 6
		dip = 2
		mil = 1
    }
	queen = {
 		name = "Purru"
		dynasty = "Ula-Miscarcand"
		birth_date = 463.1.1
		adm = 3
		dip = 1
		mil = 0
		female = yes
    }
	heir = {
 		name = "Myzas"
		monarch_name = "Myzas I"
		dynasty = "Ula-Miscarcand"
		birth_date = 496.1.1
		death_date = 542.1.1
		claim = 100
		adm = 0
		dip = 6
		mil = 6
		female = yes
    }
}

542.1.1 = {
	monarch = {
 		name = "Ironwymu"
		dynasty = "Ula-Rusifus"
		birth_date = 503.1.1
		adm = 6
		dip = 3
		mil = 6
		female = yes
    }
}

626.1.1 = {
	monarch = {
 		name = "Qessem"
		dynasty = "Vae-Waelori"
		birth_date = 597.1.1
		adm = 4
		dip = 2
		mil = 3
    }
}

662.1.1 = {
	monarch = {
 		name = "Guumond"
		dynasty = "Ula-Isollaise"
		birth_date = 633.1.1
		adm = 3
		dip = 1
		mil = 6
    }
	queen = {
 		name = "Purru"
		dynasty = "Ula-Isollaise"
		birth_date = 643.1.1
		adm = 0
		dip = 0
		mil = 5
		female = yes
    }
}

742.1.1 = {
	monarch = {
 		name = "Cyrheganund"
		dynasty = "Mea-Amane"
		birth_date = 697.1.1
		adm = 5
		dip = 6
		mil = 2
    }
	queen = {
 		name = "Lometh"
		dynasty = "Mea-Amane"
		birth_date = 692.1.1
		adm = 2
		dip = 4
		mil = 1
		female = yes
    }
	heir = {
 		name = "Limdardhunur"
		monarch_name = "Limdardhunur I"
		dynasty = "Mea-Amane"
		birth_date = 741.1.1
		death_date = 809.1.1
		claim = 100
		adm = 6
		dip = 3
		mil = 0
    }
}

809.1.1 = {
	monarch = {
 		name = "Erraduure"
		dynasty = "Mea-Andrulusus"
		birth_date = 765.1.1
		adm = 1
		dip = 4
		mil = 2
		female = yes
    }
}

900.1.1 = {
	monarch = {
 		name = "Fidhi"
		dynasty = "Mea-Brina"
		birth_date = 847.1.1
		adm = 0
		dip = 4
		mil = 6
    }
}

994.1.1 = {
	monarch = {
 		name = "Dini"
		dynasty = "Vae-Vanua"
		birth_date = 944.1.1
		adm = 5
		dip = 3
		mil = 3
		female = yes
    }
}

1038.1.1 = {
	monarch = {
 		name = "Genuuladom"
		dynasty = "Ula-Lildana"
		birth_date = 997.1.1
		adm = 3
		dip = 2
		mil = 6
    }
	queen = {
 		name = "Lymma"
		dynasty = "Ula-Lildana"
		birth_date = 989.1.1
		adm = 0
		dip = 0
		mil = 5
		female = yes
    }
	heir = {
 		name = "Hunro"
		monarch_name = "Hunro I"
		dynasty = "Ula-Lildana"
		birth_date = 1023.1.1
		death_date = 1078.1.1
		claim = 100
		adm = 4
		dip = 6
		mil = 4
    }
}

1078.1.1 = {
	monarch = {
 		name = "Illath"
		dynasty = "Ula-Lalaifre"
		birth_date = 1059.1.1
		adm = 3
		dip = 2
		mil = 4
		female = yes
    }
	queen = {
 		name = "Vingiant"
		dynasty = "Ula-Lalaifre"
		birth_date = 1055.1.1
		adm = 0
		dip = 1
		mil = 3
    }
}

1119.1.1 = {
	monarch = {
 		name = "Urenenya"
		dynasty = "Ula-Messiena"
		birth_date = 1078.1.1
		adm = 4
		dip = 3
		mil = 3
		female = yes
    }
	queen = {
 		name = "Lida"
		dynasty = "Ula-Messiena"
		birth_date = 1093.1.1
		adm = 1
		dip = 1
		mil = 2
    }
	heir = {
 		name = "Heystalero"
		monarch_name = "Heystalero I"
		dynasty = "Ula-Messiena"
		birth_date = 1119.1.1
		death_date = 1188.1.1
		claim = 100
		adm = 5
		dip = 0
		mil = 1
    }
}

1188.1.1 = {
	monarch = {
 		name = "Vem"
		dynasty = "Vae-Sardavar"
		birth_date = 1142.1.1
		adm = 4
		dip = 3
		mil = 2
    }
	queen = {
 		name = "Poman"
		dynasty = "Vae-Sardavar"
		birth_date = 1157.1.1
		adm = 5
		dip = 0
		mil = 3
		female = yes
    }
	heir = {
 		name = "Vunhualdadont"
		monarch_name = "Vunhualdadont I"
		dynasty = "Vae-Sardavar"
		birth_date = 1188.1.1
		death_date = 1239.1.1
		claim = 100
		adm = 1
		dip = 5
		mil = 2
    }
}

1239.1.1 = {
	monarch = {
 		name = "Qessem"
		dynasty = "Ula-Larilatia"
		birth_date = 1190.1.1
		adm = 1
		dip = 2
		mil = 2
    }
	queen = {
 		name = "Cimase"
		dynasty = "Ula-Larilatia"
		birth_date = 1202.1.1
		adm = 5
		dip = 0
		mil = 1
		female = yes
    }
}

1336.1.1 = {
	monarch = {
 		name = "Yrraldylith"
		dynasty = "Vae-Statain"
		birth_date = 1288.1.1
		adm = 3
		dip = 0
		mil = 5
		female = yes
    }
}

1435.1.1 = {
	monarch = {
 		name = "Curano"
		dynasty = "Mea-Amane"
		birth_date = 1382.1.1
		adm = 1
		dip = 6
		mil = 1
		female = yes
    }
	queen = {
 		name = "Vunhualdadont"
		dynasty = "Mea-Amane"
		birth_date = 1387.1.1
		adm = 5
		dip = 4
		mil = 0
    }
	heir = {
 		name = "Eradhiho"
		monarch_name = "Eradhiho I"
		dynasty = "Mea-Amane"
		birth_date = 1420.1.1
		death_date = 1477.1.1
		claim = 100
		adm = 2
		dip = 3
		mil = 6
		female = yes
    }
}

1477.1.1 = {
	monarch = {
 		name = "Helydu"
		dynasty = "Mea-Garlas"
		birth_date = 1450.1.1
		adm = 5
		dip = 4
		mil = 2
		female = yes
    }
	queen = {
 		name = "Inhuagadom"
		dynasty = "Mea-Garlas"
		birth_date = 1434.1.1
		adm = 2
		dip = 3
		mil = 0
    }
	heir = {
 		name = "Emma"
		monarch_name = "Emma I"
		dynasty = "Mea-Garlas"
		birth_date = 1464.1.1
		death_date = 1563.1.1
		claim = 100
		adm = 5
		dip = 1
		mil = 6
		female = yes
    }
}

1563.1.1 = {
	monarch = {
 		name = "Endarre"
		dynasty = "Ula-Niryastare"
		birth_date = 1522.1.1
		adm = 1
		dip = 3
		mil = 2
    }
	queen = {
 		name = "Lometh"
		dynasty = "Ula-Niryastare"
		birth_date = 1520.1.1
		adm = 5
		dip = 1
		mil = 1
		female = yes
    }
	heir = {
 		name = "Tjuunrerhen"
		monarch_name = "Tjuunrerhen I"
		dynasty = "Ula-Niryastare"
		birth_date = 1550.1.1
		death_date = 1642.1.1
		claim = 100
		adm = 2
		dip = 0
		mil = 0
    }
}

1642.1.1 = {
	monarch = {
 		name = "Nula"
		dynasty = "Mea-Bawn"
		birth_date = 1600.1.1
		adm = 1
		dip = 3
		mil = 0
		female = yes
    }
	queen = {
 		name = "Cusdar"
		dynasty = "Mea-Bawn"
		birth_date = 1607.1.1
		adm = 5
		dip = 2
		mil = 6
    }
	heir = {
 		name = "Vuudunt"
		monarch_name = "Vuudunt I"
		dynasty = "Mea-Bawn"
		birth_date = 1630.1.1
		death_date = 1679.1.1
		claim = 100
		adm = 5
		dip = 5
		mil = 0
    }
}

1679.1.1 = {
	monarch = {
 		name = "Dini"
		dynasty = "Mea-Hastrel"
		birth_date = 1654.1.1
		adm = 5
		dip = 1
		mil = 0
		female = yes
    }
}

1762.1.1 = {
	monarch = {
 		name = "Genuuladom"
		dynasty = "Mea-Corannus"
		birth_date = 1710.1.1
		adm = 3
		dip = 1
		mil = 4
    }
	queen = {
 		name = "Nyrrin"
		dynasty = "Mea-Corannus"
		birth_date = 1726.1.1
		adm = 0
		dip = 6
		mil = 3
		female = yes
    }
	heir = {
 		name = "Elis"
		monarch_name = "Elis I"
		dynasty = "Mea-Corannus"
		birth_date = 1754.1.1
		death_date = 1803.1.1
		claim = 100
		adm = 4
		dip = 5
		mil = 2
		female = yes
    }
}

1803.1.1 = {
	monarch = {
 		name = "Nogo"
		dynasty = "Mea-Arriastae"
		birth_date = 1780.1.1
		adm = 6
		dip = 6
		mil = 4
    }
	queen = {
 		name = "Carro"
		dynasty = "Mea-Arriastae"
		birth_date = 1773.1.1
		adm = 3
		dip = 5
		mil = 3
		female = yes
    }
	heir = {
 		name = "Lenalmel"
		monarch_name = "Lenalmel I"
		dynasty = "Mea-Arriastae"
		birth_date = 1789.1.1
		death_date = 1867.1.1
		claim = 100
		adm = 0
		dip = 3
		mil = 2
    }
}

1867.1.1 = {
	monarch = {
 		name = "Genuuladom"
		dynasty = "Mea-Allevar"
		birth_date = 1840.1.1
		adm = 0
		dip = 0
		mil = 2
    }
	queen = {
 		name = "Suhe"
		dynasty = "Mea-Allevar"
		birth_date = 1836.1.1
		adm = 0
		dip = 3
		mil = 4
		female = yes
    }
	heir = {
 		name = "Hunro"
		monarch_name = "Hunro I"
		dynasty = "Mea-Allevar"
		birth_date = 1864.1.1
		death_date = 1953.1.1
		claim = 100
		adm = 4
		dip = 2
		mil = 2
    }
}

1953.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Umhuur"
		monarch_name = "Umhuur I"
		dynasty = "Vae-Sercen"
		birth_date = 1946.1.1
		death_date = 1964.1.1
		claim = 100
		adm = 2
		dip = 1
		mil = 6
    }
}

1964.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Qystur"
		monarch_name = "Qystur I"
		dynasty = "Vae-Vautellia"
		birth_date = 1955.1.1
		death_date = 1973.1.1
		claim = 100
		adm = 4
		dip = 2
		mil = 1
    }
}

1973.1.1 = {
	monarch = {
 		name = "Wuria"
		dynasty = "Ula-Macelius"
		birth_date = 1930.1.1
		adm = 0
		dip = 3
		mil = 3
		female = yes
    }
}

2033.1.1 = {
	monarch = {
 		name = "Vyza"
		dynasty = "Vae-Sylolvia"
		birth_date = 1980.1.1
		adm = 5
		dip = 3
		mil = 0
		female = yes
    }
}

2077.1.1 = {
	monarch = {
 		name = "Lonnosson"
		dynasty = "Vae-Silelia"
		birth_date = 2024.1.1
		adm = 3
		dip = 2
		mil = 3
		female = yes
    }
}

2120.1.1 = {
	monarch = {
 		name = "Houtern"
		dynasty = "Mea-Alessia"
		birth_date = 2087.1.1
		adm = 1
		dip = 1
		mil = 0
    }
}

2166.1.1 = {
	monarch = {
 		name = "Azu"
		dynasty = "Mea-Arriastae"
		birth_date = 2143.1.1
		adm = 3
		dip = 2
		mil = 1
		female = yes
    }
	queen = {
 		name = "Djamduusceges"
		dynasty = "Mea-Arriastae"
		birth_date = 2123.1.1
		adm = 3
		dip = 6
		mil = 3
    }
	heir = {
 		name = "Pehi"
		monarch_name = "Pehi I"
		dynasty = "Mea-Arriastae"
		birth_date = 2166.1.1
		death_date = 2255.1.1
		claim = 100
		adm = 0
		dip = 4
		mil = 2
		female = yes
    }
}

2255.1.1 = {
	monarch = {
 		name = "Cymylmuumer"
		dynasty = "Vae-Shadrock"
		birth_date = 2205.1.1
		adm = 0
		dip = 1
		mil = 2
    }
}

2340.1.1 = {
	monarch = {
 		name = "Ammas"
		dynasty = "Vae-Trumbe"
		birth_date = 2315.1.1
		adm = 5
		dip = 0
		mil = 5
		female = yes
    }
	queen = {
 		name = "Cur"
		dynasty = "Vae-Trumbe"
		birth_date = 2287.1.1
		adm = 2
		dip = 5
		mil = 4
    }
}

2414.1.1 = {
	monarch = {
 		name = "Lagan"
		dynasty = "Mea-Calabolis"
		birth_date = 2369.1.1
		adm = 0
		dip = 5
		mil = 1
    }
}

2472.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Wimunmara"
		monarch_name = "Wimunmara I"
		dynasty = "Mea-Conoa"
		birth_date = 2468.1.1
		death_date = 2486.1.1
		claim = 100
		adm = 1
		dip = 2
		mil = 6
		female = yes
    }
}

2486.1.1 = {
	monarch = {
 		name = "Cordu"
		dynasty = "Mea-Hackdirt"
		birth_date = 2433.1.1
		adm = 4
		dip = 3
		mil = 1
    }
	queen = {
 		name = "Pymmynnenu"
		dynasty = "Mea-Hackdirt"
		birth_date = 2448.1.1
		adm = 0
		dip = 2
		mil = 0
		female = yes
    }
}

