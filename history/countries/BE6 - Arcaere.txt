government = monarchy
government_rank = 1
mercantilism = 1
technology_group = elven_tg
religion = altmeri_pantheon
primary_culture = altmer
capital = 6968

54.1.1 = {
	monarch = {
 		name = "Parmion"
		dynasty = "Wel-Calmssare"
		birth_date = 3.1.1
		adm = 2
		dip = 6
		mil = 5
    }
}

142.1.1 = {
	monarch = {
 		name = "Vandoril"
		dynasty = "Wel-Elanesse"
		birth_date = 89.1.1
		adm = 1
		dip = 5
		mil = 2
    }
	queen = {
 		name = "Erissare"
		dynasty = "Wel-Elanesse"
		birth_date = 101.1.1
		adm = 4
		dip = 4
		mil = 1
		female = yes
    }
	heir = {
 		name = "Tundullie"
		monarch_name = "Tundullie I"
		dynasty = "Wel-Elanesse"
		birth_date = 130.1.1
		death_date = 226.1.1
		claim = 100
		adm = 1
		dip = 2
		mil = 0
		female = yes
    }
}

226.1.1 = {
	monarch = {
 		name = "Lenwe"
		dynasty = "Wel-Fainwen"
		birth_date = 182.1.1
		adm = 4
		dip = 4
		mil = 2
    }
	queen = {
 		name = "Tarie"
		dynasty = "Wel-Fainwen"
		birth_date = 190.1.1
		adm = 1
		dip = 2
		mil = 1
		female = yes
    }
	heir = {
 		name = "Faryan"
		monarch_name = "Faryan I"
		dynasty = "Wel-Fainwen"
		birth_date = 219.1.1
		death_date = 284.1.1
		claim = 100
		adm = 5
		dip = 1
		mil = 0
		female = yes
    }
}

284.1.1 = {
	monarch = {
 		name = "Vancecarion"
		dynasty = "Ael-Felballin"
		birth_date = 244.1.1
		adm = 1
		dip = 2
		mil = 2
    }
}

356.1.1 = {
	monarch = {
 		name = "Pondomir"
		dynasty = "Bel-Sunhold"
		birth_date = 325.1.1
		adm = 6
		dip = 1
		mil = 6
    }
}

427.1.1 = {
	monarch = {
 		name = "Henarsare"
		dynasty = "Wel-Aelsoniane"
		birth_date = 375.1.1
		adm = 1
		dip = 3
		mil = 0
		female = yes
    }
	queen = {
 		name = "Helonel"
		dynasty = "Wel-Aelsoniane"
		birth_date = 379.1.1
		adm = 1
		dip = 6
		mil = 2
    }
	heir = {
 		name = "Undil"
		monarch_name = "Undil I"
		dynasty = "Wel-Aelsoniane"
		birth_date = 417.1.1
		death_date = 474.1.1
		claim = 100
		adm = 5
		dip = 5
		mil = 1
    }
}

474.1.1 = {
	monarch = {
 		name = "Linwirmion"
		dynasty = "Bel-Ronassa"
		birth_date = 432.1.1
		adm = 4
		dip = 1
		mil = 1
    }
	queen = {
 		name = "Telyasawen"
		dynasty = "Bel-Ronassa"
		birth_date = 447.1.1
		adm = 1
		dip = 0
		mil = 0
		female = yes
    }
	heir = {
 		name = "Ingeromir"
		monarch_name = "Ingeromir I"
		dynasty = "Bel-Ronassa"
		birth_date = 468.1.1
		death_date = 565.1.1
		claim = 100
		adm = 5
		dip = 5
		mil = 6
    }
}

565.1.1 = {
	monarch = {
 		name = "Vesteldore"
		dynasty = "Ael-Firsthold"
		birth_date = 537.1.1
		adm = 1
		dip = 0
		mil = 1
		female = yes
    }
	queen = {
 		name = "Heldil"
		dynasty = "Ael-Firsthold"
		birth_date = 523.1.1
		adm = 5
		dip = 5
		mil = 0
    }
}

642.1.1 = {
	monarch = {
 		name = "Falarel"
		dynasty = "Bel-Slaughter"
		birth_date = 600.1.1
		adm = 3
		dip = 4
		mil = 4
    }
	queen = {
 		name = "Nolinore"
		dynasty = "Bel-Slaughter"
		birth_date = 619.1.1
		adm = 0
		dip = 3
		mil = 3
		female = yes
    }
	heir = {
 		name = "Emderil"
		monarch_name = "Emderil I"
		dynasty = "Bel-Slaughter"
		birth_date = 630.1.1
		death_date = 692.1.1
		claim = 100
		adm = 4
		dip = 1
		mil = 2
    }
}

692.1.1 = {
	monarch = {
 		name = "Salerne"
		dynasty = "Wel-Alkinosin"
		birth_date = 650.1.1
		adm = 6
		dip = 3
		mil = 4
		female = yes
    }
}

767.1.1 = {
	monarch = {
 		name = "Menuldhel"
		dynasty = "Bel-Terna"
		birth_date = 739.1.1
		adm = 5
		dip = 2
		mil = 1
    }
	queen = {
 		name = "Ghisiliane"
		dynasty = "Bel-Terna"
		birth_date = 728.1.1
		adm = 1
		dip = 0
		mil = 0
		female = yes
    }
}

802.1.1 = {
	monarch = {
 		name = "Tanamo"
		dynasty = "Wel-Ardende"
		birth_date = 766.1.1
		adm = 0
		dip = 0
		mil = 3
    }
	queen = {
 		name = "Ranwe"
		dynasty = "Wel-Ardende"
		birth_date = 750.1.1
		adm = 4
		dip = 5
		mil = 2
		female = yes
    }
	heir = {
 		name = "Farmolbrian"
		monarch_name = "Farmolbrian I"
		dynasty = "Wel-Ardende"
		birth_date = 787.1.1
		death_date = 886.1.1
		claim = 100
		adm = 0
		dip = 4
		mil = 1
    }
}

886.1.1 = {
	monarch = {
 		name = "Eminelya"
		dynasty = "Bel-Virea"
		birth_date = 833.1.1
		adm = 6
		dip = 3
		mil = 5
		female = yes
    }
	queen = {
 		name = "Nemfarion"
		dynasty = "Bel-Virea"
		birth_date = 862.1.1
		adm = 3
		dip = 1
		mil = 4
    }
	heir = {
 		name = "Norarubel"
		monarch_name = "Norarubel I"
		dynasty = "Bel-Virea"
		birth_date = 877.1.1
		death_date = 958.1.1
		claim = 100
		adm = 6
		dip = 0
		mil = 3
    }
}

958.1.1 = {
	monarch = {
 		name = "Earamon"
		dynasty = "Bel-Shamia"
		birth_date = 906.1.1
		adm = 6
		dip = 3
		mil = 3
    }
	queen = {
 		name = "Arawe"
		dynasty = "Bel-Shamia"
		birth_date = 915.1.1
		adm = 6
		dip = 0
		mil = 4
		female = yes
    }
	heir = {
 		name = "Ocando"
		monarch_name = "Ocando I"
		dynasty = "Bel-Shamia"
		birth_date = 944.1.1
		death_date = 1031.1.1
		claim = 100
		adm = 3
		dip = 5
		mil = 3
    }
}

1031.1.1 = {
	monarch = {
 		name = "Casorlaure"
		dynasty = "Wel-Celae"
		birth_date = 981.1.1
		adm = 2
		dip = 2
		mil = 3
		female = yes
    }
}

1071.1.1 = {
	monarch = {
 		name = "Valla"
		dynasty = "Bel-Tanena"
		birth_date = 1036.1.1
		adm = 1
		dip = 1
		mil = 0
		female = yes
    }
	queen = {
 		name = "Glaetaldo"
		dynasty = "Bel-Tanena"
		birth_date = 1027.1.1
		adm = 4
		dip = 0
		mil = 6
    }
	heir = {
 		name = "Tuinorion"
		monarch_name = "Tuinorion I"
		dynasty = "Bel-Tanena"
		birth_date = 1060.1.1
		death_date = 1161.1.1
		claim = 100
		adm = 1
		dip = 5
		mil = 5
    }
}

1161.1.1 = {
	monarch = {
 		name = "Lenarmen"
		dynasty = "Ael-Kingdom"
		birth_date = 1118.1.1
		adm = 4
		dip = 6
		mil = 0
    }
}

1248.1.1 = {
	monarch = {
 		name = "Cartirinque"
		dynasty = "Wel-Camiana"
		birth_date = 1226.1.1
		adm = 2
		dip = 6
		mil = 4
		female = yes
    }
	queen = {
 		name = "Caluurion"
		dynasty = "Wel-Camiana"
		birth_date = 1224.1.1
		adm = 0
		dip = 1
		mil = 6
    }
	heir = {
 		name = "Astorne"
		monarch_name = "Astorne I"
		dynasty = "Wel-Camiana"
		birth_date = 1241.1.1
		death_date = 1319.1.1
		claim = 100
		adm = 3
		dip = 3
		mil = 2
		female = yes
    }
}

1319.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Erymil"
		monarch_name = "Erymil I"
		dynasty = "Wel-Angaelle"
		birth_date = 1314.1.1
		death_date = 1332.1.1
		claim = 100
		adm = 4
		dip = 0
		mil = 0
    }
}

1332.1.1 = {
	monarch = {
 		name = "Squan"
		dynasty = "Wel-Arannnarre"
		birth_date = 1304.1.1
		adm = 0
		dip = 1
		mil = 2
    }
}

1412.1.1 = {
	monarch = {
 		name = "Lirinwen"
		dynasty = "		"
		birth_date = 1367.1.1
		adm = 5
		dip = 0
		mil = 6
		female = yes
    }
}

1502.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Aldimonwe"
		monarch_name = "Aldimonwe I"
		dynasty = "		"
		birth_date = 1501.1.1
		death_date = 1519.1.1
		claim = 100
		adm = 0
		dip = 2
		mil = 0
		female = yes
    }
}

1519.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Firilanya"
		monarch_name = "Firilanya I"
		dynasty = "Wel-Angaelle"
		birth_date = 1508.1.1
		death_date = 1526.1.1
		claim = 100
		adm = 5
		dip = 1
		mil = 4
		female = yes
    }
}

1526.1.1 = {
	monarch = {
 		name = "Velandare"
		dynasty = "Ael-Mirlenya"
		birth_date = 1495.1.1
		adm = 3
		dip = 0
		mil = 1
		female = yes
    }
	queen = {
 		name = "Quarmuuril"
		dynasty = "Ael-Mirlenya"
		birth_date = 1481.1.1
		adm = 5
		dip = 1
		mil = 5
    }
	heir = {
 		name = "Fainorume"
		monarch_name = "Fainorume I"
		dynasty = "Ael-Mirlenya"
		birth_date = 1525.1.1
		death_date = 1606.1.1
		claim = 100
		adm = 4
		dip = 4
		mil = 6
    }
}

1606.1.1 = {
	monarch = {
 		name = "Linalion"
		dynasty = "Ael-Firsthold"
		birth_date = 1570.1.1
		adm = 4
		dip = 1
		mil = 0
    }
	queen = {
 		name = "Falen"
		dynasty = "Ael-Firsthold"
		birth_date = 1556.1.1
		adm = 1
		dip = 6
		mil = 6
		female = yes
    }
	heir = {
 		name = "Uulion"
		monarch_name = "Uulion I"
		dynasty = "Ael-Firsthold"
		birth_date = 1592.1.1
		death_date = 1667.1.1
		claim = 100
		adm = 5
		dip = 5
		mil = 5
    }
}

1667.1.1 = {
	monarch = {
 		name = "Varwenra"
		dynasty = "Bel-Sontarya"
		birth_date = 1649.1.1
		adm = 4
		dip = 1
		mil = 5
		female = yes
    }
}

1752.1.1 = {
	monarch = {
 		name = "Eranamo"
		dynasty = "Bel-Shaaninde"
		birth_date = 1722.1.1
		adm = 3
		dip = 0
		mil = 1
    }
}

1830.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Candangarilde"
		monarch_name = "Candangarilde I"
		dynasty = "Ael-Minoirdalin"
		birth_date = 1816.1.1
		death_date = 1834.1.1
		claim = 100
		adm = 3
		dip = 4
		mil = 6
		female = yes
    }
}

1834.1.1 = {
	monarch = {
 		name = "Quertasse"
		dynasty = "Bel-Ronassa"
		birth_date = 1802.1.1
		adm = 6
		dip = 6
		mil = 2
		female = yes
    }
	queen = {
 		name = "Cassirion"
		dynasty = "Bel-Ronassa"
		birth_date = 1807.1.1
		adm = 3
		dip = 4
		mil = 1
    }
}

1924.1.1 = {
	monarch = {
 		name = "Andresalmo"
		dynasty = "Wel-Aryria"
		birth_date = 1881.1.1
		adm = 1
		dip = 3
		mil = 4
    }
	queen = {
 		name = "Fistalime"
		dynasty = "Wel-Aryria"
		birth_date = 1897.1.1
		adm = 5
		dip = 2
		mil = 3
		female = yes
    }
	heir = {
 		name = "Volanaro"
		monarch_name = "Volanaro I"
		dynasty = "Wel-Aryria"
		birth_date = 1921.1.1
		death_date = 1972.1.1
		claim = 100
		adm = 2
		dip = 0
		mil = 2
    }
}

1972.1.1 = {
	monarch = {
 		name = "Milmendien"
		dynasty = "Bel-Tanena"
		birth_date = 1934.1.1
		adm = 5
		dip = 2
		mil = 5
    }
	queen = {
 		name = "Hanursae"
		dynasty = "Bel-Tanena"
		birth_date = 1943.1.1
		adm = 2
		dip = 0
		mil = 4
		female = yes
    }
	heir = {
 		name = "Lisdebar"
		monarch_name = "Lisdebar I"
		dynasty = "Bel-Tanena"
		birth_date = 1960.1.1
		death_date = 2014.1.1
		claim = 100
		adm = 5
		dip = 6
		mil = 3
    }
}

2014.1.1 = {
	monarch = {
 		name = "Andame"
		dynasty = "Bel-Valnoore"
		birth_date = 1993.1.1
		adm = 5
		dip = 2
		mil = 3
    }
	queen = {
 		name = "Valacarya"
		dynasty = "Bel-Valnoore"
		birth_date = 1993.1.1
		adm = 5
		dip = 6
		mil = 4
		female = yes
    }
	heir = {
 		name = "Malin"
		monarch_name = "Malin I"
		dynasty = "Bel-Valnoore"
		birth_date = 2013.1.1
		death_date = 2056.1.1
		claim = 100
		adm = 2
		dip = 4
		mil = 3
    }
}

2056.1.1 = {
	monarch = {
 		name = "Andenyerinwe"
		dynasty = "Bel-Taanye"
		birth_date = 2031.1.1
		adm = 1
		dip = 1
		mil = 3
		female = yes
    }
}

2115.1.1 = {
	monarch = {
 		name = "Sirinaire"
		dynasty = "Wel-Aelsoniane"
		birth_date = 2081.1.1
		adm = 0
		dip = 0
		mil = 0
		female = yes
    }
	queen = {
 		name = "Ehran"
		dynasty = "Wel-Aelsoniane"
		birth_date = 2093.1.1
		adm = 3
		dip = 5
		mil = 6
    }
	heir = {
 		name = "Rulnerdomas"
		monarch_name = "Rulnerdomas I"
		dynasty = "Wel-Aelsoniane"
		birth_date = 2108.1.1
		death_date = 2194.1.1
		claim = 100
		adm = 0
		dip = 4
		mil = 5
    }
}

2194.1.1 = {
	monarch = {
 		name = "Elanya"
		dynasty = "Ael-Mathiisen"
		birth_date = 2156.1.1
		adm = 3
		dip = 5
		mil = 0
		female = yes
    }
}

2285.1.1 = {
	monarch = {
 		name = "Antur"
		dynasty = "Ael-Kingshaven"
		birth_date = 2255.1.1
		adm = 1
		dip = 4
		mil = 4
    }
	queen = {
 		name = "Varwenra"
		dynasty = "Ael-Kingshaven"
		birth_date = 2262.1.1
		adm = 5
		dip = 3
		mil = 3
		female = yes
    }
	heir = {
 		name = "Wendlain"
		monarch_name = "Wendlain I"
		dynasty = "Ael-Kingshaven"
		birth_date = 2283.1.1
		death_date = 2323.1.1
		claim = 100
		adm = 2
		dip = 2
		mil = 2
		female = yes
    }
}

2323.1.1 = {
	monarch = {
 		name = "Monwe"
		dynasty = "Wel-Eleaninde"
		birth_date = 2297.1.1
		adm = 1
		dip = 5
		mil = 2
    }
}

2403.1.1 = {
	monarch = {
 		name = "Fasendil"
		dynasty = "Wel-Endrae"
		birth_date = 2350.1.1
		adm = 0
		dip = 4
		mil = 6
    }
	queen = {
 		name = "Cerunsawen"
		dynasty = "Wel-Endrae"
		birth_date = 2381.1.1
		adm = 4
		dip = 3
		mil = 5
		female = yes
    }
	heir = {
 		name = "Sanessalmo"
		monarch_name = "Sanessalmo I"
		dynasty = "Wel-Endrae"
		birth_date = 2396.1.1
		death_date = 2476.1.1
		claim = 100
		adm = 4
		dip = 6
		mil = 6
    }
}

2476.1.1 = {
	monarch = {
 		name = "Filpormo"
		dynasty = "Ael-Relnnarre"
		birth_date = 2435.1.1
		adm = 3
		dip = 3
		mil = 6
    }
	queen = {
 		name = "Pamolinwe"
		dynasty = "Ael-Relnnarre"
		birth_date = 2450.1.1
		adm = 0
		dip = 1
		mil = 5
		female = yes
    }
	heir = {
 		name = "Cursare"
		monarch_name = "Cursare I"
		dynasty = "Ael-Relnnarre"
		birth_date = 2465.1.1
		death_date = 2544.1.1
		claim = 100
		adm = 4
		dip = 0
		mil = 4
		female = yes
    }
}

