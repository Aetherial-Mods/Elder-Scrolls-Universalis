government = tribal
government_rank = 1
mercantilism = 1
technology_group = exiled_tg
religion = chimer_pantheon
primary_culture = ashlander_chimer
capital = 1019

54.1.1 = {
	monarch = {
 		name = "Elitlaya"
		dynasty = "Assurnarairan"
		birth_date = 22.1.1
		adm = 2
		dip = 3
		mil = 5
		female = yes
    }
}

113.1.1 = {
	monarch = {
 		name = "Zennammu"
		dynasty = "Gilu"
		birth_date = 74.1.1
		adm = 1
		dip = 2
		mil = 1
		female = yes
    }
}

160.1.1 = {
	monarch = {
 		name = "Shimsun"
		dynasty = "Akin"
		birth_date = 135.1.1
		adm = 6
		dip = 1
		mil = 5
    }
}

222.1.1 = {
	monarch = {
 		name = "Massarapal"
		dynasty = "Sobbinisun"
		birth_date = 202.1.1
		adm = 4
		dip = 1
		mil = 2
    }
}

277.1.1 = {
	monarch = {
 		name = "Dakin"
		dynasty = "Radansour"
		birth_date = 257.1.1
		adm = 6
		dip = 2
		mil = 3
    }
}

313.1.1 = {
	monarch = {
 		name = "Zennammu"
		dynasty = "Lasamsi"
		birth_date = 279.1.1
		adm = 4
		dip = 1
		mil = 0
		female = yes
    }
}

410.1.1 = {
	monarch = {
 		name = "Elumabi"
		dynasty = "Shalarnetus"
		birth_date = 364.1.1
		adm = 3
		dip = 0
		mil = 3
		female = yes
    }
}

477.1.1 = {
	monarch = {
 		name = "Zennammu"
		dynasty = "Assinabi"
		birth_date = 425.1.1
		adm = 1
		dip = 0
		mil = 0
    }
}

545.1.1 = {
	monarch = {
 		name = "Shirerib"
		dynasty = "Timsar-Dadisun"
		birth_date = 522.1.1
		adm = 6
		dip = 6
		mil = 4
    }
}

584.1.1 = {
	monarch = {
 		name = "Mimanu"
		dynasty = "Assonirishpal"
		birth_date = 548.1.1
		adm = 4
		dip = 5
		mil = 0
		female = yes
    }
}

656.1.1 = {
	monarch = {
 		name = "Elumabi"
		dynasty = "Mirathrernenum"
		birth_date = 614.1.1
		adm = 3
		dip = 4
		mil = 4
		female = yes
    }
}

738.1.1 = {
	monarch = {
 		name = "Zennammu"
		dynasty = "Rapli"
		birth_date = 712.1.1
		adm = 1
		dip = 3
		mil = 1
    }
}

800.1.1 = {
	monarch = {
 		name = "Shirerib"
		dynasty = "Timmiriran"
		birth_date = 750.1.1
		adm = 3
		dip = 5
		mil = 2
    }
}

891.1.1 = {
	monarch = {
 		name = "Mimanu"
		dynasty = "Selitbael"
		birth_date = 850.1.1
		adm = 1
		dip = 4
		mil = 6
		female = yes
    }
}

958.1.1 = {
	monarch = {
 		name = "Sul-Matuul"
		dynasty = "Esurarnat"
		birth_date = 929.1.1
		adm = 6
		dip = 3
		mil = 3
    }
}

1016.1.1 = {
	monarch = {
 		name = "Minnibi"
		dynasty = "Anurnudai"
		birth_date = 987.1.1
		adm = 4
		dip = 2
		mil = 6
    }
}

1093.1.1 = {
	monarch = {
 		name = "Hairan"
		dynasty = "Sammalamus"
		birth_date = 1049.1.1
		adm = 3
		dip = 1
		mil = 3
		female = yes
    }
}

1177.1.1 = {
	monarch = {
 		name = "Addammus"
		dynasty = "Telvanni"
		birth_date = 1158.1.1
		adm = 3
		dip = 6
		mil = 1
    }
}

1227.1.1 = {
	monarch = {
 		name = "Asharapli"
		dynasty = "Kaushad"
		birth_date = 1180.1.1
		adm = 2
		dip = 5
		mil = 4
		female = yes
    }
}

1289.1.1 = {
	monarch = {
 		name = "Yantus"
		dynasty = "Sehabani"
		birth_date = 1252.1.1
		adm = 0
		dip = 4
		mil = 1
    }
}

1349.1.1 = {
	monarch = {
 		name = "Sal"
		dynasty = "Dinadad"
		birth_date = 1327.1.1
		adm = 5
		dip = 3
		mil = 5
    }
}

1385.1.1 = {
	monarch = {
 		name = "Sal"
		dynasty = "Ahanidiran"
		birth_date = 1357.1.1
		adm = 3
		dip = 2
		mil = 1
    }
}

1482.1.1 = {
	monarch = {
 		name = "Asharapli"
		dynasty = "Radansour"
		birth_date = 1457.1.1
		adm = 4
		dip = 6
		mil = 6
		female = yes
    }
}

1571.1.1 = {
	monarch = {
 		name = "Yantus"
		dynasty = "Ashunbabi"
		birth_date = 1520.1.1
		adm = 4
		dip = 3
		mil = 0
    }
}

1613.1.1 = {
	monarch = {
 		name = "Raishi"
		dynasty = "Abalkala"
		birth_date = 1574.1.1
		adm = 2
		dip = 2
		mil = 3
		female = yes
    }
}

1694.1.1 = {
	monarch = {
 		name = "Yeherradad"
		dynasty = "Ilabael"
		birth_date = 1649.1.1
		adm = 0
		dip = 1
		mil = 0
    }
}

1754.1.1 = {
	monarch = {
 		name = "Salmat"
		dynasty = "Saddarnuran"
		birth_date = 1710.1.1
		adm = 5
		dip = 0
		mil = 3
    }
}

1835.1.1 = {
	monarch = {
 		name = "Kanit"
		dynasty = "Assarrimisun"
		birth_date = 1794.1.1
		adm = 4
		dip = 0
		mil = 0
    }
}

1879.1.1 = {
	monarch = {
 		name = "Ashibaal"
		dynasty = "Zainab"
		birth_date = 1831.1.1
		adm = 2
		dip = 6
		mil = 4
    }
}

1931.1.1 = {
	monarch = {
 		name = "Yeherradad"
		dynasty = "Assudiraplit"
		birth_date = 1903.1.1
		adm = 0
		dip = 5
		mil = 0
    }
}

1969.1.1 = {
	monarch = {
 		name = "Salay"
		dynasty = "Shimmabadas"
		birth_date = 1931.1.1
		adm = 5
		dip = 4
		mil = 4
    }
}

2004.1.1 = {
	monarch = {
 		name = "Kanit"
		dynasty = "Assantinalit"
		birth_date = 1976.1.1
		adm = 0
		dip = 6
		mil = 6
    }
}

2103.1.1 = {
	monarch = {
 		name = "Asha-Ammu"
		dynasty = "Samsi"
		birth_date = 2080.1.1
		adm = 5
		dip = 5
		mil = 2
    }
}

2191.1.1 = {
	monarch = {
 		name = "Maela"
		dynasty = "Timmiriran"
		birth_date = 2157.1.1
		adm = 4
		dip = 4
		mil = 6
		female = yes
    }
}

2228.1.1 = {
	monarch = {
 		name = "Ashur-Dan"
		dynasty = "Sanammasour"
		birth_date = 2203.1.1
		adm = 2
		dip = 3
		mil = 3
    }
}

2308.1.1 = {
	monarch = {
 		name = "Yenammu"
		dynasty = "Mibishanit"
		birth_date = 2289.1.1
		adm = 0
		dip = 2
		mil = 6
    }
}

2406.1.1 = {
	monarch = {
 		name = "Sakiran"
		dynasty = "Addunipu"
		birth_date = 2354.1.1
		adm = 5
		dip = 2
		mil = 3
		female = yes
    }
}

2441.1.1 = {
	monarch = {
 		name = "Lanabi"
		dynasty = "Puntumisun"
		birth_date = 2418.1.1
		adm = 4
		dip = 1
		mil = 6
		female = yes
    }
}

