government = native
government_rank = 1
mercantilism = 1
technology_group = daedric_tg
religion = mehrunes_dagon_cult
primary_culture = clanfear
capital = 3846

54.1.1 = {
	monarch = {
 		name = "Zark"
		dynasty = "Jul-Zon"
		birth_date = 20.1.1
		adm = 1
		dip = 4
		mil = 5
    }
}

114.1.1 = {
	monarch = {
 		name = "Dath"
		dynasty = "Jul-Noh"
		birth_date = 77.1.1
		adm = 0
		dip = 4
		mil = 2
		female = yes
    }
}

163.1.1 = {
	monarch = {
 		name = "Khibazzorm"
		dynasty = "Jul-Nesh"
		birth_date = 120.1.1
		adm = 5
		dip = 3
		mil = 5
    }
}

245.1.1 = {
	monarch = {
 		name = "Urunvaut"
		dynasty = "Jul-Pirul"
		birth_date = 197.1.1
		adm = 3
		dip = 2
		mil = 2
    }
}

312.1.1 = {
	monarch = {
 		name = "Zark"
		dynasty = "Jul-Uzemluox"
		birth_date = 280.1.1
		adm = 5
		dip = 3
		mil = 4
    }
}

383.1.1 = {
	monarch = {
 		name = "Gayl"
		dynasty = "Jul-Nimeclo"
		birth_date = 350.1.1
		adm = 3
		dip = 2
		mil = 0
		female = yes
    }
}

447.1.1 = {
	monarch = {
 		name = "Nalavirk"
		dynasty = "Jul-Eblu"
		birth_date = 417.1.1
		adm = 2
		dip = 2
		mil = 4
    }
}

487.1.1 = {
	monarch = {
 		name = "Iljkou"
		dynasty = "Jul-Iljke"
		birth_date = 435.1.1
		adm = 0
		dip = 1
		mil = 0
		female = yes
    }
}

531.1.1 = {
	monarch = {
 		name = "Muzrirs"
		dynasty = "Jul-Bromrix"
		birth_date = 488.1.1
		adm = 5
		dip = 0
		mil = 4
    }
}

584.1.1 = {
	monarch = {
 		name = "Ungrin"
		dynasty = "Jul-Kimgu"
		birth_date = 551.1.1
		adm = 3
		dip = 6
		mil = 1
    }
}

657.1.1 = {
	monarch = {
 		name = "Nalavirk"
		dynasty = "Jul-Zon"
		birth_date = 631.1.1
		adm = 2
		dip = 5
		mil = 4
    }
}

726.1.1 = {
	monarch = {
 		name = "Lot"
		dynasty = "Jul-Vevruo"
		birth_date = 692.1.1
		adm = 0
		dip = 5
		mil = 1
    }
}

769.1.1 = {
	monarch = {
 		name = "Leltrig"
		dynasty = "Jul-Iljke"
		birth_date = 741.1.1
		adm = 2
		dip = 6
		mil = 3
    }
}

836.1.1 = {
	monarch = {
 		name = "Sodog"
		dynasty = "Jul-Vittazu"
		birth_date = 804.1.1
		adm = 0
		dip = 5
		mil = 6
    }
}

934.1.1 = {
	monarch = {
 		name = "Guhro"
		dynasty = "Jul-Iljke"
		birth_date = 890.1.1
		adm = 5
		dip = 4
		mil = 3
		female = yes
    }
}

996.1.1 = {
	monarch = {
 		name = "Banvid"
		dynasty = "Jul-Tuvru"
		birth_date = 973.1.1
		adm = 3
		dip = 4
		mil = 0
    }
}

1058.1.1 = {
	monarch = {
 		name = "Zundadull"
		dynasty = "Jul-Derdazeth"
		birth_date = 1013.1.1
		adm = 6
		dip = 5
		mil = 2
    }
}

1120.1.1 = {
	monarch = {
 		name = "Ferin"
		dynasty = "Jul-Derdazeth"
		birth_date = 1074.1.1
		adm = 1
		dip = 6
		mil = 3
		female = yes
    }
}

1207.1.1 = {
	monarch = {
 		name = "Crir"
		dynasty = "Jul-Cabru"
		birth_date = 1189.1.1
		adm = 6
		dip = 5
		mil = 0
    }
}

1291.1.1 = {
	monarch = {
 		name = "Throlraars"
		dynasty = "Jul-Samledol"
		birth_date = 1256.1.1
		adm = 5
		dip = 5
		mil = 4
    }
}

1388.1.1 = {
	monarch = {
 		name = "Sar"
		dynasty = "Jul-Rojke"
		birth_date = 1364.1.1
		adm = 3
		dip = 4
		mil = 0
    }
}

1427.1.1 = {
	monarch = {
 		name = "Hraizzos"
		dynasty = "Jul-Tuvru"
		birth_date = 1400.1.1
		adm = 4
		dip = 1
		mil = 5
    }
}

1509.1.1 = {
	monarch = {
 		name = "Goldrait"
		dynasty = "Jul-Fehgon"
		birth_date = 1484.1.1
		adm = 2
		dip = 0
		mil = 2
    }
}

1590.1.1 = {
	monarch = {
 		name = "Khastra"
		dynasty = "Jul-Iljke"
		birth_date = 1547.1.1
		adm = 0
		dip = 6
		mil = 6
    }
}

1630.1.1 = {
	monarch = {
 		name = "Simlax"
		dynasty = "Jul-Dosh"
		birth_date = 1610.1.1
		adm = 5
		dip = 5
		mil = 2
		female = yes
    }
}

1691.1.1 = {
	monarch = {
 		name = "Kodherg"
		dynasty = "Jul-Igotax"
		birth_date = 1662.1.1
		adm = 6
		dip = 3
		mil = 0
    }
}

1739.1.1 = {
	monarch = {
 		name = "Gningurk"
		dynasty = "Jul-Sighurday"
		birth_date = 1713.1.1
		adm = 0
		dip = 0
		mil = 5
    }
}

1814.1.1 = {
	monarch = {
 		name = "Shagarga"
		dynasty = "Jul-Nulta"
		birth_date = 1778.1.1
		adm = 5
		dip = 6
		mil = 2
		female = yes
    }
}

1899.1.1 = {
	monarch = {
 		name = "Rhedrouran"
		dynasty = "Jul-Faighoullo"
		birth_date = 1871.1.1
		adm = 3
		dip = 5
		mil = 5
		female = yes
    }
}

1940.1.1 = {
	monarch = {
 		name = "Bakhu"
		dynasty = "Jul-Iljke"
		birth_date = 1911.1.1
		adm = 1
		dip = 4
		mil = 2
    }
}

1978.1.1 = {
	monarch = {
 		name = "Mars"
		dynasty = "Jul-Potaybeth"
		birth_date = 1947.1.1
		adm = 0
		dip = 3
		mil = 6
    }
}

2018.1.1 = {
	monarch = {
 		name = "Leltrig"
		dynasty = "Jul-Keth"
		birth_date = 1996.1.1
		adm = 5
		dip = 3
		mil = 2
    }
}

2113.1.1 = {
	monarch = {
 		name = "Sodog"
		dynasty = "Jul-Eblu"
		birth_date = 2066.1.1
		adm = 3
		dip = 2
		mil = 6
    }
}

2152.1.1 = {
	monarch = {
 		name = "Igetu"
		dynasty = "Jul-Gogrol"
		birth_date = 2122.1.1
		adm = 1
		dip = 1
		mil = 3
		female = yes
    }
}

2242.1.1 = {
	monarch = {
 		name = "Mars"
		dynasty = "Jul-Noh"
		birth_date = 2209.1.1
		adm = 3
		dip = 2
		mil = 4
    }
}

2290.1.1 = {
	monarch = {
 		name = "Leltrig"
		dynasty = "Jul-Hata"
		birth_date = 2256.1.1
		adm = 2
		dip = 2
		mil = 1
    }
}

2327.1.1 = {
	monarch = {
 		name = "Rhuglu"
		dynasty = "Jul-Ful"
		birth_date = 2274.1.1
		adm = 0
		dip = 1
		mil = 4
		female = yes
    }
}

2365.1.1 = {
	monarch = {
 		name = "Dendrigaak"
		dynasty = "Jul-Vittazu"
		birth_date = 2344.1.1
		adm = 5
		dip = 0
		mil = 1
    }
}

2404.1.1 = {
	monarch = {
 		name = "Brigho"
		dynasty = "Jul-Uzemluox"
		birth_date = 2372.1.1
		adm = 3
		dip = 6
		mil = 5
		female = yes
    }
}

2461.1.1 = {
	monarch = {
 		name = "Krogaabad"
		dynasty = "Jul-Ful"
		birth_date = 2424.1.1
		adm = 2
		dip = 5
		mil = 1
    }
}

