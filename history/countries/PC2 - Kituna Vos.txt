government = monarchy
government_rank = 1
mercantilism = 1
technology_group = pyandonea_tg
religion = serpant_king
primary_culture = maormer
capital = 2039
secondary_religion = hermeus_mora_cult

54.1.1 = {
	monarch = {
 		name = "Mudo"
		dynasty = "Nu-Ilgiduurnu"
		birth_date = 22.1.1
		adm = 5
		dip = 3
		mil = 3
    }
	queen = {
 		name = "Eilse"
		dynasty = "Nu-Ilgiduurnu"
		birth_date = 11.1.1
		adm = 2
		dip = 2
		mil = 2
		female = yes
    }
}

134.1.1 = {
	monarch = {
 		name = "Panohalamion"
		dynasty = "Nu-Cemranmel"
		birth_date = 112.1.1
		adm = 0
		dip = 1
		mil = 5
		female = yes
    }
	queen = {
 		name = "Heyrral"
		dynasty = "Nu-Cemranmel"
		birth_date = 111.1.1
		adm = 4
		dip = 0
		mil = 4
    }
	heir = {
 		name = "Tyisral"
		monarch_name = "Tyisral I"
		dynasty = "Nu-Cemranmel"
		birth_date = 123.1.1
		death_date = 201.1.1
		claim = 100
		adm = 1
		dip = 5
		mil = 3
    }
}

201.1.1 = {
	monarch = {
 		name = "Vilaroonmion"
		dynasty = "Na-Puhlynvurnia"
		birth_date = 176.1.1
		adm = 4
		dip = 6
		mil = 5
		female = yes
    }
}

276.1.1 = {
	monarch = {
 		name = "Cecrasnil"
		dynasty = "Ne-Jorlos"
		birth_date = 231.1.1
		adm = 2
		dip = 6
		mil = 2
    }
	queen = {
 		name = "Hetsha"
		dynasty = "Ne-Jorlos"
		birth_date = 236.1.1
		adm = 6
		dip = 4
		mil = 1
		female = yes
    }
}

324.1.1 = {
	monarch = {
 		name = "Laaviil"
		dynasty = "Na-Jasmo"
		birth_date = 288.1.1
		adm = 4
		dip = 3
		mil = 5
    }
	queen = {
 		name = "Chryiphidi"
		dynasty = "Na-Jasmo"
		birth_date = 291.1.1
		adm = 1
		dip = 2
		mil = 4
		female = yes
    }
}

395.1.1 = {
	monarch = {
 		name = "Cinqraasha"
		dynasty = "Na-Dhally"
		birth_date = 356.1.1
		adm = 6
		dip = 1
		mil = 0
		female = yes
    }
}

488.1.1 = {
	monarch = {
 		name = "Vihetdir"
		dynasty = "Ni-Vylrul"
		birth_date = 438.1.1
		adm = 4
		dip = 0
		mil = 4
		female = yes
    }
	queen = {
 		name = "Jyklos"
		dynasty = "Ni-Vylrul"
		birth_date = 470.1.1
		adm = 1
		dip = 6
		mil = 3
    }
	heir = {
 		name = "Vismonnennum"
		monarch_name = "Vismonnennum I"
		dynasty = "Ni-Vylrul"
		birth_date = 482.1.1
		death_date = 525.1.1
		claim = 100
		adm = 5
		dip = 4
		mil = 2
    }
}

525.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Orgnum"
		monarch_name = "Orgnum I"
		dynasty = "Ni-Vyssolriol"
		birth_date = 511.1.1
		death_date = 529.1.1
		claim = 100
		adm = 3
		dip = 4
		mil = 6
    }
}

529.1.1 = {
	monarch = {
 		name = "Heculoa"
		dynasty = "Na-Ihnode"
		birth_date = 496.1.1
		adm = 3
		dip = 0
		mil = 6
    }
}

570.1.1 = {
	monarch = {
 		name = "Yrdron"
		dynasty = "Ne-Onlulwis"
		birth_date = 546.1.1
		adm = 1
		dip = 6
		mil = 2
    }
	queen = {
 		name = "Ienurdil"
		dynasty = "Ne-Onlulwis"
		birth_date = 521.1.1
		adm = 5
		dip = 5
		mil = 1
		female = yes
    }
	heir = {
 		name = "Vismonnennum"
		monarch_name = "Vismonnennum I"
		dynasty = "Ne-Onlulwis"
		birth_date = 565.1.1
		death_date = 623.1.1
		claim = 100
		adm = 2
		dip = 3
		mil = 0
    }
}

623.1.1 = {
	monarch = {
 		name = "Loqraacecdron"
		dynasty = "Ne-Yohnysh"
		birth_date = 590.1.1
		adm = 4
		dip = 5
		mil = 3
    }
	queen = {
 		name = "Oharasyeda"
		dynasty = "Ne-Yohnysh"
		birth_date = 590.1.1
		adm = 1
		dip = 3
		mil = 2
		female = yes
    }
	heir = {
 		name = "Larnil"
		monarch_name = "Larnil I"
		dynasty = "Ne-Yohnysh"
		birth_date = 613.1.1
		death_date = 716.1.1
		claim = 100
		adm = 5
		dip = 2
		mil = 1
    }
}

716.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Merlolos"
		monarch_name = "Merlolos I"
		dynasty = "Nu-Mallyrylvan"
		birth_date = 711.1.1
		death_date = 729.1.1
		claim = 100
		adm = 6
		dip = 6
		mil = 6
    }
}

729.1.1 = {
	monarch = {
 		name = "Caryrparnil"
		dynasty = "Nu-Nadymodi"
		birth_date = 676.1.1
		adm = 2
		dip = 0
		mil = 1
    }
}

820.1.1 = {
	monarch = {
 		name = "Unandil"
		dynasty = "Ne-Ynlath"
		birth_date = 777.1.1
		adm = 0
		dip = 6
		mil = 5
    }
}

904.1.1 = {
	monarch = {
 		name = "Nitil"
		dynasty = "Na-Othy"
		birth_date = 858.1.1
		adm = 5
		dip = 6
		mil = 1
    }
	queen = {
 		name = "Lermonda"
		dynasty = "Na-Othy"
		birth_date = 868.1.1
		adm = 2
		dip = 4
		mil = 0
		female = yes
    }
}

977.1.1 = {
	monarch = {
 		name = "Vagun"
		dynasty = "Nu-Lyssir"
		birth_date = 924.1.1
		adm = 0
		dip = 3
		mil = 4
    }
	queen = {
 		name = "Ohaevda"
		dynasty = "Nu-Lyssir"
		birth_date = 945.1.1
		adm = 4
		dip = 2
		mil = 3
		female = yes
    }
	heir = {
 		name = "Jahuuyetil"
		monarch_name = "Jahuuyetil I"
		dynasty = "Nu-Lyssir"
		birth_date = 970.1.1
		death_date = 1033.1.1
		claim = 100
		adm = 1
		dip = 0
		mil = 2
    }
}

1033.1.1 = {
	monarch = {
 		name = "Jykoth"
		dynasty = "Nu-Himlaalniu"
		birth_date = 997.1.1
		adm = 0
		dip = 4
		mil = 2
    }
}

1109.1.1 = {
	monarch = {
 		name = "Ohmanil"
		dynasty = "Na-Ocis"
		birth_date = 1074.1.1
		adm = 6
		dip = 3
		mil = 6
    }
}

1153.1.1 = {
	monarch = {
 		name = "Laaviil"
		dynasty = "Ne-Jelerwuht"
		birth_date = 1117.1.1
		adm = 4
		dip = 2
		mil = 2
    }
}

1219.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Achenum"
		monarch_name = "Achenum I"
		dynasty = "Na-Ospeht"
		birth_date = 1219.1.1
		death_date = 1237.1.1
		claim = 100
		adm = 2
		dip = 1
		mil = 6
    }
}

1237.1.1 = {
	monarch = {
 		name = "Monwe"
		dynasty = "Ni-Ilgomeddoa"
		birth_date = 1213.1.1
		adm = 1
		dip = 5
		mil = 6
    }
}

1304.1.1 = {
	monarch = {
 		name = "Monneieis"
		dynasty = "Nu-Eiloldoh"
		birth_date = 1278.1.1
		adm = 0
		dip = 4
		mil = 3
		female = yes
    }
}

1397.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Rodisloa"
		monarch_name = "Rodisloa I"
		dynasty = "Na-Yorhasri"
		birth_date = 1388.1.1
		death_date = 1406.1.1
		claim = 100
		adm = 4
		dip = 6
		mil = 3
    }
}

1406.1.1 = {
	monarch = {
 		name = "Isardor"
		dynasty = "Nu-Qrymlite"
		birth_date = 1367.1.1
		adm = 3
		dip = 2
		mil = 3
    }
	queen = {
 		name = "Guliesha"
		dynasty = "Nu-Qrymlite"
		birth_date = 1353.1.1
		adm = 0
		dip = 1
		mil = 2
		female = yes
    }
	heir = {
 		name = "Ieardron"
		monarch_name = "Ieardron I"
		dynasty = "Nu-Qrymlite"
		birth_date = 1400.1.1
		death_date = 1458.1.1
		claim = 100
		adm = 4
		dip = 6
		mil = 1
    }
}

1458.1.1 = {
	monarch = {
 		name = "Uldor"
		dynasty = "Ne-Vorrullulio"
		birth_date = 1405.1.1
		adm = 0
		dip = 1
		mil = 4
    }
	queen = {
 		name = "Iriene"
		dynasty = "Ne-Vorrullulio"
		birth_date = 1432.1.1
		adm = 4
		dip = 6
		mil = 2
		female = yes
    }
	heir = {
 		name = "Rodevsamnum"
		monarch_name = "Rodevsamnum I"
		dynasty = "Ne-Vorrullulio"
		birth_date = 1443.1.1
		death_date = 1530.1.1
		claim = 100
		adm = 0
		dip = 5
		mil = 1
    }
}

1530.1.1 = {
	monarch = {
 		name = "Isardor"
		dynasty = "Ne-Ihni"
		birth_date = 1507.1.1
		adm = 3
		dip = 6
		mil = 4
    }
}

1601.1.1 = {
	monarch = {
 		name = "Camandar"
		dynasty = "Na-Elnah"
		birth_date = 1564.1.1
		adm = 5
		dip = 0
		mil = 5
    }
}

1684.1.1 = {
	monarch = {
 		name = "Uldor"
		dynasty = "Na-Junuth"
		birth_date = 1651.1.1
		adm = 3
		dip = 0
		mil = 2
    }
}

1748.1.1 = {
	monarch = {
 		name = "Sameht"
		dynasty = "Na-Dhally"
		birth_date = 1712.1.1
		adm = 2
		dip = 6
		mil = 6
		female = yes
    }
}

1803.1.1 = {
	monarch = {
 		name = "Unandil"
		dynasty = "Ne-Chomnath"
		birth_date = 1777.1.1
		adm = 0
		dip = 5
		mil = 2
    }
}

1842.1.1 = {
	monarch = {
 		name = "Ninordor"
		dynasty = "Nu-Lydhidrius"
		birth_date = 1805.1.1
		adm = 5
		dip = 4
		mil = 6
    }
}

1884.1.1 = {
	monarch = {
 		name = "Isculpandil"
		dynasty = "Ne-Nilminval"
		birth_date = 1856.1.1
		adm = 3
		dip = 4
		mil = 3
    }
	queen = {
 		name = "Anlaadil"
		dynasty = "Ne-Nilminval"
		birth_date = 1865.1.1
		adm = 0
		dip = 2
		mil = 2
		female = yes
    }
	heir = {
 		name = "Ilral"
		monarch_name = "Ilral I"
		dynasty = "Ne-Nilminval"
		birth_date = 1873.1.1
		death_date = 1968.1.1
		claim = 100
		adm = 4
		dip = 1
		mil = 1
    }
}

1968.1.1 = {
	monarch = {
 		name = "Umhelel"
		dynasty = "Nu-Vinimdul"
		birth_date = 1916.1.1
		adm = 0
		dip = 2
		mil = 3
    }
	queen = {
 		name = "Armalda"
		dynasty = "Nu-Vinimdul"
		birth_date = 1915.1.1
		adm = 4
		dip = 0
		mil = 2
		female = yes
    }
}

2067.1.1 = {
	monarch = {
 		name = "Cingulnya"
		dynasty = "Ne-Yutshun"
		birth_date = 2038.1.1
		adm = 2
		dip = 0
		mil = 5
    }
}

2121.1.1 = {
	monarch = {
 		name = "Chrynorisse"
		dynasty = "Nu-Piryo"
		birth_date = 2074.1.1
		adm = 4
		dip = 1
		mil = 0
		female = yes
    }
}

2204.1.1 = {
	monarch = {
 		name = "Pelwe"
		dynasty = "Nu-Ile"
		birth_date = 2158.1.1
		adm = 2
		dip = 0
		mil = 4
		female = yes
    }
}

2266.1.1 = {
	monarch = {
 		name = "Valaran"
		dynasty = "Ni-Jykheh"
		birth_date = 2248.1.1
		adm = 0
		dip = 6
		mil = 0
    }
	queen = {
 		name = "Arpanculdir"
		dynasty = "Ni-Jykheh"
		birth_date = 2245.1.1
		adm = 4
		dip = 5
		mil = 6
		female = yes
    }
}

2356.1.1 = {
	monarch = {
 		name = "Iriene"
		dynasty = "Nu-Vymnur"
		birth_date = 2318.1.1
		adm = 2
		dip = 4
		mil = 3
		female = yes
    }
	queen = {
 		name = "Nordo"
		dynasty = "Nu-Vymnur"
		birth_date = 2336.1.1
		adm = 6
		dip = 3
		mil = 2
    }
}

2439.1.1 = {
	monarch = {
 		name = "Mallinpansar"
		dynasty = "Ne-Tectordyslo"
		birth_date = 2397.1.1
		adm = 5
		dip = 2
		mil = 6
    }
	queen = {
 		name = "Iphirinyaor"
		dynasty = "Ne-Tectordyslo"
		birth_date = 2406.1.1
		adm = 1
		dip = 0
		mil = 5
		female = yes
    }
	heir = {
 		name = "Ohasri"
		monarch_name = "Ohasri I"
		dynasty = "Ne-Tectordyslo"
		birth_date = 2435.1.1
		death_date = 2495.1.1
		claim = 100
		adm = 5
		dip = 6
		mil = 3
		female = yes
    }
}

2495.1.1 = {
	monarch = {
 		name = "Despina"
		dynasty = "Ni-Qihma"
		birth_date = 2458.1.1
		adm = 4
		dip = 5
		mil = 0
		female = yes
    }
}

