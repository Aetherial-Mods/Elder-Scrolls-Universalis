government = native
government_rank = 1
mercantilism = 1
technology_group = atmora_tg
religion = dragon_cult
primary_culture = glacial
capital = 458

54.1.1 = {
	monarch = {
 		name = "Kirkom"
		dynasty = "Gilsganl"
		birth_date = 26.1.1
		adm = 2
		dip = 0
		mil = 2
    }
}

139.1.1 = {
	monarch = {
 		name = "Brungolg"
		dynasty = "Tusgrirr"
		birth_date = 98.1.1
		adm = 0
		dip = 6
		mil = 6
    }
}

237.1.1 = {
	monarch = {
 		name = "Svedryl"
		dynasty = "Skitgerr"
		birth_date = 213.1.1
		adm = 6
		dip = 5
		mil = 2
		female = yes
    }
}

333.1.1 = {
	monarch = {
 		name = "Storgath"
		dynasty = "Dilg"
		birth_date = 293.1.1
		adm = 4
		dip = 4
		mil = 6
    }
}

397.1.1 = {
	monarch = {
 		name = "Hleirjin"
		dynasty = "Skendeinl"
		birth_date = 361.1.1
		adm = 2
		dip = 4
		mil = 2
		female = yes
    }
}

446.1.1 = {
	monarch = {
 		name = "Hrund"
		dynasty = "Tosgril"
		birth_date = 415.1.1
		adm = 0
		dip = 3
		mil = 6
		female = yes
    }
}

492.1.1 = {
	monarch = {
 		name = "Sirlannen"
		dynasty = "Keld"
		birth_date = 472.1.1
		adm = 6
		dip = 2
		mil = 3
    }
}

577.1.1 = {
	monarch = {
 		name = "Frargil"
		dynasty = "Budek"
		birth_date = 528.1.1
		adm = 4
		dip = 1
		mil = 6
    }
}

646.1.1 = {
	monarch = {
 		name = "Rholl"
		dynasty = "Storgath"
		birth_date = 626.1.1
		adm = 6
		dip = 3
		mil = 1
    }
}

717.1.1 = {
	monarch = {
 		name = "Frem"
		dynasty = "Skendeinl"
		birth_date = 666.1.1
		adm = 4
		dip = 2
		mil = 5
    }
}

816.1.1 = {
	monarch = {
 		name = "Modmeldil"
		dynasty = "Rotgild"
		birth_date = 795.1.1
		adm = 2
		dip = 1
		mil = 1
		female = yes
    }
}

888.1.1 = {
	monarch = {
 		name = "Thugnom"
		dynasty = "Roewallom"
		birth_date = 865.1.1
		adm = 1
		dip = 0
		mil = 5
    }
}

953.1.1 = {
	monarch = {
 		name = "Magdrond"
		dynasty = "Thelbell"
		birth_date = 900.1.1
		adm = 6
		dip = 6
		mil = 2
		female = yes
    }
}

1017.1.1 = {
	monarch = {
 		name = "Kosner"
		dynasty = "Rotmalleskr"
		birth_date = 973.1.1
		adm = 4
		dip = 6
		mil = 5
    }
}

1070.1.1 = {
	monarch = {
 		name = "Modmeldil"
		dynasty = "Bretgolak"
		birth_date = 1051.1.1
		adm = 2
		dip = 5
		mil = 2
		female = yes
    }
}

1127.1.1 = {
	monarch = {
 		name = "Thugnom"
		dynasty = "Jochin"
		birth_date = 1074.1.1
		adm = 1
		dip = 4
		mil = 5
    }
}

1164.1.1 = {
	monarch = {
 		name = "Dyrulleim"
		dynasty = "Frechom"
		birth_date = 1134.1.1
		adm = 2
		dip = 5
		mil = 0
    }
}

1227.1.1 = {
	monarch = {
 		name = "Kosner"
		dynasty = "Tewam"
		birth_date = 1175.1.1
		adm = 1
		dip = 4
		mil = 4
    }
}

1286.1.1 = {
	monarch = {
 		name = "Skoetgogam"
		dynasty = "Frowunnirr"
		birth_date = 1262.1.1
		adm = 6
		dip = 4
		mil = 0
    }
}

1356.1.1 = {
	monarch = {
 		name = "Hrialmuhon"
		dynasty = "Dormam"
		birth_date = 1317.1.1
		adm = 4
		dip = 3
		mil = 4
		female = yes
    }
}

1411.1.1 = {
	monarch = {
 		name = "Tilgeskr"
		dynasty = "Tiwon"
		birth_date = 1370.1.1
		adm = 2
		dip = 2
		mil = 1
    }
}

1476.1.1 = {
	monarch = {
 		name = "Vilskemild"
		dynasty = "Keewogik"
		birth_date = 1434.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1549.1.1 = {
	monarch = {
 		name = "Skoetgogam"
		dynasty = "Skylskak"
		birth_date = 1518.1.1
		adm = 6
		dip = 1
		mil = 1
    }
}

1607.1.1 = {
	monarch = {
 		name = "Vukniak"
		dynasty = "Thirmoskr"
		birth_date = 1586.1.1
		adm = 4
		dip = 0
		mil = 5
    }
}

1653.1.1 = {
	monarch = {
 		name = "Hoksild"
		dynasty = "Rhanwall"
		birth_date = 1604.1.1
		adm = 6
		dip = 1
		mil = 6
    }
}

1748.1.1 = {
	monarch = {
 		name = "Heirubaf"
		dynasty = "Sirlannen"
		birth_date = 1701.1.1
		adm = 4
		dip = 0
		mil = 3
		female = yes
    }
}

1786.1.1 = {
	monarch = {
 		name = "Boechyrom"
		dynasty = "Frurramth"
		birth_date = 1743.1.1
		adm = 3
		dip = 6
		mil = 6
    }
}

1842.1.1 = {
	monarch = {
 		name = "Skechilg"
		dynasty = "Fudemth"
		birth_date = 1817.1.1
		adm = 1
		dip = 6
		mil = 3
    }
}

1902.1.1 = {
	monarch = {
 		name = "Kurmok"
		dynasty = "Dilg"
		birth_date = 1853.1.1
		adm = 6
		dip = 5
		mil = 0
    }
}

1956.1.1 = {
	monarch = {
 		name = "Bruleihof"
		dynasty = "Thelbell"
		birth_date = 1936.1.1
		adm = 4
		dip = 4
		mil = 3
		female = yes
    }
}

2009.1.1 = {
	monarch = {
 		name = "Sisgamth"
		dynasty = "Stisgreskr"
		birth_date = 1967.1.1
		adm = 3
		dip = 3
		mil = 0
    }
}

2100.1.1 = {
	monarch = {
 		name = "Skechilg"
		dynasty = "Tiwon"
		birth_date = 2055.1.1
		adm = 1
		dip = 2
		mil = 4
    }
}

2146.1.1 = {
	monarch = {
 		name = "Kurmok"
		dynasty = "Skechilg"
		birth_date = 2098.1.1
		adm = 3
		dip = 4
		mil = 5
    }
}

2182.1.1 = {
	monarch = {
 		name = "Thelbell"
		dynasty = "Folsgold"
		birth_date = 2155.1.1
		adm = 5
		dip = 5
		mil = 1
    }
}

2267.1.1 = {
	monarch = {
 		name = "Klolsgonl"
		dynasty = "Tiwon"
		birth_date = 2216.1.1
		adm = 4
		dip = 4
		mil = 4
    }
}

2326.1.1 = {
	monarch = {
 		name = "Sungiaskr"
		dynasty = "Bor"
		birth_date = 2281.1.1
		adm = 4
		dip = 1
		mil = 2
    }
}

2398.1.1 = {
	monarch = {
 		name = "Holam"
		dynasty = "Vilskemild"
		birth_date = 2350.1.1
		adm = 3
		dip = 1
		mil = 6
    }
}

2490.1.1 = {
	monarch = {
 		name = "Folsgold"
		dynasty = "Delorr"
		birth_date = 2438.1.1
		adm = 1
		dip = 0
		mil = 2
    }
}

