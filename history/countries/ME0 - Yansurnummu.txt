government = tribal
government_rank = 1
mercantilism = 1
technology_group = exiled_tg
religion = chimer_pantheon
primary_culture = ashlander_chimer
capital = 3818

54.1.1 = {
	monarch = {
 		name = "Maeli"
		dynasty = "Zeba-Adad"
		birth_date = 23.1.1
		adm = 0
		dip = 3
		mil = 2
		female = yes
    }
}

127.1.1 = {
	monarch = {
 		name = "Shanat"
		dynasty = "Assurnumausur"
		birth_date = 93.1.1
		adm = 6
		dip = 2
		mil = 6
    }
}

187.1.1 = {
	monarch = {
 		name = "Lassour"
		dynasty = "Lasamsi"
		birth_date = 145.1.1
		adm = 4
		dip = 1
		mil = 3
    }
}

279.1.1 = {
	monarch = {
 		name = "Assantus"
		dynasty = "Assudiraplit"
		birth_date = 261.1.1
		adm = 2
		dip = 0
		mil = 6
    }
}

364.1.1 = {
	monarch = {
 		name = "Zalit"
		dynasty = "Eraishah"
		birth_date = 311.1.1
		adm = 0
		dip = 6
		mil = 3
    }
}

410.1.1 = {
	monarch = {
 		name = "Seba"
		dynasty = "Hainterari"
		birth_date = 375.1.1
		adm = 6
		dip = 6
		mil = 0
		female = yes
    }
}

453.1.1 = {
	monarch = {
 		name = "Manabi"
		dynasty = "Ashun-Idantus"
		birth_date = 435.1.1
		adm = 4
		dip = 5
		mil = 3
		female = yes
    }
}

529.1.1 = {
	monarch = {
 		name = "Assumanu"
		dynasty = "Assinabi"
		birth_date = 490.1.1
		adm = 6
		dip = 6
		mil = 5
		female = yes
    }
}

569.1.1 = {
	monarch = {
 		name = "Yenabi"
		dynasty = "Kuntarnammu"
		birth_date = 549.1.1
		adm = 4
		dip = 5
		mil = 1
		female = yes
    }
}

656.1.1 = {
	monarch = {
 		name = "Assurdan"
		dynasty = "Asharapli"
		birth_date = 607.1.1
		adm = 2
		dip = 5
		mil = 5
    }
}

736.1.1 = {
	monarch = {
 		name = "Zanmulk"
		dynasty = "Odin-Ahhe"
		birth_date = 701.1.1
		adm = 1
		dip = 4
		mil = 2
    }
}

806.1.1 = {
	monarch = {
 		name = "Shannat"
		dynasty = "Saddarnuran"
		birth_date = 788.1.1
		adm = 6
		dip = 3
		mil = 5
    }
}

855.1.1 = {
	monarch = {
 		name = "Maesat"
		dynasty = "Sanammasour"
		birth_date = 828.1.1
		adm = 4
		dip = 2
		mil = 2
    }
}

896.1.1 = {
	monarch = {
 		name = "Benudni"
		dynasty = "Assunudadnud"
		birth_date = 862.1.1
		adm = 2
		dip = 1
		mil = 6
		female = yes
    }
}

980.1.1 = {
	monarch = {
 		name = "Zanat"
		dynasty = "Asserrumusa"
		birth_date = 948.1.1
		adm = 1
		dip = 1
		mil = 2
    }
}

1055.1.1 = {
	monarch = {
 		name = "Senipu"
		dynasty = "Sonnerralit"
		birth_date = 1027.1.1
		adm = 2
		dip = 2
		mil = 4
		female = yes
    }
}

1108.1.1 = {
	monarch = {
 		name = "Maesat"
		dynasty = "Mirpal"
		birth_date = 1060.1.1
		adm = 1
		dip = 1
		mil = 1
    }
}

1160.1.1 = {
	monarch = {
 		name = "Shali"
		dynasty = "Assaplit"
		birth_date = 1142.1.1
		adm = 6
		dip = 0
		mil = 4
		female = yes
    }
}

1225.1.1 = {
	monarch = {
 		name = "Mansilamat"
		dynasty = "Assumanallit"
		birth_date = 1182.1.1
		adm = 4
		dip = 6
		mil = 1
    }
}

1294.1.1 = {
	monarch = {
 		name = "Beden"
		dynasty = "Zabynatus"
		birth_date = 1252.1.1
		adm = 2
		dip = 6
		mil = 5
    }
}

1362.1.1 = {
	monarch = {
 		name = "Zanummu"
		dynasty = "Shashmassamsi"
		birth_date = 1311.1.1
		adm = 1
		dip = 5
		mil = 1
		female = yes
    }
}

1421.1.1 = {
	monarch = {
 		name = "Shali"
		dynasty = "Kaushminipu"
		birth_date = 1373.1.1
		adm = 6
		dip = 4
		mil = 5
		female = yes
    }
}

1502.1.1 = {
	monarch = {
 		name = "Mi-Ilu"
		dynasty = "Assurnarairan"
		birth_date = 1473.1.1
		adm = 4
		dip = 3
		mil = 1
		female = yes
    }
}

1568.1.1 = {
	monarch = {
 		name = "Beden"
		dynasty = "Assillariran"
		birth_date = 1537.1.1
		adm = 6
		dip = 5
		mil = 3
    }
}

1645.1.1 = {
	monarch = {
 		name = "Zanummu"
		dynasty = "Assarbeberib"
		birth_date = 1605.1.1
		adm = 4
		dip = 4
		mil = 0
		female = yes
    }
}

1735.1.1 = {
	monarch = {
 		name = "Dralas"
		dynasty = "Shamirbasour"
		birth_date = 1717.1.1
		adm = 3
		dip = 3
		mil = 3
    }
}

1803.1.1 = {
	monarch = {
 		name = "Zenabi"
		dynasty = "Shimmabadas"
		birth_date = 1765.1.1
		adm = 1
		dip = 2
		mil = 0
    }
}

1899.1.1 = {
	monarch = {
 		name = "Shin"
		dynasty = "Yassabisun"
		birth_date = 1853.1.1
		adm = 6
		dip = 1
		mil = 4
    }
}

1969.1.1 = {
	monarch = {
 		name = "Massour"
		dynasty = "Urshummarnamus"
		birth_date = 1934.1.1
		adm = 4
		dip = 1
		mil = 0
    }
}

2039.1.1 = {
	monarch = {
 		name = "Yapal"
		dynasty = "Yassabisun"
		birth_date = 2011.1.1
		adm = 4
		dip = 4
		mil = 0
    }
}

2101.1.1 = {
	monarch = {
 		name = "Ashuma-Nud"
		dynasty = "Ashirbibi"
		birth_date = 2077.1.1
		adm = 2
		dip = 3
		mil = 4
    }
}

2193.1.1 = {
	monarch = {
 		name = "Yenammu"
		dynasty = "Lasamsi"
		birth_date = 2148.1.1
		adm = 0
		dip = 2
		mil = 1
    }
}

2245.1.1 = {
	monarch = {
 		name = "Salmus"
		dynasty = "Addunipu"
		birth_date = 2220.1.1
		adm = 5
		dip = 2
		mil = 4
    }
}

2329.1.1 = {
	monarch = {
 		name = "Lanabi"
		dynasty = "Sobdishapal"
		birth_date = 2286.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

2388.1.1 = {
	monarch = {
 		name = "Ashu-Ahhe"
		dynasty = "Enturnabaelul"
		birth_date = 2346.1.1
		adm = 2
		dip = 0
		mil = 5
    }
}

2437.1.1 = {
	monarch = {
 		name = "Yen"
		dynasty = "Urshummarnamus"
		birth_date = 2416.1.1
		adm = 0
		dip = 6
		mil = 1
    }
}

2482.1.1 = {
	monarch = {
 		name = "Salmus"
		dynasty = "Shimmabadas"
		birth_date = 2432.1.1
		adm = 2
		dip = 0
		mil = 3
    }
}

