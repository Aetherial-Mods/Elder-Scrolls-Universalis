government = republic
government_rank = 1
mercantilism = 1
technology_group = cyrodiil_tg
religion = peryite_cult
primary_culture = ayleid
capital = 1236

54.1.1 = {
	monarch = {
 		name = "Pallush"
		dynasty = "Mea-Conoa"
		birth_date = 12.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

149.1.1 = {
	monarch = {
 		name = "Narilmor"
		dynasty = "Ula-Marcerienna"
		birth_date = 106.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

201.1.1 = {
	monarch = {
 		name = "Cymylmuumer"
		dynasty = "Ula-Horobrina"
		birth_date = 173.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

249.1.1 = {
	monarch = {
 		name = "Fin"
		dynasty = "Vae-Vesagrius"
		birth_date = 203.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

345.1.1 = {
	monarch = {
 		name = "Nyrrin"
		dynasty = "Vae-Selviloria"
		birth_date = 311.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

440.1.1 = {
	monarch = {
 		name = "Hunnul"
		dynasty = "Vae-Silorn"
		birth_date = 394.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

492.1.1 = {
	monarch = {
 		name = "Nilichi"
		dynasty = "Vae-Tirerius"
		birth_date = 458.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

528.1.1 = {
	monarch = {
 		name = "Rorhemeras"
		dynasty = "Ula-Macelius"
		birth_date = 481.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

580.1.1 = {
	monarch = {
 		name = "Nyrrin"
		dynasty = "Ula-Renne"
		birth_date = 549.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

649.1.1 = {
	monarch = {
 		name = "Vem"
		dynasty = "Ula-Homestead"
		birth_date = 630.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

712.1.1 = {
	monarch = {
 		name = "Carro"
		dynasty = "Ula-Istirus"
		birth_date = 694.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

790.1.1 = {
	monarch = {
 		name = "Tamhuundogo"
		dynasty = "Vae-Sercen"
		birth_date = 769.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

825.1.1 = {
	monarch = {
 		name = "Uluscant"
		dynasty = "Mea-Amane"
		birth_date = 806.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

903.1.1 = {
	monarch = {
 		name = "Vem"
		dynasty = "Vae-Stirk"
		birth_date = 852.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

965.1.1 = {
	monarch = {
 		name = "Gemhel"
		dynasty = "Ula-Rusifus"
		birth_date = 938.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1043.1.1 = {
	monarch = {
 		name = "Hennynlyho"
		dynasty = "Mea-Crownhaven"
		birth_date = 990.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1135.1.1 = {
	monarch = {
 		name = "Tjurhane"
		dynasty = "Vae-Vilverin"
		birth_date = 1091.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1196.1.1 = {
	monarch = {
 		name = "Tuanum"
		dynasty = "Mea-Culotte"
		birth_date = 1164.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1292.1.1 = {
	monarch = {
 		name = "Nym"
		dynasty = "		"
		birth_date = 1247.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1379.1.1 = {
	monarch = {
 		name = "Linduasdesem"
		dynasty = "Mea-Andrulusus"
		birth_date = 1344.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1469.1.1 = {
	monarch = {
 		name = "Djel"
		dynasty = "Ula-Julidonea"
		birth_date = 1445.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1540.1.1 = {
	monarch = {
 		name = "Fyndiscas"
		dynasty = "Mea-Bawn"
		birth_date = 1496.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1592.1.1 = {
	monarch = {
 		name = "Nym"
		dynasty = "Vae-Vlastarus"
		birth_date = 1552.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1684.1.1 = {
	monarch = {
 		name = "Harrylnes"
		dynasty = "Ula-Homestead"
		birth_date = 1660.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1778.1.1 = {
	monarch = {
 		name = "Djel"
		dynasty = "Mea-Arriastae"
		birth_date = 1753.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1868.1.1 = {
	monarch = {
 		name = "Nanalne"
		dynasty = "Vae-Sarmofza"
		birth_date = 1833.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1941.1.1 = {
	monarch = {
 		name = "Lenalmel"
		dynasty = "Mea-Allidenius"
		birth_date = 1888.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2011.1.1 = {
	monarch = {
 		name = "Dynnyan"
		dynasty = "Mea-Anvil"
		birth_date = 1971.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

2097.1.1 = {
	monarch = {
 		name = "Elis"
		dynasty = "Ula-Isollaise"
		birth_date = 2046.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

2185.1.1 = {
	monarch = {
 		name = "Nanalne"
		dynasty = "Ula-Julidonea"
		birth_date = 2152.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

2241.1.1 = {
	monarch = {
 		name = "Onya"
		dynasty = "Ula-Lildana"
		birth_date = 2198.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

2313.1.1 = {
	monarch = {
 		name = "Nilind"
		dynasty = "Ula-Linchal"
		birth_date = 2266.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2368.1.1 = {
	monarch = {
 		name = "Celethelel"
		dynasty = "Vae-Tasaso"
		birth_date = 2336.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2445.1.1 = {
	monarch = {
 		name = "Umhuur"
		dynasty = "Ula-Plalusius"
		birth_date = 2397.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2483.1.1 = {
	monarch = {
 		name = "Guumond"
		dynasty = "Ula-Niryastare"
		birth_date = 2435.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

