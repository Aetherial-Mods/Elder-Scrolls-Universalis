government = monarchy
government_rank = 5
mercantilism = 1
technology_group = northern_tg
religion = molag_bal_cult
primary_culture = vampire
capital = 6301

54.1.1 = {
	monarch = {
 		name = "Audenian"
		dynasty = "Kassa"
		birth_date = 1.1.1
		adm = 1
		dip = 5
		mil = 2
    }
	add_ruler_personality = immortal_personality
}

139.1.1 = {
	monarch = {
 		name = "Shekestra"
		dynasty = "Bivrel"
		birth_date = 114.1.1
		adm = 0
		dip = 4
		mil = 5
		female = yes
    }
	add_ruler_personality = immortal_personality
}

228.1.1 = {
	monarch = {
 		name = "Musius"
		dynasty = "Romno"
		birth_date = 186.1.1
		adm = 5
		dip = 4
		mil = 2
    }
	add_ruler_personality = immortal_personality
	queen = {
 		name = "Justa"
		dynasty = "Romno"
		birth_date = 210.1.1
		adm = 2
		dip = 2
		mil = 1
		female = yes
    }
	add_queen_personality = immortal_personality
	heir = {
 		name = "Marcurio"
		monarch_name = "Marcurio I"
		dynasty = "Romno"
		birth_date = 225.1.1
		death_date = 294.1.1
		claim = 100
		adm = 6
		dip = 1
		mil = 0
    }
	add_heir_personality = immortal_personality
}

294.1.1 = {
	monarch = {
 		name = "Atrius"
		dynasty = "Rornam"
		birth_date = 252.1.1
		adm = 5
		dip = 4
		mil = 0
    }
	add_ruler_personality = immortal_personality
	queen = {
 		name = "Volrina"
		dynasty = "Rornam"
		birth_date = 273.1.1
		adm = 5
		dip = 1
		mil = 1
		female = yes
    }
	add_queen_personality = immortal_personality
	heir = {
 		name = "Matus"
		monarch_name = "Matus I"
		dynasty = "Rornam"
		birth_date = 282.1.1
		death_date = 367.1.1
		claim = 100
		adm = 2
		dip = 6
		mil = 0
    }
	add_heir_personality = immortal_personality
}

367.1.1 = {
	monarch = {
 		name = "Bacola"
		dynasty = "Omaanaa"
		birth_date = 343.1.1
		adm = 2
		dip = 3
		mil = 1
    }
	add_ruler_personality = immortal_personality
	queen = {
 		name = "Juna"
		dynasty = "Omaanaa"
		birth_date = 340.1.1
		adm = 5
		dip = 1
		mil = 0
		female = yes
    }
	add_queen_personality = immortal_personality
}

402.1.1 = {
	monarch = {
 		name = "Iniel"
		dynasty = "Hessam"
		birth_date = 382.1.1
		adm = 4
		dip = 0
		mil = 3
    }
	add_ruler_personality = immortal_personality
}

473.1.1 = {
	monarch = {
 		name = "Capiton"
		dynasty = "Kada"
		birth_date = 446.1.1
		adm = 2
		dip = 6
		mil = 0
    }
	add_ruler_personality = immortal_personality
	queen = {
 		name = "Lekellae"
		dynasty = "Kada"
		birth_date = 424.1.1
		adm = 6
		dip = 5
		mil = 6
		female = yes
    }
	add_queen_personality = immortal_personality
}

572.1.1 = {
	monarch = {
 		name = "Janonia"
		dynasty = "Vaakreesh"
		birth_date = 532.1.1
		adm = 4
		dip = 4
		mil = 2
		female = yes
    }
	add_ruler_personality = immortal_personality
}

649.1.1 = {
	monarch = {
 		name = "Cloelia"
		dynasty = "Norsham"
		birth_date = 615.1.1
		adm = 2
		dip = 3
		mil = 6
		female = yes
    }
	add_ruler_personality = immortal_personality
	queen = {
 		name = "Nepas"
		dynasty = "Norsham"
		birth_date = 629.1.1
		adm = 6
		dip = 2
		mil = 5
    }
	add_queen_personality = immortal_personality
	heir = {
 		name = "Canctunian"
		monarch_name = "Canctunian I"
		dynasty = "Norsham"
		birth_date = 638.1.1
		death_date = 688.1.1
		claim = 100
		adm = 3
		dip = 0
		mil = 4
    }
	add_heir_personality = immortal_personality
}

688.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Tyranus"
		monarch_name = "Tyranus I"
		dynasty = "Azaamo"
		birth_date = 683.1.1
		death_date = 701.1.1
		claim = 100
		adm = 1
		dip = 0
		mil = 1
    }
}

701.1.1 = {
	monarch = {
 		name = "Janieta"
		dynasty = "Daan"
		birth_date = 660.1.1
		adm = 4
		dip = 1
		mil = 3
		female = yes
    }
	add_ruler_personality = immortal_personality
}

778.1.1 = {
	monarch = {
 		name = "Codus"
		dynasty = "Vaarla"
		birth_date = 745.1.1
		adm = 6
		dip = 2
		mil = 5
    }
	add_ruler_personality = immortal_personality
	queen = {
 		name = "Blatta"
		dynasty = "Vaarla"
		birth_date = 738.1.1
		adm = 6
		dip = 6
		mil = 6
		female = yes
    }
	add_queen_personality = immortal_personality
	heir = {
 		name = "Naspia"
		monarch_name = "Naspia I"
		dynasty = "Vaarla"
		birth_date = 775.1.1
		death_date = 871.1.1
		claim = 100
		adm = 3
		dip = 4
		mil = 5
		female = yes
    }
	add_heir_personality = immortal_personality
}

871.1.1 = {
	monarch = {
 		name = "Corrudus"
		dynasty = "Daathan"
		birth_date = 830.1.1
		adm = 2
		dip = 1
		mil = 5
    }
	add_ruler_personality = immortal_personality
	queen = {
 		name = "Marara"
		dynasty = "Daathan"
		birth_date = 842.1.1
		adm = 6
		dip = 6
		mil = 4
		female = yes
    }
	add_queen_personality = immortal_personality
	heir = {
 		name = "Cassius"
		monarch_name = "Cassius I"
		dynasty = "Daathan"
		birth_date = 865.1.1
		death_date = 916.1.1
		claim = 100
		adm = 3
		dip = 5
		mil = 3
    }
	add_heir_personality = immortal_personality
}

916.1.1 = {
	monarch = {
 		name = "Raxle"
		dynasty = "Bivrel"
		birth_date = 894.1.1
		adm = 6
		dip = 6
		mil = 5
    }
	add_ruler_personality = immortal_personality
}

980.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Narina"
		monarch_name = "Narina I"
		dynasty = "Onasso"
		birth_date = 968.1.1
		death_date = 986.1.1
		claim = 100
		adm = 0
		dip = 3
		mil = 3
		female = yes
    }
}

986.1.1 = {
	monarch = {
 		name = "Corloius"
		dynasty = "Cedo"
		birth_date = 956.1.1
		adm = 2
		dip = 5
		mil = 5
    }
	add_ruler_personality = immortal_personality
}

1042.1.1 = {
	monarch = {
 		name = "Vinicia"
		dynasty = "Likum"
		birth_date = 1007.1.1
		adm = 1
		dip = 4
		mil = 2
		female = yes
    }
	add_ruler_personality = immortal_personality
	queen = {
 		name = "Titus"
		dynasty = "Likum"
		birth_date = 1000.1.1
		adm = 5
		dip = 2
		mil = 1
    }
	add_queen_personality = immortal_personality
	heir = {
 		name = "Javad"
		monarch_name = "Javad I"
		dynasty = "Likum"
		birth_date = 1040.1.1
		death_date = 1100.1.1
		claim = 100
		adm = 1
		dip = 1
		mil = 0
    }
	add_heir_personality = immortal_personality
}

1100.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Caula"
		monarch_name = "Caula I"
		dynasty = "Kylua"
		birth_date = 1097.1.1
		death_date = 1115.1.1
		claim = 100
		adm = 0
		dip = 0
		mil = 4
		female = yes
    }
}

1115.1.1 = {
	monarch = {
 		name = "Pritia"
		dynasty = "Maahla"
		birth_date = 1092.1.1
		adm = 6
		dip = 3
		mil = 4
		female = yes
    }
	add_ruler_personality = immortal_personality
	queen = {
 		name = "Brucus"
		dynasty = "Maahla"
		birth_date = 1087.1.1
		adm = 3
		dip = 2
		mil = 3
    }
	add_queen_personality = immortal_personality
}

1200.1.1 = {
	monarch = {
 		name = "Andreas"
		dynasty = "Myn"
		birth_date = 1177.1.1
		adm = 1
		dip = 1
		mil = 0
    }
	add_ruler_personality = immortal_personality
	queen = {
 		name = "Indara"
		dynasty = "Myn"
		birth_date = 1172.1.1
		adm = 5
		dip = 0
		mil = 6
		female = yes
    }
	add_queen_personality = immortal_personality
}

1282.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Martinus"
		monarch_name = "Martinus I"
		dynasty = "Darshaa"
		birth_date = 1276.1.1
		death_date = 1294.1.1
		claim = 100
		adm = 2
		dip = 2
		mil = 6
    }
}

1294.1.1 = {
	monarch = {
 		name = "Augusta"
		dynasty = "Alerno"
		birth_date = 1274.1.1
		adm = 2
		dip = 5
		mil = 6
		female = yes
    }
	add_ruler_personality = immortal_personality
	queen = {
 		name = "Linus"
		dynasty = "Alerno"
		birth_date = 1258.1.1
		adm = 5
		dip = 4
		mil = 5
    }
	add_queen_personality = immortal_personality
	heir = {
 		name = "Ancurio"
		monarch_name = "Ancurio I"
		dynasty = "Alerno"
		birth_date = 1279.1.1
		death_date = 1344.1.1
		claim = 100
		adm = 2
		dip = 2
		mil = 4
    }
	add_heir_personality = immortal_personality
}

1344.1.1 = {
	monarch = {
 		name = "Nemesianus"
		dynasty = "Sessaa"
		birth_date = 1317.1.1
		adm = 5
		dip = 4
		mil = 6
    }
	add_ruler_personality = immortal_personality
}

1381.1.1 = {
	monarch = {
 		name = "Fralvia"
		dynasty = "Lorsha"
		birth_date = 1354.1.1
		adm = 3
		dip = 3
		mil = 3
		female = yes
    }
	add_ruler_personality = immortal_personality
	queen = {
 		name = "Decentius"
		dynasty = "Lorsha"
		birth_date = 1352.1.1
		adm = 0
		dip = 1
		mil = 2
    }
	add_queen_personality = immortal_personality
}

1442.1.1 = {
	monarch = {
 		name = "Otius"
		dynasty = "Laaka"
		birth_date = 1393.1.1
		adm = 5
		dip = 0
		mil = 5
    }
	add_ruler_personality = immortal_personality
}

1537.1.1 = {
	monarch = {
 		name = "Ilav"
		dynasty = "Nyvrus"
		birth_date = 1509.1.1
		adm = 4
		dip = 0
		mil = 2
    }
	add_ruler_personality = immortal_personality
	queen = {
 		name = "Fammana"
		dynasty = "Nyvrus"
		birth_date = 1496.1.1
		adm = 1
		dip = 5
		mil = 1
		female = yes
    }
	add_queen_personality = immortal_personality
	heir = {
 		name = "Terek"
		monarch_name = "Terek I"
		dynasty = "Nyvrus"
		birth_date = 1533.1.1
		death_date = 1572.1.1
		claim = 100
		adm = 4
		dip = 4
		mil = 0
    }
	add_heir_personality = immortal_personality
}

1572.1.1 = {
	monarch = {
 		name = "Tullius"
		dynasty = "Botaa"
		birth_date = 1534.1.1
		adm = 4
		dip = 0
		mil = 0
    }
	add_ruler_personality = immortal_personality
	queen = {
 		name = "Sandana"
		dynasty = "Botaa"
		birth_date = 1552.1.1
		adm = 1
		dip = 6
		mil = 6
		female = yes
    }
	add_queen_personality = immortal_personality
	heir = {
 		name = "Glenroy"
		monarch_name = "Glenroy I"
		dynasty = "Botaa"
		birth_date = 1567.1.1
		death_date = 1623.1.1
		claim = 100
		adm = 1
		dip = 2
		mil = 0
    }
	add_heir_personality = immortal_personality
}

1623.1.1 = {
	monarch = {
 		name = "Valton"
		dynasty = "Botaa"
		birth_date = 1574.1.1
		adm = 0
		dip = 6
		mil = 1
    }
	add_ruler_personality = immortal_personality
	queen = {
 		name = "Famia"
		dynasty = "Botaa"
		birth_date = 1594.1.1
		adm = 4
		dip = 4
		mil = 6
		female = yes
    }
	add_queen_personality = immortal_personality
	heir = {
 		name = "Teldius"
		monarch_name = "Teldius I"
		dynasty = "Botaa"
		birth_date = 1622.1.1
		death_date = 1658.1.1
		claim = 100
		adm = 1
		dip = 3
		mil = 5
    }
	add_heir_personality = immortal_personality
}

1658.1.1 = {
	monarch = {
 		name = "Hostia"
		dynasty = "Cedo"
		birth_date = 1615.1.1
		adm = 4
		dip = 4
		mil = 1
		female = yes
    }
	add_ruler_personality = immortal_personality
}

1703.1.1 = {
	monarch = {
 		name = "Carolus"
		dynasty = "Hessam"
		birth_date = 1680.1.1
		adm = 2
		dip = 3
		mil = 4
    }
	add_ruler_personality = immortal_personality
}

1800.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Bassianus"
		monarch_name = "Bassianus I"
		dynasty = "Arado"
		birth_date = 1798.1.1
		death_date = 1816.1.1
		claim = 100
		adm = 3
		dip = 0
		mil = 2
    }
}

1816.1.1 = {
	monarch = {
 		name = "Pelagius"
		dynasty = "Zaala"
		birth_date = 1794.1.1
		adm = 2
		dip = 4
		mil = 3
    }
	add_ruler_personality = immortal_personality
}

1866.1.1 = {
	monarch = {
 		name = "Iocundus"
		dynasty = "Naaneen"
		birth_date = 1843.1.1
		adm = 0
		dip = 3
		mil = 6
    }
	add_ruler_personality = immortal_personality
}

1942.1.1 = {
	monarch = {
 		name = "Oriandra"
		dynasty = "Naaneen"
		birth_date = 1906.1.1
		adm = 6
		dip = 2
		mil = 3
		female = yes
    }
	add_ruler_personality = immortal_personality
	queen = {
 		name = "Artorius"
		dynasty = "Naaneen"
		birth_date = 1901.1.1
		adm = 3
		dip = 1
		mil = 2
    }
	add_queen_personality = immortal_personality
	heir = {
 		name = "Marina"
		monarch_name = "Marina I"
		dynasty = "Naaneen"
		birth_date = 1931.1.1
		death_date = 1984.1.1
		claim = 100
		adm = 6
		dip = 6
		mil = 1
		female = yes
    }
	add_heir_personality = immortal_personality
}

1984.1.1 = {
	monarch = {
 		name = "Celsus"
		dynasty = "Naaneen"
		birth_date = 1932.1.1
		adm = 2
		dip = 0
		mil = 3
    }
	add_ruler_personality = immortal_personality
}

2042.1.1 = {
	monarch = {
 		name = "Varinia"
		dynasty = "Maahso"
		birth_date = 2018.1.1
		adm = 0
		dip = 0
		mil = 0
		female = yes
    }
	add_ruler_personality = immortal_personality
	queen = {
 		name = "Florentius"
		dynasty = "Maahso"
		birth_date = 2003.1.1
		adm = 4
		dip = 5
		mil = 6
    }
	add_queen_personality = immortal_personality
	heir = {
 		name = "Thalanius"
		monarch_name = "Thalanius I"
		dynasty = "Maahso"
		birth_date = 2036.1.1
		death_date = 2118.1.1
		claim = 100
		adm = 1
		dip = 4
		mil = 5
    }
	add_heir_personality = immortal_personality
}

2118.1.1 = {
	monarch = {
 		name = "Idonea"
		dynasty = "Anelash"
		birth_date = 2080.1.1
		adm = 4
		dip = 5
		mil = 0
		female = yes
    }
	add_ruler_personality = immortal_personality
	queen = {
 		name = "Galtus"
		dynasty = "Anelash"
		birth_date = 2066.1.1
		adm = 1
		dip = 4
		mil = 6
    }
	add_queen_personality = immortal_personality
	heir = {
 		name = "Tullius"
		monarch_name = "Tullius I"
		dynasty = "Anelash"
		birth_date = 2110.1.1
		death_date = 2177.1.1
		claim = 100
		adm = 5
		dip = 2
		mil = 5
    }
	add_heir_personality = immortal_personality
}

2177.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Otho"
		monarch_name = "Otho I"
		dynasty = "Ketto"
		birth_date = 2164.1.1
		death_date = 2182.1.1
		claim = 100
		adm = 3
		dip = 1
		mil = 2
    }
}

2182.1.1 = {
	monarch = {
 		name = "Ciirta"
		dynasty = "Misel"
		birth_date = 2135.1.1
		adm = 2
		dip = 5
		mil = 2
		female = yes
    }
	add_ruler_personality = immortal_personality
}

2268.1.1 = {
	monarch = {
 		name = "Vianis"
		dynasty = "Darshaa"
		birth_date = 2244.1.1
		adm = 0
		dip = 4
		mil = 6
		female = yes
    }
	add_ruler_personality = immortal_personality
}

2351.1.1 = {
	monarch = {
 		name = "Patia"
		dynasty = "Menaa"
		birth_date = 2303.1.1
		adm = 6
		dip = 3
		mil = 2
		female = yes
    }
	add_ruler_personality = immortal_personality
}

2444.1.1 = {
	monarch = {
 		name = "John"
		dynasty = "Bivrel"
		birth_date = 2424.1.1
		adm = 4
		dip = 2
		mil = 6
    }
	add_ruler_personality = immortal_personality
	queen = {
 		name = "Shekestra"
		dynasty = "Bivrel"
		birth_date = 2410.1.1
		adm = 1
		dip = 1
		mil = 5
		female = yes
    }
	add_queen_personality = immortal_personality
	heir = {
 		name = "Hestra"
		monarch_name = "Hestra I"
		dynasty = "Bivrel"
		birth_date = 2431.1.1
		death_date = 2500.1.1
		claim = 100
		adm = 5
		dip = 6
		mil = 4
		female = yes
    }
	add_heir_personality = immortal_personality
}

