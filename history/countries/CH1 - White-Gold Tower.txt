government = monarchy
government_rank = 5
mercantilism = 1
technology_group = cyrodiil_tg
religion = meridia_cult
primary_culture = ayleid
capital = 1206
add_government_reform = cyrodiilic_empire_reform

54.1.1 = {
	monarch = {
 		name = "Umaril"
		dynasty = "Unfeathered"
		birth_date = 18.1.1
		adm = 3
		dip = 2
		mil = 7
    }
	add_ruler_personality = immortal_personality
	add_ruler_personality = sorcerer_personality
	add_ruler_personality = cruel_personality
	add_ruler_personality = legendary_conqueror_personality
	add_ruler_personality = tactical_genius_personality
}

190.7.1 = {
	monarch = {
 		name = "Eradhiho"
		dynasty = "Mea-Gince"
		birth_date = 116.1.1
		adm = 4
		dip = 1
		mil = 0
		female = yes
    }
	queen = {
 		name = "Hasind"
		dynasty = "Mea-Gince"
		birth_date = 135.1.1
		adm = 0
		dip = 2
		mil = 3
    }
	heir = {
 		name = "Yrraldylith"
		monarch_name = "Yrraldylith I"
		dynasty = "Mea-Gince"
		birth_date = 170.1.1
		death_date = 323.1.1
		claim = 100
		adm = 4
		dip = 1
		mil = 2
		female = yes
    }
}

323.1.1 = {
	monarch = {
 		name = "Lunrun"
		dynasty = "Ula-Lildana"
		birth_date = 292.1.1
		adm = 3
		dip = 4
		mil = 2
    }
	queen = {
 		name = "Synarlysush"
		dynasty = "Ula-Lildana"
		birth_date = 299.1.1
		adm = 4
		dip = 1
		mil = 3
		female = yes
    }
	heir = {
 		name = "Pemu"
		monarch_name = "Pemu I"
		dynasty = "Ula-Lildana"
		birth_date = 322.1.1
		death_date = 388.1.1
		claim = 100
		adm = 1
		dip = 6
		mil = 2
		female = yes
    }
}

388.1.1 = {
	monarch = {
 		name = "Mimuurgusen"
		dynasty = "Vae-Vandiand"
		birth_date = 351.1.1
		adm = 0
		dip = 3
		mil = 2
    }
	queen = {
 		name = "Puvusadya"
		dynasty = "Vae-Vandiand"
		birth_date = 363.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

463.1.1 = {
	monarch = {
 		name = "Filestis"
		dynasty = "Ula-Lildana"
		birth_date = 421.1.1
		adm = 2
		dip = 0
		mil = 5
    }
}

524.1.1 = {
	monarch = {
 		name = "Leydel"
		dynasty = "Mea-Bromma"
		birth_date = 503.1.1
		adm = 0
		dip = 0
		mil = 2
    }
}

576.1.1 = {
	monarch = {
 		name = "Genuuladom"
		dynasty = "Vae-Vietia"
		birth_date = 554.1.1
		adm = 3
		dip = 1
		mil = 4
    }
	queen = {
 		name = "Erraduure"
		dynasty = "Vae-Vietia"
		birth_date = 543.1.1
		adm = 2
		dip = 4
		mil = 4
		female = yes
    }
	heir = {
 		name = "Himmon"
		monarch_name = "Himmon I"
		dynasty = "Vae-Vietia"
		birth_date = 571.1.1
		death_date = 642.1.1
		claim = 100
		adm = 4
		dip = 5
		mil = 2
		female = yes
    }
}

642.1.1 = {
	monarch = {
 		name = "Fidhi"
		dynasty = "Mea-Bromma"
		birth_date = 597.1.1
		adm = 2
		dip = 4
		mil = 6
    }
	queen = {
 		name = "Yhu"
		dynasty = "Mea-Bromma"
		birth_date = 597.1.1
		adm = 6
		dip = 3
		mil = 5
		female = yes
    }
	heir = {
 		name = "Liva"
		monarch_name = "Liva I"
		dynasty = "Mea-Bromma"
		birth_date = 632.1.1
		death_date = 683.1.1
		claim = 100
		adm = 0
		dip = 3
		mil = 2
		female = yes
    }
}

683.1.1 = {
	monarch = {
 		name = "Wuria"
		dynasty = "Mea-Andrulusus"
		birth_date = 652.1.1
		adm = 0
		dip = 0
		mil = 2
		female = yes
    }
	queen = {
 		name = "Uluscant"
		dynasty = "Mea-Andrulusus"
		birth_date = 663.1.1
		adm = 4
		dip = 5
		mil = 1
    }
	heir = {
 		name = "Himmon"
		monarch_name = "Himmon I"
		dynasty = "Mea-Andrulusus"
		birth_date = 679.1.1
		death_date = 744.1.1
		claim = 100
		adm = 0
		dip = 4
		mil = 0
		female = yes
    }
}

744.1.1 = {
	monarch = {
 		name = "Leru"
		dynasty = "Vae-Sialerius"
		birth_date = 719.1.1
		adm = 3
		dip = 5
		mil = 3
		female = yes
    }
}

824.1.1 = {
	monarch = {
 		name = "Laloriaran"
		dynasty = "Ula-Isollaise"
		birth_date = 804.1.1
		adm = 1
		dip = 5
		mil = 6
    }
	queen = {
 		name = "Eradhiho"
		dynasty = "Ula-Isollaise"
		birth_date = 784.1.1
		adm = 5
		dip = 3
		mil = 5
		female = yes
    }
	heir = {
 		name = "Teymeyladis"
		monarch_name = "Teymeyladis I"
		dynasty = "Ula-Isollaise"
		birth_date = 817.1.1
		death_date = 908.1.1
		claim = 100
		adm = 5
		dip = 6
		mil = 6
    }
}

908.1.1 = {
	monarch = {
 		name = "Inhuagadom"
		dynasty = "Mea-Allevar"
		birth_date = 864.1.1
		adm = 0
		dip = 1
		mil = 1
    }
}

970.1.1 = {
	monarch = {
 		name = "Vurel"
		dynasty = "Ula-Renne"
		birth_date = 932.1.1
		adm = 6
		dip = 0
		mil = 5
		female = yes
    }
}

1041.1.1 = {
	monarch = {
 		name = "Cam"
		dynasty = "Ula-Lildana"
		birth_date = 1022.1.1
		adm = 4
		dip = 6
		mil = 1
    }
}

1140.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Hasind"
		monarch_name = "Hasind I"
		dynasty = "Mea-Beldaburo"
		birth_date = 1136.1.1
		death_date = 1154.1.1
		claim = 100
		adm = 5
		dip = 3
		mil = 6
    }
}

1154.1.1 = {
	monarch = {
 		name = "Inhuagadom"
		dynasty = "Ula-Homestead"
		birth_date = 1120.1.1
		adm = 4
		dip = 0
		mil = 6
    }
}

1245.1.1 = {
	monarch = {
 		name = "Nilichi"
		dynasty = "Vae-Vanua"
		birth_date = 1192.1.1
		adm = 2
		dip = 6
		mil = 3
    }
}

1289.1.1 = {
	monarch = {
 		name = "Djador"
		dynasty = "Vae-Vesagrius"
		birth_date = 1240.1.1
		adm = 0
		dip = 5
		mil = 0
    }
}

1357.1.1 = {
	monarch = {
 		name = "Nyrrin"
		dynasty = "Ula-Julidonea"
		birth_date = 1331.1.1
		adm = 6
		dip = 4
		mil = 3
		female = yes
    }
}

1434.1.1 = {
	monarch = {
 		name = "Cusdar"
		dynasty = "Vae-Varondo"
		birth_date = 1411.1.1
		adm = 4
		dip = 3
		mil = 0
    }
	queen = {
 		name = "Mezol"
		dynasty = "Vae-Varondo"
		birth_date = 1397.1.1
		adm = 1
		dip = 2
		mil = 6
		female = yes
    }
	heir = {
 		name = "Cordu"
		monarch_name = "Cordu I"
		dynasty = "Vae-Varondo"
		birth_date = 1428.1.1
		death_date = 1481.1.1
		claim = 100
		adm = 5
		dip = 1
		mil = 5
    }
}

1481.1.1 = {
	monarch = {
 		name = "Rorhemeras"
		dynasty = "Mea-Aderina"
		birth_date = 1459.1.1
		adm = 0
		dip = 2
		mil = 0
    }
}

1565.1.1 = {
	monarch = {
 		name = "Murhyldiment"
		dynasty = "Ula-Ryndenyse"
		birth_date = 1527.1.1
		adm = 2
		dip = 3
		mil = 2
    }
	queen = {
 		name = "Pehyshen"
		dynasty = "Ula-Ryndenyse"
		birth_date = 1546.1.1
		adm = 3
		dip = 0
		mil = 3
		female = yes
    }
	heir = {
 		name = "Illarrymo"
		monarch_name = "Illarrymo I"
		dynasty = "Ula-Ryndenyse"
		birth_date = 1552.1.1
		death_date = 1621.1.1
		claim = 100
		adm = 6
		dip = 5
		mil = 2
		female = yes
    }
}

1621.1.1 = {
	monarch = {
 		name = "Gemhel"
		dynasty = "Ula-Horuria"
		birth_date = 1600.1.1
		adm = 6
		dip = 2
		mil = 2
    }
	queen = {
 		name = "Urenenya"
		dynasty = "Ula-Horuria"
		birth_date = 1578.1.1
		adm = 3
		dip = 0
		mil = 1
		female = yes
    }
}

1660.1.1 = {
	monarch = {
 		name = "Poman"
		dynasty = "Vae-Weatherleah"
		birth_date = 1615.1.1
		adm = 0
		dip = 2
		mil = 1
		female = yes
    }
}

1739.1.1 = {
	monarch = {
 		name = "Teymeyladis"
		dynasty = "Ula-Jorarienna"
		birth_date = 1702.1.1
		adm = 2
		dip = 3
		mil = 3
    }
}

1791.1.1 = {
	monarch = {
 		name = "Vossyduron"
		dynasty = "Vae-Varondo"
		birth_date = 1744.1.1
		adm = 0
		dip = 3
		mil = 6
    }
}

1869.1.1 = {
	monarch = {
 		name = "Orhualmuunint"
		dynasty = "Vae-Sylolvia"
		birth_date = 1842.1.1
		adm = 5
		dip = 2
		mil = 3
    }
	queen = {
 		name = "Hunne"
		dynasty = "Vae-Sylolvia"
		birth_date = 1827.1.1
		adm = 1
		dip = 3
		mil = 5
		female = yes
    }
	heir = {
 		name = "Inhuagadom"
		monarch_name = "Inhuagadom I"
		dynasty = "Vae-Sylolvia"
		birth_date = 1867.1.1
		death_date = 1958.1.1
		claim = 100
		adm = 5
		dip = 2
		mil = 4
    }
}

1958.1.1 = {
	monarch = {
 		name = "Muanhilan"
		dynasty = "Mea-Dusok"
		birth_date = 1927.1.1
		adm = 4
		dip = 5
		mil = 5
    }
	queen = {
 		name = "Poman"
		dynasty = "Mea-Dusok"
		birth_date = 1925.1.1
		adm = 1
		dip = 4
		mil = 4
		female = yes
    }
}

2013.1.1 = {
	monarch = {
 		name = "Guumond"
		dynasty = "Vae-Weyandawik"
		birth_date = 1960.1.1
		adm = 6
		dip = 3
		mil = 0
    }
}

2075.1.1 = {
	monarch = {
 		name = "Dedhan"
		dynasty = "Ula-Roebeck"
		birth_date = 2028.1.1
		adm = 5
		dip = 2
		mil = 4
    }
}

2140.1.1 = {
	monarch = {
 		name = "Nimender"
		dynasty = "Vae-Sena"
		birth_date = 2104.1.1
		adm = 3
		dip = 1
		mil = 1
    }
}

2220.1.1 = {
	monarch = {
 		name = "Endarre"
		dynasty = "Vae-Silorn"
		birth_date = 2197.1.1
		adm = 1
		dip = 0
		mil = 4
    }
	queen = {
 		name = "Immol"
		dynasty = "Vae-Silorn"
		birth_date = 2174.1.1
		adm = 5
		dip = 6
		mil = 3
		female = yes
    }
	heir = {
 		name = "Cur"
		monarch_name = "Cur I"
		dynasty = "Vae-Silorn"
		birth_date = 2216.1.1
		death_date = 2260.1.1
		claim = 100
		adm = 2
		dip = 5
		mil = 2
    }
}

2260.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Djel"
		monarch_name = "Djel I"
		dynasty = "Vae-Vonara"
		birth_date = 2260.1.1
		death_date = 2278.1.1
		claim = 100
		adm = 0
		dip = 4
		mil = 6
    }
}

2278.1.1 = {
	monarch = {
 		name = "Nimender"
		dynasty = "Ula-Rusifus"
		birth_date = 2240.1.1
		adm = 3
		dip = 5
		mil = 1
    }
	queen = {
 		name = "Cele"
		dynasty = "Ula-Rusifus"
		birth_date = 2229.1.1
		adm = 0
		dip = 4
		mil = 0
		female = yes
    }
	heir = {
 		name = "Onya"
		monarch_name = "Onya I"
		dynasty = "Ula-Rusifus"
		birth_date = 2270.1.1
		death_date = 2338.1.1
		claim = 100
		adm = 4
		dip = 2
		mil = 6
		female = yes
    }
}

2338.1.1 = {
	monarch = {
 		name = "Cosudam"
		dynasty = "Ula-Niryastare"
		birth_date = 2288.1.1
		adm = 2
		dip = 1
		mil = 3
    }
}

2428.1.1 = {
	monarch = {
 		name = "Gand"
		dynasty = "Ula-Ryndenyse"
		birth_date = 2387.1.1
		adm = 0
		dip = 1
		mil = 6
    }
}

2477.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Mennith"
		monarch_name = "Mennith I"
		dynasty = "Mea-Calabolis"
		birth_date = 2468.1.1
		death_date = 2486.1.1
		claim = 100
		adm = 1
		dip = 5
		mil = 4
		female = yes
    }
}

2486.1.1 = {
	monarch = {
 		name = "Cordu"
		dynasty = "Ula-Ryndenyse"
		birth_date = 2455.1.1
		adm = 0
		dip = 1
		mil = 5
    }
	queen = {
 		name = "Irreshyva"
		dynasty = "Ula-Ryndenyse"
		birth_date = 2438.1.1
		adm = 4
		dip = 0
		mil = 4
		female = yes
    }
}

