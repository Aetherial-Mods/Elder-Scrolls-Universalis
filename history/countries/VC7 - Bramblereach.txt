government = monarchy
government_rank = 1
mercantilism = 1
technology_group = elven_tg
religion = bosmer_pantheon
primary_culture = bosmer
capital = 2377
secondary_religion = hircine_cult

54.1.1 = {
	monarch = {
 		name = "Sarralthir"
		dynasty = "Camoran"
		birth_date = 6.1.1
		adm = 0
		dip = 0
		mil = 0
		female = yes
    }
}

151.1.1 = {
	monarch = {
 		name = "Lagrethor"
		dynasty = "Camoran"
		birth_date = 133.1.1
		adm = 5
		dip = 4
		mil = 2
    }
	queen = {
 		name = "Alandis"
		dynasty = "Or'Thaeicia"
		birth_date = 101.1.1
		adm = 2
		dip = 2
		mil = 1
		female = yes
    }
	heir = {
 		name = "Hartmin"
		monarch_name = "Hartmin I"
		dynasty = "Camoran"
		birth_date = 138.1.1
		death_date = 197.1.1
		claim = 100
		adm = 6
		dip = 1
		mil = 0
    }
}

197.1.1 = {
	monarch = {
 		name = "Celorien"
		dynasty = "Camoran"
		birth_date = 166.1.1
		adm = 2
		dip = 2
		mil = 3
		female = yes
    }
	queen = {
 		name = "Gwaerinbor"
		dynasty = "Ca'Green"
		birth_date = 145.1.1
		adm = 6
		dip = 1
		mil = 2
    }
}

261.1.1 = {
	monarch = {
 		name = "Garamiel"
		dynasty = "Camoran"
		birth_date = 211.1.1
		adm = 4
		dip = 0
		mil = 5
		female = yes
    }
}

345.1.1 = {
	monarch = {
 		name = "Dirchanor"
		dynasty = "Camoran"
		birth_date = 306.1.1
		adm = 2
		dip = 6
		mil = 2
    }
}

393.1.1 = {
	monarch = {
 		name = "Tholdaegal"
		dynasty = "Camoran"
		birth_date = 341.1.1
		adm = 0
		dip = 6
		mil = 5
    }
	queen = {
 		name = "Ralerdhil"
		dynasty = "Or'Telarwen"
		birth_date = 342.1.1
		adm = 4
		dip = 4
		mil = 4
		female = yes
    }
	heir = {
 		name = "Firolmoth"
		monarch_name = "Firolmoth I"
		dynasty = "Camoran"
		birth_date = 388.1.1
		death_date = 438.1.1
		claim = 100
		adm = 1
		dip = 3
		mil = 3
    }
}

438.1.1 = {
	monarch = {
 		name = "Gaelthagar"
		dynasty = "Camoran"
		birth_date = 412.1.1
		adm = 1
		dip = 6
		mil = 4
    }
}

519.1.1 = {
	monarch = {
 		name = "Mindirin"
		dynasty = "Camoran"
		birth_date = 488.1.1
		adm = 6
		dip = 5
		mil = 0
		female = yes
    }
}

574.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Lorwen"
		monarch_name = "Lorwen I"
		dynasty = "Camoran"
		birth_date = 570.1.1
		death_date = 588.1.1
		claim = 100
		adm = 0
		dip = 2
		mil = 5
		female = yes
    }
}

588.1.1 = {
	monarch = {
 		name = "Dorodir"
		dynasty = "Camoran"
		birth_date = 545.1.1
		adm = 2
		dip = 4
		mil = 1
    }
	queen = {
 		name = "Kaale"
		dynasty = "Ur'Eagle"
		birth_date = 558.1.1
		adm = 6
		dip = 2
		mil = 0
		female = yes
    }
	heir = {
 		name = "Cirithor"
		monarch_name = "Cirithor I"
		dynasty = "Camoran"
		birth_date = 588.1.1
		death_date = 637.1.1
		claim = 100
		adm = 3
		dip = 1
		mil = 6
    }
}

637.1.1 = {
	monarch = {
 		name = "Manilbor"
		dynasty = "Camoran"
		birth_date = 603.1.1
		adm = 1
		dip = 0
		mil = 2
    }
	queen = {
 		name = "Gathraenor"
		dynasty = "Ur'Eagle"
		birth_date = 614.1.1
		adm = 5
		dip = 5
		mil = 1
		female = yes
    }
	heir = {
 		name = "Adagwen"
		monarch_name = "Adagwen I"
		dynasty = "Camoran"
		birth_date = 637.1.1
		death_date = 735.1.1
		claim = 100
		adm = 2
		dip = 4
		mil = 0
		female = yes
    }
}

735.1.1 = {
	monarch = {
 		name = "Huunel"
		dynasty = "Camoran"
		birth_date = 699.1.1
		adm = 5
		dip = 5
		mil = 2
		female = yes
    }
	queen = {
 		name = "Tholralem"
		dynasty = "Or'Naetedir"
		birth_date = 698.1.1
		adm = 2
		dip = 4
		mil = 1
    }
}

787.1.1 = {
	monarch = {
 		name = "Parenonas"
		dynasty = "Camoran"
		birth_date = 766.1.1
		adm = 0
		dip = 3
		mil = 5
    }
}

865.1.1 = {
	monarch = {
 		name = "Indaryn"
		dynasty = "Camoran"
		birth_date = 814.1.1
		adm = 5
		dip = 2
		mil = 2
    }
	queen = {
 		name = "Guilenarth"
		dynasty = "Ca'Marlandra"
		birth_date = 826.1.1
		adm = 2
		dip = 1
		mil = 1
		female = yes
    }
}

900.1.1 = {
	monarch = {
 		name = "Thael"
		dynasty = "Camoran"
		birth_date = 870.1.1
		adm = 0
		dip = 0
		mil = 4
		female = yes
    }
	queen = {
 		name = "Obentor"
		dynasty = "Ca'Minweneth"
		birth_date = 880.1.1
		adm = 4
		dip = 6
		mil = 3
    }
	heir = {
 		name = "Orthonor"
		monarch_name = "Orthonor I"
		dynasty = "Camoran"
		birth_date = 885.1.1
		death_date = 989.1.1
		claim = 100
		adm = 1
		dip = 4
		mil = 2
    }
}

989.1.1 = {
	monarch = {
 		name = "Galereth"
		dynasty = "Camoran"
		birth_date = 949.1.1
		adm = 0
		dip = 0
		mil = 3
		female = yes
    }
}

1037.1.1 = {
	monarch = {
 		name = "Dalodir"
		dynasty = "Camoran"
		birth_date = 991.1.1
		adm = 6
		dip = 0
		mil = 6
    }
	queen = {
 		name = "Athemel"
		dynasty = "Or'Stonesquare"
		birth_date = 1008.1.1
		adm = 3
		dip = 5
		mil = 5
		female = yes
    }
}

1127.1.1 = {
	monarch = {
 		name = "Gelin"
		dynasty = "Camoran"
		birth_date = 1102.1.1
		adm = 1
		dip = 4
		mil = 2
    }
}

1174.1.1 = {
	monarch = {
 		name = "Namradis"
		dynasty = "Camoran"
		birth_date = 1140.1.1
		adm = 6
		dip = 4
		mil = 5
		female = yes
    }
}

1253.1.1 = {
	monarch = {
 		name = "Glaugluin"
		dynasty = "Camoran"
		birth_date = 1218.1.1
		adm = 4
		dip = 3
		mil = 2
    }
	queen = {
 		name = "Ronarith"
		dynasty = "Ca'Manthia"
		birth_date = 1226.1.1
		adm = 1
		dip = 1
		mil = 1
		female = yes
    }
	heir = {
 		name = "Ganrenan"
		monarch_name = "Ganrenan I"
		dynasty = "Camoran"
		birth_date = 1246.1.1
		death_date = 1339.1.1
		claim = 100
		adm = 5
		dip = 0
		mil = 0
		female = yes
    }
}

1339.1.1 = {
	monarch = {
 		name = "Whisper"
		dynasty = "Camoran"
		birth_date = 1305.1.1
		adm = 1
		dip = 1
		mil = 2
		female = yes
    }
	queen = {
 		name = "Milgangor"
		dynasty = "Ur'Archen"
		birth_date = 1304.1.1
		adm = 0
		dip = 5
		mil = 3
    }
	heir = {
 		name = "Eralin"
		monarch_name = "Eralin I"
		dynasty = "Camoran"
		birth_date = 1327.1.1
		death_date = 1411.1.1
		claim = 100
		adm = 4
		dip = 3
		mil = 2
		female = yes
    }
}

1411.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Amlion"
		monarch_name = "Amlion I"
		dynasty = "Camoran"
		birth_date = 1399.1.1
		death_date = 1417.1.1
		claim = 100
		adm = 2
		dip = 2
		mil = 5
    }
}

1417.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Negaelion"
		monarch_name = "Negaelion I"
		dynasty = "Camoran"
		birth_date = 1412.1.1
		death_date = 1430.1.1
		claim = 100
		adm = 1
		dip = 2
		mil = 2
    }
}

1430.1.1 = {
	monarch = {
 		name = "Faerelos"
		dynasty = "Camoran"
		birth_date = 1406.1.1
		adm = 3
		dip = 3
		mil = 4
    }
	queen = {
 		name = "Elriniel"
		dynasty = "Camoran"
		birth_date = 1396.1.1
		adm = 0
		dip = 1
		mil = 3
		female = yes
    }
	heir = {
 		name = "Enruvie"
		monarch_name = "Enruvie I"
		dynasty = "Camoran"
		birth_date = 1427.1.1
		death_date = 1491.1.1
		claim = 100
		adm = 4
		dip = 0
		mil = 2
		female = yes
    }
}

1491.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Hadran"
		monarch_name = "Hadran I"
		dynasty = "Camoran"
		birth_date = 1476.1.1
		death_date = 1494.1.1
		claim = 100
		adm = 2
		dip = 6
		mil = 6
    }
}

1494.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Essilion"
		monarch_name = "Essilion I"
		dynasty = "Camoran"
		birth_date = 1480.1.1
		death_date = 1498.1.1
		claim = 100
		adm = 1
		dip = 5
		mil = 3
		female = yes
    }
}

1498.1.1 = {
	monarch = {
 		name = "Orthonor"
		dynasty = "Camoran"
		birth_date = 1457.1.1
		adm = 0
		dip = 2
		mil = 3
    }
	queen = {
 		name = "Elphinia"
		dynasty = "Or'Thaeicia"
		birth_date = 1463.1.1
		adm = 4
		dip = 0
		mil = 2
		female = yes
    }
	heir = {
 		name = "Nivriian"
		monarch_name = "Nivriian I"
		dynasty = "Camoran"
		birth_date = 1496.1.1
		death_date = 1570.1.1
		claim = 100
		adm = 1
		dip = 6
		mil = 1
		female = yes
    }
}

1570.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Heragaeth"
		monarch_name = "Heragaeth I"
		dynasty = "Camoran"
		birth_date = 1566.1.1
		death_date = 1584.1.1
		claim = 100
		adm = 6
		dip = 5
		mil = 4
		female = yes
    }
}

1584.1.1 = {
	monarch = {
 		name = "Brenor"
		dynasty = "Camoran"
		birth_date = 1535.1.1
		adm = 2
		dip = 6
		mil = 0
    }
	queen = {
 		name = "Girninan"
		dynasty = "Ur'Areanragil"
		birth_date = 1562.1.1
		adm = 6
		dip = 5
		mil = 6
		female = yes
    }
}

1665.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Icarian"
		monarch_name = "Icarian I"
		dynasty = "Camoran"
		birth_date = 1650.1.1
		death_date = 1668.1.1
		claim = 100
		adm = 6
		dip = 2
		mil = 4
    }
}

1668.1.1 = {
	monarch = {
 		name = "Cunodir"
		dynasty = "Camoran"
		birth_date = 1642.1.1
		adm = 2
		dip = 3
		mil = 6
    }
	queen = {
 		name = "Arithiel"
		dynasty = "Ca'Longhaven"
		birth_date = 1627.1.1
		adm = 6
		dip = 2
		mil = 5
		female = yes
    }
}

1763.1.1 = {
	monarch = {
 		name = "Gazunil"
		dynasty = "Camoran"
		birth_date = 1713.1.1
		adm = 4
		dip = 1
		mil = 2
    }
	queen = {
 		name = "Finoriell"
		dynasty = "Ca'Filon"
		birth_date = 1730.1.1
		adm = 1
		dip = 0
		mil = 1
		female = yes
    }
	heir = {
 		name = "Firthaedal"
		monarch_name = "Firthaedal I"
		dynasty = "Camoran"
		birth_date = 1751.1.1
		death_date = 1801.1.1
		claim = 100
		adm = 5
		dip = 5
		mil = 0
    }
}

1801.1.1 = {
	monarch = {
 		name = "Thrandion"
		dynasty = "Camoran"
		birth_date = 1775.1.1
		adm = 4
		dip = 2
		mil = 0
    }
	queen = {
 		name = "Rithros"
		dynasty = "Or'Parndra"
		birth_date = 1776.1.1
		adm = 5
		dip = 5
		mil = 1
		female = yes
    }
	heir = {
 		name = "Fradion"
		monarch_name = "Fradion I"
		dynasty = "Camoran"
		birth_date = 1788.1.1
		death_date = 1852.1.1
		claim = 100
		adm = 1
		dip = 4
		mil = 0
    }
}

1852.1.1 = {
	monarch = {
 		name = "Wendirre"
		dynasty = "Camoran"
		birth_date = 1830.1.1
		adm = 1
		dip = 0
		mil = 0
		female = yes
    }
	queen = {
 		name = "Fengrin"
		dynasty = "Or'Oakwood"
		birth_date = 1820.1.1
		adm = 5
		dip = 6
		mil = 6
    }
	heir = {
 		name = "Sarmionor"
		monarch_name = "Sarmionor I"
		dynasty = "Camoran"
		birth_date = 1848.1.1
		death_date = 1932.1.1
		claim = 100
		adm = 2
		dip = 4
		mil = 5
    }
}

1932.1.1 = {
	monarch = {
 		name = "Gethawen"
		dynasty = "Camoran"
		birth_date = 1893.1.1
		adm = 4
		dip = 5
		mil = 0
		female = yes
    }
	queen = {
 		name = "Raendor"
		dynasty = "Or'Niveneth"
		birth_date = 1884.1.1
		adm = 1
		dip = 4
		mil = 6
    }
	heir = {
 		name = "Gandalith"
		monarch_name = "Gandalith I"
		dynasty = "Camoran"
		birth_date = 1921.1.1
		death_date = 1989.1.1
		claim = 100
		adm = 5
		dip = 2
		mil = 5
		female = yes
    }
}

1989.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Darahawn"
		monarch_name = "Darahawn I"
		dynasty = "Camoran"
		birth_date = 1974.1.1
		death_date = 1992.1.1
		claim = 100
		adm = 3
		dip = 2
		mil = 2
		female = yes
    }
}

1992.1.1 = {
	monarch = {
 		name = "Menthorn"
		dynasty = "Camoran"
		birth_date = 1962.1.1
		adm = 6
		dip = 3
		mil = 4
    }
}

2075.1.1 = {
	monarch = {
 		name = "Glalongoth"
		dynasty = "Camoran"
		birth_date = 2036.1.1
		adm = 1
		dip = 4
		mil = 6
    }
	queen = {
 		name = "Firuin"
		dynasty = "Or'Riverwood"
		birth_date = 2025.1.1
		adm = 1
		dip = 1
		mil = 0
		female = yes
    }
}

2130.1.1 = {
	monarch = {
 		name = "Celonron"
		dynasty = "Camoran"
		birth_date = 2085.1.1
		adm = 2
		dip = 5
		mil = 5
    }
	queen = {
 		name = "Gwaerarth"
		dynasty = "Ur'Ebon"
		birth_date = 2105.1.1
		adm = 6
		dip = 3
		mil = 4
		female = yes
    }
	heir = {
 		name = "Bodring"
		monarch_name = "Bodring I"
		dynasty = "Camoran"
		birth_date = 2117.1.1
		death_date = 2205.1.1
		claim = 100
		adm = 3
		dip = 2
		mil = 3
    }
}

2205.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Ogarendir"
		monarch_name = "Ogarendir I"
		dynasty = "Camoran"
		birth_date = 2200.1.1
		death_date = 2218.1.1
		claim = 100
		adm = 1
		dip = 1
		mil = 6
    }
}

2218.1.1 = {
	monarch = {
 		name = "Findun"
		dynasty = "Camoran"
		birth_date = 2185.1.1
		adm = 0
		dip = 4
		mil = 0
    }
}

2280.1.1 = {
	monarch = {
 		name = "Celelruin"
		dynasty = "Camoran"
		birth_date = 2244.1.1
		adm = 6
		dip = 4
		mil = 3
    }
}

2337.1.1 = {
	monarch = {
 		name = "Galathril"
		dynasty = "Camoran"
		birth_date = 2315.1.1
		adm = 4
		dip = 3
		mil = 0
		female = yes
    }
	queen = {
 		name = "Neronnir"
		dynasty = "Ca'Elsesse"
		birth_date = 2292.1.1
		adm = 1
		dip = 1
		mil = 6
    }
	heir = {
 		name = "Fayagwen"
		monarch_name = "Fayagwen I"
		dynasty = "Camoran"
		birth_date = 2333.1.1
		death_date = 2386.1.1
		claim = 100
		adm = 5
		dip = 0
		mil = 5
		female = yes
    }
}

2386.1.1 = {
	monarch = {
 		name = "Rolion"
		dynasty = "Camoran"
		birth_date = 2341.1.1
		adm = 0
		dip = 1
		mil = 0
    }
}

2455.1.1 = {
	monarch = {
 		name = "Maelin"
		dynasty = "Camoran"
		birth_date = 2422.1.1
		adm = 0
		dip = 5
		mil = 1
		female = yes
    }
	queen = {
 		name = "Draugorin"
		dynasty = "Ca'Meneia"
		birth_date = 2435.1.1
		adm = 2
		dip = 6
		mil = 3
    }
	heir = {
 		name = "Hunindir"
		monarch_name = "Hunindir I"
		dynasty = "Camoran"
		birth_date = 2453.1.1
		death_date = 2514.1.1
		claim = 100
		adm = 6
		dip = 5
		mil = 2
    }
}

