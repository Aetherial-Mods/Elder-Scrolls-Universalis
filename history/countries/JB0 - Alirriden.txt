government = monarchy
government_rank = 1
mercantilism = 1
technology_group = thousand_tg
religion = tang_mo_pantheon
primary_culture = tangmo
capital = 583
# = 583

54.1.1 = {
	monarch = {
 		name = "Tebhrota"
		dynasty = "Ejurtid"
		birth_date = 26.1.1
		adm = 6
		dip = 4
		mil = 4
    }
}

97.1.1 = {
	monarch = {
 		name = "Hade"
		dynasty = "Argarhabush"
		birth_date = 77.1.1
		adm = 4
		dip = 3
		mil = 1
		female = yes
    }
	queen = {
 		name = "Kibhunt"
		dynasty = "Argarhabush"
		birth_date = 69.1.1
		adm = 1
		dip = 1
		mil = 6
    }
	heir = {
 		name = "Prarpusthey"
		monarch_name = "Prarpusthey I"
		dynasty = "Argarhabush"
		birth_date = 88.1.1
		death_date = 164.1.1
		claim = 100
		adm = 5
		dip = 0
		mil = 5
    }
}

164.1.1 = {
	monarch = {
 		name = "Edu"
		dynasty = "Prilger"
		birth_date = 115.1.1
		adm = 4
		dip = 3
		mil = 6
		female = yes
    }
	queen = {
 		name = "Rilgunt"
		dynasty = "Prilger"
		birth_date = 113.1.1
		adm = 4
		dip = 0
		mil = 0
    }
	heir = {
 		name = "Bhingasmi"
		monarch_name = "Bhingasmi I"
		dynasty = "Prilger"
		birth_date = 149.1.1
		death_date = 229.1.1
		claim = 100
		adm = 1
		dip = 5
		mil = 6
		female = yes
    }
}

229.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Jushmad"
		monarch_name = "Jushmad I"
		dynasty = "Rabud"
		birth_date = 222.1.1
		death_date = 240.1.1
		claim = 100
		adm = 6
		dip = 4
		mil = 2
    }
}

240.1.1 = {
	monarch = {
 		name = "Amahubhas"
		dynasty = "Dacurmivor"
		birth_date = 209.1.1
		adm = 6
		dip = 1
		mil = 3
    }
}

304.1.1 = {
	monarch = {
 		name = "Ksijnay"
		dynasty = "Carmar"
		birth_date = 268.1.1
		adm = 4
		dip = 0
		mil = 6
    }
	queen = {
 		name = "Sithu"
		dynasty = "Carmar"
		birth_date = 277.1.1
		adm = 1
		dip = 6
		mil = 5
		female = yes
    }
	heir = {
 		name = "Yusvushin"
		monarch_name = "Yusvushin I"
		dynasty = "Carmar"
		birth_date = 303.1.1
		death_date = 369.1.1
		claim = 100
		adm = 5
		dip = 4
		mil = 4
    }
}

369.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Jorpet"
		monarch_name = "Jorpet I"
		dynasty = "Hagnay"
		birth_date = 363.1.1
		death_date = 381.1.1
		claim = 100
		adm = 3
		dip = 3
		mil = 1
    }
}

381.1.1 = {
	monarch = {
 		name = "Udvandhithe"
		dynasty = "Elas"
		birth_date = 347.1.1
		adm = 6
		dip = 5
		mil = 3
		female = yes
    }
	queen = {
 		name = "Vakrubutyan"
		dynasty = "Elas"
		birth_date = 347.1.1
		adm = 3
		dip = 3
		mil = 2
    }
}

427.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Ubhraken"
		monarch_name = "Ubhraken I"
		dynasty = "Dhajavyengha"
		birth_date = 426.1.1
		death_date = 444.1.1
		claim = 100
		adm = 3
		dip = 0
		mil = 0
    }
}

444.1.1 = {
	monarch = {
 		name = "Ubaptish"
		dynasty = "Asvan"
		birth_date = 420.1.1
		adm = 3
		dip = 4
		mil = 0
    }
	queen = {
 		name = "Bermumbo"
		dynasty = "Asvan"
		birth_date = 423.1.1
		adm = 3
		dip = 0
		mil = 1
		female = yes
    }
	heir = {
 		name = "Dacurmivor"
		monarch_name = "Dacurmivor I"
		dynasty = "Asvan"
		birth_date = 437.1.1
		death_date = 490.1.1
		claim = 100
		adm = 0
		dip = 6
		mil = 0
    }
}

490.1.1 = {
	monarch = {
 		name = "Intebha"
		dynasty = "Yegnabhe"
		birth_date = 459.1.1
		adm = 6
		dip = 2
		mil = 1
		female = yes
    }
	queen = {
 		name = "Alus"
		dynasty = "Yegnabhe"
		birth_date = 460.1.1
		adm = 3
		dip = 1
		mil = 0
    }
	heir = {
 		name = "Ument"
		monarch_name = "Ument I"
		dynasty = "Yegnabhe"
		birth_date = 485.1.1
		death_date = 544.1.1
		claim = 100
		adm = 0
		dip = 6
		mil = 6
    }
}

544.1.1 = {
	monarch = {
 		name = "Khupta"
		dynasty = "Kibhunt"
		birth_date = 516.1.1
		adm = 3
		dip = 1
		mil = 1
		female = yes
    }
	queen = {
 		name = "Vakribho"
		dynasty = "Kibhunt"
		birth_date = 524.1.1
		adm = 0
		dip = 6
		mil = 0
    }
	heir = {
 		name = "Dacurmivor"
		monarch_name = "Dacurmivor I"
		dynasty = "Kibhunt"
		birth_date = 537.1.1
		death_date = 620.1.1
		claim = 100
		adm = 4
		dip = 5
		mil = 6
    }
}

620.1.1 = {
	monarch = {
 		name = "Randaban"
		dynasty = "Dacurmivor"
		birth_date = 580.1.1
		adm = 0
		dip = 3
		mil = 5
    }
}

680.1.1 = {
	monarch = {
 		name = "Ihmoptibhunt"
		dynasty = "Argurhoy"
		birth_date = 629.1.1
		adm = 4
		dip = 5
		mil = 5
    }
}

735.1.1 = {
	monarch = {
 		name = "Khupta"
		dynasty = "Prilger"
		birth_date = 703.1.1
		adm = 5
		dip = 2
		mil = 3
		female = yes
    }
	queen = {
 		name = "Puskarmosh"
		dynasty = "Prilger"
		birth_date = 689.1.1
		adm = 5
		dip = 6
		mil = 3
    }
	heir = {
 		name = "Shitha"
		monarch_name = "Shitha I"
		dynasty = "Prilger"
		birth_date = 726.1.1
		death_date = 781.1.1
		claim = 100
		adm = 2
		dip = 4
		mil = 2
    }
}

781.1.1 = {
	monarch = {
 		name = "Kamayi"
		dynasty = "Galanirmer"
		birth_date = 735.1.1
		adm = 0
		dip = 3
		mil = 6
		female = yes
    }
}

866.1.1 = {
	monarch = {
 		name = "Armava"
		dynasty = "Jobor"
		birth_date = 818.1.1
		adm = 5
		dip = 3
		mil = 2
		female = yes
    }
}

956.1.1 = {
	monarch = {
 		name = "Yusi"
		dynasty = "Udragush"
		birth_date = 907.1.1
		adm = 3
		dip = 2
		mil = 6
		female = yes
    }
	queen = {
 		name = "Korpun"
		dynasty = "Udragush"
		birth_date = 925.1.1
		adm = 0
		dip = 0
		mil = 5
    }
}

1032.1.1 = {
	monarch = {
 		name = "Batha"
		dynasty = "Brirpeyir"
		birth_date = 1005.1.1
		adm = 5
		dip = 0
		mil = 2
		female = yes
    }
	queen = {
 		name = "Prabanut"
		dynasty = "Brirpeyir"
		birth_date = 998.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1067.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Mijnuvos"
		monarch_name = "Mijnuvos I"
		dynasty = "Vuhmatay"
		birth_date = 1062.1.1
		death_date = 1080.1.1
		claim = 100
		adm = 2
		dip = 5
		mil = 2
    }
}

1080.1.1 = {
	monarch = {
 		name = "Sovus"
		dynasty = "Yucabhan"
		birth_date = 1055.1.1
		adm = 1
		dip = 1
		mil = 2
    }
	queen = {
 		name = "Yilyi"
		dynasty = "Yucabhan"
		birth_date = 1050.1.1
		adm = 5
		dip = 0
		mil = 1
		female = yes
    }
}

1148.1.1 = {
	monarch = {
 		name = "Bharga"
		dynasty = "Ksushmuhunt"
		birth_date = 1125.1.1
		adm = 3
		dip = 6
		mil = 5
		female = yes
    }
}

1197.1.1 = {
	monarch = {
 		name = "Devhargu"
		dynasty = "Guhmas"
		birth_date = 1174.1.1
		adm = 2
		dip = 5
		mil = 1
		female = yes
    }
}

1244.1.1 = {
	monarch = {
 		name = "Buhergi"
		dynasty = "Bakarmortad"
		birth_date = 1225.1.1
		adm = 0
		dip = 4
		mil = 5
		female = yes
    }
}

1313.1.1 = {
	monarch = {
 		name = "Dhusvatyurman"
		dynasty = "Gubhrorhabad"
		birth_date = 1292.1.1
		adm = 5
		dip = 4
		mil = 2
    }
}

1390.1.1 = {
	monarch = {
 		name = "Jobor"
		dynasty = "Ebhuthyutod"
		birth_date = 1357.1.1
		adm = 3
		dip = 3
		mil = 5
    }
}

1430.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Bintabhe"
		monarch_name = "Bintabhe I"
		dynasty = "Rabud"
		birth_date = 1419.1.1
		death_date = 1437.1.1
		claim = 100
		adm = 4
		dip = 0
		mil = 3
		female = yes
    }
}

1437.1.1 = {
	monarch = {
 		name = "Buhergi"
		dynasty = "Cetuhuy"
		birth_date = 1419.1.1
		adm = 0
		dip = 1
		mil = 6
		female = yes
    }
	queen = {
 		name = "Randaban"
		dynasty = "Cetuhuy"
		birth_date = 1404.1.1
		adm = 4
		dip = 0
		mil = 5
    }
	heir = {
 		name = "Galanirmer"
		monarch_name = "Galanirmer I"
		dynasty = "Cetuhuy"
		birth_date = 1424.1.1
		death_date = 1509.1.1
		claim = 100
		adm = 1
		dip = 5
		mil = 4
    }
}

1509.1.1 = {
	monarch = {
 		name = "Soma"
		dynasty = "Hagnay"
		birth_date = 1490.1.1
		adm = 0
		dip = 2
		mil = 4
		female = yes
    }
}

1563.1.1 = {
	monarch = {
 		name = "Prilger"
		dynasty = "Shitha"
		birth_date = 1514.1.1
		adm = 5
		dip = 1
		mil = 1
    }
	queen = {
 		name = "Udhajerga"
		dynasty = "Shitha"
		birth_date = 1536.1.1
		adm = 1
		dip = 2
		mil = 3
		female = yes
    }
	heir = {
 		name = "Shitha"
		monarch_name = "Shitha I"
		dynasty = "Shitha"
		birth_date = 1556.1.1
		death_date = 1657.1.1
		claim = 100
		adm = 5
		dip = 1
		mil = 2
    }
}

1657.1.1 = {
	monarch = {
 		name = "Inithi"
		dynasty = "Rabud"
		birth_date = 1638.1.1
		adm = 4
		dip = 4
		mil = 2
		female = yes
    }
	queen = {
 		name = "Anatyur"
		dynasty = "Rabud"
		birth_date = 1607.1.1
		adm = 1
		dip = 3
		mil = 1
    }
	heir = {
 		name = "Argarhabush"
		monarch_name = "Argarhabush I"
		dynasty = "Rabud"
		birth_date = 1655.1.1
		death_date = 1711.1.1
		claim = 100
		adm = 1
		dip = 6
		mil = 2
    }
}

1711.1.1 = {
	monarch = {
 		name = "Tutturmet"
		dynasty = "Bushmava"
		birth_date = 1663.1.1
		adm = 1
		dip = 3
		mil = 2
    }
	queen = {
 		name = "Sitesa"
		dynasty = "Bushmava"
		birth_date = 1659.1.1
		adm = 5
		dip = 1
		mil = 1
		female = yes
    }
	heir = {
 		name = "Nibhrunt"
		monarch_name = "Nibhrunt I"
		dynasty = "Bushmava"
		birth_date = 1698.1.1
		death_date = 1798.1.1
		claim = 100
		adm = 1
		dip = 0
		mil = 0
    }
}

1798.1.1 = {
	monarch = {
 		name = "Ukayud"
		dynasty = "Rabud"
		birth_date = 1763.1.1
		adm = 4
		dip = 1
		mil = 3
    }
}

1882.1.1 = {
	monarch = {
 		name = "Bubundayo"
		dynasty = "Guhmas"
		birth_date = 1837.1.1
		adm = 2
		dip = 0
		mil = 6
		female = yes
    }
}

1923.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Vome"
		monarch_name = "Vome I"
		dynasty = "Yucabhan"
		birth_date = 1923.1.1
		death_date = 1941.1.1
		claim = 100
		adm = 3
		dip = 4
		mil = 4
		female = yes
    }
}

1941.1.1 = {
	monarch = {
 		name = "Korpun"
		dynasty = "Udragush"
		birth_date = 1899.1.1
		adm = 3
		dip = 1
		mil = 5
    }
	queen = {
 		name = "Batha"
		dynasty = "Udragush"
		birth_date = 1906.1.1
		adm = 3
		dip = 4
		mil = 6
		female = yes
    }
	heir = {
 		name = "Praptidi"
		monarch_name = "Praptidi I"
		dynasty = "Udragush"
		birth_date = 1927.1.1
		death_date = 1996.1.1
		claim = 100
		adm = 0
		dip = 3
		mil = 5
		female = yes
    }
}

1996.1.1 = {
	monarch = {
 		name = "Natta"
		dynasty = "Oskirtunghid"
		birth_date = 1965.1.1
		adm = 6
		dip = 6
		mil = 5
		female = yes
    }
}

2078.1.1 = {
	monarch = {
 		name = "Mijnuvos"
		dynasty = "Ument"
		birth_date = 2029.1.1
		adm = 4
		dip = 5
		mil = 1
    }
}

2117.1.1 = {
	monarch = {
 		name = "Ita"
		dynasty = "Nurposhrasthay"
		birth_date = 2079.1.1
		adm = 3
		dip = 5
		mil = 5
		female = yes
    }
}

2155.1.1 = {
	monarch = {
 		name = "Asvayathyos"
		dynasty = "Yulmint"
		birth_date = 2102.1.1
		adm = 1
		dip = 4
		mil = 2
    }
}

2202.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Avha"
		monarch_name = "Avha I"
		dynasty = "Sarganusthash"
		birth_date = 2197.1.1
		death_date = 2215.1.1
		claim = 100
		adm = 2
		dip = 1
		mil = 0
		female = yes
    }
}

2215.1.1 = {
	monarch = {
 		name = "Kamayi"
		dynasty = "Elas"
		birth_date = 2182.1.1
		adm = 4
		dip = 2
		mil = 2
		female = yes
    }
	queen = {
 		name = "Yusvushin"
		dynasty = "Elas"
		birth_date = 2184.1.1
		adm = 1
		dip = 1
		mil = 1
    }
	heir = {
 		name = "Nidruthyont"
		monarch_name = "Nidruthyont I"
		dynasty = "Elas"
		birth_date = 2208.1.1
		death_date = 2258.1.1
		claim = 100
		adm = 5
		dip = 6
		mil = 0
    }
}

2258.1.1 = {
	monarch = {
 		name = "Dhajavyengha"
		dynasty = "Udrarmush"
		birth_date = 2233.1.1
		adm = 4
		dip = 3
		mil = 0
    }
}

2354.1.1 = {
	monarch = {
 		name = "Vushmus"
		dynasty = "Shehant"
		birth_date = 2333.1.1
		adm = 3
		dip = 2
		mil = 4
    }
}

2394.1.1 = {
	monarch = {
 		name = "Asvayathyos"
		dynasty = "Kibhunt"
		birth_date = 2344.1.1
		adm = 1
		dip = 1
		mil = 1
    }
	queen = {
 		name = "Bhingasmi"
		dynasty = "Kibhunt"
		birth_date = 2347.1.1
		adm = 5
		dip = 0
		mil = 0
		female = yes
    }
}

2449.1.1 = {
	monarch = {
 		name = "Armengiguy"
		dynasty = "Buttash"
		birth_date = 2407.1.1
		adm = 3
		dip = 6
		mil = 3
    }
	queen = {
 		name = "Dhuntakadra"
		dynasty = "Buttash"
		birth_date = 2420.1.1
		adm = 0
		dip = 4
		mil = 2
		female = yes
    }
}

