government = theocracy
government_rank = 1
mercantilism = 1
technology_group = elven_tg
religion = old_way_cult
primary_culture = altmer
capital = 4708

54.1.1 = {
	monarch = {
 		name = "Firorore"
		dynasty = "Bel-Tanorne"
		birth_date = 26.1.1
		adm = 4
		dip = 0
		mil = 4
		female = yes
    }
}

129.1.1 = {
	monarch = {
 		name = "Curissil"
		dynasty = "Ael-Greenwater"
		birth_date = 102.1.1
		adm = 6
		dip = 1
		mil = 5
    }
}

166.1.1 = {
	monarch = {
 		name = "Ateldil"
		dynasty = "Wel-Eleaninde"
		birth_date = 132.1.1
		adm = 4
		dip = 1
		mil = 2
    }
}

223.1.1 = {
	monarch = {
 		name = "Carawen"
		dynasty = "Bel-Soneya"
		birth_date = 182.1.1
		adm = 0
		dip = 2
		mil = 4
		female = yes
    }
}

265.1.1 = {
	monarch = {
 		name = "Nanwen"
		dynasty = "Ael-Ohterane"
		birth_date = 243.1.1
		adm = 0
		dip = 6
		mil = 2
    }
}

333.1.1 = {
	monarch = {
 		name = "Firruldoril"
		dynasty = "Bel-Taarine"
		birth_date = 315.1.1
		adm = 3
		dip = 0
		mil = 5
    }
}

415.1.1 = {
	monarch = {
 		name = "Anirwe"
		dynasty = "Bel-Terna"
		birth_date = 362.1.1
		adm = 5
		dip = 2
		mil = 6
		female = yes
    }
}

485.1.1 = {
	monarch = {
 		name = "Taaruraire"
		dynasty = "Bel-Siriginia"
		birth_date = 463.1.1
		adm = 3
		dip = 1
		mil = 3
		female = yes
    }
}

582.1.1 = {
	monarch = {
 		name = "Caldartur"
		dynasty = "Ael-Granaire"
		birth_date = 531.1.1
		adm = 2
		dip = 0
		mil = 0
    }
}

622.1.1 = {
	monarch = {
 		name = "Taurilfin"
		dynasty = "Ael-Phaer"
		birth_date = 576.1.1
		adm = 0
		dip = 6
		mil = 3
    }
}

684.1.1 = {
	monarch = {
 		name = "Nemuutian"
		dynasty = "Ael-Mirlenya"
		birth_date = 632.1.1
		adm = 5
		dip = 6
		mil = 0
    }
}

779.1.1 = {
	monarch = {
 		name = "Glenadir"
		dynasty = "		"
		birth_date = 732.1.1
		adm = 3
		dip = 5
		mil = 3
    }
}

848.1.1 = {
	monarch = {
 		name = "Calcamar"
		dynasty = "Wel-Alkinosin"
		birth_date = 824.1.1
		adm = 2
		dip = 4
		mil = 0
    }
}

884.1.1 = {
	monarch = {
 		name = "Tarqualaite"
		dynasty = "Bel-Valaena"
		birth_date = 856.1.1
		adm = 0
		dip = 3
		mil = 4
    }
}

940.1.1 = {
	monarch = {
 		name = "Nelundahin"
		dynasty = "Ael-Mathiisen"
		birth_date = 909.1.1
		adm = 2
		dip = 4
		mil = 5
    }
}

1006.1.1 = {
	monarch = {
 		name = "Glamion"
		dynasty = "Bel-Salingahl"
		birth_date = 956.1.1
		adm = 0
		dip = 4
		mil = 2
    }
}

1084.1.1 = {
	monarch = {
 		name = "Muurine"
		dynasty = "Ael-Laraethfhaer"
		birth_date = 1052.1.1
		adm = 5
		dip = 3
		mil = 6
		female = yes
    }
}

1168.1.1 = {
	monarch = {
 		name = "Heldil"
		dynasty = "Ael-Miriath"
		birth_date = 1129.1.1
		adm = 3
		dip = 2
		mil = 2
    }
}

1216.1.1 = {
	monarch = {
 		name = "Camaano"
		dynasty = "Bel-Saelinoth"
		birth_date = 1169.1.1
		adm = 2
		dip = 1
		mil = 6
    }
}

1309.1.1 = {
	monarch = {
 		name = "Telultur"
		dynasty = "Wel-Angaelle"
		birth_date = 1283.1.1
		adm = 0
		dip = 0
		mil = 3
    }
}

1360.1.1 = {
	monarch = {
 		name = "Morna"
		dynasty = "Ael-Jaerfhaer"
		birth_date = 1337.1.1
		adm = 5
		dip = 0
		mil = 6
		female = yes
    }
}

1399.1.1 = {
	monarch = {
 		name = "Hecalindil"
		dynasty = "Wel-Angaelle"
		birth_date = 1368.1.1
		adm = 3
		dip = 6
		mil = 3
    }
}

1498.1.1 = {
	monarch = {
 		name = "Arfiril"
		dynasty = "Ael-Nenurmend"
		birth_date = 1474.1.1
		adm = 5
		dip = 0
		mil = 4
		female = yes
    }
}

1596.1.1 = {
	monarch = {
 		name = "Teldulsewen"
		dynasty = "Wel-Elsinthaer"
		birth_date = 1543.1.1
		adm = 4
		dip = 6
		mil = 1
		female = yes
    }
}

1694.1.1 = {
	monarch = {
 		name = "Cingarfin"
		dynasty = "Wel-Eleaninde"
		birth_date = 1649.1.1
		adm = 2
		dip = 6
		mil = 5
    }
}

1793.1.1 = {
	monarch = {
 		name = "Tilarion"
		dynasty = "Wel-Aelsinonin"
		birth_date = 1753.1.1
		adm = 0
		dip = 5
		mil = 1
    }
}

1881.1.1 = {
	monarch = {
 		name = "Norendo"
		dynasty = "Bel-Valia"
		birth_date = 1857.1.1
		adm = 5
		dip = 4
		mil = 5
    }
}

1974.1.1 = {
	monarch = {
 		name = "Aldermil"
		dynasty = "Ael-Graddun"
		birth_date = 1943.1.1
		adm = 5
		dip = 0
		mil = 5
		female = yes
    }
}

2060.1.1 = {
	monarch = {
 		name = "Rundirwe"
		dynasty = "Wel-Angaelle"
		birth_date = 2029.1.1
		adm = 3
		dip = 0
		mil = 2
		female = yes
    }
}

2159.1.1 = {
	monarch = {
 		name = "Angamar"
		dynasty = "Bel-Sondsara"
		birth_date = 2140.1.1
		adm = 1
		dip = 6
		mil = 6
    }
}

2198.1.1 = {
	monarch = {
 		name = "Seritir"
		dynasty = "Ael-Greenwater"
		birth_date = 2157.1.1
		adm = 6
		dip = 5
		mil = 2
		female = yes
    }
}

2252.1.1 = {
	monarch = {
 		name = "Mirdinnarian"
		dynasty = "Ael-Niria"
		birth_date = 2201.1.1
		adm = 5
		dip = 4
		mil = 6
    }
}

2322.1.1 = {
	monarch = {
 		name = "Fanyarel"
		dynasty = "Bel-Sontarya"
		birth_date = 2296.1.1
		adm = 3
		dip = 3
		mil = 2
    }
}

2399.1.1 = {
	monarch = {
 		name = "Altoririe"
		dynasty = "Wel-Croddlehurst"
		birth_date = 2358.1.1
		adm = 1
		dip = 3
		mil = 6
		female = yes
    }
}

2437.1.1 = {
	monarch = {
 		name = "Sintaananil"
		dynasty = "Wel-Endrae"
		birth_date = 2399.1.1
		adm = 6
		dip = 2
		mil = 3
    }
}

