government = monarchy
government_rank = 3
mercantilism = 1
technology_group = cyrodiil_tg
religion = molag_bal_cult
primary_culture = ayleid
capital = 1150

54.1.1 = {
	monarch = {
 		name = "Lida"
		dynasty = "Mea-Allevar"
		birth_date = 25.1.1
		adm = 4
		dip = 3
		mil = 2
    }
}

103.1.1 = {
	monarch = {
 		name = "Wynaldia"
		dynasty = "Vae-Vlastarus"
		birth_date = 76.1.1
		adm = 2
		dip = 3
		mil = 6
		female = yes
    }
}

197.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Laloriaran"
		monarch_name = "Laloriaran I"
		dynasty = "Mea-Civiant"
		birth_date = 182.1.1
		death_date = 200.1.1
		claim = 100
		adm = 3
		dip = 0
		mil = 4
    }
}

200.1.1 = {
	monarch = {
 		name = "Femde"
		dynasty = "Ula-Linchal"
		birth_date = 148.1.1
		adm = 1
		dip = 6
		mil = 0
    }
	queen = {
 		name = "Cimase"
		dynasty = "Ula-Linchal"
		birth_date = 160.1.1
		adm = 5
		dip = 4
		mil = 6
		female = yes
    }
	heir = {
 		name = "Wommi"
		monarch_name = "Wommi I"
		dynasty = "Ula-Linchal"
		birth_date = 193.1.1
		death_date = 277.1.1
		claim = 100
		adm = 2
		dip = 3
		mil = 5
		female = yes
    }
}

277.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Vurel"
		monarch_name = "Vurel I"
		dynasty = "Mea-Correllia"
		birth_date = 269.1.1
		death_date = 287.1.1
		claim = 100
		adm = 0
		dip = 2
		mil = 2
		female = yes
    }
}

287.1.1 = {
	monarch = {
 		name = "Nym"
		dynasty = "Mea-Alessia"
		birth_date = 234.1.1
		adm = 2
		dip = 4
		mil = 4
    }
}

345.1.1 = {
	monarch = {
 		name = "Harrylnes"
		dynasty = "Vae-Varondo"
		birth_date = 292.1.1
		adm = 1
		dip = 3
		mil = 1
		female = yes
    }
	queen = {
 		name = "Lagan"
		dynasty = "Vae-Varondo"
		birth_date = 326.1.1
		adm = 5
		dip = 1
		mil = 0
    }
}

432.1.1 = {
	monarch = {
 		name = "Nimender"
		dynasty = "Vae-Sardavar"
		birth_date = 383.1.1
		adm = 3
		dip = 1
		mil = 3
    }
}

470.1.1 = {
	monarch = {
 		name = "Endarre"
		dynasty = "Ula-Ontus"
		birth_date = 429.1.1
		adm = 5
		dip = 2
		mil = 5
    }
	queen = {
 		name = "Puzulnudu"
		dynasty = "Ula-Ontus"
		birth_date = 419.1.1
		adm = 5
		dip = 5
		mil = 6
		female = yes
    }
	heir = {
 		name = "Dynnyan"
		monarch_name = "Dynnyan I"
		dynasty = "Ula-Ontus"
		birth_date = 470.1.1
		death_date = 544.1.1
		claim = 100
		adm = 2
		dip = 4
		mil = 5
		female = yes
    }
}

544.1.1 = {
	monarch = {
 		name = "Hergor"
		dynasty = "Ula-Latara"
		birth_date = 518.1.1
		adm = 1
		dip = 0
		mil = 5
    }
	queen = {
 		name = "Hunnul"
		dynasty = "Ula-Latara"
		birth_date = 513.1.1
		adm = 5
		dip = 6
		mil = 4
		female = yes
    }
	heir = {
 		name = "Nanalne"
		monarch_name = "Nanalne I"
		dynasty = "Ula-Latara"
		birth_date = 543.1.1
		death_date = 631.1.1
		claim = 100
		adm = 2
		dip = 4
		mil = 3
		female = yes
    }
}

631.1.1 = {
	monarch = {
 		name = "Dini"
		dynasty = "Mea-Allevar"
		birth_date = 596.1.1
		adm = 5
		dip = 6
		mil = 6
		female = yes
    }
	queen = {
 		name = "Rorhemeras"
		dynasty = "Mea-Allevar"
		birth_date = 589.1.1
		adm = 2
		dip = 4
		mil = 4
    }
	heir = {
 		name = "Nilind"
		monarch_name = "Nilind I"
		dynasty = "Mea-Allevar"
		birth_date = 623.1.1
		death_date = 679.1.1
		claim = 100
		adm = 5
		dip = 3
		mil = 3
    }
}

679.1.1 = {
	monarch = {
 		name = "Hadhuul"
		dynasty = "Vae-Vassorman"
		birth_date = 659.1.1
		adm = 1
		dip = 4
		mil = 6
    }
	queen = {
 		name = "Yrru"
		dynasty = "Vae-Vassorman"
		birth_date = 646.1.1
		adm = 5
		dip = 3
		mil = 5
		female = yes
    }
	heir = {
 		name = "Nanalne"
		monarch_name = "Nanalne I"
		dynasty = "Vae-Vassorman"
		birth_date = 675.1.1
		death_date = 767.1.1
		claim = 100
		adm = 2
		dip = 1
		mil = 4
		female = yes
    }
}

767.1.1 = {
	monarch = {
 		name = "Summi"
		dynasty = "Vae-Skingrad"
		birth_date = 719.1.1
		adm = 1
		dip = 4
		mil = 4
		female = yes
    }
	queen = {
 		name = "Tamhuundogo"
		dynasty = "Vae-Skingrad"
		birth_date = 714.1.1
		adm = 2
		dip = 1
		mil = 5
    }
	heir = {
 		name = "Lomaldydaa"
		monarch_name = "Lomaldydaa I"
		dynasty = "Vae-Skingrad"
		birth_date = 755.1.1
		death_date = 849.1.1
		claim = 100
		adm = 1
		dip = 4
		mil = 5
		female = yes
    }
}

849.1.1 = {
	monarch = {
 		name = "Ymmu"
		dynasty = "Ula-Renne"
		birth_date = 823.1.1
		adm = 4
		dip = 6
		mil = 1
		female = yes
    }
}

912.1.1 = {
	monarch = {
 		name = "Celethelel"
		dynasty = "Mea-Corannus"
		birth_date = 881.1.1
		adm = 6
		dip = 0
		mil = 2
		female = yes
    }
}

972.1.1 = {
	monarch = {
 		name = "Nugant"
		dynasty = "Ula-Marcerienna"
		birth_date = 930.1.1
		adm = 4
		dip = 6
		mil = 6
    }
}

1034.1.1 = {
	monarch = {
 		name = "Glinferen"
		dynasty = "Vae-Selertia"
		birth_date = 995.1.1
		adm = 2
		dip = 5
		mil = 3
    }
}

1078.1.1 = {
	monarch = {
 		name = "Wommi"
		dynasty = "Ula-Julidonea"
		birth_date = 1052.1.1
		adm = 0
		dip = 5
		mil = 6
		female = yes
    }
}

1158.1.1 = {
	monarch = {
 		name = "Cyr"
		dynasty = "Mea-Harm's"
		birth_date = 1130.1.1
		adm = 6
		dip = 4
		mil = 3
    }
}

1206.1.1 = {
	monarch = {
 		name = "Cam"
		dynasty = "Mea-Alessia"
		birth_date = 1154.1.1
		adm = 4
		dip = 3
		mil = 6
    }
	queen = {
 		name = "Azu"
		dynasty = "Mea-Alessia"
		birth_date = 1188.1.1
		adm = 1
		dip = 2
		mil = 5
		female = yes
    }
}

1305.1.1 = {
	monarch = {
 		name = "Djel"
		dynasty = "Ula-Larilatia"
		birth_date = 1287.1.1
		adm = 6
		dip = 1
		mil = 2
    }
	queen = {
 		name = "Valasha"
		dynasty = "Ula-Larilatia"
		birth_date = 1254.1.1
		adm = 3
		dip = 6
		mil = 1
		female = yes
    }
	heir = {
 		name = "Cyr"
		monarch_name = "Cyr I"
		dynasty = "Ula-Larilatia"
		birth_date = 1299.1.1
		death_date = 1356.1.1
		claim = 100
		adm = 0
		dip = 5
		mil = 0
    }
}

1356.1.1 = {
	monarch = {
 		name = "Ynnylsasul"
		dynasty = "Mea-Hastrel"
		birth_date = 1321.1.1
		adm = 2
		dip = 6
		mil = 2
		female = yes
    }
	queen = {
 		name = "Anumaril"
		dynasty = "Mea-Hastrel"
		birth_date = 1325.1.1
		adm = 6
		dip = 5
		mil = 1
    }
	heir = {
 		name = "Narges"
		monarch_name = "Narges I"
		dynasty = "Mea-Hastrel"
		birth_date = 1348.1.1
		death_date = 1450.1.1
		claim = 100
		adm = 3
		dip = 3
		mil = 0
    }
}

1450.1.1 = {
	monarch = {
 		name = "Djel"
		dynasty = "Mea-Adrarcella"
		birth_date = 1424.1.1
		adm = 3
		dip = 0
		mil = 1
    }
}

1504.1.1 = {
	monarch = {
 		name = "Vuudunt"
		dynasty = "Mea-Harm's"
		birth_date = 1455.1.1
		adm = 1
		dip = 6
		mil = 4
    }
	queen = {
 		name = "Came"
		dynasty = "Mea-Harm's"
		birth_date = 1452.1.1
		adm = 5
		dip = 4
		mil = 3
		female = yes
    }
	heir = {
 		name = "Danasso"
		monarch_name = "Danasso I"
		dynasty = "Mea-Harm's"
		birth_date = 1495.1.1
		death_date = 1585.1.1
		claim = 100
		adm = 2
		dip = 3
		mil = 2
		female = yes
    }
}

1585.1.1 = {
	monarch = {
 		name = "Nilind"
		dynasty = "Vae-Vietia"
		birth_date = 1556.1.1
		adm = 4
		dip = 4
		mil = 5
    }
	queen = {
 		name = "Dyzas"
		dynasty = "Vae-Vietia"
		birth_date = 1536.1.1
		adm = 1
		dip = 3
		mil = 3
		female = yes
    }
}

1681.1.1 = {
	monarch = {
 		name = "Djangent"
		dynasty = "Vae-Vandiand"
		birth_date = 1655.1.1
		adm = 6
		dip = 2
		mil = 0
    }
}

1726.1.1 = {
	monarch = {
 		name = "Lydar"
		dynasty = "Ula-Lalaifre"
		birth_date = 1685.1.1
		adm = 5
		dip = 1
		mil = 4
    }
}

1761.1.1 = {
	monarch = {
 		name = "Irreshyva"
		dynasty = "Mea-Aerienna"
		birth_date = 1743.1.1
		adm = 3
		dip = 0
		mil = 0
		female = yes
    }
}

1807.1.1 = {
	monarch = {
 		name = "Elanwe"
		dynasty = "Vae-Skingrad"
		birth_date = 1766.1.1
		adm = 1
		dip = 0
		mil = 4
		female = yes
    }
	queen = {
 		name = "Muanhilan"
		dynasty = "Vae-Skingrad"
		birth_date = 1783.1.1
		adm = 5
		dip = 5
		mil = 3
    }
	heir = {
 		name = "Limdardhunur"
		monarch_name = "Limdardhunur I"
		dynasty = "Vae-Skingrad"
		birth_date = 1800.1.1
		death_date = 1867.1.1
		claim = 100
		adm = 2
		dip = 4
		mil = 2
    }
}

1867.1.1 = {
	monarch = {
 		name = "Helydu"
		dynasty = "Vae-Vlastarus"
		birth_date = 1829.1.1
		adm = 1
		dip = 0
		mil = 2
		female = yes
    }
	queen = {
 		name = "Lymher"
		dynasty = "Vae-Vlastarus"
		birth_date = 1839.1.1
		adm = 5
		dip = 6
		mil = 1
    }
	heir = {
 		name = "Vem"
		monarch_name = "Vem I"
		dynasty = "Vae-Vlastarus"
		birth_date = 1859.1.1
		death_date = 1913.1.1
		claim = 100
		adm = 1
		dip = 0
		mil = 4
    }
}

1913.1.1 = {
	monarch = {
 		name = "Immi"
		dynasty = "Vae-Vautellia"
		birth_date = 1866.1.1
		adm = 0
		dip = 3
		mil = 4
		female = yes
    }
	queen = {
 		name = "Djus"
		dynasty = "Vae-Vautellia"
		birth_date = 1885.1.1
		adm = 1
		dip = 0
		mil = 5
    }
	heir = {
 		name = "Nisnen"
		monarch_name = "Nisnen I"
		dynasty = "Vae-Vautellia"
		birth_date = 1908.1.1
		death_date = 1996.1.1
		claim = 100
		adm = 4
		dip = 5
		mil = 4
    }
}

1996.1.1 = {
	monarch = {
 		name = "Luudagarund"
		dynasty = "Ula-Istirus"
		birth_date = 1965.1.1
		adm = 3
		dip = 5
		mil = 1
    }
	queen = {
 		name = "Uzynhoth"
		dynasty = "Ula-Istirus"
		birth_date = 1949.1.1
		adm = 0
		dip = 3
		mil = 6
		female = yes
    }
	heir = {
 		name = "Yhu"
		monarch_name = "Yhu I"
		dynasty = "Ula-Istirus"
		birth_date = 1992.1.1
		death_date = 2036.1.1
		claim = 100
		adm = 3
		dip = 2
		mil = 5
		female = yes
    }
}

2036.1.1 = {
	monarch = {
 		name = "Toduurhasym"
		dynasty = "Ula-Horuria"
		birth_date = 2005.1.1
		adm = 3
		dip = 5
		mil = 6
    }
}

2117.1.1 = {
	monarch = {
 		name = "Dondont"
		dynasty = "Ula-Ryndenyse"
		birth_date = 2067.1.1
		adm = 0
		dip = 0
		mil = 6
    }
}

2189.1.1 = {
	monarch = {
 		name = "Min"
		dynasty = "Ula-Istirus"
		birth_date = 2159.1.1
		adm = 2
		dip = 1
		mil = 0
    }
	queen = {
 		name = "Nivo"
		dynasty = "Ula-Istirus"
		birth_date = 2138.1.1
		adm = 2
		dip = 5
		mil = 1
		female = yes
    }
}

2250.1.1 = {
	monarch = {
 		name = "Leydel"
		dynasty = "Vae-Vonara"
		birth_date = 2225.1.1
		adm = 4
		dip = 6
		mil = 3
    }
}

2298.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Lida"
		monarch_name = "Lida I"
		dynasty = "Ula-Renne"
		birth_date = 2283.1.1
		death_date = 2301.1.1
		claim = 100
		adm = 1
		dip = 1
		mil = 3
    }
}

2301.1.1 = {
	monarch = {
 		name = "Omashaul"
		dynasty = "Mea-Culotte"
		birth_date = 2271.1.1
		adm = 0
		dip = 4
		mil = 3
    }
	queen = {
 		name = "Leru"
		dynasty = "Mea-Culotte"
		birth_date = 2282.1.1
		adm = 4
		dip = 3
		mil = 2
		female = yes
    }
	heir = {
 		name = "Gand"
		monarch_name = "Gand I"
		dynasty = "Mea-Culotte"
		birth_date = 2293.1.1
		death_date = 2364.1.1
		claim = 100
		adm = 1
		dip = 2
		mil = 1
    }
}

2364.1.1 = {
	monarch = {
 		name = "Vunhualdadont"
		dynasty = "Ula-Ontus"
		birth_date = 2334.1.1
		adm = 4
		dip = 3
		mil = 4
    }
}

2446.1.1 = {
	monarch = {
 		name = "Gordhaur"
		dynasty = "Mea-Andrulusus"
		birth_date = 2398.1.1
		adm = 2
		dip = 2
		mil = 0
    }
	queen = {
 		name = "Vyza"
		dynasty = "Mea-Andrulusus"
		birth_date = 2418.1.1
		adm = 6
		dip = 1
		mil = 6
		female = yes
    }
}

