government = tribal
government_rank = 1
mercantilism = 1
technology_group = orsimer_tg
religion = orcish_pantheon
primary_culture = orsimer
capital = 2816

54.1.1 = {
	monarch = {
 		name = "Yam"
		dynasty = "gro-Mol"
		birth_date = 18.1.1
		adm = 1
		dip = 4
		mil = 5
    }
}

141.1.1 = {
	monarch = {
 		name = "Ragbul"
		dynasty = "gro-Gargak"
		birth_date = 118.1.1
		adm = 6
		dip = 4
		mil = 2
    }
}

181.1.1 = {
	monarch = {
 		name = "Larob"
		dynasty = "gro-Sneg"
		birth_date = 160.1.1
		adm = 1
		dip = 5
		mil = 4
    }
}

230.1.1 = {
	monarch = {
 		name = "Gahgdar"
		dynasty = "gro-Agumkul"
		birth_date = 194.1.1
		adm = 6
		dip = 4
		mil = 0
    }
}

323.1.1 = {
	monarch = {
 		name = "Gurum"
		dynasty = "gro-Ogularz"
		birth_date = 295.1.1
		adm = 4
		dip = 3
		mil = 4
		female = yes
    }
}

369.1.1 = {
	monarch = {
 		name = "Azhnakha"
		dynasty = "gro-Marzul"
		birth_date = 336.1.1
		adm = 2
		dip = 2
		mil = 1
		female = yes
    }
}

456.1.1 = {
	monarch = {
 		name = "Namoroth"
		dynasty = "gro-Bugharz"
		birth_date = 421.1.1
		adm = 5
		dip = 4
		mil = 3
    }
}

555.1.1 = {
	monarch = {
 		name = "Grundu"
		dynasty = "gro-Shurrog"
		birth_date = 530.1.1
		adm = 0
		dip = 5
		mil = 5
    }
}

626.1.1 = {
	monarch = {
 		name = "Orag"
		dynasty = "gro-Ogzar"
		birth_date = 604.1.1
		adm = 5
		dip = 4
		mil = 1
		female = yes
    }
}

712.1.1 = {
	monarch = {
 		name = "Gunran"
		dynasty = "gro-Muzgalg"
		birth_date = 667.1.1
		adm = 4
		dip = 4
		mil = 5
    }
}

776.1.1 = {
	monarch = {
 		name = "Bumnog"
		dynasty = "gro-Gorak"
		birth_date = 756.1.1
		adm = 2
		dip = 3
		mil = 1
    }
}

846.1.1 = {
	monarch = {
 		name = "Thugnekh"
		dynasty = "gro-Szugburg"
		birth_date = 819.1.1
		adm = 0
		dip = 2
		mil = 5
		female = yes
    }
}

903.1.1 = {
	monarch = {
 		name = "Nunkuk"
		dynasty = "gro-Othigu"
		birth_date = 878.1.1
		adm = 5
		dip = 1
		mil = 2
    }
}

960.1.1 = {
	monarch = {
 		name = "Glathut"
		dynasty = "gro-Usn"
		birth_date = 919.1.1
		adm = 3
		dip = 0
		mil = 5
		female = yes
    }
}

1012.1.1 = {
	monarch = {
 		name = "Bolar"
		dynasty = "gro-Shamar"
		birth_date = 961.1.1
		adm = 2
		dip = 0
		mil = 2
		female = yes
    }
}

1074.1.1 = {
	monarch = {
 		name = "Tumuthag"
		dynasty = "gro-Dular"
		birth_date = 1038.1.1
		adm = 4
		dip = 1
		mil = 4
    }
}

1159.1.1 = {
	monarch = {
 		name = "Norgol"
		dynasty = "gro-Mudush"
		birth_date = 1141.1.1
		adm = 2
		dip = 0
		mil = 0
    }
}

1250.1.1 = {
	monarch = {
 		name = "Ulghesh"
		dynasty = "gro-Shuzug"
		birth_date = 1221.1.1
		adm = 0
		dip = 6
		mil = 4
    }
}

1319.1.1 = {
	monarch = {
 		name = "Ogrumbu"
		dynasty = "gro-Ugurz"
		birth_date = 1275.1.1
		adm = 5
		dip = 5
		mil = 1
    }
}

1408.1.1 = {
	monarch = {
 		name = "Hazbur"
		dynasty = "gro-Yambagorn"
		birth_date = 1373.1.1
		adm = 4
		dip = 5
		mil = 4
    }
}

1449.1.1 = {
	monarch = {
 		name = "Charlvain"
		dynasty = "gro-Dugtosh"
		birth_date = 1413.1.1
		adm = 2
		dip = 4
		mil = 1
    }
}

1544.1.1 = {
	monarch = {
 		name = "Ulang"
		dynasty = "gro-Ulghesh"
		birth_date = 1509.1.1
		adm = 0
		dip = 3
		mil = 4
    }
}

1600.1.1 = {
	monarch = {
 		name = "Ogozod"
		dynasty = "gro-Urzog"
		birth_date = 1557.1.1
		adm = 5
		dip = 2
		mil = 1
    }
}

1692.1.1 = {
	monarch = {
 		name = "Gwilherm"
		dynasty = "gro-Gruzgob"
		birth_date = 1647.1.1
		adm = 0
		dip = 4
		mil = 3
    }
}

1746.1.1 = {
	monarch = {
 		name = "Burzura"
		dynasty = "gro-Gorbakh"
		birth_date = 1693.1.1
		adm = 5
		dip = 3
		mil = 6
    }
}

1833.1.1 = {
	monarch = {
 		name = "Khamagash"
		dynasty = "gro-Kharsthun"
		birth_date = 1798.1.1
		adm = 4
		dip = 2
		mil = 3
    }
}

1916.1.1 = {
	monarch = {
 		name = "Adkul"
		dynasty = "gro-Rokut"
		birth_date = 1880.1.1
		adm = 2
		dip = 1
		mil = 0
		female = yes
    }
}

1965.1.1 = {
	monarch = {
 		name = "Ulgush"
		dynasty = "gro-Torg"
		birth_date = 1917.1.1
		adm = 0
		dip = 0
		mil = 3
		female = yes
    }
}

2010.1.1 = {
	monarch = {
 		name = "Nazhag"
		dynasty = "gro-Ugurz"
		birth_date = 1971.1.1
		adm = 5
		dip = 0
		mil = 0
		female = yes
    }
}

2080.1.1 = {
	monarch = {
 		name = "Khagra"
		dynasty = "gro-Nabshuq"
		birth_date = 2053.1.1
		adm = 4
		dip = 6
		mil = 4
    }
}

2161.1.1 = {
	monarch = {
 		name = "Yatul"
		dynasty = "gro-Shagrol"
		birth_date = 2110.1.1
		adm = 2
		dip = 5
		mil = 0
		female = yes
    }
}

2259.1.1 = {
	monarch = {
 		name = "Ugrush"
		dynasty = "gro-Lashbag"
		birth_date = 2209.1.1
		adm = 4
		dip = 6
		mil = 2
		female = yes
    }
}

2326.1.1 = {
	monarch = {
 		name = "Narzdush"
		dynasty = "gro-Lashbag"
		birth_date = 2306.1.1
		adm = 2
		dip = 5
		mil = 5
		female = yes
    }
}

2369.1.1 = {
	monarch = {
 		name = "Uugus"
		dynasty = "gro-Ragnast"
		birth_date = 2339.1.1
		adm = 0
		dip = 5
		mil = 2
    }
}

2433.1.1 = {
	monarch = {
 		name = "Orgdugrash"
		dynasty = "gro-Sneg"
		birth_date = 2394.1.1
		adm = 6
		dip = 4
		mil = 6
    }
}

