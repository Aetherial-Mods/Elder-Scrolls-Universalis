government = tribal
government_rank = 1
mercantilism = 1
technology_group = orsimer_tg
religion = malacath_cult
primary_culture = goblin
capital = 5319

54.1.1 = {
	monarch = {
 		name = "Leestriof"
		dynasty = "Vrit"
		birth_date = 29.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

152.1.1 = {
	monarch = {
 		name = "Gnenia"
		dynasty = "Digran"
		birth_date = 128.1.1
		adm = 6
		dip = 3
		mil = 5
		female = yes
    }
}

250.1.1 = {
	monarch = {
 		name = "Cealk"
		dynasty = "Allu"
		birth_date = 222.1.1
		adm = 4
		dip = 2
		mil = 1
    }
}

323.1.1 = {
	monarch = {
 		name = "Vrenteesz"
		dynasty = "Vignu"
		birth_date = 275.1.1
		adm = 3
		dip = 1
		mil = 5
    }
}

405.1.1 = {
	monarch = {
 		name = "Uild"
		dynasty = "Irtogis"
		birth_date = 378.1.1
		adm = 1
		dip = 1
		mil = 2
    }
}

463.1.1 = {
	monarch = {
 		name = "Xect"
		dynasty = "Murkmaw"
		birth_date = 445.1.1
		adm = 6
		dip = 0
		mil = 5
    }
}

546.1.1 = {
	monarch = {
 		name = "Cealk"
		dynasty = "Brokeneye"
		birth_date = 499.1.1
		adm = 4
		dip = 6
		mil = 2
    }
}

589.1.1 = {
	monarch = {
 		name = "Gnogiefzi"
		dynasty = "Jugran"
		birth_date = 555.1.1
		adm = 3
		dip = 5
		mil = 6
		female = yes
    }
}

635.1.1 = {
	monarch = {
 		name = "Uild"
		dynasty = "Toadwart"
		birth_date = 600.1.1
		adm = 5
		dip = 6
		mil = 0
    }
}

693.1.1 = {
	monarch = {
 		name = "Bluimziang"
		dynasty = "Slugmaw"
		birth_date = 651.1.1
		adm = 3
		dip = 6
		mil = 4
    }
}

751.1.1 = {
	monarch = {
 		name = "Kasriosa"
		dynasty = "Grimeleg"
		birth_date = 705.1.1
		adm = 1
		dip = 5
		mil = 1
		female = yes
    }
}

799.1.1 = {
	monarch = {
 		name = "Aglisb"
		dynasty = "Pinkeye"
		birth_date = 767.1.1
		adm = 4
		dip = 6
		mil = 3
    }
}

837.1.1 = {
	monarch = {
 		name = "Aliohe"
		dynasty = "Cit"
		birth_date = 802.1.1
		adm = 2
		dip = 5
		mil = 0
		female = yes
    }
}

915.1.1 = {
	monarch = {
 		name = "Wryrd"
		dynasty = "Smugeyes"
		birth_date = 892.1.1
		adm = 4
		dip = 0
		mil = 1
    }
}

983.1.1 = {
	monarch = {
 		name = "Kasriosa"
		dynasty = "Vuggarg"
		birth_date = 965.1.1
		adm = 2
		dip = 6
		mil = 5
		female = yes
    }
}

1061.1.1 = {
	monarch = {
 		name = "Fufse"
		dynasty = "Dujeb"
		birth_date = 1011.1.1
		adm = 0
		dip = 5
		mil = 1
		female = yes
    }
}

1153.1.1 = {
	monarch = {
 		name = "Rorierd"
		dynasty = "Kradgug"
		birth_date = 1107.1.1
		adm = 6
		dip = 4
		mil = 5
    }
}

1197.1.1 = {
	monarch = {
 		name = "Broifzai"
		dynasty = "Gubak"
		birth_date = 1174.1.1
		adm = 4
		dip = 3
		mil = 2
		female = yes
    }
}

1257.1.1 = {
	monarch = {
 		name = "Forlags"
		dynasty = "Dargra"
		birth_date = 1227.1.1
		adm = 2
		dip = 3
		mil = 5
		female = yes
    }
}

1325.1.1 = {
	monarch = {
 		name = "Friez"
		dynasty = "Ulgruk"
		birth_date = 1276.1.1
		adm = 0
		dip = 2
		mil = 2
    }
}

1392.1.1 = {
	monarch = {
 		name = "Flettiassia"
		dynasty = "Ingrate"
		birth_date = 1373.1.1
		adm = 6
		dip = 1
		mil = 6
		female = yes
    }
}

1483.1.1 = {
	monarch = {
 		name = "Clils"
		dynasty = "Insect"
		birth_date = 1442.1.1
		adm = 1
		dip = 2
		mil = 0
		female = yes
    }
}

1559.1.1 = {
	monarch = {
 		name = "Grotez"
		dynasty = "Gnat"
		birth_date = 1532.1.1
		adm = 6
		dip = 2
		mil = 4
    }
}

1601.1.1 = {
	monarch = {
 		name = "Ovzaas"
		dynasty = "Rudgab"
		birth_date = 1575.1.1
		adm = 3
		dip = 4
		mil = 4
    }
}

1700.1.1 = {
	monarch = {
 		name = "Uilm"
		dynasty = "Dullface"
		birth_date = 1663.1.1
		adm = 5
		dip = 5
		mil = 5
		female = yes
    }
}

1784.1.1 = {
	monarch = {
 		name = "Erkerk"
		dynasty = "No-Eyes"
		birth_date = 1765.1.1
		adm = 3
		dip = 4
		mil = 2
    }
}

1859.1.1 = {
	monarch = {
 		name = "Preehzuz"
		dynasty = "Snoteye"
		birth_date = 1822.1.1
		adm = 1
		dip = 3
		mil = 6
		female = yes
    }
}

1933.1.1 = {
	monarch = {
 		name = "Slold"
		dynasty = "Olgeldir"
		birth_date = 1896.1.1
		adm = 0
		dip = 2
		mil = 2
    }
}

1986.1.1 = {
	monarch = {
 		name = "Zizux"
		dynasty = "Globcheeks"
		birth_date = 1951.1.1
		adm = 5
		dip = 2
		mil = 6
    }
}

2044.1.1 = {
	monarch = {
 		name = "Frasb"
		dynasty = "Veggag"
		birth_date = 1996.1.1
		adm = 3
		dip = 1
		mil = 3
    }
}

2135.1.1 = {
	monarch = {
 		name = "Hionalb"
		dynasty = "Goutflank"
		birth_date = 2095.1.1
		adm = 1
		dip = 0
		mil = 6
    }
}

2174.1.1 = {
	monarch = {
 		name = "Oqee"
		dynasty = "Snailgob"
		birth_date = 2126.1.1
		adm = 0
		dip = 6
		mil = 3
		female = yes
    }
}

2216.1.1 = {
	monarch = {
 		name = "Zizux"
		dynasty = "Mijug"
		birth_date = 2183.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2256.1.1 = {
	monarch = {
 		name = "Nalke"
		dynasty = "Allu"
		birth_date = 2208.1.1
		adm = 0
		dip = 0
		mil = 1
		female = yes
    }
}

2350.1.1 = {
	monarch = {
 		name = "Asuq"
		dynasty = "Ogdas"
		birth_date = 2303.1.1
		adm = 5
		dip = 6
		mil = 5
    }
}

2447.1.1 = {
	monarch = {
 		name = "Bhealk"
		dynasty = "Snailgob"
		birth_date = 2400.1.1
		adm = 3
		dip = 5
		mil = 1
		female = yes
    }
}

