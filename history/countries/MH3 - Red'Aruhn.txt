government = tribal
government_rank = 3
mercantilism = 1
technology_group = exiled_tg
religion = chimer_pantheon
primary_culture = ashlander_chimer
capital = 957

54.1.1 = {
	monarch = {
 		name = "Mamusa"
		dynasty = "Redoran"
		birth_date = 23.1.1
		adm = 4
		dip = 5
		mil = 1
		female = yes
    }
}

130.1.1 = {
	monarch = {
 		name = "Assamma-Idan"
		dynasty = "Redoran"
		birth_date = 84.1.1
		adm = 2
		dip = 4
		mil = 5
    }
}

218.1.1 = {
	monarch = {
 		name = "Zalabelk"
		dynasty = "Redoran"
		birth_date = 184.1.1
		adm = 0
		dip = 3
		mil = 1
    }
}

256.1.1 = {
	monarch = {
 		name = "Seba"
		dynasty = "Redoran"
		birth_date = 223.1.1
		adm = 2
		dip = 4
		mil = 3
		female = yes
    }
}

349.1.1 = {
	monarch = {
 		name = "Kummi-Namus"
		dynasty = "Redoran"
		birth_date = 317.1.1
		adm = 1
		dip = 3
		mil = 0
    }
}

413.1.1 = {
	monarch = {
 		name = "Shannat"
		dynasty = "Redoran"
		birth_date = 395.1.1
		adm = 6
		dip = 3
		mil = 3
    }
}

484.1.1 = {
	monarch = {
 		name = "Man-Ilu"
		dynasty = "Redoran"
		birth_date = 457.1.1
		adm = 4
		dip = 2
		mil = 0
		female = yes
    }
}

536.1.1 = {
	monarch = {
 		name = "Benudni"
		dynasty = "Redoran"
		birth_date = 513.1.1
		adm = 2
		dip = 1
		mil = 4
		female = yes
    }
}

594.1.1 = {
	monarch = {
 		name = "Zanat"
		dynasty = "Redoran"
		birth_date = 553.1.1
		adm = 1
		dip = 0
		mil = 0
    }
}

686.1.1 = {
	monarch = {
 		name = "Shanit"
		dynasty = "Redoran"
		birth_date = 641.1.1
		adm = 6
		dip = 6
		mil = 4
    }
}

740.1.1 = {
	monarch = {
 		name = "Mabarrabael"
		dynasty = "Redoran"
		birth_date = 703.1.1
		adm = 4
		dip = 6
		mil = 1
    }
}

827.1.1 = {
	monarch = {
 		name = "Benudni"
		dynasty = "Redoran"
		birth_date = 807.1.1
		adm = 6
		dip = 0
		mil = 2
		female = yes
    }
}

910.1.1 = {
	monarch = {
 		name = "Zaba"
		dynasty = "Redoran"
		birth_date = 862.1.1
		adm = 4
		dip = 6
		mil = 6
		female = yes
    }
}

975.1.1 = {
	monarch = {
 		name = "Dissu"
		dynasty = "Redoran"
		birth_date = 927.1.1
		adm = 2
		dip = 5
		mil = 2
		female = yes
    }
}

1020.1.1 = {
	monarch = {
 		name = "Zebdusipal"
		dynasty = "Redoran"
		birth_date = 984.1.1
		adm = 1
		dip = 5
		mil = 6
    }
}

1080.1.1 = {
	monarch = {
 		name = "Shali"
		dynasty = "Redoran"
		birth_date = 1045.1.1
		adm = 6
		dip = 4
		mil = 3
		female = yes
    }
}

1131.1.1 = {
	monarch = {
 		name = "Malay"
		dynasty = "Redoran"
		birth_date = 1108.1.1
		adm = 4
		dip = 3
		mil = 6
    }
}

1183.1.1 = {
	monarch = {
 		name = "Balur"
		dynasty = "Redoran"
		birth_date = 1133.1.1
		adm = 2
		dip = 2
		mil = 3
    }
}

1248.1.1 = {
	monarch = {
 		name = "Zebba"
		dynasty = "Redoran"
		birth_date = 1197.1.1
		adm = 1
		dip = 1
		mil = 0
    }
}

1333.1.1 = {
	monarch = {
 		name = "Shargon"
		dynasty = "Redoran"
		birth_date = 1298.1.1
		adm = 3
		dip = 3
		mil = 1
    }
}

1371.1.1 = {
	monarch = {
 		name = "Mi-Ilu"
		dynasty = "Redoran"
		birth_date = 1345.1.1
		adm = 1
		dip = 2
		mil = 5
		female = yes
    }
}

1455.1.1 = {
	monarch = {
 		name = "Shin"
		dynasty = "Redoran"
		birth_date = 1427.1.1
		adm = 6
		dip = 1
		mil = 1
    }
}

1497.1.1 = {
	monarch = {
 		name = "Milynea"
		dynasty = "Redoran"
		birth_date = 1445.1.1
		adm = 4
		dip = 0
		mil = 5
		female = yes
    }
}

1543.1.1 = {
	monarch = {
 		name = "Dakin"
		dynasty = "Redoran"
		birth_date = 1525.1.1
		adm = 3
		dip = 6
		mil = 2
    }
}

1642.1.1 = {
	monarch = {
 		name = "Zelay"
		dynasty = "Redoran"
		birth_date = 1611.1.1
		adm = 1
		dip = 6
		mil = 5
    }
}

1740.1.1 = {
	monarch = {
 		name = "Ilasour"
		dynasty = "Redoran"
		birth_date = 1712.1.1
		adm = 0
		dip = 2
		mil = 6
    }
}

1805.1.1 = {
	monarch = {
 		name = "Salmus"
		dynasty = "Redoran"
		birth_date = 1787.1.1
		adm = 5
		dip = 1
		mil = 2
    }
}

1901.1.1 = {
	monarch = {
 		name = "Kanud"
		dynasty = "Redoran"
		birth_date = 1855.1.1
		adm = 4
		dip = 1
		mil = 6
    }
}

1982.1.1 = {
	monarch = {
 		name = "Assamanu"
		dynasty = "Redoran"
		birth_date = 1960.1.1
		adm = 2
		dip = 0
		mil = 3
		female = yes
    }
}

2066.1.1 = {
	monarch = {
 		name = "Vabbar"
		dynasty = "Redoran"
		birth_date = 2030.1.1
		adm = 0
		dip = 6
		mil = 6
		female = yes
    }
}

2148.1.1 = {
	monarch = {
 		name = "Salmat"
		dynasty = "Redoran"
		birth_date = 2128.1.1
		adm = 5
		dip = 5
		mil = 3
    }
}

2245.1.1 = {
	monarch = {
 		name = "Kanud"
		dynasty = "Redoran"
		birth_date = 2209.1.1
		adm = 4
		dip = 4
		mil = 0
    }
}

2284.1.1 = {
	monarch = {
 		name = "Assamanu"
		dynasty = "Redoran"
		birth_date = 2251.1.1
		adm = 6
		dip = 6
		mil = 1
		female = yes
    }
}

2325.1.1 = {
	monarch = {
 		name = "Yeherradad"
		dynasty = "Redoran"
		birth_date = 2303.1.1
		adm = 4
		dip = 5
		mil = 5
    }
}

2390.1.1 = {
	monarch = {
 		name = "Assi"
		dynasty = "Redoran"
		birth_date = 2370.1.1
		adm = 2
		dip = 4
		mil = 1
		female = yes
    }
}

2448.1.1 = {
	monarch = {
 		name = "Zabamund"
		dynasty = "Redoran"
		birth_date = 2421.1.1
		adm = 0
		dip = 3
		mil = 5
    }
}

2490.1.1 = {
	monarch = {
 		name = "Sargon"
		dynasty = "Redoran"
		birth_date = 2443.1.1
		adm = 5
		dip = 2
		mil = 2
    }
}

