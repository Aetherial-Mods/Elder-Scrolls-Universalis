government = native
government_rank = 1
mercantilism = 1
technology_group = daedric_tg
religion = molag_bal_cult
primary_culture = daedroth
capital = 4287

54.1.1 = {
	monarch = {
 		name = "Risma"
		dynasty = "Darr-Vylvia"
		birth_date = 21.1.1
		adm = 4
		dip = 6
		mil = 5
		female = yes
    }
}

147.1.1 = {
	monarch = {
 		name = "Geradira"
		dynasty = "Darr-Risia"
		birth_date = 112.1.1
		adm = 2
		dip = 5
		mil = 1
		female = yes
    }
}

217.1.1 = {
	monarch = {
 		name = "Dharus"
		dynasty = "Darr-Zylbea"
		birth_date = 195.1.1
		adm = 4
		dip = 0
		mil = 3
    }
}

310.1.1 = {
	monarch = {
 		name = "Wija"
		dynasty = "Darr-Priresh"
		birth_date = 289.1.1
		adm = 2
		dip = 6
		mil = 0
		female = yes
    }
}

390.1.1 = {
	monarch = {
 		name = "Mirrya"
		dynasty = "Darr-Ranpara"
		birth_date = 344.1.1
		adm = 1
		dip = 5
		mil = 3
		female = yes
    }
}

484.1.1 = {
	monarch = {
 		name = "Rojice"
		dynasty = "Darr-Shyres"
		birth_date = 443.1.1
		adm = 6
		dip = 4
		mil = 0
		female = yes
    }
}

537.1.1 = {
	monarch = {
 		name = "Xenis"
		dynasty = "Darr-Helbise"
		birth_date = 495.1.1
		adm = 4
		dip = 3
		mil = 4
		female = yes
    }
}

635.1.1 = {
	monarch = {
 		name = "Surgrak"
		dynasty = "Darr-Moltris"
		birth_date = 604.1.1
		adm = 2
		dip = 3
		mil = 0
    }
}

699.1.1 = {
	monarch = {
 		name = "Nuvius"
		dynasty = "Darr-Wreteus"
		birth_date = 671.1.1
		adm = 1
		dip = 2
		mil = 4
    }
}

757.1.1 = {
	monarch = {
 		name = "Silbis"
		dynasty = "Darr-Zanaga"
		birth_date = 737.1.1
		adm = 6
		dip = 1
		mil = 1
		female = yes
    }
}

824.1.1 = {
	monarch = {
 		name = "Zamrath"
		dynasty = "Darr-Meben"
		birth_date = 800.1.1
		adm = 1
		dip = 2
		mil = 2
    }
}

899.1.1 = {
	monarch = {
 		name = "Xelphes"
		dynasty = "Darr-Vrymin"
		birth_date = 847.1.1
		adm = 6
		dip = 1
		mil = 6
		female = yes
    }
}

949.1.1 = {
	monarch = {
 		name = "Xekohn"
		dynasty = "Darr-Rintis"
		birth_date = 914.1.1
		adm = 4
		dip = 1
		mil = 2
		female = yes
    }
}

998.1.1 = {
	monarch = {
 		name = "Kalzius"
		dynasty = "Darr-Gnijice"
		birth_date = 955.1.1
		adm = 2
		dip = 0
		mil = 6
    }
}

1075.1.1 = {
	monarch = {
 		name = "Rindya"
		dynasty = "Darr-Granara"
		birth_date = 1035.1.1
		adm = 1
		dip = 6
		mil = 3
		female = yes
    }
}

1166.1.1 = {
	monarch = {
 		name = "Braras"
		dynasty = "Darr-Rintis"
		birth_date = 1126.1.1
		adm = 6
		dip = 5
		mil = 6
    }
}

1216.1.1 = {
	monarch = {
 		name = "Prajus"
		dynasty = "Darr-Vaves"
		birth_date = 1189.1.1
		adm = 4
		dip = 4
		mil = 3
    }
}

1272.1.1 = {
	monarch = {
 		name = "Themys"
		dynasty = "Darr-Vrymien"
		birth_date = 1233.1.1
		adm = 2
		dip = 4
		mil = 0
		female = yes
    }
}

1329.1.1 = {
	monarch = {
 		name = "Rindya"
		dynasty = "Darr-Mirmya"
		birth_date = 1298.1.1
		adm = 4
		dip = 5
		mil = 1
		female = yes
    }
}

1413.1.1 = {
	monarch = {
 		name = "Braras"
		dynasty = "Darr-Beraran"
		birth_date = 1388.1.1
		adm = 3
		dip = 4
		mil = 5
    }
}

1482.1.1 = {
	monarch = {
 		name = "Zanzel"
		dynasty = "Darr-Relbea"
		birth_date = 1435.1.1
		adm = 1
		dip = 3
		mil = 1
		female = yes
    }
}

1544.1.1 = {
	monarch = {
 		name = "Musius"
		dynasty = "Darr-Grapria"
		birth_date = 1504.1.1
		adm = 6
		dip = 3
		mil = 5
    }
}

1614.1.1 = {
	monarch = {
 		name = "Wyria"
		dynasty = "Darr-Syrvash"
		birth_date = 1587.1.1
		adm = 4
		dip = 2
		mil = 2
		female = yes
    }
}

1711.1.1 = {
	monarch = {
 		name = "Wakhis"
		dynasty = "Darr-Hiltana"
		birth_date = 1671.1.1
		adm = 2
		dip = 1
		mil = 5
		female = yes
    }
}

1748.1.1 = {
	monarch = {
 		name = "Datruk"
		dynasty = "Darr-Zapara"
		birth_date = 1730.1.1
		adm = 1
		dip = 0
		mil = 2
    }
}

1808.1.1 = {
	monarch = {
 		name = "Zarius"
		dynasty = "Darr-Syrphas"
		birth_date = 1785.1.1
		adm = 6
		dip = 6
		mil = 6
    }
}

1871.1.1 = {
	monarch = {
 		name = "Kalzak"
		dynasty = "Darr-Grapria"
		birth_date = 1821.1.1
		adm = 1
		dip = 1
		mil = 0
    }
}

1941.1.1 = {
	monarch = {
 		name = "Argaran"
		dynasty = "Darr-Zaves"
		birth_date = 1915.1.1
		adm = 6
		dip = 0
		mil = 4
    }
}

1982.1.1 = {
	monarch = {
 		name = "Xursag"
		dynasty = "Darr-Gnijice"
		birth_date = 1941.1.1
		adm = 4
		dip = 6
		mil = 1
    }
}

2045.1.1 = {
	monarch = {
 		name = "Rahdzeus"
		dynasty = "Darr-Hiltana"
		birth_date = 2013.1.1
		adm = 3
		dip = 5
		mil = 4
    }
}

2110.1.1 = {
	monarch = {
 		name = "Tysesh"
		dynasty = "Darr-Verea"
		birth_date = 2085.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

2174.1.1 = {
	monarch = {
 		name = "Zyrahg"
		dynasty = "Darr-Tysbea"
		birth_date = 2154.1.1
		adm = 6
		dip = 4
		mil = 4
    }
}

2217.1.1 = {
	monarch = {
 		name = "Honrag"
		dynasty = "Darr-Niakes"
		birth_date = 2196.1.1
		adm = 5
		dip = 0
		mil = 5
    }
}

2282.1.1 = {
	monarch = {
 		name = "Phakhia"
		dynasty = "Darr-Wenpea"
		birth_date = 2263.1.1
		adm = 4
		dip = 6
		mil = 1
		female = yes
    }
}

2355.1.1 = {
	monarch = {
 		name = "Zanmes"
		dynasty = "Darr-Priresh"
		birth_date = 2319.1.1
		adm = 2
		dip = 6
		mil = 5
		female = yes
    }
}

2434.1.1 = {
	monarch = {
 		name = "Wasmin"
		dynasty = "Darr-Dragera"
		birth_date = 2388.1.1
		adm = 0
		dip = 5
		mil = 2
		female = yes
    }
}

2479.1.1 = {
	monarch = {
 		name = "Nohrbuk"
		dynasty = "Darr-Grikhia"
		birth_date = 2461.1.1
		adm = 5
		dip = 4
		mil = 5
    }
}

