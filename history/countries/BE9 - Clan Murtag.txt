government = tribal
government_rank = 1
mercantilism = 1
technology_group = orsimer_tg
religion = orcish_pantheon
primary_culture = orsimer
capital = 1391

54.1.1 = {
	monarch = {
 		name = "Gonbush"
		dynasty = "gro-Murtag"
		birth_date = 9.1.1
		adm = 4
		dip = 3
		mil = 4
		female = yes
    }
}

101.1.1 = {
	monarch = {
 		name = "Yag"
		dynasty = "gro-Murtag"
		birth_date = 71.1.1
		adm = 2
		dip = 2
		mil = 0
		female = yes
    }
}

193.1.1 = {
	monarch = {
 		name = "Unrahg"
		dynasty = "gro-Murtag"
		birth_date = 171.1.1
		adm = 0
		dip = 1
		mil = 4
    }
}

270.1.1 = {
	monarch = {
 		name = "Olfrig"
		dynasty = "gro-Murtag"
		birth_date = 223.1.1
		adm = 2
		dip = 3
		mil = 6
    }
}

350.1.1 = {
	monarch = {
 		name = "Kazok"
		dynasty = "gro-Murtag"
		birth_date = 301.1.1
		adm = 0
		dip = 2
		mil = 2
    }
}

448.1.1 = {
	monarch = {
 		name = "Orgdragog"
		dynasty = "gro-Murtag"
		birth_date = 397.1.1
		adm = 6
		dip = 1
		mil = 6
    }
}

525.1.1 = {
	monarch = {
 		name = "Klovag"
		dynasty = "gro-Murtag"
		birth_date = 494.1.1
		adm = 4
		dip = 0
		mil = 3
    }
}

600.1.1 = {
	monarch = {
 		name = "Dumburz"
		dynasty = "gro-Murtag"
		birth_date = 566.1.1
		adm = 2
		dip = 6
		mil = 6
    }
}

644.1.1 = {
	monarch = {
 		name = "Ushat"
		dynasty = "gro-Murtag"
		birth_date = 608.1.1
		adm = 0
		dip = 6
		mil = 3
    }
}

722.1.1 = {
	monarch = {
 		name = "Ordooth"
		dynasty = "gro-Murtag"
		birth_date = 689.1.1
		adm = 6
		dip = 5
		mil = 6
    }
}

817.1.1 = {
	monarch = {
 		name = "Kirgut"
		dynasty = "gro-Murtag"
		birth_date = 767.1.1
		adm = 4
		dip = 4
		mil = 3
    }
}

869.1.1 = {
	monarch = {
 		name = "Duma"
		dynasty = "gro-Murtag"
		birth_date = 850.1.1
		adm = 6
		dip = 5
		mil = 5
    }
}

947.1.1 = {
	monarch = {
 		name = "Ulukhaz"
		dynasty = "gro-Murtag"
		birth_date = 911.1.1
		adm = 4
		dip = 5
		mil = 1
		female = yes
    }
}

1022.1.1 = {
	monarch = {
 		name = "Anbarah"
		dynasty = "gro-Murtag"
		birth_date = 990.1.1
		adm = 2
		dip = 4
		mil = 5
		female = yes
    }
}

1071.1.1 = {
	monarch = {
 		name = "Wort"
		dynasty = "gro-Murtag"
		birth_date = 1043.1.1
		adm = 0
		dip = 3
		mil = 2
    }
}

1111.1.1 = {
	monarch = {
 		name = "Orthuna"
		dynasty = "gro-Murtag"
		birth_date = 1090.1.1
		adm = 6
		dip = 2
		mil = 5
		female = yes
    }
}

1161.1.1 = {
	monarch = {
 		name = "Kurz"
		dynasty = "gro-Murtag"
		birth_date = 1118.1.1
		adm = 4
		dip = 1
		mil = 2
    }
}

1237.1.1 = {
	monarch = {
 		name = "Dushkul"
		dynasty = "gro-Murtag"
		birth_date = 1215.1.1
		adm = 2
		dip = 1
		mil = 6
    }
}

1329.1.1 = {
	monarch = {
 		name = "Vunp"
		dynasty = "gro-Murtag"
		birth_date = 1311.1.1
		adm = 0
		dip = 0
		mil = 2
    }
}

1393.1.1 = {
	monarch = {
 		name = "Osgulug"
		dynasty = "gro-Murtag"
		birth_date = 1368.1.1
		adm = 2
		dip = 1
		mil = 4
    }
}

1431.1.1 = {
	monarch = {
 		name = "Groddi"
		dynasty = "gro-Murtag"
		birth_date = 1393.1.1
		adm = 1
		dip = 0
		mil = 0
		female = yes
    }
}

1491.1.1 = {
	monarch = {
 		name = "Ozrog"
		dynasty = "gro-Murtag"
		birth_date = 1438.1.1
		adm = 6
		dip = 6
		mil = 4
		female = yes
    }
}

1569.1.1 = {
	monarch = {
 		name = "Logbur"
		dynasty = "gro-Murtag"
		birth_date = 1519.1.1
		adm = 4
		dip = 6
		mil = 1
    }
}

1634.1.1 = {
	monarch = {
 		name = "Garmeg"
		dynasty = "gro-Murtag"
		birth_date = 1583.1.1
		adm = 2
		dip = 5
		mil = 4
    }
}

1685.1.1 = {
	monarch = {
 		name = "Yambul"
		dynasty = "gro-Murtag"
		birth_date = 1643.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1751.1.1 = {
	monarch = {
 		name = "Othrag"
		dynasty = "gro-Murtag"
		birth_date = 1703.1.1
		adm = 6
		dip = 3
		mil = 5
		female = yes
    }
}

1845.1.1 = {
	monarch = {
 		name = "Gula"
		dynasty = "gro-Murtag"
		birth_date = 1800.1.1
		adm = 4
		dip = 3
		mil = 1
		female = yes
    }
}

1894.1.1 = {
	monarch = {
 		name = "Ashgel"
		dynasty = "gro-Murtag"
		birth_date = 1865.1.1
		adm = 6
		dip = 4
		mil = 3
		female = yes
    }
}

1936.1.1 = {
	monarch = {
 		name = "Yam"
		dynasty = "gro-Murtag"
		birth_date = 1918.1.1
		adm = 4
		dip = 3
		mil = 6
    }
}

1989.1.1 = {
	monarch = {
 		name = "Azhnura"
		dynasty = "gro-Murtag"
		birth_date = 1964.1.1
		adm = 2
		dip = 2
		mil = 3
		female = yes
    }
}

2074.1.1 = {
	monarch = {
 		name = "Yggruk"
		dynasty = "gro-Murtag"
		birth_date = 2024.1.1
		adm = 1
		dip = 1
		mil = 0
    }
}

2142.1.1 = {
	monarch = {
 		name = "Razbela"
		dynasty = "gro-Murtag"
		birth_date = 2114.1.1
		adm = 6
		dip = 1
		mil = 3
		female = yes
    }
}

2210.1.1 = {
	monarch = {
 		name = "Gurum"
		dynasty = "gro-Murtag"
		birth_date = 2172.1.1
		adm = 4
		dip = 0
		mil = 0
		female = yes
    }
}

2260.1.1 = {
	monarch = {
 		name = "Ghagrub"
		dynasty = "gro-Murtag"
		birth_date = 2210.1.1
		adm = 2
		dip = 6
		mil = 4
    }
}

2337.1.1 = {
	monarch = {
 		name = "Yatur"
		dynasty = "gro-Murtag"
		birth_date = 2315.1.1
		adm = 1
		dip = 5
		mil = 0
    }
}

2397.1.1 = {
	monarch = {
 		name = "Ranarsh"
		dynasty = "gro-Murtag"
		birth_date = 2365.1.1
		adm = 3
		dip = 6
		mil = 2
		female = yes
    }
}

2482.1.1 = {
	monarch = {
 		name = "Lozwug"
		dynasty = "gro-Murtag"
		birth_date = 2444.1.1
		adm = 1
		dip = 6
		mil = 6
    }
}

