government = republic
government_rank = 1
mercantilism = 1
technology_group = northern_tg
religion = druidism
primary_culture = breton
capital = 1394

54.1.1 = {
	monarch = {
 		name = "Arnand"
		dynasty = "Falon"
		birth_date = 23.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

110.1.1 = {
	monarch = {
 		name = "Rodmand"
		dynasty = "Gulitte"
		birth_date = 80.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

161.1.1 = {
	monarch = {
 		name = "Louna"
		dynasty = "Fralinie"
		birth_date = 117.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

230.1.1 = {
	monarch = {
 		name = "Erj"
		dynasty = "Derone"
		birth_date = 200.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

281.1.1 = {
	monarch = {
 		name = "Aresin"
		dynasty = "Pavelle"
		birth_date = 239.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

336.1.1 = {
	monarch = {
 		name = "Rocheric"
		dynasty = "Arcole"
		birth_date = 310.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

395.1.1 = {
	monarch = {
 		name = "Kip"
		dynasty = "DuBois"
		birth_date = 357.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

466.1.1 = {
	monarch = {
 		name = "Rozenn"
		dynasty = "Hawrond"
		birth_date = 444.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

533.1.1 = {
	monarch = {
 		name = "Lenet"
		dynasty = "Nisirrien"
		birth_date = 498.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

626.1.1 = {
	monarch = {
 		name = "Fabrelle"
		dynasty = "Normar"
		birth_date = 583.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

705.1.1 = {
	monarch = {
 		name = "Artura"
		dynasty = "Jend"
		birth_date = 656.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

765.1.1 = {
	monarch = {
 		name = "Roumiet"
		dynasty = "Lielle"
		birth_date = 717.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

822.1.1 = {
	monarch = {
 		name = "Lydi"
		dynasty = "Babiloine"
		birth_date = 798.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

870.1.1 = {
	monarch = {
 		name = "Eveline"
		dynasty = "Vienne"
		birth_date = 822.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

926.1.1 = {
	monarch = {
 		name = "Armandine"
		dynasty = "Zylippe"
		birth_date = 881.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

970.1.1 = {
	monarch = {
 		name = "Ferrand"
		dynasty = "QuintinRangouze"
		birth_date = 946.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1042.1.1 = {
	monarch = {
 		name = "Aurore"
		dynasty = "Demarie"
		birth_date = 992.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1123.1.1 = {
	monarch = {
 		name = "Sernays"
		dynasty = "Hall"
		birth_date = 1098.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1220.1.1 = {
	monarch = {
 		name = "Maelys"
		dynasty = "Winvale"
		birth_date = 1192.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1318.1.1 = {
	monarch = {
 		name = "Fiesque"
		dynasty = "Roreles"
		birth_date = 1297.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1417.1.1 = {
	monarch = {
 		name = "Baragon"
		dynasty = "Dantaine"
		birth_date = 1390.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1516.1.1 = {
	monarch = {
 		name = "Sandire"
		dynasty = "Tibbin"
		birth_date = 1491.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1593.1.1 = {
	monarch = {
 		name = "Leseph"
		dynasty = "Chachere"
		birth_date = 1547.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1631.1.1 = {
	monarch = {
 		name = "Stephile"
		dynasty = "Leraud"
		birth_date = 1584.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1682.1.1 = {
	monarch = {
 		name = "Marciele"
		dynasty = "Emain"
		birth_date = 1658.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1720.1.1 = {
	monarch = {
 		name = "Franque"
		dynasty = "Broles"
		birth_date = 1681.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1810.1.1 = {
	monarch = {
 		name = "Belchimac"
		dynasty = "Danise"
		birth_date = 1787.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1864.1.1 = {
	monarch = {
 		name = "Stenwick"
		dynasty = "Selone"
		birth_date = 1841.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1916.1.1 = {
	monarch = {
 		name = "Luc"
		dynasty = "Phien"
		birth_date = 1872.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1992.1.1 = {
	monarch = {
 		name = "Franck"
		dynasty = "Cottret"
		birth_date = 1963.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2045.1.1 = {
	monarch = {
 		name = "Beddi"
		dynasty = "Donze"
		birth_date = 1998.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

2117.1.1 = {
	monarch = {
 		name = "Gaillard"
		dynasty = "Carsitien"
		birth_date = 2099.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2185.1.1 = {
	monarch = {
 		name = "Blanche"
		dynasty = "Antienne"
		birth_date = 2143.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

2276.1.1 = {
	monarch = {
 		name = "Tetra"
		dynasty = "Ashcroft"
		birth_date = 2246.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2329.1.1 = {
	monarch = {
 		name = "Marcel"
		dynasty = "Gousse"
		birth_date = 2307.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2365.1.1 = {
	monarch = {
 		name = "Gabryel"
		dynasty = "Tauzin"
		birth_date = 2313.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2402.1.1 = {
	monarch = {
 		name = "Bethany"
		dynasty = "Laul"
		birth_date = 2379.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

2448.1.1 = {
	monarch = {
 		name = "Syndelius"
		dynasty = "Chriane"
		birth_date = 2404.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2488.1.1 = {
	monarch = {
 		name = "Marane"
		dynasty = "Spenard"
		birth_date = 2470.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

