government = native
government_rank = 1
mercantilism = 1
technology_group = atmora_tg
religion = dragon_cult
primary_culture = glacial
capital = 2450

54.1.1 = {
	monarch = {
 		name = "Frodmeldof"
		dynasty = "Mesmannath"
		birth_date = 26.1.1
		adm = 0
		dip = 6
		mil = 2
		female = yes
    }
}

109.1.1 = {
	monarch = {
 		name = "Frurramth"
		dynasty = "Frargil"
		birth_date = 62.1.1
		adm = 2
		dip = 0
		mil = 3
    }
}

193.1.1 = {
	monarch = {
 		name = "Gechem"
		dynasty = "Finremth"
		birth_date = 140.1.1
		adm = 0
		dip = 6
		mil = 0
    }
}

274.1.1 = {
	monarch = {
 		name = "Keld"
		dynasty = "Gechem"
		birth_date = 226.1.1
		adm = 5
		dip = 6
		mil = 4
    }
}

355.1.1 = {
	monarch = {
 		name = "Rhilg"
		dynasty = "Keld"
		birth_date = 304.1.1
		adm = 3
		dip = 5
		mil = 0
    }
}

403.1.1 = {
	monarch = {
 		name = "Ratmam"
		dynasty = "Rhilg"
		birth_date = 358.1.1
		adm = 2
		dip = 4
		mil = 4
    }
}

464.1.1 = {
	monarch = {
 		name = "Jochin"
		dynasty = "Valben"
		birth_date = 411.1.1
		adm = 0
		dip = 3
		mil = 1
    }
}

562.1.1 = {
	monarch = {
 		name = "Gol"
		dynasty = "Skylskak"
		birth_date = 543.1.1
		adm = 5
		dip = 2
		mil = 4
    }
}

625.1.1 = {
	monarch = {
 		name = "Stindurilg"
		dynasty = "Finwoskr"
		birth_date = 605.1.1
		adm = 3
		dip = 2
		mil = 1
    }
}

724.1.1 = {
	monarch = {
 		name = "Brotiedr"
		dynasty = "Deesmal"
		birth_date = 694.1.1
		adm = 5
		dip = 3
		mil = 3
		female = yes
    }
}

802.1.1 = {
	monarch = {
 		name = "Jochin"
		dynasty = "Frowunnirr"
		birth_date = 755.1.1
		adm = 4
		dip = 2
		mil = 6
    }
}

872.1.1 = {
	monarch = {
 		name = "Told"
		dynasty = "Thugnom"
		birth_date = 842.1.1
		adm = 2
		dip = 1
		mil = 3
    }
}

956.1.1 = {
	monarch = {
 		name = "Feegnerr"
		dynasty = "Frechom"
		birth_date = 911.1.1
		adm = 0
		dip = 0
		mil = 0
    }
}

1000.1.1 = {
	monarch = {
 		name = "Thol"
		dynasty = "Frowunnirr"
		birth_date = 962.1.1
		adm = 5
		dip = 0
		mil = 3
		female = yes
    }
}

1076.1.1 = {
	monarch = {
 		name = "Brodrunlyn"
		dynasty = "Moegianl"
		birth_date = 1036.1.1
		adm = 3
		dip = 6
		mil = 0
		female = yes
    }
}

1112.1.1 = {
	monarch = {
 		name = "Told"
		dynasty = "Mesmannath"
		birth_date = 1083.1.1
		adm = 2
		dip = 5
		mil = 3
    }
}

1201.1.1 = {
	monarch = {
 		name = "Hregjaf"
		dynasty = "Fosmil"
		birth_date = 1149.1.1
		adm = 0
		dip = 4
		mil = 0
		female = yes
    }
}

1243.1.1 = {
	monarch = {
 		name = "Rolrifodr"
		dynasty = "Finwoskr"
		birth_date = 1205.1.1
		adm = 2
		dip = 6
		mil = 2
		female = yes
    }
}

1301.1.1 = {
	monarch = {
 		name = "Svadrinlind"
		dynasty = "Boechyrom"
		birth_date = 1273.1.1
		adm = 0
		dip = 5
		mil = 5
		female = yes
    }
}

1348.1.1 = {
	monarch = {
 		name = "Folsgold"
		dynasty = "Gugnorr"
		birth_date = 1297.1.1
		adm = 5
		dip = 4
		mil = 2
    }
}

1430.1.1 = {
	monarch = {
 		name = "Klolsgonl"
		dynasty = "Rholl"
		birth_date = 1409.1.1
		adm = 4
		dip = 3
		mil = 6
    }
}

1515.1.1 = {
	monarch = {
 		name = "Kamen"
		dynasty = "Feegnerr"
		birth_date = 1483.1.1
		adm = 6
		dip = 5
		mil = 1
		female = yes
    }
}

1565.1.1 = {
	monarch = {
 		name = "Hjudr"
		dynasty = "Frelsgel"
		birth_date = 1514.1.1
		adm = 1
		dip = 6
		mil = 2
		female = yes
    }
}

1615.1.1 = {
	monarch = {
 		name = "Kleelim"
		dynasty = "Sudinl"
		birth_date = 1590.1.1
		adm = 6
		dip = 5
		mil = 6
    }
}

1655.1.1 = {
	monarch = {
 		name = "Sothes"
		dynasty = "Budek"
		birth_date = 1618.1.1
		adm = 5
		dip = 4
		mil = 3
		female = yes
    }
}

1749.1.1 = {
	monarch = {
 		name = "Hjeijes"
		dynasty = "Gugnorr"
		birth_date = 1715.1.1
		adm = 3
		dip = 3
		mil = 6
		female = yes
    }
}

1784.1.1 = {
	monarch = {
 		name = "Skolnuf"
		dynasty = "Keewogik"
		birth_date = 1735.1.1
		adm = 1
		dip = 3
		mil = 3
		female = yes
    }
}

1869.1.1 = {
	monarch = {
 		name = "Salyld"
		dynasty = "Stegath"
		birth_date = 1849.1.1
		adm = 2
		dip = 0
		mil = 1
		female = yes
    }
}

1958.1.1 = {
	monarch = {
 		name = "Thirmoskr"
		dynasty = "Rerlok"
		birth_date = 1916.1.1
		adm = 5
		dip = 1
		mil = 3
    }
}

2009.1.1 = {
	monarch = {
 		name = "Sudinl"
		dynasty = "Fosmil"
		birth_date = 1961.1.1
		adm = 3
		dip = 0
		mil = 0
    }
}

2052.1.1 = {
	monarch = {
 		name = "Skolnuf"
		dynasty = "Sirlannen"
		birth_date = 2005.1.1
		adm = 5
		dip = 2
		mil = 2
		female = yes
    }
}

2143.1.1 = {
	monarch = {
 		name = "Delorr"
		dynasty = "Rondirek"
		birth_date = 2092.1.1
		adm = 3
		dip = 1
		mil = 5
    }
}

2240.1.1 = {
	monarch = {
 		name = "Sogrur"
		dynasty = "Dusnanl"
		birth_date = 2187.1.1
		adm = 0
		dip = 3
		mil = 5
		female = yes
    }
}

2313.1.1 = {
	monarch = {
 		name = "Skendeinl"
		dynasty = "Dyrulleim"
		birth_date = 2291.1.1
		adm = 2
		dip = 4
		mil = 0
    }
}

2397.1.1 = {
	monarch = {
 		name = "Svugjis"
		dynasty = "Kirkom"
		birth_date = 2358.1.1
		adm = 6
		dip = 6
		mil = 0
		female = yes
    }
}

2433.1.1 = {
	monarch = {
 		name = "Frowunnirr"
		dynasty = "Vilskemild"
		birth_date = 2382.1.1
		adm = 4
		dip = 5
		mil = 3
    }
}

2491.1.1 = {
	monarch = {
 		name = "Kadref"
		dynasty = "Boechyrom"
		birth_date = 2454.1.1
		adm = 4
		dip = 2
		mil = 4
		female = yes
    }
}

