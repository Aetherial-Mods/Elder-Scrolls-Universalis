government = tribal
government_rank = 5
mercantilism = 1
technology_group = elsweyr_tg
religion = khajiiti_pantheon
primary_culture = khajiiti
capital = 905

54.1.1 = {
	monarch = {
 		name = "Azurdat"
		dynasty = "Rojbiri"
		birth_date = 23.1.1
		adm = 0
		dip = 1
		mil = 5
    }
}

138.1.1 = {
	monarch = {
 		name = "Ra'khajin"
		dynasty = "Jodhtasarr"
		birth_date = 91.1.1
		adm = 2
		dip = 2
		mil = 6
    }
}

174.1.1 = {
	monarch = {
 		name = "Mamaea"
		dynasty = "Kharzaymar"
		birth_date = 140.1.1
		adm = 1
		dip = 1
		mil = 3
		female = yes
    }
}

228.1.1 = {
	monarch = {
 		name = "Rakhzargo"
		dynasty = "M'arkhu"
		birth_date = 199.1.1
		adm = 6
		dip = 0
		mil = 6
    }
}

272.1.1 = {
	monarch = {
 		name = "Matbia"
		dynasty = "Zan'fazir"
		birth_date = 243.1.1
		adm = 4
		dip = 0
		mil = 3
		female = yes
    }
}

311.1.1 = {
	monarch = {
 		name = "Fa'ren-dar"
		dynasty = "Shoirr"
		birth_date = 266.1.1
		adm = 2
		dip = 6
		mil = 0
    }
}

353.1.1 = {
	monarch = {
 		name = "Zaham"
		dynasty = "Ranatasarr"
		birth_date = 309.1.1
		adm = 1
		dip = 5
		mil = 3
    }
}

405.1.1 = {
	monarch = {
 		name = "Rakgul"
		dynasty = "Zarhan"
		birth_date = 358.1.1
		adm = 6
		dip = 4
		mil = 0
    }
}

445.1.1 = {
	monarch = {
 		name = "Mashawi"
		dynasty = "Rojbiri"
		birth_date = 413.1.1
		adm = 4
		dip = 3
		mil = 4
		female = yes
    }
}

511.1.1 = {
	monarch = {
 		name = "Ezzag"
		dynasty = "Baradhari"
		birth_date = 473.1.1
		adm = 6
		dip = 5
		mil = 5
    }
}

551.1.1 = {
	monarch = {
 		name = "Zalayza"
		dynasty = "Urjoanir"
		birth_date = 525.1.1
		adm = 4
		dip = 4
		mil = 2
		female = yes
    }
}

627.1.1 = {
	monarch = {
 		name = "Fezez"
		dynasty = "Ranrjo"
		birth_date = 575.1.1
		adm = 2
		dip = 3
		mil = 5
    }
}

705.1.1 = {
	monarch = {
 		name = "Zarbidiran"
		dynasty = "Ma'jahirr"
		birth_date = 679.1.1
		adm = 5
		dip = 4
		mil = 1
    }
}

779.1.1 = {
	monarch = {
 		name = "J'zuraar"
		dynasty = "Ma'siri"
		birth_date = 753.1.1
		adm = 3
		dip = 4
		mil = 4
    }
}

819.1.1 = {
	monarch = {
 		name = "Miglah"
		dynasty = "Rokaron"
		birth_date = 800.1.1
		adm = 4
		dip = 1
		mil = 2
		female = yes
    }
}

872.1.1 = {
	monarch = {
 		name = "Fayna"
		dynasty = "Barasopor"
		birth_date = 837.1.1
		adm = 2
		dip = 0
		mil = 6
		female = yes
    }
}

967.1.1 = {
	monarch = {
 		name = "Zaydi"
		dynasty = "Zan'esi"
		birth_date = 944.1.1
		adm = 1
		dip = 6
		mil = 3
		female = yes
    }
}

1041.1.1 = {
	monarch = {
 		name = "Shiba-dra"
		dynasty = "Rahkbusihr"
		birth_date = 1006.1.1
		adm = 3
		dip = 0
		mil = 4
		female = yes
    }
}

1122.1.1 = {
	monarch = {
 		name = "Lakurr-ra"
		dynasty = "Zarhan"
		birth_date = 1075.1.1
		adm = 1
		dip = 0
		mil = 1
    }
}

1185.1.1 = {
	monarch = {
 		name = "Rihahaz"
		dynasty = "Daro'urabi"
		birth_date = 1158.1.1
		adm = 6
		dip = 6
		mil = 4
    }
}

1281.1.1 = {
	monarch = {
 		name = "Mobiba"
		dynasty = "Jodara"
		birth_date = 1242.1.1
		adm = 4
		dip = 5
		mil = 1
		female = yes
    }
}

1376.1.1 = {
	monarch = {
 		name = "Tasam-ri"
		dynasty = "Ranatasarr"
		birth_date = 1348.1.1
		adm = 0
		dip = 6
		mil = 4
    }
}

1443.1.1 = {
	monarch = {
 		name = "Nakra"
		dynasty = "Roudavi"
		birth_date = 1416.1.1
		adm = 2
		dip = 1
		mil = 5
    }
}

1503.1.1 = {
	monarch = {
 		name = "Jherak"
		dynasty = "Rokaron"
		birth_date = 1483.1.1
		adm = 0
		dip = 0
		mil = 2
    }
}

1584.1.1 = {
	monarch = {
 		name = "Nulk"
		dynasty = "oshsaad"
		birth_date = 1545.1.1
		adm = 5
		dip = 6
		mil = 5
    }
}

1650.1.1 = {
	monarch = {
 		name = "Kanrel"
		dynasty = "Zan'esi"
		birth_date = 1614.1.1
		adm = 4
		dip = 5
		mil = 2
    }
}

1739.1.1 = {
	monarch = {
 		name = "Daifa"
		dynasty = "Hasssien"
		birth_date = 1692.1.1
		adm = 2
		dip = 4
		mil = 6
		female = yes
    }
}

1810.1.1 = {
	monarch = {
 		name = "Turan"
		dynasty = "Tovikmnirn"
		birth_date = 1769.1.1
		adm = 0
		dip = 4
		mil = 2
    }
}

1851.1.1 = {
	monarch = {
 		name = "Sabarapa"
		dynasty = "Solgandihr"
		birth_date = 1832.1.1
		adm = 5
		dip = 3
		mil = 6
		female = yes
    }
}

1947.1.1 = {
	monarch = {
 		name = "Kankhu"
		dynasty = "Barasopor"
		birth_date = 1896.1.1
		adm = 4
		dip = 2
		mil = 3
    }
}

2035.1.1 = {
	monarch = {
 		name = "Dahjarsi"
		dynasty = "Kavandi"
		birth_date = 1987.1.1
		adm = 6
		dip = 3
		mil = 4
		female = yes
    }
}

2124.1.1 = {
	monarch = {
 		name = "Tubidan"
		dynasty = "Akh'irr"
		birth_date = 2093.1.1
		adm = 4
		dip = 3
		mil = 1
    }
}

2193.1.1 = {
	monarch = {
 		name = "Dethadda"
		dynasty = "Ahjgarvi"
		birth_date = 2155.1.1
		adm = 2
		dip = 2
		mil = 4
		female = yes
    }
}

2234.1.1 = {
	monarch = {
 		name = "Uzernurr"
		dynasty = "Jotadirr"
		birth_date = 2205.1.1
		adm = 0
		dip = 1
		mil = 1
    }
}

2326.1.1 = {
	monarch = {
 		name = "Ojik"
		dynasty = "Ranrjo"
		birth_date = 2280.1.1
		adm = 5
		dip = 0
		mil = 5
    }
}

2391.1.1 = {
	monarch = {
 		name = "Kaszo"
		dynasty = "Sarahasin"
		birth_date = 2350.1.1
		adm = 4
		dip = 6
		mil = 1
    }
}

2489.1.1 = {
	monarch = {
 		name = "Dazash"
		dynasty = "Ra'dara"
		birth_date = 2457.1.1
		adm = 2
		dip = 6
		mil = 5
    }
}

