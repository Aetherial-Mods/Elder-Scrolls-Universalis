government = tribal
government_rank = 1
mercantilism = 1
technology_group = elsweyr_tg
religion = khajiiti_pantheon
primary_culture = khajiiti
capital = 928

54.1.1 = {
	monarch = {
 		name = "Chinjarsi"
		dynasty = "Bavagarvi"
		birth_date = 22.1.1
		adm = 2
		dip = 2
		mil = 3
		female = yes
    }
}

152.1.1 = {
	monarch = {
 		name = "Valashi"
		dynasty = "Ri'zaadha"
		birth_date = 132.1.1
		adm = 4
		dip = 3
		mil = 5
		female = yes
    }
}

216.1.1 = {
	monarch = {
 		name = "Nanaglar"
		dynasty = "J'arr"
		birth_date = 164.1.1
		adm = 2
		dip = 3
		mil = 2
    }
}

307.1.1 = {
	monarch = {
 		name = "Yanadzi-jo"
		dynasty = "Bavagarvi"
		birth_date = 283.1.1
		adm = 0
		dip = 2
		mil = 5
		female = yes
    }
}

353.1.1 = {
	monarch = {
 		name = "Nulk"
		dynasty = "Kazasha"
		birth_date = 313.1.1
		adm = 5
		dip = 1
		mil = 2
    }
}

440.1.1 = {
	monarch = {
 		name = "Kanrel"
		dynasty = "Jo'arkhu"
		birth_date = 393.1.1
		adm = 4
		dip = 0
		mil = 5
    }
}

489.1.1 = {
	monarch = {
 		name = "Dagaril-dro"
		dynasty = "K'tabe"
		birth_date = 466.1.1
		adm = 2
		dip = 6
		mil = 2
    }
}

544.1.1 = {
	monarch = {
 		name = "Turo"
		dynasty = "Ahjhir"
		birth_date = 494.1.1
		adm = 0
		dip = 6
		mil = 6
    }
}

581.1.1 = {
	monarch = {
 		name = "Sabarapa"
		dynasty = "Ri'hasta"
		birth_date = 563.1.1
		adm = 5
		dip = 5
		mil = 2
		female = yes
    }
}

619.1.1 = {
	monarch = {
 		name = "Kunira-daro"
		dynasty = "Ab'kheran"
		birth_date = 583.1.1
		adm = 0
		dip = 6
		mil = 4
		female = yes
    }
}

709.1.1 = {
	monarch = {
 		name = "Dahzini"
		dynasty = "Kavandi"
		birth_date = 659.1.1
		adm = 6
		dip = 5
		mil = 1
		female = yes
    }
}

761.1.1 = {
	monarch = {
 		name = "Lusha"
		dynasty = "Barasopor"
		birth_date = 728.1.1
		adm = 4
		dip = 4
		mil = 4
		female = yes
    }
}

817.1.1 = {
	monarch = {
 		name = "Dhanim"
		dynasty = "Rokaron"
		birth_date = 787.1.1
		adm = 2
		dip = 4
		mil = 1
    }
}

872.1.1 = {
	monarch = {
 		name = "Uzrum"
		dynasty = "Helnrjo"
		birth_date = 835.1.1
		adm = 0
		dip = 3
		mil = 5
    }
}

913.1.1 = {
	monarch = {
 		name = "Sanobani"
		dynasty = "Kitanni"
		birth_date = 890.1.1
		adm = 5
		dip = 2
		mil = 1
		female = yes
    }
}

968.1.1 = {
	monarch = {
 		name = "Lazami"
		dynasty = "Shohad"
		birth_date = 930.1.1
		adm = 4
		dip = 1
		mil = 5
		female = yes
    }
}

1064.1.1 = {
	monarch = {
 		name = "Dazash"
		dynasty = "Dar'tesh"
		birth_date = 1016.1.1
		adm = 2
		dip = 0
		mil = 1
    }
}

1150.1.1 = {
	monarch = {
 		name = "Uzarnaym"
		dynasty = "Daro'urabi"
		birth_date = 1100.1.1
		adm = 4
		dip = 2
		mil = 3
    }
}

1203.1.1 = {
	monarch = {
 		name = "Samati-ko"
		dynasty = "Shoava"
		birth_date = 1162.1.1
		adm = 2
		dip = 1
		mil = 0
		female = yes
    }
}

1302.1.1 = {
	monarch = {
 		name = "Zahba"
		dynasty = "Dahshanji"
		birth_date = 1278.1.1
		adm = 0
		dip = 0
		mil = 3
		female = yes
    }
}

1354.1.1 = {
	monarch = {
 		name = "Qa'tesh"
		dynasty = "Kijibiri"
		birth_date = 1313.1.1
		adm = 6
		dip = 6
		mil = 0
    }
}

1429.1.1 = {
	monarch = {
 		name = "Kharjan"
		dynasty = "Zoarjhan"
		birth_date = 1388.1.1
		adm = 4
		dip = 6
		mil = 4
    }
}

1490.1.1 = {
	monarch = {
 		name = "Duzal"
		dynasty = "Ja'sien"
		birth_date = 1472.1.1
		adm = 2
		dip = 5
		mil = 0
    }
}

1530.1.1 = {
	monarch = {
 		name = "Zahanabi"
		dynasty = "Ma'hani"
		birth_date = 1492.1.1
		adm = 0
		dip = 4
		mil = 4
		female = yes
    }
}

1591.1.1 = {
	monarch = {
 		name = "Sedeza"
		dynasty = "Ahjhir"
		birth_date = 1548.1.1
		adm = 6
		dip = 3
		mil = 1
		female = yes
    }
}

1633.1.1 = {
	monarch = {
 		name = "Kharad"
		dynasty = "Ma'siri"
		birth_date = 1598.1.1
		adm = 0
		dip = 4
		mil = 2
    }
}

1703.1.1 = {
	monarch = {
 		name = "Dulan"
		dynasty = "Modumiwa"
		birth_date = 1678.1.1
		adm = 6
		dip = 4
		mil = 6
    }
}

1776.1.1 = {
	monarch = {
 		name = "Kiturr"
		dynasty = "Dahshanji"
		birth_date = 1729.1.1
		adm = 4
		dip = 3
		mil = 2
    }
}

1841.1.1 = {
	monarch = {
 		name = "Esan"
		dynasty = "Kharzaymar"
		birth_date = 1789.1.1
		adm = 2
		dip = 2
		mil = 6
    }
}

1907.1.1 = {
	monarch = {
 		name = "Zajinni"
		dynasty = "Baadjhera"
		birth_date = 1877.1.1
		adm = 0
		dip = 1
		mil = 3
		female = yes
    }
}

2004.1.1 = {
	monarch = {
 		name = "Shadeya"
		dynasty = "Anaihn"
		birth_date = 1956.1.1
		adm = 6
		dip = 0
		mil = 6
		female = yes
    }
}

2100.1.1 = {
	monarch = {
 		name = "Mararanza"
		dynasty = "J'der"
		birth_date = 2082.1.1
		adm = 4
		dip = 0
		mil = 3
		female = yes
    }
}

2192.1.1 = {
	monarch = {
 		name = "Enarri"
		dynasty = "Rawinai"
		birth_date = 2142.1.1
		adm = 2
		dip = 6
		mil = 0
		female = yes
    }
}

2247.1.1 = {
	monarch = {
 		name = "Zaban"
		dynasty = "Keshabhi"
		birth_date = 2209.1.1
		adm = 4
		dip = 0
		mil = 1
    }
}

2328.1.1 = {
	monarch = {
 		name = "Sha'ad"
		dynasty = "Ahjgarvi"
		birth_date = 2301.1.1
		adm = 2
		dip = 6
		mil = 5
		female = yes
    }
}

2396.1.1 = {
	monarch = {
 		name = "Zan"
		dynasty = "Keshabhi"
		birth_date = 2362.1.1
		adm = 1
		dip = 6
		mil = 1
    }
}

2456.1.1 = {
	monarch = {
 		name = "Shausa"
		dynasty = "Barasopor"
		birth_date = 2435.1.1
		adm = 6
		dip = 5
		mil = 5
		female = yes
    }
}

