government = tribal
government_rank = 1
mercantilism = 1
technology_group = exiled_tg
religion = chimer_pantheon
primary_culture = ashlander_chimer
capital = 3946

54.1.1 = {
	monarch = {
 		name = "Ainab"
		dynasty = "Siddurnanit"
		birth_date = 13.1.1
		adm = 2
		dip = 0
		mil = 6
    }
}

126.1.1 = {
	monarch = {
 		name = "Ulibabi"
		dynasty = "Anurnudai"
		birth_date = 76.1.1
		adm = 2
		dip = 4
		mil = 4
		female = yes
    }
}

170.1.1 = {
	monarch = {
 		name = "Zabi"
		dynasty = "Dunsamsi"
		birth_date = 124.1.1
		adm = 1
		dip = 3
		mil = 1
		female = yes
    }
}

256.1.1 = {
	monarch = {
 		name = "Shanud"
		dynasty = "Maliiran"
		birth_date = 226.1.1
		adm = 6
		dip = 2
		mil = 5
    }
}

300.1.1 = {
	monarch = {
 		name = "Man-Ilu"
		dynasty = "Assudnilamat"
		birth_date = 250.1.1
		adm = 1
		dip = 3
		mil = 6
		female = yes
    }
}

356.1.1 = {
	monarch = {
 		name = "Assurdan"
		dynasty = "Asharnalit"
		birth_date = 328.1.1
		adm = 6
		dip = 2
		mil = 3
    }
}

443.1.1 = {
	monarch = {
 		name = "Massarapal"
		dynasty = "Ahalkalun"
		birth_date = 392.1.1
		adm = 4
		dip = 2
		mil = 6
    }
}

492.1.1 = {
	monarch = {
 		name = "Conoon"
		dynasty = "Saddarnuran"
		birth_date = 473.1.1
		adm = 2
		dip = 1
		mil = 3
    }
}

551.1.1 = {
	monarch = {
 		name = "Zelaku"
		dynasty = "Shilansour"
		birth_date = 506.1.1
		adm = 1
		dip = 0
		mil = 0
    }
}

612.1.1 = {
	monarch = {
 		name = "Shallath-Piremus"
		dynasty = "Maliiran"
		birth_date = 565.1.1
		adm = 6
		dip = 6
		mil = 3
		female = yes
    }
}

682.1.1 = {
	monarch = {
 		name = "Anasour"
		dynasty = "Saladnius"
		birth_date = 658.1.1
		adm = 5
		dip = 3
		mil = 4
    }
}

760.1.1 = {
	monarch = {
 		name = "Kanud"
		dynasty = "Enturnabaelul"
		birth_date = 714.1.1
		adm = 4
		dip = 2
		mil = 0
    }
}

859.1.1 = {
	monarch = {
 		name = "Ashumanu"
		dynasty = "Hainterari"
		birth_date = 828.1.1
		adm = 2
		dip = 1
		mil = 4
		female = yes
    }
}

925.1.1 = {
	monarch = {
 		name = "Yeherradad"
		dynasty = "Dun-Ahhe"
		birth_date = 897.1.1
		adm = 0
		dip = 0
		mil = 1
    }
}

1004.1.1 = {
	monarch = {
 		name = "Rawia"
		dynasty = "Shashipal"
		birth_date = 963.1.1
		adm = 5
		dip = 0
		mil = 4
		female = yes
    }
}

1100.1.1 = {
	monarch = {
 		name = "Kushishi"
		dynasty = "Rapli"
		birth_date = 1065.1.1
		adm = 4
		dip = 6
		mil = 1
		female = yes
    }
}

1169.1.1 = {
	monarch = {
 		name = "Ashumanu"
		dynasty = "Eramarellaku"
		birth_date = 1137.1.1
		adm = 2
		dip = 5
		mil = 4
		female = yes
    }
}

1217.1.1 = {
	monarch = {
 		name = "Yassour"
		dynasty = "Zabynatus"
		birth_date = 1197.1.1
		adm = 4
		dip = 6
		mil = 6
    }
}

1291.1.1 = {
	monarch = {
 		name = "Salay"
		dynasty = "Chodala"
		birth_date = 1255.1.1
		adm = 2
		dip = 5
		mil = 3
    }
}

1386.1.1 = {
	monarch = {
 		name = "Yenammu"
		dynasty = "Odin-Ahhe"
		birth_date = 1357.1.1
		adm = 0
		dip = 5
		mil = 6
    }
}

1484.1.1 = {
	monarch = {
 		name = "Sakiran"
		dynasty = "Zansatanit"
		birth_date = 1464.1.1
		adm = 5
		dip = 4
		mil = 3
		female = yes
    }
}

1532.1.1 = {
	monarch = {
 		name = "Maela"
		dynasty = "Mannanalit"
		birth_date = 1490.1.1
		adm = 4
		dip = 3
		mil = 0
		female = yes
    }
}

1631.1.1 = {
	monarch = {
 		name = "Ashuma-Nud"
		dynasty = "Ahanidiran"
		birth_date = 1611.1.1
		adm = 2
		dip = 2
		mil = 3
    }
}

1685.1.1 = {
	monarch = {
 		name = "Yenammu"
		dynasty = "Urshummarnamus"
		birth_date = 1653.1.1
		adm = 0
		dip = 1
		mil = 0
    }
}

1728.1.1 = {
	monarch = {
 		name = "Sakiran"
		dynasty = "Pansamsi"
		birth_date = 1698.1.1
		adm = 5
		dip = 1
		mil = 4
		female = yes
    }
}

1768.1.1 = {
	monarch = {
 		name = "Kashtes"
		dynasty = "Mirpal"
		birth_date = 1743.1.1
		adm = 0
		dip = 2
		mil = 5
    }
}

1816.1.1 = {
	monarch = {
 		name = "Ashuma-Nud"
		dynasty = "Sershurrapal"
		birth_date = 1798.1.1
		adm = 6
		dip = 1
		mil = 2
    }
}

1872.1.1 = {
	monarch = {
 		name = "Kind"
		dynasty = "Chodala"
		birth_date = 1828.1.1
		adm = 4
		dip = 0
		mil = 5
    }
}

1953.1.1 = {
	monarch = {
 		name = "Assallit"
		dynasty = "Salkatanat"
		birth_date = 1911.1.1
		adm = 2
		dip = 0
		mil = 2
    }
}

2000.1.1 = {
	monarch = {
 		name = "Zainat"
		dynasty = "Assillariran"
		birth_date = 1974.1.1
		adm = 0
		dip = 6
		mil = 6
    }
}

2085.1.1 = {
	monarch = {
 		name = "Seldus"
		dynasty = "Eramarellaku"
		birth_date = 2042.1.1
		adm = 6
		dip = 5
		mil = 2
    }
}

2166.1.1 = {
	monarch = {
 		name = "Kind"
		dynasty = "Shashmassamsi"
		birth_date = 2123.1.1
		adm = 4
		dip = 4
		mil = 6
    }
}

2239.1.1 = {
	monarch = {
 		name = "Assallit"
		dynasty = "Dunsamsi"
		birth_date = 2195.1.1
		adm = 2
		dip = 3
		mil = 3
    }
}

2294.1.1 = {
	monarch = {
 		name = "Yanabani"
		dynasty = "Assattadaishah"
		birth_date = 2241.1.1
		adm = 4
		dip = 5
		mil = 4
		female = yes
    }
}

2372.1.1 = {
	monarch = {
 		name = "Seldus"
		dynasty = "Ularshanentus"
		birth_date = 2324.1.1
		adm = 2
		dip = 4
		mil = 1
    }
}

2442.1.1 = {
	monarch = {
 		name = "Zalabelk"
		dynasty = "Benamamat"
		birth_date = 2420.1.1
		adm = 0
		dip = 3
		mil = 4
    }
}

2485.1.1 = {
	monarch = {
 		name = "Seba"
		dynasty = "Atinsabia"
		birth_date = 2437.1.1
		adm = 6
		dip = 2
		mil = 1
		female = yes
    }
}

