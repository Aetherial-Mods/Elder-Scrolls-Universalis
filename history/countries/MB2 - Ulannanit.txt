government = tribal
government_rank = 1
mercantilism = 1
technology_group = exiled_tg
religion = chimer_pantheon
primary_culture = ashlander_chimer
capital = 1004

54.1.1 = {
	monarch = {
 		name = "Tussi"
		dynasty = "Indoril"
		birth_date = 3.1.1
		adm = 3
		dip = 5
		mil = 5
		female = yes
    }
}

113.1.1 = {
	monarch = {
 		name = "Anasour"
		dynasty = "Zabynatus"
		birth_date = 60.1.1
		adm = 2
		dip = 4
		mil = 2
    }
}

148.1.1 = {
	monarch = {
 		name = "Ulisamsi"
		dynasty = "Zainab"
		birth_date = 107.1.1
		adm = 0
		dip = 3
		mil = 5
		female = yes
    }
}

207.1.1 = {
	monarch = {
 		name = "Sal"
		dynasty = "Sanammasour"
		birth_date = 189.1.1
		adm = 5
		dip = 3
		mil = 2
    }
}

249.1.1 = {
	monarch = {
 		name = "Ibasour"
		dynasty = "Urshummarnamus"
		birth_date = 230.1.1
		adm = 3
		dip = 2
		mil = 6
    }
}

347.1.1 = {
	monarch = {
 		name = "Anasour"
		dynasty = "Giladren"
		birth_date = 297.1.1
		adm = 2
		dip = 1
		mil = 2
    }
}

383.1.1 = {
	monarch = {
 		name = "Yanit"
		dynasty = "Timmiriran"
		birth_date = 347.1.1
		adm = 0
		dip = 0
		mil = 6
    }
}

427.1.1 = {
	monarch = {
 		name = "Sakulerib"
		dynasty = "Assutladainab"
		birth_date = 406.1.1
		adm = 2
		dip = 2
		mil = 0
    }
}

518.1.1 = {
	monarch = {
 		name = "Ibasour"
		dynasty = "Assillariran"
		birth_date = 476.1.1
		adm = 0
		dip = 1
		mil = 4
    }
}

566.1.1 = {
	monarch = {
 		name = "Salay"
		dynasty = "Assudnilamat"
		birth_date = 535.1.1
		adm = 5
		dip = 0
		mil = 1
    }
}

659.1.1 = {
	monarch = {
 		name = "Kanit"
		dynasty = "Assemmus"
		birth_date = 637.1.1
		adm = 4
		dip = 6
		mil = 4
    }
}

697.1.1 = {
	monarch = {
 		name = "Ashibaal"
		dynasty = "Darirnaddunumm"
		birth_date = 649.1.1
		adm = 2
		dip = 5
		mil = 1
    }
}

786.1.1 = {
	monarch = {
 		name = "Yassour"
		dynasty = "Hairshashishi"
		birth_date = 735.1.1
		adm = 0
		dip = 5
		mil = 5
    }
}

834.1.1 = {
	monarch = {
 		name = "Salay"
		dynasty = "Esurarnat"
		birth_date = 786.1.1
		adm = 5
		dip = 4
		mil = 1
    }
}

905.1.1 = {
	monarch = {
 		name = "Kushishi"
		dynasty = "Yanumibaal"
		birth_date = 858.1.1
		adm = 4
		dip = 3
		mil = 5
		female = yes
    }
}

989.1.1 = {
	monarch = {
 		name = "Asha-Ammu"
		dynasty = "Tibashipal"
		birth_date = 956.1.1
		adm = 5
		dip = 4
		mil = 6
    }
}

1082.1.1 = {
	monarch = {
 		name = "Yassour"
		dynasty = "Shin-Ilu"
		birth_date = 1035.1.1
		adm = 4
		dip = 3
		mil = 3
    }
}

1147.1.1 = {
	monarch = {
 		name = "Assi"
		dynasty = "Shishara"
		birth_date = 1122.1.1
		adm = 2
		dip = 3
		mil = 0
		female = yes
    }
}

1189.1.1 = {
	monarch = {
 		name = "Yenammu"
		dynasty = "Ashishpalirdan"
		birth_date = 1160.1.1
		adm = 0
		dip = 2
		mil = 3
    }
}

1241.1.1 = {
	monarch = {
 		name = "Sakiran"
		dynasty = "Timmiriran"
		birth_date = 1203.1.1
		adm = 5
		dip = 1
		mil = 0
		female = yes
    }
}

1287.1.1 = {
	monarch = {
 		name = "Kashtes"
		dynasty = "Saharnatturapli"
		birth_date = 1250.1.1
		adm = 4
		dip = 0
		mil = 4
    }
}

1329.1.1 = {
	monarch = {
 		name = "Ashuma-Nud"
		dynasty = "Ulirbabi"
		birth_date = 1302.1.1
		adm = 2
		dip = 0
		mil = 0
    }
}

1372.1.1 = {
	monarch = {
 		name = "Yen"
		dynasty = "Ashapaladdon"
		birth_date = 1351.1.1
		adm = 0
		dip = 6
		mil = 4
    }
}

1471.1.1 = {
	monarch = {
 		name = "Salmus"
		dynasty = "Mirshamammu"
		birth_date = 1436.1.1
		adm = 2
		dip = 0
		mil = 5
    }
}

1559.1.1 = {
	monarch = {
 		name = "Kashtes"
		dynasty = "Assebiriddan"
		birth_date = 1534.1.1
		adm = 0
		dip = 6
		mil = 2
    }
}

1636.1.1 = {
	monarch = {
 		name = "Seldus"
		dynasty = "Dun-Ahhe"
		birth_date = 1590.1.1
		adm = 6
		dip = 5
		mil = 6
    }
}

1722.1.1 = {
	monarch = {
 		name = "Maeli"
		dynasty = "Sobdishapal"
		birth_date = 1679.1.1
		adm = 4
		dip = 5
		mil = 2
		female = yes
    }
}

1787.1.1 = {
	monarch = {
 		name = "Assimusa"
		dynasty = "Matluberib"
		birth_date = 1760.1.1
		adm = 2
		dip = 4
		mil = 6
		female = yes
    }
}

1838.1.1 = {
	monarch = {
 		name = "Yanabani"
		dynasty = "Radansour"
		birth_date = 1793.1.1
		adm = 0
		dip = 3
		mil = 3
		female = yes
    }
}

1921.1.1 = {
	monarch = {
 		name = "Samsi"
		dynasty = "Assurnarairan"
		birth_date = 1886.1.1
		adm = 6
		dip = 2
		mil = 6
		female = yes
    }
}

2019.1.1 = {
	monarch = {
 		name = "Kausi"
		dynasty = "Salkatanat"
		birth_date = 2000.1.1
		adm = 4
		dip = 1
		mil = 3
    }
}

2115.1.1 = {
	monarch = {
 		name = "Assaba-Bentus"
		dynasty = "Assarnuridan"
		birth_date = 2074.1.1
		adm = 6
		dip = 3
		mil = 5
    }
}

2209.1.1 = {
	monarch = {
 		name = "Zabarbael"
		dynasty = "Kaushminipu"
		birth_date = 2175.1.1
		adm = 4
		dip = 2
		mil = 1
    }
}

2248.1.1 = {
	monarch = {
 		name = "Assamma-Idan"
		dynasty = "Zabamat"
		birth_date = 2208.1.1
		adm = 0
		dip = 3
		mil = 4
    }
}

2322.1.1 = {
	monarch = {
 		name = "Patababi"
		dynasty = "Serdimapal"
		birth_date = 2273.1.1
		adm = 0
		dip = 0
		mil = 2
    }
}

2394.1.1 = {
	monarch = {
 		name = "Ibanammu"
		dynasty = "Shimmabadas"
		birth_date = 2376.1.1
		adm = 3
		dip = 2
		mil = 4
		female = yes
    }
}

2483.1.1 = {
	monarch = {
 		name = "Kummi-Namus"
		dynasty = "Tansumiran"
		birth_date = 2441.1.1
		adm = 4
		dip = 6
		mil = 2
    }
}

