government = native
government_rank = 1
mercantilism = 1
technology_group = marshan_tg
religion = hist
primary_culture = archein
capital = 6749

54.1.1 = {
	monarch = {
 		name = "Tumma-Beekus"
		dynasty = "Casmareen"
		birth_date = 26.1.1
		adm = 4
		dip = 0
		mil = 2
    }
}

128.1.1 = {
	monarch = {
 		name = "Nesh-Deeka"
		dynasty = "Er-Tei"
		birth_date = 95.1.1
		adm = 2
		dip = 0
		mil = 5
    }
}

217.1.1 = {
	monarch = {
 		name = "Uralgnaza"
		dynasty = "Otumei"
		birth_date = 184.1.1
		adm = 0
		dip = 6
		mil = 2
    }
}

289.1.1 = {
	monarch = {
 		name = "Nuduxith"
		dynasty = "Miloeeja"
		birth_date = 252.1.1
		adm = 5
		dip = 5
		mil = 6
    }
}

337.1.1 = {
	monarch = {
 		name = "Iggistill"
		dynasty = "Casdorus"
		birth_date = 295.1.1
		adm = 4
		dip = 4
		mil = 2
    }
}

421.1.1 = {
	monarch = {
 		name = "Bijum-Ta"
		dynasty = "Sakeides"
		birth_date = 401.1.1
		adm = 2
		dip = 4
		mil = 6
    }
}

511.1.1 = {
	monarch = {
 		name = "Ula-Kii"
		dynasty = "Meeros"
		birth_date = 462.1.1
		adm = 0
		dip = 3
		mil = 3
		female = yes
    }
}

596.1.1 = {
	monarch = {
 		name = "Nuxal"
		dynasty = "Cascalees"
		birth_date = 566.1.1
		adm = 5
		dip = 2
		mil = 6
		female = yes
    }
}

689.1.1 = {
	monarch = {
 		name = "Jeetum-Hei"
		dynasty = "Andreecalees"
		birth_date = 646.1.1
		adm = 0
		dip = 3
		mil = 1
		female = yes
    }
}

773.1.1 = {
	monarch = {
 		name = "Bijum-Eeto"
		dynasty = "Caligeeja"
		birth_date = 738.1.1
		adm = 5
		dip = 2
		mil = 5
    }
}

830.1.1 = {
	monarch = {
 		name = "J'ram-Jeen"
		dynasty = "Talen-Ei"
		birth_date = 784.1.1
		adm = 4
		dip = 2
		mil = 1
    }
}

878.1.1 = {
	monarch = {
 		name = "Buujhan"
		dynasty = "Theodesh"
		birth_date = 844.1.1
		adm = 2
		dip = 1
		mil = 5
    }
}

915.1.1 = {
	monarch = {
 		name = "Veek-Gai"
		dynasty = "Taierlures"
		birth_date = 882.1.1
		adm = 0
		dip = 0
		mil = 1
		female = yes
    }
}

980.1.1 = {
	monarch = {
 		name = "Omeeta"
		dynasty = "Pethees"
		birth_date = 961.1.1
		adm = 5
		dip = 6
		mil = 5
		female = yes
    }
}

1016.1.1 = {
	monarch = {
 		name = "Iskenaaz"
		dynasty = "Mereemesh"
		birth_date = 986.1.1
		adm = 4
		dip = 5
		mil = 2
    }
}

1102.1.1 = {
	monarch = {
 		name = "Bur-Waska"
		dynasty = "Casmareen"
		birth_date = 1063.1.1
		adm = 2
		dip = 5
		mil = 5
    }
}

1167.1.1 = {
	monarch = {
 		name = "Utadeek"
		dynasty = "Cascalees"
		birth_date = 1129.1.1
		adm = 4
		dip = 6
		mil = 0
    }
}

1237.1.1 = {
	monarch = {
 		name = "Odeel-Shehs"
		dynasty = "Caligeepa"
		birth_date = 1190.1.1
		adm = 2
		dip = 5
		mil = 4
    }
}

1307.1.1 = {
	monarch = {
 		name = "Veexalt"
		dynasty = "Canision"
		birth_date = 1258.1.1
		adm = 0
		dip = 4
		mil = 0
    }
}

1367.1.1 = {
	monarch = {
 		name = "Oleen-Jei"
		dynasty = "Menes"
		birth_date = 1333.1.1
		adm = 5
		dip = 4
		mil = 4
    }
}

1464.1.1 = {
	monarch = {
 		name = "Jee-Tul"
		dynasty = "Pehrsar"
		birth_date = 1422.1.1
		adm = 4
		dip = 3
		mil = 1
    }
}

1547.1.1 = {
	monarch = {
 		name = "Chirurgeon"
		dynasty = "Madeian"
		birth_date = 1507.1.1
		adm = 2
		dip = 2
		mil = 4
		female = yes
    }
}

1634.1.1 = {
	monarch = {
 		name = "Veelisheeh"
		dynasty = "Er-Tei"
		birth_date = 1610.1.1
		adm = 0
		dip = 1
		mil = 1
    }
}

1729.1.1 = {
	monarch = {
 		name = "Okeeh"
		dynasty = "Er-Tei"
		birth_date = 1693.1.1
		adm = 5
		dip = 0
		mil = 4
    }
}

1778.1.1 = {
	monarch = {
 		name = "Jee-Tan"
		dynasty = "Casdorus"
		birth_date = 1745.1.1
		adm = 0
		dip = 2
		mil = 6
    }
}

1867.1.1 = {
	monarch = {
 		name = "Chaleed-Wan"
		dynasty = "Madeian"
		birth_date = 1842.1.1
		adm = 6
		dip = 1
		mil = 3
    }
}

1921.1.1 = {
	monarch = {
 		name = "Jilux"
		dynasty = "Menes"
		birth_date = 1881.1.1
		adm = 1
		dip = 2
		mil = 5
    }
}

2016.1.1 = {
	monarch = {
 		name = "Churasu"
		dynasty = "Jeetum-Lei"
		birth_date = 1992.1.1
		adm = 2
		dip = 6
		mil = 3
		female = yes
    }
}

2096.1.1 = {
	monarch = {
 		name = "Vornan"
		dynasty = "Nefeseus"
		birth_date = 2073.1.1
		adm = 0
		dip = 5
		mil = 0
    }
}

2195.1.1 = {
	monarch = {
 		name = "Gam-Kur"
		dynasty = "Meerhaj"
		birth_date = 2164.1.1
		adm = 6
		dip = 5
		mil = 3
    }
}

2288.1.1 = {
	monarch = {
 		name = "Jesh-Minko"
		dynasty = "Lafnaresh"
		birth_date = 2264.1.1
		adm = 4
		dip = 4
		mil = 0
    }
}

2333.1.1 = {
	monarch = {
 		name = "Cheedal-Gah"
		dynasty = "Madeian"
		birth_date = 2285.1.1
		adm = 2
		dip = 3
		mil = 4
    }
}

2378.1.1 = {
	monarch = {
 		name = "Vohta-Lan"
		dynasty = "Casmareen"
		birth_date = 2332.1.1
		adm = 4
		dip = 4
		mil = 5
    }
}

2447.1.1 = {
	monarch = {
 		name = "Opatieel"
		dynasty = "Gallures"
		birth_date = 2422.1.1
		adm = 2
		dip = 4
		mil = 2
    }
}

