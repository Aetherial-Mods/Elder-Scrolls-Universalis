government = monarchy
government_rank = 1
mercantilism = 1
technology_group = northern_tg
religion = nordic_pantheon
primary_culture = nord
capital = 1315

54.1.1 = {
	monarch = {
 		name = "Sittar"
		dynasty = "Heylrianlor"
		birth_date = 9.1.1
		adm = 6
		dip = 3
		mil = 4
    }
}

111.1.1 = {
	monarch = {
 		name = "Ingritha"
		dynasty = "Thelbell"
		birth_date = 64.1.1
		adm = 5
		dip = 2
		mil = 0
		female = yes
    }
	queen = {
 		name = "Hroi"
		dynasty = "Thelbell"
		birth_date = 78.1.1
		adm = 1
		dip = 1
		mil = 6
    }
	heir = {
 		name = "Yringor"
		monarch_name = "Yringor I"
		dynasty = "Thelbell"
		birth_date = 104.1.1
		death_date = 204.1.1
		claim = 100
		adm = 5
		dip = 6
		mil = 5
    }
}

204.1.1 = {
	monarch = {
 		name = "Agni"
		dynasty = "Hjialdr"
		birth_date = 157.1.1
		adm = 5
		dip = 3
		mil = 6
		female = yes
    }
}

277.1.1 = {
	monarch = {
 		name = "Griskild"
		dynasty = "Skolnil"
		birth_date = 257.1.1
		adm = 3
		dip = 2
		mil = 2
    }
	queen = {
 		name = "Maven"
		dynasty = "Skolnil"
		birth_date = 245.1.1
		adm = 0
		dip = 0
		mil = 1
		female = yes
    }
	heir = {
 		name = "Fruth"
		monarch_name = "Fruth I"
		dynasty = "Skolnil"
		birth_date = 272.1.1
		death_date = 364.1.1
		claim = 100
		adm = 4
		dip = 6
		mil = 0
    }
}

364.1.1 = {
	monarch = {
 		name = "Slafknir"
		dynasty = "Frethold"
		birth_date = 332.1.1
		adm = 6
		dip = 0
		mil = 3
    }
}

415.1.1 = {
	monarch = {
 		name = "Knjakr"
		dynasty = "Hjangris"
		birth_date = 377.1.1
		adm = 5
		dip = 0
		mil = 6
    }
}

490.1.1 = {
	monarch = {
 		name = "Frirhild"
		dynasty = "Ratmam"
		birth_date = 459.1.1
		adm = 3
		dip = 6
		mil = 3
		female = yes
    }
}

587.1.1 = {
	monarch = {
 		name = "Amandtea"
		dynasty = "Kleelim"
		birth_date = 558.1.1
		adm = 1
		dip = 5
		mil = 0
		female = yes
    }
}

623.1.1 = {
	monarch = {
 		name = "Skorvild"
		dynasty = "Hagrudr"
		birth_date = 579.1.1
		adm = 3
		dip = 6
		mil = 1
    }
}

711.1.1 = {
	monarch = {
 		name = "Klause"
		dynasty = "Hlihuldan"
		birth_date = 687.1.1
		adm = 1
		dip = 5
		mil = 5
    }
}

763.1.1 = {
	monarch = {
 		name = "Sormund"
		dynasty = "Klytmor"
		birth_date = 737.1.1
		adm = 0
		dip = 5
		mil = 1
    }
}

799.1.1 = {
	monarch = {
 		name = "Jakirfa"
		dynasty = "Kuvinludr"
		birth_date = 781.1.1
		adm = 5
		dip = 4
		mil = 5
		female = yes
    }
	queen = {
 		name = "Virgerd"
		dynasty = "Kuvinludr"
		birth_date = 779.1.1
		adm = 2
		dip = 2
		mil = 4
    }
}

884.1.1 = {
	monarch = {
 		name = "Sorli"
		dynasty = "Thrialder"
		birth_date = 832.1.1
		adm = 0
		dip = 2
		mil = 1
		female = yes
    }
}

919.1.1 = {
	monarch = {
 		name = "Malsold"
		dynasty = "Rulur"
		birth_date = 877.1.1
		adm = 5
		dip = 1
		mil = 4
    }
}

962.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Kraldar"
		monarch_name = "Kraldar I"
		dynasty = "Veladrys"
		birth_date = 954.1.1
		death_date = 972.1.1
		claim = 100
		adm = 6
		dip = 5
		mil = 2
    }
}

972.1.1 = {
	monarch = {
 		name = "Bolgeir"
		dynasty = "Hadrehyldr"
		birth_date = 948.1.1
		adm = 2
		dip = 6
		mil = 5
    }
	queen = {
 		name = "Agni"
		dynasty = "Hadrehyldr"
		birth_date = 950.1.1
		adm = 6
		dip = 5
		mil = 4
		female = yes
    }
	heir = {
 		name = "Aslfur"
		monarch_name = "Aslfur I"
		dynasty = "Hadrehyldr"
		birth_date = 969.1.1
		death_date = 1025.1.1
		claim = 100
		adm = 2
		dip = 3
		mil = 2
    }
}

1025.1.1 = {
	monarch = {
 		name = "Majoll"
		dynasty = "Thrivedraldr"
		birth_date = 1005.1.1
		adm = 2
		dip = 0
		mil = 3
    }
}

1112.1.1 = {
	monarch = {
 		name = "Hedstagg"
		dynasty = "Hlolmof"
		birth_date = 1071.1.1
		adm = 0
		dip = 6
		mil = 6
    }
	queen = {
 		name = "Freigritte"
		dynasty = "Hlolmof"
		birth_date = 1086.1.1
		adm = 4
		dip = 4
		mil = 5
		female = yes
    }
	heir = {
 		name = "Sulrengar"
		monarch_name = "Sulrengar I"
		dynasty = "Hlolmof"
		birth_date = 1099.1.1
		death_date = 1183.1.1
		claim = 100
		adm = 4
		dip = 1
		mil = 6
    }
}

1183.1.1 = {
	monarch = {
 		name = "Hafjorg"
		dynasty = "Stetmak"
		birth_date = 1139.1.1
		adm = 4
		dip = 4
		mil = 0
		female = yes
    }
}

1275.1.1 = {
	monarch = {
 		name = "Braxek"
		dynasty = "Kligild"
		birth_date = 1250.1.1
		adm = 2
		dip = 4
		mil = 3
    }
}

1353.1.1 = {
	monarch = {
 		name = "Tolfdir"
		dynasty = "Hagrudr"
		birth_date = 1320.1.1
		adm = 0
		dip = 3
		mil = 0
    }
	queen = {
 		name = "Fredo"
		dynasty = "Hagrudr"
		birth_date = 1305.1.1
		adm = 4
		dip = 1
		mil = 6
		female = yes
    }
	heir = {
 		name = "Sernea"
		monarch_name = "Sernea I"
		dynasty = "Hagrudr"
		birth_date = 1347.1.1
		death_date = 1400.1.1
		claim = 100
		adm = 1
		dip = 0
		mil = 5
		female = yes
    }
}

1400.1.1 = {
	monarch = {
 		name = "Haelga"
		dynasty = "Rondirek"
		birth_date = 1375.1.1
		adm = 4
		dip = 1
		mil = 0
		female = yes
    }
}

1459.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Thallik"
		monarch_name = "Thallik I"
		dynasty = "Brinhal"
		birth_date = 1453.1.1
		death_date = 1471.1.1
		claim = 100
		adm = 4
		dip = 5
		mil = 5
    }
}

1471.1.1 = {
	monarch = {
 		name = "Svargret"
		dynasty = "Ratmam"
		birth_date = 1427.1.1
		adm = 4
		dip = 2
		mil = 5
		female = yes
    }
}

1526.1.1 = {
	monarch = {
 		name = "Brynjolfr"
		dynasty = "Thelmyn"
		birth_date = 1490.1.1
		adm = 2
		dip = 1
		mil = 2
    }
}

1580.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Beitild"
		monarch_name = "Beitild I"
		dynasty = "Hlogren"
		birth_date = 1567.1.1
		death_date = 1585.1.1
		claim = 100
		adm = 3
		dip = 5
		mil = 0
		female = yes
    }
}

1585.1.1 = {
	monarch = {
 		name = "Lisaa"
		dynasty = "Hleingrenyl"
		birth_date = 1548.1.1
		adm = 5
		dip = 6
		mil = 2
		female = yes
    }
}

1678.1.1 = {
	monarch = {
 		name = "Hamal"
		dynasty = "Surjafald"
		birth_date = 1638.1.1
		adm = 4
		dip = 5
		mil = 6
		female = yes
    }
}

1729.1.1 = {
	monarch = {
 		name = "Brimfja"
		dynasty = "Hlalmadrun"
		birth_date = 1677.1.1
		adm = 2
		dip = 5
		mil = 3
		female = yes
    }
	queen = {
 		name = "Aslfur"
		dynasty = "Hlalmadrun"
		birth_date = 1682.1.1
		adm = 6
		dip = 3
		mil = 2
    }
	heir = {
 		name = "Axulfa"
		monarch_name = "Axulfa I"
		dynasty = "Hlalmadrun"
		birth_date = 1728.1.1
		death_date = 1815.1.1
		claim = 100
		adm = 3
		dip = 2
		mil = 1
		female = yes
    }
}

1815.1.1 = {
	monarch = {
 		name = "Narfar"
		dynasty = "Tirmonmoll"
		birth_date = 1765.1.1
		adm = 2
		dip = 5
		mil = 1
    }
	queen = {
 		name = "Jafrera"
		dynasty = "Tirmonmoll"
		birth_date = 1777.1.1
		adm = 2
		dip = 2
		mil = 2
		female = yes
    }
	heir = {
 		name = "Bjornen"
		monarch_name = "Bjornen I"
		dynasty = "Tirmonmoll"
		birth_date = 1800.1.1
		death_date = 1905.1.1
		claim = 100
		adm = 6
		dip = 0
		mil = 1
    }
}

1905.1.1 = {
	monarch = {
 		name = "Norgred"
		dynasty = "Skolnuf"
		birth_date = 1863.1.1
		adm = 6
		dip = 4
		mil = 1
    }
	queen = {
 		name = "Anjolda"
		dynasty = "Skolnuf"
		birth_date = 1882.1.1
		adm = 2
		dip = 2
		mil = 0
		female = yes
    }
	heir = {
 		name = "Lynch"
		monarch_name = "Lynch I"
		dynasty = "Skolnuf"
		birth_date = 1899.1.1
		death_date = 1996.1.1
		claim = 100
		adm = 6
		dip = 1
		mil = 6
    }
}

1996.1.1 = {
	monarch = {
 		name = "Chalwyn"
		dynasty = "Salyld"
		birth_date = 1958.1.1
		adm = 2
		dip = 2
		mil = 1
		female = yes
    }
}

2042.1.1 = {
	monarch = {
 		name = "Undveld"
		dynasty = "Storgath"
		birth_date = 1989.1.1
		adm = 0
		dip = 1
		mil = 5
    }
	queen = {
 		name = "Fruvar"
		dynasty = "Storgath"
		birth_date = 1997.1.1
		adm = 4
		dip = 0
		mil = 4
		female = yes
    }
	heir = {
 		name = "Solding"
		monarch_name = "Solding I"
		dynasty = "Storgath"
		birth_date = 2041.1.1
		death_date = 2090.1.1
		claim = 100
		adm = 1
		dip = 5
		mil = 3
		female = yes
    }
}

2090.1.1 = {
	monarch = {
 		name = "Hogondar"
		dynasty = "Ratmam"
		birth_date = 2047.1.1
		adm = 4
		dip = 0
		mil = 5
    }
	queen = {
 		name = "Geirvarda"
		dynasty = "Ratmam"
		birth_date = 2068.1.1
		adm = 1
		dip = 5
		mil = 4
		female = yes
    }
	heir = {
 		name = "Svargret"
		monarch_name = "Svargret I"
		dynasty = "Ratmam"
		birth_date = 2080.1.1
		death_date = 2148.1.1
		claim = 100
		adm = 4
		dip = 4
		mil = 3
		female = yes
    }
}

2148.1.1 = {
	monarch = {
 		name = "Ulfric"
		dynasty = "Bruleihof"
		birth_date = 2120.1.1
		adm = 4
		dip = 0
		mil = 4
    }
}

2196.1.1 = {
	monarch = {
 		name = "Drod"
		dynasty = "Brodrunlyn"
		birth_date = 2146.1.1
		adm = 2
		dip = 6
		mil = 0
		female = yes
    }
	queen = {
 		name = "Lassnr"
		dynasty = "Brodrunlyn"
		birth_date = 2154.1.1
		adm = 6
		dip = 5
		mil = 6
    }
	heir = {
 		name = "Botrir"
		monarch_name = "Botrir I"
		dynasty = "Brodrunlyn"
		birth_date = 2191.1.1
		death_date = 2288.1.1
		claim = 100
		adm = 3
		dip = 3
		mil = 5
    }
}

2288.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Thromgar"
		monarch_name = "Thromgar I"
		dynasty = "Hjangris"
		birth_date = 2282.1.1
		death_date = 2300.1.1
		claim = 100
		adm = 1
		dip = 2
		mil = 2
    }
}

2300.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Maralthon"
		monarch_name = "Maralthon I"
		dynasty = "Hlogren"
		birth_date = 2288.1.1
		death_date = 2306.1.1
		claim = 100
		adm = 6
		dip = 2
		mil = 5
    }
}

2306.1.1 = {
	monarch = {
 		name = "Didrika"
		dynasty = "Tilgeskr"
		birth_date = 2261.1.1
		adm = 2
		dip = 3
		mil = 1
		female = yes
    }
}

2376.1.1 = {
	monarch = {
 		name = "Bedrir"
		dynasty = "Snake-eye"
		birth_date = 2324.1.1
		adm = 1
		dip = 6
		mil = 1
    }
}

2448.1.1 = {
	monarch = {
 		name = "Malfa"
		dynasty = "Ferleld"
		birth_date = 2418.1.1
		adm = 2
		dip = 4
		mil = 6
		female = yes
    }
}

2496.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Bronvid"
		monarch_name = "Bronvid I"
		dynasty = "Vudmuldr"
		birth_date = 2494.1.1
		death_date = 2512.1.1
		claim = 100
		adm = 6
		dip = 6
		mil = 6
    }
}

