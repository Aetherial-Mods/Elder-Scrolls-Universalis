government = republic
add_government_reform = pirate_republic_reform
government_rank = 1
mercantilism = 1
technology_group = cyrodiil_tg
religion = molag_bal_cult
primary_culture = ayleid
capital = 1171

54.1.1 = {
	monarch = {
 		name = "Fenlord"
		dynasty = "Ula-Horuria"
		birth_date = 30.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

144.1.1 = {
	monarch = {
 		name = "Lunnu"
		dynasty = "Mea-Civiant"
		birth_date = 93.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

220.1.1 = {
	monarch = {
 		name = "Cuussint"
		dynasty = "Mea-Brina"
		birth_date = 195.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

271.1.1 = {
	monarch = {
 		name = "Yrraldylith"
		dynasty = "Mea-Adrarcella"
		birth_date = 235.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

319.1.1 = {
	monarch = {
 		name = "Endarre"
		dynasty = "Vae-Vandiand"
		birth_date = 298.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

410.1.1 = {
	monarch = {
 		name = "Rirgurhand"
		dynasty = "Ula-Macelius"
		birth_date = 385.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

475.1.1 = {
	monarch = {
 		name = "Erraduure"
		dynasty = "Vae-Sylolvia"
		birth_date = 447.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

544.1.1 = {
	monarch = {
 		name = "Fidhi"
		dynasty = "Mea-Bromma"
		birth_date = 492.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

593.1.1 = {
	monarch = {
 		name = "Vanhur"
		dynasty = "Vae-Vonara"
		birth_date = 554.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

639.1.1 = {
	monarch = {
 		name = "Genuuladom"
		dynasty = "Ula-Matuseius"
		birth_date = 621.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

719.1.1 = {
	monarch = {
 		name = "Hergor"
		dynasty = "Vae-Weyandawik"
		birth_date = 679.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

778.1.1 = {
	monarch = {
 		name = "Nogo"
		dynasty = "Mea-Amane"
		birth_date = 749.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

849.1.1 = {
	monarch = {
 		name = "Summi"
		dynasty = "Mea-Anvil"
		birth_date = 822.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

902.1.1 = {
	monarch = {
 		name = "Irreshyva"
		dynasty = "Ula-Julidonea"
		birth_date = 855.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

959.1.1 = {
	monarch = {
 		name = "Vyza"
		dynasty = "Vae-Selertia"
		birth_date = 922.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1025.1.1 = {
	monarch = {
 		name = "Tjan"
		dynasty = "Ula-Horuria"
		birth_date = 982.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1109.1.1 = {
	monarch = {
 		name = "Lanath"
		dynasty = "Vae-Vortia"
		birth_date = 1078.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1146.1.1 = {
	monarch = {
 		name = "Tedygedo"
		dynasty = "Mea-Hastrel"
		birth_date = 1117.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1186.1.1 = {
	monarch = {
 		name = "Qegylomo"
		dynasty = "Ula-Isollaise"
		birth_date = 1153.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1280.1.1 = {
	monarch = {
 		name = "Tjan"
		dynasty = "Vae-Tasaso"
		birth_date = 1227.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1328.1.1 = {
	monarch = {
 		name = "Houtern"
		dynasty = "Vae-Selviloria"
		birth_date = 1297.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1414.1.1 = {
	monarch = {
 		name = "Tedygedo"
		dynasty = "Ula-Ryndenyse"
		birth_date = 1362.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1477.1.1 = {
	monarch = {
 		name = "Nuralanya"
		dynasty = "Mea-Crownhaven"
		birth_date = 1459.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1522.1.1 = {
	monarch = {
 		name = "Cymylmuumer"
		dynasty = "Ula-Miscarcand"
		birth_date = 1477.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1560.1.1 = {
	monarch = {
 		name = "Mevirlen"
		dynasty = "Vae-Tirerius"
		birth_date = 1536.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1629.1.1 = {
	monarch = {
 		name = "Heystuadhisind"
		dynasty = "Mea-Essassius"
		birth_date = 1608.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1716.1.1 = {
	monarch = {
 		name = "Myndhal"
		dynasty = "Mea-Brina"
		birth_date = 1683.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1807.1.1 = {
	monarch = {
 		name = "Wimunmara"
		dynasty = "Vae-Varondo"
		birth_date = 1773.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1863.1.1 = {
	monarch = {
 		name = "Dondont"
		dynasty = "Mea-Hackdirt"
		birth_date = 1810.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1911.1.1 = {
	monarch = {
 		name = "Mene"
		dynasty = "Vae-Vesagrius"
		birth_date = 1863.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1980.1.1 = {
	monarch = {
 		name = "Pehyshen"
		dynasty = "Vae-Vilverin"
		birth_date = 1930.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

2029.1.1 = {
	monarch = {
 		name = "Mennith"
		dynasty = "Mea-Bravil"
		birth_date = 2008.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

2083.1.1 = {
	monarch = {
 		name = "Odilon"
		dynasty = "Ula-Homestead"
		birth_date = 2042.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2120.1.1 = {
	monarch = {
 		name = "Hosnuugaura"
		dynasty = "Ula-Kvatch"
		birth_date = 2085.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2168.1.1 = {
	monarch = {
 		name = "Gisegilin"
		dynasty = "Ula-Nonungalo"
		birth_date = 2150.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2252.1.1 = {
	monarch = {
 		name = "Mennith"
		dynasty = "Mea-Allevar"
		birth_date = 2219.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

2304.1.1 = {
	monarch = {
 		name = "Odilon"
		dynasty = "Ula-Rusifus"
		birth_date = 2271.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2381.1.1 = {
	monarch = {
 		name = "Lunrun"
		dynasty = "Ula-Nenyond"
		birth_date = 2362.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2439.1.1 = {
	monarch = {
 		name = "Valasha"
		dynasty = "Ula-Restidina"
		birth_date = 2403.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

