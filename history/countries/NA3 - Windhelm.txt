government = monarchy
government_rank = 5
mercantilism = 1
technology_group = northern_tg
religion = nordic_pantheon
primary_culture = nord
capital = 1275
elector = yes

54.1.1 = {
	monarch = {
 		name = "Vidrald"
		dynasty = "Ysgramor"
		birth_date = 30.1.1
		adm = 3
		dip = 5
		mil = 3
    }
}

102.1.1 = {
	monarch = {
 		name = "Orryn"
		dynasty = "Ysgramor"
		birth_date = 52.1.1
		adm = 6
		dip = 2
		mil = 3
    }
	queen = {
 		name = "Aumfi"
		dynasty = "Hleingrenyl"
		birth_date = 52.1.1
		adm = 3
		dip = 0
		mil = 2
		female = yes
    }
	heir = {
 		name = "Mikrul"
		monarch_name = "Mikrul I"
		dynasty = "Ysgramor"
		birth_date = 97.1.1
		death_date = 155.1.1
		claim = 100
		adm = 6
		dip = 6
		mil = 1
    }
}

155.1.1 = {
	monarch = {
 		name = "Eislef"
		dynasty = "Ysgramor"
		birth_date = 109.1.1
		adm = 2
		dip = 0
		mil = 4
    }
	queen = {
 		name = "Bergitte"
		dynasty = "Delorr"
		birth_date = 102.1.1
		adm = 6
		dip = 6
		mil = 3
		female = yes
    }
}

207.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Ordun"
		monarch_name = "Ordun I"
		dynasty = "Ysgramor"
		birth_date = 201.1.1
		death_date = 219.1.1
		claim = 100
		adm = 0
		dip = 3
		mil = 0
    }
}

219.1.1 = {
	monarch = {
 		name = "Fruth"
		dynasty = "Ysgramor"
		birth_date = 187.1.1
		adm = 3
		dip = 4
		mil = 3
    }
}

262.1.1 = {
	monarch = {
 		name = "Yorda"
		dynasty = "Ysgramor"
		birth_date = 237.1.1
		adm = 4
		dip = 5
		mil = 4
		female = yes
    }
	queen = {
 		name = "Ulfarr"
		dynasty = "Boechyrom"
		birth_date = 219.1.1
		adm = 5
		dip = 2
		mil = 5
    }
	heir = {
 		name = "Hjolvara"
		monarch_name = "Hjolvara I"
		dynasty = "Ysgramor"
		birth_date = 251.1.1
		death_date = 338.1.1
		claim = 100
		adm = 2
		dip = 0
		mil = 4
		female = yes
    }
}

338.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Eling"
		monarch_name = "Eling I"
		dynasty = "Ysgramor"
		birth_date = 325.1.1
		death_date = 343.1.1
		claim = 100
		adm = 0
		dip = 6
		mil = 1
		female = yes
    }
}

343.1.1 = {
	monarch = {
 		name = "Oriella"
		dynasty = "Ysgramor"
		birth_date = 293.1.1
		adm = 6
		dip = 3
		mil = 1
		female = yes
    }
	queen = {
 		name = "Burd"
		dynasty = "Hleirjin"
		birth_date = 301.1.1
		adm = 3
		dip = 1
		mil = 0
    }
	heir = {
 		name = "Radd"
		monarch_name = "Radd I"
		dynasty = "Ysgramor"
		birth_date = 334.1.1
		death_date = 419.1.1
		claim = 100
		adm = 0
		dip = 0
		mil = 6
    }
}

419.1.1 = {
	monarch = {
 		name = "Fjorolfa"
		dynasty = "Ysgramor"
		birth_date = 372.1.1
		adm = 3
		dip = 1
		mil = 2
		female = yes
    }
}

500.1.1 = {
	monarch = {
 		name = "Aelakja"
		dynasty = "Ysgramor"
		birth_date = 480.1.1
		adm = 1
		dip = 0
		mil = 5
		female = yes
    }
	queen = {
 		name = "Valbern"
		dynasty = "Stisgreskr"
		birth_date = 447.1.1
		adm = 5
		dip = 6
		mil = 4
    }
	heir = {
 		name = "Virgerd"
		monarch_name = "Virgerd I"
		dynasty = "Ysgramor"
		birth_date = 491.1.1
		death_date = 547.1.1
		claim = 100
		adm = 2
		dip = 5
		mil = 3
    }
}

547.1.1 = {
	monarch = {
 		name = "Jomathak"
		dynasty = "Ysgramor"
		birth_date = 505.1.1
		adm = 1
		dip = 1
		mil = 3
    }
	queen = {
 		name = "Helgreir"
		dynasty = "Folsgold"
		birth_date = 516.1.1
		adm = 1
		dip = 4
		mil = 5
		female = yes
    }
	heir = {
 		name = "Wilhem"
		monarch_name = "Wilhem I"
		dynasty = "Ysgramor"
		birth_date = 535.1.1
		death_date = 591.1.1
		claim = 100
		adm = 5
		dip = 3
		mil = 4
    }
}

591.1.1 = {
	monarch = {
 		name = "Kaleb"
		dynasty = "Ysgramor"
		birth_date = 542.1.1
		adm = 5
		dip = 6
		mil = 4
    }
}

655.1.1 = {
	monarch = {
 		name = "Gjalder"
		dynasty = "Ysgramor"
		birth_date = 636.1.1
		adm = 3
		dip = 6
		mil = 0
    }
}

726.1.1 = {
	monarch = {
 		name = "Aldrig"
		dynasty = "Ysgramor"
		birth_date = 690.1.1
		adm = 1
		dip = 5
		mil = 4
    }
	queen = {
 		name = "Helgi"
		dynasty = "Vodrold"
		birth_date = 686.1.1
		adm = 5
		dip = 3
		mil = 3
		female = yes
    }
	heir = {
 		name = "Waren"
		monarch_name = "Waren I"
		dynasty = "Ysgramor"
		birth_date = 715.1.1
		death_date = 761.1.1
		claim = 100
		adm = 2
		dip = 2
		mil = 2
    }
}

761.1.1 = {
	monarch = {
 		name = "Kadstig"
		dynasty = "Ysgramor"
		birth_date = 725.1.1
		adm = 5
		dip = 3
		mil = 4
    }
}

831.1.1 = {
	monarch = {
 		name = "Gissur"
		dynasty = "Ysgramor"
		birth_date = 809.1.1
		adm = 3
		dip = 2
		mil = 1
    }
}

916.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Romord"
		monarch_name = "Romord I"
		dynasty = "Ysgramor"
		birth_date = 913.1.1
		death_date = 931.1.1
		claim = 100
		adm = 4
		dip = 6
		mil = 6
    }
}

931.1.1 = {
	monarch = {
 		name = "Sigyrr"
		dynasty = "Ysgramor"
		birth_date = 908.1.1
		adm = 3
		dip = 3
		mil = 6
    }
	queen = {
 		name = "Marla"
		dynasty = "Salyld"
		birth_date = 901.1.1
		adm = 0
		dip = 1
		mil = 5
		female = yes
    }
	heir = {
 		name = "Faivera"
		monarch_name = "Faivera I"
		dynasty = "Ysgramor"
		birth_date = 922.1.1
		death_date = 970.1.1
		claim = 100
		adm = 0
		dip = 5
		mil = 6
		female = yes
    }
}

970.1.1 = {
	monarch = {
 		name = "Skorvild"
		dynasty = "Ysgramor"
		birth_date = 919.1.1
		adm = 6
		dip = 1
		mil = 6
    }
}

1018.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Rolf"
		monarch_name = "Rolf I"
		dynasty = "Ysgramor"
		birth_date = 1015.1.1
		death_date = 1033.1.1
		claim = 100
		adm = 0
		dip = 5
		mil = 4
    }
}

1033.1.1 = {
	monarch = {
 		name = "Goren"
		dynasty = "Ysgramor"
		birth_date = 1012.1.1
		adm = 3
		dip = 0
		mil = 0
    }
	queen = {
 		name = "Margot"
		dynasty = "Tirmonmoll"
		birth_date = 1000.1.1
		adm = 0
		dip = 5
		mil = 6
		female = yes
    }
	heir = {
 		name = "Eyja"
		monarch_name = "Eyja I"
		dynasty = "Ysgramor"
		birth_date = 1019.1.1
		death_date = 1125.1.1
		claim = 100
		adm = 4
		dip = 4
		mil = 5
		female = yes
    }
}

1125.1.1 = {
	monarch = {
 		name = "Skjorlak"
		dynasty = "Ysgramor"
		birth_date = 1100.1.1
		adm = 6
		dip = 5
		mil = 0
    }
	queen = {
 		name = "Mette"
		dynasty = "Klytmor"
		birth_date = 1073.1.1
		adm = 3
		dip = 4
		mil = 6
		female = yes
    }
	heir = {
 		name = "Garm"
		monarch_name = "Garm I"
		dynasty = "Ysgramor"
		birth_date = 1117.1.1
		death_date = 1195.1.1
		claim = 100
		adm = 0
		dip = 2
		mil = 5
    }
}

1195.1.1 = {
	monarch = {
 		name = "Hrothmund"
		dynasty = "Ysgramor"
		birth_date = 1173.1.1
		adm = 4
		dip = 1
		mil = 4
    }
}

1249.1.1 = {
	monarch = {
 		name = "Eorlund"
		dynasty = "Ysgramor"
		birth_date = 1203.1.1
		adm = 5
		dip = 5
		mil = 2
    }
	queen = {
 		name = "Kara"
		dynasty = "Vunheldys"
		birth_date = 1203.1.1
		adm = 6
		dip = 5
		mil = 0
		female = yes
    }
	heir = {
 		name = "Brimfja"
		monarch_name = "Brimfja I"
		dynasty = "Ysgramor"
		birth_date = 1238.1.1
		death_date = 1325.1.1
		claim = 100
		adm = 3
		dip = 4
		mil = 6
		female = yes
    }
}

1325.1.1 = {
	monarch = {
 		name = "Otrovor"
		dynasty = "Ysgramor"
		birth_date = 1299.1.1
		adm = 6
		dip = 5
		mil = 1
    }
}

1400.1.1 = {
	monarch = {
 		name = "Hronolf"
		dynasty = "Ysgramor"
		birth_date = 1365.1.1
		adm = 1
		dip = 0
		mil = 2
    }
}

1486.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Thronoda"
		monarch_name = "Thronoda I"
		dynasty = "Ysgramor"
		birth_date = 1472.1.1
		death_date = 1490.1.1
		claim = 100
		adm = 5
		dip = 2
		mil = 2
		female = yes
    }
}

1490.1.1 = {
	monarch = {
 		name = "Innraek"
		dynasty = "Ysgramor"
		birth_date = 1460.1.1
		adm = 4
		dip = 5
		mil = 3
    }
	queen = {
 		name = "Sonir"
		dynasty = "Thelmyn"
		birth_date = 1459.1.1
		adm = 1
		dip = 4
		mil = 2
		female = yes
    }
}

1525.1.1 = {
	monarch = {
 		name = "Sigyrr"
		dynasty = "Ysgramor"
		birth_date = 1498.1.1
		adm = 6
		dip = 3
		mil = 5
    }
}

1599.1.1 = {
	monarch = {
 		name = "Jothorygg"
		dynasty = "Ysgramor"
		birth_date = 1557.1.1
		adm = 5
		dip = 2
		mil = 2
    }
}

1643.1.1 = {
	monarch = {
 		name = "Gethmard"
		dynasty = "Ysgramor"
		birth_date = 1618.1.1
		adm = 3
		dip = 1
		mil = 6
    }
}

1705.1.1 = {
	monarch = {
 		name = "Akar"
		dynasty = "Ysgramor"
		birth_date = 1673.1.1
		adm = 1
		dip = 0
		mil = 2
    }
}

1786.1.1 = {
	monarch = {
 		name = "Othvild"
		dynasty = "Ysgramor"
		birth_date = 1745.1.1
		adm = 6
		dip = 0
		mil = 6
		female = yes
    }
	queen = {
 		name = "Ollfar"
		dynasty = "Hjialdr"
		birth_date = 1742.1.1
		adm = 3
		dip = 5
		mil = 5
    }
	heir = {
 		name = "Ralgar"
		monarch_name = "Ralgar I"
		dynasty = "Ysgramor"
		birth_date = 1774.1.1
		death_date = 1842.1.1
		claim = 100
		adm = 0
		dip = 4
		mil = 4
    }
}

1842.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Yavni"
		monarch_name = "Yavni I"
		dynasty = "Ysgramor"
		birth_date = 1829.1.1
		death_date = 1847.1.1
		claim = 100
		adm = 5
		dip = 3
		mil = 1
    }
}

1847.1.1 = {
	monarch = {
 		name = "Afnhi"
		dynasty = "Ysgramor"
		birth_date = 1807.1.1
		adm = 5
		dip = 6
		mil = 1
		female = yes
    }
	queen = {
 		name = "Valeric"
		dynasty = "Gilsganl"
		birth_date = 1828.1.1
		adm = 2
		dip = 5
		mil = 0
    }
	heir = {
 		name = "Jakalor"
		monarch_name = "Jakalor I"
		dynasty = "Ysgramor"
		birth_date = 1835.1.1
		death_date = 1943.1.1
		claim = 100
		adm = 2
		dip = 1
		mil = 1
    }
}

1943.1.1 = {
	monarch = {
 		name = "Alskar"
		dynasty = "Ysgramor"
		birth_date = 1922.1.1
		adm = 1
		dip = 5
		mil = 1
    }
	queen = {
 		name = "Hermir"
		dynasty = "Kosner"
		birth_date = 1901.1.1
		adm = 5
		dip = 3
		mil = 0
		female = yes
    }
	heir = {
 		name = "Voddreid"
		monarch_name = "Voddreid I"
		dynasty = "Ysgramor"
		birth_date = 1932.1.1
		death_date = 2007.1.1
		claim = 100
		adm = 2
		dip = 2
		mil = 6
		female = yes
    }
}

2007.1.1 = {
	monarch = {
 		name = "Irgeikka"
		dynasty = "Ysgramor"
		birth_date = 1971.1.1
		adm = 5
		dip = 3
		mil = 1
		female = yes
    }
}

2071.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Hranir"
		monarch_name = "Hranir I"
		dynasty = "Ysgramor"
		birth_date = 2065.1.1
		death_date = 2083.1.1
		claim = 100
		adm = 5
		dip = 0
		mil = 6
		female = yes
    }
}

2083.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Fjori"
		monarch_name = "Fjori I"
		dynasty = "Ysgramor"
		birth_date = 2083.1.1
		death_date = 2101.1.1
		claim = 100
		adm = 4
		dip = 6
		mil = 3
    }
}

2101.1.1 = {
	monarch = {
 		name = "Skald"
		dynasty = "Ysgramor"
		birth_date = 2053.1.1
		adm = 3
		dip = 3
		mil = 3
    }
}

2186.1.1 = {
	monarch = {
 		name = "Kemirik"
		dynasty = "Ysgramor"
		birth_date = 2167.1.1
		adm = 1
		dip = 2
		mil = 0
    }
	queen = {
 		name = "Hjagir"
		dynasty = "Rotmalleskr"
		birth_date = 2150.1.1
		adm = 5
		dip = 0
		mil = 6
		female = yes
    }
	heir = {
 		name = "Yust"
		monarch_name = "Yust I"
		dynasty = "Ysgramor"
		birth_date = 2183.1.1
		death_date = 2241.1.1
		claim = 100
		adm = 5
		dip = 4
		mil = 0
    }
}

2241.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Olava"
		monarch_name = "Olava I"
		dynasty = "Ysgramor"
		birth_date = 2226.1.1
		death_date = 2244.1.1
		claim = 100
		adm = 0
		dip = 5
		mil = 1
		female = yes
    }
}

2244.1.1 = {
	monarch = {
 		name = "Grundskar"
		dynasty = "Ysgramor"
		birth_date = 2191.1.1
		adm = 3
		dip = 0
		mil = 4
    }
	queen = {
 		name = "Megvarda"
		dynasty = "Hreythond"
		birth_date = 2212.1.1
		adm = 0
		dip = 5
		mil = 3
		female = yes
    }
}

2312.1.1 = {
	monarch = {
 		name = "Kili"
		dynasty = "Ysgramor"
		birth_date = 2288.1.1
		adm = 5
		dip = 4
		mil = 6
		female = yes
    }
}

2355.1.1 = {
	monarch = {
 		name = "Harvald"
		dynasty = "Ysgramor"
		birth_date = 2328.1.1
		adm = 3
		dip = 3
		mil = 3
    }
}

2395.1.1 = {
	monarch = {
 		name = "Birkir"
		dynasty = "Ysgramor"
		birth_date = 2348.1.1
		adm = 2
		dip = 3
		mil = 0
    }
}

2463.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Areas"
		monarch_name = "Areas I"
		dynasty = "Ysgramor"
		birth_date = 2452.1.1
		death_date = 2470.1.1
		claim = 100
		adm = 2
		dip = 0
		mil = 5
    }
}

2470.1.1 = {
	monarch = {
 		name = "Lothakar"
		dynasty = "Ysgramor"
		birth_date = 2447.1.1
		adm = 2
		dip = 3
		mil = 5
    }
	queen = {
 		name = "Ig"
		dynasty = "Hjeijes"
		birth_date = 2452.1.1
		adm = 2
		dip = 0
		mil = 6
		female = yes
    }
	heir = {
 		name = "Asfredda"
		monarch_name = "Asfredda I"
		dynasty = "Ysgramor"
		birth_date = 2469.1.1
		death_date = 2505.1.1
		claim = 100
		adm = 6
		dip = 5
		mil = 5
		female = yes
    }
}

