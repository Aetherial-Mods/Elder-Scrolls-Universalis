government = tribal
government_rank = 1
mercantilism = 1
technology_group = daedric_tg
religion = mehrunes_dagon_cult
primary_culture = clanfear
capital = 3890

54.1.1 = {
	monarch = {
 		name = "Dendrigaak"
		dynasty = "Jul-Keth"
		birth_date = 29.1.1
		adm = 5
		dip = 6
		mil = 1
    }
}

137.1.1 = {
	monarch = {
 		name = "Ohgesa"
		dynasty = "Jul-Fidlezeth"
		birth_date = 113.1.1
		adm = 3
		dip = 5
		mil = 5
		female = yes
    }
}

193.1.1 = {
	monarch = {
 		name = "Krogaabad"
		dynasty = "Jul-Zecre"
		birth_date = 147.1.1
		adm = 2
		dip = 4
		mil = 1
    }
}

284.1.1 = {
	monarch = {
 		name = "Rhuglu"
		dynasty = "Jul-Bromrix"
		birth_date = 247.1.1
		adm = 0
		dip = 4
		mil = 5
		female = yes
    }
}

328.1.1 = {
	monarch = {
 		name = "Guhro"
		dynasty = "Jul-Eblu"
		birth_date = 279.1.1
		adm = 5
		dip = 3
		mil = 2
		female = yes
    }
}

385.1.1 = {
	monarch = {
 		name = "Ohgesa"
		dynasty = "Jul-Bex"
		birth_date = 334.1.1
		adm = 3
		dip = 2
		mil = 5
		female = yes
    }
}

429.1.1 = {
	monarch = {
 		name = "Aitu"
		dynasty = "Jul-Thordi"
		birth_date = 387.1.1
		adm = 5
		dip = 3
		mil = 0
		female = yes
    }
}

522.1.1 = {
	monarch = {
 		name = "Same"
		dynasty = "Jul-Tinla"
		birth_date = 480.1.1
		adm = 4
		dip = 3
		mil = 4
		female = yes
    }
}

604.1.1 = {
	monarch = {
 		name = "Goldrait"
		dynasty = "Jul-Gah"
		birth_date = 575.1.1
		adm = 2
		dip = 2
		mil = 0
    }
}

674.1.1 = {
	monarch = {
 		name = "Khastra"
		dynasty = "Jul-Ren"
		birth_date = 644.1.1
		adm = 0
		dip = 1
		mil = 4
    }
}

772.1.1 = {
	monarch = {
 		name = "Cevaid"
		dynasty = "Jul-Vittazu"
		birth_date = 734.1.1
		adm = 1
		dip = 5
		mil = 2
    }
}

809.1.1 = {
	monarch = {
 		name = "Ubigo"
		dynasty = "Jul-Muotaso"
		birth_date = 771.1.1
		adm = 6
		dip = 4
		mil = 5
    }
}

880.1.1 = {
	monarch = {
 		name = "Gningurk"
		dynasty = "Jul-Zecre"
		birth_date = 853.1.1
		adm = 4
		dip = 3
		mil = 2
    }
}

971.1.1 = {
	monarch = {
 		name = "Tork"
		dynasty = "Jul-Hemla"
		birth_date = 934.1.1
		adm = 2
		dip = 3
		mil = 6
    }
}

1039.1.1 = {
	monarch = {
 		name = "Benca"
		dynasty = "Jul-Kimgu"
		birth_date = 993.1.1
		adm = 1
		dip = 2
		mil = 2
		female = yes
    }
}

1127.1.1 = {
	monarch = {
 		name = "Ocro"
		dynasty = "Jul-Nah"
		birth_date = 1082.1.1
		adm = 6
		dip = 1
		mil = 6
		female = yes
    }
}

1186.1.1 = {
	monarch = {
 		name = "Rosri"
		dynasty = "Jul-Uzemluox"
		birth_date = 1157.1.1
		adm = 4
		dip = 0
		mil = 3
		female = yes
    }
}

1277.1.1 = {
	monarch = {
 		name = "Tork"
		dynasty = "Jul-Kimgu"
		birth_date = 1248.1.1
		adm = 6
		dip = 2
		mil = 4
    }
}

1344.1.1 = {
	monarch = {
 		name = "Kharogesh"
		dynasty = "Jul-Goruth"
		birth_date = 1325.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1386.1.1 = {
	monarch = {
 		name = "Him"
		dynasty = "Jul-Ful"
		birth_date = 1352.1.1
		adm = 3
		dip = 0
		mil = 5
    }
}

1448.1.1 = {
	monarch = {
 		name = "Vusleri"
		dynasty = "Jul-Bex"
		birth_date = 1404.1.1
		adm = 1
		dip = 6
		mil = 1
		female = yes
    }
}

1540.1.1 = {
	monarch = {
 		name = "Crograrm"
		dynasty = "Jul-Muotaso"
		birth_date = 1517.1.1
		adm = 6
		dip = 5
		mil = 5
    }
}

1584.1.1 = {
	monarch = {
 		name = "Adhazith"
		dynasty = "Jul-Engo"
		birth_date = 1563.1.1
		adm = 0
		dip = 2
		mil = 3
    }
}

1656.1.1 = {
	monarch = {
 		name = "Rhaynlux"
		dynasty = "Jul-Hugi"
		birth_date = 1607.1.1
		adm = 5
		dip = 2
		mil = 6
		female = yes
    }
}

1721.1.1 = {
	monarch = {
 		name = "Brigho"
		dynasty = "Jul-Pirul"
		birth_date = 1675.1.1
		adm = 3
		dip = 1
		mil = 3
		female = yes
    }
}

1791.1.1 = {
	monarch = {
 		name = "Krogaabad"
		dynasty = "Jul-Gecloth"
		birth_date = 1744.1.1
		adm = 2
		dip = 0
		mil = 0
    }
}

1873.1.1 = {
	monarch = {
 		name = "Lot"
		dynasty = "Jul-Iljke"
		birth_date = 1823.1.1
		adm = 0
		dip = 6
		mil = 3
    }
}

1945.1.1 = {
	monarch = {
 		name = "Muzrirs"
		dynasty = "Jul-Nulta"
		birth_date = 1893.1.1
		adm = 5
		dip = 5
		mil = 0
    }
}

1989.1.1 = {
	monarch = {
 		name = "Ungrin"
		dynasty = "Jul-Ren"
		birth_date = 1938.1.1
		adm = 0
		dip = 0
		mil = 2
    }
}

2051.1.1 = {
	monarch = {
 		name = "Nalavirk"
		dynasty = "Jul-Tinla"
		birth_date = 2027.1.1
		adm = 5
		dip = 6
		mil = 5
    }
}

2133.1.1 = {
	monarch = {
 		name = "Iljkola"
		dynasty = "Jul-Catrush"
		birth_date = 2081.1.1
		adm = 3
		dip = 5
		mil = 2
		female = yes
    }
}

2178.1.1 = {
	monarch = {
 		name = "Vuzanco"
		dynasty = "Jul-Hatru"
		birth_date = 2135.1.1
		adm = 2
		dip = 4
		mil = 6
		female = yes
    }
}

2267.1.1 = {
	monarch = {
 		name = "Olnigi"
		dynasty = "Jul-Teco"
		birth_date = 2244.1.1
		adm = 0
		dip = 4
		mil = 2
		female = yes
    }
}

2310.1.1 = {
	monarch = {
 		name = "Khuugal"
		dynasty = "Jul-Uzemluox"
		birth_date = 2279.1.1
		adm = 5
		dip = 3
		mil = 6
    }
}

2395.1.1 = {
	monarch = {
 		name = "Banvid"
		dynasty = "Jul-Nah"
		birth_date = 2353.1.1
		adm = 3
		dip = 2
		mil = 2
    }
}

2486.1.1 = {
	monarch = {
 		name = "Vuzanco"
		dynasty = "Jul-Bih"
		birth_date = 2456.1.1
		adm = 3
		dip = 5
		mil = 3
		female = yes
    }
}

