government = monarchy
government_rank = 1
mercantilism = 1
technology_group = cyrodiil_tg
religion = meridia_cult
primary_culture = ayleid
capital = 5691

54.1.1 = {
	monarch = {
 		name = "Narilmor"
		dynasty = "Mea-Allevar"
		birth_date = 7.1.1
		adm = 6
		dip = 3
		mil = 3
    }
}

200.7.1 = {
	monarch = {
 		name = "Emma"
		dynasty = "Vae-Vanua"
		birth_date = 111.1.1
		adm = 4
		dip = 1
		mil = 4
		female = yes
    }
}

269.1.1 = {
	monarch = {
 		name = "Cur"
		dynasty = "Mea-Correllia"
		birth_date = 245.1.1
		adm = 1
		dip = 6
		mil = 5
    }
}

333.1.1 = {
	monarch = {
 		name = "Djel"
		dynasty = "Ula-Kvatch"
		birth_date = 299.1.1
		adm = 3
		dip = 0
		mil = 6
    }
}

427.1.1 = {
	monarch = {
 		name = "Emma"
		dynasty = "Ula-Rusifus"
		birth_date = 403.1.1
		adm = 0
		dip = 2
		mil = 6
		female = yes
    }
}

478.1.1 = {
	monarch = {
 		name = "Dondont"
		dynasty = "Vae-Weyandawik"
		birth_date = 447.1.1
		adm = 5
		dip = 2
		mil = 3
    }
	queen = {
 		name = "Dynnyan"
		dynasty = "Vae-Weyandawik"
		birth_date = 459.1.1
		adm = 2
		dip = 0
		mil = 2
		female = yes
    }
	heir = {
 		name = "Hadhuul"
		monarch_name = "Hadhuul I"
		dynasty = "Vae-Weyandawik"
		birth_date = 470.1.1
		death_date = 567.1.1
		claim = 100
		adm = 6
		dip = 6
		mil = 1
    }
}

567.1.1 = {
	monarch = {
 		name = "Morilye"
		dynasty = "Ula-Miscarcand"
		birth_date = 530.1.1
		adm = 5
		dip = 2
		mil = 1
		female = yes
    }
}

618.1.1 = {
	monarch = {
 		name = "Mennith"
		dynasty = "Ula-Restidina"
		birth_date = 592.1.1
		adm = 3
		dip = 1
		mil = 5
		female = yes
    }
	queen = {
 		name = "Eguggend"
		dynasty = "Ula-Restidina"
		birth_date = 581.1.1
		adm = 0
		dip = 0
		mil = 4
    }
	heir = {
 		name = "Irreshyva"
		monarch_name = "Irreshyva I"
		dynasty = "Ula-Restidina"
		birth_date = 610.1.1
		death_date = 694.1.1
		claim = 100
		adm = 4
		dip = 5
		mil = 3
		female = yes
    }
}

694.1.1 = {
	monarch = {
 		name = "Hosnuugaura"
		dynasty = "Vae-Vilverin"
		birth_date = 664.1.1
		adm = 2
		dip = 5
		mil = 6
    }
}

775.1.1 = {
	monarch = {
 		name = "Yrru"
		dynasty = "Vae-Trumbe"
		birth_date = 730.1.1
		adm = 1
		dip = 4
		mil = 3
		female = yes
    }
	queen = {
 		name = "Vossyduron"
		dynasty = "Vae-Trumbe"
		birth_date = 730.1.1
		adm = 4
		dip = 2
		mil = 2
    }
	heir = {
 		name = "Nugant"
		monarch_name = "Nugant I"
		dynasty = "Vae-Trumbe"
		birth_date = 760.1.1
		death_date = 849.1.1
		claim = 100
		adm = 1
		dip = 1
		mil = 1
    }
}

849.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Lanath"
		monarch_name = "Lanath I"
		dynasty = "Ula-Istirus"
		birth_date = 845.1.1
		death_date = 863.1.1
		claim = 100
		adm = 1
		dip = 1
		mil = 2
		female = yes
    }
}

863.1.1 = {
	monarch = {
 		name = "Tjisneymaulont"
		dynasty = "Ula-Miscarcand"
		birth_date = 827.1.1
		adm = 4
		dip = 2
		mil = 5
    }
	queen = {
 		name = "Lonnosson"
		dynasty = "Ula-Miscarcand"
		birth_date = 825.1.1
		adm = 1
		dip = 1
		mil = 3
		female = yes
    }
	heir = {
 		name = "Nisnen"
		monarch_name = "Nisnen I"
		dynasty = "Ula-Miscarcand"
		birth_date = 852.1.1
		death_date = 923.1.1
		claim = 100
		adm = 4
		dip = 6
		mil = 2
    }
}

923.1.1 = {
	monarch = {
 		name = "Symmu"
		dynasty = "Ula-Larilatia"
		birth_date = 905.1.1
		adm = 4
		dip = 2
		mil = 3
		female = yes
    }
}

984.1.1 = {
	monarch = {
 		name = "Uzynhoth"
		dynasty = "Vae-Vassorman"
		birth_date = 948.1.1
		adm = 2
		dip = 2
		mil = 6
		female = yes
    }
	queen = {
 		name = "Cyrheganund"
		dynasty = "Vae-Vassorman"
		birth_date = 948.1.1
		adm = 6
		dip = 0
		mil = 5
    }
	heir = {
 		name = "Quaronaldil"
		monarch_name = "Quaronaldil I"
		dynasty = "Vae-Vassorman"
		birth_date = 981.1.1
		death_date = 1050.1.1
		claim = 100
		adm = 3
		dip = 6
		mil = 4
    }
}

1050.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Ymmilune"
		monarch_name = "Ymmilune I"
		dynasty = "Mea-Amane"
		birth_date = 1044.1.1
		death_date = 1062.1.1
		claim = 100
		adm = 1
		dip = 5
		mil = 1
		female = yes
    }
}

1062.1.1 = {
	monarch = {
 		name = "Luudagarund"
		dynasty = "Vae-Sena"
		birth_date = 1031.1.1
		adm = 4
		dip = 6
		mil = 3
    }
	queen = {
 		name = "Illath"
		dynasty = "Vae-Sena"
		birth_date = 1035.1.1
		adm = 1
		dip = 5
		mil = 2
		female = yes
    }
}

1110.1.1 = {
	monarch = {
 		name = "Yhu"
		dynasty = "Mea-Etirina"
		birth_date = 1081.1.1
		adm = 6
		dip = 4
		mil = 6
		female = yes
    }
	queen = {
 		name = "Cossin"
		dynasty = "Mea-Etirina"
		birth_date = 1070.1.1
		adm = 5
		dip = 0
		mil = 6
    }
	heir = {
 		name = "Nym"
		monarch_name = "Nym I"
		dynasty = "Mea-Etirina"
		birth_date = 1102.1.1
		death_date = 1151.1.1
		claim = 100
		adm = 1
		dip = 2
		mil = 2
    }
}

1151.1.1 = {
	monarch = {
 		name = "Immol"
		dynasty = "		"
		birth_date = 1111.1.1
		adm = 0
		dip = 5
		mil = 2
		female = yes
    }
	queen = {
 		name = "Masnam"
		dynasty = "		"
		birth_date = 1132.1.1
		adm = 4
		dip = 4
		mil = 1
    }
	heir = {
 		name = "Gand"
		monarch_name = "Gand I"
		dynasty = "		"
		birth_date = 1149.1.1
		death_date = 1239.1.1
		claim = 100
		adm = 5
		dip = 0
		mil = 2
    }
}

1239.1.1 = {
	monarch = {
 		name = "Undorrau"
		dynasty = "Ula-Larilatia"
		birth_date = 1212.1.1
		adm = 4
		dip = 4
		mil = 2
    }
}

1301.1.1 = {
	monarch = {
 		name = "Gordhaur"
		dynasty = "Mea-Egnagia"
		birth_date = 1276.1.1
		adm = 2
		dip = 3
		mil = 6
    }
}

1374.1.1 = {
	monarch = {
 		name = "Omashaul"
		dynasty = "Vae-Sercen"
		birth_date = 1338.1.1
		adm = 0
		dip = 2
		mil = 2
    }
}

1421.1.1 = {
	monarch = {
 		name = "Myzas"
		dynasty = "Mea-Caumana"
		birth_date = 1381.1.1
		adm = 6
		dip = 1
		mil = 6
		female = yes
    }
}

1489.1.1 = {
	monarch = {
 		name = "Muzos"
		dynasty = "Mea-Culotte"
		birth_date = 1445.1.1
		adm = 4
		dip = 0
		mil = 3
		female = yes
    }
	queen = {
 		name = "Vossyduron"
		dynasty = "Mea-Culotte"
		birth_date = 1444.1.1
		adm = 1
		dip = 6
		mil = 2
    }
}

1583.1.1 = {
	monarch = {
 		name = "Djamduusceges"
		dynasty = "Mea-Amane"
		birth_date = 1559.1.1
		adm = 6
		dip = 5
		mil = 5
    }
}

1619.1.1 = {
	monarch = {
 		name = "Lunrun"
		dynasty = "Mea-Bravil"
		birth_date = 1582.1.1
		adm = 0
		dip = 2
		mil = 3
    }
}

1660.1.1 = {
	monarch = {
 		name = "Fin"
		dynasty = "Mea-Correllia"
		birth_date = 1629.1.1
		adm = 5
		dip = 1
		mil = 0
    }
}

1756.1.1 = {
	monarch = {
 		name = "Tjughandonos"
		dynasty = "Ula-Jorarienna"
		birth_date = 1733.1.1
		adm = 3
		dip = 1
		mil = 4
    }
	queen = {
 		name = "Puvusadya"
		dynasty = "Ula-Jorarienna"
		birth_date = 1704.1.1
		adm = 0
		dip = 6
		mil = 2
		female = yes
    }
	heir = {
 		name = "Fidhi"
		monarch_name = "Fidhi I"
		dynasty = "Ula-Jorarienna"
		birth_date = 1754.1.1
		death_date = 1825.1.1
		claim = 100
		adm = 4
		dip = 5
		mil = 1
    }
}

1825.1.1 = {
	monarch = {
 		name = "Mezol"
		dynasty = "Ula-Macelius"
		birth_date = 1794.1.1
		adm = 3
		dip = 1
		mil = 2
		female = yes
    }
}

1914.1.1 = {
	monarch = {
 		name = "Valasha"
		dynasty = "Ula-Messiena"
		birth_date = 1876.1.1
		adm = 2
		dip = 0
		mil = 5
		female = yes
    }
}

1960.1.1 = {
	monarch = {
 		name = "Cavannorel"
		dynasty = "Vae-Waelori"
		birth_date = 1918.1.1
		adm = 0
		dip = 0
		mil = 2
		female = yes
    }
}

2013.1.1 = {
	monarch = {
 		name = "Min"
		dynasty = "Vae-Silelia"
		birth_date = 1963.1.1
		adm = 5
		dip = 6
		mil = 6
    }
	queen = {
 		name = "Nanalne"
		dynasty = "Vae-Silelia"
		birth_date = 1979.1.1
		adm = 2
		dip = 4
		mil = 5
		female = yes
    }
	heir = {
 		name = "Vanhur"
		monarch_name = "Vanhur I"
		dynasty = "Vae-Silelia"
		birth_date = 2010.1.1
		death_date = 2068.1.1
		claim = 100
		adm = 5
		dip = 6
		mil = 0
    }
}

2068.1.1 = {
	monarch = {
 		name = "Cossin"
		dynasty = "Vae-Waelori"
		birth_date = 2019.1.1
		adm = 4
		dip = 2
		mil = 0
    }
	queen = {
 		name = "Cavannorel"
		dynasty = "Vae-Waelori"
		birth_date = 2015.1.1
		adm = 1
		dip = 1
		mil = 6
		female = yes
    }
	heir = {
 		name = "Heystalero"
		monarch_name = "Heystalero I"
		dynasty = "Vae-Waelori"
		birth_date = 2065.1.1
		death_date = 2154.1.1
		claim = 100
		adm = 5
		dip = 6
		mil = 5
    }
}

2154.1.1 = {
	monarch = {
 		name = "Honnomule"
		dynasty = "Mea-Essassius"
		birth_date = 2130.1.1
		adm = 1
		dip = 0
		mil = 1
		female = yes
    }
	queen = {
 		name = "Djun"
		dynasty = "Mea-Essassius"
		birth_date = 2120.1.1
		adm = 5
		dip = 6
		mil = 6
    }
	heir = {
 		name = "Yve"
		monarch_name = "Yve I"
		dynasty = "Mea-Essassius"
		birth_date = 2152.1.1
		death_date = 2208.1.1
		claim = 100
		adm = 1
		dip = 4
		mil = 5
		female = yes
    }
}

2208.1.1 = {
	monarch = {
 		name = "Tamhuundogo"
		dynasty = "Mea-Crownhaven"
		birth_date = 2158.1.1
		adm = 4
		dip = 6
		mil = 1
    }
}

2305.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Inhuagadom"
		monarch_name = "Inhuagadom I"
		dynasty = "Mea-Gince"
		birth_date = 2298.1.1
		death_date = 2316.1.1
		claim = 100
		adm = 5
		dip = 3
		mil = 6
    }
}

2316.1.1 = {
	monarch = {
 		name = "Vem"
		dynasty = "Vae-Sylolvia"
		birth_date = 2264.1.1
		adm = 4
		dip = 6
		mil = 6
    }
}

2405.1.1 = {
	monarch = {
 		name = "Umarik"
		dynasty = "Vae-Skingrad"
		birth_date = 2352.1.1
		adm = 3
		dip = 6
		mil = 3
    }
	queen = {
 		name = "Ymi"
		dynasty = "Vae-Skingrad"
		birth_date = 2374.1.1
		adm = 6
		dip = 4
		mil = 2
		female = yes
    }
	heir = {
 		name = "Cuussint"
		monarch_name = "Cuussint I"
		dynasty = "Vae-Skingrad"
		birth_date = 2405.1.1
		death_date = 2460.1.1
		claim = 100
		adm = 2
		dip = 5
		mil = 4
    }
}

2460.1.1 = {
	monarch = {
 		name = "Qegylomo"
		dynasty = "Mea-Alessia"
		birth_date = 2412.1.1
		adm = 2
		dip = 2
		mil = 4
    }
}

2496.1.1 = {
	monarch = {
 		name = "Cymylmuumer"
		dynasty = "Mea-Gince"
		birth_date = 2476.1.1
		adm = 0
		dip = 1
		mil = 1
    }
}

