government = monarchy
government_rank = 3
mercantilism = 1
technology_group = yokudan_tg
religion = redguard_pantheon
primary_culture = redguard
capital = 6178

54.1.1 = {
	monarch = {
 		name = "Saryeh"
		dynasty = "Sticert"
		birth_date = 31.1.1
		adm = 3
		dip = 3
		mil = 1
    }
	queen = {
 		name = "Radwet"
		dynasty = "Sticert"
		birth_date = 2.1.1
		adm = 0
		dip = 2
		mil = 0
		female = yes
    }
	heir = {
 		name = "Gilzurjj"
		monarch_name = "Gilzurjj I"
		dynasty = "Sticert"
		birth_date = 41.1.1
		death_date = 98.1.1
		claim = 100
		adm = 0
		dip = 5
		mil = 1
    }
}

98.1.1 = {
	monarch = {
 		name = "Shazeem"
		dynasty = "Sosccean"
		birth_date = 61.1.1
		adm = 0
		dip = 2
		mil = 1
    }
	queen = {
 		name = "Fabrelle"
		dynasty = "Sosccean"
		birth_date = 73.1.1
		adm = 4
		dip = 0
		mil = 0
		female = yes
    }
	heir = {
 		name = "Rok'dun"
		monarch_name = "Rok'dun I"
		dynasty = "Sosccean"
		birth_date = 89.1.1
		death_date = 174.1.1
		claim = 100
		adm = 0
		dip = 6
		mil = 6
    }
}

174.1.1 = {
	monarch = {
 		name = "Hayazzin"
		dynasty = "Rlogtha"
		birth_date = 148.1.1
		adm = 3
		dip = 0
		mil = 1
    }
	queen = {
 		name = "Qymala"
		dynasty = "Rlogtha"
		birth_date = 147.1.1
		adm = 0
		dip = 6
		mil = 0
		female = yes
    }
}

219.1.1 = {
	monarch = {
 		name = "Nanshiq"
		dynasty = "Manielan"
		birth_date = 190.1.1
		adm = 5
		dip = 5
		mil = 4
    }
}

266.1.1 = {
	monarch = {
 		name = "Jaganvir"
		dynasty = "Curtyvond"
		birth_date = 214.1.1
		adm = 4
		dip = 4
		mil = 1
    }
}

321.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Hasuq"
		monarch_name = "Hasuq I"
		dynasty = "Toran"
		birth_date = 310.1.1
		death_date = 328.1.1
		claim = 100
		adm = 4
		dip = 1
		mil = 6
    }
}

328.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Monat"
		monarch_name = "Monat I"
		dynasty = "Rirmem"
		birth_date = 317.1.1
		death_date = 335.1.1
		claim = 100
		adm = 3
		dip = 0
		mil = 2
    }
}

335.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Hlaureg"
		monarch_name = "Hlaureg I"
		dynasty = "Killba"
		birth_date = 332.1.1
		death_date = 350.1.1
		claim = 100
		adm = 1
		dip = 0
		mil = 6
    }
}

350.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Azani"
		monarch_name = "Azani I"
		dynasty = "Namanet"
		birth_date = 346.1.1
		death_date = 364.1.1
		claim = 100
		adm = 6
		dip = 6
		mil = 2
    }
}

364.1.1 = {
	monarch = {
 		name = "Nebzezir"
		dynasty = "Endotias"
		birth_date = 327.1.1
		adm = 5
		dip = 2
		mil = 3
    }
}

407.1.1 = {
	monarch = {
 		name = "Jaline"
		dynasty = "Kithithon"
		birth_date = 354.1.1
		adm = 4
		dip = 1
		mil = 6
		female = yes
    }
	queen = {
 		name = "Sahabih"
		dynasty = "Kithithon"
		birth_date = 366.1.1
		adm = 1
		dip = 0
		mil = 5
    }
	heir = {
 		name = "Hisal"
		monarch_name = "Hisal I"
		dynasty = "Kithithon"
		birth_date = 401.1.1
		death_date = 491.1.1
		claim = 100
		adm = 4
		dip = 5
		mil = 4
    }
}

491.1.1 = {
	monarch = {
 		name = "Tralan"
		dynasty = "Kithithon"
		birth_date = 461.1.1
		adm = 0
		dip = 0
		mil = 0
    }
}

545.1.1 = {
	monarch = {
 		name = "Nebhavar"
		dynasty = "Athobur"
		birth_date = 496.1.1
		adm = 5
		dip = 6
		mil = 3
    }
}

583.1.1 = {
	monarch = {
 		name = "Margonet"
		dynasty = "Childar"
		birth_date = 540.1.1
		adm = 5
		dip = 2
		mil = 4
		female = yes
    }
}

667.1.1 = {
	monarch = {
 		name = "Caseh"
		dynasty = "Casirus"
		birth_date = 642.1.1
		adm = 6
		dip = 6
		mil = 2
    }
}

744.1.1 = {
	monarch = {
 		name = "Jireq"
		dynasty = "Orinolm"
		birth_date = 700.1.1
		adm = 4
		dip = 6
		mil = 5
    }
}

819.1.1 = {
	monarch = {
 		name = "Darewan"
		dynasty = "Chestem"
		birth_date = 773.1.1
		adm = 2
		dip = 5
		mil = 2
    }
	queen = {
 		name = "Marbita"
		dynasty = "Chestem"
		birth_date = 777.1.1
		adm = 6
		dip = 3
		mil = 1
		female = yes
    }
	heir = {
 		name = "Balanvir"
		monarch_name = "Balanvir I"
		dynasty = "Chestem"
		birth_date = 804.1.1
		death_date = 889.1.1
		claim = 100
		adm = 3
		dip = 2
		mil = 0
    }
}

889.1.1 = {
	monarch = {
 		name = "Nilu"
		dynasty = "Sosccean"
		birth_date = 836.1.1
		adm = 5
		dip = 3
		mil = 2
		female = yes
    }
	queen = {
 		name = "Almur"
		dynasty = "Sosccean"
		birth_date = 859.1.1
		adm = 2
		dip = 2
		mil = 1
    }
	heir = {
 		name = "Muhmeylah"
		monarch_name = "Muhmeylah I"
		dynasty = "Sosccean"
		birth_date = 874.1.1
		death_date = 970.1.1
		claim = 100
		adm = 6
		dip = 0
		mil = 0
		female = yes
    }
}

970.1.1 = {
	monarch = {
 		name = "Damar"
		dynasty = "Sticert"
		birth_date = 934.1.1
		adm = 2
		dip = 2
		mil = 2
    }
}

1018.1.1 = {
	monarch = {
 		name = "Ulbazar"
		dynasty = "Dhosher"
		birth_date = 986.1.1
		adm = 4
		dip = 3
		mil = 4
    }
}

1107.1.1 = {
	monarch = {
 		name = "Nesrin"
		dynasty = "Cilp'kern"
		birth_date = 1079.1.1
		adm = 2
		dip = 2
		mil = 1
		female = yes
    }
}

1203.1.1 = {
	monarch = {
 		name = "Mazrahil"
		dynasty = "Kharus"
		birth_date = 1182.1.1
		adm = 5
		dip = 4
		mil = 3
    }
}

1277.1.1 = {
	monarch = {
 		name = "Hasilium"
		dynasty = "Kithithon"
		birth_date = 1233.1.1
		adm = 3
		dip = 3
		mil = 0
    }
}

1319.1.1 = {
	monarch = {
 		name = "Ati"
		dynasty = "Namanet"
		birth_date = 1283.1.1
		adm = 1
		dip = 2
		mil = 3
		female = yes
    }
	queen = {
 		name = "Zalbec"
		dynasty = "Namanet"
		birth_date = 1294.1.1
		adm = 1
		dip = 5
		mil = 3
    }
	heir = {
 		name = "Isleif"
		monarch_name = "Isleif I"
		dynasty = "Namanet"
		birth_date = 1307.1.1
		death_date = 1370.1.1
		claim = 100
		adm = 2
		dip = 6
		mil = 1
    }
}

1370.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Brazzideh"
		monarch_name = "Brazzideh I"
		dynasty = "Stifyli"
		birth_date = 1366.1.1
		death_date = 1384.1.1
		claim = 100
		adm = 0
		dip = 5
		mil = 5
		female = yes
    }
}

1384.1.1 = {
	monarch = {
 		name = "Sorian"
		dynasty = "Athh"
		birth_date = 1361.1.1
		adm = 0
		dip = 2
		mil = 5
    }
	queen = {
 		name = "Faliara"
		dynasty = "Athh"
		birth_date = 1331.1.1
		adm = 4
		dip = 0
		mil = 4
		female = yes
    }
}

1447.1.1 = {
	monarch = {
 		name = "Caseh"
		dynasty = "Curtyvond"
		birth_date = 1413.1.1
		adm = 2
		dip = 6
		mil = 1
    }
}

1525.1.1 = {
	monarch = {
 		name = "Talumineh"
		dynasty = "B'ollka"
		birth_date = 1493.1.1
		adm = 0
		dip = 6
		mil = 4
		female = yes
    }
}

1609.1.1 = {
	monarch = {
 		name = "Nasmat"
		dynasty = "Rlogtha"
		birth_date = 1556.1.1
		adm = 5
		dip = 5
		mil = 1
		female = yes
    }
}

1650.1.1 = {
	monarch = {
 		name = "Jalid"
		dynasty = "Cyrcy"
		birth_date = 1609.1.1
		adm = 4
		dip = 4
		mil = 5
    }
	queen = {
 		name = "Rozhbeh"
		dynasty = "Cyrcy"
		birth_date = 1615.1.1
		adm = 0
		dip = 2
		mil = 4
		female = yes
    }
	heir = {
 		name = "Hifa"
		monarch_name = "Hifa I"
		dynasty = "Cyrcy"
		birth_date = 1635.1.1
		death_date = 1726.1.1
		claim = 100
		adm = 4
		dip = 1
		mil = 3
    }
}

1726.1.1 = {
	monarch = {
 		name = "Tahirah"
		dynasty = "Glanna"
		birth_date = 1704.1.1
		adm = 0
		dip = 2
		mil = 5
		female = yes
    }
}

1764.1.1 = {
	monarch = {
 		name = "Nazd"
		dynasty = "Toran"
		birth_date = 1713.1.1
		adm = 2
		dip = 4
		mil = 6
    }
	queen = {
 		name = "Manuush"
		dynasty = "Toran"
		birth_date = 1718.1.1
		adm = 2
		dip = 0
		mil = 1
		female = yes
    }
	heir = {
 		name = "Bahir"
		monarch_name = "Bahir I"
		dynasty = "Toran"
		birth_date = 1764.1.1
		death_date = 1839.1.1
		claim = 100
		adm = 6
		dip = 6
		mil = 0
    }
}

1839.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Tabar"
		monarch_name = "Tabar I"
		dynasty = "Trateve"
		birth_date = 1836.1.1
		death_date = 1854.1.1
		claim = 100
		adm = 4
		dip = 5
		mil = 3
    }
}

1854.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Mozhenie"
		monarch_name = "Mozhenie I"
		dynasty = "Jaganuin"
		birth_date = 1839.1.1
		death_date = 1857.1.1
		claim = 100
		adm = 6
		dip = 6
		mil = 5
		female = yes
    }
}

1857.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Husni"
		monarch_name = "Husni I"
		dynasty = "Flargir"
		birth_date = 1857.1.1
		death_date = 1875.1.1
		claim = 100
		adm = 4
		dip = 5
		mil = 1
    }
}

1875.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Bahadur"
		monarch_name = "Bahadur I"
		dynasty = "Kevsa"
		birth_date = 1864.1.1
		death_date = 1882.1.1
		claim = 100
		adm = 2
		dip = 0
		mil = 1
    }
}

1882.1.1 = {
	monarch = {
 		name = "Abradun"
		dynasty = "Kayehon"
		birth_date = 1829.1.1
		adm = 1
		dip = 4
		mil = 2
    }
}

1919.1.1 = {
	monarch = {
 		name = "Rithussah"
		dynasty = "Irganan"
		birth_date = 1896.1.1
		adm = 6
		dip = 3
		mil = 5
		female = yes
    }
	queen = {
 		name = "Curtis"
		dynasty = "Irganan"
		birth_date = 1881.1.1
		adm = 3
		dip = 1
		mil = 4
    }
}

1960.1.1 = {
	monarch = {
 		name = "Asiah"
		dynasty = "Manig"
		birth_date = 1927.1.1
		adm = 1
		dip = 1
		mil = 1
		female = yes
    }
}

2015.1.1 = {
	monarch = {
 		name = "Samayna"
		dynasty = "Jaganuin"
		birth_date = 1992.1.1
		adm = 0
		dip = 0
		mil = 5
		female = yes
    }
	queen = {
 		name = "Eilram"
		dynasty = "Jaganuin"
		birth_date = 1982.1.1
		adm = 3
		dip = 5
		mil = 4
    }
}

2108.1.1 = {
	monarch = {
 		name = "Behrzharah"
		dynasty = "Dindal"
		birth_date = 2079.1.1
		adm = 2
		dip = 5
		mil = 0
    }
	queen = {
 		name = "Louda"
		dynasty = "Dindal"
		birth_date = 2082.1.1
		adm = 6
		dip = 3
		mil = 6
		female = yes
    }
	heir = {
 		name = "Ashanta"
		monarch_name = "Ashanta I"
		dynasty = "Dindal"
		birth_date = 2101.1.1
		death_date = 2180.1.1
		claim = 100
		adm = 0
		dip = 4
		mil = 4
		female = yes
    }
}

2180.1.1 = {
	monarch = {
 		name = "Nahroon"
		dynasty = "Hillther"
		birth_date = 2129.1.1
		adm = 5
		dip = 3
		mil = 0
    }
}

2268.1.1 = {
	monarch = {
 		name = "Inala"
		dynasty = "Shartta"
		birth_date = 2230.1.1
		adm = 0
		dip = 4
		mil = 2
    }
}

2353.1.1 = {
	monarch = {
 		name = "Braith"
		dynasty = "Tralnthal"
		birth_date = 2309.1.1
		adm = 5
		dip = 4
		mil = 6
		female = yes
    }
	queen = {
 		name = "Afelir"
		dynasty = "Tralnthal"
		birth_date = 2319.1.1
		adm = 2
		dip = 2
		mil = 5
    }
	heir = {
 		name = "Midrasud"
		monarch_name = "Midrasud I"
		dynasty = "Tralnthal"
		birth_date = 2345.1.1
		death_date = 2414.1.1
		claim = 100
		adm = 2
		dip = 6
		mil = 6
    }
}

2414.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Hefassah"
		monarch_name = "Hefassah I"
		dynasty = "Lidekwon"
		birth_date = 2402.1.1
		death_date = 2420.1.1
		claim = 100
		adm = 4
		dip = 0
		mil = 0
		female = yes
    }
}

2420.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Rada"
		monarch_name = "Rada I"
		dynasty = "Cilp'kern"
		birth_date = 2405.1.1
		death_date = 2423.1.1
		claim = 100
		adm = 3
		dip = 6
		mil = 4
    }
}

2423.1.1 = {
	monarch = {
 		name = "Naseiran"
		dynasty = "Aviell"
		birth_date = 2403.1.1
		adm = 5
		dip = 0
		mil = 6
    }
}

