government = tribal
government_rank = 1
mercantilism = 1
technology_group = elven_tg
religion = nocturnal_cult
primary_culture = lilmothiit
capital = 6745

54.1.1 = {
	monarch = {
 		name = "Tyumyatompoi"
		dynasty = "Nadro-Topuvy"
		birth_date = 4.1.1
		adm = 6
		dip = 6
		mil = 1
    }
}

105.1.1 = {
	monarch = {
 		name = "Pampevomun"
		dynasty = "Nedre-Nato"
		birth_date = 77.1.1
		adm = 1
		dip = 0
		mil = 3
    }
}

158.1.1 = {
	monarch = {
 		name = "Mugebya"
		dynasty = "Nedre-Natu"
		birth_date = 105.1.1
		adm = 6
		dip = 0
		mil = 6
    }
}

220.1.1 = {
	monarch = {
 		name = "Papoi"
		dynasty = "Nadro-Syiru"
		birth_date = 178.1.1
		adm = 5
		dip = 6
		mil = 3
    }
}

289.1.1 = {
	monarch = {
 		name = "Nadun"
		dynasty = "Nedre-Nato"
		birth_date = 261.1.1
		adm = 0
		dip = 0
		mil = 5
		female = yes
    }
}

385.1.1 = {
	monarch = {
 		name = "Tangkadun"
		dynasty = "Nodru-Vevya"
		birth_date = 359.1.1
		adm = 6
		dip = 6
		mil = 2
		female = yes
    }
}

439.1.1 = {
	monarch = {
 		name = "Ngovyiar"
		dynasty = "Nudra-Nongilu"
		birth_date = 409.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

476.1.1 = {
	monarch = {
 		name = "Mpanyedun"
		dynasty = "Nedre-Mpu"
		birth_date = 458.1.1
		adm = 6
		dip = 0
		mil = 0
		female = yes
    }
}

541.1.1 = {
	monarch = {
 		name = "Ntontomun"
		dynasty = "Nudra-Ngkugha"
		birth_date = 492.1.1
		adm = 4
		dip = 6
		mil = 4
    }
}

628.1.1 = {
	monarch = {
 		name = "Mevyeka"
		dynasty = "Nadro-Sa"
		birth_date = 610.1.1
		adm = 2
		dip = 5
		mil = 1
    }
}

674.1.1 = {
	monarch = {
 		name = "Yangkompoi"
		dynasty = "Nodru-Yamp"
		birth_date = 654.1.1
		adm = 1
		dip = 5
		mil = 4
    }
}

759.1.1 = {
	monarch = {
 		name = "Tapoghan"
		dynasty = "Nedre-Mpumpum"
		birth_date = 729.1.1
		adm = 6
		dip = 4
		mil = 1
    }
}

841.1.1 = {
	monarch = {
 		name = "Ntontomun"
		dynasty = "Nedre-Lega"
		birth_date = 796.1.1
		adm = 4
		dip = 3
		mil = 4
    }
}

897.1.1 = {
	monarch = {
 		name = "Ngkadun"
		dynasty = "Nadro-Soyoli"
		birth_date = 861.1.1
		adm = 3
		dip = 6
		mil = 5
		female = yes
    }
}

980.1.1 = {
	monarch = {
 		name = "Goghan"
		dynasty = "Nadro-Rupang"
		birth_date = 958.1.1
		adm = 2
		dip = 6
		mil = 1
    }
}

1052.1.1 = {
	monarch = {
 		name = "Vyayuka"
		dynasty = "Nedre-Nato"
		birth_date = 1032.1.1
		adm = 0
		dip = 5
		mil = 5
    }
}

1103.1.1 = {
	monarch = {
 		name = "Syangkatyar"
		dynasty = "Nudra-Pama"
		birth_date = 1058.1.1
		adm = 5
		dip = 4
		mil = 2
		female = yes
    }
}

1150.1.1 = {
	monarch = {
 		name = "Ngkawaghan"
		dynasty = "Nodru-Vyi"
		birth_date = 1103.1.1
		adm = 3
		dip = 3
		mil = 5
    }
}

1226.1.1 = {
	monarch = {
 		name = "Lotun"
		dynasty = "Nedre-Lawu"
		birth_date = 1187.1.1
		adm = 2
		dip = 2
		mil = 2
		female = yes
    }
}

1278.1.1 = {
	monarch = {
 		name = "Vyayuka"
		dynasty = "Nudra-Nyinyasy"
		birth_date = 1236.1.1
		adm = 4
		dip = 4
		mil = 3
    }
}

1374.1.1 = {
	monarch = {
 		name = "Syadun"
		dynasty = "Nadro-Rusuny"
		birth_date = 1340.1.1
		adm = 2
		dip = 3
		mil = 0
		female = yes
    }
}

1415.1.1 = {
	monarch = {
 		name = "Vyontumpoi"
		dynasty = "Nadro-Syawan"
		birth_date = 1386.1.1
		adm = 0
		dip = 2
		mil = 4
    }
}

1450.1.1 = {
	monarch = {
 		name = "Mugebya"
		dynasty = "Nedre-Lotyo"
		birth_date = 1422.1.1
		adm = 3
		dip = 3
		mil = 6
    }
}

1491.1.1 = {
	monarch = {
 		name = "Gaseghan"
		dynasty = "Nudra-Nutita"
		birth_date = 1464.1.1
		adm = 1
		dip = 3
		mil = 3
    }
}

1533.1.1 = {
	monarch = {
 		name = "Tyitumpoi"
		dynasty = "Nodru-Va"
		birth_date = 1496.1.1
		adm = 3
		dip = 4
		mil = 4
    }
}

1575.1.1 = {
	monarch = {
 		name = "Pamoghan"
		dynasty = "Nedre-Mavy"
		birth_date = 1525.1.1
		adm = 1
		dip = 3
		mil = 1
    }
}

1655.1.1 = {
	monarch = {
 		name = "Vingovyadun"
		dynasty = "Nedre-Longkunguny"
		birth_date = 1634.1.1
		adm = 6
		dip = 2
		mil = 5
		female = yes
    }
}

1693.1.1 = {
	monarch = {
 		name = "Rasuluar"
		dynasty = "Nudra-Nayogh"
		birth_date = 1641.1.1
		adm = 5
		dip = 1
		mil = 1
		female = yes
    }
}

1731.1.1 = {
	monarch = {
 		name = "Myawomun"
		dynasty = "Nedre-Mponya"
		birth_date = 1695.1.1
		adm = 3
		dip = 1
		mil = 5
    }
}

1784.1.1 = {
	monarch = {
 		name = "Gesyighan"
		dynasty = "Nodru-Yisyusav"
		birth_date = 1736.1.1
		adm = 1
		dip = 0
		mil = 2
    }
}

1854.1.1 = {
	monarch = {
 		name = "Venegutun"
		dynasty = "Nadro-Tiny"
		birth_date = 1822.1.1
		adm = 1
		dip = 3
		mil = 2
		female = yes
    }
}

1934.1.1 = {
	monarch = {
 		name = "Syupoi"
		dynasty = "Nadro-Syuno"
		birth_date = 1912.1.1
		adm = 6
		dip = 2
		mil = 5
    }
}

2022.1.1 = {
	monarch = {
 		name = "Ntenyimpoi"
		dynasty = "Nudra-Nyuma"
		birth_date = 1990.1.1
		adm = 4
		dip = 2
		mil = 2
    }
}

2118.1.1 = {
	monarch = {
 		name = "Matyumpupoi"
		dynasty = "Nadro-Puwit"
		birth_date = 2082.1.1
		adm = 2
		dip = 1
		mil = 6
    }
}

2213.1.1 = {
	monarch = {
 		name = "Yampangkaghan"
		dynasty = "Nadro-Tomyongk"
		birth_date = 2167.1.1
		adm = 1
		dip = 0
		mil = 2
    }
}

2266.1.1 = {
	monarch = {
 		name = "Syonyeka"
		dynasty = "Nudra-Nayogh"
		birth_date = 2215.1.1
		adm = 6
		dip = 6
		mil = 6
    }
}

2335.1.1 = {
	monarch = {
 		name = "Ngumyunguar"
		dynasty = "Nedre-Napag"
		birth_date = 2312.1.1
		adm = 4
		dip = 6
		mil = 3
		female = yes
    }
}

2392.1.1 = {
	monarch = {
 		name = "Matoghan"
		dynasty = "Nodru-Yughutyin"
		birth_date = 2363.1.1
		adm = 6
		dip = 0
		mil = 4
    }
}

2484.1.1 = {
	monarch = {
 		name = "Gityumpoi"
		dynasty = "Nedre-Gangam"
		birth_date = 2434.1.1
		adm = 2
		dip = 1
		mil = 0
    }
}

