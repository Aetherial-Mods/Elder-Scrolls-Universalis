government = tribal
government_rank = 1
mercantilism = 1
technology_group = orsimer_tg
religion = malacath_cult
primary_culture = orsimer
capital = 3325

54.1.1 = {
	monarch = {
 		name = "Dugroth"
		dynasty = "gro-Dular"
		birth_date = 9.1.1
		adm = 2
		dip = 6
		mil = 4
    }
}

117.1.1 = {
	monarch = {
 		name = "Ugrash"
		dynasty = "gro-Uram"
		birth_date = 77.1.1
		adm = 0
		dip = 5
		mil = 0
		female = yes
    }
}

209.1.1 = {
	monarch = {
 		name = "Olfin"
		dynasty = "gro-Ghorub"
		birth_date = 186.1.1
		adm = 5
		dip = 5
		mil = 4
    }
}

246.1.1 = {
	monarch = {
 		name = "Kargnuth"
		dynasty = "gro-Yadphumph"
		birth_date = 218.1.1
		adm = 4
		dip = 4
		mil = 1
    }
}

343.1.1 = {
	monarch = {
 		name = "Ugor"
		dynasty = "gro-Oorg"
		birth_date = 305.1.1
		adm = 2
		dip = 3
		mil = 4
		female = yes
    }
}

418.1.1 = {
	monarch = {
 		name = "Ugarnesh"
		dynasty = "gro-Torg"
		birth_date = 398.1.1
		adm = 0
		dip = 2
		mil = 1
		female = yes
    }
}

458.1.1 = {
	monarch = {
 		name = "Okrat"
		dynasty = "gro-Shargam"
		birth_date = 410.1.1
		adm = 2
		dip = 3
		mil = 3
    }
}

550.1.1 = {
	monarch = {
 		name = "Gogul"
		dynasty = "gro-Grazhwu"
		birth_date = 517.1.1
		adm = 0
		dip = 3
		mil = 6
		female = yes
    }
}

643.1.1 = {
	monarch = {
 		name = "Orakh"
		dynasty = "gro-Lob"
		birth_date = 622.1.1
		adm = 6
		dip = 2
		mil = 3
    }
}

730.1.1 = {
	monarch = {
 		name = "Khralek"
		dynasty = "gro-Zosh"
		birth_date = 705.1.1
		adm = 4
		dip = 1
		mil = 0
    }
}

796.1.1 = {
	monarch = {
 		name = "Duluk"
		dynasty = "gro-Morothmash"
		birth_date = 765.1.1
		adm = 2
		dip = 0
		mil = 3
    }
}

835.1.1 = {
	monarch = {
 		name = "Urul"
		dynasty = "gro-Nag"
		birth_date = 790.1.1
		adm = 0
		dip = 6
		mil = 0
    }
}

927.1.1 = {
	monarch = {
 		name = "Nuza"
		dynasty = "gro-Gloth"
		birth_date = 903.1.1
		adm = 6
		dip = 6
		mil = 3
		female = yes
    }
}

993.1.1 = {
	monarch = {
 		name = "Kharsthun"
		dynasty = "gro-Both"
		birth_date = 948.1.1
		adm = 4
		dip = 5
		mil = 0
    }
}

1091.1.1 = {
	monarch = {
 		name = "Agdesh"
		dynasty = "gro-Rokaug"
		birth_date = 1072.1.1
		adm = 6
		dip = 6
		mil = 2
		female = yes
    }
}

1165.1.1 = {
	monarch = {
 		name = "Ulu"
		dynasty = "gro-Snagdurl"
		birth_date = 1118.1.1
		adm = 4
		dip = 5
		mil = 5
		female = yes
    }
}

1238.1.1 = {
	monarch = {
 		name = "Dushgor"
		dynasty = "gro-Ghorub"
		birth_date = 1197.1.1
		adm = 2
		dip = 5
		mil = 2
    }
}

1336.1.1 = {
	monarch = {
 		name = "Vundrum"
		dynasty = "gro-Maugash"
		birth_date = 1312.1.1
		adm = 0
		dip = 4
		mil = 6
    }
}

1384.1.1 = {
	monarch = {
 		name = "Osgrikh"
		dynasty = "gro-Borolg"
		birth_date = 1362.1.1
		adm = 6
		dip = 3
		mil = 2
    }
}

1472.1.1 = {
	monarch = {
 		name = "Kurd"
		dynasty = "gro-Glogob"
		birth_date = 1445.1.1
		adm = 4
		dip = 2
		mil = 6
    }
}

1562.1.1 = {
	monarch = {
 		name = "Durzol"
		dynasty = "gro-Oromog"
		birth_date = 1515.1.1
		adm = 2
		dip = 1
		mil = 3
    }
}

1649.1.1 = {
	monarch = {
 		name = "Undusha"
		dynasty = "gro-Zulgroth"
		birth_date = 1623.1.1
		adm = 0
		dip = 1
		mil = 6
		female = yes
    }
}

1703.1.1 = {
	monarch = {
 		name = "Orzbara"
		dynasty = "gro-Burz"
		birth_date = 1660.1.1
		adm = 2
		dip = 2
		mil = 1
    }
}

1744.1.1 = {
	monarch = {
 		name = "Krognak"
		dynasty = "gro-Obdeg"
		birth_date = 1709.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1828.1.1 = {
	monarch = {
 		name = "Ragbur"
		dynasty = "gro-Spagel"
		birth_date = 1801.1.1
		adm = 6
		dip = 0
		mil = 1
    }
}

1901.1.1 = {
	monarch = {
 		name = "Latumph"
		dynasty = "gro-Stugbulukh"
		birth_date = 1883.1.1
		adm = 4
		dip = 6
		mil = 5
    }
}

1973.1.1 = {
	monarch = {
 		name = "Ashgara"
		dynasty = "gro-Duguog"
		birth_date = 1953.1.1
		adm = 2
		dip = 6
		mil = 1
		female = yes
    }
}

2046.1.1 = {
	monarch = {
 		name = "Yakegg"
		dynasty = "gro-Zulgroth"
		birth_date = 2010.1.1
		adm = 0
		dip = 5
		mil = 5
    }
}

2087.1.1 = {
	monarch = {
 		name = "Rablarz"
		dynasty = "gro-Fheg"
		birth_date = 2045.1.1
		adm = 6
		dip = 4
		mil = 2
    }
}

2124.1.1 = {
	monarch = {
 		name = "Larhoth"
		dynasty = "gro-Marth"
		birth_date = 2090.1.1
		adm = 4
		dip = 3
		mil = 5
    }
}

2215.1.1 = {
	monarch = {
 		name = "Arzorag"
		dynasty = "gro-Grushbub"
		birth_date = 2186.1.1
		adm = 6
		dip = 5
		mil = 0
		female = yes
    }
}

2298.1.1 = {
	monarch = {
 		name = "Yagorkh"
		dynasty = "gro-Borab"
		birth_date = 2270.1.1
		adm = 4
		dip = 4
		mil = 3
    }
}

2386.1.1 = {
	monarch = {
 		name = "Gezorz"
		dynasty = "gro-Gunran"
		birth_date = 2364.1.1
		adm = 2
		dip = 3
		mil = 0
    }
}

2465.1.1 = {
	monarch = {
 		name = "Yatog"
		dynasty = "gro-Razgugul"
		birth_date = 2414.1.1
		adm = 5
		dip = 4
		mil = 2
    }
}

