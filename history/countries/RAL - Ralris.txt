government = republic
add_government_reform = pirate_republic_reform
government_rank = 1
mercantilism = 1
technology_group = akavir_tg
religion = po_tun_pantheon
primary_culture = po_tun
capital = 1671

54.1.1 = {
	monarch = {
 		name = "Sohemool"
		dynasty = "Tehuntu'su"
		birth_date = 7.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

120.1.1 = {
	monarch = {
 		name = "Lundrus"
		dynasty = "Nontrae'la"
		birth_date = 76.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

188.1.1 = {
	monarch = {
 		name = "Bororr"
		dynasty = "Tiar'la"
		birth_date = 159.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

232.1.1 = {
	monarch = {
 		name = "Neyak"
		dynasty = "Neyak'su"
		birth_date = 212.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

287.1.1 = {
	monarch = {
 		name = "Ti"
		dynasty = "Lis'wo"
		birth_date = 239.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

342.1.1 = {
	monarch = {
 		name = "Dalnog"
		dynasty = "Nontrae'la"
		birth_date = 298.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

403.1.1 = {
	monarch = {
 		name = "Lis"
		dynasty = "Rolum'ri"
		birth_date = 352.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

451.1.1 = {
	monarch = {
 		name = "Mur"
		dynasty = "La'wo"
		birth_date = 427.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

517.1.1 = {
	monarch = {
 		name = "Su"
		dynasty = "Settu'la"
		birth_date = 493.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

580.1.1 = {
	monarch = {
 		name = "Tus"
		dynasty = "Dis'wo"
		birth_date = 552.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

665.1.1 = {
	monarch = {
 		name = "Kusdere"
		dynasty = "Thusdu'la"
		birth_date = 625.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

722.1.1 = {
	monarch = {
 		name = "Biso"
		dynasty = "Den'su"
		birth_date = 672.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

778.1.1 = {
	monarch = {
 		name = "Su"
		dynasty = "Sok'ri"
		birth_date = 733.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

872.1.1 = {
	monarch = {
 		name = "Kontessu"
		dynasty = "Lisurr'la"
		birth_date = 845.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

942.1.1 = {
	monarch = {
 		name = "Pu"
		dynasty = "Fog'su"
		birth_date = 916.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1020.1.1 = {
	monarch = {
 		name = "Den"
		dynasty = "For'la"
		birth_date = 971.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1070.1.1 = {
	monarch = {
 		name = "Pon"
		dynasty = "Listus'wo"
		birth_date = 1032.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1169.1.1 = {
	monarch = {
 		name = "Bo"
		dynasty = "De'la"
		birth_date = 1130.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1214.1.1 = {
	monarch = {
 		name = "Pu"
		dynasty = "Gon'ri"
		birth_date = 1167.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1267.1.1 = {
	monarch = {
 		name = "Dowentem"
		dynasty = "Thur'su"
		birth_date = 1232.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1351.1.1 = {
	monarch = {
 		name = "Qevor"
		dynasty = "Win'la"
		birth_date = 1318.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1423.1.1 = {
	monarch = {
 		name = "Dusturo"
		dynasty = "Pakhak'su"
		birth_date = 1376.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1516.1.1 = {
	monarch = {
 		name = "De"
		dynasty = "Cin'la"
		birth_date = 1474.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1570.1.1 = {
	monarch = {
 		name = "Bundrumu"
		dynasty = "Cin'la"
		birth_date = 1518.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1667.1.1 = {
	monarch = {
 		name = "Thom"
		dynasty = "Thur'su"
		birth_date = 1618.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1707.1.1 = {
	monarch = {
 		name = "Domdondrul"
		dynasty = "Shreyl'wo"
		birth_date = 1688.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1752.1.1 = {
	monarch = {
 		name = "De"
		dynasty = "Settu'la"
		birth_date = 1721.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1826.1.1 = {
	monarch = {
 		name = "Krer"
		dynasty = "Neta'wo"
		birth_date = 1793.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1884.1.1 = {
	monarch = {
 		name = "Qokug"
		dynasty = "Tus'su"
		birth_date = 1854.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1932.1.1 = {
	monarch = {
 		name = "Chen"
		dynasty = "Sorove'su"
		birth_date = 1911.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1979.1.1 = {
	monarch = {
 		name = "Qarcyseim"
		dynasty = "Susthae'la"
		birth_date = 1939.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

2057.1.1 = {
	monarch = {
 		name = "Gelnar"
		dynasty = "Puhak'su"
		birth_date = 2018.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2151.1.1 = {
	monarch = {
 		name = "Nuyul"
		dynasty = "Listus'wo"
		birth_date = 2115.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

2232.1.1 = {
	monarch = {
 		name = "Vag"
		dynasty = "Chuhleyd'su"
		birth_date = 2184.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2293.1.1 = {
	monarch = {
 		name = "Gaduyd"
		dynasty = "Kumolen'ri"
		birth_date = 2272.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2362.1.1 = {
	monarch = {
 		name = "Gelnar"
		dynasty = "Duhlan'ri"
		birth_date = 2343.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2422.1.1 = {
	monarch = {
 		name = "Se"
		dynasty = "Gur'su"
		birth_date = 2391.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

2464.1.1 = {
	monarch = {
 		name = "Dochecmea"
		dynasty = "Curcu'la"
		birth_date = 2415.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

