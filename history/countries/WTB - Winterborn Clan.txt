government = tribal
government_rank = 5
mercantilism = 1
technology_group = northern_tg
religion = old_gods_cult
primary_culture = reachmen
capital = 7070
secondary_religion = malacath_cult

54.1.1 = {
	monarch = {
 		name = "Brunnuli"
		dynasty = "Ilcev"
		birth_date = 13.1.1
		adm = 1
		dip = 6
		mil = 3
    }
}

110.1.1 = {
	monarch = {
 		name = "Pal"
		dynasty = "Pal"
		birth_date = 80.1.1
		adm = 0
		dip = 5
		mil = 0
    }
}

146.1.1 = {
	monarch = {
 		name = "Olcawa"
		dynasty = "Enneda"
		birth_date = 122.1.1
		adm = 5
		dip = 4
		mil = 3
    }
}

207.1.1 = {
	monarch = {
 		name = "Ilnyc"
		dynasty = "Drerdirtan"
		birth_date = 157.1.1
		adm = 0
		dip = 5
		mil = 5
    }
}

274.1.1 = {
	monarch = {
 		name = "Elvohi"
		dynasty = "Nuolrig"
		birth_date = 241.1.1
		adm = 5
		dip = 4
		mil = 2
		female = yes
    }
}

333.1.1 = {
	monarch = {
 		name = "Mowe"
		dynasty = "Uvloz"
		birth_date = 315.1.1
		adm = 3
		dip = 4
		mil = 5
		female = yes
    }
}

387.1.1 = {
	monarch = {
 		name = "Dolvov"
		dynasty = "Olcawa"
		birth_date = 340.1.1
		adm = 1
		dip = 3
		mil = 2
		female = yes
    }
}

452.1.1 = {
	monarch = {
 		name = "Weg"
		dynasty = "Ralora"
		birth_date = 400.1.1
		adm = 0
		dip = 2
		mil = 6
    }
}

544.1.1 = {
	monarch = {
 		name = "Belrusa"
		dynasty = "Brunnuli"
		birth_date = 491.1.1
		adm = 0
		dip = 6
		mil = 3
    }
}

587.1.1 = {
	monarch = {
 		name = "Ped"
		dynasty = "Puud"
		birth_date = 537.1.1
		adm = 6
		dip = 5
		mil = 0
    }
}

627.1.1 = {
	monarch = {
 		name = "Omveda"
		dynasty = "Edlosa"
		birth_date = 599.1.1
		adm = 4
		dip = 5
		mil = 4
    }
}

693.1.1 = {
	monarch = {
 		name = "Onbam"
		dynasty = "Enneda"
		birth_date = 660.1.1
		adm = 2
		dip = 4
		mil = 0
		female = yes
    }
}

758.1.1 = {
	monarch = {
 		name = "Tresrac"
		dynasty = "Sovril"
		birth_date = 707.1.1
		adm = 0
		dip = 3
		mil = 4
    }
}

801.1.1 = {
	monarch = {
 		name = "Onnut"
		dynasty = "Gedohu"
		birth_date = 765.1.1
		adm = 6
		dip = 2
		mil = 1
		female = yes
    }
}

890.1.1 = {
	monarch = {
 		name = "Hia"
		dynasty = "Erboda"
		birth_date = 864.1.1
		adm = 1
		dip = 3
		mil = 2
		female = yes
    }
}

978.1.1 = {
	monarch = {
 		name = "Onbam"
		dynasty = "Gralret"
		birth_date = 957.1.1
		adm = 6
		dip = 3
		mil = 6
		female = yes
    }
}

1032.1.1 = {
	monarch = {
 		name = "Bruc"
		dynasty = "Pevlen"
		birth_date = 989.1.1
		adm = 4
		dip = 2
		mil = 3
    }
}

1112.1.1 = {
	monarch = {
 		name = "Uveza"
		dynasty = "Veyrbid"
		birth_date = 1063.1.1
		adm = 2
		dip = 1
		mil = 6
    }
}

1196.1.1 = {
	monarch = {
 		name = "Uaeti"
		dynasty = "Nonvemo"
		birth_date = 1147.1.1
		adm = 0
		dip = 0
		mil = 3
		female = yes
    }
}

1262.1.1 = {
	monarch = {
 		name = "Odohe"
		dynasty = "Brumroch"
		birth_date = 1235.1.1
		adm = 6
		dip = 6
		mil = 6
    }
}

1339.1.1 = {
	monarch = {
 		name = "Suae"
		dynasty = "Nonvemo"
		birth_date = 1292.1.1
		adm = 4
		dip = 6
		mil = 3
		female = yes
    }
}

1437.1.1 = {
	monarch = {
 		name = "Uveza"
		dynasty = "Pal"
		birth_date = 1396.1.1
		adm = 2
		dip = 5
		mil = 0
    }
}

1475.1.1 = {
	monarch = {
 		name = "Sovril"
		dynasty = "Erbod"
		birth_date = 1454.1.1
		adm = 4
		dip = 6
		mil = 1
    }
}

1526.1.1 = {
	monarch = {
 		name = "Tuvles"
		dynasty = "Bayl"
		birth_date = 1506.1.1
		adm = 2
		dip = 5
		mil = 5
		female = yes
    }
}

1601.1.1 = {
	monarch = {
 		name = "Pal"
		dynasty = "Braldeho"
		birth_date = 1583.1.1
		adm = 6
		dip = 0
		mil = 5
    }
}

1683.1.1 = {
	monarch = {
 		name = "Povren"
		dynasty = "Osrata"
		birth_date = 1642.1.1
		adm = 1
		dip = 2
		mil = 0
    }
}

1775.1.1 = {
	monarch = {
 		name = "Mofu"
		dynasty = "Welin"
		birth_date = 1753.1.1
		adm = 0
		dip = 1
		mil = 3
		female = yes
    }
}

1868.1.1 = {
	monarch = {
 		name = "Dilvortad"
		dynasty = "Ergamo"
		birth_date = 1832.1.1
		adm = 4
		dip = 3
		mil = 3
    }
}

1909.1.1 = {
	monarch = {
 		name = "Nanrale"
		dynasty = "Duldoso"
		birth_date = 1868.1.1
		adm = 6
		dip = 4
		mil = 5
    }
}

1949.1.1 = {
	monarch = {
 		name = "Levi"
		dynasty = "Nuldage"
		birth_date = 1908.1.1
		adm = 4
		dip = 3
		mil = 1
		female = yes
    }
}

2026.1.1 = {
	monarch = {
 		name = "Treroga"
		dynasty = "Rulmuwo"
		birth_date = 1985.1.1
		adm = 2
		dip = 2
		mil = 5
    }
}

2105.1.1 = {
	monarch = {
 		name = "Tresrac"
		dynasty = "Dennan"
		birth_date = 2054.1.1
		adm = 0
		dip = 2
		mil = 2
    }
}

2156.1.1 = {
	monarch = {
 		name = "Onnut"
		dynasty = "Weg"
		birth_date = 2117.1.1
		adm = 6
		dip = 1
		mil = 5
		female = yes
    }
}

2225.1.1 = {
	monarch = {
 		name = "Ber"
		dynasty = "Drergan"
		birth_date = 2193.1.1
		adm = 4
		dip = 0
		mil = 2
    }
}

2304.1.1 = {
	monarch = {
 		name = "Treroga"
		dynasty = "Vuur"
		birth_date = 2276.1.1
		adm = 2
		dip = 6
		mil = 6
    }
}

2384.1.1 = {
	monarch = {
 		name = "Tresrac"
		dynasty = "Orvete"
		birth_date = 2346.1.1
		adm = 0
		dip = 6
		mil = 2
    }
}

2478.1.1 = {
	monarch = {
 		name = "Duldoso"
		dynasty = "Erbod"
		birth_date = 2445.1.1
		adm = 2
		dip = 0
		mil = 4
    }
}

