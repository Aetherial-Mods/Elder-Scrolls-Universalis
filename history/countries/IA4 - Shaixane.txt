government = republic
government_rank = 1
mercantilism = 1
technology_group = kamal_tg
religion = kamal_pantheon
primary_culture = kamal
capital = 3507

54.1.1 = {
	monarch = {
 		name = "Hodnius"
		dynasty = "Xosis"
		birth_date = 1.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

116.1.1 = {
	monarch = {
 		name = "Kasoeus"
		dynasty = "Venon"
		birth_date = 79.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

182.1.1 = {
	monarch = {
 		name = "Chonaon"
		dynasty = "Thaxo"
		birth_date = 151.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

229.1.1 = {
	monarch = {
 		name = "Dyzonos"
		dynasty = "Vustrer"
		birth_date = 205.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

282.1.1 = {
	monarch = {
 		name = "Kylaestus"
		dynasty = "Husytion"
		birth_date = 261.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

364.1.1 = {
	monarch = {
 		name = "Chrumes"
		dynasty = "Nozios"
		birth_date = 324.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

452.1.1 = {
	monarch = {
 		name = "Vavomo"
		dynasty = "Phondris"
		birth_date = 419.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

494.1.1 = {
	monarch = {
 		name = "Hevuve"
		dynasty = "Chasias"
		birth_date = 449.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

537.1.1 = {
	monarch = {
 		name = "Kustaemon"
		dynasty = "Chonaon"
		birth_date = 515.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

572.1.1 = {
	monarch = {
 		name = "Hembrates"
		dynasty = "Xuxoeus"
		birth_date = 550.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

614.1.1 = {
	monarch = {
 		name = "Cheidespe"
		dynasty = "Kulcius"
		birth_date = 570.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

663.1.1 = {
	monarch = {
 		name = "Zusolco"
		dynasty = "Phondris"
		birth_date = 631.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

699.1.1 = {
	monarch = {
 		name = "Kustaemon"
		dynasty = "Kylaestus"
		birth_date = 674.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

772.1.1 = {
	monarch = {
 		name = "Hembrates"
		dynasty = "Hamis"
		birth_date = 744.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

870.1.1 = {
	monarch = {
 		name = "Zixaeon"
		dynasty = "Phyvaumas"
		birth_date = 840.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

941.1.1 = {
	monarch = {
 		name = "Ruthotos"
		dynasty = "Sthises"
		birth_date = 915.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1030.1.1 = {
	monarch = {
 		name = "Pheiadris"
		dynasty = "Chonaon"
		birth_date = 1004.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1094.1.1 = {
	monarch = {
 		name = "Midytion"
		dynasty = "Kulcius"
		birth_date = 1060.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1177.1.1 = {
	monarch = {
 		name = "Chasias"
		dynasty = "Thaegnus"
		birth_date = 1140.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1235.1.1 = {
	monarch = {
 		name = "Ruthotos"
		dynasty = "Rembrotus"
		birth_date = 1198.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1328.1.1 = {
	monarch = {
 		name = "Pheiadris"
		dynasty = "Sthises"
		birth_date = 1296.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1427.1.1 = {
	monarch = {
 		name = "Midytion"
		dynasty = "Phyvaumas"
		birth_date = 1385.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1519.1.1 = {
	monarch = {
 		name = "Chasias"
		dynasty = "Theivanes"
		birth_date = 1488.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1597.1.1 = {
	monarch = {
 		name = "Phurloteus"
		dynasty = "Deisditon"
		birth_date = 1559.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1682.1.1 = {
	monarch = {
 		name = "Phyantaon"
		dynasty = "Midytion"
		birth_date = 1644.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1718.1.1 = {
	monarch = {
 		name = "Thaxo"
		dynasty = "Xaevaumas"
		birth_date = 1681.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1787.1.1 = {
	monarch = {
 		name = "Phyvaumas"
		dynasty = "Xespaeon"
		birth_date = 1745.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1851.1.1 = {
	monarch = {
 		name = "Phurloteus"
		dynasty = "Vorlos"
		birth_date = 1829.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1902.1.1 = {
	monarch = {
 		name = "Phyantaon"
		dynasty = "Hembreas"
		birth_date = 1856.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1945.1.1 = {
	monarch = {
 		name = "Rignevia"
		dynasty = "Veuggiton"
		birth_date = 1914.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

2011.1.1 = {
	monarch = {
 		name = "Theivanes"
		dynasty = "Dylys"
		birth_date = 1976.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2068.1.1 = {
	monarch = {
 		name = "Phores"
		dynasty = "Xiragos"
		birth_date = 2015.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2126.1.1 = {
	monarch = {
 		name = "Kulcius"
		dynasty = "Thovaumas"
		birth_date = 2094.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2223.1.1 = {
	monarch = {
 		name = "Rolion"
		dynasty = "Sthamytion"
		birth_date = 2171.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2310.1.1 = {
	monarch = {
 		name = "Maxytion"
		dynasty = "Theldaeon"
		birth_date = 2286.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2409.1.1 = {
	monarch = {
 		name = "Phores"
		dynasty = "Veuggiton"
		birth_date = 2379.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2446.1.1 = {
	monarch = {
 		name = "Myza"
		dynasty = "Zixaeon"
		birth_date = 2427.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

