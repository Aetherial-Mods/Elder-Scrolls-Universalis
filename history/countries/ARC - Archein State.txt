government = tribal
government_rank = 5
mercantilism = 1
technology_group = marshan_tg
religion = hist
primary_culture = archein
capital = 1064

54.1.1 = {
	monarch = {
 		name = "Veelisheeh"
		dynasty = "Miloeeja"
		birth_date = 2.1.1
		adm = 0
		dip = 5
		mil = 2
    }
}

115.1.1 = {
	monarch = {
 		name = "Okeeh"
		dynasty = "Endoremus"
		birth_date = 62.1.1
		adm = 2
		dip = 6
		mil = 3
    }
}

174.1.1 = {
	monarch = {
 		name = "Jee-Lar"
		dynasty = "Caetius"
		birth_date = 137.1.1
		adm = 0
		dip = 5
		mil = 0
    }
}

264.1.1 = {
	monarch = {
 		name = "Peek-Neeus"
		dynasty = "Theerdaresh"
		birth_date = 220.1.1
		adm = 6
		dip = 4
		mil = 3
		female = yes
    }
}

340.1.1 = {
	monarch = {
 		name = "Jilux"
		dynasty = "Xeirtius"
		birth_date = 317.1.1
		adm = 4
		dip = 4
		mil = 0
    }
}

393.1.1 = {
	monarch = {
 		name = "Cheedal-Jeen"
		dynasty = "Androdes"
		birth_date = 352.1.1
		adm = 2
		dip = 3
		mil = 4
    }
}

459.1.1 = {
	monarch = {
 		name = "Voneesh"
		dynasty = "Tikeerdeseer"
		birth_date = 422.1.1
		adm = 0
		dip = 2
		mil = 0
    }
}

535.1.1 = {
	monarch = {
 		name = "Otumi-Lar"
		dynasty = "Sakeides"
		birth_date = 510.1.1
		adm = 6
		dip = 1
		mil = 4
    }
}

594.1.1 = {
	monarch = {
 		name = "Kloxu"
		dynasty = "Caligeeja"
		birth_date = 566.1.1
		adm = 4
		dip = 0
		mil = 1
		female = yes
    }
}

633.1.1 = {
	monarch = {
 		name = "Chukka-Saak"
		dynasty = "Nileesh"
		birth_date = 583.1.1
		adm = 6
		dip = 2
		mil = 2
		female = yes
    }
}

701.1.1 = {
	monarch = {
 		name = "Vistha-Jush"
		dynasty = "Nagdeseer"
		birth_date = 678.1.1
		adm = 4
		dip = 1
		mil = 6
    }
}

799.1.1 = {
	monarch = {
 		name = "Chuxu"
		dynasty = "Xemclesh"
		birth_date = 778.1.1
		adm = 2
		dip = 0
		mil = 3
    }
}

880.1.1 = {
	monarch = {
 		name = "Waku-Mat"
		dynasty = "Xemclesh"
		birth_date = 853.1.1
		adm = 0
		dip = 6
		mil = 6
    }
}

939.1.1 = {
	monarch = {
 		name = "Pujeerish"
		dynasty = "Saliz'k"
		birth_date = 890.1.1
		adm = 6
		dip = 6
		mil = 3
		female = yes
    }
}

1019.1.1 = {
	monarch = {
 		name = "Kamatpa"
		dynasty = "Wanan-Dar"
		birth_date = 968.1.1
		adm = 4
		dip = 5
		mil = 0
    }
}

1063.1.1 = {
	monarch = {
 		name = "Chukka-Jekka"
		dynasty = "Canision"
		birth_date = 1020.1.1
		adm = 2
		dip = 4
		mil = 3
    }
}

1111.1.1 = {
	monarch = {
 		name = "Vushtulm"
		dynasty = "Neethsareeth"
		birth_date = 1072.1.1
		adm = 0
		dip = 3
		mil = 0
    }
}

1207.1.1 = {
	monarch = {
 		name = "Pinooh"
		dynasty = "Casmareen"
		birth_date = 1173.1.1
		adm = 2
		dip = 4
		mil = 1
		female = yes
    }
}

1303.1.1 = {
	monarch = {
 		name = "Kalanu"
		dynasty = "Gallures"
		birth_date = 1253.1.1
		adm = 1
		dip = 4
		mil = 5
    }
}

1383.1.1 = {
	monarch = {
 		name = "Pideelus"
		dynasty = "Jeetum-Lei"
		birth_date = 1336.1.1
		adm = 6
		dip = 3
		mil = 2
    }
}

1474.1.1 = {
	monarch = {
 		name = "Lineem"
		dynasty = "Meerhaj"
		birth_date = 1453.1.1
		adm = 4
		dip = 2
		mil = 5
		female = yes
    }
}

1512.1.1 = {
	monarch = {
 		name = "Dar-Jasa"
		dynasty = "Meerhaj"
		birth_date = 1489.1.1
		adm = 2
		dip = 1
		mil = 2
    }
}

1604.1.1 = {
	monarch = {
 		name = "Wud-Weska"
		dynasty = "Augussareth"
		birth_date = 1581.1.1
		adm = 1
		dip = 0
		mil = 6
		female = yes
    }
}

1682.1.1 = {
	monarch = {
 		name = "Pekai-Jee"
		dynasty = "Lafnaresh"
		birth_date = 1633.1.1
		adm = 6
		dip = 0
		mil = 2
    }
}

1720.1.1 = {
	monarch = {
 		name = "Keema-Ja"
		dynasty = "Heem-Lei"
		birth_date = 1697.1.1
		adm = 4
		dip = 6
		mil = 6
    }
}

1786.1.1 = {
	monarch = {
 		name = "Dar-Zish"
		dynasty = "Ah-Jee"
		birth_date = 1743.1.1
		adm = 6
		dip = 0
		mil = 0
		female = yes
    }
}

1878.1.1 = {
	monarch = {
 		name = "Wixil"
		dynasty = "Casdorus"
		birth_date = 1845.1.1
		adm = 4
		dip = 6
		mil = 4
		female = yes
    }
}

1951.1.1 = {
	monarch = {
 		name = "Deejeeta"
		dynasty = "Talen-Ei"
		birth_date = 1902.1.1
		adm = 2
		dip = 6
		mil = 1
    }
}

2001.1.1 = {
	monarch = {
 		name = "Xazar"
		dynasty = "Pergoulus"
		birth_date = 1951.1.1
		adm = 1
		dip = 5
		mil = 4
    }
}

2087.1.1 = {
	monarch = {
 		name = "Ruja-Wan"
		dynasty = "Casmareen"
		birth_date = 2040.1.1
		adm = 6
		dip = 4
		mil = 1
		female = yes
    }
}

2185.1.1 = {
	monarch = {
 		name = "Lutapeeth"
		dynasty = "Talen-Lan"
		birth_date = 2140.1.1
		adm = 4
		dip = 3
		mil = 5
		female = yes
    }
}

2247.1.1 = {
	monarch = {
 		name = "Deegeeta"
		dynasty = "Caligeepa"
		birth_date = 2224.1.1
		adm = 2
		dip = 2
		mil = 1
    }
}

2294.1.1 = {
	monarch = {
 		name = "Xal-Geh"
		dynasty = "Xeirtius"
		birth_date = 2265.1.1
		adm = 1
		dip = 2
		mil = 5
    }
}

2386.1.1 = {
	monarch = {
 		name = "Pojeel"
		dynasty = "Canision"
		birth_date = 2337.1.1
		adm = 3
		dip = 3
		mil = 6
    }
}

2427.1.1 = {
	monarch = {
 		name = "Luraxith"
		dynasty = "Pethees"
		birth_date = 2376.1.1
		adm = 1
		dip = 2
		mil = 3
		female = yes
    }
}

2466.1.1 = {
	monarch = {
 		name = "Redieeus"
		dynasty = "Miloeeja"
		birth_date = 2433.1.1
		adm = 6
		dip = 1
		mil = 0
    }
}

