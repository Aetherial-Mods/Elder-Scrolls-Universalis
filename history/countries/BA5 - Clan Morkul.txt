government = tribal
government_rank = 1
mercantilism = 1
technology_group = orsimer_tg
religion = orcish_pantheon
primary_culture = orsimer
capital = 6892

54.1.1 = {
	monarch = {
 		name = "Grushnag"
		dynasty = "gro-Morkul"
		birth_date = 27.1.1
		adm = 3
		dip = 2
		mil = 2
    }
}

91.1.1 = {
	monarch = {
 		name = "Rogmesh"
		dynasty = "gro-Morkul"
		birth_date = 65.1.1
		adm = 2
		dip = 1
		mil = 5
		female = yes
    }
}

176.1.1 = {
	monarch = {
 		name = "Thrug"
		dynasty = "gro-Morkul"
		birth_date = 152.1.1
		adm = 0
		dip = 0
		mil = 2
    }
}

216.1.1 = {
	monarch = {
 		name = "Nar"
		dynasty = "gro-Morkul"
		birth_date = 187.1.1
		adm = 5
		dip = 0
		mil = 6
    }
}

253.1.1 = {
	monarch = {
 		name = "Grundu"
		dynasty = "gro-Morkul"
		birth_date = 209.1.1
		adm = 3
		dip = 6
		mil = 2
    }
}

346.1.1 = {
	monarch = {
 		name = "Orag"
		dynasty = "gro-Morkul"
		birth_date = 323.1.1
		adm = 2
		dip = 5
		mil = 6
		female = yes
    }
}

430.1.1 = {
	monarch = {
 		name = "Stuga"
		dynasty = "gro-Morkul"
		birth_date = 388.1.1
		adm = 4
		dip = 6
		mil = 0
		female = yes
    }
}

521.1.1 = {
	monarch = {
 		name = "Nahzush"
		dynasty = "gro-Morkul"
		birth_date = 469.1.1
		adm = 2
		dip = 5
		mil = 4
    }
}

599.1.1 = {
	monarch = {
 		name = "Thugnekh"
		dynasty = "gro-Morkul"
		birth_date = 568.1.1
		adm = 0
		dip = 5
		mil = 1
		female = yes
    }
}

693.1.1 = {
	monarch = {
 		name = "Obdeg"
		dynasty = "gro-Morkul"
		birth_date = 640.1.1
		adm = 5
		dip = 4
		mil = 4
    }
}

790.1.1 = {
	monarch = {
 		name = "Glathut"
		dynasty = "gro-Morkul"
		birth_date = 761.1.1
		adm = 3
		dip = 3
		mil = 1
		female = yes
    }
}

839.1.1 = {
	monarch = {
 		name = "Bulzog"
		dynasty = "gro-Morkul"
		birth_date = 815.1.1
		adm = 2
		dip = 2
		mil = 5
    }
}

879.1.1 = {
	monarch = {
 		name = "Tumuthag"
		dynasty = "gro-Morkul"
		birth_date = 857.1.1
		adm = 0
		dip = 2
		mil = 1
    }
}

932.1.1 = {
	monarch = {
 		name = "Morndolag"
		dynasty = "gro-Morkul"
		birth_date = 894.1.1
		adm = 5
		dip = 1
		mil = 5
		female = yes
    }
}

972.1.1 = {
	monarch = {
 		name = "Gulburz"
		dynasty = "gro-Morkul"
		birth_date = 922.1.1
		adm = 0
		dip = 2
		mil = 6
    }
}

1022.1.1 = {
	monarch = {
 		name = "Bugha"
		dynasty = "gro-Morkul"
		birth_date = 991.1.1
		adm = 3
		dip = 3
		mil = 2
		female = yes
    }
}

1087.1.1 = {
	monarch = {
 		name = "Balagog"
		dynasty = "gro-Morkul"
		birth_date = 1036.1.1
		adm = 1
		dip = 3
		mil = 6
    }
}

1168.1.1 = {
	monarch = {
 		name = "Slagwug"
		dynasty = "gro-Morkul"
		birth_date = 1147.1.1
		adm = 6
		dip = 2
		mil = 2
    }
}

1210.1.1 = {
	monarch = {
 		name = "Moghakh"
		dynasty = "gro-Morkul"
		birth_date = 1161.1.1
		adm = 5
		dip = 1
		mil = 6
    }
}

1246.1.1 = {
	monarch = {
 		name = "Bugbekh"
		dynasty = "gro-Morkul"
		birth_date = 1214.1.1
		adm = 6
		dip = 2
		mil = 0
		female = yes
    }
}

1288.1.1 = {
	monarch = {
 		name = "Azulg"
		dynasty = "gro-Morkul"
		birth_date = 1268.1.1
		adm = 5
		dip = 1
		mil = 4
    }
}

1363.1.1 = {
	monarch = {
 		name = "Druga"
		dynasty = "gro-Morkul"
		birth_date = 1328.1.1
		adm = 3
		dip = 1
		mil = 1
		female = yes
    }
}

1449.1.1 = {
	monarch = {
 		name = "Gulfim"
		dynasty = "gro-Morkul"
		birth_date = 1424.1.1
		adm = 1
		dip = 0
		mil = 4
		female = yes
    }
}

1511.1.1 = {
	monarch = {
 		name = "Snalikh"
		dynasty = "gro-Morkul"
		birth_date = 1463.1.1
		adm = 6
		dip = 6
		mil = 1
    }
}

1588.1.1 = {
	monarch = {
 		name = "Morgbrath"
		dynasty = "gro-Morkul"
		birth_date = 1544.1.1
		adm = 5
		dip = 5
		mil = 5
    }
}

1677.1.1 = {
	monarch = {
 		name = "Drienne"
		dynasty = "gro-Morkul"
		birth_date = 1640.1.1
		adm = 3
		dip = 4
		mil = 1
		female = yes
    }
}

1737.1.1 = {
	monarch = {
 		name = "Bashag"
		dynasty = "gro-Morkul"
		birth_date = 1706.1.1
		adm = 1
		dip = 4
		mil = 5
    }
}

1797.1.1 = {
	monarch = {
 		name = "Snakh"
		dynasty = "gro-Morkul"
		birth_date = 1749.1.1
		adm = 3
		dip = 5
		mil = 6
    }
}

1881.1.1 = {
	monarch = {
 		name = "Lashbesh"
		dynasty = "gro-Morkul"
		birth_date = 1847.1.1
		adm = 1
		dip = 4
		mil = 3
		female = yes
    }
}

1950.1.1 = {
	monarch = {
 		name = "Shufthakul"
		dynasty = "gro-Morkul"
		birth_date = 1920.1.1
		adm = 0
		dip = 3
		mil = 0
		female = yes
    }
}

2037.1.1 = {
	monarch = {
 		name = "Loglorag"
		dynasty = "gro-Morkul"
		birth_date = 1999.1.1
		adm = 5
		dip = 3
		mil = 3
		female = yes
    }
}

2089.1.1 = {
	monarch = {
 		name = "Dumuguk"
		dynasty = "gro-Morkul"
		birth_date = 2045.1.1
		adm = 3
		dip = 2
		mil = 0
		female = yes
    }
}

2133.1.1 = {
	monarch = {
 		name = "Blodrat"
		dynasty = "gro-Morkul"
		birth_date = 2104.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2193.1.1 = {
	monarch = {
 		name = "Shufdal"
		dynasty = "gro-Morkul"
		birth_date = 2175.1.1
		adm = 0
		dip = 0
		mil = 0
		female = yes
    }
}

2247.1.1 = {
	monarch = {
 		name = "Lig"
		dynasty = "gro-Morkul"
		birth_date = 2195.1.1
		adm = 5
		dip = 6
		mil = 4
		female = yes
    }
}

2288.1.1 = {
	monarch = {
 		name = "Dulzugha"
		dynasty = "gro-Morkul"
		birth_date = 2260.1.1
		adm = 0
		dip = 1
		mil = 5
		female = yes
    }
}

2362.1.1 = {
	monarch = {
 		name = "Bhagrun"
		dynasty = "gro-Morkul"
		birth_date = 2333.1.1
		adm = 5
		dip = 0
		mil = 2
    }
}

2436.1.1 = {
	monarch = {
 		name = "Fnagdesh"
		dynasty = "gro-Morkul"
		birth_date = 2407.1.1
		adm = 3
		dip = 6
		mil = 6
		female = yes
    }
}

