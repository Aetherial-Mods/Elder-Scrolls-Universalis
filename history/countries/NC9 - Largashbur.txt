government = tribal
government_rank = 1
mercantilism = 1
technology_group = orsimer_tg
religion = orcish_pantheon
primary_culture = orsimer
capital = 3126

54.1.1 = {
	monarch = {
 		name = "Haghai"
		dynasty = "gro-Glunurgakh"
		birth_date = 7.1.1
		adm = 4
		dip = 3
		mil = 1
		female = yes
    }
}

102.1.1 = {
	monarch = {
 		name = "Ghamron"
		dynasty = "gro-Lorbumol"
		birth_date = 73.1.1
		adm = 2
		dip = 2
		mil = 4
    }
}

198.1.1 = {
	monarch = {
 		name = "Yatava"
		dynasty = "gro-Spagel"
		birth_date = 166.1.1
		adm = 4
		dip = 3
		mil = 6
		female = yes
    }
}

251.1.1 = {
	monarch = {
 		name = "Rugdrulz"
		dynasty = "gro-Glothozug"
		birth_date = 218.1.1
		adm = 3
		dip = 2
		mil = 2
    }
}

304.1.1 = {
	monarch = {
 		name = "Zuugarz"
		dynasty = "gro-Bumnog"
		birth_date = 272.1.1
		adm = 1
		dip = 2
		mil = 6
		female = yes
    }
}

358.1.1 = {
	monarch = {
 		name = "Shamob"
		dynasty = "gro-Lothdush"
		birth_date = 314.1.1
		adm = 6
		dip = 1
		mil = 3
    }
}

433.1.1 = {
	monarch = {
 		name = "Maaga"
		dynasty = "gro-Murzog"
		birth_date = 400.1.1
		adm = 4
		dip = 0
		mil = 6
    }
}

483.1.1 = {
	monarch = {
 		name = "Ghornag"
		dynasty = "gro-Guarg"
		birth_date = 431.1.1
		adm = 3
		dip = 6
		mil = 3
    }
}

532.1.1 = {
	monarch = {
 		name = "Zubesha"
		dynasty = "gro-Ramonzul"
		birth_date = 500.1.1
		adm = 1
		dip = 5
		mil = 0
		female = yes
    }
}

629.1.1 = {
	monarch = {
 		name = "Shamagug"
		dynasty = "gro-Orgush"
		birth_date = 604.1.1
		adm = 6
		dip = 5
		mil = 3
    }
}

686.1.1 = {
	monarch = {
 		name = "Luronk"
		dynasty = "gro-Bugunh"
		birth_date = 654.1.1
		adm = 1
		dip = 6
		mil = 5
    }
}

754.1.1 = {
	monarch = {
 		name = "Ghorlorz"
		dynasty = "gro-Larob"
		birth_date = 718.1.1
		adm = 6
		dip = 5
		mil = 1
    }
}

804.1.1 = {
	monarch = {
 		name = "Mash"
		dynasty = "gro-Rhosh"
		birth_date = 784.1.1
		adm = 4
		dip = 4
		mil = 5
    }
}

894.1.1 = {
	monarch = {
 		name = "Bolgar"
		dynasty = "gro-Hagard"
		birth_date = 865.1.1
		adm = 3
		dip = 3
		mil = 2
		female = yes
    }
}

984.1.1 = {
	monarch = {
 		name = "Agstarg"
		dynasty = "gro-Gard"
		birth_date = 943.1.1
		adm = 1
		dip = 3
		mil = 5
    }
}

1019.1.1 = {
	monarch = {
 		name = "Kelrog"
		dynasty = "gro-Borzighu"
		birth_date = 966.1.1
		adm = 0
		dip = 6
		mil = 6
    }
}

1106.1.1 = {
	monarch = {
 		name = "Dugan"
		dynasty = "gro-Durz"
		birth_date = 1083.1.1
		adm = 6
		dip = 5
		mil = 2
    }
}

1177.1.1 = {
	monarch = {
 		name = "Koffutto"
		dynasty = "gro-Lurbozog"
		birth_date = 1149.1.1
		adm = 4
		dip = 4
		mil = 6
    }
}

1256.1.1 = {
	monarch = {
 		name = "Dumolg"
		dynasty = "gro-Begnar"
		birth_date = 1224.1.1
		adm = 2
		dip = 4
		mil = 3
    }
}

1342.1.1 = {
	monarch = {
 		name = "Umbugbek"
		dynasty = "gro-Ghogurz"
		birth_date = 1320.1.1
		adm = 0
		dip = 3
		mil = 6
		female = yes
    }
}

1384.1.1 = {
	monarch = {
 		name = "Oorza"
		dynasty = "gro-Gasheg"
		birth_date = 1348.1.1
		adm = 6
		dip = 2
		mil = 3
		female = yes
    }
}

1438.1.1 = {
	monarch = {
 		name = "Grashla"
		dynasty = "gro-Bulugbek"
		birth_date = 1385.1.1
		adm = 4
		dip = 1
		mil = 0
		female = yes
    }
}

1523.1.1 = {
	monarch = {
 		name = "Dumag"
		dynasty = "gro-Murkub"
		birth_date = 1489.1.1
		adm = 2
		dip = 1
		mil = 3
    }
}

1609.1.1 = {
	monarch = {
 		name = "Ushamph"
		dynasty = "gro-Ghamorz"
		birth_date = 1579.1.1
		adm = 4
		dip = 2
		mil = 5
    }
}

1696.1.1 = {
	monarch = {
 		name = "Ogzaz"
		dynasty = "gro-Okrat"
		birth_date = 1653.1.1
		adm = 2
		dip = 1
		mil = 1
		female = yes
    }
}

1753.1.1 = {
	monarch = {
 		name = "Urbzag"
		dynasty = "gro-Oromog"
		birth_date = 1722.1.1
		adm = 0
		dip = 0
		mil = 5
		female = yes
    }
}

1804.1.1 = {
	monarch = {
 		name = "Othohoth"
		dynasty = "gro-Khargol"
		birth_date = 1752.1.1
		adm = 6
		dip = 6
		mil = 2
    }
}

1850.1.1 = {
	monarch = {
 		name = "Lagarg"
		dynasty = "gro-Morlak"
		birth_date = 1803.1.1
		adm = 4
		dip = 6
		mil = 5
    }
}

1903.1.1 = {
	monarch = {
 		name = "Alzula"
		dynasty = "gro-Mungro"
		birth_date = 1850.1.1
		adm = 2
		dip = 5
		mil = 2
		female = yes
    }
}

1990.1.1 = {
	monarch = {
 		name = "Waghuth"
		dynasty = "gro-Rooglag"
		birth_date = 1953.1.1
		adm = 0
		dip = 4
		mil = 6
    }
}

2069.1.1 = {
	monarch = {
 		name = "Orluguk"
		dynasty = "gro-Hanz"
		birth_date = 2019.1.1
		adm = 6
		dip = 3
		mil = 2
		female = yes
    }
}

2123.1.1 = {
	monarch = {
 		name = "Grubalash"
		dynasty = "gro-Mokhul"
		birth_date = 2080.1.1
		adm = 1
		dip = 4
		mil = 4
		female = yes
    }
}

2203.1.1 = {
	monarch = {
 		name = "Durzum"
		dynasty = "gro-Obrash"
		birth_date = 2162.1.1
		adm = 6
		dip = 4
		mil = 0
    }
}

2264.1.1 = {
	monarch = {
 		name = "Logbur"
		dynasty = "gro-Snugok"
		birth_date = 2216.1.1
		adm = 4
		dip = 3
		mil = 4
    }
}

2302.1.1 = {
	monarch = {
 		name = "Garnikh"
		dynasty = "gro-Gluthush"
		birth_date = 2251.1.1
		adm = 2
		dip = 2
		mil = 1
    }
}

2393.1.1 = {
	monarch = {
 		name = "Ushruka"
		dynasty = "gro-Torg"
		birth_date = 2346.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

2436.1.1 = {
	monarch = {
 		name = "Ramash"
		dynasty = "gro-Dumag"
		birth_date = 2393.1.1
		adm = 6
		dip = 1
		mil = 1
    }
}

2499.1.1 = {
	monarch = {
 		name = "Gulara"
		dynasty = "gro-Mulatub"
		birth_date = 2473.1.1
		adm = 4
		dip = 0
		mil = 5
		female = yes
    }
}

