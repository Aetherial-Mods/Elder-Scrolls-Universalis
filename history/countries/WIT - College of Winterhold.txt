government = theocracy
government_rank = 5
mercantilism = 1
technology_group = northern_tg
religion = students_of_magnus
primary_culture = nord
capital = 2932

historical_friend = NA8

54.1.1 = {
	monarch = {
 		name = "Logof"
		dynasty = "Hjir"
		birth_date = 8.1.1
		adm = 5
		dip = 6
		mil = 0
    }
}

91.1.1 = {
	monarch = {
 		name = "Shalidor"
		birth_date = 41.1.1
		adm = 7
		dip = 7
		mil = 7
    }
	add_ruler_personality = immortal_personality
}

126.1.1 = {
	monarch = {
 		name = "Greily"
		dynasty = "Thimis"
		birth_date = 102.1.1
		adm = 3
		dip = 5
		mil = 4
		female = yes
    }
}

240.1.1 = {
	monarch = {
 		name = "Bedrir"
		dynasty = "Rhanwall"
		birth_date = 206.1.1
		adm = 1
		dip = 5
		mil = 1
    }
}

287.1.1 = {
	monarch = {
 		name = "Sisrytta"
		dynasty = "Rhyrrerik"
		birth_date = 244.1.1
		adm = 3
		dip = 6
		mil = 2
		female = yes
    }
}

365.1.1 = {
	monarch = {
 		name = "Lodunn"
		dynasty = "Rulur"
		birth_date = 319.1.1
		adm = 2
		dip = 5
		mil = 6
    }
}

459.1.1 = {
	monarch = {
 		name = "Songdis"
		dynasty = "Kleelim"
		birth_date = 408.1.1
		adm = 0
		dip = 4
		mil = 3
		female = yes
    }
}

554.1.1 = {
	monarch = {
 		name = "Lynch"
		dynasty = "Rotmalleskr"
		birth_date = 523.1.1
		adm = 5
		dip = 4
		mil = 6
    }
}

607.1.1 = {
	monarch = {
 		name = "Hauting"
		dynasty = "Bretgolak"
		birth_date = 588.1.1
		adm = 3
		dip = 3
		mil = 3
    }
}

652.1.1 = {
	monarch = {
 		name = "Bjalfi"
		dynasty = "Frem"
		birth_date = 621.1.1
		adm = 2
		dip = 2
		mil = 0
    }
}

715.1.1 = {
	monarch = {
 		name = "Thonarcal"
		dynasty = "Bralg"
		birth_date = 691.1.1
		adm = 0
		dip = 1
		mil = 3
    }
}

791.1.1 = {
	monarch = {
 		name = "Khala"
		dynasty = "Finwoskr"
		birth_date = 768.1.1
		adm = 5
		dip = 0
		mil = 0
		female = yes
    }
}

862.1.1 = {
	monarch = {
 		name = "Grigerda"
		dynasty = "Skolnuf"
		birth_date = 844.1.1
		adm = 0
		dip = 2
		mil = 1
		female = yes
    }
}

945.1.1 = {
	monarch = {
 		name = "Birkir"
		dynasty = "Roewallom"
		birth_date = 916.1.1
		adm = 5
		dip = 1
		mil = 5
    }
}

981.1.1 = {
	monarch = {
 		name = "Hermeskr"
		dynasty = "Tirmonmoll"
		birth_date = 960.1.1
		adm = 3
		dip = 0
		mil = 2
    }
}

1031.1.1 = {
	monarch = {
 		name = "Botrir"
		dynasty = "Skodnild"
		birth_date = 999.1.1
		adm = 2
		dip = 6
		mil = 5
    }
}

1100.1.1 = {
	monarch = {
 		name = "Thromgar"
		dynasty = "Geirvobond"
		birth_date = 1073.1.1
		adm = 0
		dip = 5
		mil = 2
    }
}

1185.1.1 = {
	monarch = {
 		name = "Maralthon"
		dynasty = "Bralg"
		birth_date = 1138.1.1
		adm = 5
		dip = 5
		mil = 6
    }
}

1270.1.1 = {
	monarch = {
 		name = "Hemming"
		dynasty = "Sungiaskr"
		birth_date = 1248.1.1
		adm = 3
		dip = 4
		mil = 2
    }
}

1347.1.1 = {
	monarch = {
 		name = "Bormir"
		dynasty = "Rhenwath"
		birth_date = 1303.1.1
		adm = 2
		dip = 3
		mil = 6
    }
}

1425.1.1 = {
	monarch = {
 		name = "Thragof"
		dynasty = "Dilg"
		birth_date = 1380.1.1
		adm = 4
		dip = 4
		mil = 0
    }
}

1462.1.1 = {
	monarch = {
 		name = "Manwe"
		dynasty = "Gorjold"
		birth_date = 1413.1.1
		adm = 2
		dip = 4
		mil = 4
    }
}

1520.1.1 = {
	monarch = {
 		name = "Torvar"
		dynasty = "Bryngeemir"
		birth_date = 1474.1.1
		adm = 0
		dip = 3
		mil = 1
    }
}

1613.1.1 = {
	monarch = {
 		name = "Molgrom"
		dynasty = "Boechyrom"
		birth_date = 1574.1.1
		adm = 5
		dip = 2
		mil = 4
    }
}

1690.1.1 = {
	monarch = {
 		name = "Hjoag"
		dynasty = "Gagrer"
		birth_date = 1664.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1753.1.1 = {
	monarch = {
 		name = "Bree"
		dynasty = "Dasgron"
		birth_date = 1714.1.1
		adm = 2
		dip = 0
		mil = 5
		female = yes
    }
}

1842.1.1 = {
	monarch = {
 		name = "Gadof"
		dynasty = "Kosner"
		birth_date = 1796.1.1
		adm = 3
		dip = 4
		mil = 3
    }
}

1886.1.1 = {
	monarch = {
 		name = "Yrsa"
		dynasty = "Hlalmadrun"
		birth_date = 1847.1.1
		adm = 1
		dip = 4
		mil = 6
		female = yes
    }
}

1936.1.1 = {
	monarch = {
 		name = "Rorred"
		dynasty = "Mesmannath"
		birth_date = 1909.1.1
		adm = 6
		dip = 3
		mil = 3
    }
}

2002.1.1 = {
	monarch = {
 		name = "Hrordis"
		dynasty = "Skoetgogam"
		birth_date = 1983.1.1
		adm = 4
		dip = 2
		mil = 0
		female = yes
    }
}

2086.1.1 = {
	monarch = {
 		name = "Fuldir"
		dynasty = "Freldohiof"
		birth_date = 2067.1.1
		adm = 3
		dip = 1
		mil = 3
    }
}

2181.1.1 = {
	monarch = {
 		name = "Yngvar"
		dynasty = "Hreythond"
		birth_date = 2150.1.1
		adm = 4
		dip = 3
		mil = 5
    }
}

2268.1.1 = {
	monarch = {
 		name = "Rorfid"
		dynasty = "Brundeskr"
		birth_date = 2223.1.1
		adm = 3
		dip = 2
		mil = 1
    }
}

2352.1.1 = {
	monarch = {
 		name = "Aelfhidil"
		dynasty = "Kosnorr"
		birth_date = 2316.1.1
		adm = 1
		dip = 1
		mil = 5
    }
}

2426.1.1 = {
	monarch = {
 		name = "Shalidor"
		dynasty = "Surjafald"
		birth_date = 2396.1.1
		adm = 6
		dip = 0
		mil = 2
    }
}

2477.1.1 = {
	monarch = {
 		name = "Jorald"
		dynasty = "Thedrildr"
		birth_date = 2449.1.1
		adm = 4
		dip = 6
		mil = 5
    }
}

