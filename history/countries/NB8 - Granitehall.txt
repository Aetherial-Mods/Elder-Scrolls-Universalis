government = monarchy
government_rank = 1
mercantilism = 1
technology_group = northern_tg
religion = nordic_pantheon
primary_culture = nord
capital = 3052

54.1.1 = {
	monarch = {
 		name = "Hjarda"
		dynasty = "Feld"
		birth_date = 8.1.1
		adm = 1
		dip = 5
		mil = 5
		female = yes
    }
	queen = {
 		name = "Heifnir"
		dynasty = "Feld"
		birth_date = 31.1.1
		adm = 1
		dip = 1
		mil = 6
    }
	heir = {
 		name = "Urlach"
		monarch_name = "Urlach I"
		dynasty = "Feld"
		birth_date = 40.1.1
		death_date = 138.1.1
		claim = 100
		adm = 5
		dip = 0
		mil = 5
    }
}

138.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Nystrom"
		monarch_name = "Nystrom I"
		dynasty = "Sarynd"
		birth_date = 127.1.1
		death_date = 145.1.1
		claim = 100
		adm = 3
		dip = 6
		mil = 2
    }
}

145.1.1 = {
	monarch = {
 		name = "Fenjor"
		dynasty = "Hagrudr"
		birth_date = 108.1.1
		adm = 2
		dip = 2
		mil = 2
    }
}

235.1.1 = {
	monarch = {
 		name = "Waren"
		dynasty = "Frodmeldof"
		birth_date = 195.1.1
		adm = 1
		dip = 2
		mil = 6
    }
	queen = {
 		name = "Gukirmar"
		dynasty = "Frodmeldof"
		birth_date = 207.1.1
		adm = 5
		dip = 0
		mil = 5
		female = yes
    }
	heir = {
 		name = "Unnvald"
		monarch_name = "Unnvald I"
		dynasty = "Frodmeldof"
		birth_date = 220.1.1
		death_date = 278.1.1
		claim = 100
		adm = 1
		dip = 6
		mil = 4
    }
}

278.1.1 = {
	monarch = {
 		name = "Hodyette"
		dynasty = "Vogeyfioldr"
		birth_date = 230.1.1
		adm = 4
		dip = 0
		mil = 6
		female = yes
    }
	queen = {
 		name = "Hildir"
		dynasty = "Vogeyfioldr"
		birth_date = 252.1.1
		adm = 1
		dip = 5
		mil = 5
    }
	heir = {
 		name = "Hoknir"
		monarch_name = "Hoknir I"
		dynasty = "Vogeyfioldr"
		birth_date = 264.1.1
		death_date = 342.1.1
		claim = 100
		adm = 5
		dip = 4
		mil = 4
    }
}

342.1.1 = {
	monarch = {
 		name = "Vareda"
		dynasty = "Brodrunlyn"
		birth_date = 301.1.1
		adm = 4
		dip = 0
		mil = 4
		female = yes
    }
}

411.1.1 = {
	monarch = {
 		name = "Narri"
		dynasty = "Gugnorr"
		birth_date = 370.1.1
		adm = 3
		dip = 0
		mil = 1
		female = yes
    }
	queen = {
 		name = "Maulkyr"
		dynasty = "Gugnorr"
		birth_date = 388.1.1
		adm = 6
		dip = 5
		mil = 0
    }
	heir = {
 		name = "Drorunn"
		monarch_name = "Drorunn I"
		dynasty = "Gugnorr"
		birth_date = 399.1.1
		death_date = 508.1.1
		claim = 100
		adm = 0
		dip = 2
		mil = 1
    }
}

508.1.1 = {
	monarch = {
 		name = "Oda"
		dynasty = "Segald"
		birth_date = 478.1.1
		adm = 6
		dip = 5
		mil = 1
		female = yes
    }
	queen = {
 		name = "Boward"
		dynasty = "Segald"
		birth_date = 479.1.1
		adm = 3
		dip = 4
		mil = 0
    }
	heir = {
 		name = "Ollfar"
		monarch_name = "Ollfar I"
		dynasty = "Segald"
		birth_date = 508.1.1
		death_date = 572.1.1
		claim = 100
		adm = 0
		dip = 2
		mil = 6
    }
}

572.1.1 = {
	monarch = {
 		name = "Eyja"
		dynasty = "Hitys"
		birth_date = 546.1.1
		adm = 3
		dip = 3
		mil = 2
		female = yes
    }
	queen = {
 		name = "Mathon"
		dynasty = "Hitys"
		birth_date = 519.1.1
		adm = 6
		dip = 2
		mil = 1
    }
}

647.1.1 = {
	monarch = {
 		name = "Kjeld"
		dynasty = "Bruleihof"
		birth_date = 617.1.1
		adm = 5
		dip = 1
		mil = 4
    }
	queen = {
 		name = "Tirlarnar"
		dynasty = "Bruleihof"
		birth_date = 627.1.1
		adm = 2
		dip = 0
		mil = 3
		female = yes
    }
	heir = {
 		name = "Jagyr"
		monarch_name = "Jagyr I"
		dynasty = "Bruleihof"
		birth_date = 632.1.1
		death_date = 694.1.1
		claim = 100
		adm = 5
		dip = 5
		mil = 2
    }
}

694.1.1 = {
	monarch = {
 		name = "Alga"
		dynasty = "Frargil"
		birth_date = 654.1.1
		adm = 1
		dip = 0
		mil = 5
		female = yes
    }
}

733.1.1 = {
	monarch = {
 		name = "Reja"
		dynasty = "Stindurilg"
		birth_date = 689.1.1
		adm = 3
		dip = 1
		mil = 6
		female = yes
    }
}

796.1.1 = {
	monarch = {
 		name = "Kjakur"
		dynasty = "Briguf"
		birth_date = 750.1.1
		adm = 1
		dip = 0
		mil = 3
    }
	queen = {
 		name = "Hjalif"
		dynasty = "Briguf"
		birth_date = 747.1.1
		adm = 5
		dip = 6
		mil = 2
		female = yes
    }
	heir = {
 		name = "Addald"
		monarch_name = "Addald I"
		dynasty = "Briguf"
		birth_date = 788.1.1
		death_date = 871.1.1
		claim = 100
		adm = 5
		dip = 2
		mil = 3
    }
}

871.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Saborm"
		monarch_name = "Saborm I"
		dynasty = "Rhilg"
		birth_date = 871.1.1
		death_date = 889.1.1
		claim = 100
		adm = 0
		dip = 3
		mil = 4
    }
}

889.1.1 = {
	monarch = {
 		name = "Gudaric"
		dynasty = "Dusnanl"
		birth_date = 840.1.1
		adm = 3
		dip = 5
		mil = 0
    }
	queen = {
 		name = "Meling"
		dynasty = "Dusnanl"
		birth_date = 847.1.1
		adm = 0
		dip = 3
		mil = 6
		female = yes
    }
	heir = {
 		name = "Gamirth"
		monarch_name = "Gamirth I"
		dynasty = "Dusnanl"
		birth_date = 878.1.1
		death_date = 971.1.1
		claim = 100
		adm = 4
		dip = 2
		mil = 5
    }
}

971.1.1 = {
	monarch = {
 		name = "Snorfin"
		dynasty = "Heyrmin"
		birth_date = 935.1.1
		adm = 6
		dip = 3
		mil = 0
    }
	queen = {
 		name = "Narri"
		dynasty = "Heyrmin"
		birth_date = 934.1.1
		adm = 3
		dip = 2
		mil = 6
		female = yes
    }
	heir = {
 		name = "Runthor"
		monarch_name = "Runthor I"
		dynasty = "Heyrmin"
		birth_date = 965.1.1
		death_date = 1051.1.1
		claim = 100
		adm = 0
		dip = 0
		mil = 5
    }
}

1051.1.1 = {
	monarch = {
 		name = "Gromm"
		dynasty = "Gagrer"
		birth_date = 1011.1.1
		adm = 0
		dip = 4
		mil = 5
    }
}

1105.1.1 = {
	monarch = {
 		name = "Angetha"
		dynasty = "Rotmalleskr"
		birth_date = 1084.1.1
		adm = 5
		dip = 3
		mil = 2
		female = yes
    }
}

1190.1.1 = {
	monarch = {
 		name = "Geirvarda"
		dynasty = "Tholnofeldr"
		birth_date = 1161.1.1
		adm = 3
		dip = 2
		mil = 5
		female = yes
    }
	queen = {
 		name = "Harvald"
		dynasty = "Tholnofeldr"
		birth_date = 1167.1.1
		adm = 0
		dip = 0
		mil = 4
    }
	heir = {
 		name = "Franja"
		monarch_name = "Franja I"
		dynasty = "Tholnofeldr"
		birth_date = 1184.1.1
		death_date = 1285.1.1
		claim = 100
		adm = 4
		dip = 6
		mil = 3
		female = yes
    }
}

1285.1.1 = {
	monarch = {
 		name = "Seirida"
		dynasty = "Fruveld"
		birth_date = 1243.1.1
		adm = 4
		dip = 3
		mil = 4
		female = yes
    }
}

1357.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Sifmir"
		monarch_name = "Sifmir I"
		dynasty = "Gorvar"
		birth_date = 1357.1.1
		death_date = 1375.1.1
		claim = 100
		adm = 0
		dip = 4
		mil = 4
    }
}

1375.1.1 = {
	monarch = {
 		name = "Guthrum"
		dynasty = "Stindurilg"
		birth_date = 1341.1.1
		adm = 4
		dip = 3
		mil = 3
    }
	queen = {
 		name = "Suwenna"
		dynasty = "Stindurilg"
		birth_date = 1346.1.1
		adm = 0
		dip = 4
		mil = 5
		female = yes
    }
	heir = {
 		name = "Hermir"
		monarch_name = "Hermir I"
		dynasty = "Stindurilg"
		birth_date = 1360.1.1
		death_date = 1472.1.1
		claim = 100
		adm = 1
		dip = 5
		mil = 3
		female = yes
    }
}

1472.1.1 = {
	monarch = {
 		name = "Wulfdis"
		dynasty = "Homyld"
		birth_date = 1441.1.1
		adm = 1
		dip = 1
		mil = 3
    }
	queen = {
 		name = "Guridda"
		dynasty = "Homyld"
		birth_date = 1446.1.1
		adm = 5
		dip = 0
		mil = 2
		female = yes
    }
	heir = {
 		name = "Valdemar"
		monarch_name = "Valdemar I"
		dynasty = "Homyld"
		birth_date = 1467.1.1
		death_date = 1514.1.1
		claim = 100
		adm = 1
		dip = 5
		mil = 1
    }
}

1514.1.1 = {
	monarch = {
 		name = "Hranir"
		dynasty = "Snake-eye"
		birth_date = 1478.1.1
		adm = 4
		dip = 0
		mil = 3
		female = yes
    }
	queen = {
 		name = "Thragof"
		dynasty = "Snake-eye"
		birth_date = 1496.1.1
		adm = 1
		dip = 5
		mil = 2
    }
	heir = {
 		name = "Honrid"
		monarch_name = "Honrid I"
		dynasty = "Snake-eye"
		birth_date = 1513.1.1
		death_date = 1550.1.1
		claim = 100
		adm = 5
		dip = 4
		mil = 1
    }
}

1550.1.1 = {
	monarch = {
 		name = "Woan"
		dynasty = "Hitys"
		birth_date = 1524.1.1
		adm = 1
		dip = 5
		mil = 4
    }
	queen = {
 		name = "Tela"
		dynasty = "Hitys"
		birth_date = 1526.1.1
		adm = 5
		dip = 4
		mil = 3
		female = yes
    }
	heir = {
 		name = "Hjagir"
		monarch_name = "Hjagir I"
		dynasty = "Hitys"
		birth_date = 1550.1.1
		death_date = 1607.1.1
		claim = 100
		adm = 1
		dip = 2
		mil = 1
		female = yes
    }
}

1607.1.1 = {
	monarch = {
 		name = "Isilr"
		dynasty = "Dorkalg"
		birth_date = 1588.1.1
		adm = 1
		dip = 6
		mil = 2
    }
}

1702.1.1 = {
	monarch = {
 		name = "Runthor"
		dynasty = "Stetmak"
		birth_date = 1670.1.1
		adm = 6
		dip = 5
		mil = 5
    }
	queen = {
 		name = "Bree"
		dynasty = "Stetmak"
		birth_date = 1668.1.1
		adm = 3
		dip = 3
		mil = 4
		female = yes
    }
	heir = {
 		name = "Orstag"
		monarch_name = "Orstag I"
		dynasty = "Stetmak"
		birth_date = 1692.1.1
		death_date = 1785.1.1
		claim = 100
		adm = 0
		dip = 2
		mil = 3
    }
}

1785.1.1 = {
	monarch = {
 		name = "Gadof"
		dynasty = "Homyld"
		birth_date = 1762.1.1
		adm = 3
		dip = 3
		mil = 6
    }
}

1828.1.1 = {
	monarch = {
 		name = "Yrsarald"
		dynasty = "Kosner"
		birth_date = 1805.1.1
		adm = 1
		dip = 3
		mil = 2
    }
	queen = {
 		name = "Halbora"
		dynasty = "Kosner"
		birth_date = 1804.1.1
		adm = 5
		dip = 1
		mil = 1
		female = yes
    }
}

1893.1.1 = {
	monarch = {
 		name = "Grisvar"
		dynasty = "Thugnom"
		birth_date = 1869.1.1
		adm = 3
		dip = 0
		mil = 5
    }
	queen = {
 		name = "Maylde"
		dynasty = "Thugnom"
		birth_date = 1841.1.1
		adm = 0
		dip = 6
		mil = 4
		female = yes
    }
	heir = {
 		name = "Fultheim"
		monarch_name = "Fultheim I"
		dynasty = "Thugnom"
		birth_date = 1892.1.1
		death_date = 1968.1.1
		claim = 100
		adm = 4
		dip = 4
		mil = 3
    }
}

1968.1.1 = {
	monarch = {
 		name = "Rigmor"
		dynasty = "Hjangris"
		birth_date = 1948.1.1
		adm = 6
		dip = 6
		mil = 5
		female = yes
    }
}

2026.1.1 = {
	monarch = {
 		name = "Ittir"
		dynasty = "Magdrond"
		birth_date = 2000.1.1
		adm = 1
		dip = 0
		mil = 0
		female = yes
    }
	queen = {
 		name = "Hylf"
		dynasty = "Magdrond"
		birth_date = 1987.1.1
		adm = 2
		dip = 3
		mil = 1
    }
	heir = {
 		name = "Aelfhidil"
		monarch_name = "Aelfhidil I"
		dynasty = "Magdrond"
		birth_date = 2013.1.1
		death_date = 2092.1.1
		claim = 100
		adm = 5
		dip = 2
		mil = 0
    }
}

2092.1.1 = {
	monarch = {
 		name = "Lanobert"
		dynasty = "Valdubild"
		birth_date = 2073.1.1
		adm = 5
		dip = 5
		mil = 0
    }
}

2157.1.1 = {
	monarch = {
 		name = "Gunthing"
		dynasty = "Rerlok"
		birth_date = 2106.1.1
		adm = 3
		dip = 5
		mil = 4
    }
}

2198.1.1 = {
	monarch = {
 		name = "Avulrund"
		dynasty = "Ridyl"
		birth_date = 2156.1.1
		adm = 1
		dip = 4
		mil = 0
    }
	queen = {
 		name = "Hjolbel"
		dynasty = "Ridyl"
		birth_date = 2150.1.1
		adm = 5
		dip = 2
		mil = 6
		female = yes
    }
	heir = {
 		name = "Adpring"
		monarch_name = "Adpring I"
		dynasty = "Ridyl"
		birth_date = 2189.1.1
		death_date = 2292.1.1
		claim = 100
		adm = 2
		dip = 1
		mil = 5
    }
}

2292.1.1 = {
	monarch = {
 		name = "Kust"
		dynasty = "Hledrind"
		birth_date = 2242.1.1
		adm = 5
		dip = 2
		mil = 1
    }
	queen = {
 		name = "Hostia"
		dynasty = "Hledrind"
		birth_date = 2257.1.1
		adm = 2
		dip = 1
		mil = 0
		female = yes
    }
	heir = {
 		name = "Jomund"
		monarch_name = "Jomund I"
		dynasty = "Hledrind"
		birth_date = 2277.1.1
		death_date = 2387.1.1
		claim = 100
		adm = 6
		dip = 6
		mil = 6
    }
}

2387.1.1 = {
	monarch = {
 		name = "Aud"
		dynasty = "Geirvobond"
		birth_date = 2356.1.1
		adm = 5
		dip = 3
		mil = 6
    }
	queen = {
 		name = "Vigwenn"
		dynasty = "Geirvobond"
		birth_date = 2348.1.1
		adm = 5
		dip = 6
		mil = 0
		female = yes
    }
	heir = {
 		name = "Ingun"
		monarch_name = "Ingun I"
		dynasty = "Geirvobond"
		birth_date = 2377.1.1
		death_date = 2439.1.1
		claim = 100
		adm = 2
		dip = 5
		mil = 6
		female = yes
    }
}

2439.1.1 = {
	monarch = {
 		name = "Bassianus"
		dynasty = "Dusnanl"
		birth_date = 2407.1.1
		adm = 1
		dip = 1
		mil = 6
    }
	queen = {
 		name = "Holmgeira"
		dynasty = "Dusnanl"
		birth_date = 2421.1.1
		adm = 5
		dip = 0
		mil = 5
		female = yes
    }
	heir = {
 		name = "Alfarmuth"
		monarch_name = "Alfarmuth I"
		dynasty = "Dusnanl"
		birth_date = 2432.1.1
		death_date = 2479.1.1
		claim = 100
		adm = 2
		dip = 5
		mil = 4
    }
}

2479.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Sirk"
		monarch_name = "Sirk I"
		dynasty = "Veladrys"
		birth_date = 2473.1.1
		death_date = 2491.1.1
		claim = 100
		adm = 0
		dip = 4
		mil = 1
    }
}

2491.1.1 = {
	monarch = {
 		name = "Hajvarr"
		dynasty = "Svedryl"
		birth_date = 2445.1.1
		adm = 3
		dip = 6
		mil = 3
    }
}

