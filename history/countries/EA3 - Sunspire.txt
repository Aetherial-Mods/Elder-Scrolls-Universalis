government = tribal
government_rank = 1
mercantilism = 1
technology_group = elsweyr_tg
religion = khajiiti_pantheon
primary_culture = khajiiti
capital = 5277

54.1.1 = {
	monarch = {
 		name = "Mazu"
		dynasty = "Akh'irr"
		birth_date = 18.1.1
		adm = 4
		dip = 6
		mil = 2
		female = yes
    }
}

114.1.1 = {
	monarch = {
 		name = "Fahurr"
		dynasty = "Ri'zaadha"
		birth_date = 65.1.1
		adm = 2
		dip = 5
		mil = 6
    }
}

169.1.1 = {
	monarch = {
 		name = "Zal-sa"
		dynasty = "Baadidzo"
		birth_date = 146.1.1
		adm = 1
		dip = 4
		mil = 2
    }
}

259.1.1 = {
	monarch = {
 		name = "Sharrasti"
		dynasty = "Ranatasarr"
		birth_date = 239.1.1
		adm = 6
		dip = 3
		mil = 6
		female = yes
    }
}

341.1.1 = {
	monarch = {
 		name = "Kundurr"
		dynasty = "Sijsopor"
		birth_date = 315.1.1
		adm = 4
		dip = 2
		mil = 3
    }
}

379.1.1 = {
	monarch = {
 		name = "Evani"
		dynasty = "Zoadran"
		birth_date = 352.1.1
		adm = 6
		dip = 4
		mil = 4
		female = yes
    }
}

442.1.1 = {
	monarch = {
 		name = "Zaham"
		dynasty = "Kimnirn"
		birth_date = 419.1.1
		adm = 4
		dip = 3
		mil = 1
    }
}

535.1.1 = {
	monarch = {
 		name = "Felira"
		dynasty = "Bhijhad"
		birth_date = 491.1.1
		adm = 2
		dip = 2
		mil = 5
		female = yes
    }
}

573.1.1 = {
	monarch = {
 		name = "Zarziri"
		dynasty = "Havnusopor"
		birth_date = 554.1.1
		adm = 1
		dip = 1
		mil = 1
    }
}

629.1.1 = {
	monarch = {
 		name = "Renaku"
		dynasty = "Omsaad"
		birth_date = 585.1.1
		adm = 6
		dip = 1
		mil = 5
    }
}

724.1.1 = {
	monarch = {
 		name = "Lharlis"
		dynasty = "Shoava"
		birth_date = 679.1.1
		adm = 4
		dip = 0
		mil = 2
    }
}

804.1.1 = {
	monarch = {
 		name = "Fezez"
		dynasty = "Omakha"
		birth_date = 775.1.1
		adm = 2
		dip = 6
		mil = 5
    }
}

901.1.1 = {
	monarch = {
 		name = "Zargal"
		dynasty = "Jodara"
		birth_date = 854.1.1
		adm = 1
		dip = 5
		mil = 2
    }
}

941.1.1 = {
	monarch = {
 		name = "Razur"
		dynasty = "Rawinai"
		birth_date = 889.1.1
		adm = 3
		dip = 6
		mil = 3
    }
}

1029.1.1 = {
	monarch = {
 		name = "Lansur"
		dynasty = "Akh'irr"
		birth_date = 996.1.1
		adm = 1
		dip = 6
		mil = 0
    }
}

1120.1.1 = {
	monarch = {
 		name = "Shuzirri"
		dynasty = "Bhijhad"
		birth_date = 1094.1.1
		adm = 6
		dip = 5
		mil = 4
		female = yes
    }
}

1219.1.1 = {
	monarch = {
 		name = "Mabkir"
		dynasty = "Ahjgarvi"
		birth_date = 1166.1.1
		adm = 4
		dip = 4
		mil = 0
    }
}

1276.1.1 = {
	monarch = {
 		name = "Haheiba"
		dynasty = "Sarahasin"
		birth_date = 1230.1.1
		adm = 3
		dip = 3
		mil = 4
		female = yes
    }
}

1332.1.1 = {
	monarch = {
 		name = "Zhiraz"
		dynasty = "Hasgh"
		birth_date = 1290.1.1
		adm = 1
		dip = 2
		mil = 1
    }
}

1383.1.1 = {
	monarch = {
 		name = "Rinam"
		dynasty = "Toviktanni"
		birth_date = 1363.1.1
		adm = 6
		dip = 2
		mil = 4
    }
}

1425.1.1 = {
	monarch = {
 		name = "Ma'jhad"
		dynasty = "Jo'arkhu"
		birth_date = 1399.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1461.1.1 = {
	monarch = {
 		name = "Finnah"
		dynasty = "Kavandi"
		birth_date = 1433.1.1
		adm = 6
		dip = 2
		mil = 2
		female = yes
    }
}

1535.1.1 = {
	monarch = {
 		name = "Zharahn"
		dynasty = "Zarhan"
		birth_date = 1499.1.1
		adm = 4
		dip = 1
		mil = 6
    }
}

1625.1.1 = {
	monarch = {
 		name = "Halash"
		dynasty = "Bavagarvi"
		birth_date = 1584.1.1
		adm = 3
		dip = 1
		mil = 3
    }
}

1685.1.1 = {
	monarch = {
 		name = "Abiznaz"
		dynasty = "Jotadirr"
		birth_date = 1657.1.1
		adm = 1
		dip = 0
		mil = 6
    }
}

1737.1.1 = {
	monarch = {
 		name = "Sa'dir"
		dynasty = "Ahjhir"
		birth_date = 1691.1.1
		adm = 6
		dip = 6
		mil = 3
    }
}

1832.1.1 = {
	monarch = {
 		name = "Nagarra"
		dynasty = "J'arr"
		birth_date = 1806.1.1
		adm = 4
		dip = 5
		mil = 0
		female = yes
    }
}

1896.1.1 = {
	monarch = {
 		name = "Halakalal"
		dynasty = "Havnusopor"
		birth_date = 1868.1.1
		adm = 3
		dip = 4
		mil = 3
    }
}

1947.1.1 = {
	monarch = {
 		name = "Mizrali"
		dynasty = "Zoarkir"
		birth_date = 1911.1.1
		adm = 3
		dip = 1
		mil = 1
		female = yes
    }
}

1988.1.1 = {
	monarch = {
 		name = "Bakhig-ja"
		dynasty = "Helarr-Jo"
		birth_date = 1941.1.1
		adm = 2
		dip = 1
		mil = 5
    }
}

2072.1.1 = {
	monarch = {
 		name = "Tahar-dar"
		dynasty = "Raioni"
		birth_date = 2033.1.1
		adm = 0
		dip = 0
		mil = 2
    }
}

2167.1.1 = {
	monarch = {
 		name = "Raerabhi"
		dynasty = "Baadjhera"
		birth_date = 2136.1.1
		adm = 5
		dip = 6
		mil = 5
		female = yes
    }
}

2266.1.1 = {
	monarch = {
 		name = "J'zagh"
		dynasty = "Hasgh"
		birth_date = 2223.1.1
		adm = 0
		dip = 0
		mil = 0
    }
}

2348.1.1 = {
	monarch = {
 		name = "Bilunna"
		dynasty = "Kijibiri"
		birth_date = 2321.1.1
		adm = 5
		dip = 0
		mil = 3
		female = yes
    }
}

2408.1.1 = {
	monarch = {
 		name = "Jobai"
		dynasty = "Roudavi"
		birth_date = 2363.1.1
		adm = 4
		dip = 6
		mil = 0
    }
}

2488.1.1 = {
	monarch = {
 		name = "Birun"
		dynasty = "Omakha"
		birth_date = 2469.1.1
		adm = 2
		dip = 5
		mil = 4
    }
}

