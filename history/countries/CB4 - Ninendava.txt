government = monarchy
government_rank = 1
mercantilism = 1
technology_group = cyrodiil_tg
religion = hermeus_mora_cult
primary_culture = ayleid
capital = 1184

54.1.1 = {
	monarch = {
 		name = "Gordhaur"
		dynasty = "of Shapers"
		birth_date = 8.1.1
		adm = 6
		dip = 1
		mil = 4
    }
}

190.1.1 = {
	monarch = {
 		name = "Narges"
		dynasty = "Vae-Sena"
		birth_date = 120.1.1
		adm = 2
		dip = 0
		mil = 2
    }
}

241.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Cenedelin"
		monarch_name = "Cenedelin I"
		dynasty = "Ula-Kvatch"
		birth_date = 237.1.1
		death_date = 255.1.1
		claim = 100
		adm = 6
		dip = 2
		mil = 2
    }
}

255.1.1 = {
	monarch = {
 		name = "Gemhel"
		dynasty = "Vae-Sercen"
		birth_date = 228.1.1
		adm = 6
		dip = 5
		mil = 3
    }
}

337.1.1 = {
	monarch = {
 		name = "Hennynlyho"
		dynasty = "Ula-Lildana"
		birth_date = 309.1.1
		adm = 0
		dip = 2
		mil = 1
		female = yes
    }
	queen = {
 		name = "Tjurhane"
		dynasty = "Ula-Lildana"
		birth_date = 318.1.1
		adm = 3
		dip = 1
		mil = 0
    }
	heir = {
 		name = "Wahemnyhol"
		monarch_name = "Wahemnyhol I"
		dynasty = "Ula-Lildana"
		birth_date = 326.1.1
		death_date = 402.1.1
		claim = 100
		adm = 4
		dip = 4
		mil = 1
		female = yes
    }
}

402.1.1 = {
	monarch = {
 		name = "Djus"
		dynasty = "Vae-Sardavar"
		birth_date = 372.1.1
		adm = 3
		dip = 1
		mil = 1
    }
}

469.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Guumond"
		monarch_name = "Guumond I"
		dynasty = "Ula-Lildana"
		birth_date = 467.1.1
		death_date = 485.1.1
		claim = 100
		adm = 4
		dip = 5
		mil = 6
    }
}

485.1.1 = {
	monarch = {
 		name = "Tedygedo"
		dynasty = "Mea-Civiant"
		birth_date = 451.1.1
		adm = 2
		dip = 4
		mil = 3
    }
	queen = {
 		name = "Monado"
		dynasty = "Mea-Civiant"
		birth_date = 464.1.1
		adm = 6
		dip = 2
		mil = 2
		female = yes
    }
}

563.1.1 = {
	monarch = {
 		name = "Fyndiscas"
		dynasty = "Vae-Varondo"
		birth_date = 522.1.1
		adm = 4
		dip = 2
		mil = 5
    }
	queen = {
 		name = "Ymmilune"
		dynasty = "Vae-Varondo"
		birth_date = 511.1.1
		adm = 1
		dip = 0
		mil = 4
		female = yes
    }
	heir = {
 		name = "Luudagarund"
		monarch_name = "Luudagarund I"
		dynasty = "Vae-Varondo"
		birth_date = 555.1.1
		death_date = 652.1.1
		claim = 100
		adm = 5
		dip = 6
		mil = 3
    }
}

652.1.1 = {
	monarch = {
 		name = "Cur"
		dynasty = "Mea-Conoa"
		birth_date = 607.1.1
		adm = 1
		dip = 0
		mil = 5
    }
	queen = {
 		name = "Nenemmeve"
		dynasty = "Mea-Conoa"
		birth_date = 604.1.1
		adm = 5
		dip = 6
		mil = 4
		female = yes
    }
	heir = {
 		name = "Cyledha"
		monarch_name = "Cyledha I"
		dynasty = "Mea-Conoa"
		birth_date = 642.1.1
		death_date = 712.1.1
		claim = 100
		adm = 1
		dip = 4
		mil = 3
		female = yes
    }
}

712.1.1 = {
	monarch = {
 		name = "Cossin"
		dynasty = "Mea-Bravil"
		birth_date = 670.1.1
		adm = 4
		dip = 5
		mil = 6
    }
}

804.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Luudagarund"
		monarch_name = "Luudagarund I"
		dynasty = "Ula-Nenyond"
		birth_date = 790.1.1
		death_date = 808.1.1
		claim = 100
		adm = 5
		dip = 2
		mil = 4
    }
}

808.1.1 = {
	monarch = {
 		name = "Cur"
		dynasty = "Vae-Sialerius"
		birth_date = 787.1.1
		adm = 4
		dip = 6
		mil = 4
    }
}

888.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Undorrau"
		monarch_name = "Undorrau I"
		dynasty = "Ula-Linchal"
		birth_date = 880.1.1
		death_date = 898.1.1
		claim = 100
		adm = 1
		dip = 1
		mil = 4
    }
}

898.1.1 = {
	monarch = {
 		name = "Qessem"
		dynasty = "Vae-Vesagrius"
		birth_date = 875.1.1
		adm = 1
		dip = 4
		mil = 4
    }
	queen = {
 		name = "Came"
		dynasty = "Vae-Vesagrius"
		birth_date = 858.1.1
		adm = 5
		dip = 3
		mil = 3
		female = yes
    }
	heir = {
 		name = "Danasso"
		monarch_name = "Danasso I"
		dynasty = "Vae-Vesagrius"
		birth_date = 891.1.1
		death_date = 986.1.1
		claim = 100
		adm = 2
		dip = 1
		mil = 2
		female = yes
    }
}

986.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Narges"
		monarch_name = "Narges I"
		dynasty = "Ula-Nonungalo"
		birth_date = 982.1.1
		death_date = 1000.1.1
		claim = 100
		adm = 0
		dip = 1
		mil = 6
    }
}

1000.1.1 = {
	monarch = {
 		name = "Umarik"
		dynasty = "Ula-Horuria"
		birth_date = 968.1.1
		adm = 3
		dip = 2
		mil = 1
    }
}

1043.1.1 = {
	monarch = {
 		name = "Eradhiho"
		dynasty = "Vae-Weatherleah"
		birth_date = 1013.1.1
		adm = 1
		dip = 1
		mil = 5
		female = yes
    }
	queen = {
 		name = "Hasind"
		dynasty = "Vae-Weatherleah"
		birth_date = 997.1.1
		adm = 5
		dip = 0
		mil = 4
    }
	heir = {
 		name = "Danasso"
		monarch_name = "Danasso I"
		dynasty = "Vae-Weatherleah"
		birth_date = 1030.1.1
		death_date = 1115.1.1
		claim = 100
		adm = 2
		dip = 5
		mil = 3
		female = yes
    }
}

1115.1.1 = {
	monarch = {
 		name = "Cylyris"
		dynasty = "Mea-Bravil"
		birth_date = 1065.1.1
		adm = 1
		dip = 2
		mil = 3
		female = yes
    }
}

1178.1.1 = {
	monarch = {
 		name = "Umarik"
		dynasty = "Vae-Sylolvia"
		birth_date = 1145.1.1
		adm = 6
		dip = 1
		mil = 0
    }
}

1265.1.1 = {
	monarch = {
 		name = "Limdardhunur"
		dynasty = "Ula-Matuseius"
		birth_date = 1226.1.1
		adm = 4
		dip = 0
		mil = 3
    }
	queen = {
 		name = "Symmu"
		dynasty = "Ula-Matuseius"
		birth_date = 1224.1.1
		adm = 1
		dip = 6
		mil = 2
		female = yes
    }
	heir = {
 		name = "Lymher"
		monarch_name = "Lymher I"
		dynasty = "Ula-Matuseius"
		birth_date = 1263.1.1
		death_date = 1358.1.1
		claim = 100
		adm = 5
		dip = 4
		mil = 1
    }
}

1358.1.1 = {
	monarch = {
 		name = "Vuudunt"
		dynasty = "Vae-Vesagrius"
		birth_date = 1317.1.1
		adm = 1
		dip = 5
		mil = 4
    }
	queen = {
 		name = "Sezenhuvo"
		dynasty = "Vae-Vesagrius"
		birth_date = 1318.1.1
		adm = 5
		dip = 4
		mil = 3
		female = yes
    }
	heir = {
 		name = "Djador"
		monarch_name = "Djador I"
		dynasty = "Vae-Vesagrius"
		birth_date = 1350.1.1
		death_date = 1405.1.1
		claim = 100
		adm = 2
		dip = 2
		mil = 2
    }
}

1405.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Illerra"
		monarch_name = "Illerra I"
		dynasty = "Mea-Anvil"
		birth_date = 1392.1.1
		death_date = 1410.1.1
		claim = 100
		adm = 0
		dip = 2
		mil = 5
		female = yes
    }
}

1410.1.1 = {
	monarch = {
 		name = "Cumhul"
		dynasty = "Mea-Arriastae"
		birth_date = 1370.1.1
		adm = 3
		dip = 3
		mil = 1
    }
}

1460.1.1 = {
	monarch = {
 		name = "Nanalne"
		dynasty = "Vae-Sercen"
		birth_date = 1435.1.1
		adm = 5
		dip = 4
		mil = 2
		female = yes
    }
}

1559.1.1 = {
	monarch = {
 		name = "Synarlysush"
		dynasty = "Ula-Isollaise"
		birth_date = 1506.1.1
		adm = 3
		dip = 4
		mil = 6
		female = yes
    }
	queen = {
 		name = "Rorguuramis"
		dynasty = "Ula-Isollaise"
		birth_date = 1514.1.1
		adm = 0
		dip = 2
		mil = 5
    }
	heir = {
 		name = "Lymma"
		monarch_name = "Lymma I"
		dynasty = "Ula-Isollaise"
		birth_date = 1555.1.1
		death_date = 1608.1.1
		claim = 100
		adm = 0
		dip = 6
		mil = 6
		female = yes
    }
}

1608.1.1 = {
	monarch = {
 		name = "Hunro"
		dynasty = "Ula-Matuseius"
		birth_date = 1583.1.1
		adm = 6
		dip = 2
		mil = 6
    }
	queen = {
 		name = "Illarrymo"
		dynasty = "Ula-Matuseius"
		birth_date = 1588.1.1
		adm = 3
		dip = 0
		mil = 5
		female = yes
    }
	heir = {
 		name = "Yhu"
		monarch_name = "Yhu I"
		dynasty = "Ula-Matuseius"
		birth_date = 1600.1.1
		death_date = 1692.1.1
		claim = 100
		adm = 0
		dip = 6
		mil = 4
		female = yes
    }
}

1692.1.1 = {
	monarch = {
 		name = "Qystur"
		dynasty = "Mea-Aerienna"
		birth_date = 1654.1.1
		adm = 3
		dip = 0
		mil = 6
    }
	queen = {
 		name = "Sumadhiash"
		dynasty = "Mea-Aerienna"
		birth_date = 1641.1.1
		adm = 0
		dip = 6
		mil = 5
		female = yes
    }
	heir = {
 		name = "Uluscant"
		monarch_name = "Uluscant I"
		dynasty = "Mea-Aerienna"
		birth_date = 1678.1.1
		death_date = 1767.1.1
		claim = 100
		adm = 4
		dip = 4
		mil = 4
    }
}

1767.1.1 = {
	monarch = {
 		name = "Hunro"
		dynasty = "Mea-Hastrel"
		birth_date = 1741.1.1
		adm = 6
		dip = 6
		mil = 0
    }
	queen = {
 		name = "Myzas"
		dynasty = "Mea-Hastrel"
		birth_date = 1730.1.1
		adm = 3
		dip = 4
		mil = 6
		female = yes
    }
	heir = {
 		name = "Umarik"
		monarch_name = "Umarik I"
		dynasty = "Mea-Hastrel"
		birth_date = 1765.1.1
		death_date = 1820.1.1
		claim = 100
		adm = 0
		dip = 3
		mil = 5
    }
}

1820.1.1 = {
	monarch = {
 		name = "Lyzelniniath"
		dynasty = "Mea-Amane"
		birth_date = 1783.1.1
		adm = 6
		dip = 6
		mil = 5
		female = yes
    }
	queen = {
 		name = "Glinferen"
		dynasty = "Mea-Amane"
		birth_date = 1789.1.1
		adm = 3
		dip = 5
		mil = 4
    }
	heir = {
 		name = "Allonde"
		monarch_name = "Allonde I"
		dynasty = "Mea-Amane"
		birth_date = 1809.1.1
		death_date = 1894.1.1
		claim = 100
		adm = 3
		dip = 1
		mil = 5
		female = yes
    }
}

1894.1.1 = {
	monarch = {
 		name = "Lurdon"
		dynasty = "Ula-Messiena"
		birth_date = 1855.1.1
		adm = 3
		dip = 5
		mil = 5
    }
	queen = {
 		name = "Vurel"
		dynasty = "Ula-Messiena"
		birth_date = 1847.1.1
		adm = 0
		dip = 3
		mil = 4
		female = yes
    }
	heir = {
 		name = "Ynnylsasul"
		monarch_name = "Ynnylsasul I"
		dynasty = "Ula-Messiena"
		birth_date = 1892.1.1
		death_date = 1947.1.1
		claim = 100
		adm = 4
		dip = 2
		mil = 3
		female = yes
    }
}

1947.1.1 = {
	monarch = {
 		name = "Lunnu"
		dynasty = "Mea-Etirina"
		birth_date = 1926.1.1
		adm = 6
		dip = 3
		mil = 5
		female = yes
    }
	queen = {
 		name = "Glinferen"
		dynasty = "Mea-Etirina"
		birth_date = 1919.1.1
		adm = 3
		dip = 2
		mil = 4
    }
	heir = {
 		name = "Djamduusceges"
		monarch_name = "Djamduusceges I"
		dynasty = "Mea-Etirina"
		birth_date = 1936.1.1
		death_date = 2002.1.1
		claim = 100
		adm = 0
		dip = 0
		mil = 3
    }
}

2002.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Emma"
		monarch_name = "Emma I"
		dynasty = "Mea-Hastrel"
		birth_date = 1995.1.1
		death_date = 2013.1.1
		claim = 100
		adm = 5
		dip = 6
		mil = 0
		female = yes
    }
}

2013.1.1 = {
	monarch = {
 		name = "Endarre"
		dynasty = "Vae-Vesagrius"
		birth_date = 1977.1.1
		adm = 4
		dip = 6
		mil = 4
    }
	queen = {
 		name = "Pymmynnenu"
		dynasty = "Vae-Vesagrius"
		birth_date = 1989.1.1
		adm = 0
		dip = 4
		mil = 3
		female = yes
    }
	heir = {
 		name = "Heystuadhisind"
		monarch_name = "Heystuadhisind I"
		dynasty = "Vae-Vesagrius"
		birth_date = 2001.1.1
		death_date = 2089.1.1
		claim = 100
		adm = 4
		dip = 3
		mil = 2
    }
}

2089.1.1 = {
	monarch = {
 		name = "Lagan"
		dynasty = "Ula-Kvatch"
		birth_date = 2067.1.1
		adm = 0
		dip = 4
		mil = 4
    }
}

2129.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Nanalne"
		monarch_name = "Nanalne I"
		dynasty = "Vae-Waelori"
		birth_date = 2126.1.1
		death_date = 2144.1.1
		claim = 100
		adm = 1
		dip = 1
		mil = 2
		female = yes
    }
}

2144.1.1 = {
	monarch = {
 		name = "Cordu"
		dynasty = "Vae-Tasaso"
		birth_date = 2094.1.1
		adm = 0
		dip = 4
		mil = 2
    }
}

2207.1.1 = {
	monarch = {
 		name = "Sumadhiash"
		dynasty = "Vae-Sialerius"
		birth_date = 2171.1.1
		adm = 5
		dip = 4
		mil = 6
		female = yes
    }
}

2293.1.1 = {
	monarch = {
 		name = "Hadhuul"
		dynasty = "Ula-Narrarae"
		birth_date = 2254.1.1
		adm = 1
		dip = 5
		mil = 1
    }
	queen = {
 		name = "Yrru"
		dynasty = "Ula-Narrarae"
		birth_date = 2243.1.1
		adm = 5
		dip = 3
		mil = 0
		female = yes
    }
	heir = {
 		name = "Vuudunt"
		monarch_name = "Vuudunt I"
		dynasty = "Ula-Narrarae"
		birth_date = 2287.1.1
		death_date = 2363.1.1
		claim = 100
		adm = 2
		dip = 2
		mil = 6
    }
}

2363.1.1 = {
	monarch = {
 		name = "Vanhur"
		dynasty = "Ula-Miscarcand"
		birth_date = 2325.1.1
		adm = 0
		dip = 1
		mil = 3
    }
	queen = {
 		name = "Irreshyva"
		dynasty = "Ula-Miscarcand"
		birth_date = 2342.1.1
		adm = 4
		dip = 0
		mil = 2
		female = yes
    }
	heir = {
 		name = "Hosnuugaura"
		monarch_name = "Hosnuugaura I"
		dynasty = "Ula-Miscarcand"
		birth_date = 2357.1.1
		death_date = 2446.1.1
		claim = 100
		adm = 1
		dip = 5
		mil = 1
    }
}

2446.1.1 = {
	monarch = {
 		name = "Heystalero"
		dynasty = "Ula-Lildana"
		birth_date = 2404.1.1
		adm = 4
		dip = 0
		mil = 3
    }
}

