government = monarchy
government_rank = 1
mercantilism = 1
technology_group = northern_tg
religion = druidism
primary_culture = breton
capital = 6934

54.1.1 = {
	monarch = {
 		name = "Ajac"
		dynasty = "Falbert"
		birth_date = 24.1.1
		adm = 1
		dip = 3
		mil = 4
    }
}

139.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Hescot"
		monarch_name = "Hescot I"
		dynasty = "Velmont"
		birth_date = 124.1.1
		death_date = 142.1.1
		claim = 100
		adm = 2
		dip = 0
		mil = 2
    }
}

142.1.1 = {
	monarch = {
 		name = "Jarnot"
		dynasty = "Lerineaux"
		birth_date = 115.1.1
		adm = 1
		dip = 4
		mil = 2
    }
}

199.1.1 = {
	monarch = {
 		name = "Rallaume"
		dynasty = "Tyne"
		birth_date = 180.1.1
		adm = 6
		dip = 3
		mil = 6
    }
	queen = {
 		name = "Celarion"
		dynasty = "Tyne"
		birth_date = 154.1.1
		adm = 3
		dip = 1
		mil = 5
		female = yes
    }
}

293.1.1 = {
	monarch = {
 		name = "Arzhela"
		dynasty = "Detelle"
		birth_date = 249.1.1
		adm = 2
		dip = 1
		mil = 2
		female = yes
    }
}

357.1.1 = {
	monarch = {
 		name = "Sadun"
		dynasty = "Normar"
		birth_date = 319.1.1
		adm = 0
		dip = 0
		mil = 5
    }
}

413.1.1 = {
	monarch = {
 		name = "Lyna"
		dynasty = "Dugot"
		birth_date = 375.1.1
		adm = 5
		dip = 6
		mil = 2
		female = yes
    }
}

499.1.1 = {
	monarch = {
 		name = "Etiache"
		dynasty = "Sourt"
		birth_date = 459.1.1
		adm = 3
		dip = 5
		mil = 6
    }
	queen = {
 		name = "Neomi"
		dynasty = "Sourt"
		birth_date = 460.1.1
		adm = 0
		dip = 4
		mil = 5
		female = yes
    }
}

573.1.1 = {
	monarch = {
 		name = "Marjaques"
		dynasty = "Catreau"
		birth_date = 526.1.1
		adm = 5
		dip = 3
		mil = 1
    }
}

635.1.1 = {
	monarch = {
 		name = "Gan"
		dynasty = "Stental"
		birth_date = 598.1.1
		adm = 4
		dip = 2
		mil = 5
    }
}

684.1.1 = {
	monarch = {
 		name = "Beryn"
		dynasty = "Zylippe"
		birth_date = 656.1.1
		adm = 2
		dip = 1
		mil = 2
    }
}

783.1.1 = {
	monarch = {
 		name = "Tevenot"
		dynasty = "Miller"
		birth_date = 736.1.1
		adm = 4
		dip = 3
		mil = 3
    }
	queen = {
 		name = "Reinette"
		dynasty = "Miller"
		birth_date = 740.1.1
		adm = 4
		dip = 6
		mil = 4
		female = yes
    }
	heir = {
 		name = "Ferarilie"
		monarch_name = "Ferarilie I"
		dynasty = "Miller"
		birth_date = 769.1.1
		death_date = 841.1.1
		claim = 100
		adm = 1
		dip = 5
		mil = 3
    }
}

841.1.1 = {
	monarch = {
 		name = "Urvie"
		dynasty = "Zulin"
		birth_date = 800.1.1
		adm = 0
		dip = 1
		mil = 3
		female = yes
    }
}

924.1.1 = {
	monarch = {
 		name = "Maul"
		dynasty = "Laussac"
		birth_date = 874.1.1
		adm = 6
		dip = 0
		mil = 0
    }
	queen = {
 		name = "Angeline"
		dynasty = "Laussac"
		birth_date = 871.1.1
		adm = 2
		dip = 6
		mil = 6
		female = yes
    }
	heir = {
 		name = "Lestault"
		monarch_name = "Lestault I"
		dynasty = "Laussac"
		birth_date = 910.1.1
		death_date = 974.1.1
		claim = 100
		adm = 6
		dip = 4
		mil = 5
    }
}

974.1.1 = {
	monarch = {
 		name = "Brussec"
		dynasty = "Altien"
		birth_date = 925.1.1
		adm = 2
		dip = 6
		mil = 0
    }
}

1046.1.1 = {
	monarch = {
 		name = "Uaile"
		dynasty = "Yunlin"
		birth_date = 1028.1.1
		adm = 0
		dip = 5
		mil = 4
		female = yes
    }
	queen = {
 		name = "Robier"
		dynasty = "Yunlin"
		birth_date = 1000.1.1
		adm = 4
		dip = 3
		mil = 3
    }
	heir = {
 		name = "Sebastian"
		monarch_name = "Sebastian I"
		dynasty = "Yunlin"
		birth_date = 1046.1.1
		death_date = 1144.1.1
		claim = 100
		adm = 1
		dip = 2
		mil = 2
    }
}

1144.1.1 = {
	monarch = {
 		name = "Grania"
		dynasty = "Pierrane"
		birth_date = 1104.1.1
		adm = 0
		dip = 5
		mil = 2
		female = yes
    }
}

1181.1.1 = {
	monarch = {
 		name = "Cairine"
		dynasty = "Virien"
		birth_date = 1161.1.1
		adm = 6
		dip = 5
		mil = 6
		female = yes
    }
}

1269.1.1 = {
	monarch = {
 		name = "Gernaud"
		dynasty = "Tustin"
		birth_date = 1227.1.1
		adm = 4
		dip = 4
		mil = 2
    }
	queen = {
 		name = "Rianne"
		dynasty = "Tustin"
		birth_date = 1236.1.1
		adm = 1
		dip = 2
		mil = 1
		female = yes
    }
	heir = {
 		name = "Francois"
		monarch_name = "Francois I"
		dynasty = "Tustin"
		birth_date = 1259.1.1
		death_date = 1356.1.1
		claim = 100
		adm = 5
		dip = 1
		mil = 0
    }
}

1356.1.1 = {
	monarch = {
 		name = "Vanisande"
		dynasty = "Gautier"
		birth_date = 1315.1.1
		adm = 0
		dip = 2
		mil = 3
		female = yes
    }
}

1411.1.1 = {
	monarch = {
 		name = "Merthyval"
		dynasty = "Rissiel"
		birth_date = 1367.1.1
		adm = 6
		dip = 1
		mil = 6
    }
	queen = {
 		name = "Annelette"
		dynasty = "Rissiel"
		birth_date = 1389.1.1
		adm = 2
		dip = 0
		mil = 5
		female = yes
    }
	heir = {
 		name = "Loupel"
		monarch_name = "Loupel I"
		dynasty = "Rissiel"
		birth_date = 1409.1.1
		death_date = 1488.1.1
		claim = 100
		adm = 6
		dip = 5
		mil = 4
    }
}

1488.1.1 = {
	monarch = {
 		name = "Cesamund"
		dynasty = "Rangifer"
		birth_date = 1470.1.1
		adm = 2
		dip = 0
		mil = 0
    }
}

1577.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Marien"
		monarch_name = "Marien I"
		dynasty = "Malyne"
		birth_date = 1573.1.1
		death_date = 1591.1.1
		claim = 100
		adm = 3
		dip = 4
		mil = 5
		female = yes
    }
}

1591.1.1 = {
	monarch = {
 		name = "Melanie"
		dynasty = "Farnele"
		birth_date = 1563.1.1
		adm = 2
		dip = 0
		mil = 5
		female = yes
    }
}

1657.1.1 = {
	monarch = {
 		name = "Vardan"
		dynasty = "Dencent"
		birth_date = 1607.1.1
		adm = 0
		dip = 6
		mil = 2
    }
	queen = {
 		name = "Eubella"
		dynasty = "Dencent"
		birth_date = 1629.1.1
		adm = 4
		dip = 5
		mil = 1
		female = yes
    }
	heir = {
 		name = "Sybille"
		monarch_name = "Sybille I"
		dynasty = "Dencent"
		birth_date = 1647.1.1
		death_date = 1730.1.1
		claim = 100
		adm = 1
		dip = 4
		mil = 6
		female = yes
    }
}

1730.1.1 = {
	monarch = {
 		name = "Hester"
		dynasty = "Cergend"
		birth_date = 1683.1.1
		adm = 4
		dip = 5
		mil = 2
		female = yes
    }
}

1779.1.1 = {
	monarch = {
 		name = "Cimon"
		dynasty = "Manis"
		birth_date = 1736.1.1
		adm = 2
		dip = 4
		mil = 6
    }
}

1865.1.1 = {
	monarch = {
 		name = "Vimy"
		dynasty = "Arbogasque"
		birth_date = 1846.1.1
		adm = 0
		dip = 3
		mil = 2
		female = yes
    }
	queen = {
 		name = "Sebastian"
		dynasty = "Arbogasque"
		birth_date = 1828.1.1
		adm = 4
		dip = 2
		mil = 1
    }
	heir = {
 		name = "Sutler"
		monarch_name = "Sutler I"
		dynasty = "Arbogasque"
		birth_date = 1853.1.1
		death_date = 1940.1.1
		claim = 100
		adm = 1
		dip = 0
		mil = 0
    }
}

1940.1.1 = {
	monarch = {
 		name = "Giorges"
		dynasty = "Gevont"
		birth_date = 1896.1.1
		adm = 1
		dip = 4
		mil = 0
    }
	queen = {
 		name = "Fasile"
		dynasty = "Gevont"
		birth_date = 1915.1.1
		adm = 1
		dip = 0
		mil = 1
		female = yes
    }
}

1992.1.1 = {
	monarch = {
 		name = "Paldrin"
		dynasty = "Luric"
		birth_date = 1947.1.1
		adm = 3
		dip = 1
		mil = 3
    }
	queen = {
 		name = "Maryse"
		dynasty = "Luric"
		birth_date = 1966.1.1
		adm = 3
		dip = 5
		mil = 4
		female = yes
    }
	heir = {
 		name = "Ciara"
		monarch_name = "Ciara I"
		dynasty = "Luric"
		birth_date = 1982.1.1
		death_date = 2061.1.1
		claim = 100
		adm = 0
		dip = 3
		mil = 3
		female = yes
    }
}

2061.1.1 = {
	monarch = {
 		name = "Phaistyr"
		dynasty = "Merchad"
		birth_date = 2036.1.1
		adm = 6
		dip = 0
		mil = 3
    }
}

2155.1.1 = {
	monarch = {
 		name = "Jacques"
		dynasty = "Serpe"
		birth_date = 2125.1.1
		adm = 4
		dip = 6
		mil = 0
    }
}

2226.1.1 = {
	monarch = {
 		name = "Destaine"
		dynasty = "Masolaude"
		birth_date = 2183.1.1
		adm = 3
		dip = 5
		mil = 4
    }
	queen = {
 		name = "Marthine"
		dynasty = "Masolaude"
		birth_date = 2181.1.1
		adm = 0
		dip = 4
		mil = 3
		female = yes
    }
}

2289.1.1 = {
	monarch = {
 		name = "Kasynac"
		dynasty = "Boissart"
		birth_date = 2257.1.1
		adm = 5
		dip = 3
		mil = 6
    }
	queen = {
 		name = "Vivonne"
		dynasty = "Boissart"
		birth_date = 2245.1.1
		adm = 2
		dip = 2
		mil = 5
		female = yes
    }
}

2326.1.1 = {
	monarch = {
 		name = "Soran"
		dynasty = "Lydelle"
		birth_date = 2273.1.1
		adm = 0
		dip = 1
		mil = 2
    }
}

2383.1.1 = {
	monarch = {
 		name = "Loucel"
		dynasty = "Dolbanitte"
		birth_date = 2345.1.1
		adm = 5
		dip = 0
		mil = 5
    }
}

2450.1.1 = {
	monarch = {
 		name = "Franara"
		dynasty = "Gernand"
		birth_date = 2428.1.1
		adm = 3
		dip = 6
		mil = 2
		female = yes
    }
}

