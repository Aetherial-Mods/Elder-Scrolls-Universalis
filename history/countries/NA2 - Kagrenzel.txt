government = republic
government_rank = 3
mercantilism = 1
technology_group = dwemer_tg
religion = reason_and_logic_cult
primary_culture = dwemer
capital = 1270

54.1.1 = {
	monarch = {
 		name = "Tnarlac"
		dynasty = "Af'Alnodrunz"
		birth_date = 5.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

152.1.1 = {
	monarch = {
 		name = "Grabnanch"
		dynasty = "Az'Azsamchin"
		birth_date = 122.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

192.1.1 = {
	monarch = {
 		name = "Chzevragch"
		dynasty = "Af'Crafrysz"
		birth_date = 141.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

237.1.1 = {
	monarch = {
 		name = "Yhnazdir"
		dynasty = "Aq'Snebchasz"
		birth_date = 195.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

280.1.1 = {
	monarch = {
 		name = "Czadlin"
		dynasty = "Aq'Drertes"
		birth_date = 247.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

315.1.1 = {
	monarch = {
 		name = "Rhzorhunch"
		dynasty = "Az'Asratchzan"
		birth_date = 290.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

394.1.1 = {
	monarch = {
 		name = "Krevzyrn"
		dynasty = "Af'Rafk"
		birth_date = 355.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

457.1.1 = {
	monarch = {
 		name = "Yhnazdir"
		dynasty = "Af'Sthord"
		birth_date = 428.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

541.1.1 = {
	monarch = {
 		name = "Jragrenz"
		dynasty = "Az'Doudrys"
		birth_date = 508.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

631.1.1 = {
	monarch = {
 		name = "Klazgar"
		dynasty = "Aq'Bluvzal"
		birth_date = 586.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

700.1.1 = {
	monarch = {
 		name = "Chrudras"
		dynasty = "Av'Jlarerhunch"
		birth_date = 647.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

760.1.1 = {
	monarch = {
 		name = "Achyggo"
		dynasty = "Av'Mhunac"
		birth_date = 737.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

802.1.1 = {
	monarch = {
 		name = "Jragrenz"
		dynasty = "Aq'Izvutchatz"
		birth_date = 782.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

882.1.1 = {
	monarch = {
 		name = "Klazgar"
		dynasty = "Aq'Chzefrach"
		birth_date = 850.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

951.1.1 = {
	monarch = {
 		name = "Nromgunch"
		dynasty = "Av'Jholzarf"
		birth_date = 900.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1011.1.1 = {
	monarch = {
 		name = "Achyggo"
		dynasty = "Az'Nohnch"
		birth_date = 971.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1065.1.1 = {
	monarch = {
 		name = "Nchuzalf"
		dynasty = "Av'Bzraban"
		birth_date = 1023.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1100.1.1 = {
	monarch = {
 		name = "Ychohnch"
		dynasty = "Az'Doudrys"
		birth_date = 1074.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1147.1.1 = {
	monarch = {
 		name = "Ychogarn"
		dynasty = "Az'Krincha"
		birth_date = 1106.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1218.1.1 = {
	monarch = {
 		name = "Jhouvin"
		dynasty = "Az'Ksrefurn"
		birth_date = 1167.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1316.1.1 = {
	monarch = {
 		name = "Nchuzalf"
		dynasty = "Af'Nchynac"
		birth_date = 1278.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1402.1.1 = {
	monarch = {
 		name = "Ychohnch"
		dynasty = "Aq'Rlorlis"
		birth_date = 1371.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1487.1.1 = {
	monarch = {
 		name = "Klalzrak"
		dynasty = "Az'Aknanwess"
		birth_date = 1448.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1567.1.1 = {
	monarch = {
 		name = "Djubchasz"
		dynasty = "Af'Chzevragch"
		birth_date = 1545.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1631.1.1 = {
	monarch = {
 		name = "Jlarerhunch"
		dynasty = "Aq'Banrynn"
		birth_date = 1603.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1704.1.1 = {
	monarch = {
 		name = "Chragrenz"
		dynasty = "Av'Brehron"
		birth_date = 1653.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1740.1.1 = {
	monarch = {
 		name = "Irhadac"
		dynasty = "Az'Ryumorn"
		birth_date = 1692.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1807.1.1 = {
	monarch = {
 		name = "Nrotchzan"
		dynasty = "Az'Jravarn"
		birth_date = 1769.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1869.1.1 = {
	monarch = {
 		name = "Shtrozril"
		dynasty = "Az'Asratchzan"
		birth_date = 1845.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1963.1.1 = {
	monarch = {
 		name = "Djuhnch"
		dynasty = "Az'Szoglynsh"
		birth_date = 1942.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

2028.1.1 = {
	monarch = {
 		name = "Grubond"
		dynasty = "Az'Damtrin"
		birth_date = 1990.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2082.1.1 = {
	monarch = {
 		name = "Grigarn"
		dynasty = "Aq'Ksredlin"
		birth_date = 2051.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

2149.1.1 = {
	monarch = {
 		name = "Nchynac"
		dynasty = "Av'Miban"
		birth_date = 2097.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2201.1.1 = {
	monarch = {
 		name = "Kolzarf"
		dynasty = "Af'Izvulzarf"
		birth_date = 2163.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2239.1.1 = {
	monarch = {
 		name = "Grubond"
		dynasty = "Aq'Gonwess"
		birth_date = 2208.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2294.1.1 = {
	monarch = {
 		name = "Bhazchyn"
		dynasty = "Az'Krevzyrn"
		birth_date = 2251.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2366.1.1 = {
	monarch = {
 		name = "Yzranrida"
		dynasty = "Av'Czavzyrn"
		birth_date = 2333.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

2456.1.1 = {
	monarch = {
 		name = "Asradlin"
		dynasty = "Av'Ishevnorz"
		birth_date = 2434.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

