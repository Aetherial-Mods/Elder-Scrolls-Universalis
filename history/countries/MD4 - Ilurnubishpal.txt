government = tribal
government_rank = 1
mercantilism = 1
technology_group = exiled_tg
religion = chimer_pantheon
primary_culture = ashlander_chimer
capital = 3426

54.1.1 = {
	monarch = {
 		name = "Yeherradad"
		dynasty = "Niladon"
		birth_date = 32.1.1
		adm = 0
		dip = 5
		mil = 3
    }
}

140.1.1 = {
	monarch = {
 		name = "Rawia"
		dynasty = "Assalatammis"
		birth_date = 97.1.1
		adm = 5
		dip = 4
		mil = 6
		female = yes
    }
}

208.1.1 = {
	monarch = {
 		name = "Kushishi"
		dynasty = "Odimabesser"
		birth_date = 179.1.1
		adm = 4
		dip = 4
		mil = 3
		female = yes
    }
}

279.1.1 = {
	monarch = {
 		name = "Ashibaal"
		dynasty = "Mirpal"
		birth_date = 240.1.1
		adm = 5
		dip = 5
		mil = 5
    }
}

378.1.1 = {
	monarch = {
 		name = "Abishpulu"
		dynasty = "Ilabael"
		birth_date = 326.1.1
		adm = 4
		dip = 4
		mil = 1
    }
}

444.1.1 = {
	monarch = {
 		name = "Sonummu"
		dynasty = "Kutebani"
		birth_date = 407.1.1
		adm = 6
		dip = 5
		mil = 4
		female = yes
    }
}

516.1.1 = {
	monarch = {
 		name = "Musa"
		dynasty = "Dagoth"
		birth_date = 488.1.1
		adm = 5
		dip = 5
		mil = 0
		female = yes
    }
}

585.1.1 = {
	monarch = {
 		name = "Hainab"
		dynasty = "Assarnibani"
		birth_date = 558.1.1
		adm = 3
		dip = 4
		mil = 4
    }
}

624.1.1 = {
	monarch = {
 		name = "Addut-Lamanu"
		dynasty = "Benamamat"
		birth_date = 590.1.1
		adm = 5
		dip = 5
		mil = 5
		female = yes
    }
}

667.1.1 = {
	monarch = {
 		name = "Tis"
		dynasty = "Siddurnanit"
		birth_date = 622.1.1
		adm = 3
		dip = 4
		mil = 2
    }
}

761.1.1 = {
	monarch = {
 		name = "Adusamsi"
		dynasty = "Assarnibani"
		birth_date = 709.1.1
		adm = 1
		dip = 3
		mil = 6
		female = yes
    }
}

815.1.1 = {
	monarch = {
 		name = "Tissamsi"
		dynasty = "Zeba-Adad"
		birth_date = 776.1.1
		adm = 0
		dip = 3
		mil = 2
		female = yes
    }
}

853.1.1 = {
	monarch = {
 		name = "Nibani"
		dynasty = "Ahanidiran"
		birth_date = 820.1.1
		adm = 5
		dip = 2
		mil = 6
		female = yes
    }
}

903.1.1 = {
	monarch = {
 		name = "Hannat"
		dynasty = "Yessur-Disadon"
		birth_date = 884.1.1
		adm = 3
		dip = 1
		mil = 3
    }
}

949.1.1 = {
	monarch = {
 		name = "Adusamsi"
		dynasty = "Maesa"
		birth_date = 913.1.1
		adm = 1
		dip = 0
		mil = 6
		female = yes
    }
}

999.1.1 = {
	monarch = {
 		name = "Tubilalk"
		dynasty = "Ashun-Idantus"
		birth_date = 954.1.1
		adm = 0
		dip = 0
		mil = 3
    }
}

1040.1.1 = {
	monarch = {
 		name = "Odaishah"
		dynasty = "Asserrumusa"
		birth_date = 1009.1.1
		adm = 1
		dip = 1
		mil = 5
    }
}

1096.1.1 = {
	monarch = {
 		name = "Hinummu"
		dynasty = "Hairshumusa"
		birth_date = 1072.1.1
		adm = 0
		dip = 0
		mil = 1
		female = yes
    }
}

1193.1.1 = {
	monarch = {
 		name = "Patus"
		dynasty = "Adidshina"
		birth_date = 1164.1.1
		adm = 5
		dip = 6
		mil = 5
    }
}

1233.1.1 = {
	monarch = {
 		name = "Hentus"
		dynasty = "Assattadaishah"
		birth_date = 1196.1.1
		adm = 3
		dip = 5
		mil = 1
    }
}

1288.1.1 = {
	monarch = {
 		name = "Addarnat"
		dynasty = "Assarrapanat"
		birth_date = 1260.1.1
		adm = 1
		dip = 5
		mil = 5
    }
}

1367.1.1 = {
	monarch = {
 		name = "Ulath-Pal"
		dynasty = "Assonirishpal"
		birth_date = 1340.1.1
		adm = 2
		dip = 2
		mil = 3
    }
}

1422.1.1 = {
	monarch = {
 		name = "Yenabi"
		dynasty = "Addinibi"
		birth_date = 1395.1.1
		adm = 0
		dip = 1
		mil = 0
		female = yes
    }
}

1475.1.1 = {
	monarch = {
 		name = "Shanat"
		dynasty = "Indoril"
		birth_date = 1441.1.1
		adm = 1
		dip = 5
		mil = 5
    }
}

1566.1.1 = {
	monarch = {
 		name = "Tis"
		dynasty = "Shilansour"
		birth_date = 1528.1.1
		adm = 6
		dip = 4
		mil = 1
    }
}

1617.1.1 = {
	monarch = {
 		name = "Ninirrasour"
		dynasty = "Shalarnetus"
		birth_date = 1567.1.1
		adm = 5
		dip = 3
		mil = 5
    }
}

1682.1.1 = {
	monarch = {
 		name = "Hainab"
		dynasty = "Sehabani"
		birth_date = 1643.1.1
		adm = 3
		dip = 3
		mil = 2
    }
}

1779.1.1 = {
	monarch = {
 		name = "Abassel"
		dynasty = "Tibashipal"
		birth_date = 1761.1.1
		adm = 1
		dip = 2
		mil = 5
    }
}

1827.1.1 = {
	monarch = {
 		name = "Tinti"
		dynasty = "Rapli"
		birth_date = 1778.1.1
		adm = 6
		dip = 1
		mil = 2
    }
}

1865.1.1 = {
	monarch = {
 		name = "Musa"
		dynasty = "Atinsabia"
		birth_date = 1812.1.1
		adm = 5
		dip = 0
		mil = 5
		female = yes
    }
}

1906.1.1 = {
	monarch = {
 		name = "Hannabi"
		dynasty = "Ududnabia"
		birth_date = 1873.1.1
		adm = 0
		dip = 1
		mil = 0
		female = yes
    }
}

2000.1.1 = {
	monarch = {
 		name = "Abassel"
		dynasty = "Assardarainat"
		birth_date = 1981.1.1
		adm = 5
		dip = 1
		mil = 4
    }
}

2056.1.1 = {
	monarch = {
 		name = "Hannat"
		dynasty = "Saladnius"
		birth_date = 2028.1.1
		adm = 3
		dip = 0
		mil = 0
    }
}

2140.1.1 = {
	monarch = {
 		name = "Adairan"
		dynasty = "Ashishpalirdan"
		birth_date = 2111.1.1
		adm = 1
		dip = 6
		mil = 4
    }
}

2210.1.1 = {
	monarch = {
 		name = "Tubilalk"
		dynasty = "Assullinbanud"
		birth_date = 2174.1.1
		adm = 0
		dip = 5
		mil = 1
    }
}

2281.1.1 = {
	monarch = {
 		name = "Nund"
		dynasty = "Benamamat"
		birth_date = 2240.1.1
		adm = 5
		dip = 4
		mil = 4
    }
}

2329.1.1 = {
	monarch = {
 		name = "Hansi"
		dynasty = "Ahalkalun"
		birth_date = 2287.1.1
		adm = 3
		dip = 4
		mil = 1
		female = yes
    }
}

2387.1.1 = {
	monarch = {
 		name = "Adusamsi"
		dynasty = "Eraishah"
		birth_date = 2350.1.1
		adm = 1
		dip = 3
		mil = 5
		female = yes
    }
}

2459.1.1 = {
	monarch = {
 		name = "Truan"
		dynasty = "Hansar"
		birth_date = 2433.1.1
		adm = 3
		dip = 4
		mil = 6
    }
}

2497.1.1 = {
	monarch = {
 		name = "Nammu"
		dynasty = "Redoran"
		birth_date = 2451.1.1
		adm = 1
		dip = 3
		mil = 3
		female = yes
    }
}

