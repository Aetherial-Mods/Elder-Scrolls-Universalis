government = tribal
government_rank = 1
mercantilism = 1
technology_group = orsimer_tg
religion = orcish_pantheon
primary_culture = wood_orsimer
capital = 4996

54.1.1 = {
	monarch = {
 		name = "Vulmon"
		dynasty = "gro-Gruloq"
		birth_date = 14.1.1
		adm = 4
		dip = 5
		mil = 4
    }
}

140.1.1 = {
	monarch = {
 		name = "Osgrikh"
		dynasty = "gro-Golbag"
		birth_date = 117.1.1
		adm = 2
		dip = 4
		mil = 1
    }
}

239.1.1 = {
	monarch = {
 		name = "Yamukuz"
		dynasty = "gro-Szugburg"
		birth_date = 220.1.1
		adm = 1
		dip = 4
		mil = 4
    }
}

300.1.1 = {
	monarch = {
 		name = "Ramash"
		dynasty = "gro-Thrag"
		birth_date = 252.1.1
		adm = 6
		dip = 3
		mil = 1
    }
}

363.1.1 = {
	monarch = {
 		name = "Lazgel"
		dynasty = "gro-Snalikh"
		birth_date = 327.1.1
		adm = 4
		dip = 2
		mil = 5
    }
}

444.1.1 = {
	monarch = {
 		name = "Gargak"
		dynasty = "gro-Gohorg"
		birth_date = 397.1.1
		adm = 2
		dip = 1
		mil = 1
    }
}

519.1.1 = {
	monarch = {
 		name = "Yamarz"
		dynasty = "gro-Azogu"
		birth_date = 497.1.1
		adm = 1
		dip = 0
		mil = 5
    }
}

613.1.1 = {
	monarch = {
 		name = "Ragbur"
		dynasty = "gro-Grodagur"
		birth_date = 581.1.1
		adm = 6
		dip = 0
		mil = 2
    }
}

660.1.1 = {
	monarch = {
 		name = "Lashbag"
		dynasty = "gro-Ghamron"
		birth_date = 608.1.1
		adm = 1
		dip = 1
		mil = 3
    }
}

739.1.1 = {
	monarch = {
 		name = "Ashgara"
		dynasty = "gro-Glogob"
		birth_date = 702.1.1
		adm = 6
		dip = 0
		mil = 0
		female = yes
    }
}

832.1.1 = {
	monarch = {
 		name = "Guth"
		dynasty = "gro-Ogrul"
		birth_date = 785.1.1
		adm = 4
		dip = 6
		mil = 3
		female = yes
    }
}

876.1.1 = {
	monarch = {
 		name = "Azhnolga"
		dynasty = "gro-Gralturg"
		birth_date = 851.1.1
		adm = 2
		dip = 6
		mil = 0
		female = yes
    }
}

925.1.1 = {
	monarch = {
 		name = "Yagrigbesh"
		dynasty = "gro-Azgarub"
		birth_date = 887.1.1
		adm = 1
		dip = 5
		mil = 4
		female = yes
    }
}

1003.1.1 = {
	monarch = {
 		name = "Roguzog"
		dynasty = "gro-Ushat"
		birth_date = 956.1.1
		adm = 1
		dip = 2
		mil = 2
    }
}

1067.1.1 = {
	monarch = {
 		name = "Stugbulukh"
		dynasty = "gro-Mothgam"
		birth_date = 1039.1.1
		adm = 0
		dip = 1
		mil = 5
    }
}

1132.1.1 = {
	monarch = {
 		name = "Lorogdu"
		dynasty = "gro-Gloth"
		birth_date = 1095.1.1
		adm = 5
		dip = 0
		mil = 2
		female = yes
    }
}

1169.1.1 = {
	monarch = {
 		name = "Graguz"
		dynasty = "gro-Urbul"
		birth_date = 1124.1.1
		adm = 3
		dip = 6
		mil = 6
    }
}

1242.1.1 = {
	monarch = {
 		name = "Bogodug"
		dynasty = "gro-Dugtosh"
		birth_date = 1219.1.1
		adm = 1
		dip = 6
		mil = 2
    }
}

1304.1.1 = {
	monarch = {
 		name = "Shugzur"
		dynasty = "gro-Ogumalg"
		birth_date = 1266.1.1
		adm = 0
		dip = 5
		mil = 6
		female = yes
    }
}

1346.1.1 = {
	monarch = {
 		name = "Mulur"
		dynasty = "gro-Ramosh"
		birth_date = 1310.1.1
		adm = 2
		dip = 6
		mil = 0
    }
}

1394.1.1 = {
	monarch = {
 		name = "Gothurg"
		dynasty = "gro-Yashnarz"
		birth_date = 1353.1.1
		adm = 0
		dip = 5
		mil = 4
    }
}

1457.1.1 = {
	monarch = {
 		name = "Muzdrulz"
		dynasty = "gro-Shum"
		birth_date = 1429.1.1
		adm = 5
		dip = 5
		mil = 1
    }
}

1542.1.1 = {
	monarch = {
 		name = "Grogmar"
		dynasty = "gro-Mol"
		birth_date = 1519.1.1
		adm = 3
		dip = 4
		mil = 4
    }
}

1596.1.1 = {
	monarch = {
 		name = "Borth"
		dynasty = "gro-Borkul"
		birth_date = 1551.1.1
		adm = 1
		dip = 3
		mil = 1
    }
}

1688.1.1 = {
	monarch = {
 		name = "Snarga"
		dynasty = "gro-Thagbush"
		birth_date = 1643.1.1
		adm = 0
		dip = 2
		mil = 5
		female = yes
    }
}

1740.1.1 = {
	monarch = {
 		name = "Murzol"
		dynasty = "gro-Oglub"
		birth_date = 1709.1.1
		adm = 5
		dip = 1
		mil = 1
    }
}

1782.1.1 = {
	monarch = {
 		name = "Grodagur"
		dynasty = "gro-Dugakh"
		birth_date = 1748.1.1
		adm = 3
		dip = 1
		mil = 5
    }
}

1834.1.1 = {
	monarch = {
 		name = "Ulumpha"
		dynasty = "gro-Shargunh"
		birth_date = 1816.1.1
		adm = 5
		dip = 2
		mil = 0
		female = yes
    }
}

1924.1.1 = {
	monarch = {
 		name = "Thagbush"
		dynasty = "gro-Bolg"
		birth_date = 1884.1.1
		adm = 3
		dip = 1
		mil = 3
    }
}

1974.1.1 = {
	monarch = {
 		name = "Brugdush"
		dynasty = "gro-Brugagikh"
		birth_date = 1946.1.1
		adm = 2
		dip = 0
		mil = 0
    }
}

2051.1.1 = {
	monarch = {
 		name = "Thragosh"
		dynasty = "gro-Ushahag"
		birth_date = 2026.1.1
		adm = 0
		dip = 6
		mil = 3
    }
}

2107.1.1 = {
	monarch = {
 		name = "Nahzush"
		dynasty = "gro-Undugar"
		birth_date = 2064.1.1
		adm = 5
		dip = 6
		mil = 0
    }
}

2150.1.1 = {
	monarch = {
 		name = "Grulzul"
		dynasty = "gro-Rozag"
		birth_date = 2131.1.1
		adm = 3
		dip = 5
		mil = 4
    }
}

2227.1.1 = {
	monarch = {
 		name = "Brokk"
		dynasty = "gro-Gladba"
		birth_date = 2186.1.1
		adm = 2
		dip = 4
		mil = 0
    }
}

2276.1.1 = {
	monarch = {
 		name = "Thorzhul"
		dynasty = "gro-Mulur"
		birth_date = 2238.1.1
		adm = 0
		dip = 3
		mil = 4
    }
}

2350.1.1 = {
	monarch = {
 		name = "Maugruhl"
		dynasty = "gro-Azrath"
		birth_date = 2315.1.1
		adm = 2
		dip = 5
		mil = 6
		female = yes
    }
}

2429.1.1 = {
	monarch = {
 		name = "Gruldum"
		dynasty = "gro-Graman"
		birth_date = 2382.1.1
		adm = 0
		dip = 4
		mil = 2
    }
}

