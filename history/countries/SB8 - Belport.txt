government = republic
government_rank = 1
mercantilism = 1
technology_group = elven_tg
religion = altmeri_pantheon
primary_culture = altmer
capital = 256
# = 4716

54.1.1 = {
	monarch = {
 		name = "Freyvene"
		dynasty = "Ael-Lillandril"
		birth_date = 9.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

100.1.1 = {
	monarch = {
 		name = "Eanurlemar"
		dynasty = "Bel-Taanye"
		birth_date = 71.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

157.1.1 = {
	monarch = {
 		name = "Urien"
		dynasty = "Bel-Sondsara"
		birth_date = 126.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

203.1.1 = {
	monarch = {
 		name = "Palomir"
		dynasty = "Bel-Silalaure"
		birth_date = 184.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

294.1.1 = {
	monarch = {
 		name = "Valtuumetil"
		dynasty = "Bel-Sondnwe"
		birth_date = 253.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

384.1.1 = {
	monarch = {
 		name = "Piromir"
		dynasty = "Ael-Muulinde"
		birth_date = 341.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

472.1.1 = {
	monarch = {
 		name = "Lelorion"
		dynasty = "Ael-Karnstern"
		birth_date = 424.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

535.1.1 = {
	monarch = {
 		name = "Eldecil"
		dynasty = "Ael-Muulinde"
		birth_date = 515.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

621.1.1 = {
	monarch = {
 		name = "Valenn"
		dynasty = "Bel-Sondsara"
		birth_date = 572.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

668.1.1 = {
	monarch = {
 		name = "Peninya"
		dynasty = "Wel-Cloudrest"
		birth_date = 633.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

738.1.1 = {
	monarch = {
 		name = "Laryantil"
		dynasty = "Bel-Valia"
		birth_date = 718.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

808.1.1 = {
	monarch = {
 		name = "Elanar"
		dynasty = "Ael-Kingdom"
		birth_date = 788.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

906.1.1 = {
	monarch = {
 		name = "Hiranesse"
		dynasty = "Wel-Endrae"
		birth_date = 884.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

946.1.1 = {
	monarch = {
 		name = "Elpion"
		dynasty = "		"
		birth_date = 909.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1023.1.1 = {
	monarch = {
 		name = "Varfandur"
		dynasty = "Bel-Tuiion"
		birth_date = 983.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1085.1.1 = {
	monarch = {
 		name = "Panersewen"
		dynasty = "Bel-Sunhold"
		birth_date = 1049.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1165.1.1 = {
	monarch = {
 		name = "Leythen"
		dynasty = "Ael-Nalcrand"
		birth_date = 1115.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1247.1.1 = {
	monarch = {
 		name = "Elodinar"
		dynasty = "Bel-Sondsara"
		birth_date = 1196.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1304.1.1 = {
	monarch = {
 		name = "Varustante"
		dynasty = "Wel-Arannnarre"
		birth_date = 1252.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1379.1.1 = {
	monarch = {
 		name = "Quaranir"
		dynasty = "Wel-Aryria"
		birth_date = 1359.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1419.1.1 = {
	monarch = {
 		name = "Vorundil"
		dynasty = "Bel-Spelluseus"
		birth_date = 1393.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1488.1.1 = {
	monarch = {
 		name = "Rolandor"
		dynasty = "Bel-Valnoore"
		birth_date = 1464.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1540.1.1 = {
	monarch = {
 		name = "Inarala"
		dynasty = "Wel-Celrith"
		birth_date = 1509.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1639.1.1 = {
	monarch = {
 		name = "Enulyanar"
		dynasty = "Wel-Aelsoniane"
		birth_date = 1604.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1675.1.1 = {
	monarch = {
 		name = "Virumariel"
		dynasty = "Ael-Firsthold"
		birth_date = 1629.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1721.1.1 = {
	monarch = {
 		name = "Rivenar"
		dynasty = "Wel-Faeire"
		birth_date = 1700.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1780.1.1 = {
	monarch = {
 		name = "Lisagnor"
		dynasty = "Bel-Siriginia"
		birth_date = 1736.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1847.1.1 = {
	monarch = {
 		name = "Cirterisse"
		dynasty = "Bel-Tanena"
		birth_date = 1824.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1925.1.1 = {
	monarch = {
 		name = "Cirdur"
		dynasty = "Ael-Lorusse"
		birth_date = 1906.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1972.1.1 = {
	monarch = {
 		name = "Timion"
		dynasty = "Bel-Valnoore"
		birth_date = 1945.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2057.1.1 = {
	monarch = {
 		name = "Nulurin"
		dynasty = "Ael-Nirae"
		birth_date = 2024.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2135.1.1 = {
	monarch = {
 		name = "Hirtel"
		dynasty = "Bel-Terna"
		birth_date = 2092.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2187.1.1 = {
	monarch = {
 		name = "Ciral"
		dynasty = "Bel-Valaena"
		birth_date = 2164.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2230.1.1 = {
	monarch = {
 		name = "Indilgalion"
		dynasty = "Bel-Syldarim"
		birth_date = 2194.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2267.1.1 = {
	monarch = {
 		name = "Coristir"
		dynasty = "Bel-Soneya"
		birth_date = 2245.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2344.1.1 = {
	monarch = {
 		name = "Turqualie"
		dynasty = "Ael-Relte"
		birth_date = 2318.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

2404.1.1 = {
	monarch = {
 		name = "Niranye"
		dynasty = "Bel-Scutters"
		birth_date = 2383.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

2445.1.1 = {
	monarch = {
 		name = "Fenewen"
		dynasty = "Wel-Elsinthaer"
		birth_date = 2414.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

