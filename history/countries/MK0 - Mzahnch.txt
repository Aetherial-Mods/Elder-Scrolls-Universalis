government = republic
government_rank = 3
mercantilism = 1
technology_group = dwemer_tg
religion = reason_and_logic_cult
primary_culture = dwemer
capital = 1041

54.1.1 = {
	monarch = {
 		name = "Tzenruz"
		dynasty = "Aq'Mlirtes"
		birth_date = 26.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

117.1.1 = {
	monarch = {
 		name = "Cfradrys"
		dynasty = "Af'Izvumratz"
		birth_date = 73.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

160.1.1 = {
	monarch = {
 		name = "Ksredlin"
		dynasty = "Aq'Mravlara"
		birth_date = 128.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

229.1.1 = {
	monarch = {
 		name = "Tnadrak"
		dynasty = "Av'Yhnazdir"
		birth_date = 207.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

284.1.1 = {
	monarch = {
 		name = "Tzenruz"
		dynasty = "Af'Jrudit"
		birth_date = 261.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

373.1.1 = {
	monarch = {
 		name = "Churd"
		dynasty = "Af'Asrahuanch"
		birth_date = 355.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

431.1.1 = {
	monarch = {
 		name = "Chzetvar"
		dynasty = "Av'Ghaznak"
		birth_date = 393.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

484.1.1 = {
	monarch = {
 		name = "Ilzeglan"
		dynasty = "Aq'Cheglas"
		birth_date = 455.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

563.1.1 = {
	monarch = {
 		name = "Jlethurzch"
		dynasty = "Aq'Bluhzis"
		birth_date = 520.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

622.1.1 = {
	monarch = {
 		name = "Sthord"
		dynasty = "Af'Chzetvar"
		birth_date = 595.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

696.1.1 = {
	monarch = {
 		name = "Cfranhatch"
		dynasty = "Az'Chzebchasz"
		birth_date = 660.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

776.1.1 = {
	monarch = {
 		name = "Nchyhrek"
		dynasty = "Af'Choalchanf"
		birth_date = 736.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

816.1.1 = {
	monarch = {
 		name = "Asradzach"
		dynasty = "Av'Ihledchu"
		birth_date = 787.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

882.1.1 = {
	monarch = {
 		name = "Rlovrin"
		dynasty = "Az'Szoglynsh"
		birth_date = 839.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

975.1.1 = {
	monarch = {
 		name = "Jlarenzgar"
		dynasty = "Az'Ylrefwinn"
		birth_date = 955.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1017.1.1 = {
	monarch = {
 		name = "Ralarn"
		dynasty = "Af'Mhanrazg"
		birth_date = 966.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1088.1.1 = {
	monarch = {
 		name = "Bzraban"
		dynasty = "Aq'Chzefrach"
		birth_date = 1063.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1144.1.1 = {
	monarch = {
 		name = "Tarlatz"
		dynasty = "Aq'Aravlen"
		birth_date = 1106.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1197.1.1 = {
	monarch = {
 		name = "Cheglas"
		dynasty = "Aq'Ksredlin"
		birth_date = 1150.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1275.1.1 = {
	monarch = {
 		name = "Mebchasz"
		dynasty = "Af'Szogarn"
		birth_date = 1224.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1372.1.1 = {
	monarch = {
 		name = "Chiuvnak"
		dynasty = "Af'Rhzorhunch"
		birth_date = 1332.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1418.1.1 = {
	monarch = {
 		name = "Choahretz"
		dynasty = "Az'Jravarn"
		birth_date = 1380.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1485.1.1 = {
	monarch = {
 		name = "Chiuhld"
		dynasty = "Az'Gzovnorz"
		birth_date = 1437.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1573.1.1 = {
	monarch = {
 		name = "Mebchasz"
		dynasty = "Av'Chrudras"
		birth_date = 1524.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1645.1.1 = {
	monarch = {
 		name = "Nohnch"
		dynasty = "Az'Jribwyr"
		birth_date = 1616.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1726.1.1 = {
	monarch = {
 		name = "Choahretz"
		dynasty = "Af'Ghafuan"
		birth_date = 1690.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1819.1.1 = {
	monarch = {
 		name = "Dark"
		dynasty = "Av'Tromgunch"
		birth_date = 1788.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1891.1.1 = {
	monarch = {
 		name = "Izvumratz"
		dynasty = "Av'Tromgunch"
		birth_date = 1841.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1939.1.1 = {
	monarch = {
 		name = "Szozchyn"
		dynasty = "Av'Sthomin"
		birth_date = 1897.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1989.1.1 = {
	monarch = {
 		name = "Gzozchyn"
		dynasty = "Aq'Bluvzal"
		birth_date = 1963.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2058.1.1 = {
	monarch = {
 		name = "Yabaln"
		dynasty = "Aq'Rhothurzch"
		birth_date = 2024.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

2142.1.1 = {
	monarch = {
 		name = "Goundam"
		dynasty = "Aq'Csilirda"
		birth_date = 2124.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2239.1.1 = {
	monarch = {
 		name = "Czavzyrn"
		dynasty = "Av'Ynzarlatz"
		birth_date = 2198.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2278.1.1 = {
	monarch = {
 		name = "Dhamuard"
		dynasty = "Av'Achygrac"
		birth_date = 2233.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2321.1.1 = {
	monarch = {
 		name = "Tahron"
		dynasty = "Aq'Kamzlin"
		birth_date = 2278.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2374.1.1 = {
	monarch = {
 		name = "Bzrazgar"
		dynasty = "Az'Cheftris"
		birth_date = 2339.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2463.1.1 = {
	monarch = {
 		name = "Alnomratz"
		dynasty = "Aq'Tchazchyn"
		birth_date = 2417.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

