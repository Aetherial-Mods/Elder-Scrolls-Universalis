government = native
government_rank = 1
mercantilism = 1
technology_group = marshan_tg
religion = hist
primary_culture = naga
capital = 6522

54.1.1 = {
	monarch = {
 		name = "Peek-Tul"
		dynasty = "Theerdaresh"
		birth_date = 18.1.1
		adm = 2
		dip = 0
		mil = 2
    }
}

135.1.1 = {
	monarch = {
 		name = "Wunalz"
		dynasty = "Nefeseus"
		birth_date = 110.1.1
		adm = 1
		dip = 6
		mil = 6
    }
}

232.1.1 = {
	monarch = {
 		name = "Muz-Medul"
		dynasty = "Mahekus"
		birth_date = 180.1.1
		adm = 1
		dip = 3
		mil = 4
		female = yes
    }
}

325.1.1 = {
	monarch = {
 		name = "Lunsu"
		dynasty = "Cascalees"
		birth_date = 307.1.1
		adm = 4
		dip = 5
		mil = 6
		female = yes
    }
}

418.1.1 = {
	monarch = {
 		name = "Deed-Mema"
		dynasty = "Teegeixth"
		birth_date = 381.1.1
		adm = 2
		dip = 4
		mil = 3
    }
}

489.1.1 = {
	monarch = {
 		name = "Wuleen-Weska"
		dynasty = "Theotius"
		birth_date = 440.1.1
		adm = 1
		dip = 3
		mil = 6
    }
}

570.1.1 = {
	monarch = {
 		name = "Pimaxi-Taeed"
		dynasty = "Caylus"
		birth_date = 542.1.1
		adm = 6
		dip = 2
		mil = 3
    }
}

648.1.1 = {
	monarch = {
 		name = "Keenem"
		dynasty = "Nefeseus"
		birth_date = 623.1.1
		adm = 1
		dip = 3
		mil = 4
    }
}

730.1.1 = {
	monarch = {
 		name = "Deechee-Noo"
		dynasty = "Derkeeja"
		birth_date = 684.1.1
		adm = 6
		dip = 3
		mil = 1
		female = yes
    }
}

778.1.1 = {
	monarch = {
 		name = "Markka"
		dynasty = "Mahekus"
		birth_date = 730.1.1
		adm = 4
		dip = 2
		mil = 5
		female = yes
    }
}

843.1.1 = {
	monarch = {
 		name = "Deerlus"
		dynasty = "Mahekus"
		birth_date = 801.1.1
		adm = 3
		dip = 1
		mil = 1
    }
}

934.1.1 = {
	monarch = {
 		name = "Xozuka"
		dynasty = "Menes"
		birth_date = 892.1.1
		adm = 1
		dip = 0
		mil = 5
    }
}

1031.1.1 = {
	monarch = {
 		name = "Ruxultav"
		dynasty = "Androteus"
		birth_date = 1009.1.1
		adm = 6
		dip = 6
		mil = 2
		female = yes
    }
}

1106.1.1 = {
	monarch = {
 		name = "Makeusta"
		dynasty = "Meerhaj"
		birth_date = 1059.1.1
		adm = 4
		dip = 6
		mil = 5
		female = yes
    }
}

1198.1.1 = {
	monarch = {
 		name = "Deekum"
		dynasty = "Caligeepa"
		birth_date = 1152.1.1
		adm = 2
		dip = 5
		mil = 2
    }
}

1281.1.1 = {
	monarch = {
 		name = "Xil-Zish"
		dynasty = "Theermarush"
		birth_date = 1257.1.1
		adm = 4
		dip = 6
		mil = 4
		female = yes
    }
}

1371.1.1 = {
	monarch = {
 		name = "Radithax"
		dynasty = "Eleedal-Teeus"
		birth_date = 1323.1.1
		adm = 3
		dip = 5
		mil = 0
    }
}

1419.1.1 = {
	monarch = {
 		name = "Yexil"
		dynasty = "Ushureel"
		birth_date = 1372.1.1
		adm = 1
		dip = 5
		mil = 4
    }
}

1516.1.1 = {
	monarch = {
 		name = "Reekisk"
		dynasty = "Meeros"
		birth_date = 1477.1.1
		adm = 6
		dip = 4
		mil = 0
    }
}

1558.1.1 = {
	monarch = {
 		name = "Meeh-Zaw"
		dynasty = "Androdes"
		birth_date = 1511.1.1
		adm = 4
		dip = 3
		mil = 4
		female = yes
    }
}

1597.1.1 = {
	monarch = {
 		name = "Derkeehez"
		dynasty = "Canision"
		birth_date = 1556.1.1
		adm = 3
		dip = 2
		mil = 1
    }
}

1689.1.1 = {
	monarch = {
 		name = "Zasha-Ja"
		dynasty = "Xemclesh"
		birth_date = 1649.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1737.1.1 = {
	monarch = {
 		name = "Shatuna"
		dynasty = "Madeian"
		birth_date = 1690.1.1
		adm = 6
		dip = 1
		mil = 1
		female = yes
    }
}

1790.1.1 = {
	monarch = {
 		name = "Kudleez"
		dynasty = "Derkeeja"
		birth_date = 1759.1.1
		adm = 1
		dip = 2
		mil = 3
    }
}

1851.1.1 = {
	monarch = {
 		name = "Demeepa"
		dynasty = "Ushureel"
		birth_date = 1822.1.1
		adm = 6
		dip = 1
		mil = 6
    }
}

1929.1.1 = {
	monarch = {
 		name = "Meetza"
		dynasty = "Wanan-Dar"
		birth_date = 1904.1.1
		adm = 4
		dip = 0
		mil = 3
		female = yes
    }
}

1977.1.1 = {
	monarch = {
 		name = "Eatzapa"
		dynasty = "Neethsareeth"
		birth_date = 1926.1.1
		adm = 3
		dip = 6
		mil = 0
		female = yes
    }
}

2013.1.1 = {
	monarch = {
 		name = "Ajim-Ra"
		dynasty = "Talen-Lan"
		birth_date = 1975.1.1
		adm = 1
		dip = 6
		mil = 3
		female = yes
    }
}

2073.1.1 = {
	monarch = {
 		name = "Ruxol"
		dynasty = "Talen-Lan"
		birth_date = 2045.1.1
		adm = 6
		dip = 5
		mil = 0
    }
}

2157.1.1 = {
	monarch = {
 		name = "Luteema"
		dynasty = "Okaweeixth"
		birth_date = 2118.1.1
		adm = 4
		dip = 4
		mil = 3
    }
}

2253.1.1 = {
	monarch = {
 		name = "Dosu-Muz"
		dynasty = "Casmareen"
		birth_date = 2202.1.1
		adm = 3
		dip = 3
		mil = 0
    }
}

2343.1.1 = {
	monarch = {
 		name = "Ahaht-Naat"
		dynasty = "Theermarush"
		birth_date = 2290.1.1
		adm = 5
		dip = 5
		mil = 2
    }
}

2415.1.1 = {
	monarch = {
 		name = "Rupah"
		dynasty = "Talen-Ei"
		birth_date = 2379.1.1
		adm = 3
		dip = 4
		mil = 5
    }
}

2481.1.1 = {
	monarch = {
 		name = "Am-Sakka"
		dynasty = "Caylus"
		birth_date = 2462.1.1
		adm = 1
		dip = 3
		mil = 2
    }
}

