government = monarchy
government_rank = 1
mercantilism = 1
technology_group = cyrodiil_tg
religion = azura_cult
primary_culture = ayleid
capital = 1217

54.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Orhualmuunint"
		monarch_name = "Orhualmuunint I"
		dynasty = "Ula-Larilatia"
		birth_date = 52.1.1
		death_date = 70.1.1
		claim = 100
		adm = 3
		dip = 4
		mil = 3
    }
}

70.1.1 = {
	monarch = {
 		name = "Filestis"
		dynasty = "Mea-Hastrel"
		birth_date = 51.1.1
		adm = 2
		dip = 0
		mil = 3
    }
}

127.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Quaronaldil"
		monarch_name = "Quaronaldil I"
		dynasty = "Vae-Selviloria"
		birth_date = 119.1.1
		death_date = 137.1.1
		claim = 100
		adm = 3
		dip = 5
		mil = 1
    }
}

137.1.1 = {
	monarch = {
 		name = "Cyr"
		dynasty = "Mea-Aderina"
		birth_date = 110.1.1
		adm = 6
		dip = 6
		mil = 4
    }
	queen = {
 		name = "Erraduure"
		dynasty = "Mea-Aderina"
		birth_date = 84.1.1
		adm = 2
		dip = 4
		mil = 3
		female = yes
    }
	heir = {
 		name = "Soreshi"
		monarch_name = "Soreshi I"
		dynasty = "Mea-Aderina"
		birth_date = 122.1.1
		death_date = 195.1.1
		claim = 100
		adm = 6
		dip = 3
		mil = 2
		female = yes
    }
}

195.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Niruugadant"
		monarch_name = "Niruugadant I"
		dynasty = "Ula-Miscarcand"
		birth_date = 185.1.1
		death_date = 203.1.1
		claim = 100
		adm = 5
		dip = 2
		mil = 5
    }
}

203.1.1 = {
	monarch = {
 		name = "Leydel"
		dynasty = "Ula-Latara"
		birth_date = 184.1.1
		adm = 4
		dip = 6
		mil = 6
    }
}

262.1.1 = {
	monarch = {
 		name = "Rorguuramis"
		dynasty = "Ula-Linchal"
		birth_date = 216.1.1
		adm = 2
		dip = 5
		mil = 2
    }
	queen = {
 		name = "Mevirlen"
		dynasty = "Ula-Linchal"
		birth_date = 223.1.1
		adm = 6
		dip = 3
		mil = 1
		female = yes
    }
	heir = {
 		name = "Varondil"
		monarch_name = "Varondil I"
		dynasty = "Ula-Linchal"
		birth_date = 259.1.1
		death_date = 303.1.1
		claim = 100
		adm = 6
		dip = 0
		mil = 2
    }
}

303.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Dyzas"
		monarch_name = "Dyzas I"
		dynasty = "Vae-Vietia"
		birth_date = 300.1.1
		death_date = 318.1.1
		claim = 100
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

318.1.1 = {
	monarch = {
 		name = "Muzos"
		dynasty = "Vae-Tasaso"
		birth_date = 299.1.1
		adm = 4
		dip = 2
		mil = 6
		female = yes
    }
	queen = {
 		name = "Qon"
		dynasty = "Vae-Tasaso"
		birth_date = 269.1.1
		adm = 1
		dip = 1
		mil = 5
    }
	heir = {
 		name = "Lida"
		monarch_name = "Lida I"
		dynasty = "Vae-Tasaso"
		birth_date = 314.1.1
		death_date = 363.1.1
		claim = 100
		adm = 5
		dip = 6
		mil = 4
    }
}

363.1.1 = {
	monarch = {
 		name = "Omashaul"
		dynasty = "Ula-Messiena"
		birth_date = 336.1.1
		adm = 0
		dip = 1
		mil = 6
    }
}

401.1.1 = {
	monarch = {
 		name = "Myzas"
		dynasty = "Vae-Vanua"
		birth_date = 362.1.1
		adm = 6
		dip = 0
		mil = 3
		female = yes
    }
}

438.1.1 = {
	monarch = {
 		name = "Muzos"
		dynasty = "Ula-Linchal"
		birth_date = 389.1.1
		adm = 1
		dip = 1
		mil = 5
		female = yes
    }
	queen = {
 		name = "Tjughandonos"
		dynasty = "Ula-Linchal"
		birth_date = 411.1.1
		adm = 1
		dip = 5
		mil = 6
    }
	heir = {
 		name = "Symmu"
		monarch_name = "Symmu I"
		dynasty = "Ula-Linchal"
		birth_date = 426.1.1
		death_date = 494.1.1
		claim = 100
		adm = 5
		dip = 3
		mil = 5
		female = yes
    }
}

494.1.1 = {
	monarch = {
 		name = "Lymher"
		dynasty = "Mea-Conoa"
		birth_date = 463.1.1
		adm = 4
		dip = 0
		mil = 5
    }
}

588.1.1 = {
	monarch = {
 		name = "Nilichi"
		dynasty = "Ula-Kvatch"
		birth_date = 548.1.1
		adm = 2
		dip = 6
		mil = 2
    }
}

677.1.1 = {
	monarch = {
 		name = "Djador"
		dynasty = "Mea-Alessia"
		birth_date = 650.1.1
		adm = 1
		dip = 5
		mil = 5
    }
	queen = {
 		name = "Pallush"
		dynasty = "Mea-Alessia"
		birth_date = 633.1.1
		adm = 4
		dip = 4
		mil = 4
		female = yes
    }
	heir = {
 		name = "Hasind"
		monarch_name = "Hasind I"
		dynasty = "Mea-Alessia"
		birth_date = 673.1.1
		death_date = 722.1.1
		claim = 100
		adm = 1
		dip = 2
		mil = 3
    }
}

722.1.1 = {
	monarch = {
 		name = "Parrunde"
		dynasty = "Ula-Latara"
		birth_date = 683.1.1
		adm = 4
		dip = 4
		mil = 5
		female = yes
    }
}

797.1.1 = {
	monarch = {
 		name = "Nilichi"
		dynasty = "Ula-Nornalhorst"
		birth_date = 771.1.1
		adm = 2
		dip = 3
		mil = 2
    }
}

837.1.1 = {
	monarch = {
 		name = "Djador"
		dynasty = "Mea-Aderina"
		birth_date = 802.1.1
		adm = 4
		dip = 4
		mil = 4
    }
	queen = {
 		name = "Cavannorel"
		dynasty = "Mea-Aderina"
		birth_date = 810.1.1
		adm = 4
		dip = 0
		mil = 5
		female = yes
    }
	heir = {
 		name = "Immol"
		monarch_name = "Immol I"
		dynasty = "Mea-Aderina"
		birth_date = 825.1.1
		death_date = 934.1.1
		claim = 100
		adm = 1
		dip = 6
		mil = 4
		female = yes
    }
}

934.1.1 = {
	monarch = {
 		name = "Vem"
		dynasty = "Ula-Istirus"
		birth_date = 891.1.1
		adm = 1
		dip = 2
		mil = 4
    }
}

982.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Cyledha"
		monarch_name = "Cyledha I"
		dynasty = "Vae-Varondo"
		birth_date = 977.1.1
		death_date = 995.1.1
		claim = 100
		adm = 1
		dip = 6
		mil = 2
		female = yes
    }
}

995.1.1 = {
	monarch = {
 		name = "Cunylne"
		dynasty = "Vae-Vassorman"
		birth_date = 956.1.1
		adm = 4
		dip = 1
		mil = 4
		female = yes
    }
}

1045.1.1 = {
	monarch = {
 		name = "Uluscant"
		dynasty = "Mea-Aderina"
		birth_date = 1001.1.1
		adm = 4
		dip = 1
		mil = 6
    }
	queen = {
 		name = "Leru"
		dynasty = "Mea-Aderina"
		birth_date = 1018.1.1
		adm = 1
		dip = 6
		mil = 5
		female = yes
    }
	heir = {
 		name = "Irgogol"
		monarch_name = "Irgogol I"
		dynasty = "Mea-Aderina"
		birth_date = 1039.1.1
		death_date = 1090.1.1
		claim = 100
		adm = 5
		dip = 5
		mil = 4
    }
}

1090.1.1 = {
	monarch = {
 		name = "Leydel"
		dynasty = "Mea-Harm's"
		birth_date = 1067.1.1
		adm = 4
		dip = 1
		mil = 4
    }
	queen = {
 		name = "Wuria"
		dynasty = "Mea-Harm's"
		birth_date = 1042.1.1
		adm = 1
		dip = 0
		mil = 3
		female = yes
    }
}

1168.1.1 = {
	monarch = {
 		name = "Uluscant"
		dynasty = "Mea-Amane"
		birth_date = 1133.1.1
		adm = 6
		dip = 6
		mil = 6
    }
}

1232.1.1 = {
	monarch = {
 		name = "Tuanum"
		dynasty = "Ula-Nonungalo"
		birth_date = 1213.1.1
		adm = 4
		dip = 5
		mil = 3
    }
}

1284.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Vunhualdadont"
		monarch_name = "Vunhualdadont I"
		dynasty = "Mea-Hackdirt"
		birth_date = 1272.1.1
		death_date = 1290.1.1
		claim = 100
		adm = 5
		dip = 2
		mil = 1
    }
}

1290.1.1 = {
	monarch = {
 		name = "Linduasdesem"
		dynasty = "Vae-Sylolvia"
		birth_date = 1249.1.1
		adm = 1
		dip = 4
		mil = 3
    }
}

1346.1.1 = {
	monarch = {
 		name = "Djamduusceges"
		dynasty = "Vae-Vilverin"
		birth_date = 1316.1.1
		adm = 6
		dip = 3
		mil = 0
    }
}

1393.1.1 = {
	monarch = {
 		name = "Emma"
		dynasty = "Ula-Jorabrina"
		birth_date = 1356.1.1
		adm = 4
		dip = 2
		mil = 4
		female = yes
    }
}

1459.1.1 = {
	monarch = {
 		name = "Nym"
		dynasty = "Ula-Ryndenyse"
		birth_date = 1420.1.1
		adm = 2
		dip = 1
		mil = 0
    }
}

1511.1.1 = {
	monarch = {
 		name = "Harrylnes"
		dynasty = "Ula-Restidina"
		birth_date = 1492.1.1
		adm = 4
		dip = 2
		mil = 2
		female = yes
    }
	queen = {
 		name = "Lagan"
		dynasty = "Ula-Restidina"
		birth_date = 1459.1.1
		adm = 5
		dip = 6
		mil = 3
    }
	heir = {
 		name = "Cusdar"
		monarch_name = "Cusdar I"
		dynasty = "Ula-Restidina"
		birth_date = 1497.1.1
		death_date = 1567.1.1
		claim = 100
		adm = 1
		dip = 4
		mil = 2
    }
}

1567.1.1 = {
	monarch = {
 		name = "Vuudunt"
		dynasty = "Mea-Brina"
		birth_date = 1533.1.1
		adm = 0
		dip = 4
		mil = 5
    }
}

1644.1.1 = {
	monarch = {
 		name = "Fin"
		dynasty = "Ula-Ontus"
		birth_date = 1603.1.1
		adm = 2
		dip = 5
		mil = 0
    }
	queen = {
 		name = "Dynnyan"
		dynasty = "Ula-Ontus"
		birth_date = 1596.1.1
		adm = 2
		dip = 1
		mil = 1
		female = yes
    }
	heir = {
 		name = "Hergor"
		monarch_name = "Hergor I"
		dynasty = "Ula-Ontus"
		birth_date = 1631.1.1
		death_date = 1709.1.1
		claim = 100
		adm = 6
		dip = 0
		mil = 0
    }
}

1709.1.1 = {
	monarch = {
 		name = "Teymeyladis"
		dynasty = "Ula-Nenyond"
		birth_date = 1690.1.1
		adm = 5
		dip = 3
		mil = 0
    }
}

1800.1.1 = {
	monarch = {
 		name = "Arro"
		dynasty = "Vae-Silorn"
		birth_date = 1758.1.1
		adm = 3
		dip = 3
		mil = 4
		female = yes
    }
}

1875.1.1 = {
	monarch = {
 		name = "Ostarand"
		dynasty = "Mea-Brina"
		birth_date = 1827.1.1
		adm = 4
		dip = 0
		mil = 2
    }
}

1967.1.1 = {
	monarch = {
 		name = "Suhe"
		dynasty = "Vae-Vanua"
		birth_date = 1917.1.1
		adm = 1
		dip = 2
		mil = 2
		female = yes
    }
	queen = {
 		name = "Vem"
		dynasty = "Vae-Vanua"
		birth_date = 1927.1.1
		adm = 5
		dip = 0
		mil = 1
    }
	heir = {
 		name = "Umhuur"
		monarch_name = "Umhuur I"
		dynasty = "Vae-Vanua"
		birth_date = 1963.1.1
		death_date = 2006.1.1
		claim = 100
		adm = 2
		dip = 6
		mil = 0
    }
}

2006.1.1 = {
	monarch = {
 		name = "Summi"
		dynasty = "Vae-Skingrad"
		birth_date = 1964.1.1
		adm = 1
		dip = 2
		mil = 0
		female = yes
    }
	queen = {
 		name = "Tamhuundogo"
		dynasty = "Vae-Skingrad"
		birth_date = 1964.1.1
		adm = 5
		dip = 1
		mil = 6
    }
}

2055.1.1 = {
	monarch = {
 		name = "Hosnuugaura"
		dynasty = "Ula-Lalaifre"
		birth_date = 2034.1.1
		adm = 3
		dip = 0
		mil = 3
    }
}

2096.1.1 = {
	monarch = {
 		name = "Wynaldia"
		dynasty = "Vae-Vesagrius"
		birth_date = 2053.1.1
		adm = 2
		dip = 6
		mil = 6
		female = yes
    }
}

2145.1.1 = {
	monarch = {
 		name = "Poman"
		dynasty = "Vae-Vonara"
		birth_date = 2109.1.1
		adm = 0
		dip = 5
		mil = 3
		female = yes
    }
}

2234.1.1 = {
	monarch = {
 		name = "Orhualmuunint"
		dynasty = "Mea-Culotte"
		birth_date = 2213.1.1
		adm = 5
		dip = 4
		mil = 0
    }
}

2329.1.1 = {
	monarch = {
 		name = "Cimase"
		dynasty = "Ula-Jorarienna"
		birth_date = 2289.1.1
		adm = 3
		dip = 4
		mil = 3
		female = yes
    }
	queen = {
 		name = "Ragedarel"
		dynasty = "Ula-Jorarienna"
		birth_date = 2283.1.1
		adm = 0
		dip = 2
		mil = 2
    }
	heir = {
 		name = "Lonnosson"
		monarch_name = "Lonnosson I"
		dynasty = "Ula-Jorarienna"
		birth_date = 2321.1.1
		death_date = 2423.1.1
		claim = 100
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

2423.1.1 = {
	monarch = {
 		name = "Ymmilune"
		dynasty = "Mea-Alessia"
		birth_date = 2400.1.1
		adm = 0
		dip = 2
		mil = 4
		female = yes
    }
	queen = {
 		name = "Cosudam"
		dynasty = "Mea-Alessia"
		birth_date = 2387.1.1
		adm = 4
		dip = 1
		mil = 3
    }
	heir = {
 		name = "Heystuadhisind"
		monarch_name = "Heystuadhisind I"
		dynasty = "Mea-Alessia"
		birth_date = 2408.1.1
		death_date = 2508.1.1
		claim = 100
		adm = 1
		dip = 6
		mil = 2
    }
}

