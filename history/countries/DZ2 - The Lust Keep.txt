government = tribal
government_rank = 1
mercantilism = 1
technology_group = daedric_tg
religion = mephala_cult
primary_culture = arachnid
capital = 4311

54.1.1 = {
	monarch = {
 		name = "Acalrir"
		dynasty = "Roq'Roth'erhi"
		birth_date = 12.1.1
		adm = 5
		dip = 5
		mil = 0
    }
}

101.1.1 = {
	monarch = {
 		name = "Caasrit"
		dynasty = "Roq'Qhakil"
		birth_date = 82.1.1
		adm = 3
		dip = 4
		mil = 3
    }
}

164.1.1 = {
	monarch = {
 		name = "Qhorirka"
		dynasty = "Roq'Lhoshiq'shah"
		birth_date = 112.1.1
		adm = 2
		dip = 3
		mil = 0
    }
}

242.1.1 = {
	monarch = {
 		name = "Zrinqaq"
		dynasty = "		"
		birth_date = 197.1.1
		adm = 3
		dip = 5
		mil = 1
    }
}

308.1.1 = {
	monarch = {
 		name = "Sharhela"
		dynasty = "Roq'Qaicoth'ad"
		birth_date = 285.1.1
		adm = 2
		dip = 4
		mil = 5
		female = yes
    }
}

366.1.1 = {
	monarch = {
 		name = "Srorraced"
		dynasty = "Roq'Zhellakie"
		birth_date = 344.1.1
		adm = 0
		dip = 3
		mil = 2
    }
}

410.1.1 = {
	monarch = {
 		name = "Nakot"
		dynasty = "Roq'Szezsavish"
		birth_date = 390.1.1
		adm = 5
		dip = 2
		mil = 5
    }
}

494.1.1 = {
	monarch = {
 		name = "Rhalzichou"
		dynasty = "Roq'Qhakil"
		birth_date = 450.1.1
		adm = 3
		dip = 2
		mil = 2
    }
}

580.1.1 = {
	monarch = {
 		name = "Ciancharoq"
		dynasty = "Roq'Ninchosh"
		birth_date = 537.1.1
		adm = 2
		dip = 1
		mil = 6
    }
}

665.1.1 = {
	monarch = {
 		name = "Ezex"
		dynasty = "Roq'Yakkel"
		birth_date = 620.1.1
		adm = 0
		dip = 0
		mil = 2
    }
}

732.1.1 = {
	monarch = {
 		name = "Sellee"
		dynasty = "Roq'Rhenquth"
		birth_date = 703.1.1
		adm = 5
		dip = 6
		mil = 6
		female = yes
    }
}

821.1.1 = {
	monarch = {
 		name = "Rhalzichou"
		dynasty = "Roq'Zheirroth"
		birth_date = 774.1.1
		adm = 0
		dip = 0
		mil = 0
    }
}

865.1.1 = {
	monarch = {
 		name = "Qithesh"
		dynasty = "Roq'Riq'sheecoh"
		birth_date = 830.1.1
		adm = 5
		dip = 0
		mil = 4
		female = yes
    }
}

923.1.1 = {
	monarch = {
 		name = "Krokos"
		dynasty = "Roq'Rel'shal"
		birth_date = 903.1.1
		adm = 4
		dip = 6
		mil = 1
    }
}

1018.1.1 = {
	monarch = {
 		name = "Qarri"
		dynasty = "Roq'Rashra"
		birth_date = 990.1.1
		adm = 2
		dip = 5
		mil = 4
    }
}

1080.1.1 = {
	monarch = {
 		name = "Krirud"
		dynasty = "Roq'Yezho"
		birth_date = 1037.1.1
		adm = 0
		dip = 4
		mil = 1
    }
}

1130.1.1 = {
	monarch = {
 		name = "Ak'zorrob"
		dynasty = "Roq'Lazus"
		birth_date = 1110.1.1
		adm = 5
		dip = 3
		mil = 5
    }
}

1210.1.1 = {
	monarch = {
 		name = "Qhantuqash"
		dynasty = "Roq'Zhok'thiecha"
		birth_date = 1175.1.1
		adm = 4
		dip = 3
		mil = 1
		female = yes
    }
}

1270.1.1 = {
	monarch = {
 		name = "Qarri"
		dynasty = "Roq'Leqa"
		birth_date = 1241.1.1
		adm = 2
		dip = 2
		mil = 5
    }
}

1360.1.1 = {
	monarch = {
 		name = "Leviqad"
		dynasty = "Roq'Szeluthe"
		birth_date = 1314.1.1
		adm = 4
		dip = 3
		mil = 0
		female = yes
    }
}

1417.1.1 = {
	monarch = {
 		name = "Ak'zorrob"
		dynasty = "Roq'Rhovu"
		birth_date = 1377.1.1
		adm = 2
		dip = 2
		mil = 3
    }
}

1488.1.1 = {
	monarch = {
 		name = "Kloxhu"
		dynasty = "Roq'Oro"
		birth_date = 1458.1.1
		adm = 0
		dip = 2
		mil = 0
		female = yes
    }
}

1538.1.1 = {
	monarch = {
 		name = "Yevu"
		dynasty = "Roq'Zilni"
		birth_date = 1510.1.1
		adm = 5
		dip = 1
		mil = 4
		female = yes
    }
}

1603.1.1 = {
	monarch = {
 		name = "Qrex'ad"
		dynasty = "Roq'Chan'qie"
		birth_date = 1555.1.1
		adm = 4
		dip = 0
		mil = 0
    }
}

1688.1.1 = {
	monarch = {
 		name = "Naqa"
		dynasty = "Roq'Klik'zud"
		birth_date = 1639.1.1
		adm = 4
		dip = 4
		mil = 5
		female = yes
    }
}

1735.1.1 = {
	monarch = {
 		name = "Rhaisu"
		dynasty = "Roq'Charosa"
		birth_date = 1687.1.1
		adm = 3
		dip = 3
		mil = 2
    }
}

1781.1.1 = {
	monarch = {
 		name = "Yorruchax"
		dynasty = "Roq'Klacu"
		birth_date = 1742.1.1
		adm = 1
		dip = 2
		mil = 5
    }
}

1867.1.1 = {
	monarch = {
 		name = "Rachel'shais"
		dynasty = "Roq'Rin'qash"
		birth_date = 1822.1.1
		adm = 6
		dip = 2
		mil = 2
		female = yes
    }
}

1914.1.1 = {
	monarch = {
 		name = "Iavak'sib"
		dynasty = "Roq'Zakacath"
		birth_date = 1868.1.1
		adm = 4
		dip = 1
		mil = 6
    }
}

1995.1.1 = {
	monarch = {
 		name = "Rhaisu"
		dynasty = "Roq'Yan'qash"
		birth_date = 1942.1.1
		adm = 3
		dip = 0
		mil = 2
    }
}

2069.1.1 = {
	monarch = {
 		name = "Yorruchax"
		dynasty = "Roq'Zhenchi"
		birth_date = 2028.1.1
		adm = 5
		dip = 1
		mil = 4
    }
}

2136.1.1 = {
	monarch = {
 		name = "Qhizsis"
		dynasty = "Roq'Klish'ta"
		birth_date = 2102.1.1
		adm = 2
		dip = 3
		mil = 4
    }
}

2222.1.1 = {
	monarch = {
 		name = "Ezex"
		dynasty = "Roq'Shreeshoki"
		birth_date = 2202.1.1
		adm = 0
		dip = 3
		mil = 1
    }
}

2284.1.1 = {
	monarch = {
 		name = "Acalrir"
		dynasty = "Roq'Khrak'zid"
		birth_date = 2249.1.1
		adm = 2
		dip = 4
		mil = 2
    }
}

2324.1.1 = {
	monarch = {
 		name = "Caasrit"
		dynasty = "Roq'Qhivor"
		birth_date = 2301.1.1
		adm = 0
		dip = 3
		mil = 6
    }
}

2409.1.1 = {
	monarch = {
 		name = "Ak'zorrob"
		dynasty = "Roq'Roke"
		birth_date = 2363.1.1
		adm = 5
		dip = 2
		mil = 2
    }
}

2445.1.1 = {
	monarch = {
 		name = "Qhantuqash"
		dynasty = "Roq'Zorud"
		birth_date = 2410.1.1
		adm = 4
		dip = 1
		mil = 6
		female = yes
    }
}

