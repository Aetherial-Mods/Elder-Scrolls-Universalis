government = monarchy
government_rank = 2
mercantilism = 1
technology_group = northern_tg
religion = nordic_pantheon
primary_culture = nord
capital = 7300

54.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Sorri"
		monarch_name = "Sorri I"
		dynasty = "Frargil"
		birth_date = 48.1.1
		death_date = 66.1.1
		claim = 100
		adm = 4
		dip = 0
		mil = 3
    }
}

66.1.1 = {
	monarch = {
 		name = "Hemgar"
		dynasty = "Frargil"
		birth_date = 17.1.1
		adm = 3
		dip = 4
		mil = 3
    }
}

107.1.1 = {
	monarch = {
 		name = "Bona"
		dynasty = "Thrialder"
		birth_date = 58.1.1
		adm = 2
		dip = 3
		mil = 0
		female = yes
    }
}

203.1.1 = {
	monarch = {
 		name = "Thorygg"
		dynasty = "Korveldr"
		birth_date = 184.1.1
		adm = 0
		dip = 2
		mil = 3
    }
}

286.1.1 = {
	monarch = {
 		name = "Korra"
		dynasty = "Fragdrudrer"
		birth_date = 248.1.1
		adm = 5
		dip = 1
		mil = 0
		female = yes
    }
	queen = {
 		name = "Admaer"
		dynasty = "Fragdrudrer"
		birth_date = 260.1.1
		adm = 2
		dip = 0
		mil = 6
    }
	heir = {
 		name = "Kraval"
		monarch_name = "Kraval I"
		dynasty = "Fragdrudrer"
		birth_date = 277.1.1
		death_date = 333.1.1
		claim = 100
		adm = 6
		dip = 5
		mil = 5
    }
}

333.1.1 = {
	monarch = {
 		name = "Bolund"
		dynasty = "Korveldr"
		birth_date = 301.1.1
		adm = 2
		dip = 0
		mil = 0
    }
	queen = {
 		name = "Ajorda"
		dynasty = "Korveldr"
		birth_date = 300.1.1
		adm = 6
		dip = 5
		mil = 6
		female = yes
    }
	heir = {
 		name = "Llewellyn"
		monarch_name = "Llewellyn I"
		dynasty = "Korveldr"
		birth_date = 318.1.1
		death_date = 388.1.1
		claim = 100
		adm = 2
		dip = 4
		mil = 5
    }
}

388.1.1 = {
	monarch = {
 		name = "Makoll"
		dynasty = "Rolrifodr"
		birth_date = 361.1.1
		adm = 2
		dip = 0
		mil = 6
    }
	queen = {
 		name = "Ingrid"
		dynasty = "Rolrifodr"
		birth_date = 348.1.1
		adm = 6
		dip = 6
		mil = 5
		female = yes
    }
}

459.1.1 = {
	monarch = {
 		name = "Thvidi"
		dynasty = "Brundeskr"
		birth_date = 429.1.1
		adm = 4
		dip = 5
		mil = 1
		female = yes
    }
	queen = {
 		name = "Irgnar"
		dynasty = "Brundeskr"
		birth_date = 407.1.1
		adm = 5
		dip = 6
		mil = 6
    }
	heir = {
 		name = "Aldis"
		monarch_name = "Aldis I"
		dynasty = "Brundeskr"
		birth_date = 458.1.1
		death_date = 525.1.1
		claim = 100
		adm = 2
		dip = 4
		mil = 5
    }
}

525.1.1 = {
	monarch = {
 		name = "Jofrior"
		dynasty = "Hlalmadrun"
		birth_date = 490.1.1
		adm = 5
		dip = 5
		mil = 0
		female = yes
    }
	queen = {
 		name = "Jeggrich"
		dynasty = "Hlalmadrun"
		birth_date = 496.1.1
		adm = 2
		dip = 4
		mil = 6
    }
	heir = {
 		name = "Jyrik"
		monarch_name = "Jyrik I"
		dynasty = "Hlalmadrun"
		birth_date = 514.1.1
		death_date = 561.1.1
		claim = 100
		adm = 6
		dip = 3
		mil = 5
    }
}

561.1.1 = {
	monarch = {
 		name = "Attiring"
		dynasty = "Hadrehyldr"
		birth_date = 529.1.1
		adm = 5
		dip = 6
		mil = 5
		female = yes
    }
}

611.1.1 = {
	monarch = {
 		name = "Styrr"
		dynasty = "Kosnorr"
		birth_date = 584.1.1
		adm = 3
		dip = 5
		mil = 2
    }
}

660.1.1 = {
	monarch = {
 		name = "Berfar"
		dynasty = "Valben"
		birth_date = 624.1.1
		adm = 2
		dip = 4
		mil = 6
    }
	queen = {
 		name = "Hridi"
		dynasty = "Valben"
		birth_date = 630.1.1
		adm = 5
		dip = 3
		mil = 5
		female = yes
    }
	heir = {
 		name = "Alvide"
		monarch_name = "Alvide I"
		dynasty = "Valben"
		birth_date = 658.1.1
		death_date = 712.1.1
		claim = 100
		adm = 2
		dip = 1
		mil = 4
		female = yes
    }
}

712.1.1 = {
	monarch = {
 		name = "Katisha"
		dynasty = "Keld"
		birth_date = 688.1.1
		adm = 5
		dip = 3
		mil = 6
		female = yes
    }
}

777.1.1 = {
	monarch = {
 		name = "Hanse"
		dynasty = "Thol"
		birth_date = 742.1.1
		adm = 3
		dip = 2
		mil = 3
    }
}

867.1.1 = {
	monarch = {
 		name = "Belrand"
		dynasty = "Kamen"
		birth_date = 847.1.1
		adm = 2
		dip = 1
		mil = 6
    }
}

962.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Anders"
		monarch_name = "Anders I"
		dynasty = "Hlalmadrun"
		birth_date = 960.1.1
		death_date = 978.1.1
		claim = 100
		adm = 2
		dip = 5
		mil = 4
    }
}

978.1.1 = {
	monarch = {
 		name = "Kari"
		dynasty = "Rhenwath"
		birth_date = 926.1.1
		adm = 2
		dip = 2
		mil = 4
		female = yes
    }
}

1025.1.1 = {
	monarch = {
 		name = "Hamar"
		dynasty = "Sisgamth"
		birth_date = 991.1.1
		adm = 0
		dip = 1
		mil = 1
    }
	queen = {
 		name = "Fjori"
		dynasty = "Sisgamth"
		birth_date = 984.1.1
		adm = 4
		dip = 6
		mil = 0
		female = yes
    }
	heir = {
 		name = "Rosrytta"
		monarch_name = "Rosrytta I"
		dynasty = "Sisgamth"
		birth_date = 1016.1.1
		death_date = 1074.1.1
		claim = 100
		adm = 4
		dip = 3
		mil = 1
		female = yes
    }
}

1074.1.1 = {
	monarch = {
 		name = "Heidmir"
		dynasty = "Moegianl"
		birth_date = 1046.1.1
		adm = 3
		dip = 6
		mil = 1
    }
}

1111.1.1 = {
	monarch = {
 		name = "Bofesar"
		dynasty = "Hjemuld"
		birth_date = 1089.1.1
		adm = 2
		dip = 5
		mil = 5
    }
}

1174.1.1 = {
	monarch = {
 		name = "Thorbald"
		dynasty = "Modmeldil"
		birth_date = 1147.1.1
		adm = 0
		dip = 5
		mil = 2
    }
	queen = {
 		name = "Fjola"
		dynasty = "Modmeldil"
		birth_date = 1130.1.1
		adm = 4
		dip = 3
		mil = 1
		female = yes
    }
	heir = {
 		name = "Sober"
		monarch_name = "Sober I"
		dynasty = "Modmeldil"
		birth_date = 1159.1.1
		death_date = 1236.1.1
		claim = 100
		adm = 1
		dip = 2
		mil = 0
    }
}

1236.1.1 = {
	monarch = {
 		name = "Heddic"
		dynasty = "Brinhal"
		birth_date = 1187.1.1
		adm = 3
		dip = 3
		mil = 2
    }
	queen = {
 		name = "Freida"
		dynasty = "Brinhal"
		birth_date = 1207.1.1
		adm = 0
		dip = 2
		mil = 1
		female = yes
    }
}

1300.1.1 = {
	monarch = {
 		name = "Nodur"
		dynasty = "Morvenend"
		birth_date = 1268.1.1
		adm = 6
		dip = 1
		mil = 5
    }
	queen = {
 		name = "Jodis"
		dynasty = "Morvenend"
		birth_date = 1254.1.1
		adm = 2
		dip = 6
		mil = 4
		female = yes
    }
	heir = {
 		name = "Boti"
		monarch_name = "Boti I"
		dynasty = "Morvenend"
		birth_date = 1294.1.1
		death_date = 1344.1.1
		claim = 100
		adm = 6
		dip = 5
		mil = 3
		female = yes
    }
}

1344.1.1 = {
	monarch = {
 		name = "Chalda"
		dynasty = "Tholnofeldr"
		birth_date = 1301.1.1
		adm = 6
		dip = 1
		mil = 3
		female = yes
    }
	queen = {
 		name = "Banthor"
		dynasty = "Tholnofeldr"
		birth_date = 1313.1.1
		adm = 3
		dip = 0
		mil = 2
    }
}

1412.1.1 = {
	monarch = {
 		name = "Iglund"
		dynasty = "Radr"
		birth_date = 1371.1.1
		adm = 1
		dip = 6
		mil = 5
    }
}

1489.1.1 = {
	monarch = {
 		name = "Rogjoll"
		dynasty = "Kamen"
		birth_date = 1442.1.1
		adm = 6
		dip = 5
		mil = 2
    }
	queen = {
 		name = "Bonwyn"
		dynasty = "Kamen"
		birth_date = 1464.1.1
		adm = 3
		dip = 4
		mil = 1
		female = yes
    }
	heir = {
 		name = "Oljorn"
		monarch_name = "Oljorn I"
		dynasty = "Kamen"
		birth_date = 1488.1.1
		death_date = 1576.1.1
		claim = 100
		adm = 0
		dip = 2
		mil = 0
    }
}

1576.1.1 = {
	monarch = {
 		name = "Frald"
		dynasty = "Skolnil"
		birth_date = 1549.1.1
		adm = 3
		dip = 4
		mil = 2
    }
}

1639.1.1 = {
	monarch = {
 		name = "Wuunferth"
		dynasty = "Brelskald"
		birth_date = 1588.1.1
		adm = 1
		dip = 3
		mil = 6
    }
}

1737.1.1 = {
	monarch = {
 		name = "Nura"
		dynasty = "Gechem"
		birth_date = 1684.1.1
		adm = 6
		dip = 2
		mil = 3
		female = yes
    }
}

1793.1.1 = {
	monarch = {
 		name = "Iver"
		dynasty = "Skoetgogam"
		birth_date = 1763.1.1
		adm = 4
		dip = 1
		mil = 6
    }
	queen = {
 		name = "Hama"
		dynasty = "Skoetgogam"
		birth_date = 1763.1.1
		adm = 1
		dip = 0
		mil = 5
		female = yes
    }
	heir = {
 		name = "Vikord"
		monarch_name = "Vikord I"
		dynasty = "Skoetgogam"
		birth_date = 1793.1.1
		death_date = 1885.1.1
		claim = 100
		adm = 5
		dip = 5
		mil = 4
    }
}

1885.1.1 = {
	monarch = {
 		name = "Wulf"
		dynasty = "Kiaheldir"
		birth_date = 1860.1.1
		adm = 4
		dip = 2
		mil = 4
    }
}

1944.1.1 = {
	monarch = {
 		name = "Gandis"
		dynasty = "Kadref"
		birth_date = 1899.1.1
		adm = 3
		dip = 1
		mil = 1
    }
	queen = {
 		name = "Linlia"
		dynasty = "Kadref"
		birth_date = 1909.1.1
		adm = 0
		dip = 0
		mil = 0
		female = yes
    }
	heir = {
 		name = "Elsar"
		monarch_name = "Elsar I"
		dynasty = "Kadref"
		birth_date = 1939.1.1
		death_date = 2006.1.1
		claim = 100
		adm = 3
		dip = 5
		mil = 6
    }
}

2006.1.1 = {
	monarch = {
 		name = "Sabonn"
		dynasty = "Godehedr"
		birth_date = 1977.1.1
		adm = 4
		dip = 2
		mil = 0
    }
}

2091.1.1 = {
	monarch = {
 		name = "Jofnir"
		dynasty = "Gorjold"
		birth_date = 2038.1.1
		adm = 6
		dip = 3
		mil = 2
    }
}

2176.1.1 = {
	monarch = {
 		name = "Faylva"
		dynasty = "Thelmyn"
		birth_date = 2144.1.1
		adm = 3
		dip = 5
		mil = 2
		female = yes
    }
	queen = {
 		name = "Dagimar"
		dynasty = "Thelmyn"
		birth_date = 2129.1.1
		adm = 1
		dip = 1
		mil = 4
    }
	heir = {
 		name = "Eilenda"
		monarch_name = "Eilenda I"
		dynasty = "Thelmyn"
		birth_date = 2176.1.1
		death_date = 2222.1.1
		claim = 100
		adm = 3
		dip = 2
		mil = 0
		female = yes
    }
}

2222.1.1 = {
	monarch = {
 		name = "Rundi"
		dynasty = "Hledrind"
		birth_date = 2182.1.1
		adm = 3
		dip = 5
		mil = 0
    }
}

2263.1.1 = {
	monarch = {
 		name = "Hrosaa"
		dynasty = "Folsgold"
		birth_date = 2213.1.1
		adm = 1
		dip = 4
		mil = 4
		female = yes
    }
}

2335.1.1 = {
	monarch = {
 		name = "Sigaar"
		dynasty = "Gagrer"
		birth_date = 2307.1.1
		adm = 6
		dip = 4
		mil = 0
    }
	queen = {
 		name = "Caretaker"
		dynasty = "Gagrer"
		birth_date = 2288.1.1
		adm = 3
		dip = 2
		mil = 6
		female = yes
    }
	heir = {
 		name = "Harrald"
		monarch_name = "Harrald I"
		dynasty = "Gagrer"
		birth_date = 2333.1.1
		death_date = 2385.1.1
		claim = 100
		adm = 4
		dip = 3
		mil = 4
    }
}

2385.1.1 = {
	monarch = {
 		name = "Ulfr"
		dynasty = "Kigrildr"
		birth_date = 2345.1.1
		adm = 0
		dip = 4
		mil = 6
    }
}

2433.1.1 = {
	monarch = {
 		name = "Afneer"
		dynasty = "Frodmeldof"
		birth_date = 2393.1.1
		adm = 2
		dip = 5
		mil = 1
    }
}

2480.1.1 = {
	monarch = {
 		name = "Hefid"
		dynasty = "Kehaldoldr"
		birth_date = 2461.1.1
		adm = 0
		dip = 5
		mil = 4
		female = yes
    }
	queen = {
 		name = "Gurund"
		dynasty = "Kehaldoldr"
		birth_date = 2445.1.1
		adm = 4
		dip = 3
		mil = 3
    }
	heir = {
 		name = "Throknolf"
		monarch_name = "Throknolf I"
		dynasty = "Kehaldoldr"
		birth_date = 2466.1.1
		death_date = 2570.1.1
		claim = 100
		adm = 4
		dip = 0
		mil = 4
    }
}

