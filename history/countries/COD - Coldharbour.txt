government = monarchy
government_rank = 10
mercantilism = 1
technology_group = daedric_tg
religion = molag_bal_cult
primary_culture = soul_shriven
capital = 3188

54.1.1 = {
	monarch = {
 		name = "Vurmesoth"
		dynasty = "Sanimala"
		birth_date = 2.1.1
		adm = 0
		dip = 5
		mil = 5
    }
	add_ruler_personality = immortal_personality
	queen = {
 		name = "Ossonel"
		dynasty = "Sanimala"
		birth_date = 24.1.1
		adm = 4
		dip = 3
		mil = 4
		female = yes
    }
	add_queen_personality = immortal_personality
	heir = {
 		name = "Hetok"
		monarch_name = "Hetok I"
		dynasty = "Sanimala"
		birth_date = 43.1.1
		death_date = 150.1.1
		claim = 100
		adm = 1
		dip = 2
		mil = 3
    }
	add_heir_personality = immortal_personality
}

150.1.1 = {
	monarch = {
 		name = "Dastith"
		dynasty = "Melfezita"
		birth_date = 114.1.1
		adm = 0
		dip = 5
		mil = 3
    }
	add_ruler_personality = immortal_personality
}

215.1.1 = {
	monarch = {
 		name = "Kaahlec"
		dynasty = "Nelfaza"
		birth_date = 188.1.1
		adm = 5
		dip = 4
		mil = 0
    }
	add_ruler_personality = immortal_personality
}

289.1.1 = {
	monarch = {
 		name = "Lerrimiv"
		dynasty = "Ilveyashe"
		birth_date = 256.1.1
		adm = 3
		dip = 3
		mil = 4
		female = yes
    }
	add_ruler_personality = immortal_personality
}

366.1.1 = {
	monarch = {
 		name = "Vredraonir"
		dynasty = "Nindaro"
		birth_date = 326.1.1
		adm = 2
		dip = 3
		mil = 0
    }
	add_ruler_personality = immortal_personality
}

407.1.1 = {
	monarch = {
 		name = "Nhystareth"
		dynasty = "Zifenne"
		birth_date = 375.1.1
		adm = 0
		dip = 2
		mil = 4
    }
	add_ruler_personality = immortal_personality
	queen = {
 		name = "Isur"
		dynasty = "Zifenne"
		birth_date = 357.1.1
		adm = 4
		dip = 0
		mil = 3
		female = yes
    }
	add_queen_personality = immortal_personality
	heir = {
 		name = "Shahaulec"
		monarch_name = "Shahaulec I"
		dynasty = "Zifenne"
		birth_date = 401.1.1
		death_date = 472.1.1
		claim = 100
		adm = 1
		dip = 6
		mil = 2
    }
	add_heir_personality = immortal_personality
}

472.1.1 = {
	monarch = {
 		name = "Vrakir"
		dynasty = "Serliri"
		birth_date = 444.1.1
		adm = 3
		dip = 0
		mil = 4
    }
	add_ruler_personality = immortal_personality
	queen = {
 		name = "Sovi"
		dynasty = "Serliri"
		birth_date = 429.1.1
		adm = 0
		dip = 6
		mil = 3
		female = yes
    }
	add_queen_personality = immortal_personality
	heir = {
 		name = "Haonuvic"
		monarch_name = "Haonuvic I"
		dynasty = "Serliri"
		birth_date = 468.1.1
		death_date = 551.1.1
		claim = 100
		adm = 4
		dip = 4
		mil = 2
    }
	add_heir_personality = immortal_personality
}

551.1.1 = {
	monarch = {
 		name = "Romale"
		dynasty = "Rirlini"
		birth_date = 512.1.1
		adm = 4
		dip = 1
		mil = 2
		female = yes
    }
	add_ruler_personality = immortal_personality
}

637.1.1 = {
	monarch = {
 		name = "Myhros"
		dynasty = "Lisaroma"
		birth_date = 615.1.1
		adm = 2
		dip = 0
		mil = 6
    }
	add_ruler_personality = immortal_personality
	queen = {
 		name = "Allalaer"
		dynasty = "Lisaroma"
		birth_date = 609.1.1
		adm = 6
		dip = 5
		mil = 5
		female = yes
    }
	add_queen_personality = immortal_personality
}

679.1.1 = {
	monarch = {
 		name = "Nhihreseth"
		dynasty = "Adase"
		birth_date = 645.1.1
		adm = 4
		dip = 5
		mil = 2
    }
	add_ruler_personality = immortal_personality
	queen = {
 		name = "Sonev"
		dynasty = "Adase"
		birth_date = 644.1.1
		adm = 1
		dip = 3
		mil = 1
		female = yes
    }
	add_queen_personality = immortal_personality
	heir = {
 		name = "Demralis"
		monarch_name = "Demralis I"
		dynasty = "Adase"
		birth_date = 664.1.1
		death_date = 742.1.1
		claim = 100
		adm = 1
		dip = 0
		mil = 2
    }
	add_heir_personality = immortal_personality
}

742.1.1 = {
	monarch = {
 		name = "Tadak"
		dynasty = "Nosenay"
		birth_date = 721.1.1
		adm = 0
		dip = 3
		mil = 2
    }
	add_ruler_personality = immortal_personality
}

788.1.1 = {
	monarch = {
 		name = "Resi"
		dynasty = "Irpase"
		birth_date = 735.1.1
		adm = 6
		dip = 2
		mil = 6
		female = yes
    }
	add_ruler_personality = immortal_personality
	queen = {
 		name = "Tudaarin"
		dynasty = "Irpase"
		birth_date = 766.1.1
		adm = 3
		dip = 1
		mil = 5
    }
	add_queen_personality = immortal_personality
	heir = {
 		name = "Vaulic"
		monarch_name = "Vaulic I"
		dynasty = "Irpase"
		birth_date = 774.1.1
		death_date = 847.1.1
		claim = 100
		adm = 6
		dip = 6
		mil = 3
    }
	add_heir_personality = immortal_personality
}

847.1.1 = {
	monarch = {
 		name = "Gumrok"
		dynasty = "Relefa"
		birth_date = 817.1.1
		adm = 2
		dip = 1
		mil = 6
    }
	add_ruler_personality = immortal_personality
	queen = {
 		name = "Sarul"
		dynasty = "Relefa"
		birth_date = 796.1.1
		adm = 6
		dip = 6
		mil = 5
		female = yes
    }
	add_queen_personality = immortal_personality
	heir = {
 		name = "Dennae"
		monarch_name = "Dennae I"
		dynasty = "Relefa"
		birth_date = 833.1.1
		death_date = 898.1.1
		claim = 100
		adm = 3
		dip = 5
		mil = 4
		female = yes
    }
	add_heir_personality = immortal_personality
}

898.1.1 = {
	monarch = {
 		name = "Gaohen"
		dynasty = "Irpase"
		birth_date = 872.1.1
		adm = 2
		dip = 1
		mil = 4
    }
	add_ruler_personality = immortal_personality
	queen = {
 		name = "Lirimiv"
		dynasty = "Irpase"
		birth_date = 871.1.1
		adm = 3
		dip = 5
		mil = 5
		female = yes
    }
	add_queen_personality = immortal_personality
	heir = {
 		name = "Zonemaer"
		monarch_name = "Zonemaer I"
		dynasty = "Irpase"
		birth_date = 890.1.1
		death_date = 983.1.1
		claim = 100
		adm = 6
		dip = 3
		mil = 4
		female = yes
    }
	add_heir_personality = immortal_personality
}

983.1.1 = {
	monarch = {
 		name = "Tamedir"
		dynasty = "Manlazo"
		birth_date = 955.1.1
		adm = 6
		dip = 0
		mil = 4
    }
	add_ruler_personality = immortal_personality
	queen = {
 		name = "Sarul"
		dynasty = "Manlazo"
		birth_date = 947.1.1
		adm = 3
		dip = 5
		mil = 3
		female = yes
    }
	add_queen_personality = immortal_personality
	heir = {
 		name = "Gilraoles"
		monarch_name = "Gilraoles I"
		dynasty = "Manlazo"
		birth_date = 977.1.1
		death_date = 1072.1.1
		claim = 100
		adm = 0
		dip = 4
		mil = 2
    }
	add_heir_personality = immortal_personality
}

1072.1.1 = {
	monarch = {
 		name = "Nirlyloth"
		dynasty = "Adase"
		birth_date = 1023.1.1
		adm = 2
		dip = 5
		mil = 5
    }
	add_ruler_personality = immortal_personality
	queen = {
 		name = "Lirimiv"
		dynasty = "Adase"
		birth_date = 1027.1.1
		adm = 6
		dip = 4
		mil = 4
		female = yes
    }
	add_queen_personality = immortal_personality
}

1156.1.1 = {
	monarch = {
 		name = "Hyker"
		dynasty = "Evrane"
		birth_date = 1115.1.1
		adm = 0
		dip = 1
		mil = 2
    }
	add_ruler_personality = immortal_personality
	queen = {
 		name = "Emah"
		dynasty = "Evrane"
		birth_date = 1133.1.1
		adm = 4
		dip = 6
		mil = 0
		female = yes
    }
	add_queen_personality = immortal_personality
	heir = {
 		name = "Shahaulec"
		monarch_name = "Shahaulec I"
		dynasty = "Evrane"
		birth_date = 1156.1.1
		death_date = 1206.1.1
		claim = 100
		adm = 4
		dip = 3
		mil = 2
    }
	add_heir_personality = immortal_personality
}

1206.1.1 = {
	monarch = {
 		name = "Lerrimiv"
		dynasty = "Evamese"
		birth_date = 1170.1.1
		adm = 3
		dip = 6
		mil = 2
		female = yes
    }
	add_ruler_personality = immortal_personality
	queen = {
 		name = "Vredraudin"
		dynasty = "Evamese"
		birth_date = 1168.1.1
		adm = 0
		dip = 5
		mil = 1
    }
	add_queen_personality = immortal_personality
	heir = {
 		name = "Mimohu"
		monarch_name = "Mimohu I"
		dynasty = "Evamese"
		birth_date = 1195.1.1
		death_date = 1260.1.1
		claim = 100
		adm = 4
		dip = 3
		mil = 0
		female = yes
    }
	add_heir_personality = immortal_personality
}

1260.1.1 = {
	monarch = {
 		name = "Vena"
		dynasty = "Sonevo"
		birth_date = 1225.1.1
		adm = 0
		dip = 4
		mil = 2
		female = yes
    }
	add_ruler_personality = immortal_personality
	queen = {
 		name = "Nharmok"
		dynasty = "Sonevo"
		birth_date = 1211.1.1
		adm = 4
		dip = 3
		mil = 1
    }
	add_queen_personality = immortal_personality
	heir = {
 		name = "Shahaulec"
		monarch_name = "Shahaulec I"
		dynasty = "Sonevo"
		birth_date = 1253.1.1
		death_date = 1337.1.1
		claim = 100
		adm = 1
		dip = 1
		mil = 0
    }
	add_heir_personality = immortal_personality
}

1337.1.1 = {
	monarch = {
 		name = "Letan"
		dynasty = "Edreli"
		birth_date = 1302.1.1
		adm = 3
		dip = 3
		mil = 2
    }
	add_ruler_personality = immortal_personality
	queen = {
 		name = "Zeli"
		dynasty = "Edreli"
		birth_date = 1290.1.1
		adm = 0
		dip = 1
		mil = 1
		female = yes
    }
	add_queen_personality = immortal_personality
	heir = {
 		name = "Sonev"
		monarch_name = "Sonev I"
		dynasty = "Edreli"
		birth_date = 1335.1.1
		death_date = 1390.1.1
		claim = 100
		adm = 4
		dip = 0
		mil = 0
		female = yes
    }
	add_heir_personality = immortal_personality
}

1390.1.1 = {
	monarch = {
 		name = "Vredraudin"
		dynasty = "Edreli"
		birth_date = 1344.1.1
		adm = 4
		dip = 3
		mil = 1
    }
	add_ruler_personality = immortal_personality
	queen = {
 		name = "Lassoba"
		dynasty = "Edreli"
		birth_date = 1367.1.1
		adm = 0
		dip = 2
		mil = 0
		female = yes
    }
	add_queen_personality = immortal_personality
	heir = {
 		name = "Herlir"
		monarch_name = "Herlir I"
		dynasty = "Edreli"
		birth_date = 1389.1.1
		death_date = 1443.1.1
		claim = 100
		adm = 1
		dip = 5
		mil = 1
    }
	add_heir_personality = immortal_personality
}

1443.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Tudaarin"
		monarch_name = "Tudaarin I"
		dynasty = "Nelfaza"
		birth_date = 1441.1.1
		death_date = 1459.1.1
		claim = 100
		adm = 3
		dip = 0
		mil = 2
    }
}

1459.1.1 = {
	monarch = {
 		name = "Vaulic"
		dynasty = "Rathinda"
		birth_date = 1436.1.1
		adm = 5
		dip = 1
		mil = 5
    }
	add_ruler_personality = immortal_personality
	queen = {
 		name = "Linne"
		dynasty = "Rathinda"
		birth_date = 1441.1.1
		adm = 5
		dip = 4
		mil = 5
		female = yes
    }
	add_queen_personality = immortal_personality
	heir = {
 		name = "Lashavun"
		monarch_name = "Lashavun I"
		dynasty = "Rathinda"
		birth_date = 1448.1.1
		death_date = 1512.1.1
		claim = 100
		adm = 5
		dip = 1
		mil = 6
		female = yes
    }
	add_heir_personality = immortal_personality
}

1512.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Gaohen"
		monarch_name = "Gaohen I"
		dynasty = "Edremelli"
		birth_date = 1504.1.1
		death_date = 1522.1.1
		claim = 100
		adm = 0
		dip = 2
		mil = 0
    }
}

1522.1.1 = {
	monarch = {
 		name = "Eshebir"
		dynasty = "Invitte"
		birth_date = 1503.1.1
		adm = 3
		dip = 3
		mil = 3
		female = yes
    }
	add_ruler_personality = immortal_personality
	queen = {
 		name = "Vaulic"
		dynasty = "Invitte"
		birth_date = 1479.1.1
		adm = 6
		dip = 2
		mil = 2
    }
	add_queen_personality = immortal_personality
	heir = {
 		name = "Ranni"
		monarch_name = "Ranni I"
		dynasty = "Invitte"
		birth_date = 1515.1.1
		death_date = 1561.1.1
		claim = 100
		adm = 3
		dip = 0
		mil = 1
		female = yes
    }
	add_heir_personality = immortal_personality
}

1561.1.1 = {
	monarch = {
 		name = "Desylis"
		dynasty = "Enkeyile"
		birth_date = 1533.1.1
		adm = 6
		dip = 2
		mil = 3
    }
	add_ruler_personality = immortal_personality
	queen = {
 		name = "Mazan"
		dynasty = "Enkeyile"
		birth_date = 1533.1.1
		adm = 3
		dip = 0
		mil = 2
		female = yes
    }
	add_queen_personality = immortal_personality
	heir = {
 		name = "Haukos"
		monarch_name = "Haukos I"
		dynasty = "Enkeyile"
		birth_date = 1559.1.1
		death_date = 1642.1.1
		claim = 100
		adm = 0
		dip = 6
		mil = 1
    }
	add_heir_personality = immortal_personality
}

1642.1.1 = {
	monarch = {
 		name = "Ihrasek"
		dynasty = "Isnosa"
		birth_date = 1594.1.1
		adm = 6
		dip = 2
		mil = 1
    }
	add_ruler_personality = immortal_personality
}

1703.1.1 = {
	monarch = {
 		name = "Serrive"
		dynasty = "Gelmana"
		birth_date = 1659.1.1
		adm = 4
		dip = 1
		mil = 5
		female = yes
    }
	add_ruler_personality = immortal_personality
}

1757.1.1 = {
	monarch = {
 		name = "Nharmok"
		dynasty = "Ralmehay"
		birth_date = 1710.1.1
		adm = 3
		dip = 1
		mil = 2
    }
	add_ruler_personality = immortal_personality
}

1856.1.1 = {
	monarch = {
 		name = "Mothal"
		dynasty = "Irlanna"
		birth_date = 1805.1.1
		adm = 1
		dip = 0
		mil = 5
		female = yes
    }
	add_ruler_personality = immortal_personality
}

1944.1.1 = {
	monarch = {
 		name = "Vredraudin"
		dynasty = "Nelriza"
		birth_date = 1893.1.1
		adm = 6
		dip = 6
		mil = 2
    }
	add_ruler_personality = immortal_personality
	queen = {
 		name = "Homin"
		dynasty = "Nelriza"
		birth_date = 1916.1.1
		adm = 3
		dip = 5
		mil = 1
		female = yes
    }
	add_queen_personality = immortal_personality
}

1981.1.1 = {
	monarch = {
 		name = "Zerlec"
		dynasty = "Civali"
		birth_date = 1941.1.1
		adm = 1
		dip = 4
		mil = 4
    }
	add_ruler_personality = immortal_personality
	queen = {
 		name = "Zahoti"
		dynasty = "Civali"
		birth_date = 1954.1.1
		adm = 5
		dip = 2
		mil = 3
		female = yes
    }
	add_queen_personality = immortal_personality
	heir = {
 		name = "Mothal"
		monarch_name = "Mothal I"
		dynasty = "Civali"
		birth_date = 1979.1.1
		death_date = 2020.1.1
		claim = 100
		adm = 2
		dip = 1
		mil = 2
		female = yes
    }
	add_heir_personality = immortal_personality
}

2020.1.1 = {
	monarch = {
 		name = "Naanuzar"
		dynasty = "Edremelli"
		birth_date = 1996.1.1
		adm = 5
		dip = 2
		mil = 5
    }
	add_ruler_personality = immortal_personality
}

2067.1.1 = {
	monarch = {
 		name = "Linne"
		dynasty = "Nelfaza"
		birth_date = 2033.1.1
		adm = 3
		dip = 1
		mil = 1
		female = yes
    }
	add_ruler_personality = immortal_personality
}

2102.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Lassoba"
		monarch_name = "Lassoba I"
		dynasty = "Ivosi"
		birth_date = 2102.1.1
		death_date = 2120.1.1
		claim = 100
		adm = 4
		dip = 6
		mil = 6
		female = yes
    }
}

2120.1.1 = {
	monarch = {
 		name = "Hetok"
		dynasty = "Edremelli"
		birth_date = 2084.1.1
		adm = 3
		dip = 2
		mil = 0
    }
	add_ruler_personality = immortal_personality
	queen = {
 		name = "Dirrinuh"
		dynasty = "Edremelli"
		birth_date = 2069.1.1
		adm = 0
		dip = 0
		mil = 6
		female = yes
    }
	add_queen_personality = immortal_personality
}

2171.1.1 = {
	monarch = {
 		name = "Sarul"
		dynasty = "Linyiva"
		birth_date = 2127.1.1
		adm = 5
		dip = 0
		mil = 2
		female = yes
    }
	add_ruler_personality = immortal_personality
}

2248.1.1 = {
	monarch = {
 		name = "Halaanis"
		dynasty = "Larsemay"
		birth_date = 2223.1.1
		adm = 4
		dip = 6
		mil = 6
    }
	add_ruler_personality = immortal_personality
}

2322.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Haukos"
		monarch_name = "Haukos I"
		dynasty = "Gerlenno"
		birth_date = 2308.1.1
		death_date = 2326.1.1
		claim = 100
		adm = 3
		dip = 6
		mil = 0
    }
}

2326.1.1 = {
	monarch = {
 		name = "Lukaazon"
		dynasty = "Vothinda"
		birth_date = 2301.1.1
		adm = 3
		dip = 2
		mil = 0
    }
	add_ruler_personality = immortal_personality
	queen = {
 		name = "Isur"
		dynasty = "Vothinda"
		birth_date = 2297.1.1
		adm = 4
		dip = 3
		mil = 5
		female = yes
    }
	add_queen_personality = immortal_personality
	heir = {
 		name = "Nirdoc"
		monarch_name = "Nirdoc I"
		dynasty = "Vothinda"
		birth_date = 2319.1.1
		death_date = 2407.1.1
		claim = 100
		adm = 1
		dip = 1
		mil = 4
    }
	add_heir_personality = immortal_personality
}

2407.1.1 = {
	monarch = {
 		name = "Vrakir"
		dynasty = "Malefa"
		birth_date = 2377.1.1
		adm = 3
		dip = 3
		mil = 6
    }
	add_ruler_personality = immortal_personality
	queen = {
 		name = "Sovi"
		dynasty = "Malefa"
		birth_date = 2370.1.1
		adm = 0
		dip = 1
		mil = 5
		female = yes
    }
	add_queen_personality = immortal_personality
	heir = {
 		name = "Vallal"
		monarch_name = "Vallal I"
		dynasty = "Malefa"
		birth_date = 2393.1.1
		death_date = 2446.1.1
		claim = 100
		adm = 4
		dip = 0
		mil = 4
		female = yes
    }
	add_heir_personality = immortal_personality
}

2446.1.1 = {
	monarch = {
 		name = "Nhystareth"
		dynasty = "Melfezita"
		birth_date = 2426.1.1
		adm = 4
		dip = 3
		mil = 5
    }
	add_ruler_personality = immortal_personality
	queen = {
 		name = "Doshir"
		dynasty = "Melfezita"
		birth_date = 2413.1.1
		adm = 4
		dip = 0
		mil = 6
		female = yes
    }
	add_queen_personality = immortal_personality
	heir = {
 		name = "Ralreloc"
		monarch_name = "Ralreloc I"
		dynasty = "Melfezita"
		birth_date = 2441.1.1
		death_date = 2536.1.1
		claim = 100
		adm = 1
		dip = 5
		mil = 5
    }
	add_heir_personality = immortal_personality
}

