government = tribal
government_rank = 1
mercantilism = 1
technology_group = exiled_tg
religion = chimer_pantheon
primary_culture = ashlander_chimer
capital = 3807

54.1.1 = {
	monarch = {
 		name = "Malay"
		dynasty = "Ashummi-Ammus"
		birth_date = 7.1.1
		adm = 4
		dip = 4
		mil = 6
    }
}

105.1.1 = {
	monarch = {
 		name = "Berapli"
		dynasty = "Assardarainat"
		birth_date = 81.1.1
		adm = 0
		dip = 5
		mil = 1
		female = yes
    }
}

160.1.1 = {
	monarch = {
 		name = "Zanummu"
		dynasty = "Yansurnummu"
		birth_date = 124.1.1
		adm = 1
		dip = 2
		mil = 6
		female = yes
    }
}

202.1.1 = {
	monarch = {
 		name = "Shargon"
		dynasty = "Sanammasour"
		birth_date = 156.1.1
		adm = 0
		dip = 6
		mil = 6
    }
}

282.1.1 = {
	monarch = {
 		name = "Salattanat"
		dynasty = "Odin-Ahhe"
		birth_date = 250.1.1
		adm = 5
		dip = 5
		mil = 3
    }
}

341.1.1 = {
	monarch = {
 		name = "Kanat"
		dynasty = "Dunsamsi"
		birth_date = 289.1.1
		adm = 4
		dip = 4
		mil = 6
    }
}

392.1.1 = {
	monarch = {
 		name = "Asha-Ammu"
		dynasty = "Atinsabia"
		birth_date = 363.1.1
		adm = 2
		dip = 3
		mil = 3
    }
}

486.1.1 = {
	monarch = {
 		name = "Urshamusa"
		dynasty = "Kaushad"
		birth_date = 439.1.1
		adm = 0
		dip = 3
		mil = 0
		female = yes
    }
}

578.1.1 = {
	monarch = {
 		name = "Salattanat"
		dynasty = "Atinsabia"
		birth_date = 537.1.1
		adm = 5
		dip = 2
		mil = 3
    }
}

676.1.1 = {
	monarch = {
 		name = "Ilasour"
		dynasty = "Ashar-Dad"
		birth_date = 650.1.1
		adm = 3
		dip = 1
		mil = 0
    }
}

765.1.1 = {
	monarch = {
 		name = "Asharapli"
		dynasty = "Assarrapanat"
		birth_date = 718.1.1
		adm = 5
		dip = 2
		mil = 2
		female = yes
    }
}

855.1.1 = {
	monarch = {
 		name = "Yapal"
		dynasty = "Assarnibani"
		birth_date = 803.1.1
		adm = 4
		dip = 1
		mil = 5
    }
}

931.1.1 = {
	monarch = {
 		name = "Assamanu"
		dynasty = "Assutlanipal"
		birth_date = 900.1.1
		adm = 2
		dip = 1
		mil = 2
		female = yes
    }
}

1008.1.1 = {
	monarch = {
 		name = "Vabbar"
		dynasty = "Dudnebisun"
		birth_date = 990.1.1
		adm = 0
		dip = 0
		mil = 5
		female = yes
    }
}

1066.1.1 = {
	monarch = {
 		name = "Rawia"
		dynasty = "Massitisun"
		birth_date = 1029.1.1
		adm = 5
		dip = 6
		mil = 2
		female = yes
    }
}

1155.1.1 = {
	monarch = {
 		name = "Kanud"
		dynasty = "Assintashiran"
		birth_date = 1129.1.1
		adm = 4
		dip = 5
		mil = 6
    }
}

1234.1.1 = {
	monarch = {
 		name = "Assamanu"
		dynasty = "Odirnapal"
		birth_date = 1200.1.1
		adm = 2
		dip = 5
		mil = 2
		female = yes
    }
}

1285.1.1 = {
	monarch = {
 		name = "Vabbar"
		dynasty = "Shishara"
		birth_date = 1259.1.1
		adm = 0
		dip = 4
		mil = 6
		female = yes
    }
}

1353.1.1 = {
	monarch = {
 		name = "Salmat"
		dynasty = "Maliiran"
		birth_date = 1326.1.1
		adm = 2
		dip = 5
		mil = 1
    }
}

1431.1.1 = {
	monarch = {
 		name = "Kanud"
		dynasty = "Asharapli"
		birth_date = 1401.1.1
		adm = 0
		dip = 4
		mil = 4
    }
}

1498.1.1 = {
	monarch = {
 		name = "Sargon"
		dynasty = "Saharnatturapli"
		birth_date = 1450.1.1
		adm = 5
		dip = 3
		mil = 1
    }
}

1580.1.1 = {
	monarch = {
 		name = "Maela"
		dynasty = "Ashurnasaddas"
		birth_date = 1538.1.1
		adm = 4
		dip = 3
		mil = 5
		female = yes
    }
}

1632.1.1 = {
	monarch = {
 		name = "Assi"
		dynasty = "Assumanallit"
		birth_date = 1587.1.1
		adm = 2
		dip = 2
		mil = 1
		female = yes
    }
}

1724.1.1 = {
	monarch = {
 		name = "Zabamund"
		dynasty = "Enturnabaelul"
		birth_date = 1673.1.1
		adm = 0
		dip = 1
		mil = 5
    }
}

1813.1.1 = {
	monarch = {
 		name = "Sargon"
		dynasty = "Ashumallit"
		birth_date = 1768.1.1
		adm = 5
		dip = 0
		mil = 2
    }
}

1865.1.1 = {
	monarch = {
 		name = "Kaushad"
		dynasty = "Sanammasour"
		birth_date = 1827.1.1
		adm = 4
		dip = 6
		mil = 5
    }
}

1917.1.1 = {
	monarch = {
 		name = "Assi"
		dynasty = "Ududnabia"
		birth_date = 1890.1.1
		adm = 6
		dip = 1
		mil = 0
		female = yes
    }
}

1993.1.1 = {
	monarch = {
 		name = "Yan-Ahhe"
		dynasty = "Benamamat"
		birth_date = 1947.1.1
		adm = 4
		dip = 0
		mil = 3
		female = yes
    }
}

2034.1.1 = {
	monarch = {
 		name = "Assirari"
		dynasty = "Abalkala"
		birth_date = 2000.1.1
		adm = 2
		dip = 6
		mil = 0
		female = yes
    }
}

2083.1.1 = {
	monarch = {
 		name = "Zairan"
		dynasty = "Kil"
		birth_date = 2046.1.1
		adm = 0
		dip = 5
		mil = 4
    }
}

2177.1.1 = {
	monarch = {
 		name = "Selk"
		dynasty = "Assaplit"
		birth_date = 2126.1.1
		adm = 6
		dip = 5
		mil = 0
    }
}

2260.1.1 = {
	monarch = {
 		name = "Maeli"
		dynasty = "Atinsabia"
		birth_date = 2209.1.1
		adm = 4
		dip = 4
		mil = 4
		female = yes
    }
}

2359.1.1 = {
	monarch = {
 		name = "Assamanut"
		dynasty = "Erushara"
		birth_date = 2317.1.1
		adm = 2
		dip = 3
		mil = 1
    }
}

2421.1.1 = {
	monarch = {
 		name = "Yanibi"
		dynasty = "Ashalkimallit"
		birth_date = 2376.1.1
		adm = 0
		dip = 2
		mil = 4
		female = yes
    }
}

2499.1.1 = {
	monarch = {
 		name = "Santinti"
		dynasty = "Malman-Ammu"
		birth_date = 2459.1.1
		adm = 2
		dip = 3
		mil = 6
		female = yes
    }
}

