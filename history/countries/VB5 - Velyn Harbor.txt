government = republic
government_rank = 1
mercantilism = 1
technology_group = elven_tg
religion = bosmer_pantheon
primary_culture = bosmer
capital = 882

54.1.1 = {
	monarch = {
 		name = "Mongoth"
		dynasty = "Or'Niveneth"
		birth_date = 22.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

116.1.1 = {
	monarch = {
 		name = "Goralgor"
		dynasty = "Or'Wasten"
		birth_date = 90.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

178.1.1 = {
	monarch = {
 		name = "Nedtharond"
		dynasty = "Ur'Eldenroot"
		birth_date = 150.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

258.1.1 = {
	monarch = {
 		name = "Gwilgoth"
		dynasty = "Ur'Black"
		birth_date = 230.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

344.1.1 = {
	monarch = {
 		name = "Engiroth"
		dynasty = "Ur'Dornlock"
		birth_date = 308.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

389.1.1 = {
	monarch = {
 		name = "Aldor"
		dynasty = "Ca'Hasaael"
		birth_date = 344.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

458.1.1 = {
	monarch = {
 		name = "Nednor"
		dynasty = "Ur'Bothenandriah"
		birth_date = 419.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

537.1.1 = {
	monarch = {
 		name = "Gwarolgor"
		dynasty = "Or'Nileneth"
		birth_date = 502.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

590.1.1 = {
	monarch = {
 		name = "Endrothir"
		dynasty = "Or'Thamnel"
		birth_date = 553.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

664.1.1 = {
	monarch = {
 		name = "Agwodir"
		dynasty = "Ur'Cori"
		birth_date = 633.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

736.1.1 = {
	monarch = {
 		name = "Eranas"
		dynasty = "Ur'Ebon"
		birth_date = 706.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

817.1.1 = {
	monarch = {
 		name = "Angeniel"
		dynasty = "Ca'Mandae"
		birth_date = 792.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

887.1.1 = {
	monarch = {
 		name = "Neronnir"
		dynasty = "Or'Oakenwood"
		birth_date = 834.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

934.1.1 = {
	monarch = {
 		name = "Gwinithor"
		dynasty = "Ca'Lynpar"
		birth_date = 915.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

974.1.1 = {
	monarch = {
 		name = "Erilthil"
		dynasty = "Or'Seledra"
		birth_date = 926.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1022.1.1 = {
	monarch = {
 		name = "Fironthor"
		dynasty = "Ca'Ivybranch"
		birth_date = 980.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1101.1.1 = {
	monarch = {
 		name = "Cirtor"
		dynasty = "		"
		birth_date = 1067.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1176.1.1 = {
	monarch = {
 		name = "Elhalem"
		dynasty = "Or'Rosebranch"
		birth_date = 1139.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1269.1.1 = {
	monarch = {
 		name = "Adwien"
		dynasty = "Or'Mossmire"
		birth_date = 1237.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1313.1.1 = {
	monarch = {
 		name = "Nieth"
		dynasty = "Ca'Hynhruviel"
		birth_date = 1262.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1412.1.1 = {
	monarch = {
 		name = "Glenora"
		dynasty = "Or'Parndra"
		birth_date = 1360.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1497.1.1 = {
	monarch = {
 		name = "Elsenia"
		dynasty = "Ur'Dra'bul"
		birth_date = 1477.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1590.1.1 = {
	monarch = {
 		name = "Adamir"
		dynasty = "Ca'Lieval"
		birth_date = 1569.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1649.1.1 = {
	monarch = {
 		name = "Nienornith"
		dynasty = "Ur'Bluewind"
		birth_date = 1599.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1725.1.1 = {
	monarch = {
 		name = "Agwodir"
		dynasty = "Or'Silvenar"
		birth_date = 1672.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1800.1.1 = {
	monarch = {
 		name = "Nedaegaer"
		dynasty = "Or'Telael"
		birth_date = 1775.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1896.1.1 = {
	monarch = {
 		name = "Gwagoth"
		dynasty = "Ur'Dra'bul"
		birth_date = 1877.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1990.1.1 = {
	monarch = {
 		name = "Endregail"
		dynasty = "Or'Thaeicia"
		birth_date = 1938.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2046.1.1 = {
	monarch = {
 		name = "Aglion"
		dynasty = "Ur'Ciinthil"
		birth_date = 2016.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2129.1.1 = {
	monarch = {
 		name = "Nallelfin"
		dynasty = "Or'Telael"
		birth_date = 2092.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2210.1.1 = {
	monarch = {
 		name = "Gwael"
		dynasty = "Ca'Kirshiron"
		birth_date = 2187.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2249.1.1 = {
	monarch = {
 		name = "Enaring"
		dynasty = "Ur'Balfstone"
		birth_date = 2206.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2333.1.1 = {
	monarch = {
 		name = "Gwingeval"
		dynasty = "Or'Niveneth"
		birth_date = 2298.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2379.1.1 = {
	monarch = {
 		name = "Enthoras"
		dynasty = "Ur'Eagle"
		birth_date = 2352.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2422.1.1 = {
	monarch = {
 		name = "Andrilion"
		dynasty = "		"
		birth_date = 2393.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2457.1.1 = {
	monarch = {
 		name = "Nistel"
		dynasty = "Ur'Black"
		birth_date = 2430.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

