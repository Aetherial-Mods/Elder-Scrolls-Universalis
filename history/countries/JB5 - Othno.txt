government = monarchy
government_rank = 1
mercantilism = 1
technology_group = thousand_tg
religion = tang_mo_pantheon
primary_culture = tangmo
capital = 3615

54.1.1 = {
	monarch = {
 		name = "Buttash"
		dynasty = "Dhusvatyurman"
		birth_date = 20.1.1
		adm = 5
		dip = 5
		mil = 5
    }
	queen = {
 		name = "Sipindante"
		dynasty = "Dhusvatyurman"
		birth_date = 31.1.1
		adm = 5
		dip = 1
		mil = 5
		female = yes
    }
	heir = {
 		name = "Dhakuna"
		monarch_name = "Dhakuna I"
		dynasty = "Dhusvatyurman"
		birth_date = 41.1.1
		death_date = 109.1.1
		claim = 100
		adm = 2
		dip = 0
		mil = 4
    }
}

109.1.1 = {
	monarch = {
 		name = "Mijnuvos"
		dynasty = "Ubaptish"
		birth_date = 79.1.1
		adm = 4
		dip = 1
		mil = 0
    }
	queen = {
 		name = "Sipindante"
		dynasty = "Ubaptish"
		birth_date = 83.1.1
		adm = 1
		dip = 0
		mil = 6
		female = yes
    }
	heir = {
 		name = "Ksijnay"
		monarch_name = "Ksijnay I"
		dynasty = "Ubaptish"
		birth_date = 98.1.1
		death_date = 197.1.1
		claim = 100
		adm = 5
		dip = 5
		mil = 4
    }
}

197.1.1 = {
	monarch = {
 		name = "Asvayathyos"
		dynasty = "Guhmas"
		birth_date = 174.1.1
		adm = 1
		dip = 0
		mil = 0
    }
}

255.1.1 = {
	monarch = {
 		name = "Yulmint"
		dynasty = "Ubaptish"
		birth_date = 203.1.1
		adm = 6
		dip = 6
		mil = 3
    }
}

337.1.1 = {
	monarch = {
 		name = "Singisna"
		dynasty = "Prilger"
		birth_date = 310.1.1
		adm = 1
		dip = 0
		mil = 5
		female = yes
    }
}

390.1.1 = {
	monarch = {
 		name = "Suju"
		dynasty = "Dhakuna"
		birth_date = 372.1.1
		adm = 6
		dip = 6
		mil = 2
    }
}

466.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Ument"
		monarch_name = "Ument I"
		dynasty = "Sarganusthash"
		birth_date = 464.1.1
		death_date = 482.1.1
		claim = 100
		adm = 3
		dip = 1
		mil = 2
    }
}

482.1.1 = {
	monarch = {
 		name = "Biskus"
		dynasty = "Mahmesthan"
		birth_date = 430.1.1
		adm = 5
		dip = 3
		mil = 3
    }
	queen = {
 		name = "Epi"
		dynasty = "Mahmesthan"
		birth_date = 462.1.1
		adm = 1
		dip = 4
		mil = 6
		female = yes
    }
	heir = {
 		name = "Gubhrorhabad"
		monarch_name = "Gubhrorhabad I"
		dynasty = "Mahmesthan"
		birth_date = 473.1.1
		death_date = 534.1.1
		claim = 100
		adm = 5
		dip = 3
		mil = 5
    }
}

534.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Bhidru"
		monarch_name = "Bhidru I"
		dynasty = "Mijnuvos"
		birth_date = 526.1.1
		death_date = 544.1.1
		claim = 100
		adm = 3
		dip = 2
		mil = 1
		female = yes
    }
}

544.1.1 = {
	monarch = {
 		name = "Nurposhrasthay"
		dynasty = "Dhajnay"
		birth_date = 506.1.1
		adm = 2
		dip = 5
		mil = 1
    }
}

595.1.1 = {
	monarch = {
 		name = "Prithu"
		dynasty = "Tebhrota"
		birth_date = 543.1.1
		adm = 1
		dip = 4
		mil = 5
    }
}

667.1.1 = {
	monarch = {
 		name = "Korpun"
		dynasty = "Prabhasthont"
		birth_date = 629.1.1
		adm = 6
		dip = 4
		mil = 2
    }
}

755.1.1 = {
	monarch = {
 		name = "Ukayud"
		dynasty = "Kalgatyasthor"
		birth_date = 728.1.1
		adm = 4
		dip = 3
		mil = 5
    }
}

853.1.1 = {
	monarch = {
 		name = "Bubundayo"
		dynasty = "Oskirtunghid"
		birth_date = 809.1.1
		adm = 2
		dip = 2
		mil = 2
		female = yes
    }
	queen = {
 		name = "Prilger"
		dynasty = "Oskirtunghid"
		birth_date = 821.1.1
		adm = 6
		dip = 1
		mil = 1
    }
	heir = {
 		name = "Guhmas"
		monarch_name = "Guhmas I"
		dynasty = "Oskirtunghid"
		birth_date = 849.1.1
		death_date = 926.1.1
		claim = 100
		adm = 3
		dip = 6
		mil = 0
    }
}

926.1.1 = {
	monarch = {
 		name = "Korpun"
		dynasty = "Ksushmuhunt"
		birth_date = 901.1.1
		adm = 6
		dip = 0
		mil = 2
    }
	queen = {
 		name = "Batha"
		dynasty = "Ksushmuhunt"
		birth_date = 876.1.1
		adm = 3
		dip = 6
		mil = 1
		female = yes
    }
	heir = {
 		name = "Bgokushun"
		monarch_name = "Bgokushun I"
		dynasty = "Ksushmuhunt"
		birth_date = 915.1.1
		death_date = 974.1.1
		claim = 100
		adm = 0
		dip = 5
		mil = 0
    }
}

974.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Ipatra"
		monarch_name = "Ipatra I"
		dynasty = "Jorpet"
		birth_date = 968.1.1
		death_date = 986.1.1
		claim = 100
		adm = 5
		dip = 4
		mil = 4
		female = yes
    }
}

986.1.1 = {
	monarch = {
 		name = "Tutturmet"
		dynasty = "Ukayud"
		birth_date = 953.1.1
		adm = 4
		dip = 0
		mil = 4
    }
	queen = {
 		name = "Sipindante"
		dynasty = "Ukayud"
		birth_date = 952.1.1
		adm = 1
		dip = 6
		mil = 3
		female = yes
    }
}

1038.1.1 = {
	monarch = {
 		name = "Udrarmush"
		dynasty = "Yulmint"
		birth_date = 985.1.1
		adm = 6
		dip = 5
		mil = 0
    }
}

1079.1.1 = {
	monarch = {
 		name = "Epi"
		dynasty = "Jorpet"
		birth_date = 1041.1.1
		adm = 5
		dip = 4
		mil = 3
		female = yes
    }
}

1127.1.1 = {
	monarch = {
 		name = "Argurhoy"
		dynasty = "Mahmesthan"
		birth_date = 1090.1.1
		adm = 3
		dip = 3
		mil = 0
    }
}

1183.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Suju"
		monarch_name = "Suju I"
		dynasty = "Rundod"
		birth_date = 1174.1.1
		death_date = 1192.1.1
		claim = 100
		adm = 4
		dip = 0
		mil = 5
    }
}

1192.1.1 = {
	monarch = {
 		name = "Dedhumasma"
		dynasty = "Debirhut"
		birth_date = 1153.1.1
		adm = 6
		dip = 2
		mil = 0
		female = yes
    }
}

1291.1.1 = {
	monarch = {
 		name = "Bhanga"
		dynasty = "Nibhrunt"
		birth_date = 1269.1.1
		adm = 5
		dip = 1
		mil = 4
		female = yes
    }
	queen = {
 		name = "Prithu"
		dynasty = "Nibhrunt"
		birth_date = 1262.1.1
		adm = 2
		dip = 6
		mil = 3
    }
	heir = {
 		name = "Kamayi"
		monarch_name = "Kamayi I"
		dynasty = "Nibhrunt"
		birth_date = 1280.1.1
		death_date = 1375.1.1
		claim = 100
		adm = 3
		dip = 0
		mil = 1
		female = yes
    }
}

1375.1.1 = {
	monarch = {
 		name = "Elas"
		dynasty = "Yulmint"
		birth_date = 1337.1.1
		adm = 2
		dip = 3
		mil = 1
    }
	queen = {
 		name = "Nattanismo"
		dynasty = "Yulmint"
		birth_date = 1335.1.1
		adm = 6
		dip = 2
		mil = 0
		female = yes
    }
	heir = {
 		name = "Jushmad"
		monarch_name = "Jushmad I"
		dynasty = "Yulmint"
		birth_date = 1375.1.1
		death_date = 1468.1.1
		claim = 100
		adm = 3
		dip = 1
		mil = 6
    }
}

1468.1.1 = {
	monarch = {
 		name = "Nurposhrasthay"
		dynasty = "Biskus"
		birth_date = 1432.1.1
		adm = 6
		dip = 2
		mil = 1
    }
}

1558.1.1 = {
	monarch = {
 		name = "Adi"
		dynasty = "Cetuhuy"
		birth_date = 1506.1.1
		adm = 4
		dip = 1
		mil = 5
		female = yes
    }
	queen = {
 		name = "Cetuhuy"
		dynasty = "Cetuhuy"
		birth_date = 1536.1.1
		adm = 1
		dip = 0
		mil = 4
    }
	heir = {
 		name = "Bhingasmi"
		monarch_name = "Bhingasmi I"
		dynasty = "Cetuhuy"
		birth_date = 1554.1.1
		death_date = 1597.1.1
		claim = 100
		adm = 5
		dip = 5
		mil = 3
		female = yes
    }
}

1597.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Bhilyusho"
		monarch_name = "Bhilyusho I"
		dynasty = "Kibhunt"
		birth_date = 1587.1.1
		death_date = 1605.1.1
		claim = 100
		adm = 3
		dip = 4
		mil = 6
		female = yes
    }
}

1605.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Argarhabush"
		monarch_name = "Argarhabush I"
		dynasty = "Dacurmivor"
		birth_date = 1591.1.1
		death_date = 1609.1.1
		claim = 100
		adm = 1
		dip = 4
		mil = 3
    }
}

1609.1.1 = {
	monarch = {
 		name = "Tutturmet"
		dynasty = "Rundod"
		birth_date = 1571.1.1
		adm = 1
		dip = 0
		mil = 3
    }
}

1644.1.1 = {
	monarch = {
 		name = "Ata"
		dynasty = "Korpun"
		birth_date = 1611.1.1
		adm = 6
		dip = 6
		mil = 0
		female = yes
    }
	queen = {
 		name = "Brirpeyir"
		dynasty = "Korpun"
		birth_date = 1617.1.1
		adm = 3
		dip = 5
		mil = 6
    }
}

1716.1.1 = {
	monarch = {
 		name = "Aju"
		dynasty = "Vushmus"
		birth_date = 1676.1.1
		adm = 4
		dip = 2
		mil = 4
    }
}

1791.1.1 = {
	monarch = {
 		name = "Vakrubutyan"
		dynasty = "Bana"
		birth_date = 1767.1.1
		adm = 2
		dip = 1
		mil = 1
    }
}

1858.1.1 = {
	monarch = {
 		name = "Udragush"
		dynasty = "Sarganusthash"
		birth_date = 1825.1.1
		adm = 0
		dip = 0
		mil = 4
    }
}

1930.1.1 = {
	monarch = {
 		name = "Vali"
		dynasty = "Udrarmush"
		birth_date = 1889.1.1
		adm = 5
		dip = 6
		mil = 1
		female = yes
    }
}

1987.1.1 = {
	monarch = {
 		name = "Bilyuti"
		dynasty = "Debirhut"
		birth_date = 1936.1.1
		adm = 4
		dip = 6
		mil = 5
		female = yes
    }
}

2077.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Galanirmer"
		monarch_name = "Galanirmer I"
		dynasty = "Puskarmosh"
		birth_date = 2068.1.1
		death_date = 2086.1.1
		claim = 100
		adm = 4
		dip = 3
		mil = 2
    }
}

2086.1.1 = {
	monarch = {
 		name = "Udragush"
		dynasty = "Ravarhid"
		birth_date = 2050.1.1
		adm = 0
		dip = 4
		mil = 5
    }
	queen = {
 		name = "Dedhumasma"
		dynasty = "Ravarhid"
		birth_date = 2066.1.1
		adm = 4
		dip = 3
		mil = 4
		female = yes
    }
	heir = {
 		name = "Ksacish"
		monarch_name = "Ksacish I"
		dynasty = "Ravarhid"
		birth_date = 2084.1.1
		death_date = 2154.1.1
		claim = 100
		adm = 1
		dip = 1
		mil = 3
    }
}

2154.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Sanupo"
		monarch_name = "Sanupo I"
		dynasty = "Biskus"
		birth_date = 2141.1.1
		death_date = 2159.1.1
		claim = 100
		adm = 6
		dip = 0
		mil = 6
		female = yes
    }
}

2159.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Rilgunt"
		monarch_name = "Rilgunt I"
		dynasty = "Tutturmet"
		birth_date = 2144.1.1
		death_date = 2162.1.1
		claim = 100
		adm = 4
		dip = 0
		mil = 3
    }
}

2162.1.1 = {
	monarch = {
 		name = "Bhingasmi"
		dynasty = "Dhajnay"
		birth_date = 2122.1.1
		adm = 4
		dip = 3
		mil = 3
		female = yes
    }
	queen = {
 		name = "Ebhuthyutod"
		dynasty = "Dhajnay"
		birth_date = 2126.1.1
		adm = 1
		dip = 1
		mil = 2
    }
	heir = {
 		name = "Kibhunt"
		monarch_name = "Kibhunt I"
		dynasty = "Dhajnay"
		birth_date = 2148.1.1
		death_date = 2247.1.1
		claim = 100
		adm = 4
		dip = 0
		mil = 1
    }
}

2247.1.1 = {
	monarch = {
 		name = "Bakarmortad"
		dynasty = "Bushmava"
		birth_date = 2204.1.1
		adm = 0
		dip = 1
		mil = 4
    }
	queen = {
 		name = "Ajaya"
		dynasty = "Bushmava"
		birth_date = 2202.1.1
		adm = 4
		dip = 0
		mil = 3
		female = yes
    }
	heir = {
 		name = "Rilgunt"
		monarch_name = "Rilgunt I"
		dynasty = "Bushmava"
		birth_date = 2246.1.1
		death_date = 2294.1.1
		claim = 100
		adm = 1
		dip = 5
		mil = 2
    }
}

2294.1.1 = {
	monarch = {
 		name = "Yusvushin"
		dynasty = "Suju"
		birth_date = 2252.1.1
		adm = 6
		dip = 5
		mil = 5
    }
}

2376.1.1 = {
	monarch = {
 		name = "Oskirtunghid"
		dynasty = "Bgubhrakun"
		birth_date = 2328.1.1
		adm = 4
		dip = 4
		mil = 2
    }
}

2472.1.1 = {
	monarch = {
 		name = "Vushmus"
		dynasty = "Nidruthyont"
		birth_date = 2451.1.1
		adm = 3
		dip = 3
		mil = 5
    }
	queen = {
 		name = "Batha"
		dynasty = "Nidruthyont"
		birth_date = 2424.1.1
		adm = 0
		dip = 2
		mil = 4
		female = yes
    }
}

