government = theocracy
government_rank = 1
mercantilism = 1
technology_group = akavir_tg
religion = po_tun_pantheon
primary_culture = po_tun
capital = 651

54.1.1 = {
	monarch = {
 		name = "Lundrus"
		dynasty = "Tuwurtus'la"
		birth_date = 17.1.1
		adm = 6
		dip = 6
		mil = 6
    }
}

110.1.1 = {
	monarch = {
 		name = "Nontrae"
		dynasty = "Gon'ri"
		birth_date = 82.1.1
		adm = 1
		dip = 0
		mil = 0
		female = yes
    }
}

147.1.1 = {
	monarch = {
 		name = "Cin"
		dynasty = "Rolum'ri"
		birth_date = 119.1.1
		adm = 6
		dip = 6
		mil = 4
		female = yes
    }
}

217.1.1 = {
	monarch = {
 		name = "Terterul"
		dynasty = "Dume'la"
		birth_date = 198.1.1
		adm = 5
		dip = 6
		mil = 1
    }
}

252.1.1 = {
	monarch = {
 		name = "Lun"
		dynasty = "Milin'wo"
		birth_date = 204.1.1
		adm = 3
		dip = 5
		mil = 4
    }
}

305.1.1 = {
	monarch = {
 		name = "Puhak"
		dynasty = "Thom'ri"
		birth_date = 284.1.1
		adm = 4
		dip = 2
		mil = 2
    }
}

397.1.1 = {
	monarch = {
 		name = "Krakug"
		dynasty = "Susdevu'la"
		birth_date = 359.1.1
		adm = 1
		dip = 4
		mil = 2
    }
}

494.1.1 = {
	monarch = {
 		name = "Nuyul"
		dynasty = "Tus'su"
		birth_date = 468.1.1
		adm = 3
		dip = 5
		mil = 4
		female = yes
    }
}

529.1.1 = {
	monarch = {
 		name = "Rolum"
		dynasty = "Dicnusthak'wo"
		birth_date = 499.1.1
		adm = 1
		dip = 4
		mil = 0
    }
}

613.1.1 = {
	monarch = {
 		name = "Duhlan"
		dynasty = "Qevor'wo"
		birth_date = 587.1.1
		adm = 4
		dip = 6
		mil = 3
    }
}

668.1.1 = {
	monarch = {
 		name = "Sok"
		dynasty = "Shreyl'wo"
		birth_date = 620.1.1
		adm = 2
		dip = 5
		mil = 6
    }
}

759.1.1 = {
	monarch = {
 		name = "Luttesso"
		dynasty = "Lis'wo"
		birth_date = 734.1.1
		adm = 0
		dip = 4
		mil = 3
		female = yes
    }
}

811.1.1 = {
	monarch = {
 		name = "Dowyandu"
		dynasty = "Krer'ri"
		birth_date = 779.1.1
		adm = 2
		dip = 5
		mil = 5
    }
}

904.1.1 = {
	monarch = {
 		name = "Lisurr"
		dynasty = "So'la"
		birth_date = 870.1.1
		adm = 0
		dip = 5
		mil = 1
		female = yes
    }
}

980.1.1 = {
	monarch = {
 		name = "Ti"
		dynasty = "Dalnog'su"
		birth_date = 952.1.1
		adm = 5
		dip = 4
		mil = 5
    }
}

1049.1.1 = {
	monarch = {
 		name = "Tho"
		dynasty = "Krakug'ri"
		birth_date = 1003.1.1
		adm = 4
		dip = 3
		mil = 1
    }
}

1134.1.1 = {
	monarch = {
 		name = "Bororr"
		dynasty = "For'la"
		birth_date = 1113.1.1
		adm = 2
		dip = 2
		mil = 5
		female = yes
    }
}

1204.1.1 = {
	monarch = {
 		name = "Piruhus"
		dynasty = "Kruyd'ri"
		birth_date = 1180.1.1
		adm = 0
		dip = 2
		mil = 2
    }
}

1240.1.1 = {
	monarch = {
 		name = "Ti"
		dynasty = "Tosdul'wo"
		birth_date = 1203.1.1
		adm = 5
		dip = 1
		mil = 5
    }
}

1286.1.1 = {
	monarch = {
 		name = "Therr"
		dynasty = "Furesses'la"
		birth_date = 1262.1.1
		adm = 6
		dip = 5
		mil = 3
		female = yes
    }
}

1342.1.1 = {
	monarch = {
 		name = "Nontrae"
		dynasty = "Chadhak'su"
		birth_date = 1320.1.1
		adm = 4
		dip = 4
		mil = 0
		female = yes
    }
}

1381.1.1 = {
	monarch = {
 		name = "Cin"
		dynasty = "Rilu'wo"
		birth_date = 1363.1.1
		adm = 3
		dip = 3
		mil = 4
		female = yes
    }
}

1449.1.1 = {
	monarch = {
 		name = "Fiandok"
		dynasty = "Ramoz'ri"
		birth_date = 1424.1.1
		adm = 1
		dip = 2
		mil = 0
    }
}

1519.1.1 = {
	monarch = {
 		name = "Rozzeg"
		dynasty = "Fiyycnes'la"
		birth_date = 1470.1.1
		adm = 3
		dip = 4
		mil = 2
    }
}

1562.1.1 = {
	monarch = {
 		name = "Furesses"
		dynasty = "Dis'wo"
		birth_date = 1524.1.1
		adm = 1
		dip = 3
		mil = 5
		female = yes
    }
}

1644.1.1 = {
	monarch = {
 		name = "Chuhleyd"
		dynasty = "Bororr'la"
		birth_date = 1618.1.1
		adm = 6
		dip = 2
		mil = 2
    }
}

1682.1.1 = {
	monarch = {
 		name = "Qadun"
		dynasty = "Sorove'su"
		birth_date = 1655.1.1
		adm = 5
		dip = 1
		mil = 6
    }
}

1780.1.1 = {
	monarch = {
 		name = "Ciar"
		dynasty = "Thur'su"
		birth_date = 1739.1.1
		adm = 3
		dip = 0
		mil = 2
		female = yes
    }
}

1862.1.1 = {
	monarch = {
 		name = "Neta"
		dynasty = "Sen'ri"
		birth_date = 1830.1.1
		adm = 4
		dip = 5
		mil = 0
		female = yes
    }
}

1935.1.1 = {
	monarch = {
 		name = "Sok"
		dynasty = "Qusoky'la"
		birth_date = 1892.1.1
		adm = 2
		dip = 4
		mil = 4
    }
}

2018.1.1 = {
	monarch = {
 		name = "Luttesso"
		dynasty = "Kaz'ri"
		birth_date = 1965.1.1
		adm = 0
		dip = 3
		mil = 1
		female = yes
    }
}

2063.1.1 = {
	monarch = {
 		name = "Dowyandu"
		dynasty = "Den'su"
		birth_date = 2014.1.1
		adm = 5
		dip = 2
		mil = 4
    }
}

2122.1.1 = {
	monarch = {
 		name = "Sen"
		dynasty = "Pihya'ri"
		birth_date = 2099.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2169.1.1 = {
	monarch = {
 		name = "Sentyam"
		dynasty = "Kontessoor'su"
		birth_date = 2128.1.1
		adm = 2
		dip = 1
		mil = 5
		female = yes
    }
}

2252.1.1 = {
	monarch = {
 		name = "Luttesso"
		dynasty = "Sok'ri"
		birth_date = 2209.1.1
		adm = 4
		dip = 2
		mil = 6
		female = yes
    }
}

2289.1.1 = {
	monarch = {
 		name = "Dowyandu"
		dynasty = "Par'ri"
		birth_date = 2263.1.1
		adm = 2
		dip = 1
		mil = 3
    }
}

2343.1.1 = {
	monarch = {
 		name = "Piruhus"
		dynasty = "Tus'su"
		birth_date = 2310.1.1
		adm = 0
		dip = 0
		mil = 6
    }
}

2411.1.1 = {
	monarch = {
 		name = "Kil"
		dynasty = "Puhak'su"
		birth_date = 2377.1.1
		adm = 5
		dip = 6
		mil = 3
		female = yes
    }
}

2470.1.1 = {
	monarch = {
 		name = "Delolcok"
		dynasty = "Kil'wo"
		birth_date = 2429.1.1
		adm = 4
		dip = 6
		mil = 0
		female = yes
    }
}

