government = native
government_rank = 1
mercantilism = 1
technology_group = daedric_tg
religion = mephala_cult
primary_culture = arachnid
capital = 4252

54.1.1 = {
	monarch = {
 		name = "Lhievho"
		dynasty = "Roq'Klirneesh"
		birth_date = 7.1.1
		adm = 3
		dip = 2
		mil = 4
		female = yes
    }
}

95.1.1 = {
	monarch = {
 		name = "Zhiacinqab"
		dynasty = "Roq'Rashra"
		birth_date = 44.1.1
		adm = 2
		dip = 1
		mil = 1
    }
}

147.1.1 = {
	monarch = {
 		name = "Aq'sheelie"
		dynasty = "Roq'Anqid"
		birth_date = 118.1.1
		adm = 0
		dip = 0
		mil = 4
		female = yes
    }
}

230.1.1 = {
	monarch = {
 		name = "Nakot"
		dynasty = "Roq'Sairhoth'iel"
		birth_date = 188.1.1
		adm = 5
		dip = 6
		mil = 1
    }
}

321.1.1 = {
	monarch = {
 		name = "Qeenqed"
		dynasty = "Roq'Chishre"
		birth_date = 283.1.1
		adm = 3
		dip = 5
		mil = 5
    }
}

420.1.1 = {
	monarch = {
 		name = "Qithesh"
		dynasty = "Roq'Shere"
		birth_date = 388.1.1
		adm = 2
		dip = 5
		mil = 1
		female = yes
    }
}

485.1.1 = {
	monarch = {
 		name = "Aq'sheelie"
		dynasty = "Roq'Sentis"
		birth_date = 440.1.1
		adm = 0
		dip = 4
		mil = 5
		female = yes
    }
}

543.1.1 = {
	monarch = {
 		name = "Raartiet"
		dynasty = "Roq'Qhakil"
		birth_date = 516.1.1
		adm = 1
		dip = 1
		mil = 3
    }
}

616.1.1 = {
	monarch = {
 		name = "Sekad"
		dynasty = "Roq'Eca"
		birth_date = 589.1.1
		adm = 6
		dip = 0
		mil = 0
    }
}

668.1.1 = {
	monarch = {
 		name = "Razax"
		dynasty = "Roq'Roth'erhi"
		birth_date = 630.1.1
		adm = 4
		dip = 6
		mil = 3
    }
}

735.1.1 = {
	monarch = {
 		name = "Lisu"
		dynasty = "Roq'Rhaxhal"
		birth_date = 713.1.1
		adm = 2
		dip = 6
		mil = 0
		female = yes
    }
}

811.1.1 = {
	monarch = {
 		name = "Lharzi"
		dynasty = "Roq'Asucai"
		birth_date = 776.1.1
		adm = 4
		dip = 0
		mil = 1
		female = yes
    }
}

863.1.1 = {
	monarch = {
 		name = "Sekad"
		dynasty = "Roq'Ninchosh"
		birth_date = 821.1.1
		adm = 3
		dip = 6
		mil = 5
    }
}

900.1.1 = {
	monarch = {
 		name = "An'qu"
		dynasty = "Roq'Szillo"
		birth_date = 860.1.1
		adm = 1
		dip = 5
		mil = 2
		female = yes
    }
}

938.1.1 = {
	monarch = {
 		name = "Yiezer"
		dynasty = "Roq'Szillo"
		birth_date = 918.1.1
		adm = 6
		dip = 4
		mil = 5
		female = yes
    }
}

981.1.1 = {
	monarch = {
 		name = "Xisod"
		dynasty = "Roq'Zheirroth"
		birth_date = 953.1.1
		adm = 4
		dip = 4
		mil = 2
    }
}

1054.1.1 = {
	monarch = {
 		name = "Qhaknu"
		dynasty = "Roq'Zorud"
		birth_date = 1006.1.1
		adm = 3
		dip = 3
		mil = 6
		female = yes
    }
}

1109.1.1 = {
	monarch = {
 		name = "Las'tu"
		dynasty = "Roq'Shrasni"
		birth_date = 1082.1.1
		adm = 1
		dip = 2
		mil = 2
    }
}

1207.1.1 = {
	monarch = {
 		name = "Yiezer"
		dynasty = "Roq'Szassul"
		birth_date = 1156.1.1
		adm = 6
		dip = 1
		mil = 6
		female = yes
    }
}

1297.1.1 = {
	monarch = {
 		name = "Neqzub"
		dynasty = "Roq'Sairhoth'iel"
		birth_date = 1269.1.1
		adm = 1
		dip = 3
		mil = 1
    }
}

1366.1.1 = {
	monarch = {
 		name = "Qhotiqzex"
		dynasty = "Roq'Asucai"
		birth_date = 1319.1.1
		adm = 6
		dip = 2
		mil = 4
    }
}

1458.1.1 = {
	monarch = {
 		name = "Qasharhith"
		dynasty = "Roq'Sherri"
		birth_date = 1425.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1528.1.1 = {
	monarch = {
 		name = "Szazsarhor"
		dynasty = "Roq'Yollo"
		birth_date = 1505.1.1
		adm = 3
		dip = 0
		mil = 4
		female = yes
    }
}

1580.1.1 = {
	monarch = {
 		name = "Krirud"
		dynasty = "Roq'Lhovhi"
		birth_date = 1536.1.1
		adm = 3
		dip = 4
		mil = 2
    }
}

1646.1.1 = {
	monarch = {
 		name = "Qithesh"
		dynasty = "Roq'Rashra"
		birth_date = 1617.1.1
		adm = 2
		dip = 3
		mil = 6
		female = yes
    }
}

1711.1.1 = {
	monarch = {
 		name = "Ezex"
		dynasty = "Roq'Lazus"
		birth_date = 1666.1.1
		adm = 0
		dip = 3
		mil = 3
    }
}

1755.1.1 = {
	monarch = {
 		name = "Nasrur"
		dynasty = "Roq'Lhoshiq'shah"
		birth_date = 1710.1.1
		adm = 5
		dip = 2
		mil = 6
    }
}

1795.1.1 = {
	monarch = {
 		name = "Rhalzichou"
		dynasty = "Roq'Sairhoth'iel"
		birth_date = 1770.1.1
		adm = 3
		dip = 1
		mil = 3
    }
}

1852.1.1 = {
	monarch = {
 		name = "Sokath"
		dynasty = "Roq'Qaicoth'ad"
		birth_date = 1824.1.1
		adm = 4
		dip = 5
		mil = 1
		female = yes
    }
}

1905.1.1 = {
	monarch = {
 		name = "Qrevir"
		dynasty = "Roq'Shrasni"
		birth_date = 1868.1.1
		adm = 2
		dip = 4
		mil = 5
    }
}

1946.1.1 = {
	monarch = {
 		name = "Chantiri"
		dynasty = "Roq'Khozuca"
		birth_date = 1919.1.1
		adm = 2
		dip = 1
		mil = 5
		female = yes
    }
}

2000.1.1 = {
	monarch = {
 		name = "Rerteched"
		dynasty = "Roq'Zhok'thiecha"
		birth_date = 1967.1.1
		adm = 0
		dip = 0
		mil = 1
		female = yes
    }
}

2089.1.1 = {
	monarch = {
 		name = "Qharhonti"
		dynasty = "Roq'Lhovhi"
		birth_date = 2046.1.1
		adm = 5
		dip = 6
		mil = 5
		female = yes
    }
}

2144.1.1 = {
	monarch = {
 		name = "Rekkot"
		dynasty = "Roq'Zhiek'zi"
		birth_date = 2094.1.1
		adm = 6
		dip = 3
		mil = 3
    }
}

2221.1.1 = {
	monarch = {
 		name = "Srichixaq"
		dynasty = "Roq'Leqa"
		birth_date = 2202.1.1
		adm = 4
		dip = 2
		mil = 0
    }
}

2278.1.1 = {
	monarch = {
 		name = "Qhotiqzex"
		dynasty = "Roq'Akuthi"
		birth_date = 2255.1.1
		adm = 3
		dip = 2
		mil = 3
    }
}

2331.1.1 = {
	monarch = {
 		name = "Yorruchax"
		dynasty = "Roq'Shroki"
		birth_date = 2305.1.1
		adm = 1
		dip = 1
		mil = 0
    }
}

2366.1.1 = {
	monarch = {
 		name = "Szashro"
		dynasty = "Roq'Lhovhi"
		birth_date = 2322.1.1
		adm = 6
		dip = 0
		mil = 4
		female = yes
    }
}

2449.1.1 = {
	monarch = {
 		name = "Neqzub"
		dynasty = "Roq'Sherri"
		birth_date = 2425.1.1
		adm = 4
		dip = 6
		mil = 0
    }
}

