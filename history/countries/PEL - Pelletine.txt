government = tribal
government_rank = 5
mercantilism = 1
technology_group = elsweyr_tg
religion = khajiiti_pantheon
primary_culture = khajiiti
capital = 922

54.1.1 = {
	monarch = {
 		name = "Zara"
		dynasty = "Ma'jahirr"
		birth_date = 31.1.1
		adm = 1
		dip = 0
		mil = 1
		female = yes
    }
}

151.1.1 = {
	monarch = {
 		name = "Shasirba"
		dynasty = "Baradhari"
		birth_date = 124.1.1
		adm = 6
		dip = 6
		mil = 4
		female = yes
    }
}

232.1.1 = {
	monarch = {
 		name = "Kunerr-jo"
		dynasty = "Daro'urabi"
		birth_date = 202.1.1
		adm = 4
		dip = 6
		mil = 1
    }
}

304.1.1 = {
	monarch = {
 		name = "Silhu-jo"
		dynasty = "Rahkbusihr"
		birth_date = 257.1.1
		adm = 3
		dip = 2
		mil = 1
    }
}

388.1.1 = {
	monarch = {
 		name = "Mulamin"
		dynasty = "Urjaabhi"
		birth_date = 370.1.1
		adm = 2
		dip = 1
		mil = 5
    }
}

487.1.1 = {
	monarch = {
 		name = "Urjukka"
		dynasty = "Urjobil"
		birth_date = 447.1.1
		adm = 0
		dip = 0
		mil = 2
		female = yes
    }
}

549.1.1 = {
	monarch = {
 		name = "Ranabia"
		dynasty = "Kesharsha"
		birth_date = 530.1.1
		adm = 5
		dip = 0
		mil = 5
		female = yes
    }
}

610.1.1 = {
	monarch = {
 		name = "Khezuli"
		dynasty = "Dahshanji"
		birth_date = 572.1.1
		adm = 3
		dip = 6
		mil = 2
		female = yes
    }
}

703.1.1 = {
	monarch = {
 		name = "Barros"
		dynasty = "Shoava"
		birth_date = 667.1.1
		adm = 2
		dip = 5
		mil = 6
    }
}

780.1.1 = {
	monarch = {
 		name = "Takanazh"
		dynasty = "Khamnin"
		birth_date = 735.1.1
		adm = 0
		dip = 4
		mil = 2
    }
}

873.1.1 = {
	monarch = {
 		name = "Muzzi"
		dynasty = "Wadaiq"
		birth_date = 826.1.1
		adm = 5
		dip = 3
		mil = 6
    }
}

913.1.1 = {
	monarch = {
 		name = "J'zurud"
		dynasty = "Khamnin"
		birth_date = 890.1.1
		adm = 0
		dip = 5
		mil = 0
    }
}

949.1.1 = {
	monarch = {
 		name = "Balag"
		dynasty = "Shoirr"
		birth_date = 928.1.1
		adm = 5
		dip = 4
		mil = 4
    }
}

1029.1.1 = {
	monarch = {
 		name = "Juz-do"
		dynasty = "Tovikmnirn"
		birth_date = 1001.1.1
		adm = 4
		dip = 3
		mil = 1
    }
}

1079.1.1 = {
	monarch = {
 		name = "Chizbari"
		dynasty = "Dahlsien"
		birth_date = 1030.1.1
		adm = 2
		dip = 2
		mil = 4
		female = yes
    }
}

1166.1.1 = {
	monarch = {
 		name = "Tazzhid"
		dynasty = "Bavagarvi"
		birth_date = 1126.1.1
		adm = 0
		dip = 2
		mil = 1
    }
}

1262.1.1 = {
	monarch = {
 		name = "Narbhulad-do"
		dynasty = "Bahrajatani"
		birth_date = 1237.1.1
		adm = 5
		dip = 1
		mil = 5
    }
}

1303.1.1 = {
	monarch = {
 		name = "Kibibi"
		dynasty = "Akpoor"
		birth_date = 1281.1.1
		adm = 4
		dip = 0
		mil = 1
		female = yes
    }
}

1390.1.1 = {
	monarch = {
 		name = "Chirba"
		dynasty = "Solgandihr"
		birth_date = 1351.1.1
		adm = 2
		dip = 6
		mil = 5
		female = yes
    }
}

1429.1.1 = {
	monarch = {
 		name = "Vanraja"
		dynasty = "Havnudawi"
		birth_date = 1389.1.1
		adm = 4
		dip = 0
		mil = 6
		female = yes
    }
}

1480.1.1 = {
	monarch = {
 		name = "Rhani"
		dynasty = "Shoirr"
		birth_date = 1447.1.1
		adm = 1
		dip = 2
		mil = 6
		female = yes
    }
}

1544.1.1 = {
	monarch = {
 		name = "Rid-Thar-ri'Datta"
		dynasty = "Havnuhior"
		birth_date = 1506.1.1
		adm = 6
		dip = 2
		mil = 3
    }
}

1611.1.1 = {
	monarch = {
 		name = "Mizrali"
		dynasty = "Kavandi"
		birth_date = 1561.1.1
		adm = 1
		dip = 3
		mil = 5
		female = yes
    }
}

1709.1.1 = {
	monarch = {
 		name = "Gohjin-jo"
		dynasty = "M'arkhu"
		birth_date = 1672.1.1
		adm = 6
		dip = 2
		mil = 1
    }
}

1784.1.1 = {
	monarch = {
 		name = "Makkhzahr"
		dynasty = "Hasssien"
		birth_date = 1753.1.1
		adm = 4
		dip = 1
		mil = 5
    }
}

1860.1.1 = {
	monarch = {
 		name = "Hasra"
		dynasty = "Dahlsien"
		birth_date = 1821.1.1
		adm = 3
		dip = 1
		mil = 2
		female = yes
    }
}

1929.1.1 = {
	monarch = {
 		name = "Zairan"
		dynasty = "Zoarjhan"
		birth_date = 1880.1.1
		adm = 3
		dip = 5
		mil = 0
		female = yes
    }
}

1977.1.1 = {
	monarch = {
 		name = "Bishaba"
		dynasty = "Jodara"
		birth_date = 1943.1.1
		adm = 2
		dip = 4
		mil = 3
		female = yes
    }
}

2018.1.1 = {
	monarch = {
 		name = "Tajahir"
		dynasty = "Baadraym"
		birth_date = 1989.1.1
		adm = 0
		dip = 3
		mil = 0
    }
}

2066.1.1 = {
	monarch = {
 		name = "Raezargi"
		dynasty = "Ri'hasta"
		birth_date = 2024.1.1
		adm = 5
		dip = 2
		mil = 3
		female = yes
    }
}

2133.1.1 = {
	monarch = {
 		name = "Zuzik"
		dynasty = "Helnrjo"
		birth_date = 2115.1.1
		adm = 3
		dip = 1
		mil = 0
    }
}

2200.1.1 = {
	monarch = {
 		name = "Bindabi"
		dynasty = "J'der"
		birth_date = 2177.1.1
		adm = 3
		dip = 5
		mil = 0
		female = yes
    }
}

2272.1.1 = {
	monarch = {
 		name = "Umisashi"
		dynasty = "Anaihn"
		birth_date = 2243.1.1
		adm = 0
		dip = 0
		mil = 0
		female = yes
    }
}

2307.1.1 = {
	monarch = {
 		name = "Raehsi"
		dynasty = "Ma'jahirr"
		birth_date = 2280.1.1
		adm = 2
		dip = 1
		mil = 2
		female = yes
    }
}

2363.1.1 = {
	monarch = {
 		name = "Narahni"
		dynasty = "Dar'tesh"
		birth_date = 2340.1.1
		adm = 5
		dip = 2
		mil = 4
		female = yes
    }
}

2415.1.1 = {
	monarch = {
 		name = "Rhani"
		dynasty = "Raioni"
		birth_date = 2387.1.1
		adm = 3
		dip = 2
		mil = 1
		female = yes
    }
}

2464.1.1 = {
	monarch = {
 		name = "Abizah"
		dynasty = "J'der"
		birth_date = 2433.1.1
		adm = 1
		dip = 1
		mil = 5
		female = yes
    }
}

