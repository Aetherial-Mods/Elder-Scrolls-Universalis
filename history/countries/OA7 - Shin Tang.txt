government = monarchy
government_rank = 1
mercantilism = 1
technology_group = akavir_tg
religion = po_tun_pantheon
primary_culture = po_tun
capital = 642

54.1.1 = {
	monarch = {
 		name = "Dirskyts"
		dynasty = "Lissok'la"
		birth_date = 4.1.1
		adm = 4
		dip = 2
		mil = 5
		female = yes
    }
}

93.1.1 = {
	monarch = {
 		name = "Chaz"
		dynasty = "Fov'su"
		birth_date = 48.1.1
		adm = 3
		dip = 2
		mil = 1
    }
}

162.1.1 = {
	monarch = {
 		name = "Doknog"
		dynasty = "Fiestemo'ri"
		birth_date = 134.1.1
		adm = 1
		dip = 1
		mil = 5
    }
}

231.1.1 = {
	monarch = {
 		name = "Remoz"
		dynasty = "Luhu'la"
		birth_date = 197.1.1
		adm = 6
		dip = 0
		mil = 2
    }
	queen = {
 		name = "Sentyam"
		dynasty = "Luhu'la"
		birth_date = 203.1.1
		adm = 3
		dip = 6
		mil = 1
		female = yes
    }
	heir = {
 		name = "Zud"
		monarch_name = "Zud I"
		dynasty = "Luhu'la"
		birth_date = 228.1.1
		death_date = 318.1.1
		claim = 100
		adm = 0
		dip = 4
		mil = 0
    }
}

318.1.1 = {
	monarch = {
 		name = "Chaz"
		dynasty = "Munok'wo"
		birth_date = 280.1.1
		adm = 3
		dip = 5
		mil = 2
    }
}

376.1.1 = {
	monarch = {
 		name = "Dochecmea"
		dynasty = "Tus'su"
		birth_date = 339.1.1
		adm = 4
		dip = 0
		mil = 4
		female = yes
    }
	queen = {
 		name = "Piruhus"
		dynasty = "Tus'su"
		birth_date = 354.1.1
		adm = 5
		dip = 3
		mil = 5
    }
	heir = {
 		name = "Bundrumu"
		monarch_name = "Bundrumu I"
		dynasty = "Tus'su"
		birth_date = 375.1.1
		death_date = 460.1.1
		claim = 100
		adm = 2
		dip = 2
		mil = 4
    }
}

460.1.1 = {
	monarch = {
 		name = "Fiandok"
		dynasty = "Ruldov'ri"
		birth_date = 414.1.1
		adm = 1
		dip = 5
		mil = 4
    }
}

534.1.1 = {
	monarch = {
 		name = "Rozzeg"
		dynasty = "Fog'su"
		birth_date = 502.1.1
		adm = 6
		dip = 4
		mil = 1
    }
	queen = {
 		name = "Bororr"
		dynasty = "Fog'su"
		birth_date = 487.1.1
		adm = 3
		dip = 3
		mil = 0
		female = yes
    }
	heir = {
 		name = "Fon"
		monarch_name = "Fon I"
		dynasty = "Fog'su"
		birth_date = 527.1.1
		death_date = 595.1.1
		claim = 100
		adm = 0
		dip = 1
		mil = 5
    }
}

595.1.1 = {
	monarch = {
 		name = "Feg"
		dynasty = "Shogi'la"
		birth_date = 561.1.1
		adm = 3
		dip = 3
		mil = 1
    }
}

677.1.1 = {
	monarch = {
 		name = "Fiandok"
		dynasty = "Su'ri"
		birth_date = 657.1.1
		adm = 1
		dip = 2
		mil = 5
    }
}

755.1.1 = {
	monarch = {
 		name = "Rusdevyal"
		dynasty = "Ruvo'su"
		birth_date = 719.1.1
		adm = 6
		dip = 1
		mil = 1
		female = yes
    }
}

801.1.1 = {
	monarch = {
 		name = "Quv"
		dynasty = "Gaduyd'ri"
		birth_date = 755.1.1
		adm = 1
		dip = 2
		mil = 3
    }
	queen = {
 		name = "Qusoky"
		dynasty = "Gaduyd'ri"
		birth_date = 776.1.1
		adm = 1
		dip = 6
		mil = 4
		female = yes
    }
	heir = {
 		name = "Cisaclol"
		monarch_name = "Cisaclol I"
		dynasty = "Gaduyd'ri"
		birth_date = 792.1.1
		death_date = 875.1.1
		claim = 100
		adm = 5
		dip = 4
		mil = 3
		female = yes
    }
}

875.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Se"
		monarch_name = "Se I"
		dynasty = "Lisurr'la"
		birth_date = 871.1.1
		death_date = 889.1.1
		claim = 100
		adm = 3
		dip = 4
		mil = 6
		female = yes
    }
}

889.1.1 = {
	monarch = {
 		name = "De"
		dynasty = "Sentyam'la"
		birth_date = 849.1.1
		adm = 3
		dip = 0
		mil = 0
		female = yes
    }
	queen = {
 		name = "Chav"
		dynasty = "Sentyam'la"
		birth_date = 849.1.1
		adm = 0
		dip = 6
		mil = 6
    }
	heir = {
 		name = "Qarcyseim"
		monarch_name = "Qarcyseim I"
		dynasty = "Sentyam'la"
		birth_date = 877.1.1
		death_date = 947.1.1
		claim = 100
		adm = 4
		dip = 4
		mil = 5
		female = yes
    }
}

947.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Cisaclol"
		monarch_name = "Cisaclol I"
		dynasty = "Qachil'wo"
		birth_date = 947.1.1
		death_date = 965.1.1
		claim = 100
		adm = 2
		dip = 3
		mil = 1
		female = yes
    }
}

965.1.1 = {
	monarch = {
 		name = "Lu"
		dynasty = "Qokug'ri"
		birth_date = 912.1.1
		adm = 5
		dip = 5
		mil = 4
		female = yes
    }
	queen = {
 		name = "Mur"
		dynasty = "Qokug'ri"
		birth_date = 913.1.1
		adm = 1
		dip = 3
		mil = 3
    }
	heir = {
 		name = "Vag"
		monarch_name = "Vag I"
		dynasty = "Qokug'ri"
		birth_date = 956.1.1
		death_date = 1046.1.1
		claim = 100
		adm = 5
		dip = 2
		mil = 2
    }
}

1046.1.1 = {
	monarch = {
 		name = "Nahag"
		dynasty = "Tosdul'wo"
		birth_date = 998.1.1
		adm = 4
		dip = 1
		mil = 5
    }
}

1092.1.1 = {
	monarch = {
 		name = "Sok"
		dynasty = "Listus'wo"
		birth_date = 1071.1.1
		adm = 2
		dip = 0
		mil = 2
    }
}

1175.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "For"
		monarch_name = "For I"
		dynasty = "Fon'su"
		birth_date = 1162.1.1
		death_date = 1180.1.1
		claim = 100
		adm = 3
		dip = 4
		mil = 0
		female = yes
    }
}

1180.1.1 = {
	monarch = {
 		name = "Moyd"
		dynasty = "Zud'su"
		birth_date = 1132.1.1
		adm = 2
		dip = 1
		mil = 0
    }
	queen = {
 		name = "Kumdem"
		dynasty = "Zud'su"
		birth_date = 1145.1.1
		adm = 2
		dip = 4
		mil = 1
		female = yes
    }
	heir = {
 		name = "Susdevu"
		monarch_name = "Susdevu I"
		dynasty = "Zud'su"
		birth_date = 1172.1.1
		death_date = 1237.1.1
		claim = 100
		adm = 6
		dip = 3
		mil = 0
		female = yes
    }
}

1237.1.1 = {
	monarch = {
 		name = "Ti"
		dynasty = "Droryrko'wo"
		birth_date = 1198.1.1
		adm = 5
		dip = 6
		mil = 0
    }
}

1322.1.1 = {
	monarch = {
 		name = "Duhlan"
		dynasty = "Susthae'la"
		birth_date = 1274.1.1
		adm = 4
		dip = 5
		mil = 4
    }
}

1394.1.1 = {
	monarch = {
 		name = "Nessis"
		dynasty = "Fov'su"
		birth_date = 1344.1.1
		adm = 2
		dip = 4
		mil = 1
		female = yes
    }
}

1448.1.1 = {
	monarch = {
 		name = "Fov"
		dynasty = "De'la"
		birth_date = 1428.1.1
		adm = 0
		dip = 4
		mil = 4
    }
	queen = {
 		name = "Luhu"
		dynasty = "De'la"
		birth_date = 1411.1.1
		adm = 4
		dip = 2
		mil = 3
		female = yes
    }
	heir = {
 		name = "Krez"
		monarch_name = "Krez I"
		dynasty = "De'la"
		birth_date = 1446.1.1
		death_date = 1495.1.1
		claim = 100
		adm = 1
		dip = 1
		mil = 2
    }
}

1495.1.1 = {
	monarch = {
 		name = "Duhlan"
		dynasty = "Susthae'la"
		birth_date = 1444.1.1
		adm = 4
		dip = 2
		mil = 5
    }
	queen = {
 		name = "Thusdu"
		dynasty = "Susthae'la"
		birth_date = 1467.1.1
		adm = 0
		dip = 1
		mil = 3
		female = yes
    }
	heir = {
 		name = "Kontessoor"
		monarch_name = "Kontessoor I"
		dynasty = "Susthae'la"
		birth_date = 1487.1.1
		death_date = 1565.1.1
		claim = 100
		adm = 4
		dip = 6
		mil = 2
    }
}

1565.1.1 = {
	monarch = {
 		name = "Fov"
		dynasty = "Susdevu'la"
		birth_date = 1522.1.1
		adm = 4
		dip = 2
		mil = 3
    }
	queen = {
 		name = "Gakuveets"
		dynasty = "Susdevu'la"
		birth_date = 1538.1.1
		adm = 1
		dip = 1
		mil = 2
		female = yes
    }
	heir = {
 		name = "Wolcor"
		monarch_name = "Wolcor I"
		dynasty = "Susdevu'la"
		birth_date = 1559.1.1
		death_date = 1614.1.1
		claim = 100
		adm = 1
		dip = 4
		mil = 3
		female = yes
    }
}

1614.1.1 = {
	monarch = {
 		name = "Neyak"
		dynasty = "Fen'ri"
		birth_date = 1564.1.1
		adm = 0
		dip = 1
		mil = 3
    }
	queen = {
 		name = "Karken"
		dynasty = "Fen'ri"
		birth_date = 1579.1.1
		adm = 4
		dip = 6
		mil = 2
		female = yes
    }
}

1700.1.1 = {
	monarch = {
 		name = "Gaduyd"
		dynasty = "Piruhus'su"
		birth_date = 1658.1.1
		adm = 2
		dip = 6
		mil = 6
    }
	queen = {
 		name = "Klirkugots"
		dynasty = "Piruhus'su"
		birth_date = 1681.1.1
		adm = 6
		dip = 4
		mil = 5
		female = yes
    }
	heir = {
 		name = "Voldar"
		monarch_name = "Voldar I"
		dynasty = "Piruhus'su"
		birth_date = 1685.1.1
		death_date = 1741.1.1
		claim = 100
		adm = 3
		dip = 3
		mil = 4
    }
}

1741.1.1 = {
	monarch = {
 		name = "Se"
		dynasty = "Krer'ri"
		birth_date = 1718.1.1
		adm = 6
		dip = 4
		mil = 6
		female = yes
    }
}

1837.1.1 = {
	monarch = {
 		name = "Bundrumu"
		dynasty = "Biso'ri"
		birth_date = 1786.1.1
		adm = 4
		dip = 3
		mil = 3
    }
}

1899.1.1 = {
	monarch = {
 		name = "Thom"
		dynasty = "Zud'su"
		birth_date = 1869.1.1
		adm = 2
		dip = 2
		mil = 6
    }
}

1981.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Zud"
		monarch_name = "Zud I"
		dynasty = "Pan'ri"
		birth_date = 1975.1.1
		death_date = 1993.1.1
		claim = 100
		adm = 3
		dip = 0
		mil = 4
    }
}

1993.1.1 = {
	monarch = {
 		name = "De"
		dynasty = "Sirendrurr'wo"
		birth_date = 1946.1.1
		adm = 2
		dip = 3
		mil = 4
    }
	queen = {
 		name = "Dohortus"
		dynasty = "Sirendrurr'wo"
		birth_date = 1947.1.1
		adm = 6
		dip = 1
		mil = 3
		female = yes
    }
	heir = {
 		name = "Ramoz"
		monarch_name = "Ramoz I"
		dynasty = "Sirendrurr'wo"
		birth_date = 1989.1.1
		death_date = 2056.1.1
		claim = 100
		adm = 0
		dip = 5
		mil = 4
    }
}

2056.1.1 = {
	monarch = {
 		name = "Beme"
		dynasty = "Se'wo"
		birth_date = 2030.1.1
		adm = 6
		dip = 1
		mil = 5
		female = yes
    }
	queen = {
 		name = "Gon"
		dynasty = "Se'wo"
		birth_date = 2035.1.1
		adm = 3
		dip = 0
		mil = 4
    }
	heir = {
 		name = "Kusdere"
		monarch_name = "Kusdere I"
		dynasty = "Se'wo"
		birth_date = 2052.1.1
		death_date = 2097.1.1
		claim = 100
		adm = 0
		dip = 5
		mil = 3
    }
}

2097.1.1 = {
	monarch = {
 		name = "Suhorrur"
		dynasty = "Nuyul'wo"
		birth_date = 2058.1.1
		adm = 2
		dip = 0
		mil = 5
    }
}

2195.1.1 = {
	monarch = {
 		name = "Tehuntu"
		dynasty = "Neyak'su"
		birth_date = 2161.1.1
		adm = 1
		dip = 6
		mil = 2
    }
}

2237.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Tus"
		monarch_name = "Tus I"
		dynasty = "Rilu'wo"
		birth_date = 2233.1.1
		death_date = 2251.1.1
		claim = 100
		adm = 1
		dip = 3
		mil = 0
    }
}

2251.1.1 = {
	monarch = {
 		name = "Rolum"
		dynasty = "Sieherr'la"
		birth_date = 2218.1.1
		adm = 1
		dip = 6
		mil = 0
    }
}

2309.1.1 = {
	monarch = {
 		name = "Lisurr"
		dynasty = "Gelnar'su"
		birth_date = 2289.1.1
		adm = 4
		dip = 1
		mil = 2
		female = yes
    }
}

2375.1.1 = {
	monarch = {
 		name = "Krakug"
		dynasty = "Sehondek'ri"
		birth_date = 2335.1.1
		adm = 2
		dip = 0
		mil = 6
    }
}

2467.1.1 = {
	monarch = {
 		name = "Shracmur"
		dynasty = "Tus'su"
		birth_date = 2431.1.1
		adm = 0
		dip = 6
		mil = 2
		female = yes
    }
	queen = {
 		name = "Gur"
		dynasty = "Tus'su"
		birth_date = 2429.1.1
		adm = 4
		dip = 5
		mil = 1
    }
	heir = {
 		name = "Somdok"
		monarch_name = "Somdok I"
		dynasty = "Tus'su"
		birth_date = 2465.1.1
		death_date = 2530.1.1
		claim = 100
		adm = 1
		dip = 3
		mil = 0
		female = yes
    }
}

