government = tribal
government_rank = 5
mercantilism = 1
technology_group = marshan_tg
religion = cult_of_ancestors
primary_culture = horwalli
capital = 6694

54.1.1 = {
	monarch = {
 		name = "Taovmek"
		dynasty = "Baluris"
		birth_date = 25.1.1
		adm = 2
		dip = 6
		mil = 1
    }
}

97.1.1 = {
	monarch = {
 		name = "Lukhades"
		dynasty = "Shyrlon"
		birth_date = 60.1.1
		adm = 1
		dip = 5
		mil = 4
    }
}

152.1.1 = {
	monarch = {
 		name = "Nhysyna"
		dynasty = "Dehlath"
		birth_date = 110.1.1
		adm = 6
		dip = 5
		mil = 1
    }
}

242.1.1 = {
	monarch = {
 		name = "Lannanuh"
		dynasty = "Ulraodac"
		birth_date = 203.1.1
		adm = 4
		dip = 4
		mil = 5
		female = yes
    }
}

285.1.1 = {
	monarch = {
 		name = "Assiv"
		dynasty = "Lahlor"
		birth_date = 256.1.1
		adm = 2
		dip = 3
		mil = 1
		female = yes
    }
}

348.1.1 = {
	monarch = {
 		name = "Vrekaoric"
		dynasty = "Nhevnith"
		birth_date = 323.1.1
		adm = 1
		dip = 2
		mil = 5
    }
}

427.1.1 = {
	monarch = {
 		name = "Molluv"
		dynasty = "Zuras"
		birth_date = 404.1.1
		adm = 6
		dip = 1
		mil = 1
		female = yes
    }
}

487.1.1 = {
	monarch = {
 		name = "Ykuzok"
		dynasty = "Nades"
		birth_date = 447.1.1
		adm = 4
		dip = 1
		mil = 5
    }
}

543.1.1 = {
	monarch = {
 		name = "Assiv"
		dynasty = "Vyhris"
		birth_date = 525.1.1
		adm = 6
		dip = 2
		mil = 0
		female = yes
    }
}

586.1.1 = {
	monarch = {
 		name = "Vrekaoric"
		dynasty = "Raurmevan"
		birth_date = 539.1.1
		adm = 4
		dip = 1
		mil = 3
    }
}

646.1.1 = {
	monarch = {
 		name = "Yhlith"
		dynasty = "Lystader"
		birth_date = 595.1.1
		adm = 2
		dip = 0
		mil = 0
    }
}

700.1.1 = {
	monarch = {
 		name = "Zumrive"
		dynasty = "Vyhris"
		birth_date = 670.1.1
		adm = 1
		dip = 6
		mil = 4
    }
}

791.1.1 = {
	monarch = {
 		name = "Limrath"
		dynasty = "Tehrydac"
		birth_date = 760.1.1
		adm = 6
		dip = 6
		mil = 0
    }
}

858.1.1 = {
	monarch = {
 		name = "Rithev"
		dynasty = "Dytan"
		birth_date = 822.1.1
		adm = 4
		dip = 5
		mil = 4
		female = yes
    }
}

901.1.1 = {
	monarch = {
 		name = "Banheth"
		dynasty = "Dytan"
		birth_date = 882.1.1
		adm = 2
		dip = 4
		mil = 1
    }
}

938.1.1 = {
	monarch = {
 		name = "Azin"
		dynasty = "Baluris"
		birth_date = 886.1.1
		adm = 1
		dip = 3
		mil = 4
		female = yes
    }
}

1023.1.1 = {
	monarch = {
 		name = "Nhevnith"
		dynasty = "Shysek"
		birth_date = 978.1.1
		adm = 3
		dip = 5
		mil = 6
    }
}

1067.1.1 = {
	monarch = {
 		name = "Rivmamen"
		dynasty = "Rivmamen"
		birth_date = 1023.1.1
		adm = 1
		dip = 4
		mil = 2
    }
}

1119.1.1 = {
	monarch = {
 		name = "Shevnic"
		dynasty = "Duruvic"
		birth_date = 1079.1.1
		adm = 6
		dip = 3
		mil = 6
    }
}

1215.1.1 = {
	monarch = {
 		name = "Maurlir"
		dynasty = "Ykhik"
		birth_date = 1173.1.1
		adm = 4
		dip = 2
		mil = 3
    }
}

1262.1.1 = {
	monarch = {
 		name = "Renhe"
		dynasty = "Lidrurek"
		birth_date = 1244.1.1
		adm = 3
		dip = 1
		mil = 6
    }
}

1359.1.1 = {
	monarch = {
 		name = "Vivatur"
		dynasty = "Gamulek"
		birth_date = 1339.1.1
		adm = 1
		dip = 1
		mil = 3
		female = yes
    }
}

1423.1.1 = {
	monarch = {
 		name = "Shevnic"
		dynasty = "Vyhris"
		birth_date = 1399.1.1
		adm = 6
		dip = 0
		mil = 0
    }
}

1507.1.1 = {
	monarch = {
 		name = "Maurlir"
		dynasty = "Raadek"
		birth_date = 1486.1.1
		adm = 4
		dip = 6
		mil = 3
    }
}

1584.1.1 = {
	monarch = {
 		name = "Renhe"
		dynasty = "Nauma"
		birth_date = 1534.1.1
		adm = 6
		dip = 0
		mil = 5
    }
}

1661.1.1 = {
	monarch = {
 		name = "Vivatur"
		dynasty = "Banheth"
		birth_date = 1626.1.1
		adm = 4
		dip = 6
		mil = 1
		female = yes
    }
}

1755.1.1 = {
	monarch = {
 		name = "Lavmizir"
		dynasty = "Tyvmauzas"
		birth_date = 1733.1.1
		adm = 3
		dip = 6
		mil = 5
    }
}

1811.1.1 = {
	monarch = {
 		name = "Tozara"
		dynasty = "Linhauzec"
		birth_date = 1781.1.1
		adm = 1
		dip = 5
		mil = 2
		female = yes
    }
}

1847.1.1 = {
	monarch = {
 		name = "Nahroc"
		dynasty = "Harinir"
		birth_date = 1827.1.1
		adm = 6
		dip = 4
		mil = 5
    }
}

1882.1.1 = {
	monarch = {
 		name = "Naruh"
		dynasty = "Valros"
		birth_date = 1847.1.1
		adm = 4
		dip = 3
		mil = 2
		female = yes
    }
}

1935.1.1 = {
	monarch = {
 		name = "Rethovin"
		dynasty = "Helith"
		birth_date = 1890.1.1
		adm = 3
		dip = 2
		mil = 6
		female = yes
    }
}

2002.1.1 = {
	monarch = {
 		name = "Tozara"
		dynasty = "Dalarac"
		birth_date = 1965.1.1
		adm = 1
		dip = 2
		mil = 2
		female = yes
    }
}

2074.1.1 = {
	monarch = {
 		name = "Mavmen"
		dynasty = "Vrekaoric"
		birth_date = 2022.1.1
		adm = 3
		dip = 3
		mil = 4
    }
}

2149.1.1 = {
	monarch = {
 		name = "Ovedin"
		dynasty = "Dehlath"
		birth_date = 2129.1.1
		adm = 1
		dip = 2
		mil = 1
		female = yes
    }
}

2232.1.1 = {
	monarch = {
 		name = "Raurmevan"
		dynasty = "Turedos"
		birth_date = 2203.1.1
		adm = 6
		dip = 1
		mil = 4
    }
}

2279.1.1 = {
	monarch = {
 		name = "Lasili"
		dynasty = "Nhevnith"
		birth_date = 2245.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

2367.1.1 = {
	monarch = {
 		name = "Nhaatalok"
		dynasty = "Kalos"
		birth_date = 2320.1.1
		adm = 3
		dip = 0
		mil = 4
    }
}

2460.1.1 = {
	monarch = {
 		name = "Gaurmas"
		dynasty = "Vredrer"
		birth_date = 2424.1.1
		adm = 1
		dip = 6
		mil = 1
    }
}

