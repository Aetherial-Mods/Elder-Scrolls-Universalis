government = tribal
government_rank = 5
mercantilism = 1
technology_group = northern_tg
religion = old_gods_cult
primary_culture = reachmen
capital = 1355
religious_school = uricanbeg_school

54.1.1 = {
	monarch = {
 		name = "Karkar"
		dynasty = "Paduvo"
		birth_date = 3.1.1
		adm = 6
		dip = 3
		mil = 6
		female = yes
    }
}

139.1.1 = {
	monarch = {
 		name = "Heto"
		dynasty = "Drinrur"
		birth_date = 91.1.1
		adm = 4
		dip = 2
		mil = 3
		female = yes
    }
}

238.1.1 = {
	monarch = {
 		name = "Orvete"
		dynasty = "Olcawa"
		birth_date = 188.1.1
		adm = 3
		dip = 1
		mil = 0
    }
}

311.1.1 = {
	monarch = {
 		name = "Elkonbuor"
		dynasty = "Pondislug"
		birth_date = 290.1.1
		adm = 1
		dip = 0
		mil = 3
    }
}

379.1.1 = {
	monarch = {
 		name = "Karkar"
		dynasty = "Duldoso"
		birth_date = 333.1.1
		adm = 6
		dip = 6
		mil = 0
		female = yes
    }
}

461.1.1 = {
	monarch = {
 		name = "Heto"
		dynasty = "Uveza"
		birth_date = 414.1.1
		adm = 1
		dip = 1
		mil = 1
		female = yes
    }
}

498.1.1 = {
	monarch = {
 		name = "Orvete"
		dynasty = "Povren"
		birth_date = 451.1.1
		adm = 6
		dip = 0
		mil = 5
    }
}

592.1.1 = {
	monarch = {
 		name = "Relo"
		dynasty = "Vomlol"
		birth_date = 561.1.1
		adm = 4
		dip = 6
		mil = 2
		female = yes
    }
}

647.1.1 = {
	monarch = {
 		name = "Harbon"
		dynasty = "Bad"
		birth_date = 619.1.1
		adm = 3
		dip = 5
		mil = 5
		female = yes
    }
}

709.1.1 = {
	monarch = {
 		name = "Sumdesa"
		dynasty = "Walage"
		birth_date = 691.1.1
		adm = 1
		dip = 5
		mil = 2
    }
}

776.1.1 = {
	monarch = {
 		name = "Edvaw"
		dynasty = "Dych"
		birth_date = 752.1.1
		adm = 6
		dip = 4
		mil = 6
    }
}

844.1.1 = {
	monarch = {
 		name = "Brumroch"
		dynasty = "Dilvortad"
		birth_date = 805.1.1
		adm = 0
		dip = 1
		mil = 4
    }
}

897.1.1 = {
	monarch = {
 		name = "Bor"
		dynasty = "Demrenu"
		birth_date = 850.1.1
		adm = 5
		dip = 0
		mil = 0
    }
}

983.1.1 = {
	monarch = {
 		name = "Egvizo"
		dynasty = "Brumroch"
		birth_date = 937.1.1
		adm = 5
		dip = 3
		mil = 1
		female = yes
    }
}

1027.1.1 = {
	monarch = {
 		name = "Veyrbid"
		dynasty = "Kog"
		birth_date = 988.1.1
		adm = 3
		dip = 3
		mil = 4
    }
}

1097.1.1 = {
	monarch = {
 		name = "Dych"
		dynasty = "Kac"
		birth_date = 1067.1.1
		adm = 1
		dip = 2
		mil = 1
    }
}

1145.1.1 = {
	monarch = {
 		name = "Pondislug"
		dynasty = "Dranres"
		birth_date = 1127.1.1
		adm = 6
		dip = 1
		mil = 4
    }
}

1211.1.1 = {
	monarch = {
 		name = "Dranres"
		dynasty = "Duldoso"
		birth_date = 1193.1.1
		adm = 4
		dip = 0
		mil = 1
    }
}

1290.1.1 = {
	monarch = {
 		name = "Noge"
		dynasty = "Uveza"
		birth_date = 1264.1.1
		adm = 3
		dip = 6
		mil = 5
		female = yes
    }
}

1328.1.1 = {
	monarch = {
 		name = "Tommuh"
		dynasty = "Endeg"
		birth_date = 1287.1.1
		adm = 1
		dip = 6
		mil = 1
		female = yes
    }
}

1426.1.1 = {
	monarch = {
 		name = "Uaeme"
		dynasty = "Brubbumi"
		birth_date = 1375.1.1
		adm = 6
		dip = 5
		mil = 5
		female = yes
    }
}

1479.1.1 = {
	monarch = {
 		name = "Dranres"
		dynasty = "Brumroch"
		birth_date = 1434.1.1
		adm = 1
		dip = 6
		mil = 0
    }
}

1521.1.1 = {
	monarch = {
 		name = "Doch"
		dynasty = "Treyn"
		birth_date = 1470.1.1
		adm = 6
		dip = 5
		mil = 3
    }
}

1566.1.1 = {
	monarch = {
 		name = "Edemo"
		dynasty = "Sovril"
		birth_date = 1528.1.1
		adm = 5
		dip = 5
		mil = 0
    }
}

1625.1.1 = {
	monarch = {
 		name = "Dog"
		dynasty = "Dog"
		birth_date = 1597.1.1
		adm = 3
		dip = 4
		mil = 4
    }
}

1690.1.1 = {
	monarch = {
 		name = "Ilcev"
		dynasty = "Dril"
		birth_date = 1651.1.1
		adm = 1
		dip = 3
		mil = 0
    }
}

1768.1.1 = {
	monarch = {
 		name = "Drerdirtan"
		dynasty = "Vomlol"
		birth_date = 1732.1.1
		adm = 6
		dip = 2
		mil = 4
    }
}

1807.1.1 = {
	monarch = {
 		name = "Ocdolo"
		dynasty = "Dennan"
		birth_date = 1759.1.1
		adm = 5
		dip = 1
		mil = 1
		female = yes
    }
}

1889.1.1 = {
	monarch = {
 		name = "Dog"
		dynasty = "Eldin"
		birth_date = 1860.1.1
		adm = 3
		dip = 1
		mil = 4
    }
}

1943.1.1 = {
	monarch = {
 		name = "Ilcev"
		dynasty = "Obrum"
		birth_date = 1902.1.1
		adm = 5
		dip = 2
		mil = 6
    }
}

1980.1.1 = {
	monarch = {
 		name = "Drerdirtan"
		dynasty = "Gralret"
		birth_date = 1927.1.1
		adm = 3
		dip = 1
		mil = 2
    }
}

2040.1.1 = {
	monarch = {
 		name = "Brunnuli"
		dynasty = "Waug"
		birth_date = 2013.1.1
		adm = 1
		dip = 0
		mil = 6
    }
}

2096.1.1 = {
	monarch = {
 		name = "Pal"
		dynasty = "Waug"
		birth_date = 2071.1.1
		adm = 6
		dip = 6
		mil = 3
    }
}

2195.1.1 = {
	monarch = {
 		name = "Olcawa"
		dynasty = "Endeg"
		birth_date = 2163.1.1
		adm = 5
		dip = 6
		mil = 6
    }
}

2238.1.1 = {
	monarch = {
 		name = "Panefu"
		dynasty = "Puud"
		birth_date = 2204.1.1
		adm = 3
		dip = 5
		mil = 3
		female = yes
    }
}

2298.1.1 = {
	monarch = {
 		name = "Tuai"
		dynasty = "Ped"
		birth_date = 2251.1.1
		adm = 1
		dip = 4
		mil = 0
		female = yes
    }
}

2382.1.1 = {
	monarch = {
 		name = "Onbus"
		dynasty = "Nanrale"
		birth_date = 2340.1.1
		adm = 6
		dip = 3
		mil = 3
    }
}

2469.1.1 = {
	monarch = {
 		name = "Povren"
		dynasty = "Orvete"
		birth_date = 2429.1.1
		adm = 1
		dip = 5
		mil = 5
    }
}

