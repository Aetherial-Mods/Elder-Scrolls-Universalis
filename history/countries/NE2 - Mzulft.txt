government = republic
government_rank = 3
mercantilism = 1
technology_group = dwemer_tg
religion = reason_and_logic_cult
primary_culture = dwemer
capital = 2924

54.1.1 = {
	monarch = {
 		name = "Bhazchyn"
		dynasty = "Av'Mhunac"
		birth_date = 20.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

127.1.1 = {
	monarch = {
 		name = "Yzranrida"
		dynasty = "Av'Ghaznak"
		birth_date = 85.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

212.1.1 = {
	monarch = {
 		name = "Inratarn"
		dynasty = "Az'Izvuvretch"
		birth_date = 194.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

283.1.1 = {
	monarch = {
 		name = "Nedac"
		dynasty = "Af'Nedac"
		birth_date = 233.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

337.1.1 = {
	monarch = {
 		name = "Dzredras"
		dynasty = "Af'Ghafuan"
		birth_date = 291.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

418.1.1 = {
	monarch = {
 		name = "Jnathunch"
		dynasty = "Av'Dzragvin"
		birth_date = 375.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

453.1.1 = {
	monarch = {
 		name = "Inratarn"
		dynasty = "Az'Chzebchasz"
		birth_date = 404.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

513.1.1 = {
	monarch = {
 		name = "Nedac"
		dynasty = "Af'Agahuanch"
		birth_date = 472.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

586.1.1 = {
	monarch = {
 		name = "Rafk"
		dynasty = "Af'Izvumratz"
		birth_date = 551.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

654.1.1 = {
	monarch = {
 		name = "Ynzarlatz"
		dynasty = "Az'Yzranrida"
		birth_date = 601.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

713.1.1 = {
	monarch = {
 		name = "Chzevragch"
		dynasty = "Av'Ychogrenz"
		birth_date = 676.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

790.1.1 = {
	monarch = {
 		name = "Choatchatz"
		dynasty = "Aq'Jlethurzch"
		birth_date = 747.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

837.1.1 = {
	monarch = {
 		name = "Ihlefuan"
		dynasty = "Av'Ralen"
		birth_date = 802.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

891.1.1 = {
	monarch = {
 		name = "Alnogwetch"
		dynasty = "Az'Ithorves"
		birth_date = 855.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

977.1.1 = {
	monarch = {
 		name = "Mchavin"
		dynasty = "Af'Churd"
		birth_date = 932.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1026.1.1 = {
	monarch = {
 		name = "Rhothurzch"
		dynasty = "Af'Nchyhrek"
		birth_date = 973.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1074.1.1 = {
	monarch = {
 		name = "Bluhzis"
		dynasty = "Af'Choalchanf"
		birth_date = 1036.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1150.1.1 = {
	monarch = {
 		name = "Alnogwetch"
		dynasty = "Af'Klalzrak"
		birth_date = 1116.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1186.1.1 = {
	monarch = {
 		name = "Nromgunch"
		dynasty = "Af'Chiuvnak"
		birth_date = 1160.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1274.1.1 = {
	monarch = {
 		name = "Grabnanch"
		dynasty = "Av'Davlar"
		birth_date = 1243.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1326.1.1 = {
	monarch = {
 		name = "Krevzyrn"
		dynasty = "Aq'Krilnif"
		birth_date = 1274.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1403.1.1 = {
	monarch = {
 		name = "Snelarn"
		dynasty = "Af'Kolzarf"
		birth_date = 1354.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1450.1.1 = {
	monarch = {
 		name = "Tnadrak"
		dynasty = "Aq'Shtrozril"
		birth_date = 1427.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1510.1.1 = {
	monarch = {
 		name = "Jlethurzch"
		dynasty = "Aq'Aravlen"
		birth_date = 1487.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1556.1.1 = {
	monarch = {
 		name = "Jribwyr"
		dynasty = "Af'Jnavraz"
		birth_date = 1523.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1603.1.1 = {
	monarch = {
 		name = "Chzetvar"
		dynasty = "Av'Ychogrenz"
		birth_date = 1578.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1639.1.1 = {
	monarch = {
 		name = "Miban"
		dynasty = "Af'Irhadac"
		birth_date = 1587.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1693.1.1 = {
	monarch = {
 		name = "Rafnyg"
		dynasty = "Aq'Cfradrys"
		birth_date = 1671.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1749.1.1 = {
	monarch = {
 		name = "Rlorlis"
		dynasty = "Af'Alnogwetch"
		birth_date = 1696.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1819.1.1 = {
	monarch = {
 		name = "Cfranhatch"
		dynasty = "Af'Jhourlac"
		birth_date = 1793.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1911.1.1 = {
	monarch = {
 		name = "Miban"
		dynasty = "Af'Chzevragch"
		birth_date = 1863.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1964.1.1 = {
	monarch = {
 		name = "Kridhis"
		dynasty = "Av'Dhamuard"
		birth_date = 1917.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

2036.1.1 = {
	monarch = {
 		name = "Jhomrumhz"
		dynasty = "Av'Mhunac"
		birth_date = 1992.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

2092.1.1 = {
	monarch = {
 		name = "Chragrenz"
		dynasty = "Av'Tahron"
		birth_date = 2065.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2139.1.1 = {
	monarch = {
 		name = "Alnodrunz"
		dynasty = "Az'Ylrefwinn"
		birth_date = 2087.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2206.1.1 = {
	monarch = {
 		name = "Nebgar"
		dynasty = "Aq'Blulnmer"
		birth_date = 2166.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2248.1.1 = {
	monarch = {
 		name = "Tadlin"
		dynasty = "Az'Ksrefurn"
		birth_date = 2219.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2329.1.1 = {
	monarch = {
 		name = "Tugradac"
		dynasty = "Aq'Rlorlis"
		birth_date = 2281.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2413.1.1 = {
	monarch = {
 		name = "Chzebchasz"
		dynasty = "Aq'Carelchanf"
		birth_date = 2372.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

2488.1.1 = {
	monarch = {
 		name = "Grigarn"
		dynasty = "Az'Nohnch"
		birth_date = 2442.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

