government = monarchy
government_rank = 1
mercantilism = 1
technology_group = cyrodiil_tg
religion = hermeus_mora_cult
primary_culture = ayleid
capital = 5777

54.1.1 = {
	monarch = {
 		name = "Mennith"
		dynasty = "Mea-Calabolis"
		birth_date = 36.1.1
		adm = 0
		dip = 4
		mil = 0
		female = yes
    }
	queen = {
 		name = "Qystur"
		dynasty = "Mea-Calabolis"
		birth_date = 36.1.1
		adm = 0
		dip = 1
		mil = 1
    }
	heir = {
 		name = "Azu"
		monarch_name = "Azu I"
		dynasty = "Mea-Calabolis"
		birth_date = 44.1.1
		death_date = 92.1.1
		claim = 100
		adm = 5
		dip = 3
		mil = 3
		female = yes
    }
}

92.1.1 = {
	monarch = {
 		name = "Sumadhiash"
		dynasty = "Mea-Alessia"
		birth_date = 61.1.1
		adm = 2
		dip = 5
		mil = 3
		female = yes
    }
	queen = {
 		name = "Talkynd"
		dynasty = "Mea-Alessia"
		birth_date = 74.1.1
		adm = 6
		dip = 4
		mil = 2
    }
}

184.1.1 = {
	monarch = {
 		name = "Uhidhon"
		dynasty = "Ula-Plalusius"
		birth_date = 165.1.1
		adm = 4
		dip = 3
		mil = 6
		female = yes
    }
}

282.1.1 = {
	monarch = {
 		name = "Lyzelniniath"
		dynasty = "Mea-Conoa"
		birth_date = 253.1.1
		adm = 6
		dip = 4
		mil = 1
		female = yes
    }
	queen = {
 		name = "Filestis"
		dynasty = "Mea-Conoa"
		birth_date = 240.1.1
		adm = 0
		dip = 1
		mil = 2
    }
	heir = {
 		name = "Sylanwesh"
		monarch_name = "Sylanwesh I"
		dynasty = "Mea-Conoa"
		birth_date = 274.1.1
		death_date = 379.1.1
		claim = 100
		adm = 3
		dip = 6
		mil = 1
		female = yes
    }
}

379.1.1 = {
	monarch = {
 		name = "Nimender"
		dynasty = "Mea-Calabolis"
		birth_date = 338.1.1
		adm = 3
		dip = 3
		mil = 1
    }
}

470.1.1 = {
	monarch = {
 		name = "Liva"
		dynasty = "Mea-Amane"
		birth_date = 421.1.1
		adm = 4
		dip = 0
		mil = 6
		female = yes
    }
	queen = {
 		name = "Cosudam"
		dynasty = "Mea-Amane"
		birth_date = 439.1.1
		adm = 4
		dip = 3
		mil = 0
    }
	heir = {
 		name = "Mene"
		monarch_name = "Mene I"
		dynasty = "Mea-Amane"
		birth_date = 457.1.1
		death_date = 554.1.1
		claim = 100
		adm = 1
		dip = 2
		mil = 6
		female = yes
    }
}

554.1.1 = {
	monarch = {
 		name = "Dyzas"
		dynasty = "Ula-Rusifus"
		birth_date = 525.1.1
		adm = 0
		dip = 5
		mil = 6
		female = yes
    }
	queen = {
 		name = "Fisno"
		dynasty = "Ula-Rusifus"
		birth_date = 529.1.1
		adm = 4
		dip = 4
		mil = 5
    }
	heir = {
 		name = "Wimunmara"
		monarch_name = "Wimunmara I"
		dynasty = "Ula-Rusifus"
		birth_date = 541.1.1
		death_date = 603.1.1
		claim = 100
		adm = 1
		dip = 2
		mil = 4
		female = yes
    }
}

603.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Dondont"
		monarch_name = "Dondont I"
		dynasty = "Mea-Hastrel"
		birth_date = 596.1.1
		death_date = 614.1.1
		claim = 100
		adm = 6
		dip = 2
		mil = 1
    }
}

614.1.1 = {
	monarch = {
 		name = "Varondil"
		dynasty = "Vae-Vandiand"
		birth_date = 589.1.1
		adm = 2
		dip = 3
		mil = 3
    }
	queen = {
 		name = "Nivo"
		dynasty = "Vae-Vandiand"
		birth_date = 595.1.1
		adm = 6
		dip = 2
		mil = 2
		female = yes
    }
	heir = {
 		name = "Malyon"
		monarch_name = "Malyon I"
		dynasty = "Vae-Vandiand"
		birth_date = 599.1.1
		death_date = 684.1.1
		claim = 100
		adm = 3
		dip = 0
		mil = 1
    }
}

684.1.1 = {
	monarch = {
 		name = "Nant"
		dynasty = "Mea-Aerienna"
		birth_date = 632.1.1
		adm = 5
		dip = 1
		mil = 3
    }
	queen = {
 		name = "Summi"
		dynasty = "Mea-Aerienna"
		birth_date = 654.1.1
		adm = 2
		dip = 0
		mil = 2
		female = yes
    }
	heir = {
 		name = "Odilon"
		monarch_name = "Odilon I"
		dynasty = "Mea-Aerienna"
		birth_date = 684.1.1
		death_date = 773.1.1
		claim = 100
		adm = 6
		dip = 5
		mil = 1
    }
}

773.1.1 = {
	monarch = {
 		name = "Anurraame"
		dynasty = "Ula-Narrarae"
		birth_date = 745.1.1
		adm = 5
		dip = 2
		mil = 2
		female = yes
    }
	queen = {
 		name = "Fenlord"
		dynasty = "Ula-Narrarae"
		birth_date = 724.1.1
		adm = 2
		dip = 0
		mil = 1
    }
	heir = {
 		name = "Lomaldydaa"
		monarch_name = "Lomaldydaa I"
		dynasty = "Ula-Narrarae"
		birth_date = 763.1.1
		death_date = 871.1.1
		claim = 100
		adm = 3
		dip = 4
		mil = 2
		female = yes
    }
}

871.1.1 = {
	monarch = {
 		name = "Cenedelin"
		dynasty = "Vae-Sena"
		birth_date = 819.1.1
		adm = 2
		dip = 0
		mil = 2
    }
}

925.1.1 = {
	monarch = {
 		name = "Symmu"
		dynasty = "Ula-Renne"
		birth_date = 897.1.1
		adm = 0
		dip = 6
		mil = 6
		female = yes
    }
}

1001.1.1 = {
	monarch = {
 		name = "Purru"
		dynasty = "Mea-Dusok"
		birth_date = 964.1.1
		adm = 5
		dip = 6
		mil = 2
		female = yes
    }
}

1056.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Ammas"
		monarch_name = "Ammas I"
		dynasty = "Vae-Sardavar"
		birth_date = 1041.1.1
		death_date = 1059.1.1
		claim = 100
		adm = 6
		dip = 3
		mil = 0
		female = yes
    }
}

1059.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Tjughandonos"
		monarch_name = "Tjughandonos I"
		dynasty = "Mea-Brina"
		birth_date = 1057.1.1
		death_date = 1075.1.1
		claim = 100
		adm = 4
		dip = 2
		mil = 4
    }
}

1075.1.1 = {
	monarch = {
 		name = "Symmu"
		dynasty = "Vae-Weyandawik"
		birth_date = 1043.1.1
		adm = 4
		dip = 5
		mil = 4
		female = yes
    }
	queen = {
 		name = "Fidhi"
		dynasty = "Vae-Weyandawik"
		birth_date = 1053.1.1
		adm = 4
		dip = 2
		mil = 5
    }
	heir = {
 		name = "Nenemmeve"
		monarch_name = "Nenemmeve I"
		dynasty = "Vae-Weyandawik"
		birth_date = 1070.1.1
		death_date = 1117.1.1
		claim = 100
		adm = 1
		dip = 0
		mil = 4
		female = yes
    }
}

1117.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Quaronaldil"
		monarch_name = "Quaronaldil I"
		dynasty = "Vae-Selertia"
		birth_date = 1113.1.1
		death_date = 1131.1.1
		claim = 100
		adm = 6
		dip = 0
		mil = 1
    }
}

1131.1.1 = {
	monarch = {
 		name = "Rorguuramis"
		dynasty = "		"
		birth_date = 1090.1.1
		adm = 6
		dip = 3
		mil = 1
    }
	queen = {
 		name = "Erraduure"
		dynasty = "		"
		birth_date = 1112.1.1
		adm = 2
		dip = 2
		mil = 0
		female = yes
    }
	heir = {
 		name = "Min"
		monarch_name = "Min I"
		dynasty = "		"
		birth_date = 1120.1.1
		death_date = 1169.1.1
		claim = 100
		adm = 6
		dip = 0
		mil = 6
    }
}

1169.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Niruugadant"
		monarch_name = "Niruugadant I"
		dynasty = "Vae-Weyandawik"
		birth_date = 1168.1.1
		death_date = 1186.1.1
		claim = 100
		adm = 5
		dip = 6
		mil = 3
    }
}

1186.1.1 = {
	monarch = {
 		name = "Leydel"
		dynasty = "Mea-Amane"
		birth_date = 1141.1.1
		adm = 0
		dip = 1
		mil = 5
    }
	queen = {
 		name = "Wimunmara"
		dynasty = "Mea-Amane"
		birth_date = 1163.1.1
		adm = 4
		dip = 6
		mil = 4
		female = yes
    }
	heir = {
 		name = "Cavannorel"
		monarch_name = "Cavannorel I"
		dynasty = "Mea-Amane"
		birth_date = 1176.1.1
		death_date = 1254.1.1
		claim = 100
		adm = 1
		dip = 5
		mil = 3
		female = yes
    }
}

1254.1.1 = {
	monarch = {
 		name = "Vond"
		dynasty = "Ula-Ryndenyse"
		birth_date = 1209.1.1
		adm = 5
		dip = 3
		mil = 2
    }
	queen = {
 		name = "Honnomule"
		dynasty = "Ula-Ryndenyse"
		birth_date = 1215.1.1
		adm = 2
		dip = 2
		mil = 1
		female = yes
    }
}

1306.1.1 = {
	monarch = {
 		name = "Gand"
		dynasty = "Ula-Nonungalo"
		birth_date = 1276.1.1
		adm = 0
		dip = 1
		mil = 4
    }
	queen = {
 		name = "Liva"
		dynasty = "Ula-Nonungalo"
		birth_date = 1254.1.1
		adm = 4
		dip = 6
		mil = 3
		female = yes
    }
	heir = {
 		name = "Wuria"
		monarch_name = "Wuria I"
		dynasty = "Ula-Nonungalo"
		birth_date = 1300.1.1
		death_date = 1348.1.1
		claim = 100
		adm = 0
		dip = 1
		mil = 6
		female = yes
    }
}

1348.1.1 = {
	monarch = {
 		name = "Lida"
		dynasty = "Mea-Anvil"
		birth_date = 1306.1.1
		adm = 4
		dip = 6
		mil = 5
    }
}

1408.1.1 = {
	monarch = {
 		name = "Varondil"
		dynasty = "Vae-Sercen"
		birth_date = 1381.1.1
		adm = 2
		dip = 6
		mil = 1
    }
	queen = {
 		name = "Curano"
		dynasty = "Vae-Sercen"
		birth_date = 1360.1.1
		adm = 6
		dip = 4
		mil = 0
		female = yes
    }
	heir = {
 		name = "Laloriaran"
		monarch_name = "Laloriaran I"
		dynasty = "Vae-Sercen"
		birth_date = 1402.1.1
		death_date = 1493.1.1
		claim = 100
		adm = 3
		dip = 3
		mil = 6
    }
}

1493.1.1 = {
	monarch = {
 		name = "Femde"
		dynasty = "Mea-Herillius"
		birth_date = 1456.1.1
		adm = 2
		dip = 6
		mil = 0
    }
	queen = {
 		name = "Helydu"
		dynasty = "Mea-Herillius"
		birth_date = 1461.1.1
		adm = 2
		dip = 3
		mil = 1
		female = yes
    }
	heir = {
 		name = "Nuralanya"
		monarch_name = "Nuralanya I"
		dynasty = "Mea-Herillius"
		birth_date = 1488.1.1
		death_date = 1555.1.1
		claim = 100
		adm = 6
		dip = 1
		mil = 0
		female = yes
    }
}

1555.1.1 = {
	monarch = {
 		name = "Ryland"
		dynasty = "Vae-White-Gold																																																																																																																																																																																		"
		birth_date = 1513.1.1
		adm = 4
		dip = 0
		mil = 3
    }
}

1619.1.1 = {
	monarch = {
 		name = "Nym"
		dynasty = "Vae-Tasaso"
		birth_date = 1569.1.1
		adm = 6
		dip = 2
		mil = 5
    }
	queen = {
 		name = "Lannessa"
		dynasty = "Vae-Tasaso"
		birth_date = 1568.1.1
		adm = 6
		dip = 5
		mil = 6
		female = yes
    }
	heir = {
 		name = "Tjosaunt"
		monarch_name = "Tjosaunt I"
		dynasty = "Vae-Tasaso"
		birth_date = 1606.1.1
		death_date = 1706.1.1
		claim = 100
		adm = 3
		dip = 4
		mil = 5
    }
}

1706.1.1 = {
	monarch = {
 		name = "Cumhul"
		dynasty = "Mea-Calabolis"
		birth_date = 1666.1.1
		adm = 3
		dip = 0
		mil = 5
    }
	queen = {
 		name = "Sullidhash"
		dynasty = "Mea-Calabolis"
		birth_date = 1664.1.1
		adm = 0
		dip = 5
		mil = 4
		female = yes
    }
	heir = {
 		name = "Nilichi"
		monarch_name = "Nilichi I"
		dynasty = "Mea-Calabolis"
		birth_date = 1698.1.1
		death_date = 1747.1.1
		claim = 100
		adm = 3
		dip = 4
		mil = 3
    }
}

1747.1.1 = {
	monarch = {
 		name = "Toduurhasym"
		dynasty = "Vae-Weatherleah"
		birth_date = 1726.1.1
		adm = 6
		dip = 5
		mil = 5
    }
}

1824.1.1 = {
	monarch = {
 		name = "Erridhish"
		dynasty = "Mea-Andrulusus"
		birth_date = 1779.1.1
		adm = 4
		dip = 5
		mil = 2
		female = yes
    }
	queen = {
 		name = "Lagan"
		dynasty = "Mea-Andrulusus"
		birth_date = 1795.1.1
		adm = 1
		dip = 3
		mil = 1
    }
	heir = {
 		name = "Hunnul"
		monarch_name = "Hunnul I"
		dynasty = "Mea-Andrulusus"
		birth_date = 1818.1.1
		death_date = 1897.1.1
		claim = 100
		adm = 5
		dip = 2
		mil = 0
		female = yes
    }
}

1897.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Haromir"
		monarch_name = "Haromir I"
		dynasty = "Ula-Linchal"
		birth_date = 1884.1.1
		death_date = 1902.1.1
		claim = 100
		adm = 3
		dip = 1
		mil = 4
    }
}

1902.1.1 = {
	monarch = {
 		name = "Lenalmel"
		dynasty = "Ula-Oressinia"
		birth_date = 1865.1.1
		adm = 3
		dip = 4
		mil = 4
    }
}

1982.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Suhe"
		monarch_name = "Suhe I"
		dynasty = "Vae-Vietia"
		birth_date = 1968.1.1
		death_date = 1986.1.1
		claim = 100
		adm = 0
		dip = 6
		mil = 4
		female = yes
    }
}

1986.1.1 = {
	monarch = {
 		name = "Hunro"
		dynasty = "Vae-Sialerius"
		birth_date = 1942.1.1
		adm = 6
		dip = 3
		mil = 4
    }
	queen = {
 		name = "Illarrymo"
		dynasty = "Vae-Sialerius"
		birth_date = 1959.1.1
		adm = 3
		dip = 1
		mil = 3
		female = yes
    }
}

2039.1.1 = {
	monarch = {
 		name = "Laloriaran"
		dynasty = "Vae-Sialerius"
		birth_date = 2012.1.1
		adm = 1
		dip = 0
		mil = 0
    }
	queen = {
 		name = "Hennynlyho"
		dynasty = "Vae-Sialerius"
		birth_date = 1999.1.1
		adm = 5
		dip = 6
		mil = 6
		female = yes
    }
	heir = {
 		name = "Dini"
		monarch_name = "Dini I"
		dynasty = "Vae-Sialerius"
		birth_date = 2036.1.1
		death_date = 2130.1.1
		claim = 100
		adm = 3
		dip = 5
		mil = 2
		female = yes
    }
}

2130.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Yrru"
		monarch_name = "Yrru I"
		dynasty = "Ula-Matuseius"
		birth_date = 2123.1.1
		death_date = 2141.1.1
		claim = 100
		adm = 2
		dip = 4
		mil = 6
		female = yes
    }
}

2141.1.1 = {
	monarch = {
 		name = "Uhidhon"
		dynasty = "Vae-Sylolvia"
		birth_date = 2092.1.1
		adm = 4
		dip = 6
		mil = 1
		female = yes
    }
}

2207.1.1 = {
	monarch = {
 		name = "Qystur"
		dynasty = "Ula-Nonungalo"
		birth_date = 2155.1.1
		adm = 6
		dip = 0
		mil = 3
    }
	queen = {
 		name = "Elonlaris"
		dynasty = "Ula-Nonungalo"
		birth_date = 2165.1.1
		adm = 0
		dip = 3
		mil = 4
		female = yes
    }
	heir = {
 		name = "Sylanwesh"
		monarch_name = "Sylanwesh I"
		dynasty = "Ula-Nonungalo"
		birth_date = 2202.1.1
		death_date = 2257.1.1
		claim = 100
		adm = 3
		dip = 2
		mil = 3
		female = yes
    }
}

2257.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Fyndiscas"
		monarch_name = "Fyndiscas I"
		dynasty = "Vae-Sialerius"
		birth_date = 2246.1.1
		death_date = 2264.1.1
		claim = 100
		adm = 2
		dip = 1
		mil = 0
    }
}

2264.1.1 = {
	monarch = {
 		name = "Endarre"
		dynasty = "Mea-Garlas"
		birth_date = 2211.1.1
		adm = 1
		dip = 5
		mil = 0
    }
	queen = {
 		name = "Hunne"
		dynasty = "Mea-Garlas"
		birth_date = 2214.1.1
		adm = 5
		dip = 3
		mil = 6
		female = yes
    }
	heir = {
 		name = "Harrylnes"
		monarch_name = "Harrylnes I"
		dynasty = "Mea-Garlas"
		birth_date = 2250.1.1
		death_date = 2345.1.1
		claim = 100
		adm = 2
		dip = 2
		mil = 5
		female = yes
    }
}

2345.1.1 = {
	monarch = {
 		name = "Dedhan"
		dynasty = "Mea-Culotte"
		birth_date = 2326.1.1
		adm = 5
		dip = 3
		mil = 0
    }
}

2401.1.1 = {
	monarch = {
 		name = "Pehi"
		dynasty = "Vae-Vanua"
		birth_date = 2371.1.1
		adm = 3
		dip = 2
		mil = 4
		female = yes
    }
	queen = {
 		name = "Haromir"
		dynasty = "Vae-Vanua"
		birth_date = 2378.1.1
		adm = 0
		dip = 1
		mil = 3
    }
	heir = {
 		name = "Ynnylsasul"
		monarch_name = "Ynnylsasul I"
		dynasty = "Vae-Vanua"
		birth_date = 2393.1.1
		death_date = 2491.1.1
		claim = 100
		adm = 4
		dip = 6
		mil = 2
		female = yes
    }
}

2491.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Tjuunrerhen"
		monarch_name = "Tjuunrerhen I"
		dynasty = "Vae-Sena"
		birth_date = 2476.1.1
		death_date = 2494.1.1
		claim = 100
		adm = 2
		dip = 5
		mil = 5
    }
}

2494.1.1 = {
	monarch = {
 		name = "Nula"
		dynasty = "Ula-Julidonea"
		birth_date = 2446.1.1
		adm = 1
		dip = 2
		mil = 6
		female = yes
    }
}

