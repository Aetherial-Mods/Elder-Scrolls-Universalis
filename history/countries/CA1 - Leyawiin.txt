government = republic
government_rank = 3
mercantilism = 1
technology_group = cyrodiil_tg
religion = nedic_pantheon
primary_culture = nedic
capital = 1120

54.1.1 = {
	monarch = {
 		name = "Ytas"
		dynasty = "Malrasoc"
		birth_date = 18.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

90.1.1 = {
	monarch = {
 		name = "Dadrades"
		dynasty = "Tudaarin"
		birth_date = 43.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

162.1.1 = {
	monarch = {
 		name = "Zakysor"
		dynasty = "Gilraoles"
		birth_date = 117.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

217.1.1 = {
	monarch = {
 		name = "Malrasoc"
		dynasty = "Lukaazon"
		birth_date = 180.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

262.1.1 = {
	monarch = {
 		name = "Tylak"
		dynasty = "Dauhrak"
		birth_date = 242.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

307.1.1 = {
	monarch = {
 		name = "Gilraoles"
		dynasty = "Tirdor"
		birth_date = 277.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

378.1.1 = {
	monarch = {
 		name = "Gahlaras"
		dynasty = "Amedoc"
		birth_date = 328.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

427.1.1 = {
	monarch = {
 		name = "Remaasok"
		dynasty = "Vinhylak"
		birth_date = 393.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

499.1.1 = {
	monarch = {
 		name = "Runhath"
		dynasty = "Myhros"
		birth_date = 453.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

576.1.1 = {
	monarch = {
 		name = "Ridravin"
		dynasty = "Muvmec"
		birth_date = 552.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

669.1.1 = {
	monarch = {
 		name = "Dame"
		dynasty = "Kynhesac"
		birth_date = 630.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

711.1.1 = {
	monarch = {
 		name = "Naonhaudes"
		dynasty = "Zunhe"
		birth_date = 685.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

747.1.1 = {
	monarch = {
 		name = "Horremel"
		dynasty = "Vaulic"
		birth_date = 696.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

817.1.1 = {
	monarch = {
 		name = "Ridravin"
		dynasty = "Ralreloc"
		birth_date = 783.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

904.1.1 = {
	monarch = {
 		name = "Dame"
		dynasty = "Kalrin"
		birth_date = 870.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

981.1.1 = {
	monarch = {
 		name = "Naonhaudes"
		dynasty = "Dastith"
		birth_date = 959.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1043.1.1 = {
	monarch = {
 		name = "Lashavun"
		dynasty = "Nuvner"
		birth_date = 994.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1103.1.1 = {
	monarch = {
 		name = "Gaohen"
		dynasty = "Nhystareth"
		birth_date = 1075.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1150.1.1 = {
	monarch = {
 		name = "Ilirah"
		dynasty = "Ridravin"
		birth_date = 1128.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1210.1.1 = {
	monarch = {
 		name = "Gumrok"
		dynasty = "Ihrasek"
		birth_date = 1187.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1258.1.1 = {
	monarch = {
 		name = "Lashavun"
		dynasty = "Riralath"
		birth_date = 1235.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1297.1.1 = {
	monarch = {
 		name = "Resi"
		dynasty = "Idith"
		birth_date = 1271.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1360.1.1 = {
	monarch = {
 		name = "Ilirah"
		dynasty = "Shelren"
		birth_date = 1325.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1422.1.1 = {
	monarch = {
 		name = "Ranni"
		dynasty = "Vrustimek"
		birth_date = 1369.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1500.1.1 = {
	monarch = {
 		name = "Oshan"
		dynasty = "Ridravin"
		birth_date = 1454.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1579.1.1 = {
	monarch = {
 		name = "Nirlyloth"
		dynasty = "Vristidok"
		birth_date = 1558.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1625.1.1 = {
	monarch = {
 		name = "Talih"
		dynasty = "Nuvner"
		birth_date = 1599.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1672.1.1 = {
	monarch = {
 		name = "Lihareh"
		dynasty = "Aroc"
		birth_date = 1640.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1767.1.1 = {
	monarch = {
 		name = "Zahoti"
		dynasty = "Nharmok"
		birth_date = 1733.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1804.1.1 = {
	monarch = {
 		name = "Disseh"
		dynasty = "Aalezos"
		birth_date = 1782.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1867.1.1 = {
	monarch = {
 		name = "Naamaulec"
		dynasty = "Zyrdusor"
		birth_date = 1841.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1963.1.1 = {
	monarch = {
 		name = "Zunhe"
		dynasty = "Nhystareth"
		birth_date = 1914.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2048.1.1 = {
	monarch = {
 		name = "Dezone"
		dynasty = "Vauleso"
		birth_date = 2029.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

2098.1.1 = {
	monarch = {
 		name = "Lukaazon"
		dynasty = "Kynhesac"
		birth_date = 2067.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2155.1.1 = {
	monarch = {
 		name = "Lysteth"
		dynasty = "Tirleros"
		birth_date = 2111.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2226.1.1 = {
	monarch = {
 		name = "Nestith"
		dynasty = "Vinhylak"
		birth_date = 2206.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2298.1.1 = {
	monarch = {
 		name = "Shelren"
		dynasty = "Vredraonir"
		birth_date = 2279.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2337.1.1 = {
	monarch = {
 		name = "Lukaazon"
		dynasty = "Kynhesac"
		birth_date = 2309.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2374.1.1 = {
	monarch = {
 		name = "Nhekhedin"
		dynasty = "Ridravin"
		birth_date = 2342.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2447.1.1 = {
	monarch = {
 		name = "Reshoha"
		dynasty = "Likoth"
		birth_date = 2413.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

