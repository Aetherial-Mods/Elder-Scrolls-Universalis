government = theocracy
government_rank = 5
mercantilism = 1
technology_group = dwemer_tg
religion = reason_and_logic_cult
primary_culture = dwemer
capital = 1062

add_country_modifier = { name = "heart_of_lorkhan" duration = -1 }

54.1.1 = {
	monarch = {
 		name = "Ychogrenz"
		dynasty = "Aq'Bluhzis"
		birth_date = 14.1.1
		adm = 2
		dip = 2
		mil = 3
    }
}

93.1.1 = {
	monarch = {
 		name = "Rhzomrond"
		dynasty = "Av'Chragrenz"
		birth_date = 56.1.1
		adm = 0
		dip = 1
		mil = 0
    }
}

152.1.1 = {
	monarch = {
 		name = "Ynzarlakch"
		dynasty = "Az'Yzrahrada"
		birth_date = 124.1.1
		adm = 5
		dip = 1
		mil = 3
		female = yes
    }
}

218.1.1 = {
	monarch = {
 		name = "Choadzach"
		dynasty = "Aq'Ksredlin"
		birth_date = 191.1.1
		adm = 0
		dip = 2
		mil = 5
		female = yes
    }
}

259.1.1 = {
	monarch = {
 		name = "Mlirtes"
		dynasty = "Aq'Snebchasz"
		birth_date = 227.1.1
		adm = 5
		dip = 1
		mil = 1
		female = yes
    }
}

347.1.1 = {
	monarch = {
 		name = "Jholzarf"
		dynasty = "Av'Djubchasz"
		birth_date = 312.1.1
		adm = 3
		dip = 0
		mil = 5
    }
}

390.1.1 = {
	monarch = {
 		name = "Ksredlin"
		dynasty = "Av'Ihlefuan"
		birth_date = 340.1.1
		adm = 2
		dip = 0
		mil = 2
		female = yes
    }
}

450.1.1 = {
	monarch = {
 		name = "Mhanrazg"
		dynasty = "Af'Inrarlakch"
		birth_date = 425.1.1
		adm = 0
		dip = 6
		mil = 5
    }
}

514.1.1 = {
	monarch = {
 		name = "Jhourlac"
		dynasty = "Av'Klazgar"
		birth_date = 478.1.1
		adm = 5
		dip = 5
		mil = 2
    }
}

575.1.1 = {
	monarch = {
 		name = "Jholzarf"
		dynasty = "Az'Nohnch"
		birth_date = 550.1.1
		adm = 3
		dip = 4
		mil = 6
    }
}

613.1.1 = {
	monarch = {
 		name = "Mhunac"
		dynasty = "Av'Czavzyrn"
		birth_date = 566.1.1
		adm = 2
		dip = 3
		mil = 2
    }
}

685.1.1 = {
	monarch = {
 		name = "Drunrynn"
		dynasty = "Az'Cfranhatch"
		birth_date = 662.1.1
		adm = 4
		dip = 5
		mil = 4
		female = yes
    }
}

767.1.1 = {
	monarch = {
 		name = "Jhourlac"
		dynasty = "Av'Brazzedit"
		birth_date = 736.1.1
		adm = 2
		dip = 4
		mil = 0
    }
}

826.1.1 = {
	monarch = {
 		name = "Dzreglan"
		dynasty = "Af'Cuohrek"
		birth_date = 794.1.1
		adm = 0
		dip = 3
		mil = 4
		female = yes
    }
}

891.1.1 = {
	monarch = {
 		name = "Szoglynsh"
		dynasty = "Af'Asrahuanch"
		birth_date = 844.1.1
		adm = 5
		dip = 2
		mil = 1
		female = yes
    }
}

947.1.1 = {
	monarch = {
 		name = "Ilzeglan"
		dynasty = "Av'Ralen"
		birth_date = 915.1.1
		adm = 3
		dip = 1
		mil = 4
		female = yes
    }
}

1038.1.1 = {
	monarch = {
 		name = "Ghaznak"
		dynasty = "Az'Ksrefurn"
		birth_date = 998.1.1
		adm = 2
		dip = 1
		mil = 1
    }
}

1122.1.1 = {
	monarch = {
 		name = "Ylrefwinn"
		dynasty = "Az'Djuhnch"
		birth_date = 1087.1.1
		adm = 0
		dip = 0
		mil = 5
		female = yes
    }
}

1182.1.1 = {
	monarch = {
 		name = "Achyggo"
		dynasty = "Af'Kolzarf"
		birth_date = 1149.1.1
		adm = 1
		dip = 4
		mil = 3
    }
}

1254.1.1 = {
	monarch = {
 		name = "Cuohrek"
		dynasty = "Az'Yabaln"
		birth_date = 1206.1.1
		adm = 6
		dip = 3
		mil = 6
    }
}

1316.1.1 = {
	monarch = {
 		name = "Snelarn"
		dynasty = "Av'Dzragvin"
		birth_date = 1281.1.1
		adm = 4
		dip = 2
		mil = 3
    }
}

1402.1.1 = {
	monarch = {
 		name = "Nromgunch"
		dynasty = "Az'Jravarn"
		birth_date = 1370.1.1
		adm = 2
		dip = 2
		mil = 0
		female = yes
    }
}

1467.1.1 = {
	monarch = {
 		name = "Grabnanch"
		dynasty = "Av'Davlar"
		birth_date = 1438.1.1
		adm = 4
		dip = 3
		mil = 1
    }
}

1509.1.1 = {
	monarch = {
 		name = "Mherbira"
		dynasty = "Az'Ryumorn"
		birth_date = 1460.1.1
		adm = 3
		dip = 2
		mil = 5
		female = yes
    }
}

1559.1.1 = {
	monarch = {
 		name = "Asrahuanch"
		dynasty = "Az'Yzranrida"
		birth_date = 1541.1.1
		adm = 1
		dip = 1
		mil = 1
    }
}

1624.1.1 = {
	monarch = {
 		name = "Sohner"
		dynasty = "Af'Ychogarn"
		birth_date = 1582.1.1
		adm = 6
		dip = 0
		mil = 5
		female = yes
    }
}

1662.1.1 = {
	monarch = {
 		name = "Djubchasz"
		dynasty = "Az'Irdarlis"
		birth_date = 1609.1.1
		adm = 4
		dip = 0
		mil = 2
    }
}

1742.1.1 = {
	monarch = {
 		name = "Ralen"
		dynasty = "Az'Ksrefurn"
		birth_date = 1723.1.1
		adm = 3
		dip = 6
		mil = 5
    }
}

1789.1.1 = {
	monarch = {
 		name = "Asrahuanch"
		dynasty = "Af'Crafrysz"
		birth_date = 1758.1.1
		adm = 1
		dip = 5
		mil = 2
    }
}

1888.1.1 = {
	monarch = {
 		name = "Klalzrak"
		dynasty = "Aq'Asradlin"
		birth_date = 1841.1.1
		adm = 6
		dip = 4
		mil = 6
    }
}

1969.1.1 = {
	monarch = {
 		name = "Djubchasz"
		dynasty = "Av'Bzraban"
		birth_date = 1919.1.1
		adm = 1
		dip = 5
		mil = 0
    }
}

2004.1.1 = {
	monarch = {
 		name = "Ralen"
		dynasty = "Af'Mchavin"
		birth_date = 1975.1.1
		adm = 6
		dip = 5
		mil = 4
    }
}

2071.1.1 = {
	monarch = {
 		name = "Chruhnch"
		dynasty = "Av'Jholzarf"
		birth_date = 2025.1.1
		adm = 4
		dip = 4
		mil = 1
    }
}

2135.1.1 = {
	monarch = {
 		name = "Rkudit"
		dynasty = "Af'Mchavin"
		birth_date = 2109.1.1
		adm = 3
		dip = 3
		mil = 4
    }
}

2206.1.1 = {
	monarch = {
 		name = "Batvar"
		dynasty = "Af'Agahuanch"
		birth_date = 2185.1.1
		adm = 1
		dip = 2
		mil = 1
    }
}

2244.1.1 = {
	monarch = {
 		name = "Ythamac"
		dynasty = "Aq'Chzefrach"
		birth_date = 2207.1.1
		adm = 6
		dip = 2
		mil = 4
    }
}

2281.1.1 = {
	monarch = {
 		name = "Chruhnch"
		dynasty = "Av'Ihlefuan"
		birth_date = 2248.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2360.1.1 = {
	monarch = {
 		name = "Brehron"
		dynasty = "Az'Aknanwess"
		birth_date = 2321.1.1
		adm = 3
		dip = 0
		mil = 5
    }
}

2407.1.1 = {
	monarch = {
 		name = "Ychohnch"
		dynasty = "Af'Goundam"
		birth_date = 2387.1.1
		adm = 5
		dip = 1
		mil = 6
		female = yes
    }
}

2443.1.1 = {
	monarch = {
 		name = "Ychogarn"
		dynasty = "Aq'Krilnif"
		birth_date = 2407.1.1
		adm = 3
		dip = 0
		mil = 3
    }
}

2499.1.1 = {
	monarch = {
 		name = "Mhuzgar"
		dynasty = "Af'Alnogwetch"
		birth_date = 2474.1.1
		adm = 1
		dip = 0
		mil = 0
    }
}

