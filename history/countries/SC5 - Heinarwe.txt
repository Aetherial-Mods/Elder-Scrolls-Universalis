government = republic
government_rank = 1
mercantilism = 1
technology_group = elven_tg
religion = altmeri_pantheon
primary_culture = altmer
capital = 4714
# = 4787

54.1.1 = {
	monarch = {
 		name = "Rulanir"
		dynasty = "Wel-Aelsoniane"
		birth_date = 36.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

130.1.1 = {
	monarch = {
 		name = "Alanwe"
		dynasty = "Bel-Shaaninde"
		birth_date = 90.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

200.1.1 = {
	monarch = {
 		name = "Ruuvitar"
		dynasty = "Ael-Ohterane"
		birth_date = 155.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

263.1.1 = {
	monarch = {
 		name = "Lairiwene"
		dynasty = "Wel-Ardende"
		birth_date = 242.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

333.1.1 = {
	monarch = {
 		name = "Curilma"
		dynasty = "Bel-Taanye"
		birth_date = 286.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

419.1.1 = {
	monarch = {
 		name = "Alande"
		dynasty = "Bel-Shattered"
		birth_date = 388.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

490.1.1 = {
	monarch = {
 		name = "Runil"
		dynasty = "Bel-Shaaninde"
		birth_date = 440.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

540.1.1 = {
	monarch = {
 		name = "Laindilaure"
		dynasty = "Ael-Graddun"
		birth_date = 520.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

575.1.1 = {
	monarch = {
 		name = "Eryon"
		dynasty = "Ael-Lorusse"
		birth_date = 549.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

618.1.1 = {
	monarch = {
 		name = "Merandil"
		dynasty = "Ael-Relte"
		birth_date = 596.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

671.1.1 = {
	monarch = {
 		name = "Falandil"
		dynasty = "Ael-Granaire"
		birth_date = 626.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

725.1.1 = {
	monarch = {
 		name = "Aldononde"
		dynasty = "Wel-Camiana"
		birth_date = 691.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

822.1.1 = {
	monarch = {
 		name = "Salelale"
		dynasty = "Ael-Phaer"
		birth_date = 769.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

906.1.1 = {
	monarch = {
 		name = "Meneval"
		dynasty = "Ael-Mirlenya"
		birth_date = 876.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

992.1.1 = {
	monarch = {
 		name = "Fairimo"
		dynasty = "Ael-Kardnar"
		birth_date = 939.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1055.1.1 = {
	monarch = {
 		name = "Aminyon"
		dynasty = "Ael-Granaire"
		birth_date = 1019.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1121.1.1 = {
	monarch = {
 		name = "Sarohanar"
		dynasty = "Bel-Silalaure"
		birth_date = 1073.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1196.1.1 = {
	monarch = {
 		name = "Angardil"
		dynasty = "Bel-Valia"
		birth_date = 1154.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1270.1.1 = {
	monarch = {
 		name = "Sircantir"
		dynasty = "Ael-Lorusse"
		birth_date = 1246.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1364.1.1 = {
	monarch = {
 		name = "Miremonwe"
		dynasty = "Wel-Alkinosin"
		birth_date = 1336.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1428.1.1 = {
	monarch = {
 		name = "Elaasare"
		dynasty = "Bel-Valaena"
		birth_date = 1406.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1486.1.1 = {
	monarch = {
 		name = "Andulalion"
		dynasty = "Wel-Elsinthaer"
		birth_date = 1446.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1522.1.1 = {
	monarch = {
 		name = "Serit"
		dynasty = "Ael-Jaerfhaer"
		birth_date = 1488.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1582.1.1 = {
	monarch = {
 		name = "Mindil"
		dynasty = "Ael-Relnnarre"
		birth_date = 1547.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1631.1.1 = {
	monarch = {
 		name = "Fanorne"
		dynasty = "Wel-Elate"
		birth_date = 1587.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1699.1.1 = {
	monarch = {
 		name = "Lliae"
		dynasty = "Bel-Sondsara"
		birth_date = 1680.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1743.1.1 = {
	monarch = {
 		name = "Fendinmin"
		dynasty = "Ael-Firsthold"
		birth_date = 1690.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1832.1.1 = {
	monarch = {
 		name = "Aranath"
		dynasty = "Ael-Kaefhaer"
		birth_date = 1785.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1905.1.1 = {
	monarch = {
 		name = "Sirinduure"
		dynasty = "Ael-Lorusse"
		birth_date = 1882.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1995.1.1 = {
	monarch = {
 		name = "Carillda"
		dynasty = "Wel-Aryria"
		birth_date = 1975.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

2046.1.1 = {
	monarch = {
 		name = "Hiradil"
		dynasty = "Bel-Sondnwe"
		birth_date = 2012.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

2112.1.1 = {
	monarch = {
 		name = "Elondil"
		dynasty = "Wel-Eleaninde"
		birth_date = 2061.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2153.1.1 = {
	monarch = {
 		name = "Vastarie"
		dynasty = "Bel-Terna"
		birth_date = 2134.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

2232.1.1 = {
	monarch = {
 		name = "Quarantar"
		dynasty = "Bel-Silsailen"
		birth_date = 2210.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2296.1.1 = {
	monarch = {
 		name = "Leythen"
		dynasty = "Bel-Valnoore"
		birth_date = 2264.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2332.1.1 = {
	monarch = {
 		name = "Ceryolminwe"
		dynasty = "Bel-Slaughter"
		birth_date = 2292.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

2414.1.1 = {
	monarch = {
 		name = "Vanendil"
		dynasty = "Bel-Sontarya"
		birth_date = 2389.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2458.1.1 = {
	monarch = {
 		name = "Quanomil"
		dynasty = "Ael-Nirae"
		birth_date = 2406.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

