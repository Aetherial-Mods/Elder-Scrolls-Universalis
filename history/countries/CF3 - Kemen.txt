government = theocracy
government_rank = 1
mercantilism = 1
technology_group = cyrodiil_tg
religion = hircine_cult
primary_culture = ayleid
capital = 5423

54.1.1 = {
	monarch = {
 		name = "Cyrheganund"
		dynasty = "Ula-Renne"
		birth_date = 17.1.1
		adm = 5
		dip = 0
		mil = 4
    }
}

112.1.1 = {
	monarch = {
 		name = "Pemu"
		dynasty = "Mea-Bravil"
		birth_date = 61.1.1
		adm = 3
		dip = 0
		mil = 0
		female = yes
    }
}

147.1.1 = {
	monarch = {
 		name = "Hergor"
		dynasty = "Vae-Sardavar"
		birth_date = 102.1.1
		adm = 1
		dip = 6
		mil = 4
    }
}

210.1.1 = {
	monarch = {
 		name = "Nogo"
		dynasty = "Mea-Gince"
		birth_date = 163.1.1
		adm = 6
		dip = 5
		mil = 1
    }
}

271.1.1 = {
	monarch = {
 		name = "Vanhur"
		dynasty = "Mea-Bravil"
		birth_date = 248.1.1
		adm = 5
		dip = 4
		mil = 4
    }
}

332.1.1 = {
	monarch = {
 		name = "Pemu"
		dynasty = "Ula-Nenyond"
		birth_date = 306.1.1
		adm = 0
		dip = 5
		mil = 6
		female = yes
    }
}

406.1.1 = {
	monarch = {
 		name = "Erraduure"
		dynasty = "Vae-Vautellia"
		birth_date = 354.1.1
		adm = 5
		dip = 5
		mil = 2
		female = yes
    }
}

463.1.1 = {
	monarch = {
 		name = "Masnam"
		dynasty = "Mea-Hastrel"
		birth_date = 434.1.1
		adm = 3
		dip = 4
		mil = 6
    }
}

503.1.1 = {
	monarch = {
 		name = "Laloriaran"
		dynasty = "Mea-Culotte"
		birth_date = 453.1.1
		adm = 1
		dip = 3
		mil = 3
    }
}

576.1.1 = {
	monarch = {
 		name = "Qon"
		dynasty = "Ula-Oressinia"
		birth_date = 555.1.1
		adm = 0
		dip = 2
		mil = 6
    }
}

648.1.1 = {
	monarch = {
 		name = "Qegylomo"
		dynasty = "Ula-Isollaise"
		birth_date = 626.1.1
		adm = 5
		dip = 1
		mil = 3
    }
}

687.1.1 = {
	monarch = {
 		name = "Tjan"
		dynasty = "Vae-Vlastarus"
		birth_date = 646.1.1
		adm = 3
		dip = 1
		mil = 0
    }
}

752.1.1 = {
	monarch = {
 		name = "Lanath"
		dynasty = "Ula-Nornalhorst"
		birth_date = 727.1.1
		adm = 1
		dip = 0
		mil = 3
		female = yes
    }
}

830.1.1 = {
	monarch = {
 		name = "Azu"
		dynasty = "Vae-Selviloria"
		birth_date = 786.1.1
		adm = 3
		dip = 1
		mil = 5
		female = yes
    }
}

882.1.1 = {
	monarch = {
 		name = "Qegylomo"
		dynasty = "Vae-Vlastarus"
		birth_date = 859.1.1
		adm = 1
		dip = 0
		mil = 1
    }
}

943.1.1 = {
	monarch = {
 		name = "Mezol"
		dynasty = "Ula-Nornalhorst"
		birth_date = 919.1.1
		adm = 0
		dip = 0
		mil = 5
		female = yes
    }
}

991.1.1 = {
	monarch = {
 		name = "Fin"
		dynasty = "Ula-Ryndenyse"
		birth_date = 948.1.1
		adm = 5
		dip = 6
		mil = 2
    }
}

1045.1.1 = {
	monarch = {
 		name = "Heystuadhisind"
		dynasty = "Mea-Conoa"
		birth_date = 1009.1.1
		adm = 3
		dip = 5
		mil = 5
    }
}

1082.1.1 = {
	monarch = {
 		name = "Nuralanya"
		dynasty = "Ula-Nonungalo"
		birth_date = 1043.1.1
		adm = 1
		dip = 4
		mil = 2
		female = yes
    }
}

1158.1.1 = {
	monarch = {
 		name = "Cymylmuumer"
		dynasty = "Mea-Bromma"
		birth_date = 1116.1.1
		adm = 0
		dip = 3
		mil = 6
    }
}

1210.1.1 = {
	monarch = {
 		name = "Mevirlen"
		dynasty = "Ula-Linchal"
		birth_date = 1183.1.1
		adm = 5
		dip = 3
		mil = 2
		female = yes
    }
}

1260.1.1 = {
	monarch = {
 		name = "Mene"
		dynasty = "Ula-Rusifus"
		birth_date = 1211.1.1
		adm = 0
		dip = 4
		mil = 4
		female = yes
    }
}

1295.1.1 = {
	monarch = {
 		name = "Myndhal"
		dynasty = "Ula-Plalusius"
		birth_date = 1270.1.1
		adm = 5
		dip = 3
		mil = 0
    }
}

1391.1.1 = {
	monarch = {
 		name = "Djun"
		dynasty = "Vae-Sardavar"
		birth_date = 1360.1.1
		adm = 3
		dip = 2
		mil = 4
    }
}

1440.1.1 = {
	monarch = {
 		name = "Onya"
		dynasty = "Vae-Vietia"
		birth_date = 1397.1.1
		adm = 6
		dip = 4
		mil = 6
		female = yes
    }
}

1491.1.1 = {
	monarch = {
 		name = "Cavannorel"
		dynasty = "Mea-Herillius"
		birth_date = 1472.1.1
		adm = 0
		dip = 1
		mil = 4
		female = yes
    }
}

1582.1.1 = {
	monarch = {
 		name = "Elis"
		dynasty = "Mea-Brina"
		birth_date = 1543.1.1
		adm = 6
		dip = 4
		mil = 5
		female = yes
    }
}

1658.1.1 = {
	monarch = {
 		name = "Nanalne"
		dynasty = "Mea-Amane"
		birth_date = 1610.1.1
		adm = 4
		dip = 3
		mil = 1
		female = yes
    }
}

1705.1.1 = {
	monarch = {
 		name = "Lyzelniniath"
		dynasty = "Vae-Vanua"
		birth_date = 1682.1.1
		adm = 3
		dip = 2
		mil = 5
		female = yes
    }
}

1785.1.1 = {
	monarch = {
 		name = "Suhe"
		dynasty = "Vae-Vonara"
		birth_date = 1765.1.1
		adm = 3
		dip = 0
		mil = 3
		female = yes
    }
}

1865.1.1 = {
	monarch = {
 		name = "Gisegilin"
		dynasty = "Vae-Sarmofza"
		birth_date = 1819.1.1
		adm = 2
		dip = 6
		mil = 0
    }
}

1951.1.1 = {
	monarch = {
 		name = "Poman"
		dynasty = "Ula-Niryastare"
		birth_date = 1913.1.1
		adm = 0
		dip = 5
		mil = 3
		female = yes
    }
}

2010.1.1 = {
	monarch = {
 		name = "Soreshi"
		dynasty = "Ula-Homestead"
		birth_date = 1977.1.1
		adm = 5
		dip = 4
		mil = 0
		female = yes
    }
}

2077.1.1 = {
	monarch = {
 		name = "Cogir"
		dynasty = "Ula-Jorarienna"
		birth_date = 2058.1.1
		adm = 3
		dip = 3
		mil = 4
    }
}

2169.1.1 = {
	monarch = {
 		name = "Quaronaldil"
		dynasty = "Ula-Horuria"
		birth_date = 2146.1.1
		adm = 2
		dip = 3
		mil = 0
    }
}

2266.1.1 = {
	monarch = {
 		name = "Ymmilune"
		dynasty = "Mea-Corannus"
		birth_date = 2217.1.1
		adm = 0
		dip = 2
		mil = 4
		female = yes
    }
}

2327.1.1 = {
	monarch = {
 		name = "Orhualmuunint"
		dynasty = "		"
		birth_date = 2285.1.1
		adm = 5
		dip = 1
		mil = 0
    }
}

2399.1.1 = {
	monarch = {
 		name = "Niruugadant"
		dynasty = "Ula-Rusifus"
		birth_date = 2350.1.1
		adm = 0
		dip = 2
		mil = 2
    }
}

2450.1.1 = {
	monarch = {
 		name = "Quaronaldil"
		dynasty = "		"
		birth_date = 2417.1.1
		adm = 5
		dip = 1
		mil = 6
    }
}

