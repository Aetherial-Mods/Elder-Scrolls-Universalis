government = native
government_rank = 1
mercantilism = 1
technology_group = orsimer_tg
religion = malacath_cult
primary_culture = riekr
capital = 7010

54.1.1 = {
	monarch = {
 		name = "Chizuil"
		dynasty = "Stulk"
		birth_date = 25.1.1
		adm = 0
		dip = 5
		mil = 1
    }
}

94.1.1 = {
	monarch = {
 		name = "Gnygs"
		dynasty = "Sickfoot"
		birth_date = 60.1.1
		adm = 5
		dip = 4
		mil = 4
    }
}

140.1.1 = {
	monarch = {
 		name = "Fral"
		dynasty = "Urga"
		birth_date = 100.1.1
		adm = 3
		dip = 4
		mil = 1
    }
}

191.1.1 = {
	monarch = {
 		name = "Shaatruige"
		dynasty = "Belch"
		birth_date = 160.1.1
		adm = 1
		dip = 3
		mil = 5
		female = yes
    }
}

270.1.1 = {
	monarch = {
 		name = "Gleek"
		dynasty = "Slimearms"
		birth_date = 232.1.1
		adm = 0
		dip = 2
		mil = 1
    }
}

346.1.1 = {
	monarch = {
 		name = "Tedbyzz"
		dynasty = "Belch"
		birth_date = 325.1.1
		adm = 5
		dip = 1
		mil = 5
    }
}

384.1.1 = {
	monarch = {
 		name = "Riasxea"
		dynasty = "Hodgos"
		birth_date = 365.1.1
		adm = 3
		dip = 0
		mil = 2
		female = yes
    }
}

482.1.1 = {
	monarch = {
 		name = "Kroihiesb"
		dynasty = "Zurzizal"
		birth_date = 435.1.1
		adm = 1
		dip = 0
		mil = 5
    }
}

563.1.1 = {
	monarch = {
 		name = "Bluilx"
		dynasty = "Hodgos"
		birth_date = 542.1.1
		adm = 3
		dip = 1
		mil = 0
    }
}

598.1.1 = {
	monarch = {
 		name = "Qoifnufz"
		dynasty = "Galgrunduk"
		birth_date = 573.1.1
		adm = 1
		dip = 0
		mil = 3
		female = yes
    }
}

687.1.1 = {
	monarch = {
 		name = "Glizz"
		dynasty = "Donkey"
		birth_date = 656.1.1
		adm = 0
		dip = 6
		mil = 0
    }
}

786.1.1 = {
	monarch = {
 		name = "Ibkeet"
		dynasty = "Uglytooth"
		birth_date = 737.1.1
		adm = 5
		dip = 6
		mil = 4
    }
}

870.1.1 = {
	monarch = {
 		name = "Ysb"
		dynasty = "Greasetooth"
		birth_date = 843.1.1
		adm = 3
		dip = 5
		mil = 0
    }
}

917.1.1 = {
	monarch = {
 		name = "Brirbakz"
		dynasty = "Rak"
		birth_date = 884.1.1
		adm = 1
		dip = 4
		mil = 4
    }
}

996.1.1 = {
	monarch = {
 		name = "Glizz"
		dynasty = "Grimeleg"
		birth_date = 951.1.1
		adm = 0
		dip = 3
		mil = 1
    }
}

1033.1.1 = {
	monarch = {
 		name = "Ibkeet"
		dynasty = "Donkey"
		birth_date = 990.1.1
		adm = 5
		dip = 2
		mil = 4
    }
}

1076.1.1 = {
	monarch = {
 		name = "Ysb"
		dynasty = "Res"
		birth_date = 1047.1.1
		adm = 0
		dip = 4
		mil = 6
    }
}

1164.1.1 = {
	monarch = {
 		name = "Brirbakz"
		dynasty = "Meekteeth"
		birth_date = 1127.1.1
		adm = 5
		dip = 3
		mil = 3
    }
}

1247.1.1 = {
	monarch = {
 		name = "Drybs"
		dynasty = "Vildrundit"
		birth_date = 1225.1.1
		adm = 3
		dip = 2
		mil = 6
    }
}

1301.1.1 = {
	monarch = {
 		name = "Perzeert"
		dynasty = "Gorralk"
		birth_date = 1268.1.1
		adm = 2
		dip = 1
		mil = 3
    }
}

1353.1.1 = {
	monarch = {
 		name = "Ier"
		dynasty = "Wideguts"
		birth_date = 1315.1.1
		adm = 0
		dip = 0
		mil = 0
    }
}

1419.1.1 = {
	monarch = {
 		name = "Aalen"
		dynasty = "Mirgrelok"
		birth_date = 1390.1.1
		adm = 5
		dip = 0
		mil = 3
		female = yes
    }
}

1454.1.1 = {
	monarch = {
 		name = "Drybs"
		dynasty = "Vregob"
		birth_date = 1434.1.1
		adm = 3
		dip = 6
		mil = 0
    }
}

1546.1.1 = {
	monarch = {
 		name = "Perzeert"
		dynasty = "Louse"
		birth_date = 1493.1.1
		adm = 1
		dip = 5
		mil = 3
    }
}

1582.1.1 = {
	monarch = {
 		name = "Tics"
		dynasty = "Urra"
		birth_date = 1529.1.1
		adm = 3
		dip = 6
		mil = 5
    }
}

1663.1.1 = {
	monarch = {
 		name = "Isbealb"
		dynasty = "Uglytooth"
		birth_date = 1642.1.1
		adm = 2
		dip = 6
		mil = 2
    }
}

1700.1.1 = {
	monarch = {
 		name = "Trustrork"
		dynasty = "Anvat"
		birth_date = 1670.1.1
		adm = 0
		dip = 5
		mil = 5
    }
}

1777.1.1 = {
	monarch = {
 		name = "Crord"
		dynasty = "Dugrolk"
		birth_date = 1749.1.1
		adm = 5
		dip = 4
		mil = 2
    }
}

1865.1.1 = {
	monarch = {
 		name = "Vivzikx"
		dynasty = "Weasel"
		birth_date = 1826.1.1
		adm = 3
		dip = 3
		mil = 6
    }
}

1936.1.1 = {
	monarch = {
 		name = "Olk"
		dynasty = "Jas"
		birth_date = 1891.1.1
		adm = 2
		dip = 2
		mil = 2
    }
}

1994.1.1 = {
	monarch = {
 		name = "Faasuik"
		dynasty = "Stajig"
		birth_date = 1943.1.1
		adm = 0
		dip = 2
		mil = 6
    }
}

2044.1.1 = {
	monarch = {
 		name = "Kufz"
		dynasty = "Galgrunduk"
		birth_date = 1996.1.1
		adm = 5
		dip = 1
		mil = 3
		female = yes
    }
}

2122.1.1 = {
	monarch = {
 		name = "Vivzikx"
		dynasty = "Vreb"
		birth_date = 2085.1.1
		adm = 0
		dip = 2
		mil = 4
    }
}

2166.1.1 = {
	monarch = {
 		name = "Criahx"
		dynasty = "Grimarm"
		birth_date = 2129.1.1
		adm = 5
		dip = 1
		mil = 1
		female = yes
    }
}

2226.1.1 = {
	monarch = {
 		name = "Stubef"
		dynasty = "Zigvin"
		birth_date = 2208.1.1
		adm = 3
		dip = 0
		mil = 4
		female = yes
    }
}

2314.1.1 = {
	monarch = {
 		name = "Sholsea"
		dynasty = "Uglyteeth"
		birth_date = 2289.1.1
		adm = 2
		dip = 0
		mil = 1
		female = yes
    }
}

2399.1.1 = {
	monarch = {
 		name = "Khinielk"
		dynasty = "Wartgob"
		birth_date = 2381.1.1
		adm = 0
		dip = 6
		mil = 5
		female = yes
    }
}

2460.1.1 = {
	monarch = {
 		name = "Sleart"
		dynasty = "Cirk"
		birth_date = 2439.1.1
		adm = 5
		dip = 5
		mil = 1
    }
}

