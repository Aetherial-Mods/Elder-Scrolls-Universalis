government = monarchy
government_rank = 1
mercantilism = 1
technology_group = akavir_tg
religion = po_tun_pantheon
primary_culture = po_tun
capital = 2639

54.1.1 = {
	monarch = {
 		name = "Delolcok"
		dynasty = "Cisaclol'wo"
		birth_date = 28.1.1
		adm = 4
		dip = 5
		mil = 0
		female = yes
    }
}

107.1.1 = {
	monarch = {
 		name = "Nessis"
		dynasty = "Rowendurr'ri"
		birth_date = 71.1.1
		adm = 2
		dip = 4
		mil = 4
		female = yes
    }
	queen = {
 		name = "Terterul"
		dynasty = "Rowendurr'ri"
		birth_date = 67.1.1
		adm = 6
		dip = 3
		mil = 3
    }
	heir = {
 		name = "Susdevu"
		monarch_name = "Susdevu I"
		dynasty = "Rowendurr'ri"
		birth_date = 97.1.1
		death_date = 205.1.1
		claim = 100
		adm = 3
		dip = 1
		mil = 2
		female = yes
    }
}

205.1.1 = {
	monarch = {
 		name = "Gur"
		dynasty = "Fon'su"
		birth_date = 160.1.1
		adm = 5
		dip = 3
		mil = 4
    }
	queen = {
 		name = "Curcu"
		dynasty = "Fon'su"
		birth_date = 159.1.1
		adm = 5
		dip = 6
		mil = 4
		female = yes
    }
	heir = {
 		name = "Dicnusthak"
		monarch_name = "Dicnusthak I"
		dynasty = "Fon'su"
		birth_date = 196.1.1
		death_date = 287.1.1
		claim = 100
		adm = 2
		dip = 4
		mil = 3
		female = yes
    }
}

287.1.1 = {
	monarch = {
 		name = "Sehondek"
		dynasty = "Kusdere'su"
		birth_date = 249.1.1
		adm = 4
		dip = 6
		mil = 6
    }
}

367.1.1 = {
	monarch = {
 		name = "Lissok"
		dynasty = "Sen'ri"
		birth_date = 324.1.1
		adm = 3
		dip = 5
		mil = 2
		female = yes
    }
}

426.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Qokug"
		monarch_name = "Qokug I"
		dynasty = "Fon'su"
		birth_date = 412.1.1
		death_date = 430.1.1
		claim = 100
		adm = 3
		dip = 2
		mil = 0
    }
}

430.1.1 = {
	monarch = {
 		name = "Ruvo"
		dynasty = "Piruhus'su"
		birth_date = 392.1.1
		adm = 6
		dip = 3
		mil = 3
    }
	queen = {
 		name = "Sohemool"
		dynasty = "Piruhus'su"
		birth_date = 400.1.1
		adm = 3
		dip = 2
		mil = 2
		female = yes
    }
}

467.1.1 = {
	monarch = {
 		name = "Les"
		dynasty = "Dirskyts'la"
		birth_date = 418.1.1
		adm = 1
		dip = 1
		mil = 5
    }
	queen = {
 		name = "Cisaclol"
		dynasty = "Dirskyts'la"
		birth_date = 423.1.1
		adm = 5
		dip = 0
		mil = 4
		female = yes
    }
	heir = {
 		name = "Lu"
		monarch_name = "Lu I"
		dynasty = "Dirskyts'la"
		birth_date = 455.1.1
		death_date = 514.1.1
		claim = 100
		adm = 2
		dip = 5
		mil = 3
		female = yes
    }
}

514.1.1 = {
	monarch = {
 		name = "Demusdoor"
		dynasty = "Munok'wo"
		birth_date = 477.1.1
		adm = 1
		dip = 2
		mil = 3
    }
	queen = {
 		name = "Sierrosde"
		dynasty = "Munok'wo"
		birth_date = 477.1.1
		adm = 5
		dip = 0
		mil = 2
		female = yes
    }
}

575.1.1 = {
	monarch = {
 		name = "Dur"
		dynasty = "Tosdul'wo"
		birth_date = 544.1.1
		adm = 2
		dip = 2
		mil = 2
		female = yes
    }
}

649.1.1 = {
	monarch = {
 		name = "Domdondrul"
		dynasty = "Chehlon'su"
		birth_date = 606.1.1
		adm = 1
		dip = 1
		mil = 6
    }
	queen = {
 		name = "Chiantre"
		dynasty = "Chehlon'su"
		birth_date = 627.1.1
		adm = 4
		dip = 0
		mil = 5
		female = yes
    }
	heir = {
 		name = "Mur"
		monarch_name = "Mur I"
		dynasty = "Chehlon'su"
		birth_date = 636.1.1
		death_date = 701.1.1
		claim = 100
		adm = 1
		dip = 5
		mil = 4
    }
}

701.1.1 = {
	monarch = {
 		name = "Sierrosde"
		dynasty = "Mekaz'su"
		birth_date = 653.1.1
		adm = 1
		dip = 2
		mil = 4
		female = yes
    }
}

747.1.1 = {
	monarch = {
 		name = "Pek"
		dynasty = "Lun'ri"
		birth_date = 726.1.1
		adm = 6
		dip = 1
		mil = 1
    }
	queen = {
 		name = "Fovi"
		dynasty = "Lun'ri"
		birth_date = 719.1.1
		adm = 3
		dip = 0
		mil = 0
		female = yes
    }
	heir = {
 		name = "Lis"
		monarch_name = "Lis I"
		dynasty = "Lun'ri"
		birth_date = 735.1.1
		death_date = 833.1.1
		claim = 100
		adm = 0
		dip = 5
		mil = 6
		female = yes
    }
}

833.1.1 = {
	monarch = {
 		name = "Settu"
		dynasty = "Zohlad'ri"
		birth_date = 803.1.1
		adm = 2
		dip = 0
		mil = 1
		female = yes
    }
	queen = {
 		name = "Moyd"
		dynasty = "Zohlad'ri"
		birth_date = 805.1.1
		adm = 6
		dip = 5
		mil = 0
    }
	heir = {
 		name = "Pin"
		monarch_name = "Pin I"
		dynasty = "Zohlad'ri"
		birth_date = 820.1.1
		death_date = 890.1.1
		claim = 100
		adm = 3
		dip = 4
		mil = 6
		female = yes
    }
}

890.1.1 = {
	monarch = {
 		name = "Nuyul"
		dynasty = "Chen'la"
		birth_date = 839.1.1
		adm = 6
		dip = 5
		mil = 2
		female = yes
    }
	queen = {
 		name = "Gur"
		dynasty = "Chen'la"
		birth_date = 846.1.1
		adm = 3
		dip = 3
		mil = 1
    }
	heir = {
 		name = "Lis"
		monarch_name = "Lis I"
		dynasty = "Chen'la"
		birth_date = 889.1.1
		death_date = 955.1.1
		claim = 100
		adm = 0
		dip = 2
		mil = 6
		female = yes
    }
}

955.1.1 = {
	monarch = {
 		name = "Suhorrur"
		dynasty = "Rile'wo"
		birth_date = 935.1.1
		adm = 6
		dip = 5
		mil = 0
    }
	queen = {
 		name = "Nessis"
		dynasty = "Rile'wo"
		birth_date = 930.1.1
		adm = 6
		dip = 2
		mil = 1
		female = yes
    }
	heir = {
 		name = "Pon"
		monarch_name = "Pon I"
		dynasty = "Rile'wo"
		birth_date = 946.1.1
		death_date = 1008.1.1
		claim = 100
		adm = 3
		dip = 0
		mil = 0
    }
}

1008.1.1 = {
	monarch = {
 		name = "Lissok"
		dynasty = "Wolcor'la"
		birth_date = 961.1.1
		adm = 3
		dip = 4
		mil = 0
		female = yes
    }
}

1058.1.1 = {
	monarch = {
 		name = "Ther"
		dynasty = "Milin'wo"
		birth_date = 1023.1.1
		adm = 1
		dip = 3
		mil = 4
		female = yes
    }
}

1095.1.1 = {
	monarch = {
 		name = "Ruvo"
		dynasty = "Fon'su"
		birth_date = 1048.1.1
		adm = 6
		dip = 2
		mil = 0
    }
}

1143.1.1 = {
	monarch = {
 		name = "Dirskyts"
		dynasty = "Rondez'su"
		birth_date = 1104.1.1
		adm = 4
		dip = 1
		mil = 4
		female = yes
    }
	queen = {
 		name = "Lihya"
		dynasty = "Rondez'su"
		birth_date = 1122.1.1
		adm = 1
		dip = 0
		mil = 3
    }
	heir = {
 		name = "Bo"
		monarch_name = "Bo I"
		dynasty = "Rondez'su"
		birth_date = 1141.1.1
		death_date = 1240.1.1
		claim = 100
		adm = 3
		dip = 1
		mil = 1
		female = yes
    }
}

1240.1.1 = {
	monarch = {
 		name = "Voldar"
		dynasty = "Pu'ri"
		birth_date = 1213.1.1
		adm = 1
		dip = 0
		mil = 4
    }
}

1307.1.1 = {
	monarch = {
 		name = "Neyak"
		dynasty = "Zud'su"
		birth_date = 1257.1.1
		adm = 0
		dip = 3
		mil = 5
    }
}

1357.1.1 = {
	monarch = {
 		name = "Ti"
		dynasty = "Se'wo"
		birth_date = 1337.1.1
		adm = 5
		dip = 2
		mil = 1
    }
	queen = {
 		name = "Gu"
		dynasty = "Se'wo"
		birth_date = 1329.1.1
		adm = 2
		dip = 1
		mil = 0
		female = yes
    }
	heir = {
 		name = "Thuhelnum"
		monarch_name = "Thuhelnum I"
		dynasty = "Se'wo"
		birth_date = 1352.1.1
		death_date = 1396.1.1
		claim = 100
		adm = 6
		dip = 0
		mil = 6
		female = yes
    }
}

1396.1.1 = {
	monarch = {
 		name = "Bororr"
		dynasty = "Lud'la"
		birth_date = 1378.1.1
		adm = 2
		dip = 1
		mil = 1
		female = yes
    }
	queen = {
 		name = "Par"
		dynasty = "Lud'la"
		birth_date = 1353.1.1
		adm = 6
		dip = 6
		mil = 0
    }
}

1493.1.1 = {
	monarch = {
 		name = "Bundrumu"
		dynasty = "Kev'ri"
		birth_date = 1446.1.1
		adm = 4
		dip = 6
		mil = 4
    }
	queen = {
 		name = "Tilnen"
		dynasty = "Kev'ri"
		birth_date = 1453.1.1
		adm = 1
		dip = 4
		mil = 3
		female = yes
    }
}

1542.1.1 = {
	monarch = {
 		name = "Droryrko"
		dynasty = "		"
		birth_date = 1502.1.1
		adm = 6
		dip = 3
		mil = 0
		female = yes
    }
}

1580.1.1 = {
	monarch = {
 		name = "Quv"
		dynasty = "So'la"
		birth_date = 1561.1.1
		adm = 4
		dip = 3
		mil = 3
    }
}

1655.1.1 = {
	monarch = {
 		name = "Feg"
		dynasty = "Le'wo"
		birth_date = 1609.1.1
		adm = 6
		dip = 4
		mil = 5
    }
	queen = {
 		name = "Pin"
		dynasty = "Le'wo"
		birth_date = 1635.1.1
		adm = 0
		dip = 0
		mil = 6
		female = yes
    }
	heir = {
 		name = "Nuyul"
		monarch_name = "Nuyul I"
		dynasty = "Le'wo"
		birth_date = 1654.1.1
		death_date = 1745.1.1
		claim = 100
		adm = 3
		dip = 6
		mil = 5
		female = yes
    }
}

1745.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Chen"
		monarch_name = "Chen I"
		dynasty = "Pek'su"
		birth_date = 1744.1.1
		death_date = 1762.1.1
		claim = 100
		adm = 2
		dip = 5
		mil = 2
		female = yes
    }
}

1762.1.1 = {
	monarch = {
 		name = "Ramoz"
		dynasty = "Gu'su"
		birth_date = 1722.1.1
		adm = 4
		dip = 6
		mil = 3
    }
	queen = {
 		name = "Thud"
		dynasty = "Gu'su"
		birth_date = 1740.1.1
		adm = 4
		dip = 3
		mil = 4
		female = yes
    }
	heir = {
 		name = "Tiar"
		monarch_name = "Tiar I"
		dynasty = "Gu'su"
		birth_date = 1749.1.1
		death_date = 1835.1.1
		claim = 100
		adm = 1
		dip = 1
		mil = 3
		female = yes
    }
}

1835.1.1 = {
	monarch = {
 		name = "Shracmur"
		dynasty = "Demusdoor'su"
		birth_date = 1809.1.1
		adm = 0
		dip = 5
		mil = 3
		female = yes
    }
	queen = {
 		name = "Pad"
		dynasty = "Demusdoor'su"
		birth_date = 1790.1.1
		adm = 4
		dip = 3
		mil = 2
    }
}

1891.1.1 = {
	monarch = {
 		name = "Pu"
		dynasty = "Tus'su"
		birth_date = 1860.1.1
		adm = 2
		dip = 2
		mil = 6
    }
}

1990.1.1 = {
	monarch = {
 		name = "Dowentem"
		dynasty = "Luttesso'wo"
		birth_date = 1950.1.1
		adm = 0
		dip = 2
		mil = 3
		female = yes
    }
	queen = {
 		name = "Rowendurr"
		dynasty = "Luttesso'wo"
		birth_date = 1943.1.1
		adm = 4
		dip = 0
		mil = 2
    }
}

2042.1.1 = {
	monarch = {
 		name = "Chaz"
		dynasty = "Demusdoor'su"
		birth_date = 1996.1.1
		adm = 3
		dip = 6
		mil = 5
    }
}

2084.1.1 = {
	monarch = {
 		name = "Dochecmea"
		dynasty = "Bo'la"
		birth_date = 2037.1.1
		adm = 1
		dip = 6
		mil = 2
		female = yes
    }
}

2139.1.1 = {
	monarch = {
 		name = "Beme"
		dynasty = "Rilu'wo"
		birth_date = 2105.1.1
		adm = 6
		dip = 5
		mil = 6
		female = yes
    }
}

2224.1.1 = {
	monarch = {
 		name = "Kruyd"
		dynasty = "Therr'la"
		birth_date = 2195.1.1
		adm = 4
		dip = 4
		mil = 2
    }
	queen = {
 		name = "Therr"
		dynasty = "Therr'la"
		birth_date = 2188.1.1
		adm = 1
		dip = 3
		mil = 1
		female = yes
    }
	heir = {
 		name = "Mur"
		monarch_name = "Mur I"
		dynasty = "Therr'la"
		birth_date = 2214.1.1
		death_date = 2304.1.1
		claim = 100
		adm = 5
		dip = 1
		mil = 0
    }
}

2304.1.1 = {
	monarch = {
 		name = "Doknog"
		dynasty = "Thurr'ri"
		birth_date = 2285.1.1
		adm = 4
		dip = 4
		mil = 0
    }
	queen = {
 		name = "Fulnod"
		dynasty = "Thurr'ri"
		birth_date = 2265.1.1
		adm = 5
		dip = 1
		mil = 1
		female = yes
    }
}

2341.1.1 = {
	monarch = {
 		name = "Lun"
		dynasty = "Klirkugots'wo"
		birth_date = 2320.1.1
		adm = 0
		dip = 2
		mil = 3
    }
	queen = {
 		name = "Fiyycnes"
		dynasty = "Klirkugots'wo"
		birth_date = 2300.1.1
		adm = 0
		dip = 6
		mil = 4
		female = yes
    }
}

2426.1.1 = {
	monarch = {
 		name = "Wegichuk"
		dynasty = "		"
		birth_date = 2377.1.1
		adm = 2
		dip = 0
		mil = 6
		female = yes
    }
}

2493.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Par"
		monarch_name = "Par I"
		dynasty = "Chen'la"
		birth_date = 2492.1.1
		death_date = 2510.1.1
		claim = 100
		adm = 6
		dip = 2
		mil = 6
    }
}

