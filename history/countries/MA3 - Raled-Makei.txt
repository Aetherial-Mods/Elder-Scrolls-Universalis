government = republic
government_rank = 1
mercantilism = 1
technology_group = dwemer_tg
religion = reason_and_logic_cult
primary_culture = dwemer
capital = 963

54.1.1 = {
	monarch = {
 		name = "Ghaznak"
		dynasty = "Av'Mzamrumhz"
		birth_date = 33.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

108.1.1 = {
	monarch = {
 		name = "Agahuanch"
		dynasty = "Az'Damtrin"
		birth_date = 85.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

173.1.1 = {
	monarch = {
 		name = "Snenard"
		dynasty = "Af'Mchavin"
		birth_date = 151.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

221.1.1 = {
	monarch = {
 		name = "Alnodrunz"
		dynasty = "Av'Batvar"
		birth_date = 201.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

289.1.1 = {
	monarch = {
 		name = "Nebgar"
		dynasty = "Aq'Nchuzalf"
		birth_date = 241.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

378.1.1 = {
	monarch = {
 		name = "Tadlin"
		dynasty = "Aq'Nromgunch"
		birth_date = 348.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

421.1.1 = {
	monarch = {
 		name = "Byrahken"
		dynasty = "Af'Ghafuan"
		birth_date = 373.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

511.1.1 = {
	monarch = {
 		name = "Chzebchasz"
		dynasty = "Av'Chiuhld"
		birth_date = 477.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

558.1.1 = {
	monarch = {
 		name = "Nebgar"
		dynasty = "Av'Snelarn"
		birth_date = 508.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

614.1.1 = {
	monarch = {
 		name = "Tadlin"
		dynasty = "Aq'Rlovrin"
		birth_date = 593.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

683.1.1 = {
	monarch = {
 		name = "Jrudit"
		dynasty = "Af'Grabnanch"
		birth_date = 634.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

777.1.1 = {
	monarch = {
 		name = "Banrynn"
		dynasty = "Av'Mzaglan"
		birth_date = 755.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

847.1.1 = {
	monarch = {
 		name = "Yhnazzefk"
		dynasty = "Az'Mrobwarn"
		birth_date = 810.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

913.1.1 = {
	monarch = {
 		name = "Nhezril"
		dynasty = "Aq'Chrotwern"
		birth_date = 895.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

968.1.1 = {
	monarch = {
 		name = "Jrudit"
		dynasty = "Az'Chzebchasz"
		birth_date = 945.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1038.1.1 = {
	monarch = {
 		name = "Shtrozril"
		dynasty = "Af'Sthord"
		birth_date = 1008.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1078.1.1 = {
	monarch = {
 		name = "Chragrenz"
		dynasty = "Az'Glibrina"
		birth_date = 1053.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1151.1.1 = {
	monarch = {
 		name = "Nhezril"
		dynasty = "Aq'Mrotchatz"
		birth_date = 1128.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1213.1.1 = {
	monarch = {
 		name = "Bhazchyn"
		dynasty = "Af'Jhourlac"
		birth_date = 1193.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1310.1.1 = {
	monarch = {
 		name = "Nchynac"
		dynasty = "Af'Tnadrak"
		birth_date = 1258.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1350.1.1 = {
	monarch = {
 		name = "Djuhnch"
		dynasty = "Af'Ghafuan"
		birth_date = 1329.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1400.1.1 = {
	monarch = {
 		name = "Grubond"
		dynasty = "Az'Irdarlis"
		birth_date = 1382.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1459.1.1 = {
	monarch = {
 		name = "Grigarn"
		dynasty = "Aq'Mlirloar"
		birth_date = 1423.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1521.1.1 = {
	monarch = {
 		name = "Nchynac"
		dynasty = "Aq'Mlirtes"
		birth_date = 1486.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1587.1.1 = {
	monarch = {
 		name = "Doudrys"
		dynasty = "Az'Ithorves"
		birth_date = 1569.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1638.1.1 = {
	monarch = {
 		name = "Grubond"
		dynasty = "Aq'Mebchasz"
		birth_date = 1605.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1673.1.1 = {
	monarch = {
 		name = "Dzredras"
		dynasty = "Av'Czadlin"
		birth_date = 1621.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1764.1.1 = {
	monarch = {
 		name = "Jnathunch"
		dynasty = "Av'Gzozchyn"
		birth_date = 1725.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1821.1.1 = {
	monarch = {
 		name = "Asradlin"
		dynasty = "Av'Bzrazgar"
		birth_date = 1790.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1903.1.1 = {
	monarch = {
 		name = "Irdarlis"
		dynasty = "Av'Ihlefuan"
		birth_date = 1869.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1998.1.1 = {
	monarch = {
 		name = "Dzredras"
		dynasty = "Az'Doudhis"
		birth_date = 1967.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2068.1.1 = {
	monarch = {
 		name = "Chzefrach"
		dynasty = "Aq'Mravlara"
		birth_date = 2049.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

2103.1.1 = {
	monarch = {
 		name = "Inratarn"
		dynasty = "Aq'Grigarn"
		birth_date = 2056.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2198.1.1 = {
	monarch = {
 		name = "Chzemgunch"
		dynasty = "Aq'Somzlin"
		birth_date = 2172.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2276.1.1 = {
	monarch = {
 		name = "Czadlin"
		dynasty = "Av'Tugradac"
		birth_date = 2240.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2343.1.1 = {
	monarch = {
 		name = "Tronruz"
		dynasty = "Av'Ralen"
		birth_date = 2296.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

2389.1.1 = {
	monarch = {
 		name = "Mchavin"
		dynasty = "Av'Brazzedit"
		birth_date = 2356.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2444.1.1 = {
	monarch = {
 		name = "Rhothurzch"
		dynasty = "Aq'Aravlen"
		birth_date = 2416.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

2480.1.1 = {
	monarch = {
 		name = "Snebchasz"
		dynasty = "Av'Ghaznak"
		birth_date = 2450.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

