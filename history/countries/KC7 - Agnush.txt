government = monarchy
government_rank = 1
mercantilism = 1
technology_group = kamal_tg
religion = dremora_pantheon
primary_culture = al_dremoran
capital = 2600

54.1.1 = {
	monarch = {
 		name = "Lilysh"
		dynasty = "Vyl-Cylniyite"
		birth_date = 15.1.1
		adm = 4
		dip = 2
		mil = 6
		female = yes
    }
	queen = {
 		name = "Dynirad"
		dynasty = "Vyl-Cylniyite"
		birth_date = 12.1.1
		adm = 1
		dip = 0
		mil = 5
    }
	heir = {
 		name = "Ohirana"
		monarch_name = "Ohirana I"
		dynasty = "Vyl-Cylniyite"
		birth_date = 52.1.1
		death_date = 123.1.1
		claim = 100
		adm = 5
		dip = 6
		mil = 4
		female = yes
    }
}

123.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Rizarniran"
		monarch_name = "Rizarniran I"
		dynasty = "Vyl-Knakroura"
		birth_date = 113.1.1
		death_date = 131.1.1
		claim = 100
		adm = 3
		dip = 5
		mil = 1
    }
}

131.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Rharied"
		monarch_name = "Rharied I"
		dynasty = "Vyl-Gnuarfaite"
		birth_date = 116.1.1
		death_date = 134.1.1
		claim = 100
		adm = 1
		dip = 4
		mil = 4
    }
}

134.1.1 = {
	monarch = {
 		name = "Xintrax"
		dynasty = "Vyl-Wouus"
		birth_date = 107.1.1
		adm = 1
		dip = 1
		mil = 5
    }
}

185.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Ohirana"
		monarch_name = "Ohirana I"
		dynasty = "Vyl-Steeira"
		birth_date = 175.1.1
		death_date = 193.1.1
		claim = 100
		adm = 1
		dip = 5
		mil = 2
		female = yes
    }
}

193.1.1 = {
	monarch = {
 		name = "Orinith"
		dynasty = "Vyl-Kamori"
		birth_date = 167.1.1
		adm = 4
		dip = 6
		mil = 5
		female = yes
    }
	queen = {
 		name = "Fyzied"
		dynasty = "Vyl-Kamori"
		birth_date = 161.1.1
		adm = 1
		dip = 5
		mil = 4
    }
	heir = {
 		name = "Rharied"
		monarch_name = "Rharied I"
		dynasty = "Vyl-Kamori"
		birth_date = 189.1.1
		death_date = 229.1.1
		claim = 100
		adm = 5
		dip = 3
		mil = 3
    }
}

229.1.1 = {
	monarch = {
 		name = "Vhesnila"
		dynasty = "Vyl-Teubal"
		birth_date = 185.1.1
		adm = 1
		dip = 5
		mil = 5
		female = yes
    }
	queen = {
 		name = "Iarsan"
		dynasty = "Vyl-Teubal"
		birth_date = 194.1.1
		adm = 5
		dip = 3
		mil = 4
    }
	heir = {
 		name = "Caranel"
		monarch_name = "Caranel I"
		dynasty = "Vyl-Teubal"
		birth_date = 216.1.1
		death_date = 314.1.1
		claim = 100
		adm = 1
		dip = 2
		mil = 3
    }
}

314.1.1 = {
	monarch = {
 		name = "Carileth"
		dynasty = "Vyl-Glumsymaeus"
		birth_date = 272.1.1
		adm = 1
		dip = 5
		mil = 3
		female = yes
    }
	queen = {
 		name = "Lanarrenar"
		dynasty = "Vyl-Glumsymaeus"
		birth_date = 295.1.1
		adm = 5
		dip = 4
		mil = 2
    }
	heir = {
 		name = "Prislyn"
		monarch_name = "Prislyn I"
		dynasty = "Vyl-Glumsymaeus"
		birth_date = 309.1.1
		death_date = 360.1.1
		claim = 100
		adm = 5
		dip = 0
		mil = 3
		female = yes
    }
}

360.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Joraner"
		monarch_name = "Joraner I"
		dynasty = "Vyl-Cupudea"
		birth_date = 355.1.1
		death_date = 373.1.1
		claim = 100
		adm = 6
		dip = 4
		mil = 1
    }
}

373.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Crynlynx"
		monarch_name = "Crynlynx I"
		dynasty = "Vyl-Knakroura"
		birth_date = 362.1.1
		death_date = 380.1.1
		claim = 100
		adm = 6
		dip = 1
		mil = 6
    }
}

380.1.1 = {
	monarch = {
 		name = "Zarinoris"
		dynasty = "Vyl-Duiala"
		birth_date = 329.1.1
		adm = 2
		dip = 2
		mil = 2
		female = yes
    }
	queen = {
 		name = "Colerrith"
		dynasty = "Vyl-Duiala"
		birth_date = 355.1.1
		adm = 2
		dip = 6
		mil = 2
    }
	heir = {
 		name = "Qyrvynia"
		monarch_name = "Qyrvynia I"
		dynasty = "Vyl-Duiala"
		birth_date = 379.1.1
		death_date = 464.1.1
		claim = 100
		adm = 5
		dip = 4
		mil = 1
		female = yes
    }
}

464.1.1 = {
	monarch = {
 		name = "Cattia"
		dynasty = "Vyl-Elmemaeus"
		birth_date = 430.1.1
		adm = 4
		dip = 4
		mil = 4
		female = yes
    }
}

545.1.1 = {
	monarch = {
 		name = "Colorad"
		dynasty = "Vyl-Wouus"
		birth_date = 492.1.1
		adm = 2
		dip = 3
		mil = 1
    }
	queen = {
 		name = "Ohirlith"
		dynasty = "Vyl-Wouus"
		birth_date = 515.1.1
		adm = 6
		dip = 1
		mil = 0
		female = yes
    }
}

604.1.1 = {
	monarch = {
 		name = "Lanarorad"
		dynasty = "Vyl-Qleahmymina"
		birth_date = 575.1.1
		adm = 4
		dip = 1
		mil = 4
    }
}

679.1.1 = {
	monarch = {
 		name = "Joranlead"
		dynasty = "Vyl-Plystius"
		birth_date = 652.1.1
		adm = 2
		dip = 0
		mil = 0
    }
}

778.1.1 = {
	monarch = {
 		name = "Zahraxis"
		dynasty = "Vyl-Stavemina"
		birth_date = 751.1.1
		adm = 0
		dip = 6
		mil = 4
    }
	queen = {
 		name = "Shylea"
		dynasty = "Vyl-Stavemina"
		birth_date = 760.1.1
		adm = 4
		dip = 5
		mil = 3
		female = yes
    }
}

844.1.1 = {
	monarch = {
 		name = "Garanlan"
		dynasty = "Vyl-Duiala"
		birth_date = 807.1.1
		adm = 3
		dip = 4
		mil = 0
    }
}

918.1.1 = {
	monarch = {
 		name = "Iphisysha"
		dynasty = "Vyl-Gnuallyrath"
		birth_date = 896.1.1
		adm = 1
		dip = 3
		mil = 3
		female = yes
    }
}

973.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Braxlax"
		monarch_name = "Braxlax I"
		dynasty = "Vyl-Wouus"
		birth_date = 961.1.1
		death_date = 979.1.1
		claim = 100
		adm = 2
		dip = 0
		mil = 1
    }
}

979.1.1 = {
	monarch = {
 		name = "Yhrvile"
		dynasty = "Vyl-Haecyunes"
		birth_date = 954.1.1
		adm = 1
		dip = 3
		mil = 1
    }
}

1075.1.1 = {
	monarch = {
 		name = "Grullax"
		dynasty = "Vyl-Eiv'aicus"
		birth_date = 1023.1.1
		adm = 6
		dip = 3
		mil = 5
    }
	queen = {
 		name = "Yrelyra"
		dynasty = "Vyl-Eiv'aicus"
		birth_date = 1049.1.1
		adm = 3
		dip = 1
		mil = 4
		female = yes
    }
	heir = {
 		name = "Salex"
		monarch_name = "Salex I"
		dynasty = "Vyl-Eiv'aicus"
		birth_date = 1067.1.1
		death_date = 1156.1.1
		claim = 100
		adm = 0
		dip = 0
		mil = 3
    }
}

1156.1.1 = {
	monarch = {
 		name = "Xenyera"
		dynasty = "Vyl-Gnuarfaite"
		birth_date = 1131.1.1
		adm = 3
		dip = 1
		mil = 5
		female = yes
    }
}

1245.1.1 = {
	monarch = {
 		name = "Lanarixan"
		dynasty = "Vyl-Opeoth"
		birth_date = 1207.1.1
		adm = 3
		dip = 5
		mil = 3
    }
	queen = {
 		name = "Wylinelin"
		dynasty = "Vyl-Opeoth"
		birth_date = 1202.1.1
		adm = 0
		dip = 4
		mil = 2
		female = yes
    }
	heir = {
 		name = "Zahrnyx"
		monarch_name = "Zahrnyx I"
		dynasty = "Vyl-Opeoth"
		birth_date = 1242.1.1
		death_date = 1291.1.1
		claim = 100
		adm = 4
		dip = 2
		mil = 1
    }
}

1291.1.1 = {
	monarch = {
 		name = "Vylysh"
		dynasty = "Vyl-Qleahmymina"
		birth_date = 1245.1.1
		adm = 0
		dip = 4
		mil = 4
		female = yes
    }
	queen = {
 		name = "Qrinnar"
		dynasty = "Vyl-Qleahmymina"
		birth_date = 1265.1.1
		adm = 4
		dip = 2
		mil = 3
    }
	heir = {
 		name = "Colerrith"
		monarch_name = "Colerrith I"
		dynasty = "Vyl-Qleahmymina"
		birth_date = 1285.1.1
		death_date = 1365.1.1
		claim = 100
		adm = 1
		dip = 1
		mil = 2
    }
}

1365.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Brenilan"
		monarch_name = "Brenilan I"
		dynasty = "Vyl-Haecyunes"
		birth_date = 1363.1.1
		death_date = 1381.1.1
		claim = 100
		adm = 6
		dip = 0
		mil = 5
    }
}

1381.1.1 = {
	monarch = {
 		name = "Ohmvyce"
		dynasty = "Vyl-Glumsymaeus"
		birth_date = 1332.1.1
		adm = 2
		dip = 1
		mil = 1
    }
	queen = {
 		name = "Phisvienne"
		dynasty = "Vyl-Glumsymaeus"
		birth_date = 1330.1.1
		adm = 1
		dip = 5
		mil = 1
		female = yes
    }
	heir = {
 		name = "Praxaxis"
		monarch_name = "Praxaxis I"
		dynasty = "Vyl-Glumsymaeus"
		birth_date = 1372.1.1
		death_date = 1470.1.1
		claim = 100
		adm = 5
		dip = 3
		mil = 0
    }
}

1470.1.1 = {
	monarch = {
 		name = "Urtiran"
		dynasty = "Vyl-Krisebal"
		birth_date = 1423.1.1
		adm = 1
		dip = 4
		mil = 2
    }
	queen = {
 		name = "Cahrielle"
		dynasty = "Vyl-Krisebal"
		birth_date = 1428.1.1
		adm = 5
		dip = 3
		mil = 1
		female = yes
    }
	heir = {
 		name = "Vohroth"
		monarch_name = "Vohroth I"
		dynasty = "Vyl-Krisebal"
		birth_date = 1462.1.1
		death_date = 1536.1.1
		claim = 100
		adm = 1
		dip = 1
		mil = 0
    }
}

1536.1.1 = {
	monarch = {
 		name = "Ohmorad"
		dynasty = "Vyl-Qleahmymina"
		birth_date = 1485.1.1
		adm = 1
		dip = 5
		mil = 0
    }
}

1575.1.1 = {
	monarch = {
 		name = "Iarylan"
		dynasty = "Vyl-Kayara"
		birth_date = 1557.1.1
		adm = 6
		dip = 4
		mil = 4
    }
	queen = {
 		name = "Krynyra"
		dynasty = "Vyl-Kayara"
		birth_date = 1530.1.1
		adm = 3
		dip = 3
		mil = 3
		female = yes
    }
	heir = {
 		name = "Qahrnex"
		monarch_name = "Qahrnex I"
		dynasty = "Vyl-Kayara"
		birth_date = 1568.1.1
		death_date = 1611.1.1
		claim = 100
		adm = 3
		dip = 6
		mil = 4
    }
}

1611.1.1 = {
	monarch = {
 		name = "Garanlan"
		dynasty = "Vyl-Kamori"
		birth_date = 1579.1.1
		adm = 3
		dip = 2
		mil = 4
    }
	queen = {
 		name = "Orelin"
		dynasty = "Vyl-Kamori"
		birth_date = 1584.1.1
		adm = 6
		dip = 1
		mil = 3
		female = yes
    }
}

1698.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Ezrinaris"
		monarch_name = "Ezrinaris I"
		dynasty = "Vyl-Pliglyura"
		birth_date = 1689.1.1
		death_date = 1707.1.1
		claim = 100
		adm = 0
		dip = 5
		mil = 1
    }
}

1707.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Yhrvile"
		monarch_name = "Yhrvile I"
		dynasty = "Vyl-Ciefeala"
		birth_date = 1694.1.1
		death_date = 1712.1.1
		claim = 100
		adm = 5
		dip = 4
		mil = 5
    }
}

1712.1.1 = {
	monarch = {
 		name = "Zrixira"
		dynasty = "Vyl-Calamity"
		birth_date = 1677.1.1
		adm = 1
		dip = 6
		mil = 0
		female = yes
    }
}

1804.1.1 = {
	monarch = {
 		name = "Fharyxir"
		dynasty = "Vyl-Ruyura"
		birth_date = 1765.1.1
		adm = 6
		dip = 5
		mil = 4
    }
}

1890.1.1 = {
	monarch = {
 		name = "Qhesrelle"
		dynasty = "Vyl-Austere"
		birth_date = 1857.1.1
		adm = 5
		dip = 4
		mil = 0
		female = yes
    }
}

1930.1.1 = {
	monarch = {
 		name = "Ylrahn"
		dynasty = "Vyl-Eiv'aicus"
		birth_date = 1896.1.1
		adm = 0
		dip = 5
		mil = 2
    }
	queen = {
 		name = "Shynixi"
		dynasty = "Vyl-Eiv'aicus"
		birth_date = 1880.1.1
		adm = 0
		dip = 2
		mil = 3
		female = yes
    }
	heir = {
 		name = "Eranoriad"
		monarch_name = "Eranoriad I"
		dynasty = "Vyl-Eiv'aicus"
		birth_date = 1924.1.1
		death_date = 2000.1.1
		claim = 100
		adm = 4
		dip = 0
		mil = 2
    }
}

2000.1.1 = {
	monarch = {
 		name = "Wraxyx"
		dynasty = "Vyl-Truor'us"
		birth_date = 1979.1.1
		adm = 3
		dip = 4
		mil = 2
    }
}

2048.1.1 = {
	monarch = {
 		name = "Ortyse"
		dynasty = "Vyl-Ruadia"
		birth_date = 1997.1.1
		adm = 1
		dip = 3
		mil = 6
		female = yes
    }
	queen = {
 		name = "Braxlyx"
		dynasty = "Vyl-Ruadia"
		birth_date = 2010.1.1
		adm = 5
		dip = 1
		mil = 5
    }
	heir = {
 		name = "Lanarrenar"
		monarch_name = "Lanarrenar I"
		dynasty = "Vyl-Ruadia"
		birth_date = 2037.1.1
		death_date = 2084.1.1
		claim = 100
		adm = 1
		dip = 3
		mil = 0
    }
}

2084.1.1 = {
	monarch = {
 		name = "Vohroth"
		dynasty = "Vyl-Truacius"
		birth_date = 2038.1.1
		adm = 0
		dip = 6
		mil = 1
    }
	queen = {
 		name = "Xislyss"
		dynasty = "Vyl-Truacius"
		birth_date = 2031.1.1
		adm = 4
		dip = 5
		mil = 0
		female = yes
    }
	heir = {
 		name = "Larsahr"
		monarch_name = "Larsahr I"
		dynasty = "Vyl-Truacius"
		birth_date = 2075.1.1
		death_date = 2128.1.1
		claim = 100
		adm = 1
		dip = 3
		mil = 5
    }
}

2128.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Phisvienne"
		monarch_name = "Phisvienne I"
		dynasty = "Vyl-Kamori"
		birth_date = 2120.1.1
		death_date = 2138.1.1
		claim = 100
		adm = 6
		dip = 2
		mil = 2
		female = yes
    }
}

2138.1.1 = {
	monarch = {
 		name = "Ohiraith"
		dynasty = "Vyl-Eiv'aicus"
		birth_date = 2088.1.1
		adm = 2
		dip = 4
		mil = 4
		female = yes
    }
	queen = {
 		name = "Saloth"
		dynasty = "Vyl-Eiv'aicus"
		birth_date = 2099.1.1
		adm = 6
		dip = 2
		mil = 3
    }
	heir = {
 		name = "Vylon"
		monarch_name = "Vylon I"
		dynasty = "Vyl-Eiv'aicus"
		birth_date = 2128.1.1
		death_date = 2201.1.1
		claim = 100
		adm = 3
		dip = 1
		mil = 2
    }
}

2201.1.1 = {
	monarch = {
 		name = "Rizarniran"
		dynasty = "Vyl-Sturaoth"
		birth_date = 2153.1.1
		adm = 6
		dip = 2
		mil = 5
    }
}

2276.1.1 = {
	monarch = {
 		name = "Rharied"
		dynasty = "Vyl-Stavemina"
		birth_date = 2240.1.1
		adm = 0
		dip = 4
		mil = 6
    }
}

2348.1.1 = {
	monarch = {
 		name = "Ohiraith"
		dynasty = "Vyl-Knakroura"
		birth_date = 2296.1.1
		adm = 6
		dip = 3
		mil = 3
		female = yes
    }
}

2397.1.1 = {
	monarch = {
 		name = "Xylney"
		dynasty = "Vyl-Plystius"
		birth_date = 2369.1.1
		adm = 4
		dip = 2
		mil = 0
		female = yes
    }
	queen = {
 		name = "Gnarvile"
		dynasty = "Vyl-Plystius"
		birth_date = 2369.1.1
		adm = 1
		dip = 0
		mil = 6
    }
	heir = {
 		name = "Laran"
		monarch_name = "Laran I"
		dynasty = "Vyl-Plystius"
		birth_date = 2385.1.1
		death_date = 2491.1.1
		claim = 100
		adm = 5
		dip = 6
		mil = 5
    }
}

2491.1.1 = {
	monarch = {
 		name = "Prislyn"
		dynasty = "Vyl-Qlucuina"
		birth_date = 2461.1.1
		adm = 0
		dip = 0
		mil = 0
		female = yes
    }
	queen = {
 		name = "Zahrnyx"
		dynasty = "Vyl-Qlucuina"
		birth_date = 2472.1.1
		adm = 4
		dip = 6
		mil = 6
    }
	heir = {
 		name = "Iarsan"
		monarch_name = "Iarsan I"
		dynasty = "Vyl-Qlucuina"
		birth_date = 2491.1.1
		death_date = 2589.1.1
		claim = 100
		adm = 1
		dip = 4
		mil = 5
    }
}

