government = tribal
government_rank = 3
mercantilism = 1
technology_group = elsweyr_tg
religion = khajiiti_pantheon
primary_culture = khajiiti
capital = 922

54.1.1 = {
	monarch = {
 		name = "Dazur"
		dynasty = "Bahrajatani"
		birth_date = 22.1.1
		adm = 2
		dip = 4
		mil = 5
    }
}

91.1.1 = {
	monarch = {
 		name = "Uzarrur"
		dynasty = "Jodara"
		birth_date = 41.1.1
		adm = 0
		dip = 3
		mil = 1
    }
}

158.1.1 = {
	monarch = {
 		name = "Ojik"
		dynasty = "Kharzaymar"
		birth_date = 112.1.1
		adm = 5
		dip = 2
		mil = 5
    }
}

195.1.1 = {
	monarch = {
 		name = "Kaszo"
		dynasty = "Toviktanni"
		birth_date = 150.1.1
		adm = 4
		dip = 1
		mil = 2
    }
}

254.1.1 = {
	monarch = {
 		name = "Dehdri"
		dynasty = "Ma'siri"
		birth_date = 204.1.1
		adm = 6
		dip = 3
		mil = 3
		female = yes
    }
}

309.1.1 = {
	monarch = {
 		name = "Unnur"
		dynasty = "Anaihn"
		birth_date = 256.1.1
		adm = 4
		dip = 2
		mil = 0
    }
}

363.1.1 = {
	monarch = {
 		name = "Duzuni"
		dynasty = "Shohad"
		birth_date = 345.1.1
		adm = 2
		dip = 1
		mil = 3
		female = yes
    }
}

426.1.1 = {
	monarch = {
 		name = "Yanabir-ja"
		dynasty = "Shohad"
		birth_date = 391.1.1
		adm = 0
		dip = 0
		mil = 0
    }
}

483.1.1 = {
	monarch = {
 		name = "Qa'jahd"
		dynasty = "Zoadran"
		birth_date = 446.1.1
		adm = 6
		dip = 0
		mil = 4
    }
}

569.1.1 = {
	monarch = {
 		name = "Maila"
		dynasty = "oshsaad"
		birth_date = 550.1.1
		adm = 4
		dip = 6
		mil = 0
		female = yes
    }
}

665.1.1 = {
	monarch = {
 		name = "Dulini"
		dynasty = "Kijibiri"
		birth_date = 616.1.1
		adm = 2
		dip = 5
		mil = 4
    }
}

707.1.1 = {
	monarch = {
 		name = "Whickmuz"
		dynasty = "Rokaron"
		birth_date = 659.1.1
		adm = 0
		dip = 4
		mil = 1
    }
}

759.1.1 = {
	monarch = {
 		name = "Pinumur"
		dynasty = "Shohad"
		birth_date = 719.1.1
		adm = 2
		dip = 5
		mil = 2
    }
}

812.1.1 = {
	monarch = {
 		name = "Magdi"
		dynasty = "Rokaron"
		birth_date = 766.1.1
		adm = 0
		dip = 5
		mil = 6
		female = yes
    }
}

880.1.1 = {
	monarch = {
 		name = "Ragariz"
		dynasty = "Ahjgarvi"
		birth_date = 832.1.1
		adm = 6
		dip = 4
		mil = 2
    }
}

917.1.1 = {
	monarch = {
 		name = "Marasadra"
		dynasty = "Anaihn"
		birth_date = 898.1.1
		adm = 4
		dip = 3
		mil = 6
		female = yes
    }
}

954.1.1 = {
	monarch = {
 		name = "Eraral-dro"
		dynasty = "Anaihn"
		birth_date = 933.1.1
		adm = 2
		dip = 2
		mil = 3
    }
}

1015.1.1 = {
	monarch = {
 		name = "Zaizami"
		dynasty = "Ahjhir"
		birth_date = 985.1.1
		adm = 0
		dip = 1
		mil = 6
		female = yes
    }
}

1110.1.1 = {
	monarch = {
 		name = "Radah"
		dynasty = "Bhijhad"
		birth_date = 1062.1.1
		adm = 6
		dip = 1
		mil = 3
    }
}

1194.1.1 = {
	monarch = {
 		name = "Kirrdul"
		dynasty = "Urjaabhi"
		birth_date = 1162.1.1
		adm = 4
		dip = 0
		mil = 0
    }
}

1289.1.1 = {
	monarch = {
 		name = "Elzhar"
		dynasty = "Shohad"
		birth_date = 1238.1.1
		adm = 6
		dip = 1
		mil = 1
    }
}

1370.1.1 = {
	monarch = {
 		name = "Zairan"
		dynasty = "Helarr-Jo"
		birth_date = 1346.1.1
		adm = 4
		dip = 0
		mil = 5
		female = yes
    }
}

1448.1.1 = {
	monarch = {
 		name = "Farit"
		dynasty = "R'athra"
		birth_date = 1418.1.1
		adm = 0
		dip = 2
		mil = 0
    }
}

1542.1.1 = {
	monarch = {
 		name = "Munuri"
		dynasty = "Dar'tesh"
		birth_date = 1524.1.1
		adm = 1
		dip = 6
		mil = 5
    }
}

1627.1.1 = {
	monarch = {
 		name = "Ralkluz"
		dynasty = "Havnusopor"
		birth_date = 1594.1.1
		adm = 6
		dip = 5
		mil = 2
    }
}

1672.1.1 = {
	monarch = {
 		name = "Kurnamu-do"
		dynasty = "Rawinai"
		birth_date = 1635.1.1
		adm = 4
		dip = 4
		mil = 5
    }
}

1733.1.1 = {
	monarch = {
 		name = "Fahurr"
		dynasty = "Javamnihn"
		birth_date = 1709.1.1
		adm = 2
		dip = 3
		mil = 2
    }
}

1795.1.1 = {
	monarch = {
 		name = "Zalurrah"
		dynasty = "Havnuhior"
		birth_date = 1756.1.1
		adm = 1
		dip = 3
		mil = 6
		female = yes
    }
}

1874.1.1 = {
	monarch = {
 		name = "Rakhzargo"
		dynasty = "Daro'urabi"
		birth_date = 1846.1.1
		adm = 2
		dip = 4
		mil = 0
    }
}

1943.1.1 = {
	monarch = {
 		name = "Mathi"
		dynasty = "Havnuhior"
		birth_date = 1918.1.1
		adm = 1
		dip = 3
		mil = 4
		female = yes
    }
}

1994.1.1 = {
	monarch = {
 		name = "Rezirjhan"
		dynasty = "Ja'sien"
		birth_date = 1941.1.1
		adm = 6
		dip = 2
		mil = 1
    }
}

2069.1.1 = {
	monarch = {
 		name = "Linihi"
		dynasty = "Anaihn"
		birth_date = 2031.1.1
		adm = 4
		dip = 1
		mil = 4
    }
}

2108.1.1 = {
	monarch = {
 		name = "Flozah-no"
		dynasty = "Urjobil"
		birth_date = 2063.1.1
		adm = 2
		dip = 1
		mil = 1
    }
}

2152.1.1 = {
	monarch = {
 		name = "Zarziri"
		dynasty = "Ranrjo"
		birth_date = 2118.1.1
		adm = 1
		dip = 0
		mil = 5
    }
}

2219.1.1 = {
	monarch = {
 		name = "Renaku"
		dynasty = "Roudavi"
		birth_date = 2187.1.1
		adm = 6
		dip = 6
		mil = 1
    }
}

2286.1.1 = {
	monarch = {
 		name = "Mina-diir"
		dynasty = "Mohamathad"
		birth_date = 2251.1.1
		adm = 4
		dip = 5
		mil = 5
		female = yes
    }
}

2328.1.1 = {
	monarch = {
 		name = "Feziri"
		dynasty = "Wadaiq"
		birth_date = 2290.1.1
		adm = 6
		dip = 0
		mil = 6
    }
}

2420.1.1 = {
	monarch = {
 		name = "Zargal"
		dynasty = "Ri'hasta"
		birth_date = 2386.1.1
		adm = 4
		dip = 6
		mil = 3
    }
}

