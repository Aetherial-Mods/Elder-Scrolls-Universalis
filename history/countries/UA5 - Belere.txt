government = theocracy
government_rank = 1
mercantilism = 1
technology_group = akavir_tg
religion = dragon_cult
primary_culture = akaviri
capital = 761

54.1.1 = {
	monarch = {
 		name = "Selassa"
		dynasty = "Vi-Mystophi"
		birth_date = 21.1.1
		adm = 1
		dip = 0
		mil = 3
		female = yes
    }
}

99.1.1 = {
	monarch = {
 		name = "Theamephia"
		dynasty = "Vi-Thanistae"
		birth_date = 56.1.1
		adm = 6
		dip = 6
		mil = 6
		female = yes
    }
}

197.1.1 = {
	monarch = {
 		name = "Theanertes"
		dynasty = "Di-Anasztochus"
		birth_date = 172.1.1
		adm = 0
		dip = 3
		mil = 4
		female = yes
    }
}

293.1.1 = {
	monarch = {
 		name = "Aphrixi"
		dynasty = "Vi-Othrithoe"
		birth_date = 247.1.1
		adm = 5
		dip = 2
		mil = 1
    }
}

385.1.1 = {
	monarch = {
 		name = "Adrasite"
		dynasty = "Vi-Batheshi"
		birth_date = 332.1.1
		adm = 4
		dip = 1
		mil = 5
		female = yes
    }
}

472.1.1 = {
	monarch = {
 		name = "Krathisthus"
		dynasty = "Vi-Anthophila"
		birth_date = 454.1.1
		adm = 2
		dip = 1
		mil = 1
    }
}

527.1.1 = {
	monarch = {
 		name = "Anathios"
		dynasty = "Di-Hespips"
		birth_date = 493.1.1
		adm = 0
		dip = 0
		mil = 5
    }
}

585.1.1 = {
	monarch = {
 		name = "Nyshisia"
		dynasty = "Du-Demithios"
		birth_date = 538.1.1
		adm = 5
		dip = 6
		mil = 2
		female = yes
    }
}

659.1.1 = {
	monarch = {
 		name = "Adrasite"
		dynasty = "Di-Ephiste"
		birth_date = 628.1.1
		adm = 4
		dip = 5
		mil = 5
		female = yes
    }
}

695.1.1 = {
	monarch = {
 		name = "Krathisthus"
		dynasty = "Vu-Daphnaia"
		birth_date = 662.1.1
		adm = 4
		dip = 2
		mil = 3
    }
}

741.1.1 = {
	monarch = {
 		name = "Anathios"
		dynasty = "Vi-Batheshi"
		birth_date = 719.1.1
		adm = 3
		dip = 2
		mil = 0
    }
}

821.1.1 = {
	monarch = {
 		name = "Salmepios"
		dynasty = "Vu-Theanaeia"
		birth_date = 801.1.1
		adm = 1
		dip = 1
		mil = 3
    }
}

879.1.1 = {
	monarch = {
 		name = "Misosise"
		dynasty = "Vu-Daphnaia"
		birth_date = 832.1.1
		adm = 0
		dip = 4
		mil = 4
		female = yes
    }
}

919.1.1 = {
	monarch = {
 		name = "Acsiphei"
		dynasty = "Du-Orthrosyne"
		birth_date = 887.1.1
		adm = 6
		dip = 3
		mil = 0
		female = yes
    }
}

1006.1.1 = {
	monarch = {
 		name = "Thanetus"
		dynasty = "Vu-Anthia"
		birth_date = 968.1.1
		adm = 4
		dip = 3
		mil = 4
    }
}

1074.1.1 = {
	monarch = {
 		name = "Sabixa"
		dynasty = "Vi-Othrithoe"
		birth_date = 1026.1.1
		adm = 2
		dip = 2
		mil = 1
		female = yes
    }
}

1155.1.1 = {
	monarch = {
 		name = "Galixeilla"
		dynasty = "Vi-Misais"
		birth_date = 1110.1.1
		adm = 0
		dip = 1
		mil = 4
		female = yes
    }
}

1209.1.1 = {
	monarch = {
 		name = "Acsiphei"
		dynasty = "Vu-Selassa"
		birth_date = 1180.1.1
		adm = 5
		dip = 0
		mil = 1
		female = yes
    }
}

1258.1.1 = {
	monarch = {
 		name = "Theamiophai"
		dynasty = "Vu-Orethaeia"
		birth_date = 1227.1.1
		adm = 0
		dip = 1
		mil = 2
		female = yes
    }
}

1308.1.1 = {
	monarch = {
 		name = "Hespips"
		dynasty = "Du-Proteserios"
		birth_date = 1278.1.1
		adm = 6
		dip = 1
		mil = 6
    }
}

1385.1.1 = {
	monarch = {
 		name = "Alethrusa"
		dynasty = "Di-Hespips"
		birth_date = 1361.1.1
		adm = 4
		dip = 0
		mil = 3
		female = yes
    }
}

1435.1.1 = {
	monarch = {
 		name = "Hespice"
		dynasty = "Vi-Rhaeniophai"
		birth_date = 1407.1.1
		adm = 2
		dip = 6
		mil = 6
    }
}

1524.1.1 = {
	monarch = {
 		name = "Spyrersis"
		dynasty = "Vi-Kisseosis"
		birth_date = 1484.1.1
		adm = 0
		dip = 5
		mil = 3
    }
}

1605.1.1 = {
	monarch = {
 		name = "Prophios"
		dynasty = "Vu-Sinopheu"
		birth_date = 1554.1.1
		adm = 6
		dip = 5
		mil = 0
    }
}

1693.1.1 = {
	monarch = {
 		name = "Agathastos"
		dynasty = "Du-Xerxelix"
		birth_date = 1658.1.1
		adm = 4
		dip = 4
		mil = 3
    }
}

1751.1.1 = {
	monarch = {
 		name = "Orphishia"
		dynasty = "Vi-Theanionis"
		birth_date = 1729.1.1
		adm = 2
		dip = 3
		mil = 0
		female = yes
    }
}

1812.1.1 = {
	monarch = {
 		name = "Eustolos"
		dynasty = "Vi-Ophiisha"
		birth_date = 1764.1.1
		adm = 4
		dip = 4
		mil = 1
    }
}

1879.1.1 = {
	monarch = {
 		name = "Ashataus"
		dynasty = "Di-Alpheas"
		birth_date = 1855.1.1
		adm = 2
		dip = 3
		mil = 5
    }
}

1920.1.1 = {
	monarch = {
 		name = "Caphanexei"
		dynasty = "Vu-Sinopheu"
		birth_date = 1885.1.1
		adm = 0
		dip = 3
		mil = 2
    }
}

1964.1.1 = {
	monarch = {
 		name = "Phrixice"
		dynasty = "Vi-Sabixa"
		birth_date = 1939.1.1
		adm = 6
		dip = 2
		mil = 5
    }
}

2029.1.1 = {
	monarch = {
 		name = "Acsais"
		dynasty = "Vu-Nyxacia"
		birth_date = 1992.1.1
		adm = 4
		dip = 1
		mil = 2
		female = yes
    }
}

2107.1.1 = {
	monarch = {
 		name = "Acsaphine"
		dynasty = "Vi-Acaliphelia"
		birth_date = 2061.1.1
		adm = 2
		dip = 0
		mil = 6
		female = yes
    }
}

2163.1.1 = {
	monarch = {
 		name = "Caphanexei"
		dynasty = "Vi-Oriphephia"
		birth_date = 2131.1.1
		adm = 0
		dip = 6
		mil = 2
    }
}

2212.1.1 = {
	monarch = {
 		name = "Phrixice"
		dynasty = "Vu-Phaisusei"
		birth_date = 2175.1.1
		adm = 6
		dip = 6
		mil = 6
    }
}

2265.1.1 = {
	monarch = {
 		name = "Soteryx"
		dynasty = "Vu-Theanertes"
		birth_date = 2227.1.1
		adm = 1
		dip = 0
		mil = 1
    }
}

2358.1.1 = {
	monarch = {
 		name = "Depheus"
		dynasty = "Vu-Kaphosa"
		birth_date = 2309.1.1
		adm = 6
		dip = 6
		mil = 4
    }
}

2432.1.1 = {
	monarch = {
 		name = "Ansteas"
		dynasty = "Du-Selice"
		birth_date = 2407.1.1
		adm = 4
		dip = 5
		mil = 1
    }
}

