government = native
government_rank = 1
mercantilism = 1
technology_group = marshan_tg
religion = hist
primary_culture = agaceph
capital = 1096

54.1.1 = {
	monarch = {
 		name = "Tulalurash"
		dynasty = "Otumei"
		birth_date = 2.1.1
		adm = 0
		dip = 2
		mil = 2
    }
}

107.1.1 = {
	monarch = {
 		name = "Nokama"
		dynasty = "Caligeeja"
		birth_date = 70.1.1
		adm = 5
		dip = 1
		mil = 5
		female = yes
    }
}

202.1.1 = {
	monarch = {
 		name = "Hareeya"
		dynasty = "Wanan-Dar"
		birth_date = 151.1.1
		adm = 3
		dip = 0
		mil = 2
    }
}

238.1.1 = {
	monarch = {
 		name = "Beeheisei"
		dynasty = "Theodesh"
		birth_date = 213.1.1
		adm = 2
		dip = 6
		mil = 6
    }
}

337.1.1 = {
	monarch = {
 		name = "Tsleitzei"
		dynasty = "Canision"
		birth_date = 312.1.1
		adm = 3
		dip = 1
		mil = 0
		female = yes
    }
}

424.1.1 = {
	monarch = {
 		name = "Nojaxia"
		dynasty = "Taierleesh"
		birth_date = 380.1.1
		adm = 2
		dip = 0
		mil = 4
		female = yes
    }
}

469.1.1 = {
	monarch = {
 		name = "Ukka-Meedish"
		dynasty = "Caylus"
		birth_date = 448.1.1
		adm = 0
		dip = 6
		mil = 0
		female = yes
    }
}

563.1.1 = {
	monarch = {
 		name = "Nowajan"
		dynasty = "Jeeraz"
		birth_date = 532.1.1
		adm = 5
		dip = 5
		mil = 4
    }
}

623.1.1 = {
	monarch = {
 		name = "Jeetum"
		dynasty = "Herneen"
		birth_date = 586.1.1
		adm = 3
		dip = 5
		mil = 1
		female = yes
    }
}

715.1.1 = {
	monarch = {
 		name = "Bur-Lei"
		dynasty = "Caligeeja"
		birth_date = 687.1.1
		adm = 2
		dip = 4
		mil = 4
		female = yes
    }
}

757.1.1 = {
	monarch = {
 		name = "Ukatsei"
		dynasty = "Canimesh"
		birth_date = 730.1.1
		adm = 0
		dip = 3
		mil = 1
    }
}

818.1.1 = {
	monarch = {
 		name = "Notei"
		dynasty = "Jeeraz"
		birth_date = 796.1.1
		adm = 5
		dip = 2
		mil = 5
    }
}

858.1.1 = {
	monarch = {
 		name = "Mim-Naza"
		dynasty = "Andreecalees"
		birth_date = 810.1.1
		adm = 5
		dip = 6
		mil = 5
		female = yes
    }
}

951.1.1 = {
	monarch = {
 		name = "Buki"
		dynasty = "Endoremus"
		birth_date = 922.1.1
		adm = 5
		dip = 3
		mil = 3
		female = yes
    }
}

1033.1.1 = {
	monarch = {
 		name = "Am-Sakka"
		dynasty = "Ushureel"
		birth_date = 1013.1.1
		adm = 1
		dip = 4
		mil = 5
    }
}

1072.1.1 = {
	monarch = {
 		name = "Shanil-Nei"
		dynasty = "Yelnneen"
		birth_date = 1038.1.1
		adm = 6
		dip = 3
		mil = 2
    }
}

1116.1.1 = {
	monarch = {
 		name = "Mim-Jas"
		dynasty = "Cascalees"
		birth_date = 1067.1.1
		adm = 5
		dip = 2
		mil = 6
		female = yes
    }
}

1156.1.1 = {
	monarch = {
 		name = "Druseesh"
		dynasty = "Herneen"
		birth_date = 1113.1.1
		adm = 6
		dip = 4
		mil = 0
    }
}

1206.1.1 = {
	monarch = {
 		name = "Junal-Nakal"
		dynasty = "Derkeeja"
		birth_date = 1153.1.1
		adm = 4
		dip = 6
		mil = 0
		female = yes
    }
}

1297.1.1 = {
	monarch = {
 		name = "Eleedal-Lai"
		dynasty = "Casdorus"
		birth_date = 1267.1.1
		adm = 3
		dip = 2
		mil = 0
    }
}

1394.1.1 = {
	monarch = {
 		name = "Aomee-Ru"
		dynasty = "Reeeepa"
		birth_date = 1351.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1431.1.1 = {
	monarch = {
 		name = "Skeewul"
		dynasty = "Peteus"
		birth_date = 1400.1.1
		adm = 6
		dip = 0
		mil = 1
    }
}

1527.1.1 = {
	monarch = {
 		name = "Murak-Lei"
		dynasty = "Endoredeseer"
		birth_date = 1492.1.1
		adm = 5
		dip = 0
		mil = 4
		female = yes
    }
}

1593.1.1 = {
	monarch = {
 		name = "Eleedal-Kia"
		dynasty = "Pethees"
		birth_date = 1569.1.1
		adm = 3
		dip = 6
		mil = 1
    }
}

1640.1.1 = {
	monarch = {
 		name = "Aojee-Da"
		dynasty = "Otumei"
		birth_date = 1612.1.1
		adm = 1
		dip = 5
		mil = 5
    }
}

1693.1.1 = {
	monarch = {
 		name = "Skasei"
		dynasty = "Magsareeth"
		birth_date = 1664.1.1
		adm = 3
		dip = 6
		mil = 6
    }
}

1785.1.1 = {
	monarch = {
 		name = "Meenosh"
		dynasty = "Xemclesh"
		birth_date = 1736.1.1
		adm = 1
		dip = 6
		mil = 3
    }
}

1879.1.1 = {
	monarch = {
 		name = "Taxilteer"
		dynasty = "Mereemesh"
		birth_date = 1859.1.1
		adm = 0
		dip = 5
		mil = 6
		female = yes
    }
}

1919.1.1 = {
	monarch = {
 		name = "Mopakuz"
		dynasty = "Derkeeja"
		birth_date = 1900.1.1
		adm = 5
		dip = 4
		mil = 3
    }
}

2013.1.1 = {
	monarch = {
 		name = "Gah-Dum"
		dynasty = "Andreecalees"
		birth_date = 1969.1.1
		adm = 3
		dip = 3
		mil = 0
    }
}

2088.1.1 = {
	monarch = {
 		name = "Baar-Taeed"
		dynasty = "Okan-La"
		birth_date = 2068.1.1
		adm = 1
		dip = 2
		mil = 3
		female = yes
    }
}

2185.1.1 = {
	monarch = {
 		name = "Tar-Makka"
		dynasty = "Pethees"
		birth_date = 2158.1.1
		adm = 0
		dip = 2
		mil = 0
    }
}

2273.1.1 = {
	monarch = {
 		name = "Mohimeem"
		dynasty = "Caligeepa"
		birth_date = 2252.1.1
		adm = 5
		dip = 1
		mil = 4
    }
}

2341.1.1 = {
	monarch = {
 		name = "Heed-Nakal"
		dynasty = "Caligeeja"
		birth_date = 2303.1.1
		adm = 0
		dip = 2
		mil = 5
		female = yes
    }
}

2376.1.1 = {
	monarch = {
 		name = "Asheeus"
		dynasty = "Ushureel"
		birth_date = 2347.1.1
		adm = 5
		dip = 1
		mil = 2
    }
}

2464.1.1 = {
	monarch = {
 		name = "Gilustulm"
		dynasty = "Peteus"
		birth_date = 2420.1.1
		adm = 3
		dip = 0
		mil = 5
    }
}

