government = monarchy
government_rank = 1
mercantilism = 1
technology_group = daedric_tg
religion = molag_bal_cult
primary_culture = vampire
capital = 2834

54.1.1 = {
	monarch = {
 		name = "Tinus"
		dynasty = "Daan"
		birth_date = 5.1.1
		adm = 0
		dip = 5
		mil = 1
    }
	add_ruler_personality = immortal_personality
	queen = {
 		name = "Eloe"
		dynasty = "Daan"
		birth_date = 26.1.1
		adm = 4
		dip = 4
		mil = 0
		female = yes
    }
	add_queen_personality = immortal_personality
	heir = {
 		name = "Septima"
		monarch_name = "Septima I"
		dynasty = "Daan"
		birth_date = 49.1.1
		death_date = 91.1.1
		claim = 100
		adm = 5
		dip = 4
		mil = 5
		female = yes
    }
	add_heir_personality = immortal_personality
}

91.1.1 = {
	monarch = {
 		name = "Lekellae"
		dynasty = "Arado"
		birth_date = 71.1.1
		adm = 5
		dip = 1
		mil = 5
		female = yes
    }
	add_ruler_personality = immortal_personality
}

129.1.1 = {
	monarch = {
 		name = "Ennodius"
		dynasty = "Akodon"
		birth_date = 93.1.1
		adm = 3
		dip = 0
		mil = 2
    }
	add_ruler_personality = immortal_personality
	queen = {
 		name = "Oriandra"
		dynasty = "Akodon"
		birth_date = 98.1.1
		adm = 0
		dip = 5
		mil = 1
		female = yes
    }
	add_queen_personality = immortal_personality
	heir = {
 		name = "Nemus"
		monarch_name = "Nemus I"
		dynasty = "Akodon"
		birth_date = 120.1.1
		death_date = 211.1.1
		claim = 100
		adm = 3
		dip = 0
		mil = 3
    }
	add_heir_personality = immortal_personality
}

211.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Vittoria"
		monarch_name = "Vittoria I"
		dynasty = "Byrlush"
		birth_date = 206.1.1
		death_date = 224.1.1
		claim = 100
		adm = 2
		dip = 3
		mil = 3
		female = yes
    }
}

224.1.1 = {
	monarch = {
 		name = "Martin"
		dynasty = "Merluar"
		birth_date = 196.1.1
		adm = 5
		dip = 5
		mil = 6
    }
	add_ruler_personality = immortal_personality
}

298.1.1 = {
	monarch = {
 		name = "Ovidius"
		dynasty = "Kerlo"
		birth_date = 267.1.1
		adm = 5
		dip = 2
		mil = 3
    }
	add_ruler_personality = immortal_personality
}

341.1.1 = {
	monarch = {
 		name = "Honorata"
		dynasty = "Bala"
		birth_date = 290.1.1
		adm = 4
		dip = 1
		mil = 0
		female = yes
    }
	add_ruler_personality = immortal_personality
}

399.1.1 = {
	monarch = {
 		name = "Camilla"
		dynasty = "Myluar"
		birth_date = 379.1.1
		adm = 2
		dip = 0
		mil = 4
		female = yes
    }
	add_ruler_personality = immortal_personality
	queen = {
 		name = "Arentus"
		dynasty = "Myluar"
		birth_date = 373.1.1
		adm = 6
		dip = 6
		mil = 3
    }
	add_queen_personality = immortal_personality
	heir = {
 		name = "August"
		monarch_name = "August I"
		dynasty = "Myluar"
		birth_date = 392.1.1
		death_date = 458.1.1
		claim = 100
		adm = 3
		dip = 4
		mil = 2
    }
	add_heir_personality = immortal_personality
}

458.1.1 = {
	monarch = {
 		name = "Otho"
		dynasty = "Cynhamoth"
		birth_date = 419.1.1
		adm = 2
		dip = 0
		mil = 2
    }
	add_ruler_personality = immortal_personality
	queen = {
 		name = "Lepida"
		dynasty = "Cynhamoth"
		birth_date = 421.1.1
		adm = 2
		dip = 4
		mil = 3
		female = yes
    }
	add_queen_personality = immortal_personality
}

513.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Percius"
		monarch_name = "Percius I"
		dynasty = "Tebro"
		birth_date = 507.1.1
		death_date = 525.1.1
		claim = 100
		adm = 3
		dip = 1
		mil = 1
    }
}

525.1.1 = {
	monarch = {
 		name = "Patia"
		dynasty = "Vaarla"
		birth_date = 505.1.1
		adm = 2
		dip = 4
		mil = 1
		female = yes
    }
	add_ruler_personality = immortal_personality
}

563.1.1 = {
	monarch = {
 		name = "Visetus"
		dynasty = "Oroda"
		birth_date = 522.1.1
		adm = 1
		dip = 4
		mil = 5
    }
	add_ruler_personality = immortal_personality
}

655.1.1 = {
	monarch = {
 		name = "Phedre"
		dynasty = "Lash"
		birth_date = 610.1.1
		adm = 6
		dip = 3
		mil = 1
		female = yes
    }
	add_ruler_personality = immortal_personality
	queen = {
 		name = "Baszone"
		dynasty = "Lash"
		birth_date = 634.1.1
		adm = 3
		dip = 1
		mil = 0
    }
	add_queen_personality = immortal_personality
	heir = {
 		name = "Pelinal"
		monarch_name = "Pelinal I"
		dynasty = "Lash"
		birth_date = 654.1.1
		death_date = 731.1.1
		claim = 100
		adm = 0
		dip = 0
		mil = 6
    }
	add_heir_personality = immortal_personality
}

731.1.1 = {
	monarch = {
 		name = "Contumeliorus"
		dynasty = "Maahla"
		birth_date = 690.1.1
		adm = 2
		dip = 1
		mil = 2
    }
	add_ruler_personality = immortal_personality
	queen = {
 		name = "Magna"
		dynasty = "Maahla"
		birth_date = 681.1.1
		adm = 6
		dip = 0
		mil = 1
		female = yes
    }
	add_queen_personality = immortal_personality
	heir = {
 		name = "Cardea"
		monarch_name = "Cardea I"
		dynasty = "Maahla"
		birth_date = 728.1.1
		death_date = 800.1.1
		claim = 100
		adm = 3
		dip = 5
		mil = 0
		female = yes
    }
	add_heir_personality = immortal_personality
}

800.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Valandrus"
		monarch_name = "Valandrus I"
		dynasty = "Onasso"
		birth_date = 787.1.1
		death_date = 805.1.1
		claim = 100
		adm = 1
		dip = 5
		mil = 3
    }
}

805.1.1 = {
	monarch = {
 		name = "Laelius"
		dynasty = "Mashra"
		birth_date = 758.1.1
		adm = 1
		dip = 1
		mil = 4
    }
	add_ruler_personality = immortal_personality
	queen = {
 		name = "Gruiand"
		dynasty = "Mashra"
		birth_date = 786.1.1
		adm = 1
		dip = 4
		mil = 5
		female = yes
    }
	add_queen_personality = immortal_personality
	heir = {
 		name = "Varius"
		monarch_name = "Varius I"
		dynasty = "Mashra"
		birth_date = 795.1.1
		death_date = 881.1.1
		claim = 100
		adm = 5
		dip = 3
		mil = 4
    }
	add_heir_personality = immortal_personality
}

881.1.1 = {
	monarch = {
 		name = "Jerina"
		dynasty = "Romno"
		birth_date = 845.1.1
		adm = 4
		dip = 6
		mil = 4
		female = yes
    }
	add_ruler_personality = immortal_personality
	queen = {
 		name = "Tiburtius"
		dynasty = "Romno"
		birth_date = 834.1.1
		adm = 1
		dip = 5
		mil = 3
    }
	add_queen_personality = immortal_personality
}

977.1.1 = {
	monarch = {
 		name = "Senilias"
		dynasty = "Misel"
		birth_date = 934.1.1
		adm = 6
		dip = 4
		mil = 0
    }
	add_ruler_personality = immortal_personality
}

1076.1.1 = {
	monarch = {
 		name = "Laurina"
		dynasty = "Beka"
		birth_date = 1053.1.1
		adm = 5
		dip = 3
		mil = 3
		female = yes
    }
	add_ruler_personality = immortal_personality
}

1167.1.1 = {
	monarch = {
 		name = "Eponis"
		dynasty = "Alerno"
		birth_date = 1134.1.1
		adm = 3
		dip = 3
		mil = 0
		female = yes
    }
	add_ruler_personality = immortal_personality
}

1237.1.1 = {
	monarch = {
 		name = "Amantius"
		dynasty = "Menaa"
		birth_date = 1204.1.1
		adm = 1
		dip = 2
		mil = 3
    }
	add_ruler_personality = immortal_personality
	queen = {
 		name = "Idonea"
		dynasty = "Menaa"
		birth_date = 1201.1.1
		adm = 5
		dip = 0
		mil = 2
		female = yes
    }
	add_queen_personality = immortal_personality
}

1281.1.1 = {
	monarch = {
 		name = "Gallus"
		dynasty = "Kaamii"
		birth_date = 1239.1.1
		adm = 3
		dip = 6
		mil = 6
    }
	add_ruler_personality = immortal_personality
}

1333.1.1 = {
	monarch = {
 		name = "Augurius"
		dynasty = "Omaanaa"
		birth_date = 1290.1.1
		adm = 1
		dip = 6
		mil = 3
    }
	add_ruler_personality = immortal_personality
}

1377.1.1 = {
	monarch = {
 		name = "Zeno"
		dynasty = "Maahla"
		birth_date = 1340.1.1
		adm = 0
		dip = 5
		mil = 6
    }
	add_ruler_personality = immortal_personality
	queen = {
 		name = "Quintia"
		dynasty = "Maahla"
		birth_date = 1334.1.1
		adm = 4
		dip = 3
		mil = 5
		female = yes
    }
	add_queen_personality = immortal_personality
	heir = {
 		name = "Fadus"
		monarch_name = "Fadus I"
		dynasty = "Maahla"
		birth_date = 1370.1.1
		death_date = 1474.1.1
		claim = 100
		adm = 0
		dip = 2
		mil = 4
    }
	add_heir_personality = immortal_personality
}

1474.1.1 = {
	monarch = {
 		name = "Gaius"
		dynasty = "Laam"
		birth_date = 1428.1.1
		adm = 0
		dip = 5
		mil = 5
    }
	add_ruler_personality = immortal_personality
}

1554.1.1 = {
	monarch = {
 		name = "Nicolas"
		dynasty = "Alerno"
		birth_date = 1510.1.1
		adm = 5
		dip = 5
		mil = 1
    }
	add_ruler_personality = immortal_personality
	queen = {
 		name = "Ysabel"
		dynasty = "Alerno"
		birth_date = 1509.1.1
		adm = 2
		dip = 3
		mil = 0
		female = yes
    }
	add_queen_personality = immortal_personality
	heir = {
 		name = "Maxilien"
		monarch_name = "Maxilien I"
		dynasty = "Alerno"
		birth_date = 1551.1.1
		death_date = 1605.1.1
		claim = 100
		adm = 6
		dip = 2
		mil = 6
    }
	add_heir_personality = immortal_personality
}

1605.1.1 = {
	monarch = {
 		name = "Baro"
		dynasty = "Tebro"
		birth_date = 1562.1.1
		adm = 2
		dip = 3
		mil = 2
    }
	add_ruler_personality = immortal_personality
}

1702.1.1 = {
	monarch = {
 		name = "Tappius"
		dynasty = "Cohna"
		birth_date = 1672.1.1
		adm = 0
		dip = 2
		mil = 5
    }
	add_ruler_personality = immortal_personality
	queen = {
 		name = "Dumania"
		dynasty = "Cohna"
		birth_date = 1657.1.1
		adm = 4
		dip = 1
		mil = 4
		female = yes
    }
	add_queen_personality = immortal_personality
}

1764.1.1 = {
	monarch = {
 		name = "Carmalo"
		dynasty = "Cedo"
		birth_date = 1730.1.1
		adm = 2
		dip = 0
		mil = 1
    }
	add_ruler_personality = immortal_personality
}

1811.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Aventina"
		monarch_name = "Aventina I"
		dynasty = "Acorsha"
		birth_date = 1811.1.1
		death_date = 1829.1.1
		claim = 100
		adm = 3
		dip = 4
		mil = 6
		female = yes
    }
}

1829.1.1 = {
	monarch = {
 		name = "Paratus"
		dynasty = "Kaatta"
		birth_date = 1795.1.1
		adm = 5
		dip = 5
		mil = 1
    }
	add_ruler_personality = immortal_personality
	queen = {
 		name = "Licinia"
		dynasty = "Kaatta"
		birth_date = 1797.1.1
		adm = 2
		dip = 4
		mil = 0
		female = yes
    }
	add_queen_personality = immortal_personality
}

1892.1.1 = {
	monarch = {
 		name = "Viator"
		dynasty = "Bolam"
		birth_date = 1857.1.1
		adm = 1
		dip = 3
		mil = 4
    }
	add_ruler_personality = immortal_personality
}

1961.1.1 = {
	monarch = {
 		name = "Peregrina"
		dynasty = "Kedas"
		birth_date = 1912.1.1
		adm = 2
		dip = 4
		mil = 5
		female = yes
    }
	add_ruler_personality = immortal_personality
	queen = {
 		name = "Novatus"
		dynasty = "Kedas"
		birth_date = 1912.1.1
		adm = 3
		dip = 1
		mil = 6
    }
	add_queen_personality = immortal_personality
}

2022.1.1 = {
	monarch = {
 		name = "Adrianne"
		dynasty = "Kaarniin"
		birth_date = 1980.1.1
		adm = 5
		dip = 2
		mil = 1
		female = yes
    }
	add_ruler_personality = immortal_personality
}

2058.1.1 = {
	monarch = {
 		name = "Rasala"
		dynasty = "Laatta"
		birth_date = 2035.1.1
		adm = 3
		dip = 1
		mil = 4
		female = yes
    }
	add_ruler_personality = immortal_personality
}

2124.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Corvus"
		monarch_name = "Corvus I"
		dynasty = "Hollow"
		birth_date = 2123.1.1
		death_date = 2141.1.1
		claim = 100
		adm = 0
		dip = 3
		mil = 5
    }
}

2141.1.1 = {
	monarch = {
 		name = "Scipion"
		dynasty = "Lorsha"
		birth_date = 2094.1.1
		adm = 6
		dip = 0
		mil = 5
    }
	add_ruler_personality = immortal_personality
}

2231.1.1 = {
	monarch = {
 		name = "Marinus"
		dynasty = "Darshaa"
		birth_date = 2184.1.1
		adm = 5
		dip = 6
		mil = 1
    }
	add_ruler_personality = immortal_personality
}

2277.1.1 = {
	monarch = {
 		name = "Dunius"
		dynasty = "Misel"
		birth_date = 2254.1.1
		adm = 4
		dip = 2
		mil = 2
    }
	add_ruler_personality = immortal_personality
}

2327.1.1 = {
	monarch = {
 		name = "Petronius"
		dynasty = "Bala"
		birth_date = 2299.1.1
		adm = 2
		dip = 1
		mil = 5
    }
	add_ruler_personality = immortal_personality
}

2411.1.1 = {
	monarch = {
 		name = "Velus"
		dynasty = "Cohna"
		birth_date = 2383.1.1
		adm = 0
		dip = 1
		mil = 2
    }
	add_ruler_personality = immortal_personality
}

2479.1.1 = {
	monarch = {
 		name = "Plautisanus"
		dynasty = "Basua"
		birth_date = 2440.1.1
		adm = 6
		dip = 0
		mil = 6
    }
	add_ruler_personality = immortal_personality
}

