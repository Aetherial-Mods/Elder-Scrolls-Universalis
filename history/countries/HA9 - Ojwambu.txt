government = monarchy
government_rank = 1
mercantilism = 1
technology_group = cyrodiil_tg
religion = four_parents
primary_culture = keptu
capital = 6292

54.1.1 = {
	monarch = {
 		name = "Zeli"
		dynasty = "Vristidok"
		birth_date = 24.1.1
		adm = 6
		dip = 2
		mil = 2
		female = yes
    }
}

119.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Virdanath"
		monarch_name = "Virdanath I"
		dynasty = "Gumrok"
		birth_date = 117.1.1
		death_date = 135.1.1
		claim = 100
		adm = 6
		dip = 2
		mil = 4
    }
}

135.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Herlir"
		monarch_name = "Herlir I"
		dynasty = "Runhath"
		birth_date = 128.1.1
		death_date = 146.1.1
		claim = 100
		adm = 4
		dip = 1
		mil = 0
    }
}

146.1.1 = {
	monarch = {
 		name = "Riralath"
		dynasty = "Kaomrar"
		birth_date = 108.1.1
		adm = 4
		dip = 4
		mil = 0
    }
	queen = {
 		name = "Ellur"
		dynasty = "Kaomrar"
		birth_date = 93.1.1
		adm = 4
		dip = 1
		mil = 2
		female = yes
    }
}

191.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Tylak"
		monarch_name = "Tylak I"
		dynasty = "Lyrmerek"
		birth_date = 181.1.1
		death_date = 199.1.1
		claim = 100
		adm = 5
		dip = 5
		mil = 6
    }
}

199.1.1 = {
	monarch = {
 		name = "Lashavun"
		dynasty = "Dauhrak"
		birth_date = 168.1.1
		adm = 4
		dip = 1
		mil = 0
		female = yes
    }
}

244.1.1 = {
	monarch = {
 		name = "Hiherev"
		dynasty = "Nuvnizo"
		birth_date = 215.1.1
		adm = 2
		dip = 1
		mil = 3
		female = yes
    }
	queen = {
 		name = "Bardek"
		dynasty = "Nuvnizo"
		birth_date = 224.1.1
		adm = 6
		dip = 6
		mil = 2
    }
	heir = {
 		name = "Remaasok"
		monarch_name = "Remaasok I"
		dynasty = "Nuvnizo"
		birth_date = 237.1.1
		death_date = 300.1.1
		claim = 100
		adm = 3
		dip = 5
		mil = 1
    }
}

300.1.1 = {
	monarch = {
 		name = "Nizae"
		dynasty = "Tamedir"
		birth_date = 273.1.1
		adm = 6
		dip = 6
		mil = 4
		female = yes
    }
}

391.1.1 = {
	monarch = {
 		name = "Vauleso"
		dynasty = "Vurmesoth"
		birth_date = 349.1.1
		adm = 4
		dip = 5
		mil = 0
    }
}

463.1.1 = {
	monarch = {
 		name = "Hiherev"
		dynasty = "Bauvmith"
		birth_date = 426.1.1
		adm = 2
		dip = 4
		mil = 4
		female = yes
    }
}

510.1.1 = {
	monarch = {
 		name = "Naamaulec"
		dynasty = "Nirlyloth"
		birth_date = 491.1.1
		adm = 1
		dip = 4
		mil = 1
    }
	queen = {
 		name = "Vena"
		dynasty = "Nirlyloth"
		birth_date = 487.1.1
		adm = 4
		dip = 2
		mil = 0
		female = yes
    }
	heir = {
 		name = "Runhath"
		monarch_name = "Runhath I"
		dynasty = "Nirlyloth"
		birth_date = 499.1.1
		death_date = 600.1.1
		claim = 100
		adm = 1
		dip = 1
		mil = 6
    }
}

600.1.1 = {
	monarch = {
 		name = "Kalrin"
		dynasty = "Likoth"
		birth_date = 548.1.1
		adm = 1
		dip = 4
		mil = 6
    }
	queen = {
 		name = "Lonnev"
		dynasty = "Likoth"
		birth_date = 557.1.1
		adm = 5
		dip = 3
		mil = 5
		female = yes
    }
}

662.1.1 = {
	monarch = {
 		name = "Vredraudin"
		dynasty = "Shahaulec"
		birth_date = 627.1.1
		adm = 2
		dip = 5
		mil = 5
    }
}

731.1.1 = {
	monarch = {
 		name = "Nestith"
		dynasty = "Lysteth"
		birth_date = 690.1.1
		adm = 0
		dip = 4
		mil = 1
    }
	queen = {
 		name = "Lassoba"
		dynasty = "Lysteth"
		birth_date = 688.1.1
		adm = 4
		dip = 2
		mil = 0
		female = yes
    }
	heir = {
 		name = "Herlir"
		monarch_name = "Herlir I"
		dynasty = "Lysteth"
		birth_date = 719.1.1
		death_date = 774.1.1
		claim = 100
		adm = 1
		dip = 1
		mil = 6
    }
}

774.1.1 = {
	monarch = {
 		name = "Lonnev"
		dynasty = "Kalrin"
		birth_date = 729.1.1
		adm = 0
		dip = 4
		mil = 0
		female = yes
    }
}

823.1.1 = {
	monarch = {
 		name = "Tazamaev"
		dynasty = "Nirdoc"
		birth_date = 771.1.1
		adm = 4
		dip = 6
		mil = 0
		female = yes
    }
	queen = {
 		name = "Halaanis"
		dynasty = "Nirdoc"
		birth_date = 797.1.1
		adm = 1
		dip = 5
		mil = 6
    }
	heir = {
 		name = "Tadak"
		monarch_name = "Tadak I"
		dynasty = "Nirdoc"
		birth_date = 813.1.1
		death_date = 879.1.1
		claim = 100
		adm = 5
		dip = 3
		mil = 5
    }
}

879.1.1 = {
	monarch = {
 		name = "Shelren"
		dynasty = "Remaasok"
		birth_date = 830.1.1
		adm = 4
		dip = 0
		mil = 5
    }
}

929.1.1 = {
	monarch = {
 		name = "Ihrasek"
		dynasty = "Zevmin"
		birth_date = 878.1.1
		adm = 3
		dip = 6
		mil = 2
    }
}

1011.1.1 = {
	monarch = {
 		name = "Zevmin"
		dynasty = "Bauvmith"
		birth_date = 972.1.1
		adm = 5
		dip = 0
		mil = 4
    }
	queen = {
 		name = "Hella"
		dynasty = "Bauvmith"
		birth_date = 969.1.1
		adm = 2
		dip = 6
		mil = 3
		female = yes
    }
}

1080.1.1 = {
	monarch = {
 		name = "Vohu"
		dynasty = "Naanuzar"
		birth_date = 1051.1.1
		adm = 0
		dip = 5
		mil = 6
		female = yes
    }
	queen = {
 		name = "Vristidok"
		dynasty = "Naanuzar"
		birth_date = 1042.1.1
		adm = 0
		dip = 1
		mil = 0
    }
}

1144.1.1 = {
	monarch = {
 		name = "Ihrasek"
		dynasty = "Vristidok"
		birth_date = 1111.1.1
		adm = 3
		dip = 3
		mil = 2
    }
}

1221.1.1 = {
	monarch = {
 		name = "Zevmin"
		dynasty = "Zakysor"
		birth_date = 1185.1.1
		adm = 1
		dip = 2
		mil = 6
    }
}

1316.1.1 = {
	monarch = {
 		name = "Vonal"
		dynasty = "Nhystareth"
		birth_date = 1288.1.1
		adm = 2
		dip = 6
		mil = 4
		female = yes
    }
	queen = {
 		name = "Dauhrak"
		dynasty = "Nhystareth"
		birth_date = 1282.1.1
		adm = 5
		dip = 5
		mil = 3
    }
}

1380.1.1 = {
	monarch = {
 		name = "Dennida"
		dynasty = "Daolo"
		birth_date = 1336.1.1
		adm = 4
		dip = 4
		mil = 6
		female = yes
    }
}

1449.1.1 = {
	monarch = {
 		name = "Menhadeth"
		dynasty = "Zurduzok"
		birth_date = 1401.1.1
		adm = 2
		dip = 3
		mil = 3
    }
	queen = {
 		name = "Resa"
		dynasty = "Zurduzok"
		birth_date = 1398.1.1
		adm = 6
		dip = 2
		mil = 2
		female = yes
    }
	heir = {
 		name = "Dulros"
		monarch_name = "Dulros I"
		dynasty = "Zurduzok"
		birth_date = 1448.1.1
		death_date = 1544.1.1
		claim = 100
		adm = 3
		dip = 0
		mil = 1
    }
}

1544.1.1 = {
	monarch = {
 		name = "Havu"
		dynasty = "Tirleros"
		birth_date = 1526.1.1
		adm = 2
		dip = 3
		mil = 1
		female = yes
    }
}

1600.1.1 = {
	monarch = {
 		name = "Norri"
		dynasty = "Lysteth"
		birth_date = 1571.1.1
		adm = 0
		dip = 3
		mil = 5
		female = yes
    }
	queen = {
 		name = "Nauhrimak"
		dynasty = "Lysteth"
		birth_date = 1573.1.1
		adm = 4
		dip = 1
		mil = 4
    }
	heir = {
 		name = "Vena"
		monarch_name = "Vena I"
		dynasty = "Lysteth"
		birth_date = 1599.1.1
		death_date = 1688.1.1
		claim = 100
		adm = 4
		dip = 5
		mil = 5
		female = yes
    }
}

1688.1.1 = {
	monarch = {
 		name = "Gikamik"
		dynasty = "Shahaulec"
		birth_date = 1648.1.1
		adm = 4
		dip = 1
		mil = 5
    }
	queen = {
 		name = "Zassun"
		dynasty = "Shahaulec"
		birth_date = 1663.1.1
		adm = 1
		dip = 0
		mil = 4
		female = yes
    }
	heir = {
 		name = "Lonnev"
		monarch_name = "Lonnev I"
		dynasty = "Shahaulec"
		birth_date = 1679.1.1
		death_date = 1730.1.1
		claim = 100
		adm = 5
		dip = 5
		mil = 3
		female = yes
    }
}

1730.1.1 = {
	monarch = {
 		name = "Izaev"
		dynasty = "Yhin"
		birth_date = 1695.1.1
		adm = 0
		dip = 6
		mil = 5
		female = yes
    }
	queen = {
 		name = "Nauhrimak"
		dynasty = "Yhin"
		birth_date = 1698.1.1
		adm = 4
		dip = 5
		mil = 4
    }
	heir = {
 		name = "Retobil"
		monarch_name = "Retobil I"
		dynasty = "Yhin"
		birth_date = 1723.1.1
		death_date = 1765.1.1
		claim = 100
		adm = 1
		dip = 4
		mil = 3
		female = yes
    }
}

1765.1.1 = {
	monarch = {
 		name = "Terenu"
		dynasty = "Likoth"
		birth_date = 1732.1.1
		adm = 4
		dip = 5
		mil = 6
		female = yes
    }
}

1844.1.1 = {
	monarch = {
 		name = "Tykisok"
		dynasty = "Hetok"
		birth_date = 1802.1.1
		adm = 6
		dip = 6
		mil = 0
    }
}

1879.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Emin"
		monarch_name = "Emin I"
		dynasty = "Shahaulec"
		birth_date = 1868.1.1
		death_date = 1886.1.1
		claim = 100
		adm = 3
		dip = 1
		mil = 0
		female = yes
    }
}

1886.1.1 = {
	monarch = {
 		name = "Nhudren"
		dynasty = "Vaulic"
		birth_date = 1841.1.1
		adm = 2
		dip = 5
		mil = 1
    }
}

1954.1.1 = {
	monarch = {
 		name = "Lyrmerek"
		dynasty = "Likoth"
		birth_date = 1914.1.1
		adm = 0
		dip = 4
		mil = 4
    }
	queen = {
 		name = "Vallal"
		dynasty = "Likoth"
		birth_date = 1909.1.1
		adm = 4
		dip = 2
		mil = 3
		female = yes
    }
	heir = {
 		name = "Nhystareth"
		monarch_name = "Nhystareth I"
		dynasty = "Likoth"
		birth_date = 1943.1.1
		death_date = 2035.1.1
		claim = 100
		adm = 1
		dip = 1
		mil = 2
    }
}

2035.1.1 = {
	monarch = {
 		name = "Amedoc"
		dynasty = "Idith"
		birth_date = 2004.1.1
		adm = 4
		dip = 2
		mil = 4
    }
	queen = {
 		name = "Viladuh"
		dynasty = "Idith"
		birth_date = 1991.1.1
		adm = 1
		dip = 1
		mil = 3
		female = yes
    }
	heir = {
 		name = "Vrakir"
		monarch_name = "Vrakir I"
		dynasty = "Idith"
		birth_date = 2023.1.1
		death_date = 2075.1.1
		claim = 100
		adm = 5
		dip = 6
		mil = 2
    }
}

2075.1.1 = {
	monarch = {
 		name = "Dulros"
		dynasty = "Zyrdusor"
		birth_date = 2050.1.1
		adm = 1
		dip = 5
		mil = 1
    }
}

2140.1.1 = {
	monarch = {
 		name = "Vristidok"
		dynasty = "Tamedir"
		birth_date = 2114.1.1
		adm = 0
		dip = 4
		mil = 5
    }
	queen = {
 		name = "Ithamav"
		dynasty = "Tamedir"
		birth_date = 2113.1.1
		adm = 2
		dip = 5
		mil = 0
		female = yes
    }
	heir = {
 		name = "Menhadeth"
		monarch_name = "Menhadeth I"
		dynasty = "Tamedir"
		birth_date = 2126.1.1
		death_date = 2221.1.1
		claim = 100
		adm = 6
		dip = 4
		mil = 6
    }
}

2221.1.1 = {
	monarch = {
 		name = "Zunhe"
		dynasty = "Vinhylak"
		birth_date = 2200.1.1
		adm = 6
		dip = 0
		mil = 0
    }
}

2295.1.1 = {
	monarch = {
 		name = "Kalrin"
		dynasty = "Halaanis"
		birth_date = 2276.1.1
		adm = 4
		dip = 6
		mil = 3
    }
	queen = {
 		name = "Sonev"
		dynasty = "Halaanis"
		birth_date = 2255.1.1
		adm = 1
		dip = 5
		mil = 2
		female = yes
    }
	heir = {
 		name = "Demralis"
		monarch_name = "Demralis I"
		dynasty = "Halaanis"
		birth_date = 2290.1.1
		death_date = 2377.1.1
		claim = 100
		adm = 5
		dip = 4
		mil = 1
    }
}

2377.1.1 = {
	monarch = {
 		name = "Tadak"
		dynasty = "Ludec"
		birth_date = 2355.1.1
		adm = 0
		dip = 5
		mil = 4
    }
}

2450.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Idith"
		monarch_name = "Idith I"
		dynasty = "Shahaulec"
		birth_date = 2445.1.1
		death_date = 2463.1.1
		claim = 100
		adm = 1
		dip = 2
		mil = 1
    }
}

2463.1.1 = {
	monarch = {
 		name = "Zyrdusor"
		dynasty = "Nauhrimak"
		birth_date = 2423.1.1
		adm = 4
		dip = 3
		mil = 4
    }
	queen = {
 		name = "Hotedi"
		dynasty = "Nauhrimak"
		birth_date = 2427.1.1
		adm = 1
		dip = 2
		mil = 3
		female = yes
    }
	heir = {
 		name = "Izaev"
		monarch_name = "Izaev I"
		dynasty = "Nauhrimak"
		birth_date = 2461.1.1
		death_date = 2531.1.1
		claim = 100
		adm = 5
		dip = 0
		mil = 2
		female = yes
    }
}

