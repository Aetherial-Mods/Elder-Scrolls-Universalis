government = republic
government_rank = 3
mercantilism = 1
technology_group = cyrodiil_tg
religion = nedic_pantheon
primary_culture = nedic
capital = 1194

54.1.1 = {
	monarch = {
 		name = "Malrasoc"
		dynasty = "Gaohen"
		birth_date = 31.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

114.1.1 = {
	monarch = {
 		name = "Homin"
		dynasty = "Ihrasek"
		birth_date = 62.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

152.1.1 = {
	monarch = {
 		name = "Idith"
		dynasty = "Nirdoc"
		birth_date = 115.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

243.1.1 = {
	monarch = {
 		name = "Taatysoth"
		dynasty = "Herlir"
		birth_date = 209.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

341.1.1 = {
	monarch = {
 		name = "Gakaazin"
		dynasty = "Vrakir"
		birth_date = 312.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

386.1.1 = {
	monarch = {
 		name = "Vrilak"
		dynasty = "Naamaulec"
		birth_date = 341.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

448.1.1 = {
	monarch = {
 		name = "Gikamik"
		dynasty = "Virdanath"
		birth_date = 399.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

495.1.1 = {
	monarch = {
 		name = "Tykisok"
		dynasty = "Vurmesoth"
		birth_date = 456.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

580.1.1 = {
	monarch = {
 		name = "Gakaazin"
		dynasty = "Rardoc"
		birth_date = 533.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

641.1.1 = {
	monarch = {
 		name = "Vrilak"
		dynasty = "Naanuzar"
		birth_date = 614.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

725.1.1 = {
	monarch = {
 		name = "Gikamik"
		dynasty = "Vurmesoth"
		birth_date = 685.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

778.1.1 = {
	monarch = {
 		name = "Tykisok"
		dynasty = "Vredraudin"
		birth_date = 730.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

845.1.1 = {
	monarch = {
 		name = "Nimoner"
		dynasty = "Rukec"
		birth_date = 812.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

892.1.1 = {
	monarch = {
 		name = "Ranni"
		dynasty = "Nhystareth"
		birth_date = 863.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

984.1.1 = {
	monarch = {
 		name = "Lyrmerek"
		dynasty = "Lukaazon"
		birth_date = 952.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1047.1.1 = {
	monarch = {
 		name = "Naonhaudes"
		dynasty = "Vredraudin"
		birth_date = 1024.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1143.1.1 = {
	monarch = {
 		name = "Horremel"
		dynasty = "Ludec"
		birth_date = 1124.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1203.1.1 = {
	monarch = {
 		name = "Ridravin"
		dynasty = "Letan"
		birth_date = 1155.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1245.1.1 = {
	monarch = {
 		name = "Nuvnizo"
		dynasty = "Bardek"
		birth_date = 1195.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1302.1.1 = {
	monarch = {
 		name = "Hynhaolen"
		dynasty = "Gaohen"
		birth_date = 1277.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1384.1.1 = {
	monarch = {
 		name = "Menhadeth"
		dynasty = "Gumrok"
		birth_date = 1342.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1447.1.1 = {
	monarch = {
 		name = "Idith"
		dynasty = "Nhekhedin"
		birth_date = 1395.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1494.1.1 = {
	monarch = {
 		name = "Taatysoth"
		dynasty = "Dauhrak"
		birth_date = 1444.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1538.1.1 = {
	monarch = {
 		name = "Halaanis"
		dynasty = "Gahlaras"
		birth_date = 1492.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1619.1.1 = {
	monarch = {
 		name = "Kaomrar"
		dynasty = "Aalezos"
		birth_date = 1567.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1701.1.1 = {
	monarch = {
 		name = "Riralath"
		dynasty = "Shelren"
		birth_date = 1649.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1793.1.1 = {
	monarch = {
 		name = "Tykisok"
		dynasty = "Tudaarin"
		birth_date = 1748.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1845.1.1 = {
	monarch = {
 		name = "Tylak"
		dynasty = "Vrustimek"
		birth_date = 1812.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1882.1.1 = {
	monarch = {
 		name = "Mazan"
		dynasty = "Tamedir"
		birth_date = 1829.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1954.1.1 = {
	monarch = {
 		name = "Gahlaras"
		dynasty = "Hyker"
		birth_date = 1907.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2008.1.1 = {
	monarch = {
 		name = "Zonemaer"
		dynasty = "Bauvmith"
		birth_date = 1969.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

2078.1.1 = {
	monarch = {
 		name = "Tylak"
		dynasty = "Tamedir"
		birth_date = 2050.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2122.1.1 = {
	monarch = {
 		name = "Gilraoles"
		dynasty = "Gumrok"
		birth_date = 2080.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2184.1.1 = {
	monarch = {
 		name = "Gahlaras"
		dynasty = "Dauhrak"
		birth_date = 2157.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2244.1.1 = {
	monarch = {
 		name = "Iran"
		dynasty = "Likoth"
		birth_date = 2195.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

2300.1.1 = {
	monarch = {
 		name = "Runhath"
		dynasty = "Haukos"
		birth_date = 2254.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2360.1.1 = {
	monarch = {
 		name = "Soshemeh"
		dynasty = "Nestith"
		birth_date = 2308.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

2401.1.1 = {
	monarch = {
 		name = "Nhihreseth"
		dynasty = "Runhyrac"
		birth_date = 2375.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2446.1.1 = {
	monarch = {
 		name = "Iran"
		dynasty = "Vrilak"
		birth_date = 2406.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

2498.1.1 = {
	monarch = {
 		name = "Runhath"
		dynasty = "Runhath"
		birth_date = 2466.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

