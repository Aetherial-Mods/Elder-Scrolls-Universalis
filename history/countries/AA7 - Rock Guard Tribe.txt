government = native
government_rank = 1
mercantilism = 1
technology_group = marshan_tg
religion = elder_gods
primary_culture = yespest
capital = 1080

54.1.1 = {
	monarch = {
 		name = "Kikena"
		dynasty = "Shuhroth"
		birth_date = 19.1.1
		adm = 5
		dip = 3
		mil = 3
    }
}

123.1.1 = {
	monarch = {
 		name = "Hirronul"
		dynasty = "Nhaadroth"
		birth_date = 87.1.1
		adm = 1
		dip = 4
		mil = 6
		female = yes
    }
}

216.1.1 = {
	monarch = {
 		name = "Taukhisir"
		dynasty = "Tutedoth"
		birth_date = 172.1.1
		adm = 6
		dip = 3
		mil = 2
    }
}

260.1.1 = {
	monarch = {
 		name = "Ydes"
		dynasty = "Myhrok"
		birth_date = 207.1.1
		adm = 4
		dip = 2
		mil = 6
    }
}

352.1.1 = {
	monarch = {
 		name = "Kikena"
		dynasty = "Nhaadroth"
		birth_date = 314.1.1
		adm = 2
		dip = 2
		mil = 2
    }
}

418.1.1 = {
	monarch = {
 		name = "Vikhesath"
		dynasty = "Nanhanir"
		birth_date = 400.1.1
		adm = 4
		dip = 3
		mil = 4
    }
}

473.1.1 = {
	monarch = {
 		name = "Hozi"
		dynasty = "Tynidor"
		birth_date = 433.1.1
		adm = 3
		dip = 2
		mil = 1
		female = yes
    }
}

510.1.1 = {
	monarch = {
 		name = "Myrledoth"
		dynasty = "Retec"
		birth_date = 474.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

570.1.1 = {
	monarch = {
 		name = "Azeniv"
		dynasty = "Nhidik"
		birth_date = 521.1.1
		adm = 6
		dip = 0
		mil = 1
		female = yes
    }
}

627.1.1 = {
	monarch = {
 		name = "Kylesan"
		dynasty = "Vuderik"
		birth_date = 593.1.1
		adm = 4
		dip = 0
		mil = 5
    }
}

693.1.1 = {
	monarch = {
 		name = "Balec"
		dynasty = "Shesemoc"
		birth_date = 660.1.1
		adm = 2
		dip = 6
		mil = 1
    }
}

765.1.1 = {
	monarch = {
 		name = "Myrledoth"
		dynasty = "Ilrisor"
		birth_date = 715.1.1
		adm = 1
		dip = 5
		mil = 5
    }
}

825.1.1 = {
	monarch = {
 		name = "Shirec"
		dynasty = "Aadyvar"
		birth_date = 788.1.1
		adm = 6
		dip = 4
		mil = 2
    }
}

876.1.1 = {
	monarch = {
 		name = "Ratih"
		dynasty = "Tulrec"
		birth_date = 845.1.1
		adm = 1
		dip = 6
		mil = 3
		female = yes
    }
}

950.1.1 = {
	monarch = {
 		name = "Shusizin"
		dynasty = "Revnauvoc"
		birth_date = 919.1.1
		adm = 6
		dip = 5
		mil = 0
    }
}

1046.1.1 = {
	monarch = {
 		name = "Zudines"
		dynasty = "Ydes"
		birth_date = 1027.1.1
		adm = 4
		dip = 4
		mil = 3
    }
}

1130.1.1 = {
	monarch = {
 		name = "Nanhanir"
		dynasty = "Idrik"
		birth_date = 1109.1.1
		adm = 3
		dip = 3
		mil = 0
    }
}

1195.1.1 = {
	monarch = {
 		name = "Shesemoc"
		dynasty = "Hunis"
		birth_date = 1174.1.1
		adm = 1
		dip = 2
		mil = 4
    }
}

1280.1.1 = {
	monarch = {
 		name = "Udaovar"
		dynasty = "Vuderik"
		birth_date = 1236.1.1
		adm = 6
		dip = 2
		mil = 0
    }
}

1356.1.1 = {
	monarch = {
 		name = "Dynevir"
		dynasty = "Timrymic"
		birth_date = 1313.1.1
		adm = 4
		dip = 1
		mil = 4
    }
}

1397.1.1 = {
	monarch = {
 		name = "Senne"
		dynasty = "Balec"
		birth_date = 1378.1.1
		adm = 3
		dip = 0
		mil = 1
		female = yes
    }
}

1435.1.1 = {
	monarch = {
 		name = "Zoshav"
		dynasty = "Nanhanir"
		birth_date = 1409.1.1
		adm = 4
		dip = 1
		mil = 2
		female = yes
    }
}

1499.1.1 = {
	monarch = {
 		name = "Udaovar"
		dynasty = "Gaurmoth"
		birth_date = 1468.1.1
		adm = 3
		dip = 0
		mil = 6
    }
}

1574.1.1 = {
	monarch = {
 		name = "Urmith"
		dynasty = "Dynevir"
		birth_date = 1528.1.1
		adm = 1
		dip = 0
		mil = 2
    }
}

1626.1.1 = {
	monarch = {
 		name = "Retec"
		dynasty = "Vrehrar"
		birth_date = 1573.1.1
		adm = 6
		dip = 6
		mil = 6
    }
}

1677.1.1 = {
	monarch = {
 		name = "Nhemros"
		dynasty = "Aohlymar"
		birth_date = 1632.1.1
		adm = 4
		dip = 5
		mil = 3
    }
}

1734.1.1 = {
	monarch = {
 		name = "Herlir"
		dynasty = "Retec"
		birth_date = 1703.1.1
		adm = 3
		dip = 4
		mil = 6
    }
}

1774.1.1 = {
	monarch = {
 		name = "Urmith"
		dynasty = "Shirec"
		birth_date = 1728.1.1
		adm = 1
		dip = 3
		mil = 3
    }
}

1844.1.1 = {
	monarch = {
 		name = "Retec"
		dynasty = "Gaurmoth"
		birth_date = 1802.1.1
		adm = 6
		dip = 3
		mil = 0
    }
}

1908.1.1 = {
	monarch = {
 		name = "Nhemros"
		dynasty = "Myrledoth"
		birth_date = 1871.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1966.1.1 = {
	monarch = {
 		name = "Roshi"
		dynasty = "Zihrisir"
		birth_date = 1929.1.1
		adm = 6
		dip = 3
		mil = 5
		female = yes
    }
}

2024.1.1 = {
	monarch = {
 		name = "Ethih"
		dynasty = "Ydes"
		birth_date = 2004.1.1
		adm = 5
		dip = 2
		mil = 2
		female = yes
    }
}

2083.1.1 = {
	monarch = {
 		name = "Sishe"
		dynasty = "Taostith"
		birth_date = 2041.1.1
		adm = 3
		dip = 2
		mil = 5
		female = yes
    }
}

2126.1.1 = {
	monarch = {
 		name = "Aastezith"
		dynasty = "Ydes"
		birth_date = 2103.1.1
		adm = 1
		dip = 1
		mil = 2
    }
}

2213.1.1 = {
	monarch = {
 		name = "Rarles"
		dynasty = "Urdis"
		birth_date = 2183.1.1
		adm = 6
		dip = 0
		mil = 5
    }
}

2276.1.1 = {
	monarch = {
 		name = "Livnas"
		dynasty = "Kelradic"
		birth_date = 2237.1.1
		adm = 5
		dip = 6
		mil = 2
    }
}

2322.1.1 = {
	monarch = {
 		name = "Laudrin"
		dynasty = "Kekemic"
		birth_date = 2299.1.1
		adm = 3
		dip = 5
		mil = 6
    }
}

2386.1.1 = {
	monarch = {
 		name = "Likhas"
		dynasty = "Idrik"
		birth_date = 2338.1.1
		adm = 5
		dip = 0
		mil = 0
    }
}

2483.1.1 = {
	monarch = {
 		name = "Sathedil"
		dynasty = "Nhikan"
		birth_date = 2461.1.1
		adm = 3
		dip = 6
		mil = 4
		female = yes
    }
}

