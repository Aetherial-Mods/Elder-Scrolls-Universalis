government = tribal
government_rank = 1
mercantilism = 1
technology_group = exiled_tg
religion = chimer_pantheon
primary_culture = ashlander_chimer
capital = 3838

54.1.1 = {
	monarch = {
 		name = "Kammu"
		dynasty = "Salvu"
		birth_date = 31.1.1
		adm = 0
		dip = 1
		mil = 1
		female = yes
    }
}

124.1.1 = {
	monarch = {
 		name = "Sakin"
		dynasty = "Sammalamus"
		birth_date = 72.1.1
		adm = 5
		dip = 0
		mil = 4
    }
}

186.1.1 = {
	monarch = {
 		name = "Ibarnadad"
		dynasty = "Telvanni"
		birth_date = 149.1.1
		adm = 6
		dip = 4
		mil = 2
    }
}

224.1.1 = {
	monarch = {
 		name = "Zallay"
		dynasty = "Assinabi"
		birth_date = 197.1.1
		adm = 4
		dip = 3
		mil = 6
    }
}

270.1.1 = {
	monarch = {
 		name = "Beden"
		dynasty = "Ilabael"
		birth_date = 223.1.1
		adm = 2
		dip = 2
		mil = 3
    }
}

316.1.1 = {
	monarch = {
 		name = "Zebba"
		dynasty = "Ashishpalirdan"
		birth_date = 290.1.1
		adm = 1
		dip = 2
		mil = 6
    }
}

383.1.1 = {
	monarch = {
 		name = "Shargon"
		dynasty = "Ilabael"
		birth_date = 338.1.1
		adm = 6
		dip = 1
		mil = 3
    }
}

446.1.1 = {
	monarch = {
 		name = "Mal"
		dynasty = "Shamirbasour"
		birth_date = 394.1.1
		adm = 4
		dip = 0
		mil = 0
    }
}

503.1.1 = {
	monarch = {
 		name = "Balur"
		dynasty = "Serimilk"
		birth_date = 464.1.1
		adm = 2
		dip = 6
		mil = 3
    }
}

554.1.1 = {
	monarch = {
 		name = "Zabi"
		dynasty = "Yessur-Disadon"
		birth_date = 501.1.1
		adm = 1
		dip = 5
		mil = 0
		female = yes
    }
}

644.1.1 = {
	monarch = {
 		name = "Shanud"
		dynasty = "Asserrumusa"
		birth_date = 606.1.1
		adm = 3
		dip = 0
		mil = 1
    }
}

735.1.1 = {
	monarch = {
 		name = "Mi-Ilu"
		dynasty = "Esurnadarpal"
		birth_date = 705.1.1
		adm = 1
		dip = 6
		mil = 5
		female = yes
    }
}

790.1.1 = {
	monarch = {
 		name = "Shin"
		dynasty = "Assunbahanammu"
		birth_date = 758.1.1
		adm = 6
		dip = 5
		mil = 2
    }
}

859.1.1 = {
	monarch = {
 		name = "Massarapal"
		dynasty = "Esatliballit"
		birth_date = 818.1.1
		adm = 4
		dip = 4
		mil = 5
    }
}

929.1.1 = {
	monarch = {
 		name = "Dakin"
		dynasty = "Addinibi"
		birth_date = 903.1.1
		adm = 2
		dip = 4
		mil = 2
    }
}

978.1.1 = {
	monarch = {
 		name = "Zennammu"
		dynasty = "Redoran"
		birth_date = 956.1.1
		adm = 1
		dip = 3
		mil = 6
		female = yes
    }
}

1029.1.1 = {
	monarch = {
 		name = "Shallath-Piremus"
		dynasty = "Assarbeberib"
		birth_date = 1001.1.1
		adm = 6
		dip = 2
		mil = 2
		female = yes
    }
}

1112.1.1 = {
	monarch = {
 		name = "Massarapal"
		dynasty = "Ashishpalirdan"
		birth_date = 1059.1.1
		adm = 0
		dip = 6
		mil = 0
    }
}

1178.1.1 = {
	monarch = {
 		name = "Conoon"
		dynasty = "Assunudadnud"
		birth_date = 1152.1.1
		adm = 6
		dip = 2
		mil = 1
    }
}

1249.1.1 = {
	monarch = {
 		name = "Zelaku"
		dynasty = "Assullinbanud"
		birth_date = 1225.1.1
		adm = 3
		dip = 4
		mil = 1
    }
}

1292.1.1 = {
	monarch = {
 		name = "Dunsalipal"
		dynasty = "Shalarnetus"
		birth_date = 1268.1.1
		adm = 3
		dip = 1
		mil = 1
    }
}

1351.1.1 = {
	monarch = {
 		name = "Zennammu"
		dynasty = "Urshumusa"
		birth_date = 1322.1.1
		adm = 1
		dip = 0
		mil = 4
    }
}

1392.1.1 = {
	monarch = {
 		name = "Shinammu"
		dynasty = "Assutladainab"
		birth_date = 1339.1.1
		adm = 6
		dip = 6
		mil = 1
		female = yes
    }
}

1472.1.1 = {
	monarch = {
 		name = "Mibdinahaz"
		dynasty = "Ranarbatus"
		birth_date = 1423.1.1
		adm = 4
		dip = 5
		mil = 5
    }
}

1515.1.1 = {
	monarch = {
 		name = "Elumabi"
		dynasty = "Timmiriran"
		birth_date = 1462.1.1
		adm = 3
		dip = 5
		mil = 1
		female = yes
    }
}

1559.1.1 = {
	monarch = {
 		name = "Zennammu"
		dynasty = "Ranarbatus"
		birth_date = 1517.1.1
		adm = 1
		dip = 4
		mil = 5
    }
}

1604.1.1 = {
	monarch = {
 		name = "Shipal"
		dynasty = "Addunipu"
		birth_date = 1569.1.1
		adm = 3
		dip = 5
		mil = 0
    }
}

1646.1.1 = {
	monarch = {
 		name = "Mibdinahaz"
		dynasty = "Dagoth"
		birth_date = 1609.1.1
		adm = 1
		dip = 4
		mil = 3
    }
}

1729.1.1 = {
	monarch = {
 		name = "Shulki"
		dynasty = "Anurnudai"
		birth_date = 1684.1.1
		adm = 6
		dip = 4
		mil = 0
		female = yes
    }
}

1778.1.1 = {
	monarch = {
 		name = "Minnibi"
		dynasty = "Ulirbabi"
		birth_date = 1740.1.1
		adm = 4
		dip = 3
		mil = 4
    }
}

1832.1.1 = {
	monarch = {
 		name = "Elibael"
		dynasty = "Puntumisun"
		birth_date = 1793.1.1
		adm = 3
		dip = 2
		mil = 0
    }
}

1910.1.1 = {
	monarch = {
 		name = "Assamanu"
		dynasty = "Assonirishpal"
		birth_date = 1867.1.1
		adm = 3
		dip = 6
		mil = 5
		female = yes
    }
}

1965.1.1 = {
	monarch = {
 		name = "Anasour"
		dynasty = "Ashananapal"
		birth_date = 1939.1.1
		adm = 2
		dip = 5
		mil = 2
    }
}

2034.1.1 = {
	monarch = {
 		name = "Ulisamsi"
		dynasty = "Addarari"
		birth_date = 1997.1.1
		adm = 0
		dip = 4
		mil = 5
		female = yes
    }
}

2119.1.1 = {
	monarch = {
 		name = "Raishi"
		dynasty = "Ababael"
		birth_date = 2067.1.1
		adm = 5
		dip = 4
		mil = 2
		female = yes
    }
}

2178.1.1 = {
	monarch = {
 		name = "Ibasour"
		dynasty = "Assardarainat"
		birth_date = 2136.1.1
		adm = 3
		dip = 3
		mil = 6
    }
}

2217.1.1 = {
	monarch = {
 		name = "Anasour"
		dynasty = "Assarrimisun"
		birth_date = 2176.1.1
		adm = 2
		dip = 2
		mil = 2
    }
}

2296.1.1 = {
	monarch = {
 		name = "Yanit"
		dynasty = "Pudashara"
		birth_date = 2271.1.1
		adm = 4
		dip = 3
		mil = 4
    }
}

2360.1.1 = {
	monarch = {
 		name = "Raishi"
		dynasty = "Salkatanat"
		birth_date = 2336.1.1
		adm = 2
		dip = 3
		mil = 1
		female = yes
    }
}

2456.1.1 = {
	monarch = {
 		name = "Vabbar"
		dynasty = "Indoril"
		birth_date = 2438.1.1
		adm = 0
		dip = 2
		mil = 4
		female = yes
    }
}

2499.1.1 = {
	monarch = {
 		name = "Rawia"
		dynasty = "Assalatammis"
		birth_date = 2457.1.1
		adm = 5
		dip = 1
		mil = 1
		female = yes
    }
}

