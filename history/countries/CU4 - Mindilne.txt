government = tribal
government_rank = 1
mercantilism = 1
technology_group = daedric_tg
religion = molag_bal_cult
primary_culture = soul_shriven
capital = 3176

54.1.1 = {
	monarch = {
 		name = "Tirdor"
		dynasty = "Yelmendo"
		birth_date = 13.1.1
		adm = 5
		dip = 5
		mil = 4
    }
	add_ruler_personality = immortal_personality
}

124.1.1 = {
	monarch = {
 		name = "Yhin"
		dynasty = "Veshenda"
		birth_date = 96.1.1
		adm = 3
		dip = 4
		mil = 1
    }
	add_ruler_personality = immortal_personality
}

178.1.1 = {
	monarch = {
 		name = "Rovamu"
		dynasty = "Yelnefa"
		birth_date = 153.1.1
		adm = 5
		dip = 5
		mil = 3
		female = yes
    }
	add_ruler_personality = immortal_personality
}

224.1.1 = {
	monarch = {
 		name = "Gikamik"
		dynasty = "Isnana"
		birth_date = 193.1.1
		adm = 4
		dip = 5
		mil = 0
    }
	add_ruler_personality = immortal_personality
}

264.1.1 = {
	monarch = {
 		name = "Tykisok"
		dynasty = "Adrela"
		birth_date = 236.1.1
		adm = 6
		dip = 6
		mil = 2
    }
	add_ruler_personality = immortal_personality
}

304.1.1 = {
	monarch = {
 		name = "Gakaazin"
		dynasty = "Nathanno"
		birth_date = 269.1.1
		adm = 4
		dip = 5
		mil = 5
    }
	add_ruler_personality = immortal_personality
}

362.1.1 = {
	monarch = {
 		name = "Nhudren"
		dynasty = "Nelfaza"
		birth_date = 333.1.1
		adm = 2
		dip = 4
		mil = 2
    }
	add_ruler_personality = immortal_personality
}

450.1.1 = {
	monarch = {
 		name = "Lyrmerek"
		dynasty = "Malneloha"
		birth_date = 420.1.1
		adm = 0
		dip = 3
		mil = 6
    }
	add_ruler_personality = immortal_personality
}

499.1.1 = {
	monarch = {
 		name = "Masehur"
		dynasty = "Alromoli"
		birth_date = 449.1.1
		adm = 6
		dip = 3
		mil = 2
		female = yes
    }
	add_ruler_personality = immortal_personality
}

583.1.1 = {
	monarch = {
 		name = "Nimoner"
		dynasty = "Irlanna"
		birth_date = 540.1.1
		adm = 4
		dip = 2
		mil = 6
		female = yes
    }
	add_ruler_personality = immortal_personality
}

632.1.1 = {
	monarch = {
 		name = "Nhudren"
		dynasty = "Vasafa"
		birth_date = 601.1.1
		adm = 2
		dip = 1
		mil = 2
    }
	add_ruler_personality = immortal_personality
}

706.1.1 = {
	monarch = {
 		name = "Lyrmerek"
		dynasty = "Endale"
		birth_date = 688.1.1
		adm = 0
		dip = 0
		mil = 6
    }
	add_ruler_personality = immortal_personality
}

802.1.1 = {
	monarch = {
 		name = "Haukos"
		dynasty = "Vindele"
		birth_date = 783.1.1
		adm = 2
		dip = 2
		mil = 1
    }
	add_ruler_personality = immortal_personality
}

869.1.1 = {
	monarch = {
 		name = "Nimoner"
		dynasty = "Garnallo"
		birth_date = 825.1.1
		adm = 0
		dip = 1
		mil = 4
		female = yes
    }
	add_ruler_personality = immortal_personality
}

937.1.1 = {
	monarch = {
 		name = "Zunhe"
		dynasty = "Evrane"
		birth_date = 890.1.1
		adm = 6
		dip = 0
		mil = 1
    }
	add_ruler_personality = immortal_personality
}

1013.1.1 = {
	monarch = {
 		name = "Zahoti"
		dynasty = "Sethiholo"
		birth_date = 977.1.1
		adm = 4
		dip = 6
		mil = 5
		female = yes
    }
	add_ruler_personality = immortal_personality
}

1051.1.1 = {
	monarch = {
 		name = "Disseh"
		dynasty = "Mesehe"
		birth_date = 1032.1.1
		adm = 2
		dip = 5
		mil = 1
		female = yes
    }
	add_ruler_personality = immortal_personality
}

1130.1.1 = {
	monarch = {
 		name = "Naamaulec"
		dynasty = "Rirlini"
		birth_date = 1079.1.1
		adm = 0
		dip = 5
		mil = 5
    }
	add_ruler_personality = immortal_personality
}

1170.1.1 = {
	monarch = {
 		name = "Zunhe"
		dynasty = "Alromoli"
		birth_date = 1143.1.1
		adm = 6
		dip = 4
		mil = 2
    }
	add_ruler_personality = immortal_personality
}

1240.1.1 = {
	monarch = {
 		name = "Zyrdusor"
		dynasty = "Vothinda"
		birth_date = 1206.1.1
		adm = 4
		dip = 3
		mil = 5
    }
	add_ruler_personality = immortal_personality
}

1339.1.1 = {
	monarch = {
 		name = "Gumrok"
		dynasty = "Melfezita"
		birth_date = 1288.1.1
		adm = 6
		dip = 4
		mil = 0
    }
	add_ruler_personality = immortal_personality
}

1429.1.1 = {
	monarch = {
 		name = "Lashavun"
		dynasty = "Manlazo"
		birth_date = 1407.1.1
		adm = 4
		dip = 3
		mil = 3
		female = yes
    }
	add_ruler_personality = immortal_personality
}

1515.1.1 = {
	monarch = {
 		name = "Reshoha"
		dynasty = "Alrilli"
		birth_date = 1462.1.1
		adm = 2
		dip = 3
		mil = 0
		female = yes
    }
	add_ruler_personality = immortal_personality
}

1605.1.1 = {
	monarch = {
 		name = "Vaorylan"
		dynasty = "Alromoli"
		birth_date = 1585.1.1
		adm = 1
		dip = 2
		mil = 4
    }
	add_ruler_personality = immortal_personality
}

1664.1.1 = {
	monarch = {
 		name = "Tamedir"
		dynasty = "Avradotte"
		birth_date = 1633.1.1
		adm = 6
		dip = 1
		mil = 0
    }
	add_ruler_personality = immortal_personality
}

1758.1.1 = {
	monarch = {
 		name = "Vauleso"
		dynasty = "Gerlenno"
		birth_date = 1705.1.1
		adm = 4
		dip = 0
		mil = 4
    }
	add_ruler_personality = immortal_personality
}

1811.1.1 = {
	monarch = {
 		name = "Hiherev"
		dynasty = "Hanera"
		birth_date = 1790.1.1
		adm = 2
		dip = 6
		mil = 1
		female = yes
    }
	add_ruler_personality = immortal_personality
}

1848.1.1 = {
	monarch = {
 		name = "Talih"
		dynasty = "Malneloha"
		birth_date = 1808.1.1
		adm = 1
		dip = 6
		mil = 4
		female = yes
    }
	add_ruler_personality = immortal_personality
}

1941.1.1 = {
	monarch = {
 		name = "Tamedir"
		dynasty = "Linyiva"
		birth_date = 1896.1.1
		adm = 2
		dip = 0
		mil = 6
    }
	add_ruler_personality = immortal_personality
}

2027.1.1 = {
	monarch = {
 		name = "Vauleso"
		dynasty = "Risero"
		birth_date = 1977.1.1
		adm = 1
		dip = 6
		mil = 2
    }
	add_ruler_personality = immortal_personality
}

2087.1.1 = {
	monarch = {
 		name = "Nuvner"
		dynasty = "Lerlafo"
		birth_date = 2056.1.1
		adm = 6
		dip = 5
		mil = 6
    }
	add_ruler_personality = immortal_personality
}

2160.1.1 = {
	monarch = {
 		name = "Kynhesac"
		dynasty = "Ivosi"
		birth_date = 2136.1.1
		adm = 4
		dip = 5
		mil = 3
    }
	add_ruler_personality = immortal_personality
}

2218.1.1 = {
	monarch = {
 		name = "Vinhylak"
		dynasty = "Avrite"
		birth_date = 2198.1.1
		adm = 2
		dip = 4
		mil = 6
    }
	add_ruler_personality = immortal_personality
}

2291.1.1 = {
	monarch = {
 		name = "Tirleros"
		dynasty = "Mophahe"
		birth_date = 2263.1.1
		adm = 1
		dip = 3
		mil = 3
    }
	add_ruler_personality = immortal_personality
}

2362.1.1 = {
	monarch = {
 		name = "Nuvner"
		dynasty = "Irolle"
		birth_date = 2314.1.1
		adm = 6
		dip = 2
		mil = 0
    }
	add_ruler_personality = immortal_personality
}

2409.1.1 = {
	monarch = {
 		name = "Kynhesac"
		dynasty = "Enkala"
		birth_date = 2366.1.1
		adm = 4
		dip = 1
		mil = 3
    }
	add_ruler_personality = immortal_personality
}

2451.1.1 = {
	monarch = {
 		name = "Vinhylak"
		dynasty = "Evamese"
		birth_date = 2433.1.1
		adm = 6
		dip = 3
		mil = 5
    }
	add_ruler_personality = immortal_personality
}

2491.1.1 = {
	monarch = {
 		name = "Orratul"
		dynasty = "Aslelle"
		birth_date = 2443.1.1
		adm = 4
		dip = 2
		mil = 2
		female = yes
    }
	add_ruler_personality = immortal_personality
}

