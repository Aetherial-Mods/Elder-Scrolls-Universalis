government = tribal
government_rank = 1
mercantilism = 1
technology_group = daedric_tg
religion = mehrunes_dagon_cult
primary_culture = clanfear
capital = 3861

54.1.1 = {
	monarch = {
 		name = "Croadror"
		dynasty = "Jul-Gotouzi"
		birth_date = 9.1.1
		adm = 2
		dip = 3
		mil = 0
    }
}

151.1.1 = {
	monarch = {
 		name = "Dakdadas"
		dynasty = "Jul-Rhoru"
		birth_date = 127.1.1
		adm = 0
		dip = 3
		mil = 4
    }
}

212.1.1 = {
	monarch = {
 		name = "Cit"
		dynasty = "Jul-Onci"
		birth_date = 186.1.1
		adm = 5
		dip = 2
		mil = 1
    }
}

268.1.1 = {
	monarch = {
 		name = "Shorlu"
		dynasty = "Jul-Tiljki"
		birth_date = 229.1.1
		adm = 4
		dip = 1
		mil = 4
		female = yes
    }
}

316.1.1 = {
	monarch = {
 		name = "Croadror"
		dynasty = "Jul-Bromrix"
		birth_date = 282.1.1
		adm = 6
		dip = 2
		mil = 6
    }
}

397.1.1 = {
	monarch = {
 		name = "Dakdadas"
		dynasty = "Jul-Hata"
		birth_date = 355.1.1
		adm = 4
		dip = 1
		mil = 2
    }
}

448.1.1 = {
	monarch = {
 		name = "Zelgrorg"
		dynasty = "Jul-Saghuhrush"
		birth_date = 430.1.1
		adm = 2
		dip = 1
		mil = 6
    }
}

511.1.1 = {
	monarch = {
 		name = "Vol"
		dynasty = "Jul-Derdazeth"
		birth_date = 462.1.1
		adm = 0
		dip = 0
		mil = 3
		female = yes
    }
}

578.1.1 = {
	monarch = {
 		name = "Kestinia"
		dynasty = "Jul-Bih"
		birth_date = 547.1.1
		adm = 6
		dip = 6
		mil = 6
    }
}

623.1.1 = {
	monarch = {
 		name = "Crat"
		dynasty = "Jul-Wibleno"
		birth_date = 586.1.1
		adm = 4
		dip = 5
		mil = 3
    }
}

674.1.1 = {
	monarch = {
 		name = "Zelgrorg"
		dynasty = "Jul-Figli"
		birth_date = 649.1.1
		adm = 2
		dip = 4
		mil = 0
    }
}

751.1.1 = {
	monarch = {
 		name = "Vol"
		dynasty = "Jul-Vittazu"
		birth_date = 733.1.1
		adm = 0
		dip = 4
		mil = 3
		female = yes
    }
}

848.1.1 = {
	monarch = {
 		name = "Egli"
		dynasty = "Jul-Noh"
		birth_date = 821.1.1
		adm = 2
		dip = 5
		mil = 5
		female = yes
    }
}

914.1.1 = {
	monarch = {
 		name = "Thanocu"
		dynasty = "Jul-Pututh"
		birth_date = 870.1.1
		adm = 0
		dip = 4
		mil = 1
		female = yes
    }
}

1000.1.1 = {
	monarch = {
 		name = "Luul"
		dynasty = "Jul-Potaybeth"
		birth_date = 947.1.1
		adm = 6
		dip = 3
		mil = 5
    }
}

1096.1.1 = {
	monarch = {
 		name = "Man"
		dynasty = "Jul-Zitton"
		birth_date = 1075.1.1
		adm = 4
		dip = 3
		mil = 2
    }
}

1143.1.1 = {
	monarch = {
 		name = "Loltrod"
		dynasty = "Jul-Fehgon"
		birth_date = 1110.1.1
		adm = 2
		dip = 2
		mil = 5
    }
}

1218.1.1 = {
	monarch = {
 		name = "Nuneva"
		dynasty = "Jul-Cosron"
		birth_date = 1178.1.1
		adm = 0
		dip = 1
		mil = 2
		female = yes
    }
}

1310.1.1 = {
	monarch = {
 		name = "Hreltrod"
		dynasty = "Jul-Bromrix"
		birth_date = 1291.1.1
		adm = 6
		dip = 0
		mil = 6
    }
}

1395.1.1 = {
	monarch = {
 		name = "Dezvosh"
		dynasty = "Jul-Sighurday"
		birth_date = 1351.1.1
		adm = 6
		dip = 4
		mil = 4
    }
}

1492.1.1 = {
	monarch = {
 		name = "Bohgah"
		dynasty = "Jul-Gitux"
		birth_date = 1471.1.1
		adm = 5
		dip = 3
		mil = 0
		female = yes
    }
}

1546.1.1 = {
	monarch = {
 		name = "Lostras"
		dynasty = "Jul-Gogrol"
		birth_date = 1510.1.1
		adm = 3
		dip = 3
		mil = 4
    }
}

1633.1.1 = {
	monarch = {
 		name = "Him"
		dynasty = "Jul-Engo"
		birth_date = 1613.1.1
		adm = 4
		dip = 0
		mil = 2
    }
}

1676.1.1 = {
	monarch = {
 		name = "Dazungat"
		dynasty = "Jul-Rerlith"
		birth_date = 1626.1.1
		adm = 2
		dip = 6
		mil = 5
    }
}

1715.1.1 = {
	monarch = {
 		name = "Taglagu"
		dynasty = "Jul-Uzemluox"
		birth_date = 1681.1.1
		adm = 0
		dip = 5
		mil = 2
		female = yes
    }
}

1803.1.1 = {
	monarch = {
 		name = "Neraibors"
		dynasty = "Jul-Hatru"
		birth_date = 1760.1.1
		adm = 5
		dip = 4
		mil = 6
    }
}

1876.1.1 = {
	monarch = {
 		name = "Hraizzos"
		dynasty = "Jul-Cabru"
		birth_date = 1825.1.1
		adm = 0
		dip = 6
		mil = 0
    }
}

1934.1.1 = {
	monarch = {
 		name = "Dazungat"
		dynasty = "Jul-Cabru"
		birth_date = 1894.1.1
		adm = 6
		dip = 5
		mil = 4
    }
}

1998.1.1 = {
	monarch = {
 		name = "Thanocu"
		dynasty = "Jul-Pirul"
		birth_date = 1954.1.1
		adm = 4
		dip = 4
		mil = 1
		female = yes
    }
}

2064.1.1 = {
	monarch = {
 		name = "Bozraik"
		dynasty = "Jul-Samledol"
		birth_date = 2033.1.1
		adm = 2
		dip = 3
		mil = 4
    }
}

2122.1.1 = {
	monarch = {
 		name = "Zall"
		dynasty = "Jul-Oghe"
		birth_date = 2088.1.1
		adm = 0
		dip = 2
		mil = 1
    }
}

2219.1.1 = {
	monarch = {
 		name = "Dregdadhum"
		dynasty = "Jul-Zitton"
		birth_date = 2200.1.1
		adm = 6
		dip = 2
		mil = 5
    }
}

2300.1.1 = {
	monarch = {
 		name = "Kattash"
		dynasty = "Jul-Rax"
		birth_date = 2249.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

2344.1.1 = {
	monarch = {
 		name = "Bozraik"
		dynasty = "Jul-Hatru"
		birth_date = 2306.1.1
		adm = 2
		dip = 0
		mil = 5
    }
}

2387.1.1 = {
	monarch = {
 		name = "Zall"
		dynasty = "Jul-Zon"
		birth_date = 2337.1.1
		adm = 4
		dip = 1
		mil = 6
    }
}

2458.1.1 = {
	monarch = {
 		name = "Dregdadhum"
		dynasty = "Jul-Nah"
		birth_date = 2411.1.1
		adm = 2
		dip = 1
		mil = 3
    }
}

