government = republic
government_rank = 1
mercantilism = 1
technology_group = cyrodiil_tg
religion = azura_cult
primary_culture = nedic
capital = 5890

54.1.1 = {
	monarch = {
 		name = "Gukhedas"
		dynasty = "Vredraudin"
		birth_date = 15.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

104.1.1 = {
	monarch = {
 		name = "Vallal"
		dynasty = "Rukec"
		birth_date = 70.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

142.1.1 = {
	monarch = {
 		name = "Zathi"
		dynasty = "Vredraudin"
		birth_date = 103.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

217.1.1 = {
	monarch = {
 		name = "Nirdoc"
		dynasty = "Vinhylak"
		birth_date = 185.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

286.1.1 = {
	monarch = {
 		name = "Gukhedas"
		dynasty = "Nhihreseth"
		birth_date = 263.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

374.1.1 = {
	monarch = {
 		name = "Haonuvic"
		dynasty = "Amedoc"
		birth_date = 323.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

440.1.1 = {
	monarch = {
 		name = "Aalezos"
		dynasty = "Ytas"
		birth_date = 405.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

533.1.1 = {
	monarch = {
 		name = "Ralreloc"
		dynasty = "Virdanath"
		birth_date = 499.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

576.1.1 = {
	monarch = {
 		name = "Dulros"
		dynasty = "Dulros"
		birth_date = 531.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

657.1.1 = {
	monarch = {
 		name = "Dyrmec"
		dynasty = "Hetok"
		birth_date = 627.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

733.1.1 = {
	monarch = {
 		name = "Sarri"
		dynasty = "Dyrmec"
		birth_date = 692.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

811.1.1 = {
	monarch = {
 		name = "Lotemu"
		dynasty = "Nhihreseth"
		birth_date = 760.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

905.1.1 = {
	monarch = {
 		name = "Tudaarin"
		dynasty = "Nhekhedin"
		birth_date = 861.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

996.1.1 = {
	monarch = {
 		name = "Dyrmec"
		dynasty = "Herlir"
		birth_date = 954.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1058.1.1 = {
	monarch = {
 		name = "Virdanath"
		dynasty = "Lysteth"
		birth_date = 1023.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1124.1.1 = {
	monarch = {
 		name = "Vurmesoth"
		dynasty = "Ytas"
		birth_date = 1099.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1195.1.1 = {
	monarch = {
 		name = "Lirimiv"
		dynasty = "Vauleso"
		birth_date = 1172.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1274.1.1 = {
	monarch = {
 		name = "Dastith"
		dynasty = "Haukos"
		birth_date = 1229.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1350.1.1 = {
	monarch = {
 		name = "Kaahlec"
		dynasty = "Vrustimek"
		birth_date = 1304.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1437.1.1 = {
	monarch = {
 		name = "Vurmesoth"
		dynasty = "Nharmok"
		birth_date = 1411.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1524.1.1 = {
	monarch = {
 		name = "Bardek"
		dynasty = "Naanuzar"
		birth_date = 1488.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1583.1.1 = {
	monarch = {
 		name = "Dastith"
		dynasty = "Virdanath"
		birth_date = 1552.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1627.1.1 = {
	monarch = {
 		name = "Kaahlec"
		dynasty = "Dastith"
		birth_date = 1578.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1706.1.1 = {
	monarch = {
 		name = "Lerrimiv"
		dynasty = "Vristidok"
		birth_date = 1657.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1771.1.1 = {
	monarch = {
 		name = "Vredraonir"
		dynasty = "Aroc"
		birth_date = 1740.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1856.1.1 = {
	monarch = {
 		name = "Nhystareth"
		dynasty = "Rukec"
		birth_date = 1822.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1935.1.1 = {
	monarch = {
 		name = "Myhros"
		dynasty = "Dulros"
		birth_date = 1907.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1984.1.1 = {
	monarch = {
 		name = "Vrakir"
		dynasty = "Nestith"
		birth_date = 1944.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2056.1.1 = {
	monarch = {
 		name = "Lysteth"
		dynasty = "Haukos"
		birth_date = 2007.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2105.1.1 = {
	monarch = {
 		name = "Vena"
		dynasty = "Nirdoc"
		birth_date = 2062.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

2186.1.1 = {
	monarch = {
 		name = "Omidiv"
		dynasty = "Tirdor"
		birth_date = 2159.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

2227.1.1 = {
	monarch = {
 		name = "Idith"
		dynasty = "Aroc"
		birth_date = 2198.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2309.1.1 = {
	monarch = {
 		name = "Vaulic"
		dynasty = "Herlir"
		birth_date = 2285.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2390.1.1 = {
	monarch = {
 		name = "Halaanis"
		dynasty = "Vauleso"
		birth_date = 2371.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2449.1.1 = {
	monarch = {
 		name = "Dennae"
		dynasty = "Dyrmec"
		birth_date = 2408.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

