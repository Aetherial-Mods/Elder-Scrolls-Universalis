government = republic
government_rank = 1
mercantilism = 1
technology_group = akavir_tg
religion = tsaesci_pantheon
primary_culture = tsaesci
capital = 709
# = 709

54.1.1 = {
	monarch = {
 		name = "Kuhshammo"
		dynasty = "Tzon'Uzshuhs"
		birth_date = 6.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

119.1.1 = {
	monarch = {
 		name = "Onezo"
		dynasty = "Sson'Yucre"
		birth_date = 93.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

185.1.1 = {
	monarch = {
 		name = "Ongruj"
		dynasty = "Tzon'Mreninna"
		birth_date = 164.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

262.1.1 = {
	monarch = {
 		name = "Hikuashuth"
		dynasty = "Tzon'Kressharu"
		birth_date = 228.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

305.1.1 = {
	monarch = {
 		name = "Zunexaush"
		dynasty = "		"
		birth_date = 269.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

362.1.1 = {
	monarch = {
 		name = "Onezo"
		dynasty = "Sson'Thaxaqash"
		birth_date = 330.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

439.1.1 = {
	monarch = {
 		name = "Xeikru"
		dynasty = "Zson'Ingrossi"
		birth_date = 408.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

478.1.1 = {
	monarch = {
 		name = "Egshath"
		dynasty = "Tzon'Ogzego"
		birth_date = 429.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

566.1.1 = {
	monarch = {
 		name = "Kizrokuhs"
		dynasty = "Sson'Chuxou"
		birth_date = 528.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

635.1.1 = {
	monarch = {
 		name = "Chegisse"
		dynasty = "Tzon'Chaenire"
		birth_date = 610.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

706.1.1 = {
	monarch = {
 		name = "Theigocuth"
		dynasty = "Zson'Zromunaesh"
		birth_date = 685.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

744.1.1 = {
	monarch = {
 		name = "Unu"
		dynasty = "Sson'Suakzuc"
		birth_date = 724.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

843.1.1 = {
	monarch = {
 		name = "Unouq"
		dynasty = "Zson'Zuzire"
		birth_date = 818.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

916.1.1 = {
	monarch = {
 		name = "Shacakshas"
		dynasty = "Zson'Zuzire"
		birth_date = 865.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

953.1.1 = {
	monarch = {
 		name = "Zrekozzej"
		dynasty = "Sson'Chuxou"
		birth_date = 903.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

995.1.1 = {
	monarch = {
 		name = "Chanauth"
		dynasty = "Sson'Eza"
		birth_date = 974.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1052.1.1 = {
	monarch = {
 		name = "Anaza"
		dynasty = "Tson'Chihxeze"
		birth_date = 1016.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1135.1.1 = {
	monarch = {
 		name = "Ghuamiash"
		dynasty = "Zson'Ngarunas"
		birth_date = 1089.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1234.1.1 = {
	monarch = {
 		name = "Rezogeix"
		dynasty = "Zson'Gumosskesh"
		birth_date = 1190.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1308.1.1 = {
	monarch = {
 		name = "Juahuh"
		dynasty = "Tson'Thugih"
		birth_date = 1265.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1377.1.1 = {
	monarch = {
 		name = "Anaza"
		dynasty = "Tson'Yogshuth"
		birth_date = 1356.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1447.1.1 = {
	monarch = {
 		name = "Heszosh"
		dynasty = "Tson'Chihxeze"
		birth_date = 1420.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1502.1.1 = {
	monarch = {
 		name = "Mreninna"
		dynasty = "Tzon'Guggin"
		birth_date = 1474.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1546.1.1 = {
	monarch = {
 		name = "Xonsac"
		dynasty = "Sson'Hakeqouth"
		birth_date = 1498.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1607.1.1 = {
	monarch = {
 		name = "Sasaqus"
		dynasty = "Tzon'Ivono"
		birth_date = 1582.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1685.1.1 = {
	monarch = {
 		name = "Suzeic"
		dynasty = "Tson'Ruhsouc"
		birth_date = 1632.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1722.1.1 = {
	monarch = {
 		name = "Ghumau"
		dynasty = "Sson'Rukza"
		birth_date = 1675.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1797.1.1 = {
	monarch = {
 		name = "Illorahz"
		dynasty = "Tson'Chakah"
		birth_date = 1746.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1890.1.1 = {
	monarch = {
 		name = "Azaaxi"
		dynasty = "Tson'Yogshuth"
		birth_date = 1860.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1946.1.1 = {
	monarch = {
 		name = "Jhivhergia"
		dynasty = "Tzon'Ghokizsha"
		birth_date = 1899.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

2017.1.1 = {
	monarch = {
 		name = "Ghumau"
		dynasty = "Sson'Hehsuaqus"
		birth_date = 1982.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

2083.1.1 = {
	monarch = {
 		name = "Zonuajeth"
		dynasty = "Tzon'Zrokkugzu"
		birth_date = 2056.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2162.1.1 = {
	monarch = {
 		name = "Ngarunas"
		dynasty = "Tson'Ruhsouc"
		birth_date = 2114.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

2219.1.1 = {
	monarch = {
 		name = "Niggiza"
		dynasty = "Sson'Xogyauz"
		birth_date = 2201.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

2282.1.1 = {
	monarch = {
 		name = "Shocaja"
		dynasty = "Sson'Thuacahsoq"
		birth_date = 2246.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2336.1.1 = {
	monarch = {
 		name = "Chakah"
		dynasty = "Tzon'Evounni"
		birth_date = 2310.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2431.1.1 = {
	monarch = {
 		name = "Hehsuaqus"
		dynasty = "Tson'Xucaxaz"
		birth_date = 2390.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2488.1.1 = {
	monarch = {
 		name = "Thuacahsoq"
		dynasty = "Tzon'Avuzes"
		birth_date = 2459.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

