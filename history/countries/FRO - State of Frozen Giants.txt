government = tribal
government_rank = 5
mercantilism = 1
technology_group = atmora_tg
religion = dragon_cult
primary_culture = frost_giant
capital = 367

54.1.1 = {
	monarch = {
 		name = "Skagskorfee"
		dynasty = "Vongke"
		birth_date = 11.1.1
		adm = 0
		dip = 4
		mil = 3
    }
}

131.1.1 = {
	monarch = {
 		name = "Ildwil"
		dynasty = "Mpatyampamy"
		birth_date = 107.1.1
		adm = 5
		dip = 3
		mil = 0
		female = yes
    }
}

172.1.1 = {
	monarch = {
 		name = "Rorildfyr"
		dynasty = "Geghal"
		birth_date = 139.1.1
		adm = 3
		dip = 2
		mil = 4
    }
}

257.1.1 = {
	monarch = {
 		name = "Thuskag"
		dynasty = "Nimy"
		birth_date = 221.1.1
		adm = 1
		dip = 2
		mil = 0
		female = yes
    }
}

343.1.1 = {
	monarch = {
 		name = "Ithvumbri"
		dynasty = "Vya"
		birth_date = 318.1.1
		adm = 3
		dip = 3
		mil = 2
    }
}

390.1.1 = {
	monarch = {
 		name = "Safdrilbri"
		dynasty = "Longkunguny"
		birth_date = 372.1.1
		adm = 2
		dip = 2
		mil = 6
    }
}

453.1.1 = {
	monarch = {
 		name = "Bilhan"
		dynasty = "Ngompemp"
		birth_date = 403.1.1
		adm = 0
		dip = 1
		mil = 2
		female = yes
    }
}

508.1.1 = {
	monarch = {
 		name = "Skoldkisdrey"
		dynasty = "Ghonya"
		birth_date = 475.1.1
		adm = 5
		dip = 1
		mil = 6
    }
}

553.1.1 = {
	monarch = {
 		name = "Bushan"
		dynasty = "Rolavav"
		birth_date = 525.1.1
		adm = 3
		dip = 0
		mil = 2
		female = yes
    }
}

624.1.1 = {
	monarch = {
 		name = "Drilsenkis"
		dynasty = "Nupos"
		birth_date = 596.1.1
		adm = 2
		dip = 6
		mil = 6
    }
}

669.1.1 = {
	monarch = {
 		name = "Tthanre"
		dynasty = "Syulo"
		birth_date = 622.1.1
		adm = 0
		dip = 5
		mil = 3
		female = yes
    }
}

761.1.1 = {
	monarch = {
 		name = "Skoldkisdrey"
		dynasty = "Myesamp"
		birth_date = 720.1.1
		adm = 5
		dip = 4
		mil = 6
    }
}

845.1.1 = {
	monarch = {
 		name = "Breykisvum"
		dynasty = "Ngompemp"
		birth_date = 793.1.1
		adm = 0
		dip = 6
		mil = 1
    }
}

924.1.1 = {
	monarch = {
 		name = "Wilme"
		dynasty = "Ra"
		birth_date = 896.1.1
		adm = 5
		dip = 5
		mil = 5
		female = yes
    }
}

982.1.1 = {
	monarch = {
 		name = "Meskagror"
		dynasty = "Ngangko"
		birth_date = 950.1.1
		adm = 3
		dip = 4
		mil = 1
    }
}

1070.1.1 = {
	monarch = {
 		name = "Bilskag"
		dynasty = "Viwe"
		birth_date = 1031.1.1
		adm = 2
		dip = 3
		mil = 5
		female = yes
    }
}

1157.1.1 = {
	monarch = {
 		name = "Safbreywil"
		dynasty = "Nevivus"
		birth_date = 1116.1.1
		adm = 0
		dip = 2
		mil = 2
    }
}

1232.1.1 = {
	monarch = {
 		name = "Rynskag"
		dynasty = "Wu"
		birth_date = 1210.1.1
		adm = 5
		dip = 2
		mil = 5
		female = yes
    }
}

1287.1.1 = {
	monarch = {
 		name = "Meskagror"
		dynasty = "Ghivyor"
		birth_date = 1269.1.1
		adm = 3
		dip = 1
		mil = 2
    }
}

1333.1.1 = {
	monarch = {
 		name = "Skoldsaf"
		dynasty = "Lega"
		birth_date = 1297.1.1
		adm = 2
		dip = 0
		mil = 5
		female = yes
    }
}

1370.1.1 = {
	monarch = {
 		name = "Safbreywil"
		dynasty = "Lepapevy"
		birth_date = 1340.1.1
		adm = 4
		dip = 1
		mil = 0
    }
}

1414.1.1 = {
	monarch = {
 		name = "Ofhofh"
		dynasty = "Tavyo"
		birth_date = 1365.1.1
		adm = 2
		dip = 1
		mil = 4
		female = yes
    }
}

1485.1.1 = {
	monarch = {
 		name = "Feevum"
		dynasty = "Wuvyom"
		birth_date = 1442.1.1
		adm = 0
		dip = 0
		mil = 0
		female = yes
    }
}

1546.1.1 = {
	monarch = {
 		name = "Eimskold"
		dynasty = "Nyopurun"
		birth_date = 1496.1.1
		adm = 5
		dip = 6
		mil = 4
		female = yes
    }
}

1602.1.1 = {
	monarch = {
 		name = "Bilwil"
		dynasty = "Gati"
		birth_date = 1574.1.1
		adm = 4
		dip = 5
		mil = 1
		female = yes
    }
}

1679.1.1 = {
	monarch = {
 		name = "Bilreskag"
		dynasty = "Gati"
		birth_date = 1637.1.1
		adm = 2
		dip = 4
		mil = 4
    }
}

1716.1.1 = {
	monarch = {
 		name = "Vumme"
		dynasty = "Sila"
		birth_date = 1694.1.1
		adm = 0
		dip = 4
		mil = 1
		female = yes
    }
}

1765.1.1 = {
	monarch = {
 		name = "Wilithbrey"
		dynasty = "Lemy"
		birth_date = 1746.1.1
		adm = 5
		dip = 3
		mil = 5
    }
}

1847.1.1 = {
	monarch = {
 		name = "Ildskagre"
		dynasty = "Ngusy"
		birth_date = 1825.1.1
		adm = 0
		dip = 4
		mil = 6
    }
}

1891.1.1 = {
	monarch = {
 		name = "Bilreskag"
		dynasty = "Nyavyo"
		birth_date = 1848.1.1
		adm = 5
		dip = 3
		mil = 3
    }
}

1980.1.1 = {
	monarch = {
 		name = "Winrynbil"
		dynasty = "Gun"
		birth_date = 1950.1.1
		adm = 4
		dip = 2
		mil = 6
    }
}

2024.1.1 = {
	monarch = {
 		name = "Riman"
		dynasty = "Syulo"
		birth_date = 1995.1.1
		adm = 2
		dip = 2
		mil = 3
		female = yes
    }
}

2064.1.1 = {
	monarch = {
 		name = "Manill"
		dynasty = "Sum"
		birth_date = 2044.1.1
		adm = 0
		dip = 1
		mil = 0
		female = yes
    }
}

2117.1.1 = {
	monarch = {
 		name = "Kisvumnu"
		dynasty = "Votyong"
		birth_date = 2082.1.1
		adm = 5
		dip = 0
		mil = 3
    }
}

2195.1.1 = {
	monarch = {
 		name = "Winrynbil"
		dynasty = "Ngkoli"
		birth_date = 2144.1.1
		adm = 4
		dip = 6
		mil = 0
    }
}

2260.1.1 = {
	monarch = {
 		name = "Yelwinryn"
		dynasty = "Sinyal"
		birth_date = 2242.1.1
		adm = 2
		dip = 5
		mil = 4
    }
}

2305.1.1 = {
	monarch = {
 		name = "Tthantthanhan"
		dynasty = "Yisyusav"
		birth_date = 2264.1.1
		adm = 4
		dip = 0
		mil = 5
    }
}

2381.1.1 = {
	monarch = {
 		name = "Kisvumnu"
		dynasty = "Tiny"
		birth_date = 2333.1.1
		adm = 2
		dip = 6
		mil = 2
    }
}

2457.1.1 = {
	monarch = {
 		name = "Thugun"
		dynasty = "Nyanongavy"
		birth_date = 2431.1.1
		adm = 0
		dip = 5
		mil = 5
		female = yes
    }
}

