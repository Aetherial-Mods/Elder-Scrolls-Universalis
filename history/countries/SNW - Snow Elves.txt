government = monarchy
government_rank = 5
mercantilism = 1
technology_group = elven_tg
religion = snow_elves_pantheon
primary_culture = snow_elven
capital = 3191

54.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Crynyrwen"
		monarch_name = "Crynyrwen I"
		dynasty = "Tir-Rinenyish"
		birth_date = 47.1.1
		death_date = 65.1.1
		claim = 100
		adm = 1
		dip = 2
		mil = 4
    }
}

65.1.1 = {
	monarch = {
 		name = "Prysyis"
		dynasty = "Tyr-Masgarwen"
		birth_date = 15.1.1
		adm = 4
		dip = 4
		mil = 6
		female = yes
    }
}

117.1.1 = {
	monarch = {
 		name = "Uresur"
		dynasty = "Tor-Ynhepireth"
		birth_date = 65.1.1
		adm = 2
		dip = 3
		mil = 3
    }
	queen = {
 		name = "Rinenyish"
		dynasty = "Tor-Ynhepireth"
		birth_date = 94.1.1
		adm = 6
		dip = 1
		mil = 2
		female = yes
    }
	heir = {
 		name = "Shanrawyn"
		monarch_name = "Shanrawyn I"
		dynasty = "Tor-Ynhepireth"
		birth_date = 105.1.1
		death_date = 188.1.1
		claim = 100
		adm = 3
		dip = 0
		mil = 1
    }
}

188.1.1 = {
	monarch = {
 		name = "Roneparwen"
		dynasty = "Ter-Uresur"
		birth_date = 168.1.1
		adm = 2
		dip = 3
		mil = 1
		female = yes
    }
}

241.1.1 = {
	monarch = {
 		name = "Prysyis"
		dynasty = "Tyr-Cendhora"
		birth_date = 188.1.1
		adm = 0
		dip = 3
		mil = 5
		female = yes
    }
}

318.1.1 = {
	monarch = {
 		name = "Rynyis"
		dynasty = "Ter-Orhorith"
		birth_date = 267.1.1
		adm = 5
		dip = 2
		mil = 1
		female = yes
    }
}

388.1.1 = {
	monarch = {
 		name = "Nirayane"
		dynasty = "Tir-Awith"
		birth_date = 368.1.1
		adm = 4
		dip = 1
		mil = 5
		female = yes
    }
	queen = {
 		name = "Prisbath"
		dynasty = "Tir-Awith"
		birth_date = 351.1.1
		adm = 1
		dip = 6
		mil = 4
    }
	heir = {
 		name = "Vranzoth"
		monarch_name = "Vranzoth I"
		dynasty = "Tir-Awith"
		birth_date = 387.1.1
		death_date = 428.1.1
		claim = 100
		adm = 4
		dip = 5
		mil = 3
    }
}

428.1.1 = {
	monarch = {
 		name = "Mirring"
		dynasty = "Tir-Heregriath"
		birth_date = 383.1.1
		adm = 0
		dip = 6
		mil = 5
    }
}

527.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Minevarys"
		monarch_name = "Minevarys I"
		dynasty = "Ter-Nirire"
		birth_date = 519.1.1
		death_date = 537.1.1
		claim = 100
		adm = 1
		dip = 3
		mil = 3
		female = yes
    }
}

537.1.1 = {
	monarch = {
 		name = "Nyrdhor"
		dynasty = "Tir-Awith"
		birth_date = 485.1.1
		adm = 4
		dip = 5
		mil = 5
    }
}

592.1.1 = {
	monarch = {
 		name = "Herekaris"
		dynasty = "Tor-Faefani"
		birth_date = 560.1.1
		adm = 6
		dip = 6
		mil = 0
    }
	queen = {
 		name = "Narasaroth"
		dynasty = "Tor-Faefani"
		birth_date = 554.1.1
		adm = 6
		dip = 2
		mil = 1
		female = yes
    }
	heir = {
 		name = "Niriwe"
		monarch_name = "Niriwe I"
		dynasty = "Tor-Faefani"
		birth_date = 585.1.1
		death_date = 657.1.1
		claim = 100
		adm = 3
		dip = 1
		mil = 0
    }
}

657.1.1 = {
	monarch = {
 		name = "Famhor"
		dynasty = "Tyr-Berellen"
		birth_date = 609.1.1
		adm = 2
		dip = 4
		mil = 0
    }
	queen = {
 		name = "Aithvhis"
		dynasty = "Tyr-Berellen"
		birth_date = 610.1.1
		adm = 6
		dip = 3
		mil = 6
		female = yes
    }
	heir = {
 		name = "Rynpireth"
		monarch_name = "Rynpireth I"
		dynasty = "Tyr-Berellen"
		birth_date = 656.1.1
		death_date = 755.1.1
		claim = 100
		adm = 3
		dip = 2
		mil = 5
		female = yes
    }
}

755.1.1 = {
	monarch = {
 		name = "Sindras"
		dynasty = "Tir-Prisbath"
		birth_date = 716.1.1
		adm = 5
		dip = 3
		mil = 1
    }
	queen = {
 		name = "Narasaroth"
		dynasty = "Tir-Prisbath"
		birth_date = 715.1.1
		adm = 2
		dip = 1
		mil = 6
		female = yes
    }
	heir = {
 		name = "Niriwe"
		monarch_name = "Niriwe I"
		dynasty = "Tir-Prisbath"
		birth_date = 744.1.1
		death_date = 843.1.1
		claim = 100
		adm = 6
		dip = 0
		mil = 5
    }
}

843.1.1 = {
	monarch = {
 		name = "Famhor"
		dynasty = "Tyr-Faezhina"
		birth_date = 822.1.1
		adm = 2
		dip = 1
		mil = 1
    }
}

920.1.1 = {
	monarch = {
 		name = "Unarrenoth"
		dynasty = "Tor-Inheselin"
		birth_date = 868.1.1
		adm = 0
		dip = 0
		mil = 4
    }
	queen = {
 		name = "Minlenor"
		dynasty = "Tor-Inheselin"
		birth_date = 868.1.1
		adm = 4
		dip = 6
		mil = 3
		female = yes
    }
}

1018.1.1 = {
	monarch = {
 		name = "Emerenoth"
		dynasty = "Tir-Agzoth"
		birth_date = 991.1.1
		adm = 2
		dip = 5
		mil = 0
		female = yes
    }
	queen = {
 		name = "Uresur"
		dynasty = "Tir-Agzoth"
		birth_date = 982.1.1
		adm = 6
		dip = 4
		mil = 6
    }
	heir = {
 		name = "Uredras"
		monarch_name = "Uredras I"
		dynasty = "Tir-Agzoth"
		birth_date = 1015.1.1
		death_date = 1090.1.1
		claim = 100
		adm = 3
		dip = 2
		mil = 5
    }
}

1090.1.1 = {
	monarch = {
 		name = "Fairen"
		dynasty = "Tir-Leneparis"
		birth_date = 1063.1.1
		adm = 2
		dip = 6
		mil = 5
		female = yes
    }
	queen = {
 		name = "Koryrwen"
		dynasty = "Tir-Leneparis"
		birth_date = 1046.1.1
		adm = 6
		dip = 4
		mil = 4
    }
}

1188.1.1 = {
	monarch = {
 		name = "Orhorith"
		dynasty = "Tor-Inheselin"
		birth_date = 1137.1.1
		adm = 3
		dip = 6
		mil = 4
    }
	queen = {
 		name = "Gangarwen"
		dynasty = "Tor-Inheselin"
		birth_date = 1163.1.1
		adm = 0
		dip = 5
		mil = 3
		female = yes
    }
	heir = {
 		name = "Wanhan"
		monarch_name = "Wanhan I"
		dynasty = "Tor-Inheselin"
		birth_date = 1175.1.1
		death_date = 1259.1.1
		claim = 100
		adm = 4
		dip = 3
		mil = 2
    }
}

1259.1.1 = {
	monarch = {
 		name = "Zongarwen"
		dynasty = "Tyr-Vranhan"
		birth_date = 1237.1.1
		adm = 4
		dip = 0
		mil = 2
    }
}

1323.1.1 = {
	monarch = {
 		name = "Koryrwen"
		dynasty = "Tyr-Lynzis"
		birth_date = 1300.1.1
		adm = 2
		dip = 6
		mil = 6
    }
}

1359.1.1 = {
	monarch = {
 		name = "Heleshan"
		dynasty = "Ter-Celesur"
		birth_date = 1331.1.1
		adm = 0
		dip = 5
		mil = 3
		female = yes
    }
	queen = {
 		name = "Ydhebor"
		dynasty = "Ter-Celesur"
		birth_date = 1321.1.1
		adm = 4
		dip = 4
		mil = 2
    }
	heir = {
 		name = "Cenlebor"
		monarch_name = "Cenlebor I"
		dynasty = "Ter-Celesur"
		birth_date = 1345.1.1
		death_date = 1421.1.1
		claim = 100
		adm = 1
		dip = 2
		mil = 1
    }
}

1421.1.1 = {
	monarch = {
 		name = "Nyrdhor"
		dynasty = "Ter-Torsur"
		birth_date = 1383.1.1
		adm = 4
		dip = 4
		mil = 3
    }
}

1460.1.1 = {
	monarch = {
 		name = "Herekaris"
		dynasty = "Ter-Orhorith"
		birth_date = 1438.1.1
		adm = 2
		dip = 3
		mil = 0
    }
}

1554.1.1 = {
	monarch = {
 		name = "Agmyn"
		dynasty = "Tyr-Unaryrwen"
		birth_date = 1534.1.1
		adm = 0
		dip = 2
		mil = 3
    }
}

1649.1.1 = {
	monarch = {
 		name = "Maszras"
		dynasty = "Tir-Waidhor"
		birth_date = 1600.1.1
		adm = 5
		dip = 1
		mil = 0
    }
	queen = {
 		name = "Aithvhis"
		dynasty = "Tir-Waidhor"
		birth_date = 1605.1.1
		adm = 2
		dip = 0
		mil = 6
		female = yes
    }
	heir = {
 		name = "Rynpireth"
		monarch_name = "Rynpireth I"
		dynasty = "Tir-Waidhor"
		birth_date = 1639.1.1
		death_date = 1744.1.1
		claim = 100
		adm = 6
		dip = 5
		mil = 5
		female = yes
    }
}

1744.1.1 = {
	monarch = {
 		name = "Yrerawyn"
		dynasty = "Tor-Sinbath"
		birth_date = 1706.1.1
		adm = 5
		dip = 2
		mil = 5
    }
}

1834.1.1 = {
	monarch = {
 		name = "Teredanyis"
		dynasty = "Ter-Harkir"
		birth_date = 1788.1.1
		adm = 4
		dip = 1
		mil = 2
    }
	queen = {
 		name = "Vorhan"
		dynasty = "Ter-Harkir"
		birth_date = 1787.1.1
		adm = 1
		dip = 6
		mil = 1
		female = yes
    }
	heir = {
 		name = "Lenetaroth"
		monarch_name = "Lenetaroth I"
		dynasty = "Ter-Harkir"
		birth_date = 1825.1.1
		death_date = 1904.1.1
		claim = 100
		adm = 4
		dip = 5
		mil = 0
		female = yes
    }
}

1904.1.1 = {
	monarch = {
 		name = "Unarrenoth"
		dynasty = "Tyr-Cendhora"
		birth_date = 1872.1.1
		adm = 0
		dip = 6
		mil = 2
    }
}

1992.1.1 = {
	monarch = {
 		name = "Rynyis"
		dynasty = "Tor-Minevarys"
		birth_date = 1955.1.1
		adm = 5
		dip = 5
		mil = 6
		female = yes
    }
	queen = {
 		name = "Nirire"
		dynasty = "Tor-Minevarys"
		birth_date = 1971.1.1
		adm = 2
		dip = 4
		mil = 5
    }
	heir = {
 		name = "Vorzoth"
		monarch_name = "Vorzoth I"
		dynasty = "Tor-Minevarys"
		birth_date = 1987.1.1
		death_date = 2029.1.1
		claim = 100
		adm = 6
		dip = 2
		mil = 4
		female = yes
    }
}

2029.1.1 = {
	monarch = {
 		name = "Shanbath"
		dynasty = "Ter-Shanrawyn"
		birth_date = 2000.1.1
		adm = 2
		dip = 4
		mil = 6
    }
}

2122.1.1 = {
	monarch = {
 		name = "Unarrenoth"
		dynasty = "Ter-Wirifaris"
		birth_date = 2078.1.1
		adm = 4
		dip = 5
		mil = 1
    }
	queen = {
 		name = "Minlenor"
		dynasty = "Ter-Wirifaris"
		birth_date = 2101.1.1
		adm = 4
		dip = 2
		mil = 2
		female = yes
    }
	heir = {
 		name = "Celesur"
		monarch_name = "Celesur I"
		dynasty = "Ter-Wirifaris"
		birth_date = 2109.1.1
		death_date = 2160.1.1
		claim = 100
		adm = 1
		dip = 0
		mil = 1
    }
}

2160.1.1 = {
	monarch = {
 		name = "Inheselin"
		dynasty = "Tir-Ansaroth"
		birth_date = 2128.1.1
		adm = 6
		dip = 6
		mil = 4
		female = yes
    }
	queen = {
 		name = "Parezhor"
		dynasty = "Tir-Ansaroth"
		birth_date = 2111.1.1
		adm = 3
		dip = 5
		mil = 3
    }
	heir = {
 		name = "Harkir"
		monarch_name = "Harkir I"
		dynasty = "Tir-Ansaroth"
		birth_date = 2150.1.1
		death_date = 2217.1.1
		claim = 100
		adm = 0
		dip = 3
		mil = 2
    }
}

2217.1.1 = {
	monarch = {
 		name = "Tereyaris"
		dynasty = "Tir-Koryrwen"
		birth_date = 2177.1.1
		adm = 6
		dip = 0
		mil = 2
    }
	queen = {
 		name = "Naratan"
		dynasty = "Tir-Koryrwen"
		birth_date = 2191.1.1
		adm = 3
		dip = 5
		mil = 1
		female = yes
    }
	heir = {
 		name = "Balprith"
		monarch_name = "Balprith I"
		dynasty = "Tir-Koryrwen"
		birth_date = 2205.1.1
		death_date = 2252.1.1
		claim = 100
		adm = 3
		dip = 2
		mil = 3
    }
}

2252.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Celefiath"
		monarch_name = "Celefiath I"
		dynasty = "Tor-Nirapath"
		birth_date = 2248.1.1
		death_date = 2266.1.1
		claim = 100
		adm = 5
		dip = 3
		mil = 4
    }
}

2266.1.1 = {
	monarch = {
 		name = "Niriwe"
		dynasty = "Tor-Zarkelor"
		birth_date = 2230.1.1
		adm = 4
		dip = 2
		mil = 1
    }
	queen = {
 		name = "Yrenzras"
		dynasty = "Tor-Zarkelor"
		birth_date = 2216.1.1
		adm = 0
		dip = 1
		mil = 0
		female = yes
    }
	heir = {
 		name = "Teretil"
		monarch_name = "Teretil I"
		dynasty = "Tor-Zarkelor"
		birth_date = 2255.1.1
		death_date = 2354.1.1
		claim = 100
		adm = 1
		dip = 4
		mil = 1
    }
}

2354.1.1 = {
	monarch = {
 		name = "Heleshan"
		dynasty = "Ter-Varriath"
		birth_date = 2301.1.1
		adm = 0
		dip = 1
		mil = 1
		female = yes
    }
	queen = {
 		name = "Vrankelor"
		dynasty = "Ter-Varriath"
		birth_date = 2315.1.1
		adm = 4
		dip = 6
		mil = 0
    }
	heir = {
 		name = "Crynyrwen"
		monarch_name = "Crynyrwen I"
		dynasty = "Ter-Varriath"
		birth_date = 2347.1.1
		death_date = 2399.1.1
		claim = 100
		adm = 1
		dip = 5
		mil = 6
    }
}

2399.1.1 = {
	monarch = {
 		name = "Lenenoth"
		dynasty = "Tor-Nirapath"
		birth_date = 2348.1.1
		adm = 4
		dip = 6
		mil = 1
    }
}

2448.1.1 = {
	monarch = {
 		name = "Leneferys"
		dynasty = "Tor-Fairen"
		birth_date = 2428.1.1
		adm = 2
		dip = 5
		mil = 5
		female = yes
    }
	queen = {
 		name = "Nirire"
		dynasty = "Tor-Fairen"
		birth_date = 2416.1.1
		adm = 6
		dip = 4
		mil = 4
    }
	heir = {
 		name = "Shanrawyn"
		monarch_name = "Shanrawyn I"
		dynasty = "Tor-Fairen"
		birth_date = 2448.1.1
		death_date = 2526.1.1
		claim = 100
		adm = 3
		dip = 2
		mil = 3
    }
}

