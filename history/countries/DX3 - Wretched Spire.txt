government = republic
government_rank = 2
mercantilism = 1
technology_group = daedric_tg
religion = students_of_magnus
primary_culture = dremoran
add_accepted_culture = house_dunmer
capital = 4189

54.1.1 = {
	monarch = {
 		name = "Shareth"
		dynasty = "Vyl-Luna"
		birth_date = 21.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

145.1.1 = {
	monarch = {
 		name = "Ohirvyn"
		dynasty = "Vyl-Stouicus"
		birth_date = 127.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

215.1.1 = {
	monarch = {
 		name = "Lanarorad"
		dynasty = "Vyl-Stouicus"
		birth_date = 189.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

307.1.1 = {
	monarch = {
 		name = "Grynera"
		dynasty = "Vyl-Pheafedea"
		birth_date = 269.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

392.1.1 = {
	monarch = {
 		name = "Ysernila"
		dynasty = "Vyl-Wogludea"
		birth_date = 371.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

472.1.1 = {
	monarch = {
 		name = "Maranilan"
		dynasty = "Vyl-Pheafedea"
		birth_date = 444.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

547.1.1 = {
	monarch = {
 		name = "Lanarorad"
		dynasty = "Vyl-Wouus"
		birth_date = 503.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

632.1.1 = {
	monarch = {
 		name = "Joranlead"
		dynasty = "Vyl-Gnuallyrath"
		birth_date = 584.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

704.1.1 = {
	monarch = {
 		name = "Harelvyra"
		dynasty = "Vyl-Kamori"
		birth_date = 666.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

797.1.1 = {
	monarch = {
 		name = "Iarylan"
		dynasty = "Vyl-Opeoth"
		birth_date = 778.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

865.1.1 = {
	monarch = {
 		name = "Xintrax"
		dynasty = "Vyl-Ciefeala"
		birth_date = 822.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

917.1.1 = {
	monarch = {
 		name = "Ryztar"
		dynasty = "Vyl-Calamity"
		birth_date = 872.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1008.1.1 = {
	monarch = {
 		name = "Orinith"
		dynasty = "Vyl-Truor'us"
		birth_date = 986.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1096.1.1 = {
	monarch = {
 		name = "Berinona"
		dynasty = "Vyl-Sturaoth"
		birth_date = 1071.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1169.1.1 = {
	monarch = {
 		name = "Vhesnila"
		dynasty = "Vyl-Ruyura"
		birth_date = 1135.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1219.1.1 = {
	monarch = {
 		name = "Xisoth"
		dynasty = "Vyl-Flycymina"
		birth_date = 1175.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1279.1.1 = {
	monarch = {
 		name = "Carileth"
		dynasty = "Vyl-Plaequnes"
		birth_date = 1239.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1368.1.1 = {
	monarch = {
 		name = "Glysvyra"
		dynasty = "Vyl-Plaequnes"
		birth_date = 1336.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1431.1.1 = {
	monarch = {
 		name = "Pamnex"
		dynasty = "Vyl-Luna"
		birth_date = 1387.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1494.1.1 = {
	monarch = {
 		name = "Wraxon"
		dynasty = "			Vyl-Ah'cath"
		birth_date = 1452.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1552.1.1 = {
	monarch = {
 		name = "Orless"
		dynasty = "Vyl-Stouicus"
		birth_date = 1510.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1643.1.1 = {
	monarch = {
 		name = "Glysvyra"
		dynasty = "			Vyl-Ah'cath"
		birth_date = 1612.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1702.1.1 = {
	monarch = {
 		name = "Qyrvynia"
		dynasty = "Vyl-Wouus"
		birth_date = 1663.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1773.1.1 = {
	monarch = {
 		name = "Jilriane"
		dynasty = "Vyl-Ruyura"
		birth_date = 1742.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1856.1.1 = {
	monarch = {
 		name = "Cahrysha"
		dynasty = "Vyl-Grace"
		birth_date = 1830.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1909.1.1 = {
	monarch = {
 		name = "Garanlan"
		dynasty = "Vyl-Truacius"
		birth_date = 1862.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1970.1.1 = {
	monarch = {
 		name = "Lanarixan"
		dynasty = "Vyl-Plystius"
		birth_date = 1921.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2046.1.1 = {
	monarch = {
 		name = "Charanya"
		dynasty = "Vyl-Ruadia"
		birth_date = 2005.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

2099.1.1 = {
	monarch = {
 		name = "Winess"
		dynasty = "Vyl-Cluepeal"
		birth_date = 2081.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

2151.1.1 = {
	monarch = {
 		name = "Garanlan"
		dynasty = "Vyl-Stouicus"
		birth_date = 2125.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2186.1.1 = {
	monarch = {
 		name = "Iphisysha"
		dynasty = "Vyl-Flycymina"
		birth_date = 2145.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

2225.1.1 = {
	monarch = {
 		name = "Meridnore"
		dynasty = "Vyl-Steeira"
		birth_date = 2197.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

2269.1.1 = {
	monarch = {
 		name = "Winess"
		dynasty = "Vyl-Wouus"
		birth_date = 2248.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

2344.1.1 = {
	monarch = {
 		name = "Grullax"
		dynasty = "Vyl-Kamori"
		birth_date = 2293.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2397.1.1 = {
	monarch = {
 		name = "Meridesha"
		dynasty = "Vyl-Gnuallyrath"
		birth_date = 2368.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

2462.1.1 = {
	monarch = {
 		name = "Xenyera"
		dynasty = "Vyl-Qlucuina"
		birth_date = 2437.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

