government = republic
government_rank = 1
mercantilism = 1
technology_group = akavir_tg
religion = po_tun_pantheon
primary_culture = po_tun
capital = 611

54.1.1 = {
	monarch = {
 		name = "Moyd"
		dynasty = "Dowyandu'su"
		birth_date = 32.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

115.1.1 = {
	monarch = {
 		name = "Zohlad"
		dynasty = "Qecnymos'la"
		birth_date = 86.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

203.1.1 = {
	monarch = {
 		name = "Gon"
		dynasty = "Rondez'su"
		birth_date = 178.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

288.1.1 = {
	monarch = {
 		name = "Chiantre"
		dynasty = "Pihya'ri"
		birth_date = 262.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

350.1.1 = {
	monarch = {
 		name = "Dohortus"
		dynasty = "Dalnog'su"
		birth_date = 313.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

445.1.1 = {
	monarch = {
 		name = "Tilnen"
		dynasty = "Derreho'ri"
		birth_date = 415.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

537.1.1 = {
	monarch = {
 		name = "Bim"
		dynasty = "Kumolen'ri"
		birth_date = 492.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

614.1.1 = {
	monarch = {
 		name = "Lu"
		dynasty = "Cin'la"
		birth_date = 596.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

709.1.1 = {
	monarch = {
 		name = "Sok"
		dynasty = "Dowyandu'su"
		birth_date = 682.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

800.1.1 = {
	monarch = {
 		name = "Chehlon"
		dynasty = "Lihya'su"
		birth_date = 767.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

882.1.1 = {
	monarch = {
 		name = "Vag"
		dynasty = "Fiyycnes'la"
		birth_date = 830.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

968.1.1 = {
	monarch = {
 		name = "Remoz"
		dynasty = "Qevor'wo"
		birth_date = 929.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1049.1.1 = {
	monarch = {
 		name = "Kruyd"
		dynasty = "Nuk'ri"
		birth_date = 1028.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1140.1.1 = {
	monarch = {
 		name = "Win"
		dynasty = "		"
		birth_date = 1093.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1237.1.1 = {
	monarch = {
 		name = "Dochecmea"
		dynasty = "Sok'ri"
		birth_date = 1194.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1324.1.1 = {
	monarch = {
 		name = "Remoz"
		dynasty = "Nessis'la"
		birth_date = 1304.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1383.1.1 = {
	monarch = {
 		name = "Dirskyts"
		dynasty = "Sirendrurr'wo"
		birth_date = 1361.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1465.1.1 = {
	monarch = {
 		name = "Chaz"
		dynasty = "Susdevu'la"
		birth_date = 1441.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1501.1.1 = {
	monarch = {
 		name = "Dochecmea"
		dynasty = "Tho'ri"
		birth_date = 1474.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1567.1.1 = {
	monarch = {
 		name = "Feg"
		dynasty = "Qokug'ri"
		birth_date = 1523.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1629.1.1 = {
	monarch = {
 		name = "Ther"
		dynasty = "Luttesso'wo"
		birth_date = 1583.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1694.1.1 = {
	monarch = {
 		name = "Rozzeg"
		dynasty = "Pihya'ri"
		birth_date = 1651.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1729.1.1 = {
	monarch = {
 		name = "Quv"
		dynasty = "Demusdoor'su"
		birth_date = 1688.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1824.1.1 = {
	monarch = {
 		name = "Feg"
		dynasty = "Cin'la"
		birth_date = 1804.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1879.1.1 = {
	monarch = {
 		name = "Fiandok"
		dynasty = "Sentyam'la"
		birth_date = 1843.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1960.1.1 = {
	monarch = {
 		name = "Rozzeg"
		dynasty = "Milin'wo"
		birth_date = 1916.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2054.1.1 = {
	monarch = {
 		name = "Quv"
		dynasty = "Voldar'ri"
		birth_date = 2009.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2152.1.1 = {
	monarch = {
 		name = "Chuhleyd"
		dynasty = "Kusdere'su"
		birth_date = 2115.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2195.1.1 = {
	monarch = {
 		name = "Lu"
		dynasty = "Demusdoor'su"
		birth_date = 2154.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

2276.1.1 = {
	monarch = {
 		name = "Pad"
		dynasty = "Pi'la"
		birth_date = 2242.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2326.1.1 = {
	monarch = {
 		name = "Shracmur"
		dynasty = "Chaz'ri"
		birth_date = 2300.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

2406.1.1 = {
	monarch = {
 		name = "Sok"
		dynasty = "Sehondek'ri"
		birth_date = 2378.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2472.1.1 = {
	monarch = {
 		name = "Luttesso"
		dynasty = "Luttesso'wo"
		birth_date = 2420.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

