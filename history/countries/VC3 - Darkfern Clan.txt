government = tribal
government_rank = 1
mercantilism = 1
technology_group = orsimer_tg
religion = orcish_pantheon
primary_culture = wood_orsimer
capital = 2375

54.1.1 = {
	monarch = {
 		name = "Morothmash"
		dynasty = "gro-Snalikh"
		birth_date = 9.1.1
		adm = 5
		dip = 1
		mil = 5
    }
}

124.1.1 = {
	monarch = {
 		name = "Droka"
		dynasty = "gro-Laurig"
		birth_date = 85.1.1
		adm = 3
		dip = 0
		mil = 2
		female = yes
    }
}

209.1.1 = {
	monarch = {
 		name = "Bashnag"
		dynasty = "gro-Goruz"
		birth_date = 187.1.1
		adm = 1
		dip = 0
		mil = 5
    }
}

275.1.1 = {
	monarch = {
 		name = "Snakha"
		dynasty = "gro-Zbulg"
		birth_date = 248.1.1
		adm = 6
		dip = 6
		mil = 2
    }
}

355.1.1 = {
	monarch = {
 		name = "Lashbura"
		dynasty = "gro-Malkus"
		birth_date = 331.1.1
		adm = 5
		dip = 5
		mil = 6
		female = yes
    }
}

421.1.1 = {
	monarch = {
 		name = "Golbag"
		dynasty = "gro-Uragor"
		birth_date = 399.1.1
		adm = 3
		dip = 4
		mil = 2
    }
}

479.1.1 = {
	monarch = {
 		name = "Bash"
		dynasty = "gro-Yakegg"
		birth_date = 457.1.1
		adm = 5
		dip = 5
		mil = 4
    }
}

559.1.1 = {
	monarch = {
 		name = "Snaglak"
		dynasty = "gro-Borgath"
		birth_date = 521.1.1
		adm = 3
		dip = 5
		mil = 0
    }
}

595.1.1 = {
	monarch = {
 		name = "Bogakh"
		dynasty = "gro-Muzgalg"
		birth_date = 577.1.1
		adm = 1
		dip = 4
		mil = 4
    }
}

678.1.1 = {
	monarch = {
 		name = "Snugar"
		dynasty = "gro-Grodoguz"
		birth_date = 652.1.1
		adm = 0
		dip = 3
		mil = 1
    }
}

716.1.1 = {
	monarch = {
 		name = "Logdotha"
		dynasty = "gro-Gurag"
		birth_date = 666.1.1
		adm = 5
		dip = 2
		mil = 4
		female = yes
    }
}

782.1.1 = {
	monarch = {
 		name = "Gorrath"
		dynasty = "gro-Nagoth"
		birth_date = 746.1.1
		adm = 3
		dip = 2
		mil = 1
    }
}

839.1.1 = {
	monarch = {
 		name = "Bisquelas"
		dynasty = "gro-Goramalg"
		birth_date = 812.1.1
		adm = 1
		dip = 1
		mil = 5
    }
}

898.1.1 = {
	monarch = {
 		name = "Shubesha"
		dynasty = "gro-Ulag"
		birth_date = 872.1.1
		adm = 0
		dip = 0
		mil = 1
		female = yes
    }
}

996.1.1 = {
	monarch = {
 		name = "Muglugd"
		dynasty = "gro-Klang"
		birth_date = 950.1.1
		adm = 1
		dip = 1
		mil = 3
    }
}

1039.1.1 = {
	monarch = {
 		name = "Dulroi"
		dynasty = "gro-Yamukuz"
		birth_date = 988.1.1
		adm = 0
		dip = 0
		mil = 0
		female = yes
    }
}

1128.1.1 = {
	monarch = {
 		name = "Murkub"
		dynasty = "gro-Bugunh"
		birth_date = 1107.1.1
		adm = 5
		dip = 0
		mil = 3
    }
}

1220.1.1 = {
	monarch = {
 		name = "Grat"
		dynasty = "gro-Molg"
		birth_date = 1167.1.1
		adm = 3
		dip = 6
		mil = 0
    }
}

1307.1.1 = {
	monarch = {
 		name = "Shelur"
		dynasty = "gro-Bumbub"
		birth_date = 1284.1.1
		adm = 1
		dip = 5
		mil = 3
		female = yes
    }
}

1350.1.1 = {
	monarch = {
 		name = "Targoth"
		dynasty = "gro-Othohoth"
		birth_date = 1301.1.1
		adm = 0
		dip = 4
		mil = 0
    }
}

1397.1.1 = {
	monarch = {
 		name = "Murgoz"
		dynasty = "gro-Thaz"
		birth_date = 1352.1.1
		adm = 5
		dip = 3
		mil = 4
    }
}

1467.1.1 = {
	monarch = {
 		name = "Grashbag"
		dynasty = "gro-Garikh"
		birth_date = 1420.1.1
		adm = 3
		dip = 3
		mil = 0
    }
}

1537.1.1 = {
	monarch = {
 		name = "Grubathag"
		dynasty = "gro-Balmeg"
		birth_date = 1515.1.1
		adm = 4
		dip = 0
		mil = 5
		female = yes
    }
}

1592.1.1 = {
	monarch = {
 		name = "Dushkul"
		dynasty = "gro-Bugharz"
		birth_date = 1547.1.1
		adm = 2
		dip = 6
		mil = 2
    }
}

1655.1.1 = {
	monarch = {
 		name = "Brag"
		dynasty = "gro-Bogham"
		birth_date = 1611.1.1
		adm = 2
		dip = 2
		mil = 2
    }
}

1714.1.1 = {
	monarch = {
 		name = "Thereg"
		dynasty = "gro-Olfim"
		birth_date = 1681.1.1
		adm = 0
		dip = 2
		mil = 6
    }
}

1777.1.1 = {
	monarch = {
 		name = "Mabgruhl"
		dynasty = "gro-Lugrub"
		birth_date = 1755.1.1
		adm = 5
		dip = 1
		mil = 3
		female = yes
    }
}

1817.1.1 = {
	monarch = {
 		name = "Grookh"
		dynasty = "gro-Brog"
		birth_date = 1799.1.1
		adm = 3
		dip = 0
		mil = 6
    }
}

1915.1.1 = {
	monarch = {
 		name = "Both"
		dynasty = "gro-Ogmash"
		birth_date = 1871.1.1
		adm = 2
		dip = 6
		mil = 3
    }
}

1981.1.1 = {
	monarch = {
 		name = "Sneehash"
		dynasty = "gro-Kazok"
		birth_date = 1963.1.1
		adm = 0
		dip = 5
		mil = 6
		female = yes
    }
}

2051.1.1 = {
	monarch = {
 		name = "Mabgrorga"
		dynasty = "gro-Mulur"
		birth_date = 2014.1.1
		adm = 2
		dip = 0
		mil = 1
		female = yes
    }
}

2143.1.1 = {
	monarch = {
 		name = "Grommok"
		dynasty = "gro-Ragbur"
		birth_date = 2116.1.1
		adm = 0
		dip = 6
		mil = 5
    }
}

2186.1.1 = {
	monarch = {
 		name = "Narkhozikh"
		dynasty = "gro-Sharnag"
		birth_date = 2143.1.1
		adm = 5
		dip = 5
		mil = 1
    }
}

2278.1.1 = {
	monarch = {
 		name = "Gruzdash"
		dynasty = "gro-Mekag"
		birth_date = 2235.1.1
		adm = 3
		dip = 4
		mil = 5
    }
}

2363.1.1 = {
	monarch = {
 		name = "Bugdul"
		dynasty = "gro-Grishduf"
		birth_date = 2337.1.1
		adm = 2
		dip = 3
		mil = 2
    }
}

2422.1.1 = {
	monarch = {
 		name = "Azhnura"
		dynasty = "gro-Slagwug"
		birth_date = 2373.1.1
		adm = 2
		dip = 1
		mil = 0
		female = yes
    }
}

2472.1.1 = {
	monarch = {
 		name = "Yarlak"
		dynasty = "gro-Dular"
		birth_date = 2426.1.1
		adm = 1
		dip = 0
		mil = 3
		female = yes
    }
}

