government = native
government_rank = 1
mercantilism = 1
technology_group = marshan_tg
religion = hist
primary_culture = naga
capital = 6569

54.1.1 = {
	monarch = {
 		name = "Keema-Ru"
		dynasty = "Neethcalees"
		birth_date = 26.1.1
		adm = 4
		dip = 1
		mil = 2
    }
}

148.1.1 = {
	monarch = {
 		name = "Dasha"
		dynasty = "Er-Tei"
		birth_date = 118.1.1
		adm = 2
		dip = 1
		mil = 5
		female = yes
    }
}

210.1.1 = {
	monarch = {
 		name = "Wud-Weska"
		dynasty = "Taierleesh"
		birth_date = 171.1.1
		adm = 1
		dip = 0
		mil = 2
		female = yes
    }
}

305.1.1 = {
	monarch = {
 		name = "Pejureel"
		dynasty = "Casmareen"
		birth_date = 258.1.1
		adm = 6
		dip = 6
		mil = 6
    }
}

367.1.1 = {
	monarch = {
 		name = "Keel-Medul"
		dynasty = "Menes"
		birth_date = 332.1.1
		adm = 4
		dip = 5
		mil = 2
    }
}

450.1.1 = {
	monarch = {
 		name = "Dar-Liurz"
		dynasty = "Taierlures"
		birth_date = 417.1.1
		adm = 6
		dip = 6
		mil = 4
		female = yes
    }
}

510.1.1 = {
	monarch = {
 		name = "Weedum-Eius"
		dynasty = "Neethsareeth"
		birth_date = 458.1.1
		adm = 4
		dip = 6
		mil = 1
    }
}

603.1.1 = {
	monarch = {
 		name = "Deet-Waska"
		dynasty = "Theerdaresh"
		birth_date = 554.1.1
		adm = 2
		dip = 5
		mil = 4
		female = yes
    }
}

691.1.1 = {
	monarch = {
 		name = "Xal-Nur"
		dynasty = "Taierlures"
		birth_date = 670.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

735.1.1 = {
	monarch = {
 		name = "Pojeel"
		dynasty = "Andreecalees"
		birth_date = 694.1.1
		adm = 6
		dip = 3
		mil = 5
    }
}

834.1.1 = {
	monarch = {
 		name = "Keesu"
		dynasty = "Taierlures"
		birth_date = 797.1.1
		adm = 4
		dip = 2
		mil = 1
    }
}

889.1.1 = {
	monarch = {
 		name = "Deegeeta"
		dynasty = "Ushureel"
		birth_date = 848.1.1
		adm = 2
		dip = 2
		mil = 5
    }
}

954.1.1 = {
	monarch = {
 		name = "Wunalz"
		dynasty = "Otumei"
		birth_date = 912.1.1
		adm = 1
		dip = 1
		mil = 1
    }
}

1043.1.1 = {
	monarch = {
 		name = "Pimsy"
		dynasty = "Yelei"
		birth_date = 1014.1.1
		adm = 3
		dip = 2
		mil = 3
    }
}

1093.1.1 = {
	monarch = {
 		name = "Lunsu"
		dynasty = "Androteus"
		birth_date = 1066.1.1
		adm = 1
		dip = 1
		mil = 0
		female = yes
    }
}

1129.1.1 = {
	monarch = {
 		name = "Iggistill"
		dynasty = "Mereemesh"
		birth_date = 1103.1.1
		adm = 6
		dip = 1
		mil = 3
    }
}

1215.1.1 = {
	monarch = {
 		name = "Chana-La"
		dynasty = "Mahekus"
		birth_date = 1197.1.1
		adm = 2
		dip = 2
		mil = 6
		female = yes
    }
}

1265.1.1 = {
	monarch = {
 		name = "Deet-Zeeus"
		dynasty = "Jeetum-Lei"
		birth_date = 1214.1.1
		adm = 3
		dip = 6
		mil = 4
    }
}

1312.1.1 = {
	monarch = {
 		name = "Xohaneel"
		dynasty = "Wanan-Dar"
		birth_date = 1282.1.1
		adm = 1
		dip = 5
		mil = 0
		female = yes
    }
}

1382.1.1 = {
	monarch = {
 		name = "Rarez"
		dynasty = "Caseus"
		birth_date = 1342.1.1
		adm = 6
		dip = 4
		mil = 4
    }
}

1471.1.1 = {
	monarch = {
 		name = "Kluleesh"
		dynasty = "Peteus"
		birth_date = 1421.1.1
		adm = 4
		dip = 4
		mil = 1
    }
}

1509.1.1 = {
	monarch = {
 		name = "Deerlus"
		dynasty = "Caetius"
		birth_date = 1456.1.1
		adm = 6
		dip = 5
		mil = 2
    }
}

1577.1.1 = {
	monarch = {
 		name = "Xinka"
		dynasty = "Xeirtius"
		birth_date = 1530.1.1
		adm = 4
		dip = 4
		mil = 6
		female = yes
    }
}

1651.1.1 = {
	monarch = {
 		name = "Drujal"
		dynasty = "Gallures"
		birth_date = 1624.1.1
		adm = 3
		dip = 3
		mil = 2
		female = yes
    }
}

1747.1.1 = {
	monarch = {
 		name = "Ah-Mudeska"
		dynasty = "Lafnaresh"
		birth_date = 1720.1.1
		adm = 1
		dip = 2
		mil = 6
		female = yes
    }
}

1846.1.1 = {
	monarch = {
 		name = "Resari"
		dynasty = "Effe-Shei"
		birth_date = 1796.1.1
		adm = 6
		dip = 2
		mil = 3
    }
}

1902.1.1 = {
	monarch = {
 		name = "Meema-Na"
		dynasty = "Wanan-Dar"
		birth_date = 1863.1.1
		adm = 4
		dip = 1
		mil = 6
		female = yes
    }
}

1959.1.1 = {
	monarch = {
 		name = "Dreet-Wulm"
		dynasty = "Mahekus"
		birth_date = 1932.1.1
		adm = 3
		dip = 0
		mil = 3
		female = yes
    }
}

2008.1.1 = {
	monarch = {
 		name = "Zatzu"
		dynasty = "Andreecalees"
		birth_date = 1972.1.1
		adm = 1
		dip = 6
		mil = 0
		female = yes
    }
}

2060.1.1 = {
	monarch = {
 		name = "Reekisk"
		dynasty = "Talen-Ei"
		birth_date = 2035.1.1
		adm = 3
		dip = 1
		mil = 1
    }
}

2096.1.1 = {
	monarch = {
 		name = "Loh-Jat"
		dynasty = "Taierlures"
		birth_date = 2077.1.1
		adm = 1
		dip = 0
		mil = 5
    }
}

2176.1.1 = {
	monarch = {
 		name = "Seed-Maht"
		dynasty = "Casdorus"
		birth_date = 2158.1.1
		adm = 6
		dip = 6
		mil = 1
    }
}

2249.1.1 = {
	monarch = {
 		name = "Methei"
		dynasty = "Andreecalees"
		birth_date = 2221.1.1
		adm = 5
		dip = 5
		mil = 5
		female = yes
    }
}

2297.1.1 = {
	monarch = {
 		name = "Drakaws"
		dynasty = "Gallures"
		birth_date = 2278.1.1
		adm = 3
		dip = 4
		mil = 2
    }
}

2336.1.1 = {
	monarch = {
 		name = "Ajim-Jaa"
		dynasty = "Augussareth"
		birth_date = 2288.1.1
		adm = 1
		dip = 4
		mil = 5
    }
}

2378.1.1 = {
	monarch = {
 		name = "Sar-Keer"
		dynasty = "Gallures"
		birth_date = 2343.1.1
		adm = 6
		dip = 3
		mil = 2
    }
}

2439.1.1 = {
	monarch = {
 		name = "Mach-Loh"
		dynasty = "Asumicus"
		birth_date = 2410.1.1
		adm = 4
		dip = 2
		mil = 6
    }
}

