government = tribal
government_rank = 5
mercantilism = 1
technology_group = atmora_tg
religion = dragon_cult
primary_culture = giant
capital = 3030

54.1.1 = {
	monarch = {
 		name = "Glenir"
		dynasty = "Kixgar"
		birth_date = 18.1.1
		adm = 1
		dip = 5
		mil = 6
    }
}

127.1.1 = {
	monarch = {
 		name = "Buxbrog"
		dynasty = "Zloxlos"
		birth_date = 80.1.1
		adm = 0
		dip = 5
		mil = 2
		female = yes
    }
}

165.1.1 = {
	monarch = {
 		name = "Daver"
		dynasty = "Xathos"
		birth_date = 133.1.1
		adm = 5
		dip = 4
		mil = 6
		female = yes
    }
}

258.1.1 = {
	monarch = {
 		name = "Mirvag"
		dynasty = "Crurlith"
		birth_date = 209.1.1
		adm = 0
		dip = 5
		mil = 0
		female = yes
    }
}

299.1.1 = {
	monarch = {
 		name = "Glenir"
		dynasty = "Glizus"
		birth_date = 269.1.1
		adm = 5
		dip = 4
		mil = 4
    }
}

336.1.1 = {
	monarch = {
 		name = "Freog"
		dynasty = "Blatheus"
		birth_date = 307.1.1
		adm = 3
		dip = 4
		mil = 1
    }
}

384.1.1 = {
	monarch = {
 		name = "Jomthos"
		dynasty = "Kretlor"
		birth_date = 339.1.1
		adm = 6
		dip = 5
		mil = 3
    }
}

475.1.1 = {
	monarch = {
 		name = "Arinus"
		dynasty = "Brerus"
		birth_date = 433.1.1
		adm = 4
		dip = 4
		mil = 0
		female = yes
    }
}

525.1.1 = {
	monarch = {
 		name = "Aarym"
		dynasty = "Negkrus"
		birth_date = 479.1.1
		adm = 6
		dip = 5
		mil = 1
    }
}

608.1.1 = {
	monarch = {
 		name = "Tibar"
		dynasty = "Zlibog"
		birth_date = 559.1.1
		adm = 4
		dip = 5
		mil = 5
		female = yes
    }
}

670.1.1 = {
	monarch = {
 		name = "Oftius"
		dynasty = "Vrilkaros"
		birth_date = 635.1.1
		adm = 3
		dip = 4
		mil = 1
		female = yes
    }
}

762.1.1 = {
	monarch = {
 		name = "Klunnus"
		dynasty = "Neslith"
		birth_date = 735.1.1
		adm = 1
		dip = 3
		mil = 5
		female = yes
    }
}

810.1.1 = {
	monarch = {
 		name = "Kuos"
		dynasty = "Glixthor"
		birth_date = 787.1.1
		adm = 6
		dip = 2
		mil = 2
		female = yes
    }
}

888.1.1 = {
	monarch = {
 		name = "Grucnus"
		dynasty = "Ikzar"
		birth_date = 847.1.1
		adm = 4
		dip = 1
		mil = 5
    }
}

962.1.1 = {
	monarch = {
 		name = "Oftius"
		dynasty = "Negkrus"
		birth_date = 912.1.1
		adm = 3
		dip = 1
		mil = 2
		female = yes
    }
}

1000.1.1 = {
	monarch = {
 		name = "Freog"
		dynasty = "Nofrus"
		birth_date = 981.1.1
		adm = 1
		dip = 0
		mil = 6
    }
}

1057.1.1 = {
	monarch = {
 		name = "Woos"
		dynasty = "Trigos"
		birth_date = 1030.1.1
		adm = 3
		dip = 1
		mil = 0
		female = yes
    }
}

1092.1.1 = {
	monarch = {
 		name = "Hurym"
		dynasty = "Caom"
		birth_date = 1072.1.1
		adm = 1
		dip = 0
		mil = 4
		female = yes
    }
}

1170.1.1 = {
	monarch = {
 		name = "Zlibog"
		dynasty = "Crofkaros"
		birth_date = 1124.1.1
		adm = 6
		dip = 6
		mil = 1
    }
}

1255.1.1 = {
	monarch = {
 		name = "Roznir"
		dynasty = "Bethos"
		birth_date = 1205.1.1
		adm = 5
		dip = 6
		mil = 4
    }
}

1299.1.1 = {
	monarch = {
 		name = "Blatheus"
		dynasty = "Neslith"
		birth_date = 1252.1.1
		adm = 3
		dip = 5
		mil = 1
    }
}

1370.1.1 = {
	monarch = {
 		name = "Glolmir"
		dynasty = "Krusal"
		birth_date = 1330.1.1
		adm = 1
		dip = 4
		mil = 5
    }
}

1459.1.1 = {
	monarch = {
 		name = "Zlibog"
		dynasty = "Ilog"
		birth_date = 1427.1.1
		adm = 6
		dip = 3
		mil = 1
    }
}

1539.1.1 = {
	monarch = {
 		name = "Roznir"
		dynasty = "Xekgi"
		birth_date = 1503.1.1
		adm = 5
		dip = 2
		mil = 5
    }
}

1591.1.1 = {
	monarch = {
 		name = "Blatheus"
		dynasty = "Sigom"
		birth_date = 1553.1.1
		adm = 6
		dip = 4
		mil = 6
    }
}

1634.1.1 = {
	monarch = {
 		name = "Xewlith"
		dynasty = "Ebver"
		birth_date = 1585.1.1
		adm = 5
		dip = 3
		mil = 3
		female = yes
    }
}

1727.1.1 = {
	monarch = {
 		name = "Cliffius"
		dynasty = "Warlog"
		birth_date = 1705.1.1
		adm = 3
		dip = 2
		mil = 0
    }
}

1806.1.1 = {
	monarch = {
 		name = "Gogvir"
		dynasty = "Farion"
		birth_date = 1759.1.1
		adm = 1
		dip = 1
		mil = 3
		female = yes
    }
}

1900.1.1 = {
	monarch = {
 		name = "Deto"
		dynasty = "Jomthos"
		birth_date = 1876.1.1
		adm = 6
		dip = 1
		mil = 0
    }
}

1958.1.1 = {
	monarch = {
 		name = "Trifur"
		dynasty = "Gabor"
		birth_date = 1911.1.1
		adm = 5
		dip = 0
		mil = 4
		female = yes
    }
}

2013.1.1 = {
	monarch = {
 		name = "Cliffius"
		dynasty = "Bethos"
		birth_date = 1983.1.1
		adm = 3
		dip = 6
		mil = 0
    }
}

2058.1.1 = {
	monarch = {
 		name = "Hugan"
		dynasty = "Airoch"
		birth_date = 2012.1.1
		adm = 1
		dip = 5
		mil = 4
    }
}

2139.1.1 = {
	monarch = {
 		name = "Nutnar"
		dynasty = "Roznir"
		birth_date = 2115.1.1
		adm = 3
		dip = 6
		mil = 5
    }
}

2215.1.1 = {
	monarch = {
 		name = "Nulbor"
		dynasty = "Nitag"
		birth_date = 2167.1.1
		adm = 1
		dip = 6
		mil = 2
    }
}

2289.1.1 = {
	monarch = {
 		name = "Ikzar"
		dynasty = "Noxdius"
		birth_date = 2265.1.1
		adm = 0
		dip = 5
		mil = 6
    }
}

2327.1.1 = {
	monarch = {
 		name = "Neslith"
		dynasty = "Zizlas"
		birth_date = 2280.1.1
		adm = 5
		dip = 4
		mil = 2
    }
}

2412.1.1 = {
	monarch = {
 		name = "Vozos"
		dynasty = "Gurbog"
		birth_date = 2385.1.1
		adm = 3
		dip = 3
		mil = 6
		female = yes
    }
}

2456.1.1 = {
	monarch = {
 		name = "Bumrym"
		dynasty = "Vlicbog"
		birth_date = 2434.1.1
		adm = 1
		dip = 2
		mil = 3
    }
}

2497.1.1 = {
	monarch = {
 		name = "urus"
		dynasty = "Cumas"
		birth_date = 2459.1.1
		adm = 0
		dip = 2
		mil = 6
		female = yes
    }
}

