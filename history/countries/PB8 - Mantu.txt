government = monarchy
government_rank = 1
mercantilism = 1
technology_group = pyandonea_tg
religion = serpant_king
primary_culture = maormer
capital = 1552

54.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Coloirros"
		monarch_name = "Coloirros I"
		dynasty = "Nu-Nadymodi"
		birth_date = 48.1.1
		death_date = 66.1.1
		claim = 100
		adm = 3
		dip = 0
		mil = 6
    }
}

66.1.1 = {
	monarch = {
 		name = "Qraalel"
		dynasty = "Ne-Dhyshislosh"
		birth_date = 46.1.1
		adm = 6
		dip = 1
		mil = 1
    }
	queen = {
 		name = "Ieeis"
		dynasty = "Ne-Dhyshislosh"
		birth_date = 17.1.1
		adm = 3
		dip = 0
		mil = 0
		female = yes
    }
}

160.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Anarwen"
		monarch_name = "Anarwen I"
		dynasty = "Na-Othy"
		birth_date = 147.1.1
		death_date = 165.1.1
		claim = 100
		adm = 4
		dip = 4
		mil = 5
		female = yes
    }
}

165.1.1 = {
	monarch = {
 		name = "Rodnurnum"
		dynasty = "Ne-Ellan"
		birth_date = 145.1.1
		adm = 6
		dip = 5
		mil = 0
    }
	queen = {
 		name = "Memanwen"
		dynasty = "Ne-Ellan"
		birth_date = 140.1.1
		adm = 3
		dip = 4
		mil = 6
		female = yes
    }
	heir = {
 		name = "Hetnorlinmo"
		monarch_name = "Hetnorlinmo I"
		dynasty = "Ne-Ellan"
		birth_date = 164.1.1
		death_date = 246.1.1
		claim = 100
		adm = 0
		dip = 2
		mil = 5
    }
}

246.1.1 = {
	monarch = {
 		name = "Yryrna"
		dynasty = "Ni-Qanralre"
		birth_date = 224.1.1
		adm = 0
		dip = 6
		mil = 6
		female = yes
    }
}

341.1.1 = {
	monarch = {
 		name = "Monwe"
		dynasty = "Na-Pirhah"
		birth_date = 304.1.1
		adm = 5
		dip = 5
		mil = 2
    }
}

399.1.1 = {
	monarch = {
 		name = "Iphilinmanius"
		dynasty = "Na-Vucydal"
		birth_date = 375.1.1
		adm = 3
		dip = 4
		mil = 6
    }
	queen = {
 		name = "Memanwen"
		dynasty = "Na-Vucydal"
		birth_date = 379.1.1
		adm = 0
		dip = 3
		mil = 5
		female = yes
    }
	heir = {
 		name = "Hetnorlinmo"
		monarch_name = "Hetnorlinmo I"
		dynasty = "Na-Vucydal"
		birth_date = 389.1.1
		death_date = 459.1.1
		claim = 100
		adm = 4
		dip = 1
		mil = 4
    }
}

459.1.1 = {
	monarch = {
 		name = "Sewe"
		dynasty = "Na-Pirhah"
		birth_date = 410.1.1
		adm = 0
		dip = 2
		mil = 6
    }
	queen = {
 		name = "Ohlayrwen"
		dynasty = "Na-Pirhah"
		birth_date = 430.1.1
		adm = 3
		dip = 1
		mil = 5
		female = yes
    }
	heir = {
 		name = "Armalda"
		monarch_name = "Armalda I"
		dynasty = "Na-Pirhah"
		birth_date = 453.1.1
		death_date = 500.1.1
		claim = 100
		adm = 0
		dip = 0
		mil = 4
		female = yes
    }
}

500.1.1 = {
	monarch = {
 		name = "Arpanculdir"
		dynasty = "Na-Seirrarmir"
		birth_date = 466.1.1
		adm = 3
		dip = 1
		mil = 6
		female = yes
    }
	queen = {
 		name = "Ungun"
		dynasty = "Na-Seirrarmir"
		birth_date = 447.1.1
		adm = 1
		dip = 4
		mil = 2
    }
	heir = {
 		name = "Laanhetmo"
		monarch_name = "Laanhetmo I"
		dynasty = "Na-Seirrarmir"
		birth_date = 485.1.1
		death_date = 555.1.1
		claim = 100
		adm = 5
		dip = 2
		mil = 1
    }
}

555.1.1 = {
	monarch = {
 		name = "Yealros"
		dynasty = "Nu-Nolnondoh"
		birth_date = 520.1.1
		adm = 1
		dip = 4
		mil = 3
    }
	queen = {
 		name = "Ansri"
		dynasty = "Nu-Nolnondoh"
		birth_date = 502.1.1
		adm = 5
		dip = 2
		mil = 2
		female = yes
    }
}

649.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Ohasri"
		monarch_name = "Ohasri I"
		dynasty = "Nu-Cinwos"
		birth_date = 641.1.1
		death_date = 659.1.1
		claim = 100
		adm = 5
		dip = 6
		mil = 0
		female = yes
    }
}

659.1.1 = {
	monarch = {
 		name = "Aranlel"
		dynasty = "Ni-Vylrul"
		birth_date = 617.1.1
		adm = 1
		dip = 0
		mil = 3
    }
}

749.1.1 = {
	monarch = {
 		name = "Rodevsamnum"
		dynasty = "Ni-Melral"
		birth_date = 707.1.1
		adm = 6
		dip = 0
		mil = 6
    }
}

817.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Jykne"
		monarch_name = "Jykne I"
		dynasty = "Ne-Espullon"
		birth_date = 810.1.1
		death_date = 828.1.1
		claim = 100
		adm = 0
		dip = 4
		mil = 4
		female = yes
    }
}

828.1.1 = {
	monarch = {
 		name = "Heyrral"
		dynasty = "Na-Othy"
		birth_date = 806.1.1
		adm = 6
		dip = 0
		mil = 4
    }
	queen = {
 		name = "Isuneis"
		dynasty = "Na-Othy"
		birth_date = 799.1.1
		adm = 0
		dip = 4
		mil = 5
		female = yes
    }
}

886.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Ilral"
		monarch_name = "Ilral I"
		dynasty = "Ni-Qephir"
		birth_date = 884.1.1
		death_date = 902.1.1
		claim = 100
		adm = 0
		dip = 1
		mil = 3
    }
}

902.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Arsamlel"
		monarch_name = "Arsamlel I"
		dynasty = "Na-Pyctollyl"
		birth_date = 898.1.1
		death_date = 916.1.1
		claim = 100
		adm = 6
		dip = 0
		mil = 0
    }
}

916.1.1 = {
	monarch = {
 		name = "Ninordor"
		dynasty = "Nu-Cronrorolom"
		birth_date = 882.1.1
		adm = 5
		dip = 3
		mil = 0
    }
}

981.1.1 = {
	monarch = {
 		name = "Chryiphidi"
		dynasty = "Nu-Croslun"
		birth_date = 940.1.1
		adm = 3
		dip = 2
		mil = 4
		female = yes
    }
}

1045.1.1 = {
	monarch = {
 		name = "Carnum"
		dynasty = "Nu-Lyssir"
		birth_date = 1009.1.1
		adm = 2
		dip = 2
		mil = 1
    }
	queen = {
 		name = "Niohpelna"
		dynasty = "Nu-Lyssir"
		birth_date = 994.1.1
		adm = 5
		dip = 0
		mil = 0
		female = yes
    }
	heir = {
 		name = "Ararcinlos"
		monarch_name = "Ararcinlos I"
		dynasty = "Nu-Lyssir"
		birth_date = 1032.1.1
		death_date = 1106.1.1
		claim = 100
		adm = 2
		dip = 6
		mil = 6
    }
}

1106.1.1 = {
	monarch = {
 		name = "Nenleroonoth"
		dynasty = "Nu-Vymnur"
		birth_date = 1060.1.1
		adm = 5
		dip = 0
		mil = 1
    }
	queen = {
 		name = "Jahnidil"
		dynasty = "Nu-Vymnur"
		birth_date = 1055.1.1
		adm = 2
		dip = 6
		mil = 0
		female = yes
    }
	heir = {
 		name = "Desparohasri"
		monarch_name = "Desparohasri I"
		dynasty = "Nu-Vymnur"
		birth_date = 1099.1.1
		death_date = 1204.1.1
		claim = 100
		adm = 6
		dip = 4
		mil = 6
		female = yes
    }
}

1204.1.1 = {
	monarch = {
 		name = "Carjykne"
		dynasty = "Na-Hinrylles"
		birth_date = 1163.1.1
		adm = 5
		dip = 1
		mil = 6
    }
}

1265.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Vimevismion"
		monarch_name = "Vimevismion I"
		dynasty = "Na-Jahluslolvuht"
		birth_date = 1250.1.1
		death_date = 1268.1.1
		claim = 100
		adm = 2
		dip = 3
		mil = 6
		female = yes
    }
}

1268.1.1 = {
	monarch = {
 		name = "Cecmeros"
		dynasty = "Na-Himnalwiash"
		birth_date = 1247.1.1
		adm = 2
		dip = 6
		mil = 6
    }
	queen = {
 		name = "Jahnidil"
		dynasty = "Na-Himnalwiash"
		birth_date = 1232.1.1
		adm = 6
		dip = 5
		mil = 5
		female = yes
    }
	heir = {
 		name = "Irinwe"
		monarch_name = "Irinwe I"
		dynasty = "Na-Himnalwiash"
		birth_date = 1268.1.1
		death_date = 1344.1.1
		claim = 100
		adm = 2
		dip = 3
		mil = 4
		female = yes
    }
}

1344.1.1 = {
	monarch = {
 		name = "Nordo"
		dynasty = "Na-Ycti"
		birth_date = 1305.1.1
		adm = 5
		dip = 4
		mil = 0
    }
}

1408.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Vimevismion"
		monarch_name = "Vimevismion I"
		dynasty = "Na-Cenri"
		birth_date = 1393.1.1
		death_date = 1411.1.1
		claim = 100
		adm = 6
		dip = 1
		mil = 5
		female = yes
    }
}

1411.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Iphiculdar"
		monarch_name = "Iphiculdar I"
		dynasty = "Nu-Pysnur"
		birth_date = 1403.1.1
		death_date = 1421.1.1
		claim = 100
		adm = 4
		dip = 1
		mil = 1
    }
}

1421.1.1 = {
	monarch = {
 		name = "Caruuda"
		dynasty = "Nu-Jinwydder"
		birth_date = 1372.1.1
		adm = 0
		dip = 2
		mil = 4
		female = yes
    }
}

1491.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Isanmetil"
		monarch_name = "Isanmetil I"
		dynasty = "Na-Chryhash"
		birth_date = 1483.1.1
		death_date = 1501.1.1
		claim = 100
		adm = 1
		dip = 6
		mil = 2
    }
}

1501.1.1 = {
	monarch = {
 		name = "Ansri"
		dynasty = "Ne-Yithinveth"
		birth_date = 1471.1.1
		adm = 0
		dip = 2
		mil = 2
		female = yes
    }
}

1595.1.1 = {
	monarch = {
 		name = "Normanyedil"
		dynasty = "Nu-Remrel"
		birth_date = 1562.1.1
		adm = 5
		dip = 2
		mil = 5
    }
}

1646.1.1 = {
	monarch = {
 		name = "Jykion"
		dynasty = "Nu-Ranvu"
		birth_date = 1597.1.1
		adm = 4
		dip = 1
		mil = 2
    }
}

1723.1.1 = {
	monarch = {
 		name = "Cecrasnil"
		dynasty = "Ne-Dhorruth"
		birth_date = 1688.1.1
		adm = 2
		dip = 0
		mil = 6
    }
	queen = {
 		name = "Lermonda"
		dynasty = "Ne-Dhorruth"
		birth_date = 1700.1.1
		adm = 6
		dip = 6
		mil = 5
		female = yes
    }
	heir = {
 		name = "Aryaamo"
		monarch_name = "Aryaamo I"
		dynasty = "Ne-Dhorruth"
		birth_date = 1723.1.1
		death_date = 1778.1.1
		claim = 100
		adm = 3
		dip = 4
		mil = 4
    }
}

1778.1.1 = {
	monarch = {
 		name = "Ircecsri"
		dynasty = "Ne-Dhutshil"
		birth_date = 1747.1.1
		adm = 5
		dip = 6
		mil = 6
		female = yes
    }
}

1828.1.1 = {
	monarch = {
 		name = "Vilaroonmion"
		dynasty = "Ni-Kelmernyrlil"
		birth_date = 1787.1.1
		adm = 3
		dip = 5
		mil = 3
		female = yes
    }
}

1878.1.1 = {
	monarch = {
 		name = "Umuor"
		dynasty = "Na-Notshonurly"
		birth_date = 1857.1.1
		adm = 5
		dip = 6
		mil = 4
		female = yes
    }
	queen = {
 		name = "Ararcinlos"
		dynasty = "Na-Notshonurly"
		birth_date = 1844.1.1
		adm = 6
		dip = 2
		mil = 5
    }
	heir = {
 		name = "Nenleroonoth"
		monarch_name = "Nenleroonoth I"
		dynasty = "Na-Notshonurly"
		birth_date = 1875.1.1
		death_date = 1968.1.1
		claim = 100
		adm = 2
		dip = 1
		mil = 4
    }
}

1968.1.1 = {
	monarch = {
 		name = "Yalorasse"
		dynasty = "Ne-Onlulwis"
		birth_date = 1934.1.1
		adm = 2
		dip = 4
		mil = 5
		female = yes
    }
	queen = {
 		name = "Manhetil"
		dynasty = "Ne-Onlulwis"
		birth_date = 1938.1.1
		adm = 6
		dip = 3
		mil = 4
    }
	heir = {
 		name = "Ohalorne"
		monarch_name = "Ohalorne I"
		dynasty = "Ne-Onlulwis"
		birth_date = 1964.1.1
		death_date = 2004.1.1
		claim = 100
		adm = 3
		dip = 1
		mil = 2
		female = yes
    }
}

2004.1.1 = {
	monarch = {
 		name = "Noryaaius"
		dynasty = "Na-Jytshysiaht"
		birth_date = 1954.1.1
		adm = 5
		dip = 3
		mil = 5
    }
}

2093.1.1 = {
	monarch = {
 		name = "Jyklos"
		dynasty = "		"
		birth_date = 2069.1.1
		adm = 4
		dip = 2
		mil = 1
    }
}

2147.1.1 = {
	monarch = {
 		name = "Chryinius"
		dynasty = "Na-Seirrarmir"
		birth_date = 2119.1.1
		adm = 2
		dip = 1
		mil = 5
    }
	queen = {
 		name = "Irinwe"
		dynasty = "Na-Seirrarmir"
		birth_date = 2124.1.1
		adm = 6
		dip = 0
		mil = 4
		female = yes
    }
	heir = {
 		name = "Neidir"
		monarch_name = "Neidir I"
		dynasty = "Na-Seirrarmir"
		birth_date = 2147.1.1
		death_date = 2239.1.1
		claim = 100
		adm = 3
		dip = 5
		mil = 3
		female = yes
    }
}

2239.1.1 = {
	monarch = {
 		name = "Norrasdar"
		dynasty = "Na-Jasmo"
		birth_date = 2212.1.1
		adm = 2
		dip = 2
		mil = 3
    }
	queen = {
 		name = "Vimevismion"
		dynasty = "Na-Jasmo"
		birth_date = 2194.1.1
		adm = 2
		dip = 5
		mil = 4
		female = yes
    }
}

2277.1.1 = {
	monarch = {
 		name = "Cichrymuwen"
		dynasty = "Ni-Ymnath"
		birth_date = 2242.1.1
		adm = 4
		dip = 6
		mil = 6
		female = yes
    }
	queen = {
 		name = "Unandil"
		dynasty = "Ni-Ymnath"
		birth_date = 2225.1.1
		adm = 4
		dip = 3
		mil = 0
    }
	heir = {
 		name = "Manmanse"
		monarch_name = "Manmanse I"
		dynasty = "Ni-Ymnath"
		birth_date = 2276.1.1
		death_date = 2325.1.1
		claim = 100
		adm = 1
		dip = 1
		mil = 6
		female = yes
    }
}

2325.1.1 = {
	monarch = {
 		name = "Yaalaadar"
		dynasty = "Ni-Nihmolen"
		birth_date = 2303.1.1
		adm = 1
		dip = 5
		mil = 6
    }
	queen = {
 		name = "Pelrasanse"
		dynasty = "Ni-Nihmolen"
		birth_date = 2275.1.1
		adm = 4
		dip = 3
		mil = 5
		female = yes
    }
	heir = {
 		name = "Vagun"
		monarch_name = "Vagun I"
		dynasty = "Ni-Nihmolen"
		birth_date = 2319.1.1
		death_date = 2419.1.1
		claim = 100
		adm = 1
		dip = 2
		mil = 4
    }
}

2419.1.1 = {
	monarch = {
 		name = "Acarsha"
		dynasty = "Na-Dhysmiry"
		birth_date = 2395.1.1
		adm = 4
		dip = 3
		mil = 0
		female = yes
    }
	queen = {
 		name = "Umhelel"
		dynasty = "Na-Dhysmiry"
		birth_date = 2386.1.1
		adm = 1
		dip = 2
		mil = 5
    }
}

2457.1.1 = {
	monarch = {
 		name = "Rirlel"
		dynasty = "Nu-Ilgiduurnu"
		birth_date = 2415.1.1
		adm = 6
		dip = 1
		mil = 2
    }
	queen = {
 		name = "Arslone"
		dynasty = "Nu-Ilgiduurnu"
		birth_date = 2430.1.1
		adm = 3
		dip = 0
		mil = 1
		female = yes
    }
}

