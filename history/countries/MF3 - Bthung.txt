government = republic
government_rank = 3
mercantilism = 1
technology_group = dwemer_tg
religion = reason_and_logic_cult
primary_culture = dwemer
capital = 3981

54.1.1 = {
	monarch = {
 		name = "Sthomin"
		dynasty = "Av'Jlarerhunch"
		birth_date = 12.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

140.1.1 = {
	monarch = {
 		name = "Rhothurzch"
		dynasty = "Az'Glibrina"
		birth_date = 100.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

228.1.1 = {
	monarch = {
 		name = "Ihlefuan"
		dynasty = "Af'Jhouvin"
		birth_date = 188.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

276.1.1 = {
	monarch = {
 		name = "Jhourlac"
		dynasty = "Av'Gzozchyn"
		birth_date = 225.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

356.1.1 = {
	monarch = {
 		name = "Krilnif"
		dynasty = "Af'Jragrenz"
		birth_date = 323.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

441.1.1 = {
	monarch = {
 		name = "Ychogrenz"
		dynasty = "Aq'Blulnmer"
		birth_date = 390.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

515.1.1 = {
	monarch = {
 		name = "Talchanf"
		dynasty = "Az'Ruerbira"
		birth_date = 497.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

591.1.1 = {
	monarch = {
 		name = "Gonwess"
		dynasty = "Af'Chruhnch"
		birth_date = 550.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

651.1.1 = {
	monarch = {
 		name = "Ylrefwinn"
		dynasty = "Af'Irhadac"
		birth_date = 628.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

718.1.1 = {
	monarch = {
 		name = "Szoglynsh"
		dynasty = "Af'Rhzomrond"
		birth_date = 691.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

763.1.1 = {
	monarch = {
 		name = "Achygrac"
		dynasty = "Af'Ychogarn"
		birth_date = 742.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

802.1.1 = {
	monarch = {
 		name = "Ksredlin"
		dynasty = "Av'Mzamrumhz"
		birth_date = 767.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

866.1.1 = {
	monarch = {
 		name = "Tnadrak"
		dynasty = "Av'Jholzarf"
		birth_date = 844.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

926.1.1 = {
	monarch = {
 		name = "Tzenruz"
		dynasty = "Az'Mherbira"
		birth_date = 900.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

987.1.1 = {
	monarch = {
 		name = "Jribwyr"
		dynasty = "Av'Mhunac"
		birth_date = 938.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1034.1.1 = {
	monarch = {
 		name = "Chzetvar"
		dynasty = "Az'Azsamchin"
		birth_date = 996.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1098.1.1 = {
	monarch = {
 		name = "Miban"
		dynasty = "Av'Ynzarlatz"
		birth_date = 1047.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1153.1.1 = {
	monarch = {
 		name = "Rafnyg"
		dynasty = "Af'Rhzomrond"
		birth_date = 1114.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1244.1.1 = {
	monarch = {
 		name = "Jribwyr"
		dynasty = "Af'Sthord"
		birth_date = 1193.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1283.1.1 = {
	monarch = {
 		name = "Chzetvar"
		dynasty = "Az'Shtrorlis"
		birth_date = 1252.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1367.1.1 = {
	monarch = {
 		name = "Miban"
		dynasty = "Aq'Snenard"
		birth_date = 1344.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1405.1.1 = {
	monarch = {
 		name = "Jlethurzch"
		dynasty = "Av'Djubchasz"
		birth_date = 1363.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1466.1.1 = {
	monarch = {
 		name = "Jhomrumhz"
		dynasty = "Aq'Badzach"
		birth_date = 1442.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1503.1.1 = {
	monarch = {
 		name = "Chragrenz"
		dynasty = "Av'Mhunac"
		birth_date = 1476.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1593.1.1 = {
	monarch = {
 		name = "Irhadac"
		dynasty = "Aq'Cheglas"
		birth_date = 1559.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1692.1.1 = {
	monarch = {
 		name = "Kridhis"
		dynasty = "Av'Brazzedit"
		birth_date = 1654.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1730.1.1 = {
	monarch = {
 		name = "Tadlin"
		dynasty = "Av'Choatchatz"
		birth_date = 1703.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1794.1.1 = {
	monarch = {
 		name = "Tugradac"
		dynasty = "Av'Asradzach"
		birth_date = 1743.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1858.1.1 = {
	monarch = {
 		name = "Alnodrunz"
		dynasty = "Av'Ihlefuan"
		birth_date = 1813.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1914.1.1 = {
	monarch = {
 		name = "Nebgar"
		dynasty = "Aq'Ioranrida"
		birth_date = 1886.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1971.1.1 = {
	monarch = {
 		name = "Ynzatarn"
		dynasty = "Aq'Mlirtes"
		birth_date = 1951.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2014.1.1 = {
	monarch = {
 		name = "Doudrys"
		dynasty = "Aq'Ararbira"
		birth_date = 1985.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

2052.1.1 = {
	monarch = {
 		name = "Banrynn"
		dynasty = "Av'Ychogrenz"
		birth_date = 2025.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

2141.1.1 = {
	monarch = {
 		name = "Yhnazzefk"
		dynasty = "Af'Jragrenz"
		birth_date = 2104.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2191.1.1 = {
	monarch = {
 		name = "Nromratz"
		dynasty = "Av'Rafnyg"
		birth_date = 2157.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

2284.1.1 = {
	monarch = {
 		name = "Dark"
		dynasty = "Av'Jlarerhunch"
		birth_date = 2250.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2323.1.1 = {
	monarch = {
 		name = "Ishevnorz"
		dynasty = "Aq'Blunrida"
		birth_date = 2293.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2386.1.1 = {
	monarch = {
 		name = "Mlirloar"
		dynasty = "Az'Ruerbira"
		birth_date = 2368.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

2439.1.1 = {
	monarch = {
 		name = "Gzozchyn"
		dynasty = "Av'Sthomin"
		birth_date = 2406.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

