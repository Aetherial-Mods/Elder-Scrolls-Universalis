government = monarchy
government_rank = 5
mercantilism = 1
technology_group = northern_tg
religion = altmeri_pantheon
primary_culture = breton
capital = 1394

54.1.1 = {
	monarch = {
 		name = "Emoryan"
		dynasty = "Althen"
		birth_date = 26.1.1
		adm = 3
		dip = 5
		mil = 0
    }
}

141.1.1 = {
	monarch = {
 		name = "Antoine"
		dynasty = "Geonette"
		birth_date = 118.1.1
		adm = 1
		dip = 4
		mil = 4
    }
	queen = {
 		name = "Inirisi"
		dynasty = "Geonette"
		birth_date = 117.1.1
		adm = 5
		dip = 3
		mil = 3
		female = yes
    }
}

201.1.1 = {
	monarch = {
 		name = "Fonten"
		dynasty = "Frey"
		birth_date = 160.1.1
		adm = 3
		dip = 2
		mil = 0
    }
}

299.1.1 = {
	monarch = {
 		name = "Baurion"
		dynasty = "Gimbert"
		birth_date = 265.1.1
		adm = 2
		dip = 1
		mil = 3
    }
}

361.1.1 = {
    capital = 1369
	religion = twelve_divines
}

363.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Androche"
		monarch_name = "Androche I"
		dynasty = "Longis"
		birth_date = 350.1.1
		death_date = 368.1.1
		claim = 100
		adm = 2
		dip = 5
		mil = 1
    }
}

368.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Reynald"
		monarch_name = "Reynald I"
		dynasty = "Lonacque"
		birth_date = 362.1.1
		death_date = 380.1.1
		claim = 100
		adm = 1
		dip = 5
		mil = 5
    }
}

380.1.1 = {
	monarch = {
 		name = "Florenot"
		dynasty = "Sidrey"
		birth_date = 345.1.1
		adm = 0
		dip = 1
		mil = 5
    }
	queen = {
 		name = "Edelinne"
		dynasty = "Sidrey"
		birth_date = 345.1.1
		adm = 0
		dip = 4
		mil = 6
		female = yes
    }
	heir = {
 		name = "Rosalind"
		monarch_name = "Rosalind I"
		dynasty = "Sidrey"
		birth_date = 365.1.1
		death_date = 472.1.1
		claim = 100
		adm = 4
		dip = 3
		mil = 5
		female = yes
    }
}

472.1.1 = {
	monarch = {
 		name = "Gabin"
		dynasty = "Barclay"
		birth_date = 448.1.1
		adm = 4
		dip = 6
		mil = 5
    }
}

558.1.1 = {
	monarch = {
 		name = "Bernique"
		dynasty = "Lelles"
		birth_date = 520.1.1
		adm = 2
		dip = 6
		mil = 2
		female = yes
    }
	queen = {
 		name = "Jean"
		dynasty = "Lelles"
		birth_date = 528.1.1
		adm = 6
		dip = 4
		mil = 1
    }
	heir = {
 		name = "Aren"
		monarch_name = "Aren I"
		dynasty = "Lelles"
		birth_date = 558.1.1
		death_date = 625.1.1
		claim = 100
		adm = 3
		dip = 3
		mil = 0
		female = yes
    }
}

625.1.1 = {
	monarch = {
 		name = "Malkoran"
		dynasty = "Leveque"
		birth_date = 606.1.1
		adm = 5
		dip = 4
		mil = 2
    }
}

702.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Luce"
		monarch_name = "Luce I"
		dynasty = "DuBois"
		birth_date = 692.1.1
		death_date = 710.1.1
		claim = 100
		adm = 6
		dip = 1
		mil = 0
		female = yes
    }
}

710.1.1 = {
	monarch = {
 		name = "Benry"
		dynasty = "Belette"
		birth_date = 689.1.1
		adm = 2
		dip = 2
		mil = 3
    }
	queen = {
 		name = "Anabeth"
		dynasty = "Belette"
		birth_date = 665.1.1
		adm = 6
		dip = 1
		mil = 2
		female = yes
    }
	heir = {
 		name = "Leobois"
		monarch_name = "Leobois I"
		dynasty = "Belette"
		birth_date = 707.1.1
		death_date = 784.1.1
		claim = 100
		adm = 3
		dip = 0
		mil = 1
    }
}

784.1.1 = {
	monarch = {
 		name = "Margaud"
		dynasty = "Boulat"
		birth_date = 757.1.1
		adm = 2
		dip = 3
		mil = 1
		female = yes
    }
}

849.1.1 = {
	monarch = {
 		name = "Theyo"
		dynasty = "Maulinie"
		birth_date = 812.1.1
		adm = 0
		dip = 2
		mil = 5
    }
}

920.1.1 = {
	monarch = {
 		name = "Martine"
		dynasty = "Tibbin"
		birth_date = 886.1.1
		adm = 5
		dip = 1
		mil = 1
    }
	queen = {
 		name = "Amelie"
		dynasty = "Tibbin"
		birth_date = 892.1.1
		adm = 2
		dip = 0
		mil = 0
		female = yes
    }
}

974.1.1 = {
	monarch = {
 		name = "Verney"
		dynasty = "Mon"
		birth_date = 927.1.1
		adm = 1
		dip = 6
		mil = 4
    }
	queen = {
 		name = "Falette"
		dynasty = "Mon"
		birth_date = 936.1.1
		adm = 4
		dip = 5
		mil = 3
		female = yes
    }
	heir = {
 		name = "Therich"
		monarch_name = "Therich I"
		dynasty = "Mon"
		birth_date = 961.1.1
		death_date = 1009.1.1
		claim = 100
		adm = 1
		dip = 3
		mil = 2
    }
}

1009.1.1 = {
	monarch = {
 		name = "Ines"
		dynasty = "Bruhl"
		birth_date = 987.1.1
		adm = 4
		dip = 4
		mil = 4
		female = yes
    }
}

1077.1.1 = {
	monarch = {
 		name = "Charmela"
		dynasty = "Begnaud"
		birth_date = 1051.1.1
		adm = 2
		dip = 4
		mil = 1
		female = yes
    }
}

1137.1.1 = {
	monarch = {
 		name = "Verdrey"
		dynasty = "Metrick"
		birth_date = 1116.1.1
		adm = 1
		dip = 3
		mil = 4
    }
	queen = {
 		name = "Senna"
		dynasty = "Metrick"
		birth_date = 1101.1.1
		adm = 4
		dip = 1
		mil = 3
		female = yes
    }
	heir = {
 		name = "Gwynnifer"
		monarch_name = "Gwynnifer I"
		dynasty = "Metrick"
		birth_date = 1127.1.1
		death_date = 1217.1.1
		claim = 100
		adm = 1
		dip = 0
		mil = 2
		female = yes
    }
}

1217.1.1 = {
	monarch = {
 		name = "Grandeau"
		dynasty = "Adrognese"
		birth_date = 1181.1.1
		adm = 1
		dip = 3
		mil = 3
    }
}

1271.1.1 = {
	monarch = {
 		name = "Odvan"
		dynasty = "Yunlin"
		birth_date = 1242.1.1
		adm = 6
		dip = 3
		mil = 6
    }
}

1312.1.1 = {
	monarch = {
 		name = "Janeve"
		dynasty = "Gilbeau"
		birth_date = 1268.1.1
		adm = 4
		dip = 2
		mil = 3
		female = yes
    }
}

1373.1.1 = {
	monarch = {
 		name = "Claudette"
		dynasty = "Crowe"
		birth_date = 1325.1.1
		adm = 2
		dip = 1
		mil = 0
		female = yes
    }
}

1442.1.1 = {
	monarch = {
 		name = "Ysabel"
		dynasty = "Canne"
		birth_date = 1404.1.1
		adm = 1
		dip = 0
		mil = 3
		female = yes
    }
	queen = {
 		name = "Ferarilie"
		dynasty = "Canne"
		birth_date = 1423.1.1
		adm = 5
		dip = 6
		mil = 2
    }
	heir = {
 		name = "Tildus"
		monarch_name = "Tildus I"
		dynasty = "Canne"
		birth_date = 1433.1.1
		death_date = 1528.1.1
		claim = 100
		adm = 1
		dip = 4
		mil = 1
    }
}

1528.1.1 = {
	monarch = {
 		name = "Janand"
		dynasty = "Bielle"
		birth_date = 1503.1.1
		adm = 4
		dip = 6
		mil = 3
		female = yes
    }
}

1602.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Urfon"
		monarch_name = "Urfon I"
		dynasty = "Doisne"
		birth_date = 1588.1.1
		death_date = 1606.1.1
		claim = 100
		adm = 5
		dip = 3
		mil = 1
    }
}

1606.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Milo"
		monarch_name = "Milo I"
		dynasty = "Molose"
		birth_date = 1591.1.1
		death_date = 1609.1.1
		claim = 100
		adm = 3
		dip = 2
		mil = 5
    }
}

1609.1.1 = {
	monarch = {
 		name = "Dame"
		dynasty = "Virane"
		birth_date = 1561.1.1
		adm = 3
		dip = 5
		mil = 5
		female = yes
    }
}

1674.1.1 = {
	monarch = {
 		name = "Zephyrine"
		dynasty = "Carlier"
		birth_date = 1655.1.1
		adm = 1
		dip = 4
		mil = 2
		female = yes
    }
}

1722.1.1 = {
	monarch = {
 		name = "Ninette"
		dynasty = "Munier"
		birth_date = 1704.1.1
		adm = 6
		dip = 4
		mil = 6
		female = yes
    }
}

1761.1.1 = {
	monarch = {
 		name = "Heron"
		dynasty = "Dencent"
		birth_date = 1709.1.1
		adm = 5
		dip = 0
		mil = 6
    }
}

1841.1.1 = {
	monarch = {
 		name = "Stephen"
		dynasty = "Jeanne"
		birth_date = 1818.1.1
		adm = 4
		dip = 6
		mil = 2
    }
}

1914.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Evolitte"
		monarch_name = "Evolitte I"
		dynasty = "Oncent"
		birth_date = 1903.1.1
		death_date = 1921.1.1
		claim = 100
		adm = 1
		dip = 1
		mil = 3
		female = yes
    }
}

1921.1.1 = {
	monarch = {
 		name = "Theodore"
		dynasty = "Virane"
		birth_date = 1881.1.1
		adm = 0
		dip = 5
		mil = 3
    }
	queen = {
 		name = "Marieve"
		dynasty = "Virane"
		birth_date = 1878.1.1
		adm = 3
		dip = 6
		mil = 5
		female = yes
    }
	heir = {
 		name = "Charlotte"
		monarch_name = "Charlotte I"
		dynasty = "Virane"
		birth_date = 1911.1.1
		death_date = 2015.1.1
		claim = 100
		adm = 0
		dip = 5
		mil = 4
		female = yes
    }
}

2015.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Lauric"
		monarch_name = "Lauric I"
		dynasty = "Aloette"
		birth_date = 2010.1.1
		death_date = 2028.1.1
		claim = 100
		adm = 6
		dip = 1
		mil = 4
    }
}

2028.1.1 = {
	monarch = {
 		name = "Beryn"
		dynasty = "Lanie"
		birth_date = 1975.1.1
		adm = 2
		dip = 2
		mil = 0
    }
	queen = {
 		name = "Aniette"
		dynasty = "Lanie"
		birth_date = 1992.1.1
		adm = 6
		dip = 1
		mil = 6
		female = yes
    }
}

2080.1.1 = {
	monarch = {
 		name = "Gordyn"
		dynasty = "Armene"
		birth_date = 2042.1.1
		adm = 4
		dip = 0
		mil = 2
    }
	queen = {
 		name = "Finia"
		dynasty = "Armene"
		birth_date = 2053.1.1
		adm = 1
		dip = 6
		mil = 1
		female = yes
    }
	heir = {
 		name = "Gaffer"
		monarch_name = "Gaffer I"
		dynasty = "Armene"
		birth_date = 2075.1.1
		death_date = 2176.1.1
		claim = 100
		adm = 5
		dip = 4
		mil = 0
    }
}

2176.1.1 = {
	monarch = {
 		name = "Vardan"
		dynasty = "Benoit"
		birth_date = 2123.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2231.1.1 = {
	monarch = {
 		name = "Munche"
		dynasty = "Helena"
		birth_date = 2194.1.1
		adm = 2
		dip = 0
		mil = 4
    }
	queen = {
 		name = "Maelrisse"
		dynasty = "Helena"
		birth_date = 2202.1.1
		adm = 6
		dip = 5
		mil = 3
		female = yes
    }
	heir = {
 		name = "Callice"
		monarch_name = "Callice I"
		dynasty = "Helena"
		birth_date = 2216.1.1
		death_date = 2312.1.1
		claim = 100
		adm = 0
		dip = 2
		mil = 4
		female = yes
    }
}

2312.1.1 = {
	monarch = {
 		name = "Muiri"
		dynasty = "Bloodcaller"
		birth_date = 2291.1.1
		adm = 6
		dip = 5
		mil = 4
		female = yes
    }
}

2374.1.1 = {
	monarch = {
 		name = "Gustave"
		dynasty = "Masoriane"
		birth_date = 2354.1.1
		adm = 4
		dip = 4
		mil = 1
    }
}

2466.1.1 = {
	monarch = {
 		name = "Clairene"
		dynasty = "Lozon"
		birth_date = 2435.1.1
		adm = 2
		dip = 4
		mil = 5
		female = yes
    }
}

