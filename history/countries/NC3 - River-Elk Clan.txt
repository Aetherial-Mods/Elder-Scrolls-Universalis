government = tribal
government_rank = 1
mercantilism = 1
technology_group = northern_tg
religion = old_gods_cult
primary_culture = reachmen
capital = 7204
religious_school = alrabeg_school
secondary_religion = namira_cult

54.1.1 = {
	monarch = {
 		name = "Predriro"
		dynasty = "Ped"
		birth_date = 17.1.1
		adm = 6
		dip = 1
		mil = 3
		female = yes
    }
}

153.1.1 = {
	monarch = {
 		name = "Relo"
		dynasty = "Omrafi"
		birth_date = 101.1.1
		adm = 4
		dip = 0
		mil = 0
		female = yes
    }
}

218.1.1 = {
	monarch = {
 		name = "Walage"
		dynasty = "Olcawa"
		birth_date = 186.1.1
		adm = 5
		dip = 4
		mil = 5
    }
}

269.1.1 = {
	monarch = {
 		name = "Eifa"
		dynasty = "Omveda"
		birth_date = 224.1.1
		adm = 3
		dip = 3
		mil = 2
		female = yes
    }
}

356.1.1 = {
	monarch = {
 		name = "Edvaw"
		dynasty = "Omveda"
		birth_date = 331.1.1
		adm = 6
		dip = 5
		mil = 4
    }
}

413.1.1 = {
	monarch = {
 		name = "Brumroch"
		dynasty = "Welin"
		birth_date = 385.1.1
		adm = 1
		dip = 6
		mil = 6
    }
}

506.1.1 = {
	monarch = {
 		name = "Walage"
		dynasty = "Greglih"
		birth_date = 453.1.1
		adm = 6
		dip = 5
		mil = 2
    }
}

545.1.1 = {
	monarch = {
 		name = "Meldich"
		dynasty = "Vereho"
		birth_date = 511.1.1
		adm = 5
		dip = 4
		mil = 6
    }
}

633.1.1 = {
	monarch = {
 		name = "Pevlen"
		dynasty = "Ergamo"
		birth_date = 608.1.1
		adm = 3
		dip = 4
		mil = 3
    }
}

718.1.1 = {
	monarch = {
 		name = "Dych"
		dynasty = "Elkonbuor"
		birth_date = 679.1.1
		adm = 1
		dip = 3
		mil = 6
    }
}

765.1.1 = {
	monarch = {
 		name = "Pondislug"
		dynasty = "Treroga"
		birth_date = 744.1.1
		adm = 6
		dip = 2
		mil = 3
    }
}

860.1.1 = {
	monarch = {
 		name = "Dranres"
		dynasty = "Wovror"
		birth_date = 813.1.1
		adm = 4
		dip = 1
		mil = 6
    }
}

959.1.1 = {
	monarch = {
 		name = "Doch"
		dynasty = "Bragranbac"
		birth_date = 930.1.1
		adm = 3
		dip = 0
		mil = 3
    }
}

1042.1.1 = {
	monarch = {
 		name = "Obrum"
		dynasty = "Olcivo"
		birth_date = 1013.1.1
		adm = 5
		dip = 2
		mil = 5
    }
}

1111.1.1 = {
	monarch = {
 		name = "Uaeme"
		dynasty = "Brunnuli"
		birth_date = 1080.1.1
		adm = 3
		dip = 1
		mil = 1
		female = yes
    }
}

1190.1.1 = {
	monarch = {
 		name = "Ilcev"
		dynasty = "Ralora"
		birth_date = 1144.1.1
		adm = 1
		dip = 0
		mil = 5
    }
}

1283.1.1 = {
	monarch = {
 		name = "Aiga"
		dynasty = "Belrusa"
		birth_date = 1256.1.1
		adm = 6
		dip = 6
		mil = 2
		female = yes
    }
}

1380.1.1 = {
	monarch = {
 		name = "Ocdolo"
		dynasty = "Braldeho"
		birth_date = 1347.1.1
		adm = 5
		dip = 5
		mil = 5
		female = yes
    }
}

1443.1.1 = {
	monarch = {
 		name = "Dog"
		dynasty = "Osrata"
		birth_date = 1407.1.1
		adm = 5
		dip = 3
		mil = 3
    }
}

1483.1.1 = {
	monarch = {
 		name = "Wovror"
		dynasty = "Taud"
		birth_date = 1455.1.1
		adm = 4
		dip = 2
		mil = 0
    }
}

1581.1.1 = {
	monarch = {
 		name = "Welin"
		dynasty = "Bor"
		birth_date = 1557.1.1
		adm = 2
		dip = 1
		mil = 3
    }
}

1669.1.1 = {
	monarch = {
 		name = "Drergan"
		dynasty = "Dridvomvac"
		birth_date = 1650.1.1
		adm = 0
		dip = 0
		mil = 0
    }
}

1764.1.1 = {
	monarch = {
 		name = "Proch"
		dynasty = "Dranres"
		birth_date = 1736.1.1
		adm = 5
		dip = 6
		mil = 4
    }
}

1839.1.1 = {
	monarch = {
 		name = "Wovror"
		dynasty = "Dridvomvac"
		birth_date = 1814.1.1
		adm = 4
		dip = 6
		mil = 0
    }
}

1901.1.1 = {
	monarch = {
 		name = "Wovvavi"
		dynasty = "Pal"
		birth_date = 1852.1.1
		adm = 5
		dip = 0
		mil = 2
		female = yes
    }
}

1946.1.1 = {
	monarch = {
 		name = "Vuur"
		dynasty = "Osrata"
		birth_date = 1912.1.1
		adm = 4
		dip = 6
		mil = 6
    }
}

2045.1.1 = {
	monarch = {
 		name = "Dridvomvac"
		dynasty = "Greg"
		birth_date = 2006.1.1
		adm = 2
		dip = 5
		mil = 2
    }
}

2094.1.1 = {
	monarch = {
 		name = "Nuolrig"
		dynasty = "Enneda"
		birth_date = 2045.1.1
		adm = 0
		dip = 4
		mil = 6
    }
}

2171.1.1 = {
	monarch = {
 		name = "Nuvvahe"
		dynasty = "Odohe"
		birth_date = 2153.1.1
		adm = 5
		dip = 4
		mil = 3
		female = yes
    }
}

2256.1.1 = {
	monarch = {
 		name = "Taud"
		dynasty = "Bayl"
		birth_date = 2224.1.1
		adm = 4
		dip = 3
		mil = 6
    }
}

2316.1.1 = {
	monarch = {
 		name = "Ruldihu"
		dynasty = "Tuch"
		birth_date = 2277.1.1
		adm = 2
		dip = 2
		mil = 3
		female = yes
    }
}

2379.1.1 = {
	monarch = {
 		name = "Levi"
		dynasty = "Onbus"
		birth_date = 2327.1.1
		adm = 0
		dip = 1
		mil = 0
		female = yes
    }
}

2467.1.1 = {
	monarch = {
 		name = "Abrari"
		dynasty = "Brunnuli"
		birth_date = 2417.1.1
		adm = 2
		dip = 3
		mil = 1
    }
}

