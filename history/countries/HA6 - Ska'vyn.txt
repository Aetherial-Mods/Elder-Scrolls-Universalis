government = monarchy
government_rank = 3
mercantilism = 1
technology_group = cyrodiil_tg
religion = four_parents
primary_culture = keptu
capital = 1474

54.1.1 = {
	monarch = {
 		name = "Ludec"
		dynasty = "Bauvmith"
		birth_date = 12.1.1
		adm = 3
		dip = 4
		mil = 4
    }
	queen = {
 		name = "Masehur"
		dynasty = "Bauvmith"
		birth_date = 26.1.1
		adm = 0
		dip = 3
		mil = 3
		female = yes
    }
	heir = {
 		name = "Ihrasek"
		monarch_name = "Ihrasek I"
		dynasty = "Bauvmith"
		birth_date = 51.1.1
		death_date = 117.1.1
		claim = 100
		adm = 0
		dip = 6
		mil = 4
    }
}

117.1.1 = {
	monarch = {
 		name = "Likoth"
		dynasty = "Kynhesac"
		birth_date = 97.1.1
		adm = 6
		dip = 3
		mil = 5
    }
	queen = {
 		name = "Ranni"
		dynasty = "Kynhesac"
		birth_date = 65.1.1
		adm = 3
		dip = 1
		mil = 4
		female = yes
    }
	heir = {
 		name = "Desylis"
		monarch_name = "Desylis I"
		dynasty = "Kynhesac"
		birth_date = 116.1.1
		death_date = 181.1.1
		claim = 100
		adm = 0
		dip = 0
		mil = 3
    }
}

181.1.1 = {
	monarch = {
 		name = "Sovi"
		dynasty = "Shelren"
		birth_date = 158.1.1
		adm = 3
		dip = 1
		mil = 5
		female = yes
    }
	queen = {
 		name = "Haukos"
		dynasty = "Shelren"
		birth_date = 146.1.1
		adm = 0
		dip = 0
		mil = 4
    }
	heir = {
 		name = "Vinhylak"
		monarch_name = "Vinhylak I"
		dynasty = "Shelren"
		birth_date = 179.1.1
		death_date = 235.1.1
		claim = 100
		adm = 4
		dip = 5
		mil = 3
    }
}

235.1.1 = {
	monarch = {
 		name = "Doshir"
		dynasty = "Bardek"
		birth_date = 193.1.1
		adm = 6
		dip = 0
		mil = 5
		female = yes
    }
}

300.1.1 = {
	monarch = {
 		name = "Allalaer"
		dynasty = "Naonhaudes"
		birth_date = 255.1.1
		adm = 1
		dip = 1
		mil = 0
		female = yes
    }
	queen = {
 		name = "Kalrin"
		dynasty = "Naonhaudes"
		birth_date = 269.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

367.1.1 = {
	monarch = {
 		name = "Vristidok"
		dynasty = "Haonuvic"
		birth_date = 339.1.1
		adm = 3
		dip = 6
		mil = 2
    }
	queen = {
 		name = "Nomuh"
		dynasty = "Haonuvic"
		birth_date = 339.1.1
		adm = 4
		dip = 2
		mil = 4
		female = yes
    }
}

461.1.1 = {
	monarch = {
 		name = "Homin"
		dynasty = "Aroc"
		birth_date = 411.1.1
		adm = 6
		dip = 3
		mil = 5
		female = yes
    }
}

499.1.1 = {
	monarch = {
 		name = "Issaeh"
		dynasty = "Amedoc"
		birth_date = 452.1.1
		adm = 4
		dip = 3
		mil = 2
		female = yes
    }
}

556.1.1 = {
	monarch = {
 		name = "Ronah"
		dynasty = "Tylak"
		birth_date = 510.1.1
		adm = 2
		dip = 2
		mil = 5
		female = yes
    }
}

650.1.1 = {
	monarch = {
 		name = "Izaev"
		dynasty = "Yhin"
		birth_date = 611.1.1
		adm = 0
		dip = 1
		mil = 2
		female = yes
    }
}

721.1.1 = {
	monarch = {
 		name = "Vrilak"
		dynasty = "Demralis"
		birth_date = 675.1.1
		adm = 5
		dip = 0
		mil = 6
    }
	queen = {
 		name = "Resa"
		dynasty = "Demralis"
		birth_date = 689.1.1
		adm = 2
		dip = 6
		mil = 5
		female = yes
    }
	heir = {
 		name = "Bardek"
		monarch_name = "Bardek I"
		dynasty = "Demralis"
		birth_date = 719.1.1
		death_date = 802.1.1
		claim = 100
		adm = 6
		dip = 4
		mil = 4
    }
}

802.1.1 = {
	monarch = {
 		name = "Tykisok"
		dynasty = "Larduve"
		birth_date = 769.1.1
		adm = 2
		dip = 6
		mil = 6
    }
	queen = {
 		name = "Zathi"
		dynasty = "Larduve"
		birth_date = 761.1.1
		adm = 6
		dip = 4
		mil = 5
		female = yes
    }
	heir = {
 		name = "Kaahlec"
		monarch_name = "Kaahlec I"
		dynasty = "Larduve"
		birth_date = 794.1.1
		death_date = 882.1.1
		claim = 100
		adm = 3
		dip = 3
		mil = 4
    }
}

882.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Ihrasek"
		monarch_name = "Ihrasek I"
		dynasty = "Lukaazon"
		birth_date = 874.1.1
		death_date = 892.1.1
		claim = 100
		adm = 3
		dip = 0
		mil = 2
    }
}

892.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Talih"
		monarch_name = "Talih I"
		dynasty = "Bauvmith"
		birth_date = 880.1.1
		death_date = 898.1.1
		claim = 100
		adm = 2
		dip = 6
		mil = 5
		female = yes
    }
}

898.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Oman"
		monarch_name = "Oman I"
		dynasty = "Tirdor"
		birth_date = 887.1.1
		death_date = 905.1.1
		claim = 100
		adm = 2
		dip = 3
		mil = 3
		female = yes
    }
}

905.1.1 = {
	monarch = {
 		name = "Zurduzok"
		dynasty = "Virdanath"
		birth_date = 858.1.1
		adm = 6
		dip = 1
		mil = 2
    }
	queen = {
 		name = "Ranni"
		dynasty = "Virdanath"
		birth_date = 880.1.1
		adm = 0
		dip = 5
		mil = 3
		female = yes
    }
	heir = {
 		name = "Satohar"
		monarch_name = "Satohar I"
		dynasty = "Virdanath"
		birth_date = 905.1.1
		death_date = 1004.1.1
		claim = 100
		adm = 3
		dip = 3
		mil = 2
		female = yes
    }
}

1004.1.1 = {
	monarch = {
 		name = "Nuvnizo"
		dynasty = "Vaulic"
		birth_date = 977.1.1
		adm = 3
		dip = 0
		mil = 3
    }
}

1041.1.1 = {
	monarch = {
 		name = "Vurmesoth"
		dynasty = "Hetok"
		birth_date = 998.1.1
		adm = 4
		dip = 4
		mil = 1
    }
}

1087.1.1 = {
	monarch = {
 		name = "Emin"
		dynasty = "Zyrdusor"
		birth_date = 1039.1.1
		adm = 2
		dip = 3
		mil = 4
		female = yes
    }
	queen = {
 		name = "Taaner"
		dynasty = "Zyrdusor"
		birth_date = 1055.1.1
		adm = 6
		dip = 2
		mil = 3
    }
}

1170.1.1 = {
	monarch = {
 		name = "Dame"
		dynasty = "Vrakir"
		birth_date = 1131.1.1
		adm = 4
		dip = 1
		mil = 0
		female = yes
    }
}

1257.1.1 = {
	monarch = {
 		name = "Disseh"
		dynasty = "Lyrmerek"
		birth_date = 1218.1.1
		adm = 2
		dip = 0
		mil = 4
		female = yes
    }
	queen = {
 		name = "Virdanath"
		dynasty = "Lyrmerek"
		birth_date = 1223.1.1
		adm = 6
		dip = 6
		mil = 2
    }
	heir = {
 		name = "Menhadeth"
		monarch_name = "Menhadeth I"
		dynasty = "Lyrmerek"
		birth_date = 1251.1.1
		death_date = 1311.1.1
		claim = 100
		adm = 3
		dip = 4
		mil = 1
    }
}

1311.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Issaeh"
		monarch_name = "Issaeh I"
		dynasty = "Tadak"
		birth_date = 1298.1.1
		death_date = 1316.1.1
		claim = 100
		adm = 1
		dip = 3
		mil = 5
		female = yes
    }
}

1316.1.1 = {
	monarch = {
 		name = "Zyrdusor"
		dynasty = "Bakumoth"
		birth_date = 1279.1.1
		adm = 4
		dip = 5
		mil = 0
    }
	queen = {
 		name = "Sonev"
		dynasty = "Bakumoth"
		birth_date = 1265.1.1
		adm = 1
		dip = 3
		mil = 6
		female = yes
    }
	heir = {
 		name = "Halaanis"
		monarch_name = "Halaanis I"
		dynasty = "Bakumoth"
		birth_date = 1314.1.1
		death_date = 1376.1.1
		claim = 100
		adm = 5
		dip = 2
		mil = 5
    }
}

1376.1.1 = {
	monarch = {
 		name = "Tadak"
		dynasty = "Remaasok"
		birth_date = 1323.1.1
		adm = 0
		dip = 3
		mil = 1
    }
}

1413.1.1 = {
	monarch = {
 		name = "Resi"
		dynasty = "Dulros"
		birth_date = 1367.1.1
		adm = 2
		dip = 4
		mil = 2
		female = yes
    }
	queen = {
 		name = "Bardek"
		dynasty = "Dulros"
		birth_date = 1389.1.1
		adm = 3
		dip = 1
		mil = 3
    }
}

1462.1.1 = {
	monarch = {
 		name = "Mothal"
		dynasty = "Tylak"
		birth_date = 1428.1.1
		adm = 3
		dip = 5
		mil = 1
		female = yes
    }
}

1549.1.1 = {
	monarch = {
 		name = "Sarul"
		dynasty = "Muvmec"
		birth_date = 1525.1.1
		adm = 2
		dip = 4
		mil = 5
		female = yes
    }
	queen = {
 		name = "Zerlec"
		dynasty = "Muvmec"
		birth_date = 1509.1.1
		adm = 1
		dip = 1
		mil = 5
    }
	heir = {
 		name = "Gahlaras"
		monarch_name = "Gahlaras I"
		dynasty = "Muvmec"
		birth_date = 1549.1.1
		death_date = 1585.1.1
		claim = 100
		adm = 5
		dip = 6
		mil = 4
    }
}

1585.1.1 = {
	monarch = {
 		name = "Vaorylan"
		dynasty = "Ridravin"
		birth_date = 1557.1.1
		adm = 3
		dip = 5
		mil = 1
    }
	queen = {
 		name = "Nizae"
		dynasty = "Ridravin"
		birth_date = 1555.1.1
		adm = 0
		dip = 4
		mil = 0
		female = yes
    }
	heir = {
 		name = "Nharmok"
		monarch_name = "Nharmok I"
		dynasty = "Ridravin"
		birth_date = 1579.1.1
		death_date = 1631.1.1
		claim = 100
		adm = 4
		dip = 2
		mil = 6
    }
}

1631.1.1 = {
	monarch = {
 		name = "Viladuh"
		dynasty = "Ridravin"
		birth_date = 1591.1.1
		adm = 0
		dip = 4
		mil = 1
		female = yes
    }
}

1691.1.1 = {
	monarch = {
 		name = "Nathael"
		dynasty = "Letan"
		birth_date = 1657.1.1
		adm = 5
		dip = 3
		mil = 5
		female = yes
    }
}

1740.1.1 = {
	monarch = {
 		name = "Haonuvic"
		dynasty = "Vaulic"
		birth_date = 1709.1.1
		adm = 3
		dip = 2
		mil = 1
    }
	queen = {
 		name = "Ithamav"
		dynasty = "Vaulic"
		birth_date = 1700.1.1
		adm = 0
		dip = 1
		mil = 0
		female = yes
    }
	heir = {
 		name = "Nharmok"
		monarch_name = "Nharmok I"
		dynasty = "Vaulic"
		birth_date = 1740.1.1
		death_date = 1788.1.1
		claim = 100
		adm = 4
		dip = 6
		mil = 6
    }
}

1788.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Vrustimek"
		monarch_name = "Vrustimek I"
		dynasty = "Lukaazon"
		birth_date = 1776.1.1
		death_date = 1794.1.1
		claim = 100
		adm = 2
		dip = 5
		mil = 3
    }
}

1794.1.1 = {
	monarch = {
 		name = "Bakumoth"
		dynasty = "Bakumoth"
		birth_date = 1773.1.1
		adm = 1
		dip = 2
		mil = 3
    }
	queen = {
 		name = "Ollatuv"
		dynasty = "Bakumoth"
		birth_date = 1742.1.1
		adm = 5
		dip = 0
		mil = 2
		female = yes
    }
	heir = {
 		name = "Runhyrac"
		monarch_name = "Runhyrac I"
		dynasty = "Bakumoth"
		birth_date = 1784.1.1
		death_date = 1884.1.1
		claim = 100
		adm = 1
		dip = 2
		mil = 5
    }
}

1884.1.1 = {
	monarch = {
 		name = "Runhath"
		dynasty = "Nhekhedin"
		birth_date = 1847.1.1
		adm = 0
		dip = 5
		mil = 5
    }
}

1937.1.1 = {
	monarch = {
 		name = "Soshemeh"
		dynasty = "Ridravin"
		birth_date = 1896.1.1
		adm = 6
		dip = 4
		mil = 1
		female = yes
    }
	queen = {
 		name = "Tudaarin"
		dynasty = "Ridravin"
		birth_date = 1915.1.1
		adm = 3
		dip = 3
		mil = 0
    }
	heir = {
 		name = "Vaulic"
		monarch_name = "Vaulic I"
		dynasty = "Ridravin"
		birth_date = 1928.1.1
		death_date = 2033.1.1
		claim = 100
		adm = 3
		dip = 6
		mil = 2
    }
}

2033.1.1 = {
	monarch = {
 		name = "Gumrok"
		dynasty = "Nuvnizo"
		birth_date = 2007.1.1
		adm = 2
		dip = 3
		mil = 2
    }
	queen = {
 		name = "Levadaeh"
		dynasty = "Nuvnizo"
		birth_date = 1982.1.1
		adm = 6
		dip = 1
		mil = 1
		female = yes
    }
	heir = {
 		name = "Kaomrar"
		monarch_name = "Kaomrar I"
		dynasty = "Nuvnizo"
		birth_date = 2019.1.1
		death_date = 2090.1.1
		claim = 100
		adm = 3
		dip = 0
		mil = 0
    }
}

2090.1.1 = {
	monarch = {
 		name = "Gaohen"
		dynasty = "Haukos"
		birth_date = 2037.1.1
		adm = 6
		dip = 1
		mil = 2
    }
}

2169.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Vaulic"
		monarch_name = "Vaulic I"
		dynasty = "Gukhedas"
		birth_date = 2164.1.1
		death_date = 2182.1.1
		claim = 100
		adm = 6
		dip = 5
		mil = 0
    }
}

2182.1.1 = {
	monarch = {
 		name = "Gumrok"
		dynasty = "Runhath"
		birth_date = 2149.1.1
		adm = 2
		dip = 0
		mil = 2
    }
}

2239.1.1 = {
	monarch = {
 		name = "Lyrmerek"
		dynasty = "Vauleso"
		birth_date = 2206.1.1
		adm = 4
		dip = 1
		mil = 4
    }
	queen = {
 		name = "Ashodev"
		dynasty = "Vauleso"
		birth_date = 2208.1.1
		adm = 4
		dip = 4
		mil = 5
		female = yes
    }
	heir = {
 		name = "Gahlaras"
		monarch_name = "Gahlaras I"
		dynasty = "Vauleso"
		birth_date = 2229.1.1
		death_date = 2326.1.1
		claim = 100
		adm = 1
		dip = 3
		mil = 4
    }
}

2326.1.1 = {
	monarch = {
 		name = "Talih"
		dynasty = "Ihrasek"
		birth_date = 2289.1.1
		adm = 1
		dip = 6
		mil = 4
		female = yes
    }
	queen = {
 		name = "Ralreloc"
		dynasty = "Ihrasek"
		birth_date = 2304.1.1
		adm = 4
		dip = 5
		mil = 3
    }
	heir = {
 		name = "Hosir"
		monarch_name = "Hosir I"
		dynasty = "Ihrasek"
		birth_date = 2321.1.1
		death_date = 2405.1.1
		claim = 100
		adm = 1
		dip = 3
		mil = 2
		female = yes
    }
}

2405.1.1 = {
	monarch = {
 		name = "Kalrin"
		dynasty = "Nhudren"
		birth_date = 2382.1.1
		adm = 4
		dip = 5
		mil = 5
    }
}

