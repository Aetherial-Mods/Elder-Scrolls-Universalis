government = republic
government_rank = 1
mercantilism = 1
technology_group = yokudan_tg
religion = redguard_pantheon
primary_culture = redguard
capital = 1470

54.1.1 = {
	monarch = {
 		name = "Riza"
		dynasty = "Churcur"
		birth_date = 28.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

131.1.1 = {
	monarch = {
 		name = "Lurana"
		dynasty = "Faliaron"
		birth_date = 111.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

173.1.1 = {
	monarch = {
 		name = "Gervaise"
		dynasty = "Wilbo"
		birth_date = 127.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

222.1.1 = {
	monarch = {
 		name = "Adeena"
		dynasty = "Manielan"
		birth_date = 199.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

291.1.1 = {
	monarch = {
 		name = "Rhano"
		dynasty = "Torlhael"
		birth_date = 252.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

368.1.1 = {
	monarch = {
 		name = "Lathdahan"
		dynasty = "Chararon"
		birth_date = 339.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

467.1.1 = {
	monarch = {
 		name = "Gagni"
		dynasty = "Sadssean"
		birth_date = 440.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

554.1.1 = {
	monarch = {
 		name = "Abdahel"
		dynasty = "Cyrcy"
		birth_date = 525.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

649.1.1 = {
	monarch = {
 		name = "Glanlian"
		dynasty = "Dindal"
		birth_date = 625.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

691.1.1 = {
	monarch = {
 		name = "Afshar"
		dynasty = "Irgher"
		birth_date = 661.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

741.1.1 = {
	monarch = {
 		name = "Rouda"
		dynasty = "Grekrk"
		birth_date = 703.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

782.1.1 = {
	monarch = {
 		name = "Machouz"
		dynasty = "Proanan"
		birth_date = 759.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

859.1.1 = {
	monarch = {
 		name = "Ginal"
		dynasty = "Nisir"
		birth_date = 812.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

954.1.1 = {
	monarch = {
 		name = "Adruzan"
		dynasty = "Jaganuin"
		birth_date = 907.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1050.1.1 = {
	monarch = {
 		name = "Roshan"
		dynasty = "Traah"
		birth_date = 1006.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1122.1.1 = {
	monarch = {
 		name = "Loqman"
		dynasty = "Greelan"
		birth_date = 1086.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1184.1.1 = {
	monarch = {
 		name = "Sakh"
		dynasty = "Manig"
		birth_date = 1146.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1236.1.1 = {
	monarch = {
 		name = "Mamidah"
		dynasty = "Sadssean"
		birth_date = 1207.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1293.1.1 = {
	monarch = {
 		name = "Boldon"
		dynasty = "Shelamon"
		birth_date = 1261.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1363.1.1 = {
	monarch = {
 		name = "Jayri"
		dynasty = "B'ollka"
		birth_date = 1315.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1428.1.1 = {
	monarch = {
 		name = "Coyle"
		dynasty = "Shelamon"
		birth_date = 1387.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1469.1.1 = {
	monarch = {
 		name = "Turaman"
		dynasty = "Nust-Si"
		birth_date = 1420.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1520.1.1 = {
	monarch = {
 		name = "Neenia"
		dynasty = "Nistolm"
		birth_date = 1478.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1594.1.1 = {
	monarch = {
 		name = "Jawanan"
		dynasty = "Chestem"
		birth_date = 1575.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1691.1.1 = {
	monarch = {
 		name = "Dahnadreel"
		dynasty = "Khirdrn"
		birth_date = 1654.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1759.1.1 = {
	monarch = {
 		name = "Trayvond"
		dynasty = "Endotias"
		birth_date = 1726.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1814.1.1 = {
	monarch = {
 		name = "Nebzez"
		dynasty = "Delmin"
		birth_date = 1791.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1876.1.1 = {
	monarch = {
 		name = "Varnado"
		dynasty = "Sadekim"
		birth_date = 1848.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1952.1.1 = {
	monarch = {
 		name = "Ohrmarz"
		dynasty = "M'urcti"
		birth_date = 1902.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2044.1.1 = {
	monarch = {
 		name = "Jonis"
		dynasty = "Irgher"
		birth_date = 2025.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2107.1.1 = {
	monarch = {
 		name = "Darwaz"
		dynasty = "Teztha"
		birth_date = 2074.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2202.1.1 = {
	monarch = {
 		name = "Gilzir"
		dynasty = "M'arcki"
		birth_date = 2161.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2257.1.1 = {
	monarch = {
 		name = "Adenar"
		dynasty = "Killba"
		birth_date = 2220.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2294.1.1 = {
	monarch = {
 		name = "Romehdi"
		dynasty = "Delmin"
		birth_date = 2241.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

2390.1.1 = {
	monarch = {
 		name = "Maeresha"
		dynasty = "B'ollka"
		birth_date = 2343.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

2442.1.1 = {
	monarch = {
 		name = "Ghiyath"
		dynasty = "Firink"
		birth_date = 2394.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2497.1.1 = {
	monarch = {
 		name = "Afareen"
		dynasty = "Chestem"
		birth_date = 2456.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

