government = republic
government_rank = 1
mercantilism = 1
technology_group = cyrodiil_tg
religion = nedic_pantheon
primary_culture = nedic
capital = 5318

54.1.1 = {
	monarch = {
 		name = "Nhudren"
		dynasty = "Daolo"
		birth_date = 20.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

99.1.1 = {
	monarch = {
 		name = "Lyrmerek"
		dynasty = "Ytas"
		birth_date = 53.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

156.1.1 = {
	monarch = {
 		name = "Haukos"
		dynasty = "Bardek"
		birth_date = 120.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

237.1.1 = {
	monarch = {
 		name = "Amedoc"
		dynasty = "Gaohen"
		birth_date = 190.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

329.1.1 = {
	monarch = {
 		name = "Lihareh"
		dynasty = "Desylis"
		birth_date = 287.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

387.1.1 = {
	monarch = {
 		name = "Zahoti"
		dynasty = "Dauhrak"
		birth_date = 360.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

446.1.1 = {
	monarch = {
 		name = "Bauvmith"
		dynasty = "Tudaarin"
		birth_date = 401.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

501.1.1 = {
	monarch = {
 		name = "Naamaulec"
		dynasty = "Ihrasek"
		birth_date = 464.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

544.1.1 = {
	monarch = {
 		name = "Resi"
		dynasty = "Dulros"
		birth_date = 491.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

584.1.1 = {
	monarch = {
 		name = "Ilirah"
		dynasty = "Nhihreseth"
		birth_date = 535.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

660.1.1 = {
	monarch = {
 		name = "Gumrok"
		dynasty = "Zakysor"
		birth_date = 630.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

758.1.1 = {
	monarch = {
 		name = "Lashavun"
		dynasty = "Bakumoth"
		birth_date = 739.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

808.1.1 = {
	monarch = {
 		name = "Hiherev"
		dynasty = "Hetok"
		birth_date = 771.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

898.1.1 = {
	monarch = {
 		name = "Bardek"
		dynasty = "Menhadeth"
		birth_date = 865.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

964.1.1 = {
	monarch = {
 		name = "Dastith"
		dynasty = "Tudaarin"
		birth_date = 932.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1019.1.1 = {
	monarch = {
 		name = "Kaahlec"
		dynasty = "Kynhesac"
		birth_date = 967.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1099.1.1 = {
	monarch = {
 		name = "Retobil"
		dynasty = "Halaanis"
		birth_date = 1064.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1171.1.1 = {
	monarch = {
 		name = "Vredraonir"
		dynasty = "Daolo"
		birth_date = 1127.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1225.1.1 = {
	monarch = {
 		name = "Nhystareth"
		dynasty = "Zakysor"
		birth_date = 1187.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1263.1.1 = {
	monarch = {
 		name = "Emin"
		dynasty = "Nuvnizo"
		birth_date = 1240.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1310.1.1 = {
	monarch = {
 		name = "Lerrimiv"
		dynasty = "Taaner"
		birth_date = 1284.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1381.1.1 = {
	monarch = {
 		name = "Vredraonir"
		dynasty = "Vrilak"
		birth_date = 1344.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1421.1.1 = {
	monarch = {
 		name = "Vinhylak"
		dynasty = "Rardoc"
		birth_date = 1382.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1459.1.1 = {
	monarch = {
 		name = "Tirleros"
		dynasty = "Naanuzar"
		birth_date = 1411.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1514.1.1 = {
	monarch = {
 		name = "Satohar"
		dynasty = "Taatysoth"
		birth_date = 1464.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1604.1.1 = {
	monarch = {
 		name = "Havu"
		dynasty = "Vauleso"
		birth_date = 1567.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1660.1.1 = {
	monarch = {
 		name = "Norri"
		dynasty = "Nharmok"
		birth_date = 1607.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1730.1.1 = {
	monarch = {
 		name = "Kaomrar"
		dynasty = "Vrakir"
		birth_date = 1700.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1778.1.1 = {
	monarch = {
 		name = "Riralath"
		dynasty = "Gumrok"
		birth_date = 1751.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1829.1.1 = {
	monarch = {
 		name = "Tazamaev"
		dynasty = "Demralis"
		birth_date = 1811.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1890.1.1 = {
	monarch = {
 		name = "Halaanis"
		dynasty = "Gikamik"
		birth_date = 1840.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1938.1.1 = {
	monarch = {
 		name = "Milaer"
		dynasty = "Naonhaudes"
		birth_date = 1893.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1992.1.1 = {
	monarch = {
 		name = "Eshebir"
		dynasty = "Nharmok"
		birth_date = 1940.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

2059.1.1 = {
	monarch = {
 		name = "Remaasok"
		dynasty = "Dadrades"
		birth_date = 2037.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2131.1.1 = {
	monarch = {
 		name = "Vonal"
		dynasty = "Riralath"
		birth_date = 2096.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

2193.1.1 = {
	monarch = {
 		name = "Gilraoles"
		dynasty = "Tirdor"
		birth_date = 2157.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2246.1.1 = {
	monarch = {
 		name = "Dennida"
		dynasty = "Demralis"
		birth_date = 2224.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

2302.1.1 = {
	monarch = {
 		name = "Zonemaer"
		dynasty = "Larduve"
		birth_date = 2283.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

2381.1.1 = {
	monarch = {
 		name = "Nharmok"
		dynasty = "Rardoc"
		birth_date = 2363.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

2459.1.1 = {
	monarch = {
 		name = "Gilraoles"
		dynasty = "Tirlaasir"
		birth_date = 2435.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

