government = monarchy
government_rank = 1
mercantilism = 1
technology_group = elven_tg
religion = altmeri_pantheon
primary_culture = altmer
capital = 2441

54.1.1 = {
	monarch = {
 		name = "Lanitaale"
		dynasty = "Wel-Aelsinonin"
		birth_date = 24.1.1
		adm = 5
		dip = 5
		mil = 1
		female = yes
    }
	queen = {
 		name = "Tymonir"
		dynasty = "Wel-Aelsinonin"
		birth_date = 12.1.1
		adm = 1
		dip = 4
		mil = 0
    }
}

153.1.1 = {
	monarch = {
 		name = "Talonir"
		dynasty = "Wel-Calmssare"
		birth_date = 106.1.1
		adm = 0
		dip = 3
		mil = 4
    }
	queen = {
 		name = "Ceryra"
		dynasty = "Wel-Calmssare"
		birth_date = 120.1.1
		adm = 4
		dip = 2
		mil = 3
		female = yes
    }
	heir = {
 		name = "Sanyon"
		monarch_name = "Sanyon I"
		dynasty = "Wel-Calmssare"
		birth_date = 151.1.1
		death_date = 212.1.1
		claim = 100
		adm = 6
		dip = 3
		mil = 5
    }
}

212.1.1 = {
	monarch = {
 		name = "Belardirwe"
		dynasty = "Bel-Spelluseus"
		birth_date = 159.1.1
		adm = 6
		dip = 6
		mil = 5
		female = yes
    }
}

250.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Nolontar"
		monarch_name = "Nolontar I"
		dynasty = "Ael-Mirlenya"
		birth_date = 239.1.1
		death_date = 257.1.1
		claim = 100
		adm = 3
		dip = 1
		mil = 5
    }
}

257.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Hendaril"
		monarch_name = "Hendaril I"
		dynasty = "Wel-Elsinthaer"
		birth_date = 245.1.1
		death_date = 263.1.1
		claim = 100
		adm = 5
		dip = 3
		mil = 0
    }
}

263.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Caryarel"
		monarch_name = "Caryarel I"
		dynasty = "Ael-Korael"
		birth_date = 261.1.1
		death_date = 279.1.1
		claim = 100
		adm = 3
		dip = 2
		mil = 3
    }
}

279.1.1 = {
	monarch = {
 		name = "Nurofire"
		dynasty = "Wel-Eleaninde"
		birth_date = 259.1.1
		adm = 6
		dip = 3
		mil = 6
		female = yes
    }
}

323.1.1 = {
	monarch = {
 		name = "Isinborn"
		dynasty = "Wel-Caminalda"
		birth_date = 274.1.1
		adm = 4
		dip = 2
		mil = 2
    }
	queen = {
 		name = "Endetuile"
		dynasty = "Wel-Caminalda"
		birth_date = 299.1.1
		adm = 1
		dip = 1
		mil = 1
		female = yes
    }
}

396.1.1 = {
	monarch = {
 		name = "Quen"
		dynasty = "Bel-Scutters"
		birth_date = 375.1.1
		adm = 1
		dip = 5
		mil = 6
		female = yes
    }
}

462.1.1 = {
	monarch = {
 		name = "Tanulldel"
		dynasty = "Ael-Kardnar"
		birth_date = 417.1.1
		adm = 0
		dip = 4
		mil = 3
    }
	queen = {
 		name = "Cirterisse"
		dynasty = "Ael-Kardnar"
		birth_date = 427.1.1
		adm = 4
		dip = 3
		mil = 2
		female = yes
    }
	heir = {
 		name = "Sawen"
		monarch_name = "Sawen I"
		dynasty = "Ael-Kardnar"
		birth_date = 452.1.1
		death_date = 501.1.1
		claim = 100
		adm = 0
		dip = 1
		mil = 1
		female = yes
    }
}

501.1.1 = {
	monarch = {
 		name = "Galirlmo"
		dynasty = "Bel-Vareya"
		birth_date = 449.1.1
		adm = 3
		dip = 3
		mil = 3
    }
}

542.1.1 = {
	monarch = {
 		name = "Avys"
		dynasty = "Ael-Jaerfhaer"
		birth_date = 516.1.1
		adm = 4
		dip = 0
		mil = 1
    }
}

594.1.1 = {
	monarch = {
 		name = "Earrastel"
		dynasty = "Bel-Scutters"
		birth_date = 568.1.1
		adm = 3
		dip = 3
		mil = 1
    }
	queen = {
 		name = "Renduril"
		dynasty = "Bel-Scutters"
		birth_date = 555.1.1
		adm = 4
		dip = 0
		mil = 3
		female = yes
    }
	heir = {
 		name = "Fasundil"
		monarch_name = "Fasundil I"
		dynasty = "Bel-Scutters"
		birth_date = 586.1.1
		death_date = 639.1.1
		claim = 100
		adm = 0
		dip = 5
		mil = 1
    }
}

639.1.1 = {
	monarch = {
 		name = "Telandil"
		dynasty = "Bel-Shimmerene"
		birth_date = 596.1.1
		adm = 0
		dip = 2
		mil = 2
		female = yes
    }
}

712.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Siramanwe"
		monarch_name = "Siramanwe I"
		dynasty = "Ael-Kardnar"
		birth_date = 703.1.1
		death_date = 721.1.1
		claim = 100
		adm = 1
		dip = 6
		mil = 0
		female = yes
    }
}

721.1.1 = {
	monarch = {
 		name = "Halimorion"
		dynasty = "Wel-Angaelle"
		birth_date = 691.1.1
		adm = 3
		dip = 0
		mil = 2
    }
	queen = {
 		name = "Raven"
		dynasty = "Wel-Angaelle"
		birth_date = 701.1.1
		adm = 0
		dip = 6
		mil = 1
		female = yes
    }
	heir = {
 		name = "Elannie"
		monarch_name = "Elannie I"
		dynasty = "Wel-Angaelle"
		birth_date = 710.1.1
		death_date = 791.1.1
		claim = 100
		adm = 4
		dip = 4
		mil = 0
		female = yes
    }
}

791.1.1 = {
	monarch = {
 		name = "Tarwannewen"
		dynasty = "Ael-Nenurmend"
		birth_date = 771.1.1
		adm = 0
		dip = 5
		mil = 2
		female = yes
    }
	queen = {
 		name = "Sanderion"
		dynasty = "Ael-Nenurmend"
		birth_date = 762.1.1
		adm = 4
		dip = 4
		mil = 1
    }
}

880.1.1 = {
	monarch = {
 		name = "Coreyon"
		dynasty = "Wel-Celrith"
		birth_date = 833.1.1
		adm = 2
		dip = 3
		mil = 5
    }
	queen = {
 		name = "Angorwe"
		dynasty = "Wel-Celrith"
		birth_date = 849.1.1
		adm = 6
		dip = 2
		mil = 4
		female = yes
    }
	heir = {
 		name = "Caldandien"
		monarch_name = "Caldandien I"
		dynasty = "Wel-Celrith"
		birth_date = 877.1.1
		death_date = 974.1.1
		claim = 100
		adm = 3
		dip = 0
		mil = 3
    }
}

974.1.1 = {
	monarch = {
 		name = "Ohartalmo"
		dynasty = "Bel-Siriginia"
		birth_date = 930.1.1
		adm = 2
		dip = 4
		mil = 3
    }
	queen = {
 		name = "Maalamenwe"
		dynasty = "Bel-Siriginia"
		birth_date = 952.1.1
		adm = 2
		dip = 0
		mil = 4
		female = yes
    }
	heir = {
 		name = "Carindon"
		monarch_name = "Carindon I"
		dynasty = "Bel-Siriginia"
		birth_date = 967.1.1
		death_date = 1048.1.1
		claim = 100
		adm = 6
		dip = 6
		mil = 3
    }
}

1048.1.1 = {
	monarch = {
 		name = "Orninlur"
		dynasty = "Ael-Jaerfhaer"
		birth_date = 1018.1.1
		adm = 6
		dip = 2
		mil = 3
    }
	queen = {
 		name = "Angalsama"
		dynasty = "Ael-Jaerfhaer"
		birth_date = 1008.1.1
		adm = 2
		dip = 1
		mil = 2
		female = yes
    }
	heir = {
 		name = "Nirunar"
		monarch_name = "Nirunar I"
		dynasty = "Ael-Jaerfhaer"
		birth_date = 1035.1.1
		death_date = 1103.1.1
		claim = 100
		adm = 6
		dip = 6
		mil = 1
    }
}

1103.1.1 = {
	monarch = {
 		name = "Curvanen"
		dynasty = "Ael-Muulinde"
		birth_date = 1081.1.1
		adm = 2
		dip = 0
		mil = 4
    }
	queen = {
 		name = "Lorne"
		dynasty = "Ael-Muulinde"
		birth_date = 1058.1.1
		adm = 6
		dip = 6
		mil = 3
		female = yes
    }
	heir = {
 		name = "Arnarra"
		monarch_name = "Arnarra I"
		dynasty = "Ael-Muulinde"
		birth_date = 1099.1.1
		death_date = 1178.1.1
		claim = 100
		adm = 3
		dip = 5
		mil = 2
		female = yes
    }
}

1178.1.1 = {
	monarch = {
 		name = "Nonde"
		dynasty = "Wel-Estodith"
		birth_date = 1154.1.1
		adm = 6
		dip = 6
		mil = 4
		female = yes
    }
	queen = {
 		name = "Naranbar"
		dynasty = "Wel-Estodith"
		birth_date = 1126.1.1
		adm = 2
		dip = 4
		mil = 3
    }
}

1251.1.1 = {
	monarch = {
 		name = "Vincano"
		dynasty = "Bel-Virea"
		birth_date = 1210.1.1
		adm = 1
		dip = 4
		mil = 0
    }
}

1339.1.1 = {
	monarch = {
 		name = "Randor"
		dynasty = "Wel-Aelsoniane"
		birth_date = 1290.1.1
		adm = 6
		dip = 3
		mil = 3
    }
}

1381.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Cararilde"
		monarch_name = "Cararilde I"
		dynasty = "Wel-Edala"
		birth_date = 1367.1.1
		death_date = 1385.1.1
		claim = 100
		adm = 0
		dip = 0
		mil = 1
		female = yes
    }
}

1385.1.1 = {
	monarch = {
 		name = "Emderil"
		dynasty = "Wel-Cinrie"
		birth_date = 1348.1.1
		adm = 6
		dip = 3
		mil = 1
    }
}

1440.1.1 = {
	monarch = {
 		name = "Lorcalin"
		dynasty = "Wel-Edala"
		birth_date = 1415.1.1
		adm = 4
		dip = 2
		mil = 5
    }
}

1491.1.1 = {
	monarch = {
 		name = "Corerine"
		dynasty = "Bel-Spelluseus"
		birth_date = 1460.1.1
		adm = 3
		dip = 2
		mil = 2
		female = yes
    }
	queen = {
 		name = "Norion"
		dynasty = "Bel-Spelluseus"
		birth_date = 1467.1.1
		adm = 6
		dip = 0
		mil = 1
    }
	heir = {
 		name = "Earenwe"
		monarch_name = "Earenwe I"
		dynasty = "Bel-Spelluseus"
		birth_date = 1487.1.1
		death_date = 1534.1.1
		claim = 100
		adm = 3
		dip = 6
		mil = 0
    }
}

1534.1.1 = {
	monarch = {
 		name = "Ruliel"
		dynasty = "Ael-Minoirdalin"
		birth_date = 1511.1.1
		adm = 6
		dip = 0
		mil = 2
    }
}

1573.1.1 = {
	monarch = {
 		name = "Lithyyorion"
		dynasty = "Ael-Nalcrand"
		birth_date = 1554.1.1
		adm = 0
		dip = 4
		mil = 0
    }
}

1651.1.1 = {
	monarch = {
 		name = "Nemfarion"
		dynasty = "Ael-Jaerfhaer"
		birth_date = 1619.1.1
		adm = 5
		dip = 3
		mil = 4
    }
}

1732.1.1 = {
	monarch = {
 		name = "Glenadir"
		dynasty = "Ael-Niria"
		birth_date = 1700.1.1
		adm = 3
		dip = 3
		mil = 0
    }
}

1790.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Elanande"
		monarch_name = "Elanande I"
		dynasty = "Wel-Celrandil"
		birth_date = 1775.1.1
		death_date = 1793.1.1
		claim = 100
		adm = 4
		dip = 0
		mil = 5
		female = yes
    }
}

1793.1.1 = {
	monarch = {
 		name = "Tanerline"
		dynasty = "Wel-Ardende"
		birth_date = 1763.1.1
		adm = 0
		dip = 1
		mil = 1
		female = yes
    }
	queen = {
 		name = "Ruucaramil"
		dynasty = "Wel-Ardende"
		birth_date = 1757.1.1
		adm = 4
		dip = 0
		mil = 0
    }
}

1891.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Enginaire"
		monarch_name = "Enginaire I"
		dynasty = "Ael-Kardnar"
		birth_date = 1877.1.1
		death_date = 1895.1.1
		claim = 100
		adm = 4
		dip = 4
		mil = 4
		female = yes
    }
}

1895.1.1 = {
	monarch = {
 		name = "Tuinorion"
		dynasty = "Wel-Aelsoniane"
		birth_date = 1855.1.1
		adm = 4
		dip = 0
		mil = 5
    }
}

1977.1.1 = {
	monarch = {
 		name = "Nuulehtel"
		dynasty = "Ael-Kingdom"
		birth_date = 1952.1.1
		adm = 2
		dip = 6
		mil = 1
    }
}

2020.1.1 = {
	monarch = {
 		name = "Umbelmion"
		dynasty = "Wel-Ardende"
		birth_date = 1973.1.1
		adm = 6
		dip = 1
		mil = 1
    }
}

2056.1.1 = {
	monarch = {
 		name = "Laanestii"
		dynasty = "Wel-Alkinosin"
		birth_date = 2018.1.1
		adm = 1
		dip = 2
		mil = 3
		female = yes
    }
	queen = {
 		name = "Inganirnei"
		dynasty = "Wel-Alkinosin"
		birth_date = 2022.1.1
		adm = 1
		dip = 6
		mil = 4
    }
}

2119.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Menuldhel"
		monarch_name = "Menuldhel I"
		dynasty = "Bel-Sontarya"
		birth_date = 2113.1.1
		death_date = 2131.1.1
		claim = 100
		adm = 2
		dip = 3
		mil = 2
    }
}

2131.1.1 = {
	monarch = {
 		name = "Mornandil"
		dynasty = "Ael-Jaerfhaer"
		birth_date = 2112.1.1
		adm = 1
		dip = 6
		mil = 2
    }
}

2203.1.1 = {
	monarch = {
 		name = "Tanamo"
		dynasty = "Ael-Laraethfhaer"
		birth_date = 2168.1.1
		adm = 0
		dip = 6
		mil = 6
    }
}

2298.1.1 = {
	monarch = {
 		name = "Namornen"
		dynasty = "Wel-Cinrie"
		birth_date = 2256.1.1
		adm = 5
		dip = 5
		mil = 3
    }
}

2336.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Lantelcare"
		monarch_name = "Lantelcare I"
		dynasty = "Wel-Elsinthaer"
		birth_date = 2335.1.1
		death_date = 2353.1.1
		claim = 100
		adm = 6
		dip = 2
		mil = 0
		female = yes
    }
}

2353.1.1 = {
	monarch = {
 		name = "Arrimoril"
		dynasty = "Ael-Nenyfire"
		birth_date = 2308.1.1
		adm = 1
		dip = 3
		mil = 3
    }
}

2429.1.1 = {
	monarch = {
 		name = "Taarenyawen"
		dynasty = "Wel-Arannnarre"
		birth_date = 2404.1.1
		adm = 0
		dip = 2
		mil = 6
		female = yes
    }
	queen = {
 		name = "Rolumdel"
		dynasty = "Wel-Arannnarre"
		birth_date = 2408.1.1
		adm = 4
		dip = 1
		mil = 5
    }
	heir = {
 		name = "Sarandil"
		monarch_name = "Sarandil I"
		dynasty = "Wel-Arannnarre"
		birth_date = 2422.1.1
		death_date = 2474.1.1
		claim = 100
		adm = 0
		dip = 0
		mil = 4
    }
}

2474.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Angalmo"
		monarch_name = "Angalmo I"
		dynasty = "Bel-Siriginia"
		birth_date = 2463.1.1
		death_date = 2481.1.1
		claim = 100
		adm = 6
		dip = 6
		mil = 1
    }
}

2481.1.1 = {
	monarch = {
 		name = "Armion"
		dynasty = "Wel-Cinrie"
		birth_date = 2454.1.1
		adm = 5
		dip = 2
		mil = 1
    }
}

