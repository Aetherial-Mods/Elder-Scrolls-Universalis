government = monarchy
government_rank = 1
mercantilism = 1
technology_group = pyandonea_tg
religion = serpant_king
primary_culture = maormer
capital = 2085

54.1.1 = {
	monarch = {
 		name = "Chryinius"
		dynasty = "Nu-Zerstes"
		birth_date = 16.1.1
		adm = 2
		dip = 4
		mil = 4
    }
	queen = {
 		name = "Norania"
		dynasty = "Nu-Zerstes"
		birth_date = 1.1.1
		adm = 6
		dip = 3
		mil = 3
		female = yes
    }
	heir = {
 		name = "Camandar"
		monarch_name = "Camandar I"
		dynasty = "Nu-Zerstes"
		birth_date = 47.1.1
		death_date = 108.1.1
		claim = 100
		adm = 3
		dip = 1
		mil = 2
    }
}

108.1.1 = {
	monarch = {
 		name = "Normanyedil"
		dynasty = "Ne-Ishehyn"
		birth_date = 71.1.1
		adm = 5
		dip = 3
		mil = 4
    }
	queen = {
 		name = "Cinurmion"
		dynasty = "Ne-Ishehyn"
		birth_date = 76.1.1
		adm = 2
		dip = 1
		mil = 3
		female = yes
    }
	heir = {
 		name = "Vaeht"
		monarch_name = "Vaeht I"
		dynasty = "Ne-Ishehyn"
		birth_date = 103.1.1
		death_date = 171.1.1
		claim = 100
		adm = 6
		dip = 0
		mil = 2
		female = yes
    }
}

171.1.1 = {
	monarch = {
 		name = "Cecrasnil"
		dynasty = "Na-Chorrohirnuht"
		birth_date = 128.1.1
		adm = 2
		dip = 1
		mil = 4
    }
}

226.1.1 = {
	monarch = {
 		name = "Unwe"
		dynasty = "Na-Ospeht"
		birth_date = 188.1.1
		adm = 0
		dip = 0
		mil = 1
    }
}

314.1.1 = {
	monarch = {
 		name = "Parevdil"
		dynasty = "Ne-Ellan"
		birth_date = 286.1.1
		adm = 2
		dip = 2
		mil = 3
		female = yes
    }
}

377.1.1 = {
	monarch = {
 		name = "Jykion"
		dynasty = "Na-Himnalwiash"
		birth_date = 328.1.1
		adm = 0
		dip = 1
		mil = 6
    }
	queen = {
 		name = "Yryrna"
		dynasty = "Na-Himnalwiash"
		birth_date = 341.1.1
		adm = 4
		dip = 6
		mil = 5
		female = yes
    }
	heir = {
 		name = "Umhelel"
		monarch_name = "Umhelel I"
		dynasty = "Na-Himnalwiash"
		birth_date = 367.1.1
		death_date = 451.1.1
		claim = 100
		adm = 4
		dip = 3
		mil = 6
    }
}

451.1.1 = {
	monarch = {
 		name = "Jykoth"
		dynasty = "Ni-Vemruth"
		birth_date = 407.1.1
		adm = 4
		dip = 6
		mil = 0
    }
}

488.1.1 = {
	monarch = {
 		name = "Ieda"
		dynasty = "Ni-Eclalras"
		birth_date = 453.1.1
		adm = 6
		dip = 1
		mil = 2
		female = yes
    }
	queen = {
 		name = "Orgnum"
		dynasty = "Ni-Eclalras"
		birth_date = 454.1.1
		adm = 3
		dip = 6
		mil = 1
    }
	heir = {
 		name = "Heculoa"
		monarch_name = "Heculoa I"
		dynasty = "Ni-Eclalras"
		birth_date = 473.1.1
		death_date = 530.1.1
		claim = 100
		adm = 0
		dip = 5
		mil = 0
    }
}

530.1.1 = {
	monarch = {
 		name = "Ilarral"
		dynasty = "Na-Ospiso"
		birth_date = 492.1.1
		adm = 0
		dip = 1
		mil = 0
    }
	queen = {
 		name = "Ohlayrwen"
		dynasty = "Na-Ospiso"
		birth_date = 484.1.1
		adm = 3
		dip = 0
		mil = 6
		female = yes
    }
}

584.1.1 = {
	monarch = {
 		name = "Nenleroonoth"
		dynasty = "Nu-Eiloldoh"
		birth_date = 549.1.1
		adm = 2
		dip = 6
		mil = 3
    }
	queen = {
 		name = "Jahnidil"
		dynasty = "Nu-Eiloldoh"
		birth_date = 536.1.1
		adm = 6
		dip = 4
		mil = 2
		female = yes
    }
	heir = {
 		name = "Arstul"
		monarch_name = "Arstul I"
		dynasty = "Nu-Eiloldoh"
		birth_date = 584.1.1
		death_date = 643.1.1
		claim = 100
		adm = 6
		dip = 1
		mil = 3
    }
}

643.1.1 = {
	monarch = {
 		name = "Insri"
		dynasty = "Nu-Croslun"
		birth_date = 600.1.1
		adm = 5
		dip = 4
		mil = 3
		female = yes
    }
	queen = {
 		name = "Yrdron"
		dynasty = "Nu-Croslun"
		birth_date = 611.1.1
		adm = 2
		dip = 3
		mil = 2
    }
}

727.1.1 = {
	monarch = {
 		name = "Valaran"
		dynasty = "Nu-Nemriuth"
		birth_date = 694.1.1
		adm = 0
		dip = 2
		mil = 6
    }
}

787.1.1 = {
	monarch = {
 		name = "Ohavislel"
		dynasty = "Ne-Jyrreh"
		birth_date = 742.1.1
		adm = 6
		dip = 1
		mil = 2
    }
}

831.1.1 = {
	monarch = {
 		name = "Manmanse"
		dynasty = "Ni-Eclalras"
		birth_date = 794.1.1
		adm = 4
		dip = 0
		mil = 6
		female = yes
    }
}

870.1.1 = {
	monarch = {
 		name = "Alleria"
		dynasty = "Ni-Imneldi"
		birth_date = 850.1.1
		adm = 2
		dip = 0
		mil = 3
		female = yes
    }
}

929.1.1 = {
	monarch = {
 		name = "Vagun"
		dynasty = "Ni-Moclen"
		birth_date = 881.1.1
		adm = 0
		dip = 6
		mil = 6
    }
	queen = {
 		name = "Desnorwen"
		dynasty = "Ni-Moclen"
		birth_date = 904.1.1
		adm = 4
		dip = 4
		mil = 5
		female = yes
    }
	heir = {
 		name = "Jykion"
		monarch_name = "Jykion I"
		dynasty = "Ni-Moclen"
		birth_date = 922.1.1
		death_date = 980.1.1
		claim = 100
		adm = 1
		dip = 3
		mil = 4
    }
}

980.1.1 = {
	monarch = {
 		name = "Manmanse"
		dynasty = "Ne-Onlulwis"
		birth_date = 947.1.1
		adm = 0
		dip = 6
		mil = 4
		female = yes
    }
}

1066.1.1 = {
	monarch = {
 		name = "Unia"
		dynasty = "Ne-Dodunwado"
		birth_date = 1029.1.1
		adm = 6
		dip = 5
		mil = 1
		female = yes
    }
}

1130.1.1 = {
	monarch = {
 		name = "Ladesmerdo"
		dynasty = "Na-Junuth"
		birth_date = 1088.1.1
		adm = 4
		dip = 5
		mil = 5
    }
}

1179.1.1 = {
	monarch = {
 		name = "Isuneis"
		dynasty = "Na-Cenri"
		birth_date = 1133.1.1
		adm = 2
		dip = 4
		mil = 1
		female = yes
    }
}

1268.1.1 = {
	monarch = {
 		name = "Vayaatul"
		dynasty = "Na-Yorhasri"
		birth_date = 1220.1.1
		adm = 0
		dip = 3
		mil = 5
    }
	queen = {
 		name = "Monneieis"
		dynasty = "Na-Yorhasri"
		birth_date = 1239.1.1
		adm = 4
		dip = 2
		mil = 4
		female = yes
    }
}

1328.1.1 = {
	monarch = {
 		name = "Eilgun"
		dynasty = "Na-Othy"
		birth_date = 1301.1.1
		adm = 2
		dip = 1
		mil = 1
    }
	queen = {
 		name = "Ircecsri"
		dynasty = "Na-Othy"
		birth_date = 1308.1.1
		adm = 6
		dip = 6
		mil = 0
		female = yes
    }
	heir = {
 		name = "Unse"
		monarch_name = "Unse I"
		dynasty = "Na-Othy"
		birth_date = 1323.1.1
		death_date = 1392.1.1
		claim = 100
		adm = 3
		dip = 5
		mil = 5
		female = yes
    }
}

1392.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Vayaatul"
		monarch_name = "Vayaatul I"
		dynasty = "Nu-Qyhmesoth"
		birth_date = 1377.1.1
		death_date = 1395.1.1
		claim = 100
		adm = 1
		dip = 4
		mil = 2
    }
}

1395.1.1 = {
	monarch = {
 		name = "Linsar"
		dynasty = "Ni-Zuumoas"
		birth_date = 1373.1.1
		adm = 1
		dip = 0
		mil = 2
    }
}

1490.1.1 = {
	monarch = {
 		name = "Eilgun"
		dynasty = "Ni-Zuumoas"
		birth_date = 1446.1.1
		adm = 6
		dip = 0
		mil = 6
    }
}

1541.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Orgnum"
		monarch_name = "Orgnum I"
		dynasty = "Nu-Qakhasa"
		birth_date = 1534.1.1
		death_date = 1552.1.1
		claim = 100
		adm = 3
		dip = 2
		mil = 6
    }
}

1552.1.1 = {
	monarch = {
 		name = "Gulcarmemo"
		dynasty = "Na-Arhylwassen"
		birth_date = 1512.1.1
		adm = 3
		dip = 5
		mil = 6
    }
	queen = {
 		name = "Ilciia"
		dynasty = "Na-Arhylwassen"
		birth_date = 1504.1.1
		adm = 6
		dip = 4
		mil = 5
		female = yes
    }
}

1610.1.1 = {
	monarch = {
 		name = "Merlolos"
		dynasty = "Ni-Ilgomeddoa"
		birth_date = 1589.1.1
		adm = 5
		dip = 3
		mil = 2
    }
}

1700.1.1 = {
	monarch = {
 		name = "Desohcana"
		dynasty = "Na-Enluh"
		birth_date = 1659.1.1
		adm = 3
		dip = 2
		mil = 6
		female = yes
    }
	queen = {
 		name = "Oonnum"
		dynasty = "Na-Enluh"
		birth_date = 1681.1.1
		adm = 0
		dip = 1
		mil = 5
    }
	heir = {
 		name = "Isnencamia"
		monarch_name = "Isnencamia I"
		dynasty = "Na-Enluh"
		birth_date = 1687.1.1
		death_date = 1753.1.1
		claim = 100
		adm = 4
		dip = 6
		mil = 4
		female = yes
    }
}

1753.1.1 = {
	monarch = {
 		name = "Gulsha"
		dynasty = "Na-Puhlynvurnia"
		birth_date = 1702.1.1
		adm = 6
		dip = 0
		mil = 6
		female = yes
    }
	queen = {
 		name = "Pannenvissar"
		dynasty = "Na-Puhlynvurnia"
		birth_date = 1700.1.1
		adm = 3
		dip = 6
		mil = 5
    }
	heir = {
 		name = "Anlaadil"
		monarch_name = "Anlaadil I"
		dynasty = "Na-Puhlynvurnia"
		birth_date = 1744.1.1
		death_date = 1818.1.1
		claim = 100
		adm = 0
		dip = 5
		mil = 4
		female = yes
    }
}

1818.1.1 = {
	monarch = {
 		name = "Desohcana"
		dynasty = "Na-Himnalwiash"
		birth_date = 1788.1.1
		adm = 0
		dip = 1
		mil = 4
		female = yes
    }
	queen = {
 		name = "Dairtil"
		dynasty = "Na-Himnalwiash"
		birth_date = 1784.1.1
		adm = 0
		dip = 4
		mil = 5
    }
	heir = {
 		name = "Rirlel"
		monarch_name = "Rirlel I"
		dynasty = "Na-Himnalwiash"
		birth_date = 1815.1.1
		death_date = 1857.1.1
		claim = 100
		adm = 4
		dip = 3
		mil = 4
    }
}

1857.1.1 = {
	monarch = {
 		name = "Iphilinmanius"
		dynasty = "Na-Chorrohirnuht"
		birth_date = 1816.1.1
		adm = 3
		dip = 6
		mil = 4
    }
	queen = {
 		name = "Memanwen"
		dynasty = "Na-Chorrohirnuht"
		birth_date = 1823.1.1
		adm = 0
		dip = 5
		mil = 3
		female = yes
    }
	heir = {
 		name = "Hetnorlinmo"
		monarch_name = "Hetnorlinmo I"
		dynasty = "Na-Chorrohirnuht"
		birth_date = 1842.1.1
		death_date = 1924.1.1
		claim = 100
		adm = 4
		dip = 3
		mil = 2
    }
}

1924.1.1 = {
	monarch = {
 		name = "Tyhewe"
		dynasty = "Ni-Zonrel"
		birth_date = 1884.1.1
		adm = 0
		dip = 5
		mil = 5
    }
	queen = {
 		name = "Paror"
		dynasty = "Ni-Zonrel"
		birth_date = 1903.1.1
		adm = 3
		dip = 3
		mil = 4
		female = yes
    }
	heir = {
 		name = "Rirlel"
		monarch_name = "Rirlel I"
		dynasty = "Ni-Zonrel"
		birth_date = 1911.1.1
		death_date = 1991.1.1
		claim = 100
		adm = 0
		dip = 2
		mil = 3
    }
}

1991.1.1 = {
	monarch = {
 		name = "Iphilinmanius"
		dynasty = "Ne-Vorrullulio"
		birth_date = 1950.1.1
		adm = 3
		dip = 3
		mil = 5
    }
	queen = {
 		name = "Oondir"
		dynasty = "Ne-Vorrullulio"
		birth_date = 1954.1.1
		adm = 0
		dip = 2
		mil = 4
		female = yes
    }
}

2038.1.1 = {
	monarch = {
 		name = "Norevalion"
		dynasty = "Nu-Cracor"
		birth_date = 2000.1.1
		adm = 5
		dip = 1
		mil = 1
    }
}

2105.1.1 = {
	monarch = {
 		name = "Jahuuyetil"
		dynasty = "Nu-Riklolneva"
		birth_date = 2077.1.1
		adm = 3
		dip = 0
		mil = 4
    }
	queen = {
 		name = "Arslana"
		dynasty = "Nu-Riklolneva"
		birth_date = 2068.1.1
		adm = 0
		dip = 6
		mil = 3
		female = yes
    }
	heir = {
 		name = "Uldor"
		monarch_name = "Uldor I"
		dynasty = "Nu-Riklolneva"
		birth_date = 2103.1.1
		death_date = 2188.1.1
		claim = 100
		adm = 4
		dip = 4
		mil = 2
    }
}

2188.1.1 = {
	monarch = {
 		name = "Uuvasha"
		dynasty = "Na-Cheehle"
		birth_date = 2162.1.1
		adm = 4
		dip = 1
		mil = 2
		female = yes
    }
}

2243.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Isardor"
		monarch_name = "Isardor I"
		dynasty = "Na-Notshonurly"
		birth_date = 2231.1.1
		death_date = 2249.1.1
		claim = 100
		adm = 1
		dip = 3
		mil = 2
    }
}

2249.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Camandar"
		monarch_name = "Camandar I"
		dynasty = "Ni-Cenvah"
		birth_date = 2240.1.1
		death_date = 2258.1.1
		claim = 100
		adm = 3
		dip = 4
		mil = 4
    }
}

2258.1.1 = {
	monarch = {
 		name = "Norrasdar"
		dynasty = "Ne-Eilmenwirweis"
		birth_date = 2227.1.1
		adm = 5
		dip = 5
		mil = 6
    }
}

2293.1.1 = {
	monarch = {
 		name = "Lase"
		dynasty = "Nu-Zomroar"
		birth_date = 2270.1.1
		adm = 4
		dip = 4
		mil = 3
		female = yes
    }
}

2382.1.1 = {
	monarch = {
 		name = "Virindi"
		dynasty = "Na-Junuth"
		birth_date = 2332.1.1
		adm = 2
		dip = 4
		mil = 0
		female = yes
    }
	queen = {
 		name = "Arsoth"
		dynasty = "Na-Junuth"
		birth_date = 2358.1.1
		adm = 6
		dip = 2
		mil = 6
    }
}

2427.1.1 = {
	monarch = {
 		name = "Larnil"
		dynasty = "Nu-Ranvu"
		birth_date = 2403.1.1
		adm = 4
		dip = 1
		mil = 2
    }
	queen = {
 		name = "Pelrasanse"
		dynasty = "Nu-Ranvu"
		birth_date = 2400.1.1
		adm = 1
		dip = 0
		mil = 1
		female = yes
    }
	heir = {
 		name = "Jykion"
		monarch_name = "Jykion I"
		dynasty = "Nu-Ranvu"
		birth_date = 2413.1.1
		death_date = 2484.1.1
		claim = 100
		adm = 5
		dip = 5
		mil = 0
    }
}

2484.1.1 = {
	monarch = {
 		name = "Viscarne"
		dynasty = "Ne-Chomnath"
		birth_date = 2431.1.1
		adm = 4
		dip = 2
		mil = 0
    }
	queen = {
 		name = "Caorgia"
		dynasty = "Ne-Chomnath"
		birth_date = 2438.1.1
		adm = 4
		dip = 5
		mil = 2
		female = yes
    }
	heir = {
 		name = "Jykoth"
		monarch_name = "Jykoth I"
		dynasty = "Ne-Chomnath"
		birth_date = 2475.1.1
		death_date = 2523.1.1
		claim = 100
		adm = 1
		dip = 4
		mil = 0
    }
}

