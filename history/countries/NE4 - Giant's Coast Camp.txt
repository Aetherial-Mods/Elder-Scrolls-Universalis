government = native
government_rank = 1
mercantilism = 1
technology_group = northern_tg
religion = cult_of_ancestors
primary_culture = sea_giant
capital = 1347

54.1.1 = {
	monarch = {
 		name = "Zumbrimia"
		dynasty = "Griglas"
		birth_date = 7.1.1
		adm = 4
		dip = 0
		mil = 1
		female = yes
    }
}

108.1.1 = {
	monarch = {
 		name = "Xulotos"
		dynasty = "Mallid"
		birth_date = 84.1.1
		adm = 2
		dip = 6
		mil = 4
    }
}

174.1.1 = {
	monarch = {
 		name = "Vagnome"
		dynasty = "Zetril"
		birth_date = 156.1.1
		adm = 1
		dip = 6
		mil = 1
		female = yes
    }
}

219.1.1 = {
	monarch = {
 		name = "Chrincor"
		dynasty = "Sunges"
		birth_date = 168.1.1
		adm = 3
		dip = 0
		mil = 2
    }
}

272.1.1 = {
	monarch = {
 		name = "Riserus"
		dynasty = "Bleglil"
		birth_date = 223.1.1
		adm = 1
		dip = 6
		mil = 6
    }
}

329.1.1 = {
	monarch = {
 		name = "Zara"
		dynasty = "Lebsok"
		birth_date = 305.1.1
		adm = 6
		dip = 5
		mil = 3
		female = yes
    }
}

385.1.1 = {
	monarch = {
 		name = "Chalcotos"
		dynasty = "Birgem"
		birth_date = 358.1.1
		adm = 4
		dip = 5
		mil = 6
    }
}

422.1.1 = {
	monarch = {
 		name = "Chrasaon"
		dynasty = "Chillix"
		birth_date = 393.1.1
		adm = 3
		dip = 4
		mil = 3
    }
}

482.1.1 = {
	monarch = {
 		name = "Daelon"
		dynasty = "Jobsod"
		birth_date = 434.1.1
		adm = 1
		dip = 3
		mil = 0
    }
}

543.1.1 = {
	monarch = {
 		name = "Zara"
		dynasty = "Kesce"
		birth_date = 510.1.1
		adm = 6
		dip = 2
		mil = 3
		female = yes
    }
}

599.1.1 = {
	monarch = {
 		name = "Thusante"
		dynasty = "Krungax"
		birth_date = 562.1.1
		adm = 4
		dip = 1
		mil = 0
		female = yes
    }
}

644.1.1 = {
	monarch = {
 		name = "Chrasaon"
		dynasty = "Logmuwr"
		birth_date = 611.1.1
		adm = 6
		dip = 3
		mil = 2
    }
}

712.1.1 = {
	monarch = {
 		name = "Daelon"
		dynasty = "Bleglil"
		birth_date = 681.1.1
		adm = 4
		dip = 2
		mil = 5
    }
}

749.1.1 = {
	monarch = {
 		name = "Vilamea"
		dynasty = "Kesce"
		birth_date = 730.1.1
		adm = 3
		dip = 1
		mil = 2
		female = yes
    }
}

815.1.1 = {
	monarch = {
 		name = "Zamaestus"
		dynasty = "Byshish"
		birth_date = 784.1.1
		adm = 1
		dip = 0
		mil = 6
    }
}

908.1.1 = {
	monarch = {
 		name = "Xiarroeus"
		dynasty = "Krugneg"
		birth_date = 889.1.1
		adm = 6
		dip = 6
		mil = 2
    }
}

998.1.1 = {
	monarch = {
 		name = "Muvibos"
		dynasty = "Scostem"
		birth_date = 962.1.1
		adm = 4
		dip = 6
		mil = 6
    }
}

1093.1.1 = {
	monarch = {
 		name = "Voses"
		dynasty = "Kagram"
		birth_date = 1069.1.1
		adm = 3
		dip = 5
		mil = 2
    }
}

1146.1.1 = {
	monarch = {
 		name = "Daelon"
		dynasty = "Birgem"
		birth_date = 1116.1.1
		adm = 1
		dip = 4
		mil = 6
    }
}

1182.1.1 = {
	monarch = {
 		name = "Hispovo"
		dynasty = "Gugnak"
		birth_date = 1140.1.1
		adm = 3
		dip = 5
		mil = 1
		female = yes
    }
}

1256.1.1 = {
	monarch = {
 		name = "Chura"
		dynasty = "Lykowr"
		birth_date = 1227.1.1
		adm = 1
		dip = 5
		mil = 4
		female = yes
    }
}

1304.1.1 = {
	monarch = {
 		name = "Noneas"
		dynasty = "Mynryx"
		birth_date = 1279.1.1
		adm = 6
		dip = 4
		mil = 1
    }
}

1348.1.1 = {
	monarch = {
 		name = "Vilo"
		dynasty = "Cratrog"
		birth_date = 1306.1.1
		adm = 5
		dip = 3
		mil = 5
    }
}

1423.1.1 = {
	monarch = {
 		name = "Zyndrora"
		dynasty = "Jiskam"
		birth_date = 1383.1.1
		adm = 3
		dip = 2
		mil = 1
		female = yes
    }
}

1515.1.1 = {
	monarch = {
 		name = "Zonoeus"
		dynasty = "Kakla"
		birth_date = 1483.1.1
		adm = 1
		dip = 1
		mil = 5
    }
}

1553.1.1 = {
	monarch = {
 		name = "Doscias"
		dynasty = "Kesce"
		birth_date = 1532.1.1
		adm = 0
		dip = 5
		mil = 5
    }
}

1599.1.1 = {
	monarch = {
 		name = "Zaizoro"
		dynasty = "Glerkosh"
		birth_date = 1573.1.1
		adm = 6
		dip = 4
		mil = 2
		female = yes
    }
}

1675.1.1 = {
	monarch = {
 		name = "Theda"
		dynasty = "Grorkywr"
		birth_date = 1650.1.1
		adm = 4
		dip = 3
		mil = 5
		female = yes
    }
}

1756.1.1 = {
	monarch = {
 		name = "Riderion"
		dynasty = "Bludril"
		birth_date = 1711.1.1
		adm = 2
		dip = 2
		mil = 2
    }
}

1853.1.1 = {
	monarch = {
 		name = "Resco"
		dynasty = "Troscik"
		birth_date = 1833.1.1
		adm = 0
		dip = 2
		mil = 6
    }
}

1919.1.1 = {
	monarch = {
 		name = "Noser"
		dynasty = "Cratrog"
		birth_date = 1898.1.1
		adm = 6
		dip = 1
		mil = 2
    }
}

1957.1.1 = {
	monarch = {
 		name = "Vinius"
		dynasty = "Birgem"
		birth_date = 1933.1.1
		adm = 4
		dip = 0
		mil = 6
    }
}

2010.1.1 = {
	monarch = {
 		name = "Riderion"
		dynasty = "Kallog"
		birth_date = 1990.1.1
		adm = 6
		dip = 1
		mil = 1
    }
}

2083.1.1 = {
	monarch = {
 		name = "Resco"
		dynasty = "Scostem"
		birth_date = 2037.1.1
		adm = 4
		dip = 0
		mil = 4
    }
}

2159.1.1 = {
	monarch = {
 		name = "Vaiktaios"
		dynasty = "Jakral"
		birth_date = 2136.1.1
		adm = 2
		dip = 0
		mil = 1
    }
}

2238.1.1 = {
	monarch = {
 		name = "Xuzezea"
		dynasty = "Goki"
		birth_date = 2185.1.1
		adm = 1
		dip = 6
		mil = 4
		female = yes
    }
}

2326.1.1 = {
	monarch = {
 		name = "Chusea"
		dynasty = "Mendan"
		birth_date = 2280.1.1
		adm = 6
		dip = 5
		mil = 1
		female = yes
    }
}

2383.1.1 = {
	monarch = {
 		name = "Zestates"
		dynasty = "Kogmir"
		birth_date = 2363.1.1
		adm = 4
		dip = 4
		mil = 5
    }
}

2425.1.1 = {
	monarch = {
 		name = "Vaiktaios"
		dynasty = "Lidrur"
		birth_date = 2384.1.1
		adm = 2
		dip = 4
		mil = 1
    }
}

2489.1.1 = {
	monarch = {
 		name = "Thusneus"
		dynasty = "Jastes"
		birth_date = 2460.1.1
		adm = 1
		dip = 3
		mil = 5
    }
}

