government = republic
government_rank = 3
mercantilism = 1
technology_group = cyrodiil_tg
religion = molag_bal_cult
primary_culture = ayleid
capital = 1161

54.1.1 = {
	monarch = {
 		name = "Cylyris"
		dynasty = "Vae-Trumbe"
		birth_date = 32.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

149.1.1 = {
	monarch = {
 		name = "Umarik"
		dynasty = "Mea-Dusok"
		birth_date = 102.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

221.1.1 = {
	monarch = {
 		name = "Calinden"
		dynasty = "Ula-Larilatia"
		birth_date = 175.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

306.1.1 = {
	monarch = {
 		name = "Hasind"
		dynasty = "		"
		birth_date = 260.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

368.1.1 = {
	monarch = {
 		name = "Ryland"
		dynasty = "Vae-Silorn"
		birth_date = 338.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

458.1.1 = {
	monarch = {
 		name = "Tjimdymegi"
		dynasty = "Vae-Sylolvia"
		birth_date = 426.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

551.1.1 = {
	monarch = {
 		name = "Lannessa"
		dynasty = "Ula-Matuseius"
		birth_date = 528.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

608.1.1 = {
	monarch = {
 		name = "Nulynhi"
		dynasty = "Mea-Andrulusus"
		birth_date = 578.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

654.1.1 = {
	monarch = {
 		name = "Dalin"
		dynasty = "Vae-Sardavar"
		birth_date = 601.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

695.1.1 = {
	monarch = {
 		name = "Sezenhuvo"
		dynasty = "Ula-Messiena"
		birth_date = 648.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

772.1.1 = {
	monarch = {
 		name = "Hilli"
		dynasty = "Mea-Hastrel"
		birth_date = 724.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

861.1.1 = {
	monarch = {
 		name = "Heystalero"
		dynasty = "Vae-Vandiand"
		birth_date = 839.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

897.1.1 = {
	monarch = {
 		name = "Ceyran"
		dynasty = "Vae-Stirk"
		birth_date = 872.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

961.1.1 = {
	monarch = {
 		name = "Yve"
		dynasty = "Mea-Hastrel"
		birth_date = 940.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1025.1.1 = {
	monarch = {
 		name = "Hegint"
		dynasty = "Mea-Corannus"
		birth_date = 981.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1114.1.1 = {
	monarch = {
 		name = "Ymmu"
		dynasty = "Ula-Larilatia"
		birth_date = 1086.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1203.1.1 = {
	monarch = {
 		name = "Illarrymo"
		dynasty = "Ula-Oressinia"
		birth_date = 1177.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1248.1.1 = {
	monarch = {
 		name = "Yve"
		dynasty = "Ula-Rusifus"
		birth_date = 1221.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

1340.1.1 = {
	monarch = {
 		name = "Glinferen"
		dynasty = "Ula-Ryndenyse"
		birth_date = 1298.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1394.1.1 = {
	monarch = {
 		name = "Inhuagadom"
		dynasty = "Ula-Nenyond"
		birth_date = 1368.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1448.1.1 = {
	monarch = {
 		name = "Nas"
		dynasty = "Mea-Arriastae"
		birth_date = 1405.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1513.1.1 = {
	monarch = {
 		name = "Cam"
		dynasty = "Vae-Vassorman"
		birth_date = 1460.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1560.1.1 = {
	monarch = {
 		name = "Glinferen"
		dynasty = "Ula-Messiena"
		birth_date = 1519.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1654.1.1 = {
	monarch = {
 		name = "Inhuagadom"
		dynasty = "Vae-Waelori"
		birth_date = 1617.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1733.1.1 = {
	monarch = {
 		name = "Vurel"
		dynasty = "Vae-Sepades"
		birth_date = 1713.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1808.1.1 = {
	monarch = {
 		name = "Rirgurhand"
		dynasty = "Vae-Weatherleah"
		birth_date = 1771.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1864.1.1 = {
	monarch = {
 		name = "Lagan"
		dynasty = "Ula-Linchal"
		birth_date = 1823.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1951.1.1 = {
	monarch = {
 		name = "Dalin"
		dynasty = "Vae-Vanua"
		birth_date = 1933.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2014.1.1 = {
	monarch = {
 		name = "Cordu"
		dynasty = "Vae-Weatherleah"
		birth_date = 1969.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2102.1.1 = {
	monarch = {
 		name = "Anumaril"
		dynasty = "Mea-Conoa"
		birth_date = 2062.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2140.1.1 = {
	monarch = {
 		name = "Lagan"
		dynasty = "Ula-Latara"
		birth_date = 2122.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2197.1.1 = {
	monarch = {
 		name = "Nivi"
		dynasty = "Vae-Weyandawik"
		birth_date = 2174.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

2232.1.1 = {
	monarch = {
 		name = "Yve"
		dynasty = "Vae-Vortia"
		birth_date = 2184.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

2278.1.1 = {
	monarch = {
 		name = "Hegint"
		dynasty = "Ula-Julidonea"
		birth_date = 2256.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2360.1.1 = {
	monarch = {
 		name = "Heystalero"
		dynasty = "Ula-Nenyond"
		birth_date = 2328.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2427.1.1 = {
	monarch = {
 		name = "Cenedelin"
		dynasty = "Vae-Silelia"
		birth_date = 2408.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

