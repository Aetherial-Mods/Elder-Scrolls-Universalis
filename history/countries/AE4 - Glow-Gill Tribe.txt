government = native
government_rank = 1
mercantilism = 1
technology_group = marshan_tg
religion = hist
primary_culture = agaceph
capital = 6797

54.1.1 = {
	monarch = {
 		name = "Muranatepa"
		dynasty = "Theermarush"
		birth_date = 21.1.1
		adm = 5
		dip = 5
		mil = 5
    }
}

139.1.1 = {
	monarch = {
 		name = "Geeh-Lurz"
		dynasty = "Miloeeja"
		birth_date = 121.1.1
		adm = 3
		dip = 4
		mil = 2
    }
}

176.1.1 = {
	monarch = {
 		name = "Az-Muz"
		dynasty = "Teegeixth"
		birth_date = 156.1.1
		adm = 1
		dip = 3
		mil = 5
    }
}

240.1.1 = {
	monarch = {
 		name = "Teeba-Deesei"
		dynasty = "Cascalees"
		birth_date = 187.1.1
		adm = 0
		dip = 2
		mil = 2
		female = yes
    }
}

334.1.1 = {
	monarch = {
 		name = "Narnum"
		dynasty = "Antigoneeixth"
		birth_date = 305.1.1
		adm = 1
		dip = 4
		mil = 3
		female = yes
    }
}

422.1.1 = {
	monarch = {
 		name = "Gam-Kur"
		dynasty = "Silm-Na"
		birth_date = 370.1.1
		adm = 0
		dip = 3
		mil = 0
    }
}

481.1.1 = {
	monarch = {
 		name = "Nazuux"
		dynasty = "Endoremus"
		birth_date = 436.1.1
		adm = 5
		dip = 2
		mil = 4
    }
}

558.1.1 = {
	monarch = {
 		name = "Gom-Tulm"
		dynasty = "Meeros"
		birth_date = 533.1.1
		adm = 3
		dip = 1
		mil = 0
    }
}

612.1.1 = {
	monarch = {
 		name = "Bahtei"
		dynasty = "Mereemesh"
		birth_date = 580.1.1
		adm = 1
		dip = 0
		mil = 4
    }
}

696.1.1 = {
	monarch = {
 		name = "Topeeth-Gih"
		dynasty = "Androdes"
		birth_date = 665.1.1
		adm = 0
		dip = 0
		mil = 1
    }
}

760.1.1 = {
	monarch = {
 		name = "Naxaltan"
		dynasty = "Taierlures"
		birth_date = 716.1.1
		adm = 5
		dip = 6
		mil = 4
    }
}

835.1.1 = {
	monarch = {
 		name = "Ilas-Betu"
		dynasty = "Andreecalees"
		birth_date = 787.1.1
		adm = 3
		dip = 5
		mil = 1
		female = yes
    }
}

894.1.1 = {
	monarch = {
 		name = "Bahrei"
		dynasty = "Menes"
		birth_date = 850.1.1
		adm = 5
		dip = 6
		mil = 3
    }
}

976.1.1 = {
	monarch = {
 		name = "Terameen"
		dynasty = "Neethsareeth"
		birth_date = 943.1.1
		adm = 3
		dip = 6
		mil = 6
		female = yes
    }
}

1051.1.1 = {
	monarch = {
 		name = "Baxilt-Gah"
		dynasty = "Caligeeja"
		birth_date = 1022.1.1
		adm = 2
		dip = 5
		mil = 3
    }
}

1111.1.1 = {
	monarch = {
 		name = "Tsoxolza"
		dynasty = "Saliz'k"
		birth_date = 1086.1.1
		adm = 0
		dip = 4
		mil = 6
    }
}

1201.1.1 = {
	monarch = {
 		name = "Nojaxia"
		dynasty = "Mahekus"
		birth_date = 1153.1.1
		adm = 5
		dip = 3
		mil = 3
		female = yes
    }
}

1289.1.1 = {
	monarch = {
 		name = "Jaree-Li"
		dynasty = "Pethees"
		birth_date = 1262.1.1
		adm = 3
		dip = 2
		mil = 0
		female = yes
    }
}

1330.1.1 = {
	monarch = {
 		name = "Batuus"
		dynasty = "Huzdeek"
		birth_date = 1299.1.1
		adm = 2
		dip = 2
		mil = 3
    }
}

1425.1.1 = {
	monarch = {
 		name = "Tsaesh"
		dynasty = "Casdorus"
		birth_date = 1401.1.1
		adm = 0
		dip = 1
		mil = 0
		female = yes
    }
}

1463.1.1 = {
	monarch = {
 		name = "Nijuna"
		dynasty = "Yelei"
		birth_date = 1412.1.1
		adm = 2
		dip = 2
		mil = 2
		female = yes
    }
}

1560.1.1 = {
	monarch = {
 		name = "Itzaki"
		dynasty = "Peteus"
		birth_date = 1508.1.1
		adm = 0
		dip = 1
		mil = 5
		female = yes
    }
}

1622.1.1 = {
	monarch = {
 		name = "Nuleem-Malem"
		dynasty = "Mahekus"
		birth_date = 1599.1.1
		adm = 5
		dip = 0
		mil = 2
		female = yes
    }
}

1696.1.1 = {
	monarch = {
 		name = "Hikathus"
		dynasty = "Im-Kajin"
		birth_date = 1655.1.1
		adm = 3
		dip = 0
		mil = 6
    }
}

1735.1.1 = {
	monarch = {
 		name = "Buki"
		dynasty = "Huzdeek"
		birth_date = 1707.1.1
		adm = 2
		dip = 6
		mil = 2
		female = yes
    }
}

1797.1.1 = {
	monarch = {
 		name = "Ukaspa"
		dynasty = "Magsareeth"
		birth_date = 1771.1.1
		adm = 0
		dip = 5
		mil = 6
    }
}

1865.1.1 = {
	monarch = {
 		name = "Nomu"
		dynasty = "Sakeides"
		birth_date = 1819.1.1
		adm = 5
		dip = 4
		mil = 2
    }
}

1931.1.1 = {
	monarch = {
 		name = "Heir-Marza"
		dynasty = "Taierlures"
		birth_date = 1891.1.1
		adm = 3
		dip = 3
		mil = 6
    }
}

1983.1.1 = {
	monarch = {
 		name = "Benas"
		dynasty = "Canimesh"
		birth_date = 1964.1.1
		adm = 5
		dip = 5
		mil = 1
    }
}

2044.1.1 = {
	monarch = {
 		name = "Tuxo"
		dynasty = "Canimesh"
		birth_date = 2008.1.1
		adm = 4
		dip = 4
		mil = 4
    }
}

2134.1.1 = {
	monarch = {
 		name = "Bun-Maxath"
		dynasty = "Effe-Shei"
		birth_date = 2087.1.1
		adm = 2
		dip = 3
		mil = 1
    }
}

2193.1.1 = {
	monarch = {
 		name = "Utazaw"
		dynasty = "Casmareen"
		birth_date = 2161.1.1
		adm = 0
		dip = 2
		mil = 5
		female = yes
    }
}

2289.1.1 = {
	monarch = {
 		name = "Oleed-Meen"
		dynasty = "Antigoneeixth"
		birth_date = 2248.1.1
		adm = 5
		dip = 2
		mil = 1
		female = yes
    }
}

2333.1.1 = {
	monarch = {
 		name = "Jukka"
		dynasty = "Lafnaresh"
		birth_date = 2298.1.1
		adm = 4
		dip = 1
		mil = 5
		female = yes
    }
}

2389.1.1 = {
	monarch = {
 		name = "Chaneej"
		dynasty = "An-Wulm"
		birth_date = 2360.1.1
		adm = 2
		dip = 0
		mil = 2
		female = yes
    }
}

2470.1.1 = {
	monarch = {
 		name = "Uraz"
		dynasty = "Peteus"
		birth_date = 2430.1.1
		adm = 0
		dip = 6
		mil = 5
    }
}

