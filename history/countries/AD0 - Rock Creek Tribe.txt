government = native
government_rank = 1
mercantilism = 1
technology_group = marshan_tg
religion = hist
primary_culture = agaceph
capital = 6787

54.1.1 = {
	monarch = {
 		name = "Azri"
		dynasty = "Eleedal-Teeus"
		birth_date = 18.1.1
		adm = 1
		dip = 0
		mil = 2
    }
}

120.1.1 = {
	monarch = {
 		name = "Tikkus"
		dynasty = "Bun-Neeus"
		birth_date = 86.1.1
		adm = 0
		dip = 6
		mil = 6
    }
}

198.1.1 = {
	monarch = {
 		name = "Neeka"
		dynasty = "An-Wulm"
		birth_date = 158.1.1
		adm = 5
		dip = 5
		mil = 2
		female = yes
    }
}

258.1.1 = {
	monarch = {
 		name = "Ghelos"
		dynasty = "Jeeraz"
		birth_date = 221.1.1
		adm = 3
		dip = 4
		mil = 6
    }
}

302.1.1 = {
	monarch = {
 		name = "Azinar"
		dynasty = "Neethcalees"
		birth_date = 272.1.1
		adm = 1
		dip = 3
		mil = 3
    }
}

377.1.1 = {
	monarch = {
 		name = "Tikaasi"
		dynasty = "Ushureel"
		birth_date = 324.1.1
		adm = 3
		dip = 5
		mil = 4
    }
}

445.1.1 = {
	monarch = {
 		name = "Mush-Shei"
		dynasty = "Neethcalees"
		birth_date = 400.1.1
		adm = 2
		dip = 4
		mil = 1
    }
}

496.1.1 = {
	monarch = {
 		name = "Tludul"
		dynasty = "Taierleesh"
		birth_date = 460.1.1
		adm = 0
		dip = 3
		mil = 5
		female = yes
    }
}

537.1.1 = {
	monarch = {
 		name = "Neeti-Nur"
		dynasty = "Pehrsar"
		birth_date = 504.1.1
		adm = 5
		dip = 2
		mil = 1
    }
}

621.1.1 = {
	monarch = {
 		name = "Gulum-La"
		dynasty = "Andreecalees"
		birth_date = 573.1.1
		adm = 3
		dip = 2
		mil = 5
    }
}

706.1.1 = {
	monarch = {
 		name = "Bar-Neeus"
		dynasty = "Wanan-Dar"
		birth_date = 683.1.1
		adm = 2
		dip = 1
		mil = 1
    }
}

781.1.1 = {
	monarch = {
 		name = "Tsatva-Lan"
		dynasty = "Yelnneen"
		birth_date = 734.1.1
		adm = 0
		dip = 0
		mil = 5
    }
}

824.1.1 = {
	monarch = {
 		name = "Neelo"
		dynasty = "Meeros"
		birth_date = 787.1.1
		adm = 5
		dip = 6
		mil = 2
    }
}

877.1.1 = {
	monarch = {
 		name = "Guleesh"
		dynasty = "Pergoulus"
		birth_date = 846.1.1
		adm = 0
		dip = 0
		mil = 3
    }
}

970.1.1 = {
	monarch = {
 		name = "Beem-Jee"
		dynasty = "Androdes"
		birth_date = 928.1.1
		adm = 5
		dip = 0
		mil = 0
		female = yes
    }
}

1006.1.1 = {
	monarch = {
 		name = "Heetzasi"
		dynasty = "Pethees"
		birth_date = 971.1.1
		adm = 3
		dip = 6
		mil = 4
    }
}

1064.1.1 = {
	monarch = {
 		name = "Beenalz"
		dynasty = "Deeureel"
		birth_date = 1030.1.1
		adm = 2
		dip = 5
		mil = 0
    }
}

1144.1.1 = {
	monarch = {
 		name = "Tunbam-Kahz"
		dynasty = "Neethcalees"
		birth_date = 1107.1.1
		adm = 0
		dip = 4
		mil = 4
    }
}

1238.1.1 = {
	monarch = {
 		name = "Netapatuu"
		dynasty = "Miloeeja"
		birth_date = 1219.1.1
		adm = 5
		dip = 3
		mil = 1
    }
}

1290.1.1 = {
	monarch = {
 		name = "Heek-Dimik"
		dynasty = "Canision"
		birth_date = 1268.1.1
		adm = 3
		dip = 3
		mil = 4
    }
}

1337.1.1 = {
	monarch = {
 		name = "Beek-Metaku"
		dynasty = "Sakeides"
		birth_date = 1308.1.1
		adm = 2
		dip = 2
		mil = 1
    }
}

1377.1.1 = {
	monarch = {
 		name = "Tulalurash"
		dynasty = "Eleedal-Teeus"
		birth_date = 1344.1.1
		adm = 4
		dip = 3
		mil = 2
    }
}

1466.1.1 = {
	monarch = {
 		name = "Nesh-Deeka"
		dynasty = "Cascalees"
		birth_date = 1431.1.1
		adm = 2
		dip = 2
		mil = 6
    }
}

1540.1.1 = {
	monarch = {
 		name = "Uralgnaza"
		dynasty = "Meerhaj"
		birth_date = 1519.1.1
		adm = 0
		dip = 2
		mil = 3
    }
}

1598.1.1 = {
	monarch = {
 		name = "Noyei-Heem"
		dynasty = "Caligeepa"
		birth_date = 1555.1.1
		adm = 5
		dip = 1
		mil = 6
    }
}

1690.1.1 = {
	monarch = {
 		name = "Jeetum-Lah"
		dynasty = "Derkeeja"
		birth_date = 1662.1.1
		adm = 4
		dip = 0
		mil = 3
		female = yes
    }
}

1734.1.1 = {
	monarch = {
 		name = "Bijum-Ta"
		dynasty = "Yelnneen"
		birth_date = 1704.1.1
		adm = 2
		dip = 6
		mil = 0
    }
}

1775.1.1 = {
	monarch = {
 		name = "Ukka-Meedish"
		dynasty = "Derkeeja"
		birth_date = 1749.1.1
		adm = 0
		dip = 5
		mil = 3
		female = yes
    }
}

1868.1.1 = {
	monarch = {
 		name = "Nowajeem"
		dynasty = "Wanan-Dar"
		birth_date = 1848.1.1
		adm = 5
		dip = 5
		mil = 0
    }
}

1906.1.1 = {
	monarch = {
 		name = "Hleezeireeus"
		dynasty = "Nefeseus"
		birth_date = 1853.1.1
		adm = 0
		dip = 6
		mil = 1
    }
}

1984.1.1 = {
	monarch = {
 		name = "Bura-Natoo"
		dynasty = "Gallures"
		birth_date = 1958.1.1
		adm = 5
		dip = 5
		mil = 5
		female = yes
    }
}

2039.1.1 = {
	monarch = {
 		name = "Kaloo"
		dynasty = "Taierleesh"
		birth_date = 2015.1.1
		adm = 4
		dip = 4
		mil = 2
		female = yes
    }
}

2079.1.1 = {
	monarch = {
 		name = "Buujhan"
		dynasty = "Er-Tei"
		birth_date = 2052.1.1
		adm = 2
		dip = 3
		mil = 5
    }
}

2170.1.1 = {
	monarch = {
 		name = "Utamukeeus"
		dynasty = "Pethees"
		birth_date = 2151.1.1
		adm = 0
		dip = 3
		mil = 2
    }
}

2260.1.1 = {
	monarch = {
 		name = "Omeeta"
		dynasty = "Talen-Ei"
		birth_date = 2223.1.1
		adm = 5
		dip = 2
		mil = 6
		female = yes
    }
}

2345.1.1 = {
	monarch = {
 		name = "Iskenaaz"
		dynasty = "Canision"
		birth_date = 2298.1.1
		adm = 4
		dip = 1
		mil = 2
    }
}

2403.1.1 = {
	monarch = {
 		name = "Chee-Dooka"
		dynasty = "Asumicus"
		birth_date = 2360.1.1
		adm = 2
		dip = 0
		mil = 6
		female = yes
    }
}

2455.1.1 = {
	monarch = {
 		name = "Utadeek"
		dynasty = "Reeeepa"
		birth_date = 2421.1.1
		adm = 4
		dip = 2
		mil = 1
    }
}

