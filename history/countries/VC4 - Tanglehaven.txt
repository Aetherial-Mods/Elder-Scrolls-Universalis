government = monarchy
government_rank = 1
mercantilism = 1
technology_group = elven_tg
religion = bosmer_pantheon
primary_culture = bosmer
capital = 5001

54.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Rolding"
		monarch_name = "Rolding I"
		dynasty = "Or'Naearil"
		birth_date = 41.1.1
		death_date = 59.1.1
		claim = 100
		adm = 1
		dip = 4
		mil = 1
    }
}

59.1.1 = {
	monarch = {
 		name = "Geldaeniel"
		dynasty = "Ur'Bluewind"
		birth_date = 21.1.1
		adm = 4
		dip = 5
		mil = 3
		female = yes
    }
	queen = {
 		name = "Orchenas"
		dynasty = "Ur'Bluewind"
		birth_date = 10.1.1
		adm = 1
		dip = 4
		mil = 2
    }
	heir = {
 		name = "Frenidela"
		monarch_name = "Frenidela I"
		dynasty = "Ur'Bluewind"
		birth_date = 57.1.1
		death_date = 146.1.1
		claim = 100
		adm = 5
		dip = 2
		mil = 1
		female = yes
    }
}

146.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Cirnean"
		monarch_name = "Cirnean I"
		dynasty = "Or'Physandra"
		birth_date = 146.1.1
		death_date = 164.1.1
		claim = 100
		adm = 3
		dip = 1
		mil = 5
    }
}

164.1.1 = {
	monarch = {
 		name = "Manroth"
		dynasty = "Or'Tarlain"
		birth_date = 123.1.1
		adm = 3
		dip = 5
		mil = 5
    }
}

229.1.1 = {
	monarch = {
 		name = "Galolion"
		dynasty = "Ur'Donae"
		birth_date = 184.1.1
		adm = 1
		dip = 4
		mil = 2
    }
	queen = {
 		name = "Finde"
		dynasty = "Ur'Donae"
		birth_date = 209.1.1
		adm = 5
		dip = 3
		mil = 1
		female = yes
    }
	heir = {
 		name = "Saldaer"
		monarch_name = "Saldaer I"
		dynasty = "Ur'Donae"
		birth_date = 223.1.1
		death_date = 287.1.1
		claim = 100
		adm = 5
		dip = 6
		mil = 2
    }
}

287.1.1 = {
	monarch = {
 		name = "Gerethel"
		dynasty = "Ca'Marlandra"
		birth_date = 268.1.1
		adm = 4
		dip = 2
		mil = 2
		female = yes
    }
	queen = {
 		name = "Pegebor"
		dynasty = "Ca'Marlandra"
		birth_date = 264.1.1
		adm = 1
		dip = 1
		mil = 1
    }
	heir = {
 		name = "Foral"
		monarch_name = "Foral I"
		dynasty = "Ca'Marlandra"
		birth_date = 273.1.1
		death_date = 331.1.1
		claim = 100
		adm = 5
		dip = 6
		mil = 0
    }
}

331.1.1 = {
	monarch = {
 		name = "Ulphroth"
		dynasty = "Ca'Hasaael"
		birth_date = 278.1.1
		adm = 1
		dip = 1
		mil = 3
    }
	queen = {
 		name = "Filenarth"
		dynasty = "Ca'Hasaael"
		birth_date = 303.1.1
		adm = 5
		dip = 6
		mil = 2
		female = yes
    }
	heir = {
 		name = "Thael"
		monarch_name = "Thael I"
		dynasty = "Ca'Hasaael"
		birth_date = 319.1.1
		death_date = 423.1.1
		claim = 100
		adm = 2
		dip = 5
		mil = 1
		female = yes
    }
}

423.1.1 = {
	monarch = {
 		name = "Gilragaith"
		dynasty = "Or'Silvenar"
		birth_date = 392.1.1
		adm = 4
		dip = 6
		mil = 3
    }
	queen = {
 		name = "Firgelin"
		dynasty = "Or'Silvenar"
		birth_date = 388.1.1
		adm = 1
		dip = 5
		mil = 2
		female = yes
    }
	heir = {
 		name = "Tholdegil"
		monarch_name = "Tholdegil I"
		dynasty = "Or'Silvenar"
		birth_date = 417.1.1
		death_date = 482.1.1
		claim = 100
		adm = 5
		dip = 3
		mil = 1
    }
}

482.1.1 = {
	monarch = {
 		name = "Tildri"
		dynasty = "Ur'Eldenroot"
		birth_date = 448.1.1
		adm = 4
		dip = 0
		mil = 1
		female = yes
    }
	queen = {
 		name = "Raniluin"
		dynasty = "Ur'Eldenroot"
		birth_date = 462.1.1
		adm = 1
		dip = 5
		mil = 0
    }
}

565.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Zifri"
		monarch_name = "Zifri I"
		dynasty = "Or'Vullen"
		birth_date = 556.1.1
		death_date = 574.1.1
		claim = 100
		adm = 5
		dip = 0
		mil = 0
		female = yes
    }
}

574.1.1 = {
	monarch = {
 		name = "Gwingelruin"
		dynasty = "Ur'Dornwing"
		birth_date = 547.1.1
		adm = 5
		dip = 4
		mil = 0
    }
	queen = {
 		name = "Thaerin"
		dynasty = "Ur'Dornwing"
		birth_date = 551.1.1
		adm = 2
		dip = 2
		mil = 6
		female = yes
    }
	heir = {
 		name = "Glauloroth"
		monarch_name = "Glauloroth I"
		dynasty = "Ur'Dornwing"
		birth_date = 560.1.1
		death_date = 665.1.1
		claim = 100
		adm = 6
		dip = 1
		mil = 5
    }
}

665.1.1 = {
	monarch = {
 		name = "Amridil"
		dynasty = "Ur'Bothenandriah"
		birth_date = 637.1.1
		adm = 1
		dip = 2
		mil = 1
		female = yes
    }
}

739.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Uwedrin"
		monarch_name = "Uwedrin I"
		dynasty = "Ur'Applerun"
		birth_date = 730.1.1
		death_date = 748.1.1
		claim = 100
		adm = 2
		dip = 6
		mil = 6
    }
}

748.1.1 = {
	monarch = {
 		name = "Hanthaerin"
		dynasty = "Ur'Ebon"
		birth_date = 701.1.1
		adm = 5
		dip = 0
		mil = 1
		female = yes
    }
}

783.1.1 = {
	monarch = {
 		name = "Enthilin"
		dynasty = "Ur'Eagle"
		birth_date = 763.1.1
		adm = 3
		dip = 0
		mil = 5
    }
}

867.1.1 = {
	monarch = {
 		name = "Amariel"
		dynasty = "Ur'Applerun"
		birth_date = 848.1.1
		adm = 5
		dip = 1
		mil = 6
		female = yes
    }
	queen = {
 		name = "Thongonor"
		dynasty = "Ur'Applerun"
		birth_date = 839.1.1
		adm = 5
		dip = 4
		mil = 0
    }
	heir = {
 		name = "Gluin"
		monarch_name = "Gluin I"
		dynasty = "Ur'Applerun"
		birth_date = 859.1.1
		death_date = 914.1.1
		claim = 100
		adm = 2
		dip = 3
		mil = 6
    }
}

914.1.1 = {
	monarch = {
 		name = "Arenion"
		dynasty = "Ca'Ivybranch"
		birth_date = 863.1.1
		adm = 0
		dip = 2
		mil = 3
    }
	queen = {
 		name = "Nona"
		dynasty = "Ca'Ivybranch"
		birth_date = 896.1.1
		adm = 4
		dip = 1
		mil = 2
		female = yes
    }
	heir = {
 		name = "Filenarth"
		monarch_name = "Filenarth I"
		dynasty = "Ca'Ivybranch"
		birth_date = 914.1.1
		death_date = 958.1.1
		claim = 100
		adm = 1
		dip = 6
		mil = 1
		female = yes
    }
}

958.1.1 = {
	monarch = {
 		name = "Finonas"
		dynasty = "Ur'Elrainthil"
		birth_date = 932.1.1
		adm = 0
		dip = 3
		mil = 1
    }
	queen = {
 		name = "Essonarth"
		dynasty = "Ur'Elrainthil"
		birth_date = 930.1.1
		adm = 4
		dip = 1
		mil = 0
		female = yes
    }
	heir = {
 		name = "Rathril"
		monarch_name = "Rathril I"
		dynasty = "Ur'Elrainthil"
		birth_date = 957.1.1
		death_date = 1018.1.1
		claim = 100
		adm = 4
		dip = 5
		mil = 1
		female = yes
    }
}

1018.1.1 = {
	monarch = {
 		name = "Fonalor"
		dynasty = "Or'Niveneth"
		birth_date = 985.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1091.1.1 = {
	monarch = {
 		name = "Dalrelvir"
		dynasty = "Ca'Meneia"
		birth_date = 1041.1.1
		adm = 2
		dip = 0
		mil = 5
    }
	queen = {
 		name = "Heragaeth"
		dynasty = "Ca'Meneia"
		birth_date = 1049.1.1
		adm = 6
		dip = 6
		mil = 4
		female = yes
    }
	heir = {
 		name = "Briminor"
		monarch_name = "Briminor I"
		dynasty = "Ca'Meneia"
		birth_date = 1087.1.1
		death_date = 1179.1.1
		claim = 100
		adm = 3
		dip = 4
		mil = 3
    }
}

1179.1.1 = {
	monarch = {
 		name = "Lonor"
		dynasty = "Or'Niveneth"
		birth_date = 1147.1.1
		adm = 6
		dip = 6
		mil = 5
    }
	queen = {
 		name = "Iirond"
		dynasty = "Or'Niveneth"
		birth_date = 1156.1.1
		adm = 2
		dip = 4
		mil = 4
		female = yes
    }
}

1232.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Daenilroth"
		monarch_name = "Daenilroth I"
		dynasty = "Or'Silvenar"
		birth_date = 1232.1.1
		death_date = 1250.1.1
		claim = 100
		adm = 1
		dip = 3
		mil = 1
    }
}

1250.1.1 = {
	monarch = {
 		name = "Ronarith"
		dynasty = "Ca'Meneia"
		birth_date = 1211.1.1
		adm = 0
		dip = 0
		mil = 1
		female = yes
    }
}

1328.1.1 = {
	monarch = {
 		name = "Genboril"
		dynasty = "Ca'Filen"
		birth_date = 1287.1.1
		adm = 1
		dip = 4
		mil = 6
		female = yes
    }
	queen = {
 		name = "Filbolrel"
		dynasty = "Ca'Filen"
		birth_date = 1283.1.1
		adm = 1
		dip = 0
		mil = 0
    }
	heir = {
 		name = "Thaereledh"
		monarch_name = "Thaereledh I"
		dynasty = "Ca'Filen"
		birth_date = 1313.1.1
		death_date = 1381.1.1
		claim = 100
		adm = 5
		dip = 6
		mil = 6
		female = yes
    }
}

1381.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Malgawn"
		monarch_name = "Malgawn I"
		dynasty = "Or'Naaillas"
		birth_date = 1373.1.1
		death_date = 1391.1.1
		claim = 100
		adm = 3
		dip = 5
		mil = 3
    }
}

1391.1.1 = {
	monarch = {
 		name = "Eiola"
		dynasty = "Ca'Laenlin"
		birth_date = 1344.1.1
		adm = 3
		dip = 2
		mil = 3
		female = yes
    }
}

1466.1.1 = {
	monarch = {
 		name = "Ungrendor"
		dynasty = "Or'Mossmire"
		birth_date = 1424.1.1
		adm = 1
		dip = 1
		mil = 0
    }
}

1550.1.1 = {
	monarch = {
 		name = "Methring"
		dynasty = "Ur'Arenthia"
		birth_date = 1518.1.1
		adm = 6
		dip = 0
		mil = 3
    }
}

1639.1.1 = {
	monarch = {
 		name = "Gilbonel"
		dynasty = "Ur'Ebon"
		birth_date = 1617.1.1
		adm = 4
		dip = 6
		mil = 0
		female = yes
    }
}

1686.1.1 = {
	monarch = {
 		name = "Edthas"
		dynasty = "Ca'Lichenrock"
		birth_date = 1648.1.1
		adm = 3
		dip = 5
		mil = 4
    }
	queen = {
 		name = "Cerweriell"
		dynasty = "Ca'Lichenrock"
		birth_date = 1638.1.1
		adm = 6
		dip = 4
		mil = 3
		female = yes
    }
}

1734.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Menthorn"
		monarch_name = "Menthorn I"
		dynasty = "Ca'Laenoniel"
		birth_date = 1731.1.1
		death_date = 1749.1.1
		claim = 100
		adm = 0
		dip = 1
		mil = 0
    }
}

1749.1.1 = {
	monarch = {
 		name = "Eniras"
		dynasty = "Ca'Gelwen"
		birth_date = 1704.1.1
		adm = 0
		dip = 4
		mil = 1
		female = yes
    }
}

1845.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Minthelir"
		monarch_name = "Minthelir I"
		dynasty = "Or'Seledra"
		birth_date = 1833.1.1
		death_date = 1851.1.1
		claim = 100
		adm = 4
		dip = 6
		mil = 1
    }
}

1851.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Glongor"
		monarch_name = "Glongor I"
		dynasty = "Ur'Eldenroot"
		birth_date = 1845.1.1
		death_date = 1863.1.1
		claim = 100
		adm = 2
		dip = 6
		mil = 4
    }
}

1863.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Elphaniel"
		monarch_name = "Elphaniel I"
		dynasty = "Or'Stonesquare"
		birth_date = 1851.1.1
		death_date = 1869.1.1
		claim = 100
		adm = 4
		dip = 0
		mil = 6
		female = yes
    }
}

1869.1.1 = {
	monarch = {
 		name = "Nidras"
		dynasty = "Ca'Iingenyl"
		birth_date = 1820.1.1
		adm = 0
		dip = 1
		mil = 1
    }
}

1924.1.1 = {
	monarch = {
 		name = "Gwiras"
		dynasty = "Ca'Elsesse"
		birth_date = 1872.1.1
		adm = 5
		dip = 0
		mil = 5
    }
}

2004.1.1 = {
	monarch = {
 		name = "Eranas"
		dynasty = "Ur'Bluewood"
		birth_date = 1956.1.1
		adm = 6
		dip = 4
		mil = 3
    }
}

2061.1.1 = {
	monarch = {
 		name = "Firuin"
		dynasty = "Ca'Hynhruviel"
		birth_date = 2031.1.1
		adm = 4
		dip = 4
		mil = 0
    }
}

2112.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Fangilmir"
		monarch_name = "Fangilmir I"
		dynasty = "Ca'Glien"
		birth_date = 2112.1.1
		death_date = 2130.1.1
		claim = 100
		adm = 5
		dip = 1
		mil = 4
    }
}

2130.1.1 = {
	monarch = {
 		name = "Rollin"
		dynasty = "Ur'Black"
		birth_date = 2100.1.1
		adm = 0
		dip = 2
		mil = 0
    }
	queen = {
 		name = "Nothemril"
		dynasty = "Ur'Black"
		birth_date = 2087.1.1
		adm = 4
		dip = 1
		mil = 6
		female = yes
    }
	heir = {
 		name = "Orenthel"
		monarch_name = "Orenthel I"
		dynasty = "Ur'Black"
		birth_date = 2128.1.1
		death_date = 2193.1.1
		claim = 100
		adm = 1
		dip = 6
		mil = 5
    }
}

2193.1.1 = {
	monarch = {
 		name = "Fironthor"
		dynasty = "Ur'Black"
		birth_date = 2146.1.1
		adm = 0
		dip = 3
		mil = 5
    }
	queen = {
 		name = "Faedre"
		dynasty = "Ur'Black"
		birth_date = 2156.1.1
		adm = 1
		dip = 6
		mil = 6
		female = yes
    }
	heir = {
 		name = "Rafel"
		monarch_name = "Rafel I"
		dynasty = "Ur'Black"
		birth_date = 2181.1.1
		death_date = 2270.1.1
		claim = 100
		adm = 5
		dip = 5
		mil = 5
    }
}

2270.1.1 = {
	monarch = {
 		name = "Forongon"
		dynasty = "Or'Wasten"
		birth_date = 2233.1.1
		adm = 4
		dip = 1
		mil = 5
    }
	queen = {
 		name = "Norweras"
		dynasty = "Or'Wasten"
		birth_date = 2243.1.1
		adm = 1
		dip = 6
		mil = 4
		female = yes
    }
	heir = {
 		name = "Fingrathel"
		monarch_name = "Fingrathel I"
		dynasty = "Or'Wasten"
		birth_date = 2257.1.1
		death_date = 2334.1.1
		claim = 100
		adm = 5
		dip = 5
		mil = 3
		female = yes
    }
}

2334.1.1 = {
	monarch = {
 		name = "Sandaenion"
		dynasty = "Ca'Mandae"
		birth_date = 2291.1.1
		adm = 0
		dip = 6
		mil = 6
    }
}

2380.1.1 = {
	monarch = {
 		name = "Maenrin"
		dynasty = "Ca'Lynpar"
		birth_date = 2336.1.1
		adm = 6
		dip = 6
		mil = 2
    }
}

2473.1.1 = {
	monarch = {
 		name = "Gananith"
		dynasty = "Ur'Eldenroot"
		birth_date = 2449.1.1
		adm = 4
		dip = 5
		mil = 6
		female = yes
    }
	queen = {
 		name = "Faenir"
		dynasty = "Ur'Eldenroot"
		birth_date = 2425.1.1
		adm = 1
		dip = 3
		mil = 5
    }
	heir = {
 		name = "Faulor"
		monarch_name = "Faulor I"
		dynasty = "Ur'Eldenroot"
		birth_date = 2473.1.1
		death_date = 2549.1.1
		claim = 100
		adm = 5
		dip = 2
		mil = 4
    }
}

