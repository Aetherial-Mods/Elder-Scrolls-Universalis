government = theocracy
government_rank = 3
mercantilism = 1
technology_group = elven_tg
religion = bosmer_pantheon
primary_culture = bosmer
capital = 4924
secondary_religion = hircine_cult

54.1.1 = {
	monarch = {
 		name = "Hanthaerin"
		dynasty = "Ca'Marlandra"
		birth_date = 8.1.1
		adm = 5
		dip = 3
		mil = 4
		female = yes
    }
}

131.1.1 = {
	monarch = {
 		name = "Eraneth"
		dynasty = "Ca'Green"
		birth_date = 105.1.1
		adm = 3
		dip = 2
		mil = 1
		female = yes
    }
}

214.1.1 = {
	monarch = {
 		name = "Amariel"
		dynasty = "Ca'Lieval"
		birth_date = 174.1.1
		adm = 5
		dip = 3
		mil = 2
		female = yes
    }
}

298.1.1 = {
	monarch = {
 		name = "Nelgorn"
		dynasty = "Ca'Laenlin"
		birth_date = 262.1.1
		adm = 3
		dip = 2
		mil = 6
    }
}

352.1.1 = {
	monarch = {
 		name = "Arganar"
		dynasty = "Or'Run"
		birth_date = 302.1.1
		adm = 1
		dip = 2
		mil = 3
    }
}

398.1.1 = {
	monarch = {
 		name = "Ninglenel"
		dynasty = "Or'Telael"
		birth_date = 346.1.1
		adm = 0
		dip = 1
		mil = 6
    }
}

453.1.1 = {
	monarch = {
 		name = "Hadras"
		dynasty = "Ur'Andaone"
		birth_date = 412.1.1
		adm = 5
		dip = 0
		mil = 3
    }
}

497.1.1 = {
	monarch = {
 		name = "Erodir"
		dynasty = "Ur'Dornlock"
		birth_date = 444.1.1
		adm = 3
		dip = 6
		mil = 0
    }
}

555.1.1 = {
	monarch = {
 		name = "Arengo"
		dynasty = "Ca'Mandae"
		birth_date = 507.1.1
		adm = 1
		dip = 5
		mil = 3
    }
}

623.1.1 = {
	monarch = {
 		name = "Nivrilin"
		dynasty = "Ca'Mandae"
		birth_date = 570.1.1
		adm = 0
		dip = 5
		mil = 0
		female = yes
    }
}

717.1.1 = {
	monarch = {
 		name = "Herdor"
		dynasty = "Ca'Lynpar"
		birth_date = 695.1.1
		adm = 1
		dip = 6
		mil = 1
		female = yes
    }
}

768.1.1 = {
	monarch = {
 		name = "Erengor"
		dynasty = "Ca'Elsyon"
		birth_date = 718.1.1
		adm = 0
		dip = 5
		mil = 5
    }
}

824.1.1 = {
	monarch = {
 		name = "Imriel"
		dynasty = "Ca'Marlandra"
		birth_date = 801.1.1
		adm = 5
		dip = 4
		mil = 2
		female = yes
    }
}

907.1.1 = {
	monarch = {
 		name = "Esunil"
		dynasty = "Ur'Applegrass"
		birth_date = 871.1.1
		adm = 3
		dip = 3
		mil = 5
    }
}

959.1.1 = {
	monarch = {
 		name = "Behelir"
		dynasty = "Ca'Greenheart"
		birth_date = 910.1.1
		adm = 1
		dip = 3
		mil = 2
    }
}

1003.1.1 = {
	monarch = {
 		name = "Nothrelras"
		dynasty = "Ur'Applepool"
		birth_date = 974.1.1
		adm = 0
		dip = 2
		mil = 6
    }
}

1094.1.1 = {
	monarch = {
 		name = "Haraenion"
		dynasty = "Ur'Disticia"
		birth_date = 1062.1.1
		adm = 5
		dip = 1
		mil = 2
    }
}

1142.1.1 = {
	monarch = {
 		name = "Esladir"
		dynasty = "Or'Vullen"
		birth_date = 1121.1.1
		adm = 3
		dip = 0
		mil = 6
    }
}

1237.1.1 = {
	monarch = {
 		name = "Gadinas"
		dynasty = "Or'Vulkwasten"
		birth_date = 1188.1.1
		adm = 4
		dip = 4
		mil = 4
    }
}

1336.1.1 = {
	monarch = {
 		name = "Deregor"
		dynasty = "Or'Mossmire"
		birth_date = 1285.1.1
		adm = 2
		dip = 4
		mil = 0
    }
}

1396.1.1 = {
	monarch = {
 		name = "Thaenor"
		dynasty = "Ur'Eineneth"
		birth_date = 1344.1.1
		adm = 0
		dip = 3
		mil = 4
		female = yes
    }
}

1481.1.1 = {
	monarch = {
 		name = "Melledh"
		dynasty = "Ur'Cormair"
		birth_date = 1442.1.1
		adm = 2
		dip = 4
		mil = 6
		female = yes
    }
}

1535.1.1 = {
	monarch = {
 		name = "Gannel"
		dynasty = "Ca'Kirshiron"
		birth_date = 1488.1.1
		adm = 1
		dip = 3
		mil = 2
		female = yes
    }
}

1616.1.1 = {
	monarch = {
 		name = "Mangaer"
		dynasty = "Or'Rosethorn"
		birth_date = 1594.1.1
		adm = 6
		dip = 2
		mil = 6
    }
}

1663.1.1 = {
	monarch = {
 		name = "Garwiruin"
		dynasty = "Ur'Disticia"
		birth_date = 1623.1.1
		adm = 4
		dip = 2
		mil = 3
		female = yes
    }
}

1705.1.1 = {
	monarch = {
 		name = "Donthengeval"
		dynasty = "Ca'Minweneth"
		birth_date = 1666.1.1
		adm = 2
		dip = 1
		mil = 6
    }
}

1753.1.1 = {
	monarch = {
 		name = "Thololin"
		dynasty = "Ur'Ciinthil"
		birth_date = 1704.1.1
		adm = 1
		dip = 0
		mil = 3
    }
}

1839.1.1 = {
	monarch = {
 		name = "Mennir"
		dynasty = "Ca'Heaven"
		birth_date = 1804.1.1
		adm = 6
		dip = 6
		mil = 0
		female = yes
    }
}

1874.1.1 = {
	monarch = {
 		name = "Galalron"
		dynasty = "Ca'Greenheart"
		birth_date = 1821.1.1
		adm = 4
		dip = 5
		mil = 3
    }
}

1939.1.1 = {
	monarch = {
 		name = "Dollian"
		dynasty = "Ca'Iingenyl"
		birth_date = 1895.1.1
		adm = 6
		dip = 0
		mil = 5
		female = yes
    }
}

1999.1.1 = {
	monarch = {
 		name = "Thollidor"
		dynasty = "Ur'Applerun"
		birth_date = 1978.1.1
		adm = 4
		dip = 6
		mil = 1
    }
}

2044.1.1 = {
	monarch = {
 		name = "Dorathil"
		dynasty = "Ca'Elsyon"
		birth_date = 2020.1.1
		adm = 2
		dip = 5
		mil = 5
		female = yes
    }
}

2079.1.1 = {
	monarch = {
 		name = "Tralor"
		dynasty = "Ca'Minweneth"
		birth_date = 2037.1.1
		adm = 1
		dip = 4
		mil = 2
    }
}

2148.1.1 = {
	monarch = {
 		name = "Minonel"
		dynasty = "Or'Riverwood"
		birth_date = 2116.1.1
		adm = 6
		dip = 4
		mil = 5
		female = yes
    }
}

2214.1.1 = {
	monarch = {
 		name = "Ganwidrin"
		dynasty = "Or'Nathenyl"
		birth_date = 2189.1.1
		adm = 4
		dip = 3
		mil = 2
    }
}

2309.1.1 = {
	monarch = {
 		name = "Draulduin"
		dynasty = "Or'Oakenwood"
		birth_date = 2288.1.1
		adm = 2
		dip = 2
		mil = 6
    }
}

2383.1.1 = {
	monarch = {
 		name = "Thiirion"
		dynasty = "Or'Oakwood"
		birth_date = 2360.1.1
		adm = 1
		dip = 1
		mil = 2
		female = yes
    }
}

2435.1.1 = {
	monarch = {
 		name = "Marthine"
		dynasty = "Ca'Marbruk"
		birth_date = 2415.1.1
		adm = 3
		dip = 2
		mil = 4
    }
}

