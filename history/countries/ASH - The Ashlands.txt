government = monarchy
government_rank = 5
mercantilism = 1
technology_group = exiled_tg
religion = chimer_pantheon
primary_culture = ashlander_dunmer
capital = 1043

54.1.1 = {
	monarch = {
 		name = "Balur"
		dynasty = "Sobbinisun"
		birth_date = 7.1.1
		adm = 2
		dip = 4
		mil = 4
    }
	queen = {
 		name = "Pilu"
		dynasty = "Sobbinisun"
		birth_date = 29.1.1
		adm = 6
		dip = 2
		mil = 3
		female = yes
    }
	heir = {
 		name = "Assi"
		monarch_name = "Assi I"
		dynasty = "Sobbinisun"
		birth_date = 42.1.1
		death_date = 125.1.1
		claim = 100
		adm = 3
		dip = 1
		mil = 2
		female = yes
    }
}

125.1.1 = {
	monarch = {
 		name = "Shanud"
		dynasty = "Ashunbabi"
		birth_date = 102.1.1
		adm = 6
		dip = 2
		mil = 5
    }
}

177.1.1 = {
	monarch = {
 		name = "Mal"
		dynasty = "Ahanidiran"
		birth_date = 137.1.1
		adm = 4
		dip = 1
		mil = 1
    }
	queen = {
 		name = "Kurapli"
		dynasty = "Ahanidiran"
		birth_date = 135.1.1
		adm = 1
		dip = 0
		mil = 0
		female = yes
    }
	heir = {
 		name = "Yanibi"
		monarch_name = "Yanibi I"
		dynasty = "Ahanidiran"
		birth_date = 168.1.1
		death_date = 271.1.1
		claim = 100
		adm = 5
		dip = 5
		mil = 6
		female = yes
    }
}

271.1.1 = {
	monarch = {
 		name = "Zanmulk"
		dynasty = "Mibishanit"
		birth_date = 237.1.1
		adm = 4
		dip = 2
		mil = 6
    }
	queen = {
 		name = "Ulisamsi"
		dynasty = "Mibishanit"
		birth_date = 220.1.1
		adm = 1
		dip = 0
		mil = 5
		female = yes
    }
}

334.1.1 = {
	monarch = {
 		name = "Dutadalk"
		dynasty = "Nerendus"
		birth_date = 286.1.1
		adm = 6
		dip = 0
		mil = 2
    }
}

428.1.1 = {
	monarch = {
 		name = "Nind"
		dynasty = "Assantinalit"
		birth_date = 384.1.1
		adm = 5
		dip = 6
		mil = 6
    }
}

501.1.1 = {
	monarch = {
 		name = "Esar-Don"
		dynasty = "Assintashiran"
		birth_date = 473.1.1
		adm = 3
		dip = 5
		mil = 2
    }
}

594.1.1 = {
	monarch = {
 		name = "Nerevar"
		dynasty = "Asharapli"
		birth_date = 562.1.1
		adm = 1
		dip = 4
		mil = 6
    }
}

638.1.1 = {
	monarch = {
 		name = "Shullay"
		dynasty = "Zaintashara"
		birth_date = 608.1.1
		adm = 6
		dip = 3
		mil = 3
		female = yes
    }
}

683.1.1 = {
	monarch = {
 		name = "Nind"
		dynasty = "Hainnabibi"
		birth_date = 657.1.1
		adm = 5
		dip = 3
		mil = 6
    }
}

782.1.1 = {
	monarch = {
 		name = "Hanarai"
		dynasty = "Assullinbanud"
		birth_date = 759.1.1
		adm = 3
		dip = 2
		mil = 3
		female = yes
    }
}

835.1.1 = {
	monarch = {
 		name = "Nerevar"
		dynasty = "Ilath-Pal"
		birth_date = 801.1.1
		adm = 5
		dip = 3
		mil = 4
    }
}

914.1.1 = {
	monarch = {
 		name = "Shullay"
		dynasty = "Shashipal"
		birth_date = 877.1.1
		adm = 3
		dip = 2
		mil = 1
		female = yes
    }
}

983.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Elumabi"
		monarch_name = "Elumabi I"
		dynasty = "Assurnipilu"
		birth_date = 968.1.1
		death_date = 986.1.1
		claim = 100
		adm = 0
		dip = 4
		mil = 1
		female = yes
    }
}

986.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Zenabi"
		monarch_name = "Zenabi I"
		dynasty = "Tansumiran"
		birth_date = 984.1.1
		death_date = 1002.1.1
		claim = 100
		adm = 2
		dip = 6
		mil = 3
    }
}

1002.1.1 = {
	monarch = {
 		name = "Ninirrasour"
		dynasty = "Sershurrapal"
		birth_date = 974.1.1
		adm = 5
		dip = 0
		mil = 5
    }
}

1071.1.1 = {
	monarch = {
 		name = "Hainab"
		dynasty = "Zelma-Alit"
		birth_date = 1024.1.1
		adm = 3
		dip = 6
		mil = 2
    }
}

1106.1.1 = {
	monarch = {
 		name = "Shallath-Piremus"
		dynasty = "Sobdishapal"
		birth_date = 1079.1.1
		adm = 4
		dip = 3
		mil = 0
		female = yes
    }
	queen = {
 		name = "Tinti"
		dynasty = "Sobdishapal"
		birth_date = 1085.1.1
		adm = 1
		dip = 2
		mil = 6
    }
}

1161.1.1 = {
	monarch = {
 		name = "Shannat"
		dynasty = "Eramarellaku"
		birth_date = 1122.1.1
		adm = 6
		dip = 1
		mil = 2
    }
	queen = {
 		name = "Antu"
		dynasty = "Eramarellaku"
		birth_date = 1140.1.1
		adm = 3
		dip = 6
		mil = 1
		female = yes
    }
	heir = {
 		name = "Sakiran"
		monarch_name = "Sakiran I"
		dynasty = "Eramarellaku"
		birth_date = 1154.1.1
		death_date = 1208.1.1
		claim = 100
		adm = 0
		dip = 5
		mil = 0
		female = yes
    }
}

1208.1.1 = {
	monarch = {
 		name = "Assurdan"
		dynasty = "Esurarnat"
		birth_date = 1167.1.1
		adm = 2
		dip = 6
		mil = 2
    }
}

1249.1.1 = {
	monarch = {
 		name = "Zabi"
		dynasty = "Pansamsi"
		birth_date = 1223.1.1
		adm = 1
		dip = 6
		mil = 6
		female = yes
    }
}

1338.1.1 = {
	monarch = {
 		name = "Shannat"
		dynasty = "Urshumusa"
		birth_date = 1309.1.1
		adm = 6
		dip = 5
		mil = 3
    }
	queen = {
 		name = "Raishi"
		dynasty = "Urshumusa"
		birth_date = 1295.1.1
		adm = 3
		dip = 3
		mil = 2
		female = yes
    }
}

1422.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Nammu"
		monarch_name = "Nammu I"
		dynasty = "Odirnapal"
		birth_date = 1407.1.1
		death_date = 1425.1.1
		claim = 100
		adm = 6
		dip = 5
		mil = 1
		female = yes
    }
}

1425.1.1 = {
	monarch = {
 		name = "Ashamanu"
		dynasty = "Muddumummu"
		birth_date = 1391.1.1
		adm = 2
		dip = 0
		mil = 3
		female = yes
    }
}

1491.1.1 = {
	monarch = {
 		name = "Yanit"
		dynasty = "Dun-Ahhe"
		birth_date = 1438.1.1
		adm = 0
		dip = 6
		mil = 0
    }
	queen = {
 		name = "Shullay"
		dynasty = "Dun-Ahhe"
		birth_date = 1448.1.1
		adm = 4
		dip = 4
		mil = 6
		female = yes
    }
	heir = {
 		name = "Truan"
		monarch_name = "Truan I"
		dynasty = "Dun-Ahhe"
		birth_date = 1481.1.1
		death_date = 1526.1.1
		claim = 100
		adm = 1
		dip = 3
		mil = 5
    }
}

1526.1.1 = {
	monarch = {
 		name = "Kummu"
		dynasty = "Ashunbabi"
		birth_date = 1474.1.1
		adm = 0
		dip = 6
		mil = 5
		female = yes
    }
}

1588.1.1 = {
	monarch = {
 		name = "Ainat"
		dynasty = "Dun-Ahhe"
		birth_date = 1560.1.1
		adm = 5
		dip = 5
		mil = 2
    }
	queen = {
 		name = "Zaba"
		dynasty = "Dun-Ahhe"
		birth_date = 1537.1.1
		adm = 1
		dip = 0
		mil = 4
		female = yes
    }
	heir = {
 		name = "Kind"
		monarch_name = "Kind I"
		dynasty = "Dun-Ahhe"
		birth_date = 1578.1.1
		death_date = 1655.1.1
		claim = 100
		adm = 5
		dip = 5
		mil = 3
    }
}

1655.1.1 = {
	monarch = {
 		name = "Zanummu"
		dynasty = "Ashun-Idantus"
		birth_date = 1610.1.1
		adm = 4
		dip = 2
		mil = 3
		female = yes
    }
}

1701.1.1 = {
	monarch = {
 		name = "Shargon"
		dynasty = "Assurnipilu"
		birth_date = 1661.1.1
		adm = 3
		dip = 1
		mil = 0
    }
}

1793.1.1 = {
	monarch = {
 		name = "Zenabi"
		dynasty = "Serdimapal"
		birth_date = 1752.1.1
		adm = 1
		dip = 0
		mil = 4
    }
}

1855.1.1 = {
	monarch = {
 		name = "Shin"
		dynasty = "Ularshanentus"
		birth_date = 1835.1.1
		adm = 6
		dip = 6
		mil = 0
    }
	queen = {
 		name = "Ashumanu"
		dynasty = "Ularshanentus"
		birth_date = 1828.1.1
		adm = 3
		dip = 5
		mil = 6
		female = yes
    }
	heir = {
 		name = "Seba"
		monarch_name = "Seba I"
		dynasty = "Ularshanentus"
		birth_date = 1844.1.1
		death_date = 1924.1.1
		claim = 100
		adm = 0
		dip = 3
		mil = 5
		female = yes
    }
}

1924.1.1 = {
	monarch = {
 		name = "Elitlaya"
		dynasty = "Sobbinisun"
		birth_date = 1890.1.1
		adm = 3
		dip = 5
		mil = 1
		female = yes
    }
	queen = {
 		name = "Salattanat"
		dynasty = "Sobbinisun"
		birth_date = 1873.1.1
		adm = 6
		dip = 3
		mil = 0
    }
	heir = {
 		name = "Assamma-Idan"
		monarch_name = "Assamma-Idan I"
		dynasty = "Sobbinisun"
		birth_date = 1912.1.1
		death_date = 1969.1.1
		claim = 100
		adm = 3
		dip = 2
		mil = 6
    }
}

1969.1.1 = {
	monarch = {
 		name = "Shin"
		dynasty = "Dunsamsi"
		birth_date = 1926.1.1
		adm = 6
		dip = 3
		mil = 1
    }
}

2052.1.1 = {
	monarch = {
 		name = "Massour"
		dynasty = "Muddumummu"
		birth_date = 2014.1.1
		adm = 1
		dip = 4
		mil = 2
    }
	queen = {
 		name = "Lanabi"
		dynasty = "Muddumummu"
		birth_date = 2009.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
	heir = {
 		name = "Zanat"
		monarch_name = "Zanat I"
		dynasty = "Muddumummu"
		birth_date = 2044.1.1
		death_date = 2092.1.1
		claim = 100
		adm = 5
		dip = 6
		mil = 2
    }
}

2092.1.1 = {
	monarch = {
 		name = "Minisun"
		dynasty = "Assarnuridan"
		birth_date = 2052.1.1
		adm = 4
		dip = 3
		mil = 3
    }
	queen = {
 		name = "Vabbar"
		dynasty = "Assarnuridan"
		birth_date = 2054.1.1
		adm = 1
		dip = 1
		mil = 2
		female = yes
    }
	heir = {
 		name = "Man-Ilu"
		monarch_name = "Man-Ilu I"
		dynasty = "Assarnuridan"
		birth_date = 2080.1.1
		death_date = 2155.1.1
		claim = 100
		adm = 5
		dip = 0
		mil = 1
		female = yes
    }
}

2155.1.1 = {
	monarch = {
 		name = "Zula"
		dynasty = "Zansatanit"
		birth_date = 2117.1.1
		adm = 1
		dip = 1
		mil = 3
		female = yes
    }
	queen = {
 		name = "Kanud"
		dynasty = "Zansatanit"
		birth_date = 2109.1.1
		adm = 5
		dip = 0
		mil = 2
    }
	heir = {
 		name = "Zaba"
		monarch_name = "Zaba I"
		dynasty = "Zansatanit"
		birth_date = 2154.1.1
		death_date = 2242.1.1
		claim = 100
		adm = 2
		dip = 5
		mil = 1
		female = yes
    }
}

2242.1.1 = {
	monarch = {
 		name = "Mimanu"
		dynasty = "Gilu"
		birth_date = 2220.1.1
		adm = 4
		dip = 0
		mil = 3
		female = yes
    }
}

2319.1.1 = {
	monarch = {
 		name = "Dunsalipal"
		dynasty = "Odin-Ahhe"
		birth_date = 2278.1.1
		adm = 3
		dip = 6
		mil = 0
    }
	queen = {
 		name = "Assimusa"
		dynasty = "Odin-Ahhe"
		birth_date = 2288.1.1
		adm = 0
		dip = 4
		mil = 6
		female = yes
    }
	heir = {
 		name = "Shargon"
		monarch_name = "Shargon I"
		dynasty = "Odin-Ahhe"
		birth_date = 2308.1.1
		death_date = 2361.1.1
		claim = 100
		adm = 3
		dip = 3
		mil = 5
    }
}

2361.1.1 = {
	monarch = {
 		name = "Shirerib"
		dynasty = "Assintashiran"
		birth_date = 2332.1.1
		adm = 3
		dip = 6
		mil = 5
    }
	queen = {
 		name = "Samsi"
		dynasty = "Assintashiran"
		birth_date = 2332.1.1
		adm = 0
		dip = 5
		mil = 4
		female = yes
    }
}

2448.1.1 = {
	monarch = {
 		name = "Abishpulu"
		dynasty = "Ulannanit"
		birth_date = 2429.1.1
		adm = 5
		dip = 4
		mil = 1
    }
}

