government = monarchy
government_rank = 1
mercantilism = 1
technology_group = northern_tg
religion = druidism
primary_culture = breton
capital = 1430

54.1.1 = {
	monarch = {
 		name = "Valcent"
		dynasty = "Countenan"
		birth_date = 31.1.1
		adm = 4
		dip = 4
		mil = 1
    }
}

140.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Grace"
		monarch_name = "Grace I"
		dynasty = "Aumilie"
		birth_date = 134.1.1
		death_date = 152.1.1
		claim = 100
		adm = 1
		dip = 6
		mil = 1
		female = yes
    }
}

152.1.1 = {
	monarch = {
 		name = "Vicmond"
		dynasty = "Arsenault"
		birth_date = 134.1.1
		adm = 1
		dip = 3
		mil = 1
    }
	queen = {
 		name = "Fasele"
		dynasty = "Arsenault"
		birth_date = 114.1.1
		adm = 4
		dip = 1
		mil = 0
		female = yes
    }
	heir = {
 		name = "Thibault"
		monarch_name = "Thibault I"
		dynasty = "Arsenault"
		birth_date = 147.1.1
		death_date = 224.1.1
		claim = 100
		adm = 1
		dip = 0
		mil = 6
    }
}

224.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Mathias"
		monarch_name = "Mathias I"
		dynasty = "Etelette"
		birth_date = 212.1.1
		death_date = 230.1.1
		claim = 100
		adm = 0
		dip = 6
		mil = 3
    }
}

230.1.1 = {
	monarch = {
 		name = "Corbyn"
		dynasty = "Cassel"
		birth_date = 184.1.1
		adm = 2
		dip = 0
		mil = 5
    }
	queen = {
 		name = "Lyze"
		dynasty = "Cassel"
		birth_date = 197.1.1
		adm = 6
		dip = 6
		mil = 4
		female = yes
    }
}

306.1.1 = {
	monarch = {
 		name = "Jacmund"
		dynasty = "Urbyn"
		birth_date = 273.1.1
		adm = 4
		dip = 5
		mil = 1
    }
}

380.1.1 = {
	monarch = {
 		name = "Dianette"
		dynasty = "Jegnole"
		birth_date = 345.1.1
		adm = 3
		dip = 4
		mil = 4
		female = yes
    }
}

455.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Chental"
		monarch_name = "Chental I"
		dynasty = "Tailas"
		birth_date = 450.1.1
		death_date = 468.1.1
		claim = 100
		adm = 3
		dip = 1
		mil = 2
		female = yes
    }
}

468.1.1 = {
	monarch = {
 		name = "Perrot"
		dynasty = "Ginsen"
		birth_date = 447.1.1
		adm = 3
		dip = 5
		mil = 2
    }
}

538.1.1 = {
	monarch = {
 		name = "Katrine"
		dynasty = "Ruman"
		birth_date = 515.1.1
		adm = 1
		dip = 4
		mil = 6
		female = yes
    }
	queen = {
 		name = "Geonard"
		dynasty = "Ruman"
		birth_date = 493.1.1
		adm = 5
		dip = 3
		mil = 5
    }
	heir = {
 		name = "Vyrois"
		monarch_name = "Vyrois I"
		dynasty = "Ruman"
		birth_date = 536.1.1
		death_date = 592.1.1
		claim = 100
		adm = 5
		dip = 6
		mil = 6
    }
}

592.1.1 = {
	monarch = {
 		name = "Jean-Pierre"
		dynasty = "Marck"
		birth_date = 550.1.1
		adm = 5
		dip = 2
		mil = 6
    }
	queen = {
 		name = "Valentine"
		dynasty = "Marck"
		birth_date = 574.1.1
		adm = 1
		dip = 1
		mil = 5
		female = yes
    }
	heir = {
 		name = "Handyn"
		monarch_name = "Handyn I"
		dynasty = "Marck"
		birth_date = 585.1.1
		death_date = 681.1.1
		claim = 100
		adm = 5
		dip = 6
		mil = 4
    }
}

681.1.1 = {
	monarch = {
 		name = "Alberic"
		dynasty = "Stentor"
		birth_date = 640.1.1
		adm = 1
		dip = 1
		mil = 0
    }
}

735.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Vontan"
		monarch_name = "Vontan I"
		dynasty = "Ergalla"
		birth_date = 726.1.1
		death_date = 744.1.1
		claim = 100
		adm = 2
		dip = 5
		mil = 5
    }
}

744.1.1 = {
	monarch = {
 		name = "Javier"
		dynasty = "Panoit"
		birth_date = 711.1.1
		adm = 5
		dip = 6
		mil = 0
    }
	queen = {
 		name = "Henrisa"
		dynasty = "Panoit"
		birth_date = 720.1.1
		adm = 1
		dip = 5
		mil = 6
		female = yes
    }
	heir = {
 		name = "Yves"
		monarch_name = "Yves I"
		dynasty = "Panoit"
		birth_date = 735.1.1
		death_date = 815.1.1
		claim = 100
		adm = 5
		dip = 3
		mil = 5
    }
}

815.1.1 = {
	monarch = {
 		name = "Alix"
		dynasty = "Viralaine"
		birth_date = 762.1.1
		adm = 5
		dip = 0
		mil = 5
		female = yes
    }
	queen = {
 		name = "Utheccan"
		dynasty = "Viralaine"
		birth_date = 780.1.1
		adm = 2
		dip = 5
		mil = 4
    }
	heir = {
 		name = "Hilaire"
		monarch_name = "Hilaire I"
		dynasty = "Viralaine"
		birth_date = 808.1.1
		death_date = 873.1.1
		claim = 100
		adm = 2
		dip = 2
		mil = 5
    }
}

873.1.1 = {
	monarch = {
 		name = "Alouis"
		dynasty = "Begnaud"
		birth_date = 850.1.1
		adm = 1
		dip = 5
		mil = 5
    }
	queen = {
 		name = "Henneque"
		dynasty = "Begnaud"
		birth_date = 836.1.1
		adm = 5
		dip = 4
		mil = 4
		female = yes
    }
	heir = {
 		name = "Yncien"
		monarch_name = "Yncien I"
		dynasty = "Begnaud"
		birth_date = 866.1.1
		death_date = 920.1.1
		claim = 100
		adm = 2
		dip = 2
		mil = 3
    }
}

920.1.1 = {
	monarch = {
 		name = "Jocien"
		dynasty = "Magaudin"
		birth_date = 887.1.1
		adm = 5
		dip = 4
		mil = 6
    }
}

970.1.1 = {
	monarch = {
 		name = "Ealis"
		dynasty = "Begnaud"
		birth_date = 935.1.1
		adm = 3
		dip = 3
		mil = 2
    }
}

1033.1.1 = {
	monarch = {
 		name = "Allunen"
		dynasty = "Stroud"
		birth_date = 985.1.1
		adm = 1
		dip = 2
		mil = 6
    }
	queen = {
 		name = "Vivien"
		dynasty = "Stroud"
		birth_date = 1002.1.1
		adm = 5
		dip = 0
		mil = 5
		female = yes
    }
	heir = {
 		name = "Irnand"
		monarch_name = "Irnand I"
		dynasty = "Stroud"
		birth_date = 1019.1.1
		death_date = 1132.1.1
		claim = 100
		adm = 2
		dip = 6
		mil = 4
    }
}

1132.1.1 = {
	monarch = {
 		name = "Jesper"
		dynasty = "Diel"
		birth_date = 1094.1.1
		adm = 1
		dip = 2
		mil = 4
    }
	queen = {
 		name = "Inawyn"
		dynasty = "Diel"
		birth_date = 1094.1.1
		adm = 5
		dip = 1
		mil = 3
		female = yes
    }
	heir = {
 		name = "Adam"
		monarch_name = "Adam I"
		dynasty = "Diel"
		birth_date = 1122.1.1
		death_date = 1206.1.1
		claim = 100
		adm = 0
		dip = 2
		mil = 1
    }
}

1206.1.1 = {
	monarch = {
 		name = "Colbois"
		dynasty = "Emax"
		birth_date = 1182.1.1
		adm = 2
		dip = 3
		mil = 3
    }
}

1276.1.1 = {
	monarch = {
 		name = "Vistra"
		dynasty = "Hawrond"
		birth_date = 1234.1.1
		adm = 1
		dip = 2
		mil = 0
		female = yes
    }
}

1323.1.1 = {
	monarch = {
 		name = "Miramel"
		dynasty = "Gelves"
		birth_date = 1292.1.1
		adm = 2
		dip = 3
		mil = 1
		female = yes
    }
}

1382.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Canollot"
		monarch_name = "Canollot I"
		dynasty = "Jeanyn"
		birth_date = 1374.1.1
		death_date = 1392.1.1
		claim = 100
		adm = 0
		dip = 5
		mil = 2
    }
}

1392.1.1 = {
	monarch = {
 		name = "Odei"
		dynasty = "Charnis"
		birth_date = 1347.1.1
		adm = 6
		dip = 2
		mil = 2
    }
	queen = {
 		name = "Aurore"
		dynasty = "Charnis"
		birth_date = 1355.1.1
		adm = 3
		dip = 0
		mil = 1
		female = yes
    }
	heir = {
 		name = "Mavus"
		monarch_name = "Mavus I"
		dynasty = "Charnis"
		birth_date = 1380.1.1
		death_date = 1447.1.1
		claim = 100
		adm = 0
		dip = 6
		mil = 0
    }
}

1447.1.1 = {
	monarch = {
 		name = "Crendal"
		dynasty = "Geves"
		birth_date = 1404.1.1
		adm = 2
		dip = 0
		mil = 2
    }
	queen = {
 		name = "Magali"
		dynasty = "Geves"
		birth_date = 1427.1.1
		adm = 6
		dip = 6
		mil = 1
		female = yes
    }
}

1490.1.1 = {
	monarch = {
 		name = "Jauffre"
		dynasty = "Gestor"
		birth_date = 1447.1.1
		adm = 5
		dip = 5
		mil = 5
    }
	queen = {
 		name = "Urvie"
		dynasty = "Gestor"
		birth_date = 1439.1.1
		adm = 1
		dip = 4
		mil = 4
		female = yes
    }
	heir = {
 		name = "Guybert"
		monarch_name = "Guybert I"
		dynasty = "Gestor"
		birth_date = 1490.1.1
		death_date = 1538.1.1
		claim = 100
		adm = 5
		dip = 2
		mil = 3
    }
}

1538.1.1 = {
	monarch = {
 		name = "Alexia"
		dynasty = "Grondin"
		birth_date = 1504.1.1
		adm = 1
		dip = 3
		mil = 5
		female = yes
    }
}

1615.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Ygerne"
		monarch_name = "Ygerne I"
		dynasty = "Franis"
		birth_date = 1610.1.1
		death_date = 1628.1.1
		claim = 100
		adm = 2
		dip = 0
		mil = 3
		female = yes
    }
}

1628.1.1 = {
	monarch = {
 		name = "Janvin"
		dynasty = "Belaram"
		birth_date = 1603.1.1
		adm = 0
		dip = 0
		mil = 0
    }
}

1689.1.1 = {
	monarch = {
 		name = "Didier"
		dynasty = "Velmont"
		birth_date = 1658.1.1
		adm = 6
		dip = 3
		mil = 0
    }
	queen = {
 		name = "Cecile"
		dynasty = "Velmont"
		birth_date = 1660.1.1
		adm = 3
		dip = 2
		mil = 6
		female = yes
    }
	heir = {
 		name = "Orvel"
		monarch_name = "Orvel I"
		dynasty = "Velmont"
		birth_date = 1685.1.1
		death_date = 1750.1.1
		claim = 100
		adm = 4
		dip = 5
		mil = 0
    }
}

1750.1.1 = {
	monarch = {
 		name = "Dureau"
		dynasty = "Lynielle"
		birth_date = 1697.1.1
		adm = 3
		dip = 2
		mil = 0
    }
	queen = {
 		name = "Melodie"
		dynasty = "Lynielle"
		birth_date = 1700.1.1
		adm = 0
		dip = 0
		mil = 6
		female = yes
    }
	heir = {
 		name = "Daniel"
		monarch_name = "Daniel I"
		dynasty = "Lynielle"
		birth_date = 1743.1.1
		death_date = 1819.1.1
		claim = 100
		adm = 4
		dip = 6
		mil = 5
    }
}

1819.1.1 = {
	monarch = {
 		name = "Quentyn"
		dynasty = "Andras"
		birth_date = 1766.1.1
		adm = 6
		dip = 0
		mil = 0
    }
}

1883.1.1 = {
	monarch = {
 		name = "Jernald"
		dynasty = "Jeanard"
		birth_date = 1846.1.1
		adm = 5
		dip = 6
		mil = 4
    }
}

1952.1.1 = {
	monarch = {
 		name = "Duncan"
		dynasty = "Viranes"
		birth_date = 1905.1.1
		adm = 3
		dip = 5
		mil = 1
    }
	queen = {
 		name = "Charlisse"
		dynasty = "Viranes"
		birth_date = 1934.1.1
		adm = 0
		dip = 4
		mil = 0
		female = yes
    }
	heir = {
 		name = "Odyle"
		monarch_name = "Odyle I"
		dynasty = "Viranes"
		birth_date = 1946.1.1
		death_date = 2029.1.1
		claim = 100
		adm = 4
		dip = 2
		mil = 6
		female = yes
    }
}

2029.1.1 = {
	monarch = {
 		name = "Praxin"
		dynasty = "Cheval"
		birth_date = 1998.1.1
		adm = 3
		dip = 6
		mil = 6
    }
	queen = {
 		name = "Miquela"
		dynasty = "Cheval"
		birth_date = 1983.1.1
		adm = 0
		dip = 4
		mil = 5
		female = yes
    }
	heir = {
 		name = "Derella"
		monarch_name = "Derella I"
		dynasty = "Cheval"
		birth_date = 2014.1.1
		death_date = 2069.1.1
		claim = 100
		adm = 0
		dip = 1
		mil = 6
		female = yes
    }
}

2069.1.1 = {
	monarch = {
 		name = "Renald"
		dynasty = "Thenitte"
		birth_date = 2044.1.1
		adm = 0
		dip = 4
		mil = 6
    }
}

2167.1.1 = {
	monarch = {
 		name = "Jouane"
		dynasty = "Gimbert"
		birth_date = 2133.1.1
		adm = 5
		dip = 3
		mil = 3
    }
	queen = {
 		name = "Violette"
		dynasty = "Gimbert"
		birth_date = 2124.1.1
		adm = 2
		dip = 2
		mil = 2
		female = yes
    }
	heir = {
 		name = "Idaron"
		monarch_name = "Idaron I"
		dynasty = "Gimbert"
		birth_date = 2167.1.1
		death_date = 2247.1.1
		claim = 100
		adm = 6
		dip = 0
		mil = 1
    }
}

2247.1.1 = {
	monarch = {
 		name = "Angeline"
		dynasty = "Metrick"
		birth_date = 2199.1.1
		adm = 1
		dip = 2
		mil = 3
		female = yes
    }
	queen = {
 		name = "Vilhim"
		dynasty = "Metrick"
		birth_date = 2219.1.1
		adm = 5
		dip = 0
		mil = 2
    }
	heir = {
 		name = "Zyssa"
		monarch_name = "Zyssa I"
		dynasty = "Metrick"
		birth_date = 2235.1.1
		death_date = 2294.1.1
		claim = 100
		adm = 2
		dip = 6
		mil = 1
		female = yes
    }
}

2294.1.1 = {
	monarch = {
 		name = "Jorden"
		dynasty = "Carlier"
		birth_date = 2244.1.1
		adm = 1
		dip = 2
		mil = 1
    }
	queen = {
 		name = "Isabeth"
		dynasty = "Carlier"
		birth_date = 2261.1.1
		adm = 2
		dip = 6
		mil = 2
		female = yes
    }
	heir = {
 		name = "Ales"
		monarch_name = "Ales I"
		dynasty = "Carlier"
		birth_date = 2281.1.1
		death_date = 2373.1.1
		claim = 100
		adm = 5
		dip = 4
		mil = 1
		female = yes
    }
}

2373.1.1 = {
    monarch = {
        name = "Regency Council"
        adm = 0
        dip = 0
        mil = 0
        regent = yes
    }
	heir = {
 		name = "Phelain"
		monarch_name = "Phelain I"
		dynasty = "Elles"
		birth_date = 2364.1.1
		death_date = 2382.1.1
		claim = 100
		adm = 4
		dip = 4
		mil = 5
    }
}

2382.1.1 = {
	monarch = {
 		name = "Ennemond"
		dynasty = "Falvo"
		birth_date = 2338.1.1
		adm = 3
		dip = 0
		mil = 5
    }
	queen = {
 		name = "Morrigh"
		dynasty = "Falvo"
		birth_date = 2347.1.1
		adm = 0
		dip = 5
		mil = 4
		female = yes
    }
	heir = {
 		name = "Dominique"
		monarch_name = "Dominique I"
		dynasty = "Falvo"
		birth_date = 2381.1.1
		death_date = 2447.1.1
		claim = 100
		adm = 4
		dip = 4
		mil = 3
		female = yes
    }
}

2447.1.1 = {
	monarch = {
 		name = "Ricque"
		dynasty = "Marose"
		birth_date = 2400.1.1
		adm = 0
		dip = 5
		mil = 6
    }
}

