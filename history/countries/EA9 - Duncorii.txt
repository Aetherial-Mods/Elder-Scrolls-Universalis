government = tribal
government_rank = 1
mercantilism = 1
technology_group = elsweyr_tg
religion = khajiiti_pantheon
primary_culture = khajiiti
capital = 940

54.1.1 = {
	monarch = {
 		name = "Hadamo"
		dynasty = "Rouhaan"
		birth_date = 1.1.1
		adm = 3
		dip = 5
		mil = 0
    }
}

123.1.1 = {
	monarch = {
 		name = "Zidal"
		dynasty = "Tovikmnirn"
		birth_date = 79.1.1
		adm = 1
		dip = 4
		mil = 3
    }
}

190.1.1 = {
	monarch = {
 		name = "Rinbu"
		dynasty = "Mohamlima"
		birth_date = 158.1.1
		adm = 6
		dip = 3
		mil = 0
    }
}

238.1.1 = {
	monarch = {
 		name = "Munaea"
		dynasty = "Jobaaaj-Dar"
		birth_date = 210.1.1
		adm = 4
		dip = 3
		mil = 4
		female = yes
    }
}

334.1.1 = {
	monarch = {
 		name = "Hadam-do"
		dynasty = "K'tabe"
		birth_date = 307.1.1
		adm = 3
		dip = 2
		mil = 0
    }
}

403.1.1 = {
	monarch = {
 		name = "Zhiraz"
		dynasty = "Ma'jahirr"
		birth_date = 364.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

447.1.1 = {
	monarch = {
 		name = "Shuva"
		dynasty = "Sarahasin"
		birth_date = 400.1.1
		adm = 3
		dip = 2
		mil = 5
		female = yes
    }
}

510.1.1 = {
	monarch = {
 		name = "Mobiba"
		dynasty = "Jodara"
		birth_date = 485.1.1
		adm = 1
		dip = 1
		mil = 2
		female = yes
    }
}

553.1.1 = {
	monarch = {
 		name = "Sabashur-dar"
		dynasty = "Javamnihn"
		birth_date = 503.1.1
		adm = 6
		dip = 1
		mil = 6
    }
}

615.1.1 = {
	monarch = {
 		name = "Makmargo"
		dynasty = "Sibari"
		birth_date = 586.1.1
		adm = 4
		dip = 0
		mil = 2
    }
}

661.1.1 = {
	monarch = {
 		name = "Halash"
		dynasty = "Zan'nor"
		birth_date = 643.1.1
		adm = 3
		dip = 6
		mil = 6
    }
}

760.1.1 = {
	monarch = {
 		name = "Abiznaz"
		dynasty = "Do'randru-Jo"
		birth_date = 715.1.1
		adm = 3
		dip = 3
		mil = 4
    }
}

826.1.1 = {
	monarch = {
 		name = "Bishu"
		dynasty = "Ja'bar"
		birth_date = 774.1.1
		adm = 2
		dip = 2
		mil = 1
		female = yes
    }
}

871.1.1 = {
	monarch = {
 		name = "Takanasur"
		dynasty = "Jodara"
		birth_date = 821.1.1
		adm = 0
		dip = 2
		mil = 4
    }
}

952.1.1 = {
	monarch = {
 		name = "Muzur"
		dynasty = "Anaihn"
		birth_date = 932.1.1
		adm = 5
		dip = 1
		mil = 1
    }
}

1050.1.1 = {
	monarch = {
 		name = "Khara-ko"
		dynasty = "Wadaiq"
		birth_date = 999.1.1
		adm = 3
		dip = 0
		mil = 5
		female = yes
    }
}

1094.1.1 = {
	monarch = {
 		name = "Birai"
		dynasty = "Ranrjo"
		birth_date = 1064.1.1
		adm = 2
		dip = 6
		mil = 1
		female = yes
    }
}

1178.1.1 = {
	monarch = {
 		name = "Tahuda"
		dynasty = "Rahkkar"
		birth_date = 1129.1.1
		adm = 4
		dip = 0
		mil = 3
    }
}

1243.1.1 = {
	monarch = {
 		name = "Raerabhi"
		dynasty = "M'arkhu"
		birth_date = 1206.1.1
		adm = 2
		dip = 0
		mil = 6
		female = yes
    }
}

1317.1.1 = {
	monarch = {
 		name = "Tashmin"
		dynasty = "Bahrajatani"
		birth_date = 1297.1.1
		adm = 0
		dip = 6
		mil = 3
    }
}

1358.1.1 = {
	monarch = {
 		name = "Riba"
		dynasty = "Amarsha"
		birth_date = 1332.1.1
		adm = 5
		dip = 5
		mil = 0
		female = yes
    }
}

1431.1.1 = {
	monarch = {
 		name = "Khuzi"
		dynasty = "Ahjhir"
		birth_date = 1395.1.1
		adm = 4
		dip = 4
		mil = 3
		female = yes
    }
}

1506.1.1 = {
	monarch = {
 		name = "Caska"
		dynasty = "Havnusopor"
		birth_date = 1463.1.1
		adm = 2
		dip = 3
		mil = 0
		female = yes
    }
}

1576.1.1 = {
	monarch = {
 		name = "Valashi"
		dynasty = "Helarr-Jo"
		birth_date = 1533.1.1
		adm = 0
		dip = 3
		mil = 4
		female = yes
    }
}

1621.1.1 = {
	monarch = {
 		name = "Rejmina"
		dynasty = "Khamnin"
		birth_date = 1587.1.1
		adm = 5
		dip = 2
		mil = 0
		female = yes
    }
}

1672.1.1 = {
	monarch = {
 		name = "Khurga"
		dynasty = "Urjobil"
		birth_date = 1632.1.1
		adm = 0
		dip = 3
		mil = 2
		female = yes
    }
}

1748.1.1 = {
	monarch = {
 		name = "Bilam"
		dynasty = "Kesharsha"
		birth_date = 1728.1.1
		adm = 5
		dip = 2
		mil = 5
    }
}

1807.1.1 = {
	monarch = {
 		name = "Kanrel"
		dynasty = "Hasgh"
		birth_date = 1785.1.1
		adm = 4
		dip = 2
		mil = 2
    }
}

1868.1.1 = {
	monarch = {
 		name = "Dagaril-dro"
		dynasty = "Bhijhad"
		birth_date = 1817.1.1
		adm = 2
		dip = 1
		mil = 6
    }
}

1923.1.1 = {
	monarch = {
 		name = "Turo"
		dynasty = "Zoarkir"
		birth_date = 1873.1.1
		adm = 0
		dip = 0
		mil = 2
    }
}

1977.1.1 = {
	monarch = {
 		name = "Sabarapa"
		dynasty = "Sarahasin"
		birth_date = 1932.1.1
		adm = 5
		dip = 6
		mil = 6
		female = yes
    }
}

2021.1.1 = {
	monarch = {
 		name = "Kankhu"
		dynasty = "Roudavi"
		birth_date = 1995.1.1
		adm = 4
		dip = 5
		mil = 3
    }
}

2113.1.1 = {
	monarch = {
 		name = "Chezan"
		dynasty = "Rahkkar"
		birth_date = 2076.1.1
		adm = 2
		dip = 5
		mil = 6
    }
}

2196.1.1 = {
	monarch = {
 		name = "Tulashurr"
		dynasty = "J'der"
		birth_date = 2149.1.1
		adm = 4
		dip = 6
		mil = 1
    }
}

2234.1.1 = {
	monarch = {
 		name = "Roznali"
		dynasty = "Sijsopor"
		birth_date = 2216.1.1
		adm = 2
		dip = 5
		mil = 5
		female = yes
    }
}

2317.1.1 = {
	monarch = {
 		name = "Uzernurr"
		dynasty = "Daro'urabi"
		birth_date = 2296.1.1
		adm = 0
		dip = 4
		mil = 1
    }
}

2397.1.1 = {
	monarch = {
 		name = "Omrasha"
		dynasty = "Roudavi"
		birth_date = 2373.1.1
		adm = 5
		dip = 3
		mil = 5
    }
}

2449.1.1 = {
	monarch = {
 		name = "Lazami"
		dynasty = "Zan'nor"
		birth_date = 2426.1.1
		adm = 4
		dip = 3
		mil = 2
		female = yes
    }
}

