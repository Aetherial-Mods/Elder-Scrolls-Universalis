government = republic
government_rank = 3
mercantilism = 1
technology_group = dwemer_tg
religion = reason_and_logic_cult
primary_culture = dwemer
capital = 7320

54.1.1 = {
	monarch = {
 		name = "Chzebchasz"
		dynasty = "Az'Kridhis"
		birth_date = 4.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

108.1.1 = {
	monarch = {
 		name = "Ylregriln"
		dynasty = "Aq'Snenard"
		birth_date = 82.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

191.1.1 = {
	monarch = {
 		name = "Ylregriln"
		dynasty = "Aq'Azsanwess"
		birth_date = 149.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

289.1.1 = {
	monarch = {
 		name = "Byrahken"
		dynasty = "Aq'Rlorlis"
		birth_date = 262.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

351.1.1 = {
	monarch = {
 		name = "Alnodrunz"
		dynasty = "Aq'Ararbira"
		birth_date = 316.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

412.1.1 = {
	monarch = {
 		name = "Nebgar"
		dynasty = "Af'Jragrenz"
		birth_date = 383.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

460.1.1 = {
	monarch = {
 		name = "Tadlin"
		dynasty = "Av'Chragrenz"
		birth_date = 441.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

547.1.1 = {
	monarch = {
 		name = "Tugradac"
		dynasty = "Af'Ynzatarn"
		birth_date = 515.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

627.1.1 = {
	monarch = {
 		name = "Ishevnorz"
		dynasty = "Az'Nhetchatz"
		birth_date = 587.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

689.1.1 = {
	monarch = {
 		name = "Mlirloar"
		dynasty = "Az'Mherbira"
		birth_date = 640.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

763.1.1 = {
	monarch = {
 		name = "Ynzatarn"
		dynasty = "Az'Szoglynsh"
		birth_date = 716.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

850.1.1 = {
	monarch = {
 		name = "Jrudit"
		dynasty = "Az'Ksrefurn"
		birth_date = 831.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

919.1.1 = {
	monarch = {
 		name = "Jlarerhunch"
		dynasty = "Av'Snelarn"
		birth_date = 870.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1006.1.1 = {
	monarch = {
 		name = "Chragrenz"
		dynasty = "Aq'Mlirtes"
		birth_date = 977.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1092.1.1 = {
	monarch = {
 		name = "Irhadac"
		dynasty = "Aq'Banrynn"
		birth_date = 1054.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1138.1.1 = {
	monarch = {
 		name = "Izvulzarf"
		dynasty = "Af'Dark"
		birth_date = 1106.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1217.1.1 = {
	monarch = {
 		name = "Yzranrida"
		dynasty = "Az'Azsamchin"
		birth_date = 1184.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1267.1.1 = {
	monarch = {
 		name = "Kolzarf"
		dynasty = "Az'Doudhis"
		birth_date = 1246.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1347.1.1 = {
	monarch = {
 		name = "Grubond"
		dynasty = "Az'Mrobwarn"
		birth_date = 1294.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1429.1.1 = {
	monarch = {
 		name = "Bhazchyn"
		dynasty = "Af'Achyggo"
		birth_date = 1401.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1523.1.1 = {
	monarch = {
 		name = "Yzranrida"
		dynasty = "Af'Asrahuanch"
		birth_date = 1500.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

1564.1.1 = {
	monarch = {
 		name = "Kolzarf"
		dynasty = "Aq'Chrotwern"
		birth_date = 1537.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

1599.1.1 = {
	monarch = {
 		name = "Grubond"
		dynasty = "Az'Ksrefurn"
		birth_date = 1549.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

1674.1.1 = {
	monarch = {
 		name = "Grigarn"
		dynasty = "Aq'Jhomrumhz"
		birth_date = 1654.1.1
		adm = 1
		dip = 1
		mil = 4
		female = yes
    }
}

1773.1.1 = {
	monarch = {
 		name = "Jnathunch"
		dynasty = "Av'Brehron"
		birth_date = 1735.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1868.1.1 = {
	monarch = {
 		name = "Inratarn"
		dynasty = "Az'Szoglynsh"
		birth_date = 1831.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

1954.1.1 = {
	monarch = {
 		name = "Irdarlis"
		dynasty = "Av'Chiuhld"
		birth_date = 1932.1.1
		adm = 4
		dip = 1
		mil = 1
		female = yes
    }
}

2025.1.1 = {
	monarch = {
 		name = "Dzredras"
		dynasty = "Aq'Grigarn"
		birth_date = 1999.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2124.1.1 = {
	monarch = {
 		name = "Jnathunch"
		dynasty = "Aq'Cheglas"
		birth_date = 2102.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2160.1.1 = {
	monarch = {
 		name = "Asradlin"
		dynasty = "Af'Nchyhrek"
		birth_date = 2139.1.1
		adm = 1
		dip = 4
		mil = 1
		female = yes
    }
}

2236.1.1 = {
	monarch = {
 		name = "Chzemgunch"
		dynasty = "Af'Inrarlakch"
		birth_date = 2196.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2282.1.1 = {
	monarch = {
 		name = "Rafk"
		dynasty = "Af'Nchynac"
		birth_date = 2250.1.1
		adm = 1
		dip = 4
		mil = 1
    }
}

2380.1.1 = {
	monarch = {
 		name = "Rhzorhunch"
		dynasty = "Af'Ghafuan"
		birth_date = 2347.1.1
		adm = 1
		dip = 1
		mil = 4
    }
}

2476.1.1 = {
	monarch = {
 		name = "Nchyhrek"
		dynasty = "Af'Asrahuanch"
		birth_date = 2450.1.1
		adm = 4
		dip = 1
		mil = 1
    }
}

