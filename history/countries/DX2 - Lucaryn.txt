government = native
government_rank = 1
mercantilism = 1
technology_group = daedric_tg
religion = molag_bal_cult
primary_culture = xivilai
capital = 4171

54.1.1 = {
	monarch = {
 		name = "Holavaris"
		dynasty = "Quored"
		birth_date = 8.1.1
		adm = 1
		dip = 2
		mil = 0
    }
}

148.1.1 = {
	monarch = {
 		name = "Zinmyar"
		dynasty = "Stenyeras"
		birth_date = 97.1.1
		adm = 2
		dip = 4
		mil = 2
    }
}

244.1.1 = {
	monarch = {
 		name = "Nerivaris"
		dynasty = "Jarthorn"
		birth_date = 219.1.1
		adm = 5
		dip = 5
		mil = 4
    }
}

314.1.1 = {
	monarch = {
 		name = "Faezorwyn"
		dynasty = "Reykian"
		birth_date = 263.1.1
		adm = 6
		dip = 2
		mil = 2
    }
}

404.1.1 = {
	monarch = {
 		name = "Fenjor"
		dynasty = "Trisynore"
		birth_date = 385.1.1
		adm = 2
		dip = 3
		mil = 5
    }
}

444.1.1 = {
	monarch = {
 		name = "Zyllee"
		dynasty = "Tolamar"
		birth_date = 414.1.1
		adm = 5
		dip = 5
		mil = 4
    }
}

511.1.1 = {
	monarch = {
 		name = "Beikas"
		dynasty = "Gralion"
		birth_date = 471.1.1
		adm = 3
		dip = 5
		mil = 1
    }
}

561.1.1 = {
	monarch = {
 		name = "Glynven"
		dynasty = "Ravacan"
		birth_date = 528.1.1
		adm = 1
		dip = 4
		mil = 4
    }
}

623.1.1 = {
	monarch = {
 		name = "Tyllartha"
		dynasty = "Qindove"
		birth_date = 582.1.1
		adm = 0
		dip = 3
		mil = 1
		female = yes
    }
}

721.1.1 = {
	monarch = {
 		name = "Heiphyra"
		dynasty = "Malcutorin"
		birth_date = 700.1.1
		adm = 5
		dip = 2
		mil = 4
    }
}

820.1.1 = {
	monarch = {
 		name = "Beikas"
		dynasty = "Xilcan"
		birth_date = 771.1.1
		adm = 3
		dip = 1
		mil = 1
    }
}

908.1.1 = {
	monarch = {
 		name = "Inadan"
		dynasty = "Jamekath"
		birth_date = 856.1.1
		adm = 5
		dip = 6
		mil = 4
    }
}

965.1.1 = {
	monarch = {
 		name = "Xilhorn"
		dynasty = "Trisynore"
		birth_date = 933.1.1
		adm = 6
		dip = 3
		mil = 2
    }
}

1025.1.1 = {
	monarch = {
 		name = "Lianorin"
		dynasty = "Reykian"
		birth_date = 1001.1.1
		adm = 4
		dip = 2
		mil = 5
		female = yes
    }
}

1062.1.1 = {
	monarch = {
 		name = "Erqen"
		dynasty = "Xilcan"
		birth_date = 1034.1.1
		adm = 4
		dip = 6
		mil = 6
    }
}

1110.1.1 = {
	monarch = {
 		name = "Jendris"
		dynasty = "Rodnan"
		birth_date = 1081.1.1
		adm = 2
		dip = 5
		mil = 2
		female = yes
    }
}

1145.1.1 = {
	monarch = {
 		name = "Aellarel"
		dynasty = "Quorune"
		birth_date = 1109.1.1
		adm = 0
		dip = 4
		mil = 6
		female = yes
    }
}

1206.1.1 = {
	monarch = {
 		name = "Alutrana"
		dynasty = "Yorron"
		birth_date = 1160.1.1
		adm = 5
		dip = 3
		mil = 3
		female = yes
    }
}

1275.1.1 = {
	monarch = {
 		name = "Mirawynn"
		dynasty = "Reynos"
		birth_date = 1239.1.1
		adm = 4
		dip = 3
		mil = 6
    }
}

1359.1.1 = {
	monarch = {
 		name = "Grelee"
		dynasty = "Mikrynnon"
		birth_date = 1308.1.1
		adm = 2
		dip = 2
		mil = 3
    }
}

1419.1.1 = {
	monarch = {
 		name = "Shapetor"
		dynasty = "Xilcan"
		birth_date = 1392.1.1
		adm = 0
		dip = 1
		mil = 6
    }
}

1461.1.1 = {
	monarch = {
 		name = "Herrieth"
		dynasty = "Quowin"
		birth_date = 1440.1.1
		adm = 2
		dip = 2
		mil = 1
    }
}

1526.1.1 = {
	monarch = {
 		name = "Morven"
		dynasty = "Balthor"
		birth_date = 1494.1.1
		adm = 0
		dip = 2
		mil = 5
		female = yes
    }
}

1606.1.1 = {
	monarch = {
 		name = "Omakian"
		dynasty = "Halen"
		birth_date = 1564.1.1
		adm = 6
		dip = 1
		mil = 1
		female = yes
    }
}

1646.1.1 = {
	monarch = {
 		name = "Umdorr"
		dynasty = "Keakian"
		birth_date = 1603.1.1
		adm = 4
		dip = 0
		mil = 5
    }
}

1706.1.1 = {
	monarch = {
 		name = "Olafir"
		dynasty = "Heiphyra"
		birth_date = 1684.1.1
		adm = 2
		dip = 6
		mil = 2
    }
}

1743.1.1 = {
	monarch = {
 		name = "Glynven"
		dynasty = "Keakian"
		birth_date = 1711.1.1
		adm = 0
		dip = 5
		mil = 5
    }
}

1812.1.1 = {
	monarch = {
 		name = "Omakian"
		dynasty = "Saladriel"
		birth_date = 1788.1.1
		adm = 6
		dip = 5
		mil = 2
		female = yes
    }
}

1849.1.1 = {
	monarch = {
 		name = "Miamys"
		dynasty = "Gabqinor"
		birth_date = 1816.1.1
		adm = 4
		dip = 4
		mil = 6
		female = yes
    }
}

1887.1.1 = {
	monarch = {
 		name = "Umexidor"
		dynasty = "Iansatra"
		birth_date = 1861.1.1
		adm = 6
		dip = 5
		mil = 0
		female = yes
    }
}

1926.1.1 = {
	monarch = {
 		name = "Elamaris"
		dynasty = "Quowin"
		birth_date = 1906.1.1
		adm = 4
		dip = 4
		mil = 4
		female = yes
    }
}

2000.1.1 = {
	monarch = {
 		name = "Umefaren"
		dynasty = "Grelee"
		birth_date = 1971.1.1
		adm = 2
		dip = 3
		mil = 0
    }
}

2042.1.1 = {
	monarch = {
 		name = "Faeceran"
		dynasty = "Nerivaris"
		birth_date = 1995.1.1
		adm = 0
		dip = 3
		mil = 4
		female = yes
    }
}

2141.1.1 = {
	monarch = {
 		name = "Quonor"
		dynasty = "Qindove"
		birth_date = 2104.1.1
		adm = 6
		dip = 2
		mil = 1
    }
}

2189.1.1 = {
	monarch = {
 		name = "Aexina"
		dynasty = "Quored"
		birth_date = 2166.1.1
		adm = 4
		dip = 1
		mil = 4
		female = yes
    }
}

2283.1.1 = {
	monarch = {
 		name = "Galered"
		dynasty = "Ivodar"
		birth_date = 2257.1.1
		adm = 2
		dip = 0
		mil = 1
    }
}

2376.1.1 = {
	monarch = {
 		name = "Corladon"
		dynasty = "Jefmeron"
		birth_date = 2345.1.1
		adm = 0
		dip = 6
		mil = 5
    }
}

2430.1.1 = {
	monarch = {
 		name = "Quonor"
		dynasty = "Marlyn"
		birth_date = 2381.1.1
		adm = 2
		dip = 1
		mil = 6
    }
}

