country_decisions = {
	
	united_vampire_duchies_nation = {
		major = yes
		potential = {
			NOT = { exists = VAM }
			capital_scope = { superregion = coldharbour_superregion }
			primary_culture = vampire
			NOT = { has_country_flag = formed_vam_flag }
		}
		provinces_to_highlight = {
		    OR = {
            province_id = 2897
			province_id = 2850
			province_id = 2710
			province_id = 2756
			province_id = 2834
			}
			NOT = { owned_by = ROOT }
		}
		allow = {
			is_free_or_tributary_trigger = yes
			is_at_war = no
			owns_core_province = 2897
			owns_core_province = 2850
			owns_core_province = 2710
			owns_core_province = 2756
			owns_core_province = 2834
		}
		effect = {
			change_tag = VAM
			set_country_flag = formed_vam_flag
			#
		    if = {
				limit = {
					NOT = { government_rank = 5 }
				}
				set_government_rank = 5
			}
			add_country_modifier = {
				name = "centralization_modifier"
				duration = 7300
			}
			add_prestige = 25
			if = { limit = { has_custom_ideas = no } country_event = { id = ideagroups.1 days = 31 } restore_country_name = yes }
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 150
	}
}