country_decisions = {

	republican_administration = {
		major = yes
		potential = {
			NOT = { government = republic }
			NOT = { government = tribal }
			NOT = { government = native }
			is_emperor_of_china = no
			is_emperor = no
		}
		allow =  {
			stability = 3
			full_idea_group = trade_ideas
			full_idea_group = diplomatic_ideas
			full_idea_group = maritime_ideas
			is_at_war = no 
			is_subject = no
			NOT = { primitives = yes }
		}
		effect = {
		    es_remove_stability_3 = yes
			change_government = republic
			change_government_reform_progress = -99999 country_event = { id = temporarytitle.1 }
		}
		ai_will_do = {
			factor = 0
		}
	}
	
	theocratic_administration = {
		major = yes
		potential = {
			NOT = { government = theocracy }
			NOT = { government = tribal }
			NOT = { government = native }
			is_emperor_of_china = no
			is_emperor = no
		}
		allow =  {
			stability = 3
			full_idea_group = mysticism_ideas
			full_idea_group = staatsverwaltung0
			full_idea_group = justiz0
			is_at_war = no 
			is_subject = no
			NOT = { primitives = yes }
		}
		effect = {
		    es_remove_stability_3 = yes
			change_government = theocracy
			change_government_reform_progress = -99999 country_event = { id = temporarytitle.1 }
		}
		ai_will_do = {
			factor = 0
		}
	}
	
	monarchic_administration = {
		major = yes
		potential = {
			NOT = { government = monarchy }
			NOT = { government = tribal }
			NOT = { government = native }
		}
		allow =  {
			stability = 3
			full_idea_group = smithing_ideas
			full_idea_group = mobility_ideas
			full_idea_group = generalstab0
			is_at_war = no 
			is_subject = no
			NOT = { primitives = yes }
		}
		effect = {
		    es_remove_stability_3 = yes
			change_government = monarchy
			change_government_reform_progress = -99999 country_event = { id = temporarytitle.1 }
		}
		ai_will_do = {
			factor = 0
		}
	}
	
	es_tribal_reform = {
		major = yes
		potential = {
			government = tribal
			NOT = { has_country_flag = es_tribal_reform_flag }
			is_year = 457
		}
		allow =  {
			stability = 3
			is_at_war = no 
			NOT = { is_lacking_institutions = yes }
			NOT = { primitives = yes }
		}
		effect = {
		    es_remove_stability_3 = yes
			change_government = monarchy
			change_government_reform_progress = -99999 country_event = { id = temporarytitle.1 }
			set_country_flag = es_tribal_reform_flag
		}
		ai_will_do = {
			factor = 0
		}
	}
	
	es_keep_tribal_reform = {
		major = yes
		potential = {
			government = tribal
			NOT = { has_country_flag = es_tribal_reform_flag }
			is_year = 457
		}
		allow =  {
			is_at_war = no 
			NOT = { is_lacking_institutions = yes }
			NOT = { primitives = yes }
		}
		effect = {
		    es_add_stability_1 = yes
			set_country_flag = es_tribal_reform_flag
		}
		ai_will_do = {
			factor = 1
		}
	}
	
}
