country_decisions = {

	# Ensalve population of a province WITH slaves as a trade good
	enslave_population = {
		potential = {
			has_reform = slavery
			NOT = { has_reform = abolition }
			any_owned_province = { NOT = { has_province_modifier = enslaved_province } trade_goods = slaves }
		}
		allow =  {
			any_owned_province = { NOT = { has_province_modifier = enslaved_province } trade_goods = slaves }
		}
		effect = {
		    add_years_of_income = 0.25
		    random_owned_province = { limit = { trade_goods = slaves NOT = { has_province_modifier = enslaved_province } }
			add_permanent_province_modifier = { name = "enslaved_province" duration = -1 } 
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	# Liberate slaves in one province
	liberate_population = {
		potential = {
			NOT = { has_reform = slavery }
			OR = {
				any_owned_province = { has_province_modifier = enslaved_province }
				AND = {
					NOT = { is_elven_nation_trigger = yes }
					any_owned_province = {
						has_province_modifier = es_enslaved_population
					}
				}
			}
		}
		allow =  {
		    years_of_income = 0.15
			adm_power = 50
			OR = {
				any_owned_province = { has_province_modifier = enslaved_province }
				AND = {
					NOT = { is_elven_nation_trigger = yes }
					any_owned_province = {
						has_province_modifier = es_enslaved_population
					}
				}
			}
		}
		effect = {
			add_years_of_income = -0.15
			add_adm_power = -50
			add_prestige = 5
			random_owned_province = {
				limit = {
					OR = { 
						has_province_modifier = enslaved_province
						has_province_modifier = es_enslaved_population
					}
				}
				# Slaves based on Trade Good
				if = {
					limit = {
						has_province_modifier = enslaved_province
					}
					remove_province_modifier = enslaved_province
					add_permanent_province_modifier = { name = "liberated_province" duration = 9125 }
				}
				# Slaves based on NOT trade Good
				else = {
					remove_province_modifier = es_enslaved_population
					add_permanent_province_modifier = { name = "liberated_province" duration = 9125 }
				}
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	# Liberate all Slaves and remove the trade good
	es_abolish_slavery = {
	    major = yes
		potential = {
			NOT = { has_reform = slavery }
			OR = {
				full_idea_group = humanist_ideas
				has_reform = abolition
			}
			any_owned_province = {
				OR = {
					trade_goods = slaves
					has_province_modifier = enslaved_province
					has_province_modifier = es_enslaved_population
				}
			}
			NOT = { has_reform = slave_monarchy_reform }
			NOT = { has_reform = pirate_king_reform }
			NOT = { has_reform = pirate_republic_reform }
			NOT = { has_reform = criminal_consortium_reform }
		}
		allow = {
			dip = 5
		}
		effect = {
			set_country_flag = es_abolished_slavery
			add_prestige = 10
			# Remove the trade good
			every_owned_province = {
				limit = { trade_goods = slaves }
				change_trade_goods = random
				set_province_flag = banned_slaves_production_flag
				remove_building = tradecompany
				add_permanent_province_modifier = { name = "liberated_province" duration = 9125 }
			}
			# Remove the modifiers
			every_owned_province = {
				limit = {
					OR = { 
						has_province_modifier = enslaved_province
						has_province_modifier = es_enslaved_population
					}
				}
				remove_province_modifier = enslaved_province
				remove_province_modifier = es_enslaved_population
				add_permanent_province_modifier = { name = "liberated_province" duration = 9125 }
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	# Eestore the salve trade good
	restore_slave_trade = {
		potential = {
			NOT = { has_reform = abolition }
			OR = {
				has_reform = slavery
				NOT = { full_idea_group = humanist_ideas }
			}
			OR = {
				has_country_flag = es_abolished_slavery
				any_owned_province = { has_province_flag = banned_slaves_production_flag }
			}
		}
		allow = {
			adm = 5
		}
		effect = {
			clr_country_flag = es_abolished_slavery
			add_prestige = -15
			every_owned_province = {
				limit = {
					has_province_flag = banned_slaves_production_flag
				}
				change_trade_goods = slaves
				clr_province_flag = banned_slaves_production_flag
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	es_bannes_production = {
		potential = {
		    OR = { has_reform = drug_limitation has_reform = prohibition }
			any_owned_province = {
				NOT = { has_province_flag = es_banned_production_flag }
				OR = {
					trade_goods = moon_sugar
					trade_goods = milk_of_kynareth
					trade_goods = skooma
					trade_goods = tobacco
					trade_goods = hist_sap
					trade_goods = incense_of_mara
					trade_goods = ayleid_nose_hash
					trade_goods = sleeping_tree_sap
					trade_goods = daril
					trade_goods = hags_breath
				}
			}
		}
		allow =  {
		    NOT = { prestige = 95 }
		}
		effect = {
		    add_prestige = 5
		    random_owned_province = {
			limit = {
				NOT = { has_province_flag = es_banned_production_flag }
				OR = {
					trade_goods = moon_sugar
					trade_goods = milk_of_kynareth
					trade_goods = skooma
					trade_goods = tobacco
					trade_goods = hist_sap
					trade_goods = incense_of_mara
					trade_goods = ayleid_nose_hash
					trade_goods = sleeping_tree_sap
					trade_goods = daril
					trade_goods = hags_breath
				}
			}
			if = { limit = { trade_goods = moon_sugar } set_province_flag = banned_sugar_production_flag }
			if = { limit = { trade_goods = milk_of_kynareth } set_province_flag = banned_milf_waters_of_kynareth_production_flag }
			if = { limit = { trade_goods = skooma } set_province_flag = banned_skooma_production_flag }
			if = { limit = { trade_goods = tobacco } set_province_flag = banned_tobacco_production_flag }
			if = { limit = { trade_goods = hist_sap } set_province_flag = banned_hist_sap_production_flag }
			if = { limit = { trade_goods = incense_of_mara } set_province_flag = banned_incense_of_mara_production_flag }
			if = { limit = { trade_goods = ayleid_nose_hash } set_province_flag = banned_ayleid_nose_hash_production_flag }
			if = { limit = { trade_goods = sleeping_tree_sap } set_province_flag = banned_sleeping_tree_sap_production_flag }
			if = { limit = { trade_goods = daril } set_province_flag = banned_daril_production_flag }
			if = { limit = { trade_goods = hags_breath } set_province_flag = banned_hags_breath_production_flag }
			change_trade_goods = random
			set_province_flag = es_banned_production_flag
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	es_remove_ban = {
		potential = {
			NOT = { has_reform = drug_limitation }
			NOT = { has_reform = prohibition }
			any_owned_province = {
				 has_province_flag = es_banned_production_flag
			}
		}
		allow =  {
		    prestige = 0 
		}
		effect = {
		    add_prestige = -5
		    random_owned_province = { 
				limit = {
					has_province_flag = es_banned_production_flag
				}
			if = { limit = { has_province_flag = banned_sugar_production_flag } clr_province_flag = banned_sugar_production_flag change_trade_goods = moon_sugar }
			if = { limit = { has_province_flag = banned_milf_waters_of_kynareth_production_flag } clr_province_flag = banned_milf_waters_of_kynareth_production_flag change_trade_goods = milk_of_kynareth }
			if = { limit = { has_province_flag = banned_skooma_production_flag } clr_province_flag = banned_skooma_production_flag change_trade_goods = skooma }
			if = { limit = { has_province_flag = banned_tobacco_production_flag } clr_province_flag = banned_tobacco_production_flag change_trade_goods = tobacco }
			if = { limit = { has_province_flag = banned_hist_sap_production_flag } clr_province_flag = banned_hist_sap_production_flag change_trade_goods = hist_sap }
			if = { limit = { has_province_flag = banned_incense_of_mara_production_flag } clr_province_flag = banned_incense_of_mara_production_flag change_trade_goods = incense_of_mara }
			if = { limit = { has_province_flag = banned_ayleid_nose_hash_production_flag } clr_province_flag = banned_ayleid_nose_hash_production_flag change_trade_goods = ayleid_nose_hash }
			if = { limit = { has_province_flag = banned_sleeping_tree_sap_production_flag } clr_province_flag = banned_sleeping_tree_sap_production_flag change_trade_goods = sleeping_tree_sap }
			if = { limit = { has_province_flag = banned_daril_production_flag } clr_province_flag = banned_daril_production_flag change_trade_goods = daril }
			if = { limit = { has_province_flag = banned_hags_breath_production_flag } clr_province_flag = banned_hags_breath_production_flag change_trade_goods = hags_breath }
			clr_province_flag = es_banned_production_flag
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
	
}