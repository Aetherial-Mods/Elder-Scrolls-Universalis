country_decisions = {

	elsweyr_nation = {
		major = yes
		potential = {
			NOT = { exists = ELS }
			culture_group = khajiiti_cg
			NOT = { has_country_flag = formed_els_flag }
		}
		provinces_to_highlight = {
		    OR = {
			province_id = 922		
			province_id = 5117
			province_id = 5226
			province_id = 5285
			province_id = 5136
			}
			NOT = { owned_by = ROOT }
		}
		allow = {
			is_free_or_tributary_trigger = yes
			is_at_war = no
			owns_core_province = 922		
			owns_core_province = 5117
			owns_core_province = 5226
			owns_core_province = 5285
			owns_core_province = 5136
		}
		effect = {
			change_tag = ELS
			set_country_flag = formed_els_flag
			#
			if = {
				limit = {
					NOT = { government_rank = 5 }
				}
				set_government_rank = 5
			}
			add_country_modifier = {
				name = "centralization_modifier"
				duration = 7300
			}
			add_prestige = 25
			if = { limit = { has_custom_ideas = no } country_event = { id = ideagroups.1 days = 31 } restore_country_name = yes }
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 150
	}
	
	anequinan_nation = {
		major = yes
		potential = {
			NOT = { exists = ANE }
			culture_group = khajiiti_cg
			capital_scope = {
				OR = {
					region = anequina_lr
					region = reapers_march_lr
				}
			}
			NOT = { has_country_flag = formed_ane_flag }
			NOT = { tag = ELS }
			NOT = { tag = PEL }
		}
		provinces_to_highlight = {
		    OR = {
			province_id = 5108		
			province_id = 5226
			province_id = 900
			province_id = 889
			province_id = 905
			}
			NOT = { owned_by = ROOT }
		}
		allow = {
			is_free_or_tributary_trigger = yes
			is_at_war = no
			OR = {
				AND = {
					owns_core_province = 5108		
					owns_core_province = 5226
					owns_core_province = 900
					owns_core_province = 889
					owns_core_province = 905
				}
				AND = {
					has_global_flag = thrassian_plague_flag
					owns_core_province = 5108
				}
			}
		}
		effect = {
			change_tag = ANE
			set_country_flag = formed_ane_flag
			#
			if = {
				limit = {
					NOT = { government_rank = 5 }
				}
				set_government_rank = 5
			}
			add_country_modifier = {
				name = "centralization_modifier"
				duration = 7300
			}
			add_prestige = 25
			if = { limit = { has_custom_ideas = no } country_event = { id = ideagroups.1 days = 31 } restore_country_name = yes }
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 150
	}
	
	pellitinian_nation = {
		major = yes
		potential = {
			NOT = { exists = PEL }
			culture_group = khajiiti_cg
			capital_scope = {
				OR = {
					region = pelletine_lr
					region = quin_rawl_lr
				}
			}
			NOT = { has_country_flag = formed_pel_flag }
			NOT = { tag = ELS }
			NOT = { tag = ANE }
		}
		provinces_to_highlight = {
		    OR = {
			province_id = 922		
			province_id = 5117
			province_id = 5195
			province_id = 5285
			province_id = 5155
			}
			NOT = { owned_by = ROOT }
		}
		allow = {
			is_free_or_tributary_trigger = yes
			is_at_war = no
			OR = {
				AND = {
					owns_core_province = 922		
					owns_core_province = 5117
					owns_core_province = 5195
					owns_core_province = 5285
					owns_core_province = 5155
				}
				AND = {
					has_global_flag = thrassian_plague_flag
					owns_core_province = 922
				}
			}
		}
		effect = {
			change_tag = PEL
			set_country_flag = formed_pel_flag
			#
			if = {
				limit = {
					NOT = { government_rank = 5 }
				}
				set_government_rank = 5
			}
			add_country_modifier = {
				name = "centralization_modifier"
				duration = 7300
			}
			add_prestige = 25
			if = { limit = { has_custom_ideas = no } country_event = { id = ideagroups.1 days = 31 } restore_country_name = yes }
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 150
	}
	
	hollowfang_clan_nation = {
		major = yes
		potential = {
			NOT = { exists = HOW }
			primary_culture = vampire
			capital_scope = { superregion = elsweyr_superregion }
			NOT = { has_country_flag = formed_how_flag }
			NOT = { tag = TEN }
		}
		provinces_to_highlight = {
		    OR = {
			province_id = 5252		
			province_id = 5106
			province_id = 899
			province_id = 5172
			province_id = 5051
			}
			NOT = { owned_by = ROOT }
		}
		allow = {
			is_free_or_tributary_trigger = yes
			is_at_war = no
			owns_core_province = 5252		
			owns_core_province = 5106
			owns_core_province = 899
			owns_core_province = 5172
			owns_core_province = 5051
		}
		effect = {
			change_tag = HOW
			set_country_flag = formed_how_flag
			#
			if = {
				limit = {
					NOT = { government_rank = 5 }
				}
				set_government_rank = 5
			}
			add_country_modifier = {
				name = "centralization_modifier"
				duration = 7300
			}
			add_prestige = 25
			if = { limit = { has_custom_ideas = no } country_event = { id = ideagroups.1 days = 31 } restore_country_name = yes }
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 150
	}
	
	tennar_zalviit_nation = {
		major = yes
		potential = {
			NOT = { exists = TEN }
			primary_culture = vampire
			capital_scope = { superregion = elsweyr_superregion }
			NOT = { has_country_flag = formed_ten_flag }
			NOT = { tag = HOW }
		}
		provinces_to_highlight = {
		    OR = {
			province_id = 5240		
			province_id = 5223
			province_id = 939
			province_id = 5226
			province_id = 889
			}
			NOT = { owned_by = ROOT }
		}
		allow = {
			is_free_or_tributary_trigger = yes
			is_at_war = no
			owns_core_province = 5240		
			owns_core_province = 5223
			owns_core_province = 939
			owns_core_province = 5226
			owns_core_province = 889
		}
		effect = {
			change_tag = TEN
			set_country_flag = formed_ten_flag
			#
			if = {
				limit = {
					NOT = { government_rank = 5 }
				}
				set_government_rank = 5
			}
			add_country_modifier = {
				name = "centralization_modifier"
				duration = 7300
			}
			add_prestige = 25
			if = { limit = { has_custom_ideas = no } country_event = { id = ideagroups.1 days = 31 } restore_country_name = yes }
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 150
	}
	
}