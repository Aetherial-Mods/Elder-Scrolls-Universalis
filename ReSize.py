import os
from PIL import Image

directory = os.getcwd()
flags_names = os.listdir(directory+"\\gfx\\flags")
print(flags_names)

for i in range(len(flags_names)):
    im = Image.open(directory+"\\gfx\\flags\\"+flags_names[i], "r")
    flag = im.load()
    if(im.size[0]>128):
        print(im.size[0])
        im.save(directory+"\\gfx_stuff\\flags_full_size\\"+flags_names[i])
        im_resized = im.resize((128, 128))
        flag_name = flags_names[i].split('.', 1)[0]
        im_resized.save(directory+"\\gfx\\flags\\"+flag_name+".tga")