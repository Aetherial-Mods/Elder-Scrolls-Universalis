### HIGH ROCK ###

#Lake Beletor - High Rock
lake = {
	triangle_strip = { 
        908 1934
		921 1937
		916 1920
		927 1920
	}
	height = 29
}

#Lake Elace - High Rock
lake = {
	triangle_strip = { 
        809 1904
		827 1919
		817 1899
		828 1911
	}
	height = 31
}

# Loch Llugwych - High Rock
lake = {
	triangle_strip = { 
        603 1861
		602 1879
		623 1862
		628 1877
	}
	height = 20
}

### SKYRIM ###

#Lake Ilinalta - Skyrim
lake = {
	triangle_strip = { 
        1240 1870
		1262 1879
		1295 1844
		1317 1858
	}
	height = 28
}

#Lake Geir - Skyrim
lake = {
	triangle_strip = { 
        1445 1848
		1476 1864
		1485 1851
		1474 1841
		1445 1848
	}
	height = 30
}

#Lake Honnith (Honrich) - Skyrim
lake = {
	triangle_strip = {
        1516 1828
		1545 1823
		1525 1806
		1545 1823
		1550 1793
		1583 1826
		1593 1796
	}
	height = 32
}

#Lake Yorgrim - Skyrim
lake = {
	triangle_strip = { 
		1447 1982
        1447 1997
		1480 1981
		1480 1998
	}
	height = -1
}

# Lake Neugrad
lake = {
	triangle_strip = { 
		1385 1806
		1376 1811
		1387 1815
		1382 1820
	}
	height = 35
}

# Sleeping Tree Lake
lake = {
	triangle_strip = { 
		1268 1915
		1268 1927
		1285 1915
		1285 1927
	}
	height = 19
}

# Silent Moons Lake
lake = {
	triangle_strip = { 
		1333 1941
		1326 1947
		1348 1961
		1339 1966
	}
	height = 22
}

### MORROWIND ###

#Lake Tonaara - Morrowind
lake = {
	triangle_strip = { 
        1657 1784
		1657 1798
		1673 1788
		1672 1800
		
	}
	height = 30
}

#Lake Boethiah - Morrowind
lake = {
	triangle_strip = { 
        2095 1772
		2075 1845
		2110 1788
		2095 1845
	}
	height = 21
}

### VALENWOOD ###

# Tanglehaven Lake - Valenwood
lake = {
	triangle_strip = {
		1056 1339
		1034 1363
		1078 1361
		1067 1377
	}
	height = 23
}

### CYRODIIL ###

# Kvatch Moat - Cyrodiil
lake = {
	triangle_strip = {
		1080 1511
		1075 1523
		1107 1532
		1091 1534
	}
	height = 23
}

# Walker Lake - Cyrodiil
lake = {
	triangle_strip = {
		1623 1740
		1618 1747
		1635 1740
		1635 1747
	}
	height = 31
}

# Arrius Lake - Cyrodiil
lake = {
	triangle_strip = {
		1584 1708
		1575 1712
		1600 1717
		1591 1727
	}
	height = 27
}

# Bruma Caverns Lake - Cyrodiil
lake = {
	triangle_strip = {
		1393 1730
		1392 1740
		1401 1730
		1401 1740
	}
	height = 24
}

# Glademist Lake - Cyrodiil
lake = {
	triangle_strip = {
		1303 1719
		1312 1722
		1316 1707
		1332 1711
	}
	height = 27
}

# Black Rock Lake - Cyrodiil
lake = {
	triangle_strip = {
		1250 1692
		1249 1702
		1256 1691
		1256 1701
	}
	height = 25
}

# Battlehorn Lake - Cyrodiil
lake = {
	triangle_strip = {
		1227 1653
		1225 1669
		1237 1653
		1238 1667
	}
	height = 25
}

# Donen Lake - Cyrodiil
lake = {
	triangle_strip = {
		1197 1682
		1206 1695
		1205 1678
		1212 1692
	}
	height = 30
}

### HAMMERFELL ###

# Dragonstar Lake - Hammerfell
lake = {
	triangle_strip = {
		997 1834
		997 1880
		1041 1834
		1041 1880
	}
	height = 24
}

# Thunder Falls Basin - Hammerfell
lake = {
	triangle_strip = {
		1077 1792
		1079 1807
		1094 1792
		1094 1807
	}
	height = 22
}

# Nimbel Lake - Hammerfell
lake = {
	triangle_strip = {
		1108 1811
		1108 1824
		1123 1811
		1123 1824
	}
	height = 26
}

# Reinhold Lake - Hammerfell
lake = {
	triangle_strip = {
		1140 1726
		1123 1748
		1156 1731
		1156 1743
	}
	height = 23
}

# Undertower Lake - Hammerfell
lake = {
	triangle_strip = {
		1128 1812
		1128 1829
		1144 1812
		1144 1829
	}
	height = 30
}

# Rkundzelft Lake - Hammerfell
lake = {
	triangle_strip = {
		1154 1802
		1154 1811
		1168 1802
		1168 1811
	}
	height = 27
}

# Hyrba Lake - Hammerfell
lake = {
	triangle_strip = {
		1158 1695
		1165 1704
		1170 1689
		1171 1698
	}
	height = 38
}

# Lake Curennius - Hammerfell
lake = {
	triangle_strip = {
		1117 1684
		1132 1703
		1146 1681
		1150 1706
	}
	height = 28
}

# Lake Munullus - Hammerfell
lake = {
	triangle_strip = {
		1120 1652
		1118 1665
		1140 1668
		1134 1676

	}
	height = 30
}

# Lake Hermillea - Hammerfell
lake = {
	triangle_strip = {
		1113 1645
		1110 1650
		1120 1650
		1117 1654

	}
	height = 28
}

# Lake Ag'Krazak - Hammerfell
lake = {
	triangle_strip = {
		1084 1611
		1075 1622
		1109 1637
		1102 1645
	}
	height = 22
}

### SUMMERSET ###

# Archon's Lake
lake = {
	triangle_strip = {
		650 1173
		658 1187
		670 1167
		666 1184
	}
	height = 28
}

# Lake Aurion
lake = {
	triangle_strip = {
		516 1190
		524 1209
		543 1192
		544 1206
	}
	height = 30
}

# Lake Heinarwe
lake = {
	triangle_strip = {
		504 1161
		504 1178
		516 1161		
		516 1178
	}
	height = 29
}

# Lake Sondsara
lake = {
	triangle_strip = {
		506 1124
		482 1135
		516 1151
		484 1143
	}
	height = 29
}