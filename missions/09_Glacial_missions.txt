flavour_missions_2_gla = {
	slot = 7
    generic = no
    ai = yes
    has_country_shield = no
    potential = { primary_culture = glacial }
	
	defenders_of_the_dragon_sanctuary_mission = {
        icon = dragon_sanctuary_mission
        position = 2
        required_missions = { } 
		
		provinces_to_highlight = {
			province_id = 1572
			NOT = { country_or_subject_holds = ROOT }
        }
    
        trigger = {
            owns = 1572
			1572 = { religion = ROOT }
        }
        
        effect = {
            add_country_modifier = {
				name = "es_protectors_of_the_dragon_sanctuary"
				duration = -1
			}
        }
	}
	
	restore_the_dragon_cult_mission = {
        icon = dragon_cult_tag
        position = 3
        required_missions = { defenders_of_the_dragon_sanctuary_mission } 
		
		provinces_to_highlight = {
			province_id = 1572
			NOT = { country_or_subject_holds = ROOT }
        }
    
        trigger = {
            OR = {
				tag = DRA
				AND = {
					NOT = { religion = dragon_cult }
					owns = 1572
				}
			}
        }
        
        effect = {
            if = {
				limit = {
					religion = dragon_cult
				}
				add_adm_power = 500
				add_dip_power = 500
				add_mil_power = 500
				every_owned_province = {
					limit = { NOT = { religion = dragon_cult } }
					change_religion = dragon_cult
				}
			}
			else = {
				1572 = { change_religion = dragon_cult spawn_rebels = { type = dragon_cult_rebels size = 3 win = yes unrest = 15 } }
				capital_scope = {
					spawn_rebels = { type = dragon_cult_rebels size = 3 }
				}
				random_owned_province = { spawn_rebels = { type = dragon_cult_rebels size = 3 win = yes unrest = 10 } }
				random_owned_province = { spawn_rebels = { type = dragon_cult_rebels size = 3 win = yes unrest = 10 } }
				random_owned_province = { spawn_rebels = { type = dragon_cult_rebels size = 3 win = yes unrest = 10 } }
			}
		}
	}
	
	nordic_subjugation_mission = {
        icon = windhelm_throne
        position = 6
        required_missions = { reclaim_tamriel_mission } 
		
		provinces_to_highlight = {
			is_capital = yes
			owner = { primary_culture = nord NOT = { is_subject_of = ROOT } }
		}
		
        trigger = {
			custom_trigger_tooltip = {
				tooltip = es_nordic_subjugation_tooltip
				NOT = {
					any_country = {
						primary_culture = nord
						is_free_or_tributary_trigger = yes
					}
				}
			}
        }
        
        effect = {
            add_country_modifier = {
				name = "es_nordic_subjugation"
				duration = -1
			}
        }
	}
}

flavour_missions_3_gla = {
	slot = 8
    generic = no
    ai = yes
    has_country_shield = no
    potential = { primary_culture = glacial }
	
	gold_of_the_north_mission = {
        icon = fur_mission
        position = 1
        required_missions = { } 
    
        trigger = {
            any_owned_province = {
				trade_goods = fur
			}
        }
		
		provinces_to_highlight = {
			trade_goods = fur
			NOT = { owned_by = ROOT }
        }
        
        effect = {
            add_country_modifier = {
				name = "es_fur_trade"
				duration = 9125
			}
        }
	}
	
	island_of_civilization_mission = {
        icon = nordic_village
        position = 2
        required_missions = { gold_of_the_north_mission } 
    
        trigger = {
			esu_steward = 1
			stability = 3
            any_owned_province = {
				base_tax = 10
			}
        }
        
        effect = {
            add_country_modifier = {
				name = "es_island_of_civilization"
				duration = -1
			}
        }
	}
	
	reform_the_glacial_tribe_mission = {
        icon = native_tribe_reform
        position = 3
        required_missions = { island_of_civilization_mission } 
    
        trigger = {
            NOT = { government = native }
        }
        
        effect = {
			add_prestige = 10
            add_country_modifier = {
				name = "es_tribal_reform"
				duration = 9125
			}
        }
	}
	
	the_new_era_mission = {
        icon = dragon_priest
        position = 4
        required_missions = { reform_the_glacial_tribe_mission restore_the_dragon_cult_mission protector_of_atmora_mission } 
		
		provinces_to_highlight = {
		    OR = {
			province_id = 479		
			province_id = 1342
			province_id = 1299
			province_id = 407
			province_id = 800
			}
			NOT = { country_or_subject_holds = ROOT }
		}
		
        trigger = {
            owns_or_subject_of = 479
            owns_or_subject_of = 1342
            owns_or_subject_of = 1299
			OR = {
				patriarch_authority = 1.0
				AND = {
					owns_or_subject_of = 407
					owns_or_subject_of = 800
				}
			}
        }
        
        effect = {
            add_country_modifier = {
				name = "es_the_dragon_era"
				duration = -1
			}
        }
	}
	
	reclaim_tamriel_mission = {
        icon = bromjunaar
        position = 5
        required_missions = { the_new_era_mission } 
		
		provinces_to_highlight = {
		    province_id = 1325		
			NOT = { country_or_subject_holds = ROOT }
		}
		
        trigger = {
            owns_or_subject_of = 1325
        }
        
        effect = {
			add_prestige = 10
			set_capital = 1325
			skyrim_superregion = {
				limit = {
					NOT = { country_or_subject_holds = ROOT }
				}
				add_core = ROOT
			}
        }
	}
}

flavour_missions_4_gla = {
	slot = 9
    generic = no
    ai = yes
    has_country_shield = no
    potential = { primary_culture = glacial }
	
	glacial_raiders_mission = {
        icon = nodic_conquest
        position = 1
        required_missions = { } 
		
        trigger = {
            num_of_cavalry = 10
			esu_hunter = 2
			army_tradition = 25
        }
        
        effect = {
			add_country_modifier = {
				name = "es_glacial_raiders"
				duration = 9125
			}
        }
	}
	
	protector_of_atmora_mission = {
        icon = atmoran_sanctuary
        position = 3
        required_missions = { glacial_raiders_mission island_of_civilization_mission } 
		
        trigger = {
            land_forcelimit = 35
			is_defender_of_faith = yes
        }
        
        effect = {
			add_country_modifier = {
				name = "es_protector_of_atmora"
				duration = 9125
			}
        }
	}
	
	legends_of_the_past_mission = {
        icon = national_epic
        position = 4
        required_missions = { protector_of_atmora_mission } 
		
        trigger = {
            prestige = 100
			esu_bard = 3
        }
        
        effect = {
			add_country_modifier = {
				name = "es_glacial_legends_of_the_past"
				duration = -1
			}
        }
	}
	
	restore_forelhost_mission = {
        icon = forelhost
        position = 6
        required_missions = { reclaim_tamriel_mission }

		provinces_to_highlight = {
			area = skyrim_58_la
			NOT = { country_or_subject_holds = ROOT }
        }		
		
        trigger = {
            skyrim_58_la = {
				type = all
				country_or_subject_holds = ROOT
			}
        }
        
        effect = {
			add_prestige = 10
			every_owned_province = {
				limit = {
					area = skyrim_58_la
				}
				cede_province = FRS
				add_core = FRS
			}
			create_subject = { subject_type = march subject = FRS }
			3119 = {
				add_permanent_province_modifier = {
					name = "03_walls_of_forelhost"
					duration = -1
				}
				if = {
					limit = {
						NOT = { has_building = fort_15th }
						NOT = { has_building = fort_16th }
						NOT = { has_building = fort_17th }
						NOT = { has_building = fort_18th }
					}
					add_building = fort_15th
				}
			}
        }
	}
}