##############################
Version: 3.2.6
Date: 29.03.24
EU4 Version: 1.36.*
##############################

### Additions ###
- Added Alchemy Mechanic, allowing a player to use ingredients for brewing powerful potions
- Added 107 Alchemy-related events
- Added Smithing Mechanic, allowing a player to select materials and smithing technique, which will be used for army composition
- Added Tonal Architecture Mechanic for Dwemer nations, allowing them to use the power of Tonal Resonators
- Added 55 new Trade Goods
- Added 11 new flavour Dwemer events
- Added event to give the player a choice to become a new EoC or dismantle the Cyrodiilic Empire
- Added event to remind a player that it is possible to disable regions to improve performance
- Added Tonal Engineer advisor with a unique portrait

### Changes ###
- Shifted the spawn of the Alessian Empire during the Alessian Slave rebellion to a nation-scale instead of a province-scale.
- Rebalanced rewards for some government reform mechanics
- Vvardenfell is now a more habitable place due to new bonuses for permanent Ashland modifiers
- Dwemer events for the construction of underground buildings will no longer fire if you are poor
- Rebalanced most of the Trade Goods modifiers to remove unuseful bonuses
- Rebalanced the spawn of all trade goods to make it more equal and count for all present terrain types
- All missions that require to own a specific number of provinces with a particular trade good now only need one such province
- Renamed Keptu State to Forest Kingdom
- Now, to rebuild Orsinium, you will actually need to own that province
- HRE members can no longer annex or integrate other HRE members unless either of the pre-final reforms is passed
- Increased the desire of AI to join HRE, vote for the Emperor and pass reforms
- Updated icons for most of all existing personal deities and added additional icons for different variations of the same deity
- Reduced the number of upgraded trade centres to pass the generic mission tree
- Oblivion nations can now bypass the generic mission with Coastal Defence buildings
- Increased the desire of AI to become a vassal via elevated diplomatic action
- The Emperor can no longer enforce Religious Unity Action if he or the target is at war
- Bonuses for State-wide buildings are not also applied to the province with the building itself
- Reduced the State-wide governing cost bonuses

### Fixes ###
- Fixed Elevate Alliance Member diplomatic action
- Fixed tooltips for some government reform mechanics
- Fixed the development bonus of some Holy Orders
- Fixed various small bugs
- Fixed minor issues with localisation

### Deletions ###
- Removed Dwemer Influence decisions