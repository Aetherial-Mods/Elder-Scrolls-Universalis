##############################
Version: 2.3.9
Date: 04.06.2022
EU4 Version: 1.33.*
##############################

### Additions ###
- Added Orsimer mission tree
- Added Ogre culture
- Added Lamian culture
- Added 5 Beverages events
- Added 2 Ogre events
- Added 1 Lamian events
- Added 5 Trade events
- Added 7 Maormer events
- Added 2 Mercantilism events 
- Added 6 Flavour events for 2nd Era
- Added 10 Harpy events
- Added 12 Tribal events
- Added Descriptions for all of the 53 Holy Orders 
- Added National Ideas for Scamp culture
- Added National Ideas for Imga culture
- Added National Ideas for Skaal culture
- Added National Ideas for Goblin culture
- Added National Ideas for Falmer culture
- Added National Ideas for Hollow culture
- Added National Ideas for Volkihar
- Added National Ideas for Tamriel
- Added National Ideas for Cyrodiil
- Added National Ideas for Colovia
- Added National Ideas for Heartlands
- Added National Ideas for Nibenay
- Added National Ideas for Alessian Empire
- Added National Ideas for Alessian Order
- Added National Ideas for Golden Coast
- Added National Ideas for Jerall Mountains
- Added National Ideas for Valus Mountains
- Added National Ideas for Akaviri Potentate
- Added National Ideas for Horder of the Hour
- Added National Ideas for Anvil
- Added National Ideas for Bravil
- Added National Ideas for Bruma
- Added National Ideas for Cheydinhal
- Added National Ideas for Chorrol
- Added National Ideas for Kvatch
- Added National Ideas for Leyawiin
- Added National Ideas for Skingrad
- Added Nocturnal Sisterhood government reform
- Added Reformation Center for Nocturnal Cult
- Added some historical lucky nations, but this feature is still WIP
- Added new localisation lines to German, French and Spanish localisations
- Added the Endgame mechanics

### Changes ###
- Increased Income from Base Tax
- Increased the number of Sailors
- Max Combat Dice Side is increased from 6 to 10
- Decreased terrain defence modifiers by 1 for all terrain types
- Females may always rule the nation with Nocturnal Sisterhood government reform
- Females may no longer rule over countries with Ashland Chimer or Ashland Dunmer primary culture
- The City of Mages monument now requires the owner to have Mages Colleges privilege
- Accepted cultures may no longer be converted using decisions of Vampires, Tsaesci and Soul-Shriven
- Updated triggers for Nordic and Breton missions to make them more clear and avoid buggy situations
- Events for Statists/Orangists mechanics should now fire more often

### Fixes ###
- Fixed minor localisation issues
- Fixed Oblivion Centers of Trade
- Fixed Ysgramor Dynasty for Windhelm
- Fixed various minor bugs

### Deletions ###
- Deleted two 2nd Era events, as they had no unique picture and were merged with the other two events

### Miscellaneous ###
- Changed WIN tag to SNW tag to avoid code issues