##############################
Version: 3.0.2
Date: 19.03.2023
EU4 Version: 1.34
##############################

### Additions ###
- Added Trade Centers for all the Tamriel & Roscrea
- Added 16 Idea Sets for Khajiiti Nations
- Added 3 new Events for Students of Magnus religion
- Added 10 new Personal Deities for Nordic Pantheon (Pantheons of Atmora and Tamriel are now split)
- Completed Khajiiti Fur Trade event chain
- Added a bunch of new Ruler Titles
- Added the Idea Ser for Abeceanean pirates

### Changes ###
- Miraak Temple Great Project now requires only 100% of religious unity - Should make the start of the Miraak event chain much easier.
- AI is now encouraged to build more buildings

### Fixes ###
- Fixed Scenario 3: Second Nordic-Elven War
- Fixed various minor issues