##############################
Version: 3.0.0
Date: 28.01.23
EU4 Version: 1.34.*
##############################

### Overhauls ###
- Complete Map Overhaul of High Rock
- Complete Map Overhaul of Skyrim
- Complete Map Overhaul of Morrowind, including Vvardenfell
- Complete Map Overhaul of Hammerfell
- Complete Map Overhaul of Cyrodiil
- Complete Map Overhaul of Black Marsh
- Complete Map Overhaul of Elsweyr
- Complete Map Overhaul of Valenwood
- Complete Map Overhaul of Summerset Islands
- Map Overhaul of Roscrea
- Overhaul of Terrain Textures
- Sea lane added to Akavir

### Additions ###
- Added New Countries
- Added New Religions: Druidism, Keptu Pantheon, Forgotten Insect God, Cult of Miraak and Magne-Ge
- Added New Cultures: Horsemen, Giant Goblins, Kreathmen, Naga, Hapsleet, Paatru, Sarpa, Orma, Yespest, Horwalli, Riekr, Bird Men, Centaur
- Added Lava for Morrowind
- Added New Flags
- Added New on map models
- Added Dominion Unit Model
- Added Keptu Pantheon as a new Religion
- Added Cult of Sithis as a new Religion
- Added Cult of Miraak as a new Religion
- Added 1 New event for Nordic Mission Tree
- Added 10 New events for Khajiiti Pantheon religion
- Added 6 New events for Miraak Cult religion
- Added 1 New Flavour event
- Added A NEW HAND TOUCHES THE BEACON line
- Added New Decisions for Formable nations
- Added New Monuments
- Added various fixed trade goods for some provinces
- Added Mission Tree for Khajiits and Solitude
- Added Archein Ideas
- Added Agaceph Ideas
- Added Naga Ideas
- Added Hapsleet Ideas
- Added Sarpa Ideas
- New Terra Incognita Overlay
- Blackreach Cavern System
- Forgotten Vale access through Darkfall Passage

### Changes ###
- Placed Sithis Cult religion on the map
- Renamed Cult of Lorellia to Nereid Cult
- Positions of Reachmen Schools are now fixed
- Moved Keptu into its own Ket Keptu Culture Group
- Improved various Flags
- Improved Altmer models
- Improved Argonian models
- Improved Maormer models
- Updated Xorme AI integration
- Changed Infantry and Cavalry breakthrough from 1.0 to 0.15
- Nerfed Tribal Development Growth for Natives
- Increased Ahead of Time Technology bonuses
- Rebalanced Lunar Lattice bonuses
- Ogres now follow the Cult of Malacath instead of the Cult of Ancestors

### Fixes ###
- Barsaebic Ayleids now have Ayleid models as well
- Fixed Snow Elf Models
- Fixed Altmer Models
- Fixed Corelayan Models
- Fixed Maormer Models
- Fixed Russian and Cossack War Exhaustion abilities
- Fixed High Rock city foundation decisions
- Fixed Iron Orc mission tree
- Fixed various minor bugs in the code
- Fixed estate agenda event target names not displaying
- Fixed various minor localisation issues
- Fixed mission tooltips
- Fixed various minor bugs

### Deletions ###
- Removed Alessian Revolt Bookmark (temporarily)
- Removed Most of the Diplomatic ties on Tamriel
- Removed remaining vanilla music