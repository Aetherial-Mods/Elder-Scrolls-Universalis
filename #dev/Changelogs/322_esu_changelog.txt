##############################
Version: 3.2.2
Date: 07.11.23
EU4 Version: 1.36
##############################

### Changes ###
- Updated the mod to the 1.36 version of the base game
- Great Orsimer Clan reform is also allowed for Iron Orsinium
- Razing a province now also adds a picture to its modifiers
- Great Project's names in Ledger are now more visible
- Added notification for state edicts, indicating when you might want to change them
- Building of infrastructure, production, taxation, trade and manufactory buildings increases prosperity

### Fixes ###
- Fixed an issue with idea changing event, not changing non-generic missions
- Fixed minor localisation issues

##############################
Version: 3.2.2a
Date: 07.11.23
EU4 Version: 1.36
##############################

### Additions ###
- Added Icons for Bi-Lunar Guard and Vinedusk Rangers Special Units
- Added support for Bi-Lunar Guard and Vinedusk Rangers Special Units, available for Bosmeri and Khajiiti Nations (Their localisation is still WIP though, so they are still named as Mamluks and Qizilbash)

### Changes ###
- Added Highlands and Hills terrain type for AI evaluations

### Fixes ###
- Fixed a couple of minor code issues related to the 1.36 update
- Fixed Elections of Nordic Emperor
- Fixed a bunch of Vanilla tooltips