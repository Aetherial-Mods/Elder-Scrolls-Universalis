##############################
Version: 2.3.5
Date: 02.03.22
EU4 Version: 1.33.*
##############################

### Additions ###
- Compatibility changes for 1.33 update of EU4
- Added 9 new Trade Goods

### Changes ###
- Reduced max amount of Client States to 15, but they are going to be available since the year 300, when most Maormer nations complete reforms and get away from Doom.
- Forts are available from the start of the game

### Fixes ###
- Fixed various small issues