##############################
Version: 2.3.6
Date: 10.03.22
EU4 Version: 1.33.*
##############################

### Additions ###
- Added 10 new events for Elective monarchy
- Added 15 new events for Dynasties
- Added 8 new events for Statists Vs Orangists mechanics 
- Added new trade goods to Manufactories
- Added localisation for a bunch of new trade goods

### Changes ###
- Rebalanced National Ideas
- Rebalanced Chimer Cults

### Fixes ###
- Fixed CTD issues
- Fixed various small issues