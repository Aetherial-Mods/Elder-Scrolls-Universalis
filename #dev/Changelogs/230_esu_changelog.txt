##############################
Version: 2.3.0
Date: 30.12.21
EU4 Version: 1.32.*
##############################

### Additions ###
- Conquest info events - Learn about the different regions of Nirn as you expand your territory! (These can be disabled in the mod menu)
- 54 new Holy Orders with unique bonuses - each religion has 3 of them
- 12 new random flavour Events for Akaviri Religions
- The Old Orc event chain - a little easter egg for those familiar with Skyrim or Morrowind
- Added mechanics, that will reduce your government rank, if you no longer have enough development for it
- Added new colonial region and trade node to Yokuda
- Health System was recreated and readded with a bunch of new events.
- Added State Edicts for nations under Dictatorship rule
- Added glowing mark for easy countries for people, who are new to ESU
- Health System was expanded with different flavour text and additional events

### Changes ###
- Updated some daedric deity icons
- New image for the Halls of Colossus monument
- Lowered max camera height to fix those red bars on the edges of the screen
- Reduced the base size of rebels
- Vassal nations provide +1 manpower instead of +1 force limit
- Changed chance of music themes for compatibility with Themes of the Old World
- Reduced the requirements to become a hegemon
- Reduced the base loan interest to 5% and increased its duration to 75 months
- Increased the base general, admiral, conquistador and explorer cost to 100 mil/dip points
- Nerfed drill decay bonuses from governments, ideas and policies
- Replaced +25% land/naval bonus for Ai nations with -2,5% army/navy tradition decay and added +1 pip to all general/admiral skill
- Changed negative bonuses of hostile policy towards natives with positive
- Increased number of regiments under one AI general to 50
- Nerfed leader cost bonus from governments, ideas and policies
- AI nations get +15% of religion unity instead of +2,5% heretic missionary strength
- Recruiting a general no longer boosts army professionalism by 1%
- Changed Professionalism bonuses
- Changed war and war exhaustion penalties. Now it should be easier to stay in the war, but more costly to suffer war exhaustion
- Reduced the basic size of the rebel army back to 3.5
- Changed the duration of modifiers, given by Flavour religious events from 25 to 10 years
- Client states can now be created only since 100 and maps can be stolen only since 75 diplomatic technology level
- Bonuses from sending officers to Marches and having 100% drilled army are nerfed
- Client states will have +15% manpower instead of 15% army morale
- Dictatorship rule can no longer be ended by switching the reform
- It is impossible to start a war between Oblivion and not Oblivion nations if the End Game Crises is not initiated
- During End-Game Crisis Oblivion nations cannot use Destroy the Great nation Casus Belli
- AI now has a permanent base resistance for the creation of Personal Union via Diplomatic Action
- Community Militia increases the autonomy only by 0.05 instead of 0.10
- The Land and naval maintenance is increased based on your army and navy traditions
- Buildings should now boost development, when they are built
- Restored Difficulty bonuses for the player or AI
- Added glowing effect in the main menu for easy countries
### Fixes ###
- Fixed Halls of Colussus name (was "Hall of Colossus")
- Fixed holiday events to fit 365-day year
- Fixed triggers for age splendour
- Fixed interesting nations screen for users with a narrow monitor
- Fixed PU nations, that for some reason were not monarchies
- Soul Shriven nations will no longer convert their provinces to the vampire culture
- Fixed spawn of 8 divines religion before its foundation
- Fixed Peasant War disaster, triggered on start by low manpower
- Fixed declaring war on your subject nation via lore event
- Fixed a travel issue with Oblivion Gates
- Fixed Canals and Oblivion Gates
- Fixed some event pictures for the Linux users
- Fixed missing deities for the Ayleid pantheon