##############################
Version: 2.4.2
Date: 03.09.2022
EU4 Version: 1.33.*
##############################

### Additions ###
- Added National Ideas for Orsinium
- Added National Ideas for Wrothgar
- Added National Ideas for Winterborn Clan
- Added National Ideas for Wayrest
- Added National Ideas for Evermore
- Added National Ideas for White-Gold Tower
- Added National Ideas for Ayleid State
- Added Druidism as a WIP religion
- Added 7 New Generic Events
- Added 4 New Events for Native Nations
- Added 1 New Event for Emperor of China/Akavir
- Added 10 New Events for Parliaments
- Added 6 New Events for Nations, that follow Aedric Religions 
- Added New Governemnt Reform for Wilderking
- Added 10 New Levels of Professionalism

### Changes ###
- Rebalanced Daggerfall National Ideas
- Replaced Hist Karma mechanics with Secondary Religion Mechanics
- Changed Event Pictures for Lunar Lattice Events
- Added Extra Leader Slot for 200 Forcelimit
- Increased Basic Revolt size from 1 to 2.5
- Invading Oblivion nations now spawn 45K army on the other side of the portal
- Abolition reform no longer can be enforced, while other Slave Reforms are enforced
- Subject nations now contribute from 5 to 25% of their forcelimit to the Overlord
- Nation, which occupies the capital of Emperor of China/Akavir now automatically becomes the new Emperor of China/Akavir
- Increased frequency of Extend Regency events
- Ogres should no longer spawn outside Tamriel

### Fixes ###
- Fixed some Estate Agendas
- Fixed Spawn of Slave Rebels
- Fixed a couple of events, firing when their goal is already completed
- Fixed some spelling issues
- Fixed missions, which required some specific idea to be completed