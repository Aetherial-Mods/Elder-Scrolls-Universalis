##############################
Version: 2.2.8
Date: [Date Released]
EU4 Version: 1.32.*
##############################

### Additions ###
- Kothringi Models
- Reachmen Models
- 5 New events for Ayleids of Cyrodiil
- 2 new decisions
- New Casus belli
- 11 Flavour Religious events for Yokudan and Redguard Pantheons

### Changes ###
- The birthsign event now shows the modifiers that the birthsign applies.
- Update of Ayleid Unit models
- Increased the unrest bonus from high stability
- Increased chance of Ice Age event

### Fixes ###
- Fix of various small bugs