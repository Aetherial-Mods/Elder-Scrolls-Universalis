##############################
Version: 2.4.3
Date: 20.09.2022
EU4 Version: 1.34.*
##############################

### Additions ###
- Added Russian Mechanics for Merchant Republics, Trading Cities, Pirate Republics, Federal Republics
- Added Iqta Mechanics to Military Dictatorships, Akaviri Monarchies, Celestial Council
- Added Militarisation Mechanics to Feudal Monarchy
- Added Dragonguard Special Units to Akaviri Republics, Akaviri Monarchies, Elven Monarchies
- Added Feudal Theocracy Mechanics to Human Regnum, Tribunal gov reform, Sisterhood gov reform
- Added Hussars as Dwemer Automats for Dwemers
- Added new Mission to Dwemers, which allows construction of Dwemer Automats
- Added Additional bonuses to Pyandonean Mission tree, to make it more balanced
- Added new Dragon Culture
- Added 2 new flavor events featuring Dragons and Maormers
- Added 3 new loading screens

### Changes ###
- Uopdated the mod files to 1.34 version of the base game
- Nerfed Orcish Fury from +25% Moral and +15% Unit Power to +15% Moral and +5% Unit Power
- Replaced one Harpy Event Picture with another one, as the first one was already used in Calendar events
- Gold Spawn event is limited to countries with more than 50 cities
- Ogre Spawn event is limited to countries with more than 5 cities 
- Renamed Gov Interactions mechanics of Cossacks, Russian, Iqta, Mamluk mechanics
- Renamed Banners to Dragonguard, Rajputs to Elite Warriors, Cossacks to Brigants, Janissaries to Sovereign Wardens, Streltsy to National Guardsmen, Hussars to Dwemer Automats, Cawa to Exalted Warriors, Caroleans to Dragons
- Renamed Steppe Hordes to Nomad Tribes
- Renamed Orthodoxy-Patriarchy Localisation lines to fit the Dragon Cult
- Renamed Flower Wars to Serpent Wars

### Fixes ###
- Fixed localisation of some National Ideas
- Some immediate code in Tang Mo Missions is now hidden
- Fixed the description of the Arkngthamz Holy Site
- Fixed Governemnt reforms for Bosmer Monarchies, that were missing one reform due to the new Wilderking gov reform.
- Fixed Pyandonean Mission tree for those, who do not have Rule Brittania DLC.

### Deletions ###
- Removed Dragonguard Special Units from Mercenary gov reform
- Removed Flags from city province models to boost perfomance
