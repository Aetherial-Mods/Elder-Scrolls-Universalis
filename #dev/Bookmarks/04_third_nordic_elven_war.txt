bookmark = {
	name = THIRD_NORDIC_ELVEN_WAR
	desc = THIRD_NORDIC_ELVEN_WAR_DESC
	date = 139.1.1
	
	center = 1313
	default = yes
	
	country = SNW
	country = FRS
	country = REA
	country = CB8
}