#########################
### Country Modifiers ###
#########################

### Military & Navy Theory
army_tradition = 1
army_tradition_decay = -0.01
army_tradition_from_battle = 0.01
yearly_army_professionalism = 0.01
drill_gain_modifier = 0.10
drill_decay_modifier = -0.10

navy_tradition = 1
navy_tradition_decay = -0.01
naval_tradition_from_battle = 0.1
naval_tradition_from_trade = 0.1

attack_bonus_in_capital_terrain = 1
military_tactics = 0.25
global_defender_dice_roll_bonus = 1
own_territory_dice_roll_bonus = 1
global_attacker_dice_roll_bonus = 1

### War Economy
infantry_cost = -0.10
cavalry_cost = -0.10
artillery_cost = -0.10
fort_maintenance_modifier = -0.10
rival_border_fort_maintenance = -0.10
garrison_size = 0.10
global_garrison_growth = 0.10
war_exhaustion = -0.10
war_exhaustion_cost = -0.10
war_taxes_cost_modifier = -0.10
global_regiment_cost = -0.10
global_regiment_recruit_speed = -0.10
global_supply_limit_modifier = 0.10
land_forcelimit = 1
land_forcelimit_modifier = 0.10
land_maintenance_modifier = -0.10
loot_amount = 0.10
reinforce_cost_modifier = -0.10
reinforce_speed = 0.10
available_province_loot = 0.10

movement_speed = 0.10
regiment_disembark_speed = 0.25
land_attrition = -0.10
hostile_attrition = 1
max_hostile_attrition = 3
prestige_from_land = 0.10
raze_power_gain = 0.10

### Land Warfare
infantry_power = 0.10
cavalry_power = 0.10
artillery_power = 0.10
infantry_fire = 1
cavalry_fire = 1
artillery_fire = 1
infantry_shock = 1
cavalry_shock = 1
artillery_shock = 1
cav_to_inf_ratio = 0.10
cavalry_flanking = 0.10
artillery_levels_available_vs_fort = 1
backrow_artillery_damage = 0.10
siege_blockade_progress = 1
reserves_organisation = 0.10
discipline = 0.01
land_morale = 0.10
recover_army_morale_speed = 0.10
fire_damage = 0.10
fire_damage_received = -0.10
shock_damage = 0.10
shock_damage_received = -0.10
morale_damage = 0.10
morale_damage_received = -0.10
defensiveness = 0.10
siege_ability = 0.10
artillery_barrage_cost = -0.10
artillery_level_modifier = 0.10 # Multiplies artillery during sieges
assault_fort_cost_modifier = -0.10
assault_fort_ability = 0.10
land_morale_constant = 1 # Adds a flat amount of morale
garrison_damage = 0.10 # Damage the garrison deals

### Manpower
manpower_in_accepted_culture_provinces = 0.10
manpower_in_culture_group_provinces = 0.10
manpower_in_own_culture_provinces = 0.10
manpower_in_true_faith_provinces = 0.10
global_manpower = 1
global_manpower_modifier = 0.10
manpower_recovery_speed = 0.10
regiment_manpower_usage = -0.1

### Mercenaries
mercenary_manpower = 0.10
mercenary_cost = -0.10
merc_maintenance_modifier = -0.10
mercenary_discipline = 0.10
allow_mercenary_drill = yes
merc_leader_army_tradition = 0.10
merc_independent_from_trade_range = yes # Recruit any Mercs you want

### Special Units
special_unit_forcelimit = 0.10
special_unit_cost_modifier = -0.10
has_banners = yes
has_<special_unit> = yes #Country scope, unlocks the special unit for the country - Needs testing
amount_of_banners = 0.10
amount_of_cawa = 0.10
cawa_cost_modifier = -0.10
has_carolean = yes
amount_of_carolean = 0.10
can_recruit_hussars = yes
amount_of_hussars = 0.10
allowed_marine_fraction = 0.1
allowed_cossack_fraction = 0.1
allowed_rajput_fraction = 0.1
can_recruit_cawa = yes
can_recruit_cossacks = yes
can_recruit_rajputs = yes
can_recruit_revolutionary_guards = yes
janissary_cost_modifier = -0.10

### Generals & Admirals
free_leader_pool = 1
leader_cost = -0.10
general_cost = -0.10
may_recruit_female_generals = yes
leader_land_fire = 1
leader_land_manuever = 1
leader_land_shock = 1
leader_siege = 1
leader_naval_fire = 1
leader_naval_manuever = 1
leader_naval_shock = 1

### Navy
heavy_ship_cost = -0.1
heavy_ship_power = 0.1
light_ship_cost = -0.1
light_ship_power = 0.1
galley_cost = -0.1
galley_power = 0.1
transport_cost = -0.1
transport_power = 0.1
global_ship_cost = -0.1
global_ship_recruit_speed = -0.1
global_ship_repair = 0.1
naval_forcelimit = 1
naval_forcelimit_modifier = 0.1
naval_maintenance_modifier = -0.1
global_sailors = 1000
global_sailors_modifier = 0.1
sailor_maintenance_modifer = -0.1
sailors_recovery_speed = 0.1
blockade_efficiency = 0.1
capture_ship_chance = 0.1
global_naval_engagement_modifier = 0.1
naval_attrition = -0.1
naval_morale = 0.1
ship_durability = 0.1
sunk_ship_morale_hit_recieved = -0.1
recover_navy_morale_speed = 0.1
prestige_from_naval = 0.1
own_coast_naval_combat_bonus = 1.0
admiral_cost = -0.1
global_naval_barrage_cost = -0.1
flagship_cost = -0.1
may_perform_slave_raid = yes
may_perform_slave_raid_on_same_religion = yes
transport_attrition = 0.50
movement_speed_in_fleet_modifier = 1
number_of_cannons_modifier = 0.10 # +10%
hull_size_modifier = 0.10 # +10%
heavy_ship_cannons = 0.10 # +10%
heavy_ship_hull_size = 0.10 # +10%
light_ship_cannons = 0.10 # +10%
light_ship_hull_size = 0.10 # +10%
galley_cannons = 0.10 # +10%
galley_hullsize = 0.10 # +10%
transport_cannons = 0.10 # +10%
transport_hullsize = 0.10 # +10%
max_flagships = 1 
naval_morale_damage = 0.10
naval_morale_damage_received = -0.10
naval_morale_constant = 1 # Adds a flat amount of morale

coast_raid_range = 1 # Adds a flat number of sea tiles to the safe raiding distance

### Dplomacy
diplomats = 1
diplomatic_reputation = 1
diplomatic_upkeep = 1
envoy_travel_time = -0.2
fabricate_claims_cost = -0.2
improve_relation_modifier = 0.2
vassal_forcelimit_bonus = 0.2
vassal_income = 0.2
ae_impact = -0.2
claim_duration = 0.2
diplomatic_annexation_cost = -0.2
province_warscore_cost = -0.2
unjustified_demands = -0.2
enemy_core_creation = 0.2
rival_change_cost = -0.2
justify_trade_conflict_cost = -0.2
stability_cost_to_declare_war = 1 #additive
accept_vassalization_reasons = 5 #additive
transfer_trade_power_reasons = 5 #additive
possible_condottieri = 1
relation_with_same_culture = 5 #additive
relation_with_same_culture_group = 5 #additive
relation_with_accepted_culture = 5 #additive
relation_with_other_culture = 5 #additive
monthly_favor_modifier = 0.05
monarch_power_tribute = 10 #Increases the yearly monarch power tribute a tributary pays to its overlord
tributary_conversion_cost_modifier = -0.10 # Mandate
can_claim_states = yes
no_claim_cost_increasement = yes
chance_to_inherit = 0.10 # Chance to Inherit PU
reverse_relation_with_same_religion = 25
reduced_liberty_desire_on_other_continent = -20
overextension_impact_modifier = -0.10

### Economy
global_allowed_num_of_buildings = 1
global_tax_income = 12
global_tax_modifier = 0.05
production_efficiency = 0.05
state_maintenance_modifier = -0.25
inflation_action_cost = -0.1
inflation_reduction = 0.1
interest = -1
development_cost = -0.1
development_cost_modifier = -0.10
build_cost = -0.1
build_time = -0.25
administrative_efficiency = 0.2
core_creation = -0.1
core_decay_on_your_own = -0.1
global_prosperity_growth = 0.1 #Additive
global_monthly_devastation = 0.1 #Additive
gold_depletion_chance_modifier = 0.1
monthly_gold_inflation_modifier = -0.10
global_allowed_num_of_manufactories = 1
free_concentrate_development = yes
expand_infrastructure_cost = -0.10
great_project_upgrade_time = -0.10

### Technology
adm_tech_cost_modifier = -0.1
dip_tech_cost_modifier = -0.1
mil_tech_cost_modifier  = -0.1
technology_cost = -0.1
idea_cost = -0.1
embracement_cost = -0.1
global_institution_spread = 0.1
institution_spread_from_true_faith = 0.1
native_advancement_cost = -0.2
all_power_cost = -0.1
innovativeness_gain = 0.5
free_adm_policy = 1
free_dip_policy = 1
free_mil_policy = 1
possible_adm_policy = 1
possible_dip_policy = 1
possible_mil_policy = 1
possible_policy = 1
free_policy = 1
country_admin_power = 1
country_diplomatic_power = 1
country_military_power = 1

### Court
prestige = 0.5
prestige_decay = -0.01
monthly_splendor = 1
yearly_corruption = -0.1
advisor_cost = -0.25
advisor_pool = 1
female_advisor_chance = 0.1
heir_chance = 0.5
block_introduce_heir = yes
monarch_admin_power = 1
monarch_diplomatic_power = 1
monarch_military_power = 1
adm_advisor_cost = -0.1
dip_advisor_cost = -0.1
mil_advisor_cost = -0.1
monthly_support_heir_gain = 0.1
power_projection_from_insults = 0.1
monarch_lifespan = 0.1
no_stability_loss_on_monarch_death = yes

### Government
yearly_absolutism = 0.1
max_absolutism = 10
legitimacy = 1
republican_tradition = 0.5
devotion = 1
horde_unity = 1
meritocracy = 1
monthly_militarized_society = 0.05
yearly_government_power = 0.05
<faction>_influence = 0.1
imperial_mandate = 0.1
election_cycle = 1
candidate_random_bonus = 1
reelection_cost = -0.1
reform_progress_growth = 0.1
governing_capacity = 100
governing_capacity_modifier = 0.1
state_governing_cost = 0.1
trade_company_governing_cost = 0.1
expand_administration_cost = 0.1
yearly_revolutionary_zeal = 0.1
max_revolutionary_zeal = 10
move_capital_cost_modifier = -0.25
national_focus_years = -1 # Additive 

### Estates
<estate>_influence_modifier	= 0.1
<estate>_loyalty_modifier = 0.1
allow_free_estate_privilege_revocation = yes
all_estate_influence_modifier = 0.1
all_estate_loyalty_equilibrium = 0.05

### Holy Roman Empire
imperial_authority = 0.05
imperial_authority_value = 0.1 #additive
free_city_imperial_authority = 0.1 #additive
imperial_mercenary_cost = -0.1
max_free_cities = 3
max_electors = 3
legitimate_subject_elector = 50 #additive
reasons_to_elect = 10 #additive

### Culture
culture_conversion_cost = -0.25
num_accepted_cultures = 1
same_culture_advisor_cost = -0.5
promote_culture_cost = -0.25
culture_conversion_time = -0.10

### Stability 
global_unrest = -2
stability_cost_modifier = -0.1
global_autonomy = -0.05
min_autonomy = 50
autonomy_change_time = -0.25
harsh_treatment_cost = -0.2
years_of_nationalism = -5
min_autonomy_in_territories = 0.1
unrest_catholic_provinces = -2
global_rebel_suppression_efficiency = 0.25

### Subject
allow_client_states = yes
liberty_desire = -10
liberty_desire_from_subject_development = -0.25
reduced_liberty_desire = 10
reduced_liberty_desire_on_same_continent = 10
years_to_integrate_personal_union = -10 # Additive
overlord_naval_forcelimit = 1 #Additive

### Espionage
spy_offence = 0.1
global_spy_defence = 0.1
discovered_relations_impact = -0.1
rebel_support_efficiency = 0.5

### Religion
global_missionary_strength = 0.03
global_heretic_missionary_strength = 0.03
missionaries = 1
missionary_maintenance_cost = -0.1
religious_unity = 0.5
tolerance_own = 1
tolerance_heretic = 1
tolerance_heathen = 1
tolerance_of_heretics_capacity = 1
tolerance_of_heathens_capacity = 1
enforce_religion_cost = -0.5
prestige_per_development_from_conversion = 0.5
warscore_cost_vs_other_religion = -0.25
establish_order_cost = -0.1
global_religious_conversion_resistance = -0.1
relation_with_heretics = -20
cb_on_religious_enemies = yes
global_heathen_missionary_strength = 0.03

# Religious Mechanics
yearly_authority = 0.25 #Additive
yearly_doom_reduction = 1.5 # Additive
monthly_church_power = 0.02 # Additive
monthly_karma_accelerator = 0.02 # Additive
monthly_piety_accelerator = 0.001 # Additive
yearly_patriarch_authority = 0.1 # Additive
papal_influence = 0.5
curia_powers_cost = 0.1
curia_treasury_contribution = 0.1
church_power_modifier = 0.5
monthly_fervor_increase = 1 # Additive
harmonization_speed = 0.2
yearly_harmony = 1
monthly_piety = 0.1
monthly_karma = 0.1

### Colonisation
colonists = 1
colony_development_boost = 1
colonist_placement_chance = 0.05
global_colonial_growth = 10
range = 0.5
native_uprising_chance = -0.25
native_assimilation = 0.25
migration_cooldown = -0.2
global_tariffs = 0.25
treasure_fleet_income = 0.1
expel_minorities_cost = -0.1

### Trade
caravan_power = 0.2
merchants = 1
placed_merchant_power = 3
global_trade_power = 0.1
global_foreign_trade_power = 0.1
global_own_trade_power = 0.1
global_prov_trade_power_modifier = 0.1
global_trade_goods_size_modifier = 0.1 #Multiplicative
global_trade_goods_size = 0.1 #Additive
trade_efficiency = 0.1
trade_range_modifier = 0.25
trade_steering = 0.1
global_ship_trade_power = 0.2
privateer_efficiency = 0.2
embargo_efficiency = 0.25
ship_power_propagation = 0.1
center_of_trade_upgrade_cost = -0.1
trade_company_investment_cost = -0.1
mercantilism_cost = -0.1

### Flagships
admiral_skill_gain_modifier = 0.1 #additive
flagship_durability = 1
flagship_morale = 1
flagship_naval_engagement_modifier = 1 #additive
movement_speed_onto_off_boat_modifier = -0.1
flagship_landing_penalty = 1
number_of_cannons_flagship_modifier = 0.1
naval_maintenance_flagship_modifier = -0.1
trade_power_in_fleet_modifier = 1
morale_in_fleet_modifier = 0.1
blockade_impact_on_siege_in_fleet_modifier = 0.1
exploration_mission_range_in_fleet_modifier = 100 #additive
barrage_cost_in_fleet_modifier = -0.1
naval_attrition_in_fleet_modifier = -0.1
privateering_efficiency_in_fleet_modifier = 0.1
prestige_from_battles_in_fleet_modifier = 1
naval_tradition_in_fleet_modifier = 1
cannons_for_hunting_pirates_in_fleet = 1
movement_speed_in_fleet_modifier = 1 #additive

##########################
### Province Modifiers ###
##########################

### Technology
institution_growth = 1 #additive

### Military
max_attrition = 5
attrition = 5
local_hostile_attrition = 5
fort_level = 1
garrison_growth = 0.05
local_defensiveness = 0.1
local_friendly_movement_speed = 0.1
local_hostile_movement_speed = 0.1
local_manpower = 1
local_manpower_modifier = 0.25
local_regiment_cost = -0.25
regiment_recruit_speed = -0.25
supply_limit = 1
supply_limit_modifier = 0.25
local_amount_of_banners = 0.1
local_amount_of_carolean = 0.1
local_amount_of_hussars = 0.1
local_development_cost_modifier = -0.25
local_fort_maintenance_modifier = -0.25
local_garrison_size = 0.25
local_has_carolean = yes
local_attacker_dice_roll_bonus = 1 # Additive
local_own_coast_naval_combat_bonus = 1 # Additive
local_defender_dice_roll_bonus = 1 # Additive

### Navy
local_naval_engagement_modifier = 0.1
local_sailors = 250
local_sailors_modifier = 0.1
local_ship_cost = -0.2
local_ship_repair = 0.2
ship_recruit_speed = -0.25
blockade_force_required = 0.5
hostile_disembark_speed = 0.5
hostile_fleet_attrition = 5

### Colonisation
local_colonial_growth = 10
local_colonist_placement_chance = 0.05

### Economy
inflation_reduction_local = 0.1
local_state_maintenance_modifier = -0.25
local_build_cost = -0.25
local_build_time = -0.25
local_monthly_devastation = -0.1
local_production_efficiency = 0.1
local_tax_modifier = 0.1
tax_income = 12
allowed_num_of_buildings = 1
local_development_cost = -0.1
local_institution_spread = 0.1
local_core_creation = -0.25
local_governing_cost = -0.25
statewide_governing_cost = -0.25
local_gold_depletion_chance_modifier = -0.25
local_prosperity_growth = 0.10 #Additive

### Trade
province_trade_power_modifier = 0.1
province_trade_power_value = 0.5
trade_goods_size_modifier = 0.25
trade_goods_size = 0.5
trade_value_modifier = 0.1
trade_value = 1

### Religion
local_missionary_strength = 0.01
local_religious_unity_contribution = 0.1
local_missionary_maintenance_cost = -0.1
local_religious_conversion_resistance = 0.1

### Culture
local_culture_conversion_cost = -0.25

### Stability 
local_unrest = -1
local_autonomy = 0.05
local_years_of_nationalism = -5
min_local_autonomy = 50

#####################
### Rule Modifier ###
#####################

### Only for Age Bonus
can_bypass_forts = yes
can_chain_claim = yes
can_colony_boost_development = yes
can_transfer_vassal_wargoal = yes
force_march_free = yes
free_maintenance_on_expl_conq = yes
ignore_coring_distance = yes

### Only for Idea Bonuses
cb_on_government_enemies = yes
cb_on_primitives = yes
cb_on_overseas = yes
idea_claim_colonies = yes
may_explore = yes
no_religion_penalty = yes
reduced_stab_impacts = yes
sea_repair = yes
may_establish_frontier = yes
extra_manpower_at_religious_war = yes
auto_explore_adjacent_to_colony = yes
can_fabricate_for_vassals = yes

### Only for Banners
local_has_banners = 1

### Only for Event Modifiers
religion = yes
secondary_religion = yes
is_janissary_modifier = yes
is_rajput_modifier = yes
is_carolean_modifier = yes
is_hussars_modifier = yes

# 1.35 
# Country




warscore_from_battles_modifier = 0.10


max_absolutism_effect = 0.10

same_religion_advisor_cost = -0.10

yearly_innovativeness = 0.25 # +0.25
yearly_government_power = 0.25 # +0.25?

centralize_state_cost = -0.10

loyalty_change_on_revoked = 0.10 # e.g. lose 30% Estate Loyalty instead of 20%
estate_interaction_cooldown_modifier = -0.10
all_estate_possible_privileges = 1 # DO NOT USE in ESU


development_cost_in_primary_culture = -0.10

reduced_trade_penalty_on_non_main_tradenode = 0.10
colony_cost_modifier = -0.10
placed_merchant_power_modifier = 0.10

spy_action_cost_modifier = -0.10

num_of_parliament_issues = 1 # increases the max amount of issues to pick from
max_possible_parliament_seats = 10 # how many possible seats parliament can have
parliament_chance_of_decision = 0.10 # +10% chance that a debate is ended after PARLIAMENT_DEBATE_DURATION
parliament_backing_chance = 0.10 # +10% Seats to back an issue in general
parliament_debate_duration = 1 # +1 Year
parliament_effect_duration = 1 # +1 Year
can_revoke_parliament_seats = yes

vassal_manpower_bonus = 0.10
vassal_sailors_bonus = 0.10
vassal_naval_forcelimit_bonus = 0.10

annexation_relations_impact = -0.10

# Province
local_culture_conversion_time = -0.10
local_great_project_upgrade_time = -0.10

local_centralize_state_cost = -0.10
local_colony_cost_modifier = -0.10

local_tolerance_of_heretics = 1.5
local_tolerance_of_heathens = 1.5

local_garrison_damage = 0.10 # Damage the garrison deals

local_assault_fort_cost_modifier = -0.10
local_assault_fort_ability = 0.10