# Monarchy:
# 4 iqta_mechanic - Taxation
# 4 devshirme_mechanic - State Governance
# 5 russian_rule_mechanic - Royal Rule
# 7 militarization_mechanic - Militarization

# Republic:
# 5 cossacks_mechanic - Commoners Government Interactions
# 5 russian_mechanic - National Government
# 5 hessian_militarization_mechanic - Mercenary Militarization
# 5 table_of_ranks_mechanic - High Chamber

# Theocracy:
# 5 divine_monarchy_mechanic - Religious Primacy
# 3 perfectionism_mechanic - Perfectionism
# 5 system_of_councils_mechanic - Religious Council
# 4 land_of_the_christian_sun_mechanic - Sacred Powers
# 3 end_time_mechanic - End Times

# Tribal:
# 4 tribal_federation_mechanic - Commonwealth

# Special:
# 2 mamluk_mechanic - ???
# 1 ottoman_decadence_mechanic
# 0 russian_modernization_mechanic
# 3 naval_professionalism_mechanic - Naval Professionalism
# 3 parliament_vs_monarchy_mechanic - High Council vs. Absolute Rule

### Tribal
native_tribe_reform - tribal_federation_mechanic
nomad_tribe_reform - hessian_militarization_mechanic
# Mission
tribal_federation_reform - tribal_federation_mechanic
tribal_despotism_reform - devshirme_mechanic
tribal_kingdom_reform - russian_rule_mechanic
# End of Game
great_horde_reform - militarization_mechanic
martial_tribe_reform - hessian_militarization_mechanic
civil_tribe_reform - iqta_mechanic
religious_tribe_reform - divine_monarchy_mechanic
hierarchy_rule_reform - tribal_federation_mechanic

### Theocracy
leading_clergy_reform - divine_monarchy_mechanic
monastic_order_reform - land_of_the_christian_sun_mechanic
# Special
priesthood_rule_reform - land_of_the_christian_sun_mechanic
tribunal_rule_reform - system_of_councils_mechanic
nocturnal_sisterhood_reform - divine_monarchy_mechanic
# Mission
clerical_council_reform - perfectionism_mechanic
warrior_order_reform - hessian_militarization_mechanic
theocratic_republic_reform - cossacks_mechanic
theocratic_aristocracy_reform - system_of_councils_mechanic
# Elves
sacred_state_reform - system_of_councils_mechanic
theocratic_democracy_reform - table_of_ranks_mechanic
# Humans
divine_guidance_reform - land_of_the_christian_sun_mechanic
monastic_democracy_reform - russian_mechanic
# Beasts
battle_priest_rule_reform - end_time_mechanic
council_of_harmony_reform - perfectionism_mechanic
# Akaviri
religious_rule_reform - system_of_councils_mechanic
priest_admiral_rule_reform - naval_professionalism_mechanic
### Daedric
priest_commander_rule_reform - militarization_mechanic
religious_committee_reform - end_time_mechanic

### Republic
aristocratic_rule_reform - russian_mechanic
merchant_republic_reform - cossacks_mechanic
# Special
colonial_government_reform - cossacks_mechanic
presidential_dictatorship_reform - militarization_mechanic
revolutionary_republic_reform - table_of_ranks_mechanic
junior_revolutionary_republic_reform - devshirme_mechanic
trading_republic_reform - hessian_militarization_mechanic
free_nordic_state_reform - iqta_mechanic
# Pirates
pirate_republic_reform - naval_professionalism_mechanic
pirate_king_reform - mamluk_mechanic
war_against_the_world_doctrine_reform - militarization_mechanic
# Mission
noble_republic_reform - table_of_ranks_mechanic
peasants_republic_reform - cossacks_mechanic
parliamentary_republic_reform - iqta_mechanic
# Elven
elven_republic_reform - russian_mechanic
religious_republic_reform - system_of_councils_mechanic
# Human
presidential_republic_reform - militarization_mechanic
metropolitan_republic_reform - naval_professionalism_mechanic
# Beast
federal_republic_reform - russian_mechanic
nomadic_republic_reform - cossacks_mechanic
# Akaviri
akaviri_republic_reform - devshirme_mechanic
confederative_republic_reform - table_of_ranks_mechanic
# Daedric
daedric_republic_reform - divine_monarchy_mechanic
military_dictatorship_reform - russian_rule_mechanic

### Monarchy
autocratic_monarchy_reform - table_of_ranks_mechanic
elective_monarchy_reform - iqta_mechanic
# Special
cyrodiilic_empire_reform - ottoman_decadence_mechanic
revolutionary_empire_reform - russian_rule_mechanic
wilderking_reform - perfectionism_mechanic
# Mission
feudal_monarchy_reform - devshirme_mechanic
the_archduchy_reform - russian_mechanic
grand_duchy_reform - hessian_militarization_mechanic
dual_monarchy_reform - parliament_vs_monarchy_mechanic
# Elven
slave_monarchy_reform - mamluk_mechanic
elven_regnum_reform - parliament_vs_monarchy_mechanic
# Human
human_regnum_reform - militarization_mechanic
hereditary_rule_reform - russian_rule_mechanic
# Beast
confederative_monarchy_reform - tribal_federation_mechanic
divine_monarchy_reform - land_of_the_christian_sun_mechanic
# Akaviri
akaviri_monarchy_reform - russian_rule_mechanic
plutocratic_rule_reform - parliament_vs_monarchy_mechanic
# Daedric
daedric_rule_reform - militarization_mechanic
daedric_regnum_reform - end_time_mechanic