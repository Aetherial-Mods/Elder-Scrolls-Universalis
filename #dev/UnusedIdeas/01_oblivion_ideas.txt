CR4_ideas = {
    start = {
    sailor_maintenance_modifer = -0.25
    same_culture_advisor_cost = -0.25
    }

    bonus = {
    dip_tech_cost_modifier = -0.1
    }

    trigger = {
        tag = CR4
    }

    free = yes

    CR4_idea_1 = {
    native_uprising_chance = -0.25
    }
    CR4_idea_2 = {
    interest = -0.25
    }
    CR4_idea_3 = {
    infantry_cost = -0.1
    }
    CR4_idea_4 = {
    land_attrition = -0.10
    }
    CR4_idea_5 = {
    monarch_admin_power = 1
    }
    CR4_idea_6 = {
    harsh_treatment_cost = -0.25
    }
    CR4_idea_7 = {
    merc_maintenance_modifier = -0.10
    }
}

CQ6_ideas = {
    start = {
    merchants = 1
    global_unrest = -2.5
    }

    bonus = {
    siege_ability = 0.15
    }

    trigger = {
        tag = CQ6
    }

    free = yes

    CQ6_idea_1 = {
    cavalry_power = 0.1
    }
    CQ6_idea_2 = {
    reduced_liberty_desire_on_same_continent = 7
    }
    CQ6_idea_3 = {
    production_efficiency = 0.10
    }
    CQ6_idea_4 = {
    army_tradition_from_battle = 0.25
    }
    CQ6_idea_5 = {
    artillery_cost = -0.1
    }
    CQ6_idea_6 = {
    global_manpower_modifier = 0.10
    }
    CQ6_idea_7 = {
    dip_advisor_cost = -0.10
    }
}

CQ3_ideas = {
    start = {
    global_own_trade_power = 0.25
    prestige_from_naval = 0.5
    }

    bonus = {
    dip_advisor_cost = -0.25
    }

    trigger = {
        tag = CQ3
    }

    free = yes

    CQ3_idea_1 = {
    loot_amount = 0.25
    }
    CQ3_idea_2 = {
    defensiveness = 0.10
    }
    CQ3_idea_3 = {
    global_institution_spread = 0.10
    }
    CQ3_idea_4 = {
    merchants = 1
    }
    CQ3_idea_5 = {
    build_cost = -0.10
    }
    CQ3_idea_6 = {
    global_ship_repair = 0.10
    }
    CQ3_idea_7 = {
    unjustified_demands = -0.10
    }
}

CQ9_ideas = {
    start = {
    yearly_absolutism = 0.5
    vassal_forcelimit_bonus = 0.25
    }

    bonus = {
    backrow_artillery_damage = 0.25
    }

    trigger = {
        tag = CQ9
    }

    free = yes

    CQ9_idea_1 = {
    navy_tradition_decay = -0.01
    }
    CQ9_idea_2 = {
    global_trade_goods_size_modifier = 0.1
    }
    CQ9_idea_3 = {
    naval_tradition_from_battle = 0.25
    }
    CQ9_idea_4 = {
    naval_morale = 0.10
    }
    CQ9_idea_5 = {
    monarch_military_power = 1
    }
    CQ9_idea_6 = {
    colonists = 1
    }
    CQ9_idea_7 = {
    caravan_power = 0.10
    }
}

CR1_ideas = {
    start = {
    global_own_trade_power = 0.25
    general_cost = -0.35
    }

    bonus = {
    leader_land_manuever = 1
    }

    trigger = {
        tag = CR1
    }

    free = yes

    CR1_idea_1 = {
    free_adm_policy = 1
    }
    CR1_idea_2 = {
    trade_efficiency = 0.10
    }
    CR1_idea_3 = {
    trade_steering = 0.10
    }
    CR1_idea_4 = {
    garrison_size = 0.10
    }
    CR1_idea_5 = {
    global_ship_repair = 0.10
    }
    CR1_idea_6 = {
    global_ship_cost = -0.10
    }
    CR1_idea_7 = {
    navy_tradition_decay = -0.01
    }
}

CQ1_ideas = {
    start = {
    rebel_support_efficiency = 0.5
    global_ship_recruit_speed = -0.25
    }

    bonus = {
    monarch_military_power = 1
    }

    trigger = {
        tag = CQ1
    }

    free = yes

    CQ1_idea_1 = {
    global_ship_repair = 0.10
    }
    CQ1_idea_2 = {
    global_spy_defence = 0.10
    }
    CQ1_idea_3 = {
    free_leader_pool = 1
    }
    CQ1_idea_4 = {
    naval_forcelimit_modifier = 0.10
    }
    CQ1_idea_5 = {
    diplomats = 1
    }
    CQ1_idea_6 = {
    envoy_travel_time = -0.25
    }
    CQ1_idea_7 = {
    recover_navy_morale_speed = 0.05
    }
}

CQ7_ideas = {
    start = {
    global_manpower_modifier = 0.25
    yearly_absolutism = 0.5
    }

    bonus = {
    global_regiment_recruit_speed = -0.25
    }

    trigger = {
        tag = CQ7
    }

    free = yes

    CQ7_idea_1 = {
    army_tradition = 0.25
    }
    CQ7_idea_2 = {
    loot_amount = 0.25
    }
    CQ7_idea_3 = {
    mil_tech_cost_modifier = -0.05
    }
    CQ7_idea_4 = {
    reinforce_cost_modifier = -0.10
    }
    CQ7_idea_5 = {
    land_attrition = -0.10
    }
    CQ7_idea_6 = {
    heavy_ship_power = 0.1
    }
    CQ7_idea_7 = {
    sailors_recovery_speed = 0.10
    }
}

CR5_ideas = {
    start = {
    fire_damage_received = -0.15
    artillery_cost = -0.25
    }

    bonus = {
    naval_tradition_from_battle = 0.5
    }

    trigger = {
        tag = CR5
    }

    free = yes

    CR5_idea_1 = {
    trade_steering = 0.10
    }
    CR5_idea_2 = {
    global_unrest = -1
    }
    CR5_idea_3 = {
    fort_maintenance_modifier = -0.1
    }
    CR5_idea_4 = {
    global_colonial_growth = 10
    }
    CR5_idea_5 = {
    movement_speed = 0.10
    }
    CR5_idea_6 = {
    global_ship_cost = -0.10
    }
    CR5_idea_7 = {
    tolerance_heathen = 1
    }
}

CS1_ideas = {
    start = {
    shock_damage_received = -0.15
    galley_cost = -0.25
    }

    bonus = {
    general_cost = -0.35
    }

    trigger = {
        tag = CS1
    }

    free = yes

    CS1_idea_1 = {
    mil_tech_cost_modifier = -0.05
    }
    CS1_idea_2 = {
    vassal_forcelimit_bonus = 0.10
    }
    CS1_idea_3 = {
    discipline = 0.05
    }
    CS1_idea_4 = {
    leader_siege = 1
    }
    CS1_idea_5 = {
    merchants = 1
    }
    CS1_idea_6 = {
    recover_navy_morale_speed = 0.05
    }
    CS1_idea_7 = {
    native_assimilation = 0.25
    }
}

CS6_ideas = {
    start = {
    global_trade_power = 0.25
    ae_impact = -0.25
    }

    bonus = {
    army_tradition = 0.5
    }

    trigger = {
        tag = CS6
    }

    free = yes

    CS6_idea_1 = {
    movement_speed = 0.10
    }
    CS6_idea_2 = {
    spy_offence = 0.10
    }
    CS6_idea_3 = {
    drill_decay_modifier = -0.10
    }
    CS6_idea_4 = {
    naval_maintenance_modifier = -0.10
    }
    CS6_idea_5 = {
    global_ship_repair = 0.10
    }
    CS6_idea_6 = {
    global_ship_cost = -0.10
    }
    CS6_idea_7 = {
    diplomatic_upkeep = 1
    }
}

CS7_ideas = {
    start = {
    trade_steering = 0.25
    light_ship_power = 0.15
    }

    bonus = {
    interest = -0.5
    }

    trigger = {
        tag = CS7
    }

    free = yes

    CS7_idea_1 = {
    prestige_per_development_from_conversion = 0.05
    }
    CS7_idea_2 = {
    artillery_levels_available_vs_fort = 1
    }
    CS7_idea_3 = {
    galley_cost = -0.10
    }
    CS7_idea_4 = {
    leader_land_fire = 1
    }
    CS7_idea_5 = {
    reinforce_cost_modifier = -0.10
    }
    CS7_idea_6 = {
    same_culture_advisor_cost = -0.10
    }
    CS7_idea_7 = {
    yearly_absolutism = 0.25
    }
}

CS8_ideas = {
    start = {
    idea_cost = -0.25
    mercenary_manpower = 0.5
    }

    bonus = {
    galley_power = 0.15
    }

    trigger = {
        tag = CS8
    }

    free = yes

    CS8_idea_1 = {
    build_time = -0.10
    }
    CS8_idea_2 = {
    infantry_power = 0.1
    }
    CS8_idea_3 = {
    discipline = 0.05
    }
    CS8_idea_4 = {
    yearly_corruption = -0.07
    }
    CS8_idea_5 = {
    range = 0.10
    }
    CS8_idea_6 = {
    ae_impact = -0.10
    }
    CS8_idea_7 = {
    trade_range_modifier = 0.15
    }
}

CT2_ideas = {
    start = {
    army_tradition = 0.5
    reinforce_cost_modifier = -0.25
    }

    bonus = {
    state_maintenance_modifier = -0.25
    }

    trigger = {
        tag = CT2
    }

    free = yes

    CT2_idea_1 = {
    global_trade_goods_size_modifier = 0.1
    }
    CT2_idea_2 = {
    free_leader_pool = 1
    }
    CT2_idea_3 = {
    dip_advisor_cost = -0.10
    }
    CT2_idea_4 = {
    global_regiment_cost = -0.1
    }
    CT2_idea_5 = {
    global_ship_trade_power = 0.10
    }
    CT2_idea_6 = {
    hostile_attrition = 1
    }
    CT2_idea_7 = {
    tolerance_heretic = 1
    }
}

CS2_ideas = {
    start = {
    infantry_cost = -0.25
    vassal_forcelimit_bonus = 0.25
    }

    bonus = {
    prestige_per_development_from_conversion = 0.10
    }

    trigger = {
        tag = CS2
    }

    free = yes

    CS2_idea_1 = {
    manpower_recovery_speed = 0.10
    }
    CS2_idea_2 = {
    global_missionary_strength = 0.01
    }
    CS2_idea_3 = {
    liberty_desire_from_subject_development = -0.10
    }
    CS2_idea_4 = {
    blockade_efficiency = 0.25
    }
    CS2_idea_5 = {
    heavy_ship_power = 0.1
    }
    CS2_idea_6 = {
    build_cost = -0.10
    }
    CS2_idea_7 = {
    artillery_power = 0.1
    }
}

CS3_ideas = {
    start = {
    global_tariffs = 0.25
    ae_impact = -0.25
    }

    bonus = {
    culture_conversion_cost = -0.25
    }

    trigger = {
        tag = CS3
    }

    free = yes

    CS3_idea_1 = {
    light_ship_power = 0.1
    }
    CS3_idea_2 = {
    general_cost = -0.15
    }
    CS3_idea_3 = {
    naval_morale = 0.10
    }
    CS3_idea_4 = {
    land_attrition = -0.10
    }
    CS3_idea_5 = {
    yearly_absolutism = 0.25
    }
    CS3_idea_6 = {
    dip_advisor_cost = -0.10
    }
    CS3_idea_7 = {
    tolerance_heathen = 1
    }
}

CS4_ideas = {
    start = {
    global_prov_trade_power_modifier = 0.25
    num_accepted_cultures = 1
    }

    bonus = {
    global_tariffs = 0.25
    }

    trigger = {
        tag = CS4
    }

    free = yes

    CS4_idea_1 = {
    cavalry_flanking = 0.25
    }
    CS4_idea_2 = {
    free_leader_pool = 1
    }
    CS4_idea_3 = {
    tolerance_heretic = 1
    }
    CS4_idea_4 = {
    institution_spread_from_true_faith = 0.10
    }
    CS4_idea_5 = {
    army_tradition = 0.25
    }
    CS4_idea_6 = {
    prestige_per_development_from_conversion = 0.05
    }
    CS4_idea_7 = {
    merc_maintenance_modifier = -0.10
    }
}

CS5_ideas = {
    start = {
    shock_damage = 0.15
    institution_spread_from_true_faith = 0.25
    }

    bonus = {
    culture_conversion_cost = -0.25
    }

    trigger = {
        tag = CS5
    }

    free = yes

    CS5_idea_1 = {
    heavy_ship_cost = -0.10
    }
    CS5_idea_2 = {
    global_missionary_strength = 0.01
    }
    CS5_idea_3 = {
    vassal_forcelimit_bonus = 0.10
    }
    CS5_idea_4 = {
    loot_amount = 0.25
    }
    CS5_idea_5 = {
    monarch_admin_power = 1
    }
    CS5_idea_6 = {
    land_morale = 0.10
    }
    CS5_idea_7 = {
    development_cost = -0.1
    }
}

CT3_ideas = {
    start = {
    trade_range_modifier = 0.35
    leader_siege = 1
    }

    bonus = {
    land_forcelimit_modifier = 0.25
    }

    trigger = {
        tag = CT3
    }

    free = yes

    CT3_idea_1 = {
    trade_efficiency = 0.10
    }
    CT3_idea_2 = {
    idea_cost = -0.1
    }
    CT3_idea_3 = {
    defensiveness = 0.10
    }
    CT3_idea_4 = {
    fire_damage_received = -0.10
    }
    CT3_idea_5 = {
    discipline = 0.05
    }
    CT3_idea_6 = {
    backrow_artillery_damage = 0.10
    }
    CT3_idea_7 = {
    same_culture_advisor_cost = -0.10
    }
}

CT4_ideas = {
    start = {
    free_policy = 1
    global_manpower_modifier = 0.25
    }

    bonus = {
    diplomatic_annexation_cost = -0.25
    }

    trigger = {
        tag = CT4
    }

    free = yes

    CT4_idea_1 = {
    recover_army_morale_speed = 0.05
    }
    CT4_idea_2 = {
    free_mil_policy = 1
    }
    CT4_idea_3 = {
    sunk_ship_morale_hit_recieved = -0.10
    }
    CT4_idea_4 = {
    administrative_efficiency = 0.05
    }
    CT4_idea_5 = {
    artillery_levels_available_vs_fort = 1
    }
    CT4_idea_6 = {
    build_cost = -0.10
    }
    CT4_idea_7 = {
    reinforce_speed = 0.10
    }
}

CT5_ideas = {
    start = {
    transport_power = 0.15
    galley_power = 0.15
    }

    bonus = {
    colonists = 1
    }

    trigger = {
        tag = CT5
    }

    free = yes

    CT5_idea_1 = {
    yearly_corruption = -0.07
    }
    CT5_idea_2 = {
    reinforce_cost_modifier = -0.10
    }
    CT5_idea_3 = {
    monarch_military_power = 1
    }
    CT5_idea_4 = {
    general_cost = -0.15
    }
    CT5_idea_5 = {
    discipline = 0.05
    }
    CT5_idea_6 = {
    prestige_per_development_from_conversion = 0.05
    }
    CT5_idea_7 = {
    global_colonial_growth = 10
    }
}

CU1_ideas = {
    start = {
    cavalry_power = 0.15
    global_prov_trade_power_modifier = 0.25
    }

    bonus = {
    drill_decay_modifier = -0.25
    }

    trigger = {
        tag = CU1
    }

    free = yes

    CU1_idea_1 = {
    infantry_cost = -0.1
    }
    CU1_idea_2 = {
    same_culture_advisor_cost = -0.10
    }
    CU1_idea_3 = {
    transport_power = 0.1
    }
    CU1_idea_4 = {
    siege_blockade_progress = 1
    }
    CU1_idea_5 = {
    global_ship_recruit_speed = -0.1
    }
    CU1_idea_6 = {
    heavy_ship_cost = -0.10
    }
    CU1_idea_7 = {
    sailors_recovery_speed = 0.10
    }
}

CU2_ideas = {
    start = {
    war_exhaustion_cost = -0.25
    global_missionary_strength = 0.025
    }

    bonus = {
    vassal_forcelimit_bonus = 0.25
    }

    trigger = {
        tag = CU2
    }

    free = yes

    CU2_idea_1 = {
    prestige_from_naval = 0.25
    }
    CU2_idea_2 = {
    global_regiment_recruit_speed = -0.1
    }
    CU2_idea_3 = {
    culture_conversion_cost = -0.10
    }
    CU2_idea_4 = {
    infantry_power = 0.1
    }
    CU2_idea_5 = {
    diplomatic_upkeep = 1
    }
    CU2_idea_6 = {
    adm_advisor_cost = -0.1
    }
    CU2_idea_7 = {
    reduced_liberty_desire = 7
    }
}

CU3_ideas = {
    start = {
    mil_tech_cost_modifier = -0.1
    embargo_efficiency = 0.5
    }

    bonus = {
    war_exhaustion_cost = -0.25
    }

    trigger = {
        tag = CU3
    }

    free = yes

    CU3_idea_1 = {
    fire_damage_received = -0.10
    }
    CU3_idea_2 = {
    global_ship_recruit_speed = -0.1
    }
    CU3_idea_3 = {
    leader_land_manuever = 1
    }
    CU3_idea_4 = {
    range = 0.10
    }
    CU3_idea_5 = {
    recover_army_morale_speed = 0.05
    }
    CU3_idea_6 = {
    army_tradition_decay = -0.01
    }
    CU3_idea_7 = {
    advisor_cost = -0.10
    }
}

CU4_ideas = {
    start = {
    siege_ability = 0.15
    advisor_cost = -0.25
    }

    bonus = {
    development_cost = -0.25
    }

    trigger = {
        tag = CU4
    }

    free = yes

    CU4_idea_1 = {
    prestige_per_development_from_conversion = 0.05
    }
    CU4_idea_2 = {
    transport_cost = -0.10
    }
    CU4_idea_3 = {
    global_foreign_trade_power = 0.10
    }
    CU4_idea_4 = {
    capture_ship_chance = 0.25
    }
    CU4_idea_5 = {
    siege_blockade_progress = 1
    }
    CU4_idea_6 = {
    monarch_military_power = 1
    }
    CU4_idea_7 = {
    global_tax_modifier = 0.10
    }
}

CU5_ideas = {
    start = {
    cavalry_cost = -0.25
    general_cost = -0.35
    }

    bonus = {
    adm_advisor_cost = -0.25
    }

    trigger = {
        tag = CU5
    }

    free = yes

    CU5_idea_1 = {
    cavalry_power = 0.1
    }
    CU5_idea_2 = {
    global_sailors_modifier = 0.10
    }
    CU5_idea_3 = {
    religious_unity = 0.10
    }
    CU5_idea_4 = {
    prestige_per_development_from_conversion = 0.05
    }
    CU5_idea_5 = {
    institution_spread_from_true_faith = 0.10
    }
    CU5_idea_6 = {
    drill_gain_modifier = 0.25
    }
    CU5_idea_7 = {
    development_cost = -0.1
    }
}

DT1_ideas = {
    start = {
    prestige_from_naval = 0.5
    army_tradition = 0.5
    }

    bonus = {
    embargo_efficiency = 0.5
    }

    trigger = {
        tag = DT1
    }

    free = yes

    DT1_idea_1 = {
    ae_impact = -0.10
    }
    DT1_idea_2 = {
    mil_advisor_cost = -0.1
    }
    DT1_idea_3 = {
    light_ship_power = 0.1
    }
    DT1_idea_4 = {
    movement_speed = 0.10
    }
    DT1_idea_5 = {
    transport_cost = -0.10
    }
    DT1_idea_6 = {
    diplomats = 1
    }
    DT1_idea_7 = {
    monarch_admin_power = 1
    }
}

DT2_ideas = {
    start = {
    colonists = 1
    governing_capacity = 250
    }

    bonus = {
    possible_condottieri = 0.50
    }

    trigger = {
        tag = DT2
    }

    free = yes

    DT2_idea_1 = {
    advisor_pool = 1
    }
    DT2_idea_2 = {
    free_leader_pool = 1
    }
    DT2_idea_3 = {
    global_own_trade_power = 0.10
    }
    DT2_idea_4 = {
    heavy_ship_power = 0.1
    }
    DT2_idea_5 = {
    naval_morale = 0.10
    }
    DT2_idea_6 = {
    infantry_power = 0.1
    }
    DT2_idea_7 = {
    spy_offence = 0.10
    }
}

DT3_ideas = {
    start = {
    native_assimilation = 0.5
    light_ship_cost = -0.25
    }

    bonus = {
    land_maintenance_modifier = -0.25
    }

    trigger = {
        tag = DT3
    }

    free = yes

    DT3_idea_1 = {
    adm_advisor_cost = -0.1
    }
    DT3_idea_2 = {
    global_heretic_missionary_strength = 0.01
    }
    DT3_idea_3 = {
    diplomats = 1
    }
    DT3_idea_4 = {
    privateer_efficiency = 0.25
    }
    DT3_idea_5 = {
    recover_army_morale_speed = 0.05
    }
    DT3_idea_6 = {
    diplomatic_upkeep = 1
    }
    DT3_idea_7 = {
    administrative_efficiency = 0.05
    }
}

DX0_ideas = {
    start = {
    reduced_liberty_desire_on_same_continent = 15
    mercenary_manpower = 0.5
    }

    bonus = {
    unjustified_demands = -0.25
    }

    trigger = {
        tag = DX0
    }

    free = yes

    DX0_idea_1 = {
    light_ship_cost = -0.10
    }
    DX0_idea_2 = {
    global_prov_trade_power_modifier = 0.10
    }
    DX0_idea_3 = {
    rebel_support_efficiency = 0.25
    }
    DX0_idea_4 = {
    monarch_admin_power = 1
    }
    DX0_idea_5 = {
    global_own_trade_power = 0.10
    }
    DX0_idea_6 = {
    naval_morale = 0.10
    }
    DX0_idea_7 = {
    global_sailors_modifier = 0.10
    }
}

DT4_ideas = {
    start = {
    free_dip_policy = 1
    transport_power = 0.15
    }

    bonus = {
    reinforce_speed = 0.25
    }

    trigger = {
        tag = DT4
    }

    free = yes

    DT4_idea_1 = {
    range = 0.10
    }
    DT4_idea_2 = {
    naval_morale = 0.10
    }
    DT4_idea_3 = {
    ship_power_propagation = 0.10
    }
    DT4_idea_4 = {
    trade_steering = 0.10
    }
    DT4_idea_5 = {
    global_unrest = -1
    }
    DT4_idea_6 = {
    prestige_decay = -0.01
    }
    DT4_idea_7 = {
    navy_tradition = 0.25
    }
}

DS1_ideas = {
    start = {
    galley_cost = -0.25
    mil_tech_cost_modifier = -0.1
    }

    bonus = {
    tolerance_heretic = 2.5
    }

    trigger = {
        tag = DS1
    }

    free = yes

    DS1_idea_1 = {
    manpower_recovery_speed = 0.10
    }
    DS1_idea_2 = {
    spy_offence = 0.10
    }
    DS1_idea_3 = {
    vassal_forcelimit_bonus = 0.10
    }
    DS1_idea_4 = {
    leader_land_fire = 1
    }
    DS1_idea_5 = {
    free_leader_pool = 1
    }
    DS1_idea_6 = {
    defensiveness = 0.10
    }
    DS1_idea_7 = {
    cavalry_power = 0.1
    }
}

DW1_ideas = {
    start = {
    manpower_recovery_speed = 0.25
    tolerance_heretic = 2.5
    }

    bonus = {
    global_colonial_growth = 25
    }

    trigger = {
        tag = DW1
    }

    free = yes

    DW1_idea_1 = {
    reinforce_speed = 0.10
    }
    DW1_idea_2 = {
    naval_tradition_from_battle = 0.25
    }
    DW1_idea_3 = {
    cavalry_flanking = 0.25
    }
    DW1_idea_4 = {
    trade_range_modifier = 0.15
    }
    DW1_idea_5 = {
    rebel_support_efficiency = 0.25
    }
    DW1_idea_6 = {
    land_maintenance_modifier = -0.1
    }
    DW1_idea_7 = {
    warscore_cost_vs_other_religion = -0.10
    }
}

DW2_ideas = {
    start = {
    ae_impact = -0.25
    sailor_maintenance_modifer = -0.25
    }

    bonus = {
    global_regiment_recruit_speed = -0.25
    }

    trigger = {
        tag = DW2
    }

    free = yes

    DW2_idea_1 = {
    reinforce_speed = 0.10
    }
    DW2_idea_2 = {
    build_time = -0.10
    }
    DW2_idea_3 = {
    artillery_cost = -0.1
    }
    DW2_idea_4 = {
    artillery_levels_available_vs_fort = 1
    }
    DW2_idea_5 = {
    transport_power = 0.1
    }
    DW2_idea_6 = {
    army_tradition_decay = -0.01
    }
    DW2_idea_7 = {
    dip_tech_cost_modifier = -0.05
    }
}

DX1_ideas = {
    start = {
    years_of_nationalism = -10
    missionaries = 1
    }

    bonus = {
    loot_amount = 0.50
    }

    trigger = {
        tag = DX1
    }

    free = yes

    DX1_idea_1 = {
    war_exhaustion_cost = -0.10
    }
    DX1_idea_2 = {
    global_ship_cost = -0.10
    }
    DX1_idea_3 = {
    navy_tradition_decay = -0.01
    }
    DX1_idea_4 = {
    trade_efficiency = 0.10
    }
    DX1_idea_5 = {
    adm_tech_cost_modifier = -0.05
    }
    DX1_idea_6 = {
    reinforce_cost_modifier = -0.10
    }
    DX1_idea_7 = {
    merc_maintenance_modifier = -0.10
    }
}

DX2_ideas = {
    start = {
    land_morale = 0.15
    fire_damage = 0.15
    }

    bonus = {
    global_sailors_modifier = 0.25
    }

    trigger = {
        tag = DX2
    }

    free = yes

    DX2_idea_1 = {
    prestige_from_naval = 0.25
    }
    DX2_idea_2 = {
    governing_capacity = 100
    }
    DX2_idea_3 = {
    transport_cost = -0.10
    }
    DX2_idea_4 = {
    naval_forcelimit_modifier = 0.10
    }
    DX2_idea_5 = {
    global_regiment_recruit_speed = -0.1
    }
    DX2_idea_6 = {
    galley_cost = -0.10
    }
    DX2_idea_7 = {
    global_ship_cost = -0.10
    }
}

DW9_ideas = {
    start = {
    army_tradition_decay = -0.025
    global_tariffs = 0.25
    }

    bonus = {
    shock_damage = 0.15
    }

    trigger = {
        tag = DW9
    }

    free = yes

    DW9_idea_1 = {
    tolerance_own = 1
    }
    DW9_idea_2 = {
    drill_decay_modifier = -0.10
    }
    DW9_idea_3 = {
    naval_attrition = -0.10
    }
    DW9_idea_4 = {
    ship_durability = 0.1
    }
    DW9_idea_5 = {
    global_prov_trade_power_modifier = 0.10
    }
    DW9_idea_6 = {
    prestige = 0.25
    }
    DW9_idea_7 = {
    global_regiment_recruit_speed = -0.1
    }
}

DX3_ideas = {
    start = {
    global_spy_defence = 0.25
    leader_naval_shock = 1
    }

    bonus = {
    siege_ability = 0.15
    }

    trigger = {
        tag = DX3
    }

    free = yes

    DX3_idea_1 = {
    shock_damage = 0.10
    }
    DX3_idea_2 = {
    free_mil_policy = 1
    }
    DX3_idea_3 = {
    global_ship_repair = 0.10
    }
    DX3_idea_4 = {
    naval_attrition = -0.10
    }
    DX3_idea_5 = {
    ship_power_propagation = 0.10
    }
    DX3_idea_6 = {
    diplomatic_upkeep = 1
    }
    DX3_idea_7 = {
    global_trade_power = 0.1
    }
}

DX4_ideas = {
    start = {
    caravan_power = 0.25
    colonists = 1
    }

    bonus = {
    idea_cost = -0.25
    }

    trigger = {
        tag = DX4
    }

    free = yes

    DX4_idea_1 = {
    light_ship_power = 0.1
    }
    DX4_idea_2 = {
    merchants = 1
    }
    DX4_idea_3 = {
    leader_naval_manuever = 1
    }
    DX4_idea_4 = {
    leader_land_shock = 1
    }
    DX4_idea_5 = {
    diplomatic_annexation_cost = -0.10
    }
    DX4_idea_6 = {
    prestige_from_land = 0.25
    }
    DX4_idea_7 = {
    production_efficiency = 0.10
    }
}

DX5_ideas = {
    start = {
    global_ship_repair = 0.25
    naval_forcelimit_modifier = 0.25
    }

    bonus = {
    prestige_per_development_from_conversion = 0.10
    }

    trigger = {
        tag = DX5
    }

    free = yes

    DX5_idea_1 = {
    rival_border_fort_maintenance = -0.10
    }
    DX5_idea_2 = {
    capture_ship_chance = 0.25
    }
    DX5_idea_3 = {
    free_dip_policy = 1
    }
    DX5_idea_4 = {
    core_creation = -0.10
    }
    DX5_idea_5 = {
    reduced_liberty_desire_on_same_continent = 7
    }
    DX5_idea_6 = {
    colonists = 1
    }
    DX5_idea_7 = {
    diplomatic_annexation_cost = -0.10
    }
}

DY9_ideas = {
    start = {
    global_missionary_strength = 0.025
    ae_impact = -0.25
    }

    bonus = {
    reinforce_speed = 0.25
    }

    trigger = {
        tag = DY9
    }

    free = yes

    DY9_idea_1 = {
    unjustified_demands = -0.10
    }
    DY9_idea_2 = {
    blockade_efficiency = 0.25
    }
    DY9_idea_3 = {
    num_accepted_cultures = 1
    }
    DY9_idea_4 = {
    naval_maintenance_modifier = -0.10
    }
    DY9_idea_5 = {
    idea_cost = -0.1
    }
    DY9_idea_6 = {
    range = 0.10
    }
    DY9_idea_7 = {
    army_tradition_from_battle = 0.25
    }
}

DZ1_ideas = {
    start = {
    fire_damage = 0.15
    global_trade_goods_size_modifier = 0.25
    }

    bonus = {
    yearly_corruption = -0.15
    }

    trigger = {
        tag = DZ1
    }

    free = yes

    DZ1_idea_1 = {
    rival_border_fort_maintenance = -0.10
    }
    DZ1_idea_2 = {
    reinforce_speed = 0.10
    }
    DZ1_idea_3 = {
    embargo_efficiency = 0.25
    }
    DZ1_idea_4 = {
    monarch_admin_power = 1
    }
    DZ1_idea_5 = {
    sailor_maintenance_modifer = -0.1
    }
    DZ1_idea_6 = {
    free_leader_pool = 1
    }
    DZ1_idea_7 = {
    leader_naval_manuever = 1
    }
}

DZ2_ideas = {
    start = {
    advisor_pool = 1
    defensiveness = 0.25
    }

    bonus = {
    institution_spread_from_true_faith = 0.25
    }

    trigger = {
        tag = DZ2
    }

    free = yes

    DZ2_idea_1 = {
    land_morale = 0.10
    }
    DZ2_idea_2 = {
    monarch_admin_power = 1
    }
    DZ2_idea_3 = {
    galley_cost = -0.10
    }
    DZ2_idea_4 = {
    sailors_recovery_speed = 0.10
    }
    DZ2_idea_5 = {
    infantry_cost = -0.1
    }
    DZ2_idea_6 = {
    yearly_absolutism = 0.25
    }
    DZ2_idea_7 = {
    harsh_treatment_cost = -0.25
    }
}

DZ3_ideas = {
    start = {
    war_exhaustion_cost = -0.25
    global_regiment_recruit_speed = -0.25
    }

    bonus = {
    land_morale = 0.15
    }

    trigger = {
        tag = DZ3
    }

    free = yes

    DZ3_idea_1 = {
    diplomats = 1
    }
    DZ3_idea_2 = {
    production_efficiency = 0.10
    }
    DZ3_idea_3 = {
    heavy_ship_power = 0.1
    }
    DZ3_idea_4 = {
    liberty_desire_from_subject_development = -0.10
    }
    DZ3_idea_5 = {
    advisor_cost = -0.10
    }
    DZ3_idea_6 = {
    leader_land_manuever = 1
    }
    DZ3_idea_7 = {
    galley_cost = -0.10
    }
}

DZ4_ideas = {
    start = {
    monarch_admin_power = 1
    harsh_treatment_cost = -0.50
    }

    bonus = {
    province_warscore_cost = -0.25
    }

    trigger = {
        tag = DZ4
    }

    free = yes

    DZ4_idea_1 = {
    global_sailors_modifier = 0.10
    }
    DZ4_idea_2 = {
    naval_maintenance_modifier = -0.10
    }
    DZ4_idea_3 = {
    sunk_ship_morale_hit_recieved = -0.10
    }
    DZ4_idea_4 = {
    mercenary_cost = -0.10
    }
    DZ4_idea_5 = {
    native_assimilation = 0.25
    }
    DZ4_idea_6 = {
    global_regiment_cost = -0.1
    }
    DZ4_idea_7 = {
    warscore_cost_vs_other_religion = -0.10
    }
}

DZ5_ideas = {
    start = {
    spy_offence = 0.25
    yearly_absolutism = 0.5
    }

    bonus = {
    rival_border_fort_maintenance = -0.25
    }

    trigger = {
        tag = DZ5
    }

    free = yes

    DZ5_idea_1 = {
    free_dip_policy = 1
    }
    DZ5_idea_2 = {
    ship_durability = 0.1
    }
    DZ5_idea_3 = {
    light_ship_power = 0.1
    }
    DZ5_idea_4 = {
    siege_ability = 0.1
    }
    DZ5_idea_5 = {
    global_tax_modifier = 0.10
    }
    DZ5_idea_6 = {
    navy_tradition_decay = -0.01
    }
    DZ5_idea_7 = {
    governing_capacity = 100
    }
}