namespace = es_subjects

# es_subjects.1.t: "[new_treethane_overlord_event_target.Monarch.Dynasty.GetName] Dynasty of Valenwood"
# es_subjects.1.d: "As the soldiers of [new_treethane_overlord_event_target.GetName] marched into the city of Falinesti, Bosmer cheered on the streets as they welcomed their liberators from the tyranny of insane [old_treethane_overlord_event_target.Monarch.Dynasty.GetName] rulers. When [new_treethane_overlord_event_target.Monarch.GetName] [new_treethane_overlord_event_target.Monarch.Dynasty.GetName] sat on the throne that once belonged to the Camoran dynasty, all the noblemen, priests and merchants knelt and raised their heads only when the deafening silence, which enveloped the throne room like cobwebs of Shagrath, was broken by someone in the crowd shouting - 'Long live the [new_treethane_overlord_event_target.Monarch.GetTitle]! Soon the dusk was gathering over Falinesti, but the city remained awake: some who had adhered to the old order were trying to leave its walls, and others for whom the new power was opening all doors were not willing to sit still for a moment. The news of the ascent of the [new_treethane_overlord_event_target.Monarch.Dynasty.GetName] dynasty quickly spread throughout Valenwood, but it took each local ruler a while to realise what the change was about. Thus, Bosmer now have a new ruler whose name is [new_treethane_overlord_event_target.Monarch.GetName] [new_treethane_overlord_event_target.Monarch.Dynasty.GetName]."
# es_subjects.1.a: "A new age in the history of [new_treethane_overlord_event_target.GetName] has begun!"

country_event = {
	id = es_subjects.1
	title = es_subjects.1.t
	desc = es_subjects.1.d
	picture = ES_CAMORAN_THRONE_eventPicture
	
	# Decisions
	is_triggered_only = yes
	
	immediate = {
		hidden_effect = {
			if = {
				limit = {
					any_country = {
						has_subject_of_type = treethane
						NOT = { owns = 820 }
					}
				}
				random_country = {
					limit = {
						has_subject_of_type = treethane
						NOT = { owns = 820 }
					}
					save_global_event_target_as = old_treethane_overlord_event_target
				}
			}
			820 = { owner = { save_event_target_as = new_treethane_overlord_event_target } }
			every_known_country = {
				limit = {
					is_subject_of_type = treethane
				}
				event_target:new_treethane_overlord_event_target = { create_subject = { subject_type = treethane subject = PREV } }
			}
		}
	}
	
	option = {
        name = es_subjects.1.a
		add_prestige = 25
		# Notification
		if = {
			limit = {
				any_known_country = { ai = no primary_culture = bosmer NOT = { owns = 820 } }
			}
			random_known_country = {
				limit = {
					ai = no primary_culture = bosmer NOT = { owns = 820 }
				}
				country_event = { id = es_subjects.2 days = 31 }
			}
		}
		else = {
			event_target:old_treethane_overlord_event_target = { country_event = { id = es_subjects.2 days = 31 } }
		}
    }
	
	after = {
		event_target:old_treethane_overlord_event_target = { clr_country_flag = es_claimed_throne_of_valenwood_flag }
	}
}

# es_subjects.2.a: "Is this the end for [old_treethane_overlord_event_target.Monarch.Dynasty.GetName] dynasty?"
# es_subjects.2.tooltip: "§RControl over the province of Falinesti (820) is required to restore the power over Valenwood.§!"
country_event = {
	id = es_subjects.2
	title = es_subjects.1.t
	desc = es_subjects.1.d
	picture = ES_CAMORAN_THRONE_eventPicture
	
	is_triggered_only = yes
	
	option = {
        name = es_subjects.2.a
		event_target:old_treethane_overlord_event_target = { es_remove_stability_1 = yes }
		custom_tooltip = es_subjects.2.tooltip
	}
}

# es_subjects.3.t: "Potential Colony of [es_subjects_3_event_target.GetName]"
# es_subjects.3.d: "The closest regon of Tamriel to the Summerset islands is Valenwood, which has attracted explorers, traders and adventurers since time immemorial. The abundance of such mer allowed us to establish several colonies, which unfortunately fell into disrepair over time and were abandoned by our settlers. Nevertheless, we have now gained dominion over [es_subjects_3_event_target.GetName], located in the forests of Valenwood. Perhaps we should try to turn it into a thriving colony? Or should we rather keep things as they are?"
# es_subjects.3.a: "It shall become our Vassal!"
# es_subjects.3.b: "We can turn it into a march."
# es_subjects.3.c: "Let's better grant it to a Holy Order."
# es_subjects.3.e: "The current status of [es_subjects_3_event_target.GetName] is already perfect."

country_event = {
	id = es_subjects.3
	title = es_subjects.3.t
	desc = es_subjects.3.d
	picture = AYLEID_RUINS_VALENWOOD_eventPicture
	
	# Scripted Effect - On actions event
	is_triggered_only = yes
	
	immediate = {
		random_subject_country = {
			limit = {
				NOT = { is_subject_of_type = vassal }
				NOT = { is_subject_of_type = march }
				NOT = { is_subject_of_type = holy_order_1 }
				culture_group = high_elves_cg
				NOT = { has_country_flag = es_subjects_3_flag }
				capital_scope = { superregion = valenwood_superregion }
			}
			set_country_flag = es_subjects_3_flag
			save_event_target_as = es_subjects_3_event_target
		}
	}
	
	option = {
        name = es_subjects.3.a
		trigger = { NOT = { event_target:es_subjects_3_event_target = { is_subject_of_type = vassal } } }
		add_prestige = 10
		create_subject = { subject_type = vassal subject = event_target:es_subjects_3_event_target }
	}
	option = {
        name = es_subjects.3.b
		trigger = { NOT = { event_target:es_subjects_3_event_target = { is_subject_of_type = march } } }
		add_yearly_manpower = 2.5
		create_subject = { subject_type = march subject = event_target:es_subjects_3_event_target }
	}
	option = {
        name = es_subjects.3.c
		trigger = { NOT = { event_target:es_subjects_3_event_target = { is_subject_of_type = holy_order_1 } } }
		add_years_of_income = 1.5
		create_subject = { subject_type = holy_order_1 subject = event_target:es_subjects_3_event_target }
	}
	option = {
        name = es_subjects.3.e
		es_add_stability_1 = yes
	}
}

# es_subjects.4.t: "[From.GetName] Wants to Join the Nordic Empire"
# es_subjects.4.d: "The Nord Empire has always expanded through conquest, subjugation and suppression. This was largely due to both the constant threat from neighboring peoples and their reluctance to have any relations with the barbarians who sailed from the freezing Atmora. However, over the past time, the Nordic Empire was able to reach certain heights both in the economy and in diplomacy, which made it possible not only to normalize relations with some neighbors, but also to make them think about voluntarily joining the Empire. Thus, having arrived at the imperial palace in [Root.CapitalGetName], envoys from [From.GetName] are already asking to accept their state under the protection of the Emperor in exchange for an oath of allegiance. Of course, most likely they are driven not so much by good goals as by the desire to ensure the prosperity of [From.GetName], but maybe this marriage of convenience can bear fruit for us too?"
# es_subjects.4.a: "[From.GetName] shall join our Empire!"
# es_subjects.4.b: "We cannot allow [From.GetName] to be the part of the Nordic Empre."
# es_send_envoys_to_the_emperor: "We will send out enviys to [emperor.GetName] to ask them if we can join the Nordic Empire."

country_event = {
	id = es_subjects.4
	title = es_subjects.4.t
	desc = es_subjects.4.d
	picture = ES_NORDIC_EMPIRE_eventPicture
	
	is_triggered_only = yes
	
	option = {
        name = es_subjects.4.a
		ai_chance = { factor = 100 }
		add_imperial_influence = 10
		FROM = {
			tooltip = {
				capital_scope = {
					set_in_empire = yes
				}
			}
			# Notification that FROM have joined HRE
			country_event = { id = es_subjects.5 days = 31 }
		}
	}
	option = {
        name = es_subjects.4.b
		ai_chance = { factor = 0 }
		FROM = {
			# Notification that FROM have NOT joined HRE
			country_event = { id = es_subjects.6 days = 31 }
		}
	}
}

# es_subjects.5.t: "[Root.GetName] Joins the Nordic Empire"
# es_subjects.5.d: "The ambassadors have returned with good news! We managed to convince the Emperor to accept us as a new member of the Nordic Empire. Thus, a wide range of new opportunities opens up for us to ensure the prosperity of [Root.GetName], both through economic and diplomatic levers. Moreover, additional security is now guaranteed in the event of an invasion by other countries outside the Empire, since the entire Imperial army will be on our side. Of course, among the inhabitants of [Root.GetName] there may be an opinion that joining the Nordic Empire shows the weakness of our government, but we should not take this position seriously."
# es_subjects.5.a: "A small to pay for our future."

country_event = {
	id = es_subjects.5
	title = es_subjects.5.t
	desc = es_subjects.5.d
	picture = ES_NORDIC_EMPIRE_eventPicture
	
	is_triggered_only = yes
	
	option = {
        name = es_subjects.5.a
		ai_chance = { factor = 100 }
		capital_scope = {
			set_in_empire = yes
		}
		hidden_effect = {
			every_owned_province = {
				set_in_empire = yes
			}
		}
		add_prestige = -10
	}
	after = {
		clr_country_flag = asked_to_join_hre_flag
	}
}

# es_subjects.6.t: "[Root.GetName] is Refused to Join the Nordic Empire"
# es_subjects.6.d: "It looks like a diplomatic scandal is brewing between us and the Nord Empire. Our ambassadors who returned after an audience with the Emperor of the Nords said that the request to accept the COUNTRY into the Empire was refused. Moreover, it was regarded as a joke, which made the entire court council laugh. Under no circumstances can we accept such a disdainful attitude, which will certainly result in one of the most rapid deteriorations in relations with the Nordic Empire in the entire history of our relations with them. Perhaps we are not at all on the same path with the Nords and their state and we need to independently find our own path to a bright future."
# es_subjects.6.a: "We do not need them anyway!"
# opinion_was_refused_to_join_the_empire: "Was Refused to Join Nordic Empire"
# opinion_was_refused_to_join_the_empire_desc: " "

country_event = {
	id = es_subjects.6
	title = es_subjects.6.t
	desc = es_subjects.6.d
	picture = ES_NORDIC_EMPIRE_eventPicture
	
	is_triggered_only = yes
	
	option = {
        name = es_subjects.6.a
		ai_chance = { factor = 100 }
		add_prestige = 10
		add_opinion = {
			modifier = opinion_was_refused_to_join_the_empire
			who = emperor
		}
		reverse_add_opinion = {
			modifier = opinion_was_refused_to_join_the_empire
			who = emperor
		}
	}
	after = {
		clr_country_flag = asked_to_join_hre_flag
	}
}