namespace = es_soldat_flavour_events

# Event Manager

country_event = {
	id = es_soldat_flavour_events.1000
	title = es_soldat_flavour_events.1000.t
	desc = es_soldat_flavour_events.1000.d
	picture = event_soldat_placeholder_image
	
	hidden = yes
	is_triggered_only = yes

	immediate = {
		random_list = {
			1 = { country_event = { id = es_soldat_flavour_events.1 } modifier = { factor = 2 overextension_percentage = 50 } modifier = { factor = 2.5 overextension_percentage = 100 } }
			10 = { country_event = { id = es_soldat_flavour_events.3 } modifier = { factor = 2 capital_scope = { has_port = no } } modifier = { factor = 0 any_owned_province = { has_port = no } } modifier = { factor = 0.5 has_country_flag = soldat_village_coast_guard_debounce } }
			10 = { country_event = { id = es_soldat_flavour_events.2 } modifier = { factor = 0 ROOT = { NOT = { total_development = 30 } } } modifier = { factor = 0.5 has_country_flag = soldat_village_witch_debounce } }
			#hohaho
			1 = { random_list = {
				1 = { country_event = { id = es_soldat_flavour_events.4 } modifier = { factor = 0 OR = { has_global_flag = hohaho ai = yes } } }
				9 = {
					set_global_flag = hohaho 
					country_event = {
						id = es_soldat_flavour_events.1000
						days = 1
					}
				}
			}	}
			1 = { random_list = {
				1 = { country_event = { id = es_soldat_flavour_events.5 } modifier = { factor = 0 OR = { any_country = { has_country_flag = skyisnh } ai = yes } } }
				4 = {
					clr_country_flag = skyisnh
				}
				5 = {
					country_event = {
						id = es_soldat_flavour_events.1000
						days = 1
					}
				}
			}	}
			25 = { set_country_flag = successful_soldat_flavour_event modifier = { factor = 2 has_country_flag = soldat_debounce } }
		}
		if = {
			limit = { has_country_modifier = soldat_legal_fur_trade_modifier }
			country_event = { id = es_soldat_flavour_events.108 }
		}
		if = {
			limit = { has_country_modifier = soldat_hunting_fur_trade_modifier }
			country_event = { id = es_soldat_flavour_events.109 }
		}
	}
	
	option = {
		name = es_soldat_flavour_events.1000.a
	}
	
	after = {
		if = {
			limit = { has_country_flag = successful_soldat_flavour_event }
			clr_country_flag = successful_soldat_flavour_event
		}
		else = {
			country_event = {
				id = es_soldat_flavour_events.1000
				days = 1
			}
		}
	}
}

#es_soldat_flavour_events.1.t: "You'll make a fine rug, cat!"
#es_soldat_flavour_events.1.d: "While it is no secret our nation does not look fondly upon the Khajiit, the treatment they are facing of late has become simply atrocious: a trade in Khajiit pelts has arisen in our nation. Many clamour for us to aid the Khajiit in stopping this evil, while a few others have different ideas..."
#es_soldat_flavour_events.1.a: "We must stop this evil!"
#es_soldat_flavour_events.1.b: "Remove the filthy beasts!"
#es_soldat_flavour_events.1.c: "I could use a new cloak..."

country_event = { #You'll make a fine rug, cat!
	id = es_soldat_flavour_events.1
	title = es_soldat_flavour_events.1.t
	desc = es_soldat_flavour_events.1.d
	picture = event_soldat_banditry_image
	
	trigger = {
		NOT = { has_country_flag = successful_soldat_flavour_event }
		NOT = { has_country_flag = soldat_khajiit_fur_trade_debounce }
		any_owned_province = {
			culture_group = khajiiti_cg
		}
		NOT = {
			culture_group = khajiiti_cg
			accepted_culture = khajiiti
			accepted_culture = anequinan
			accepted_culture = pellitinian
		}
	}
	
	is_triggered_only = yes
	
	immediate = {
		hidden_effect = {
			set_country_flag = successful_soldat_flavour_event
			set_country_flag = soldat_khajiit_fur_trade_debounce
		}
	}
	option = { #Stop it!
		name = es_soldat_flavour_events.1.a
		set_country_flag = soldat_refused_fur_trade
		add_country_modifier = { 
			name = soldat_stop_fur_trade_modifier
			duration = -1
		}
		tooltip = {
			random_owned_province = {
				limit = { culture_group = khajiiti_cg }
				add_province_modifier = { 
					name = soldat_stop_fur_trade_prov_modifier
					duration = 3650
				}
			}
			random_known_country = {
				limit = {
					culture_group = khajiiti_cg
				}
				reverse_add_opinion = { who = FROM modifier = opinion_fur_trade_stop_accepted }
			}
			random_known_country = {
				limit = {
					OR = {
						accepted_culture = khajiiti
						accepted_culture = anequinan
						accepted_culture = pellitinian
					}
				}
				reverse_add_opinion = { who = FROM modifier = opinion_fur_trade_stop_accepted }
			}
		}
		hidden_effect = {
			every_owned_province = {
				limit = {
					culture_group = khajiiti_cg
				}
				add_province_modifier = { 
					name = soldat_stop_fur_trade_prov_modifier
					duration = 3650
				}
			}
			every_known_country = {
				limit = {
					culture_group = khajiiti_cg
				}
				#add_opinion = { who = FROM modifier = opinion_fur_trade_stop_primary }
				reverse_add_opinion = { who = FROM modifier = opinion_fur_trade_stop_primary }
			}
			every_known_country = {
				limit = {
					OR = {
						accepted_culture = khajiiti
						accepted_culture = anequinan
						accepted_culture = pellitinian
					}
				}
				#add_opinion = { who = FROM modifier = opinion_fur_trade_stop_accepted }
				reverse_add_opinion = { who = FROM modifier = opinion_fur_trade_stop_accepted }
			}
		}
	}
	option = { #Evict them!
		name = es_soldat_flavour_events.1.b
		add_country_modifier = { 
			name = soldat_evict_fur_trade_modifier
			duration = -1
		}
		tooltip = {
			random_owned_province = {
				limit = { culture_group = khajiiti_cg }
				add_province_modifier = { 
					name = soldat_evict_fur_trade_prov_modifier
					duration = -1
				}
			}
			random_known_country = {
				limit = {
					culture_group = khajiiti_cg
				}
				reverse_add_opinion = { who = FROM modifier = opinion_fur_trade_evict_accepted }
			}
			random_known_country = {
				limit = {
					OR = {
						accepted_culture = khajiiti
						accepted_culture = anequinan
						accepted_culture = pellitinian
					}
				}
				reverse_add_opinion = { who = FROM modifier = opinion_fur_trade_evict_accepted }
			}
		}
		hidden_effect = {
			every_owned_province = {
				limit = {
					culture_group = khajiiti_cg
				}
				add_province_modifier = {
					name = soldat_evict_fur_trade_prov_modifier
					duration = 9125
				}
			}
			every_known_country = {
				limit = { 
					culture_group = khajiiti_cg
				}
				#add_opinion = { who = FROM modifier = opinion_fur_trade_evict_primary }
				reverse_add_opinion = { who = FROM modifier = opinion_fur_trade_evict_primary }
			}
			every_known_country = {
				limit = {
					OR = {
						accepted_culture = khajiiti
						accepted_culture = anequinan
						accepted_culture = pellitinian
					}
				}
				#add_opinion = { who = FROM modifier = opinion_fur_trade_evict_accepted }
				reverse_add_opinion = { who = FROM modifier = opinion_fur_trade_evict_accepted }
			}
		}
	}
	option = { #Legalize it!
		name = es_soldat_flavour_events.1.c
		add_country_modifier = {
			name = soldat_legal_fur_trade_modifier
			duration = -1
		}
		tooltip = {
			random_owned_province = {
				limit = { culture_group = khajiiti_cg }
				add_permanent_province_modifier = {
					name = soldat_legal_fur_trade_prov_modifier
					duration = -1
				}
			}
			random_known_country = {
				limit = {
					culture_group = khajiiti_cg
				}
				reverse_add_opinion = { who = FROM modifier = opinion_fur_trade_legal_accepted }
			}
			random_known_country = {
				limit = {
					OR = {
						accepted_culture = khajiiti
						accepted_culture = anequinan
						accepted_culture = pellitinian
					}
				}
				reverse_add_opinion = { who = FROM modifier = opinion_fur_trade_legal_accepted }
			}
		}
		hidden_effect = {
			every_owned_province = {
				limit = {
					culture_group = khajiiti_cg
				}
				add_permanent_province_modifier = {
					name = soldat_legal_fur_trade_prov_modifier
					duration = -1
				}
			}
			every_known_country = {
				limit = {
					culture_group = khajiiti_cg
				}
				#add_opinion = { who = FROM modifier = opinion_fur_trade_legal_primary }
				reverse_add_opinion = { who = FROM modifier = opinion_fur_trade_legal_primary }
			}
			every_known_country = {
				limit = {
					OR = {
						accepted_culture = khajiiti
						accepted_culture = anequinan
						accepted_culture = pellitinian
					}
				}
				#add_opinion = { who = FROM modifier = opinion_fur_trade_legal_accepted }
				reverse_add_opinion = { who = FROM modifier = opinion_fur_trade_legal_accepted }
			}
		}
	}
}
#101
#102
country_event = { #not legal, has modifier; remove?
	id = es_soldat_flavour_events.103
	title = es_soldat_flavour_events.103.t
	desc = es_soldat_flavour_events.103.d
	picture = event_soldat_banditry_image
	trigger = {
		NOT = {
			has_country_flag = soldat_refused_fur_trade
			OR = {
				has_country_modifier = soldat_legal_fur_trade_modifier
				has_country_modifier = soldat_hunting_fur_trade_modifier
			}
		}
		any_owned_province = {
			has_province_modifier = soldat_legal_fur_trade_prov_modifier
		}
	}
	#hidden = yes
	
	is_triggered_only = yes
	#mean_time_to_happen = {
	#	months = 0
	#}
	immediate = {
		hidden_effect = {
		}
	}
	
	option = { #what manner of evil...
		name = es_soldat_flavour_events.103.a
		set_country_flag = soldat_refused_fur_trade
		tooltip = {
			random_owned_province = {
				limit = {
					culture_group = khajiiti_cg
					has_province_modifier = soldat_legal_fur_trade_prov_modifier
				}
				remove_province_modifier = soldat_legal_fur_trade_prov_modifier
				add_permanent_province_modifier = {
					name = soldat_legal_fur_trade_prov_old_modifier
					duration = 1825
				}
			}
		}
		hidden_effect = {
			every_owned_province = {
				limit = {
					culture_group = khajiiti_cg
					has_province_modifier = soldat_legal_fur_trade_prov_modifier
				}
				remove_province_modifier = soldat_legal_fur_trade_prov_modifier
				add_permanent_province_modifier = {
					name = soldat_legal_fur_trade_prov_old_modifier
					duration = 1825
				}
			}
		}
	}
	option = { #legalize fur trade
		name = es_soldat_flavour_events.103.b
		trigger = { NOT = { culture_group = khajiiti_cg } } #disable for khajiit nations
		if = { #for countries with accepted culture
			limit = {
				OR = {
					accepted_culture = khajiiti
					accepted_culture = anequinan
					accepted_culture = pellitinian
				}
			}
			if = {
				limit = { accepted_culture = khajiiti }
				remove_accepted_culture = khajiiti
				add_adm_power = -100
				add_dip_power = -100
				add_mil_power = 100
				add_prestige = -10
			}
			if = {
				limit = { accepted_culture = anequinan }
				remove_accepted_culture = anequinan
				add_adm_power = -100
				add_dip_power = -100
				add_mil_power = 100
				add_prestige = -10
			}
			if = {
				limit = { accepted_culture = pellitinian }
				remove_accepted_culture = pellitinian
				add_adm_power = -100
				add_dip_power = -100
				add_mil_power = 100
				add_prestige = -10
			}
		}
		add_country_modifier = { 
			name = soldat_legal_fur_trade_modifier
			duration = -1
		}
		tooltip = {
			random_owned_province = {
				limit = {
					culture_group = khajiiti_cg
					NOT = { has_province_modifier = soldat_legal_fur_trade_prov_modifier }
				}
				add_permanent_province_modifier = {
					name = soldat_legal_fur_trade_prov_modifier
					duration = -1
				}
			}
			random_known_country = {
				limit = {
					culture_group = khajiiti_cg
				}
				#add_opinion = { who = FROM modifier = opinion_fur_trade_legal_primary }
				reverse_add_opinion = { who = ROOT modifier = opinion_fur_trade_legal_primary }
			}
			random_known_country = {
				limit = {
					OR = {
						accepted_culture = khajiiti
						accepted_culture = anequinan
						accepted_culture = pellitinian
					}
				}
				#add_opinion = { who = FROM modifier = opinion_fur_trade_legal_accepted }
				reverse_add_opinion = { who = ROOT modifier = opinion_fur_trade_legal_accepted }
			}
		}
		hidden_effect = {
			every_owned_province = {
				limit = {
					culture_group = khajiiti_cg
					NOT = { has_province_modifier = soldat_legal_fur_trade_prov_modifier }
				}
				add_permanent_province_modifier = {
					name = soldat_legal_fur_trade_prov_modifier
					duration = -1
				}
			}
			every_known_country = {
				limit = {
					culture_group = khajiiti_cg
				}
				#add_opinion = { who = FROM modifier = opinion_fur_trade_legal_primary }
				reverse_add_opinion = { who = ROOT modifier = opinion_fur_trade_legal_primary }
			}
			every_known_country = {
				limit = {
					OR = {
						accepted_culture = khajiiti
						accepted_culture = anequinan
						accepted_culture = pellitinian
					}
				}
				#add_opinion = { who = FROM modifier = opinion_fur_trade_legal_accepted }
				reverse_add_opinion = { who = ROOT modifier = opinion_fur_trade_legal_accepted }
			}
		}
	}
}
country_event = { #all khajiit legal dead
	id = es_soldat_flavour_events.104
	title = es_soldat_flavour_events.104.t
	desc = es_soldat_flavour_events.104.d
	picture = event_soldat_furHunt_image
	trigger = {
		has_country_modifier = soldat_legal_fur_trade_modifier
		all_core_province = { 
			NOT = { culture_group = khajiiti_cg }
		}
	}
	#hidden = yes
	#mean_time_to_happen = {
	#	months = 0
	#}
	is_triggered_only = yes
	
	immediate = {
		hidden_effect = {
		}
	}
	
	option = { #"The end of an era..."
		name = es_soldat_flavour_events.104.a 
		remove_country_modifier = soldat_legal_fur_trade_modifier
		add_country_modifier = {
			name = soldat_extinct_fur_trade_modifier
			duration = 3650
		}
		hidden_effect = {
		every_owned_province = {
			limit = {
				OR = {
					has_province_modifier = soldat_legal_fur_trade_prov_modifier
					has_province_modifier = soldat_legal_fur_trade_prov_old_modifier
				}
			}
			remove_province_modifier = soldat_legal_fur_trade_prov_modifier
			remove_province_modifier = soldat_legal_fur_trade_prov_old_modifier
			add_permanent_province_modifier = {
				name = soldat_extinct_fur_trade_prov_modifier
				duration = 3650
			}
		}
		}
		hidden_effect = {
			every_known_country = {
				limit = {
					culture_group = khajiiti_cg
				}
				#add_opinion = { who = FROM modifier = opinion_fur_trade_legal_primary }
				reverse_remove_opinion = { who = ROOT modifier = opinion_fur_trade_legal_primary }
			}
			every_known_country = {
				limit = {
					OR = {
						accepted_culture = khajiiti
						accepted_culture = anequinan
						accepted_culture = pellitinian
					}
				}
				#add_opinion = { who = FROM modifier = opinion_fur_trade_legal_accepted }
				reverse_remove_opinion = { who = ROOT modifier = opinion_fur_trade_legal_accepted }
			}
		}		
	}
	option = { #"The hunt must go on!"
		name = es_soldat_flavour_events.104.b
		trigger = { ai = no }
		remove_country_modifier = soldat_legal_fur_trade_modifier
		add_country_modifier = {
			name = soldat_hunting_fur_trade_modifier
			duration = 3650
		}
		hidden_effect = {
		custom_tooltip = es_soldat_flavour_events.104.c
		every_owned_province = {
			limit = {
				OR = {
					has_province_modifier = soldat_legal_fur_trade_prov_modifier
					has_province_modifier = soldat_legal_fur_trade_prov_old_modifier
				}
			}
			remove_province_modifier = soldat_legal_fur_trade_prov_modifier
			remove_province_modifier = soldat_legal_fur_trade_prov_old_modifier
			add_province_modifier = {
				name = soldat_hunting_fur_trade_prov_modifier
				duration = 3650
			}
		}
		}
	}
}
country_event = { #all khajiit evicted
	id = es_soldat_flavour_events.105
	title = es_soldat_flavour_events.105.t
	desc = es_soldat_flavour_events.105.d
	picture = event_soldat_furEvict_image
	trigger = {
		has_country_modifier = soldat_evict_fur_trade_modifier
		all_owned_province = { 
			NOT = { culture_group = khajiiti_cg }
		}
	}
	#hidden = yes
	#mean_time_to_happen = {
	#	months = 0
	#}
	is_triggered_only = yes
	
	immediate = {
		hidden_effect = {
		}
	}
	
	option = { #"At last."
		name = es_soldat_flavour_events.105.a 
		remove_country_modifier = soldat_evict_fur_trade_modifier
		add_country_modifier = {
			name = soldat_evicted_fur_trade_modifier
			duration = 3650
		}
		hidden_effect = {
		every_owned_province = {
			limit = {
				has_province_modifier = soldat_evict_fur_trade_prov_modifier
			}
			remove_province_modifier = soldat_evict_fur_trade_prov_modifier
			add_permanent_province_modifier = {
				name = soldat_evicted_fur_trade_prov_modifier
				duration = 3650
			}
		}	
		}
	}
}
country_event = { #all khajiit hunted
	id = es_soldat_flavour_events.106
	title = es_soldat_flavour_events.106.t
	desc = es_soldat_flavour_events.106.d
	picture = event_soldat_furHunt_image
	trigger = {
		has_country_modifier = soldat_evict_fur_trade_modifier
		all_owned_province = { 
			NOT = { culture_group = khajiiti_cg }
		}
	}
	#hidden = yes
	#mean_time_to_happen = {
	#	months = 0
	#}
	is_triggered_only = yes
	
	immediate = {
		hidden_effect = {
		}
	}
	
	option = { #"The Hunt Goes On!"
		name = es_soldat_flavour_events.106.a 
		hidden_effect = {
			every_owned_province = {
				limit = {
					any_neighbor_province = { culture_group = khajiiti_cg }
					NOT = { has_province_modifier = soldat_legal_fur_trade_prov_modifier }
				}
				add_province_modifier = {
					name = soldat_hunting_fur_trade_prov_modifier
					duration = 3650
				}
			}	
		}
	}
}
#107 #khajiit evict by chance(?)
country_event = { #khajiit legal death
	id = es_soldat_flavour_events.108
	title = es_soldat_flavour_events.108.t
	desc = es_soldat_flavour_events.108.d
	picture = event_soldat_banditry_image
	trigger = {
		has_country_modifier = soldat_legal_fur_trade_modifier
		any_owned_province = {
			has_province_modifier = soldat_legal_fur_trade_prov_modifier
			devastation = 25
		}
	}
	hidden = yes
	#mean_time_to_happen = {
	#	months = 0
	#}
	is_triggered_only = yes
	
	immediate = {
		hidden_effect = {
		}
	}
	
	option = {
		name = es_soldat_flavour_events.108.a
		random_owned_province = {
			limit = {
				has_province_modifier = soldat_legal_fur_trade_prov_modifier
				devastation = 50				
			}
			remove_province_modifier = soldat_legal_fur_trade_prov_modifier
			change_culture = ROOT
		}
	}
}
country_event = { #khajiit hunt death
	id = es_soldat_flavour_events.109
	title = es_soldat_flavour_events.109.t
	desc = es_soldat_flavour_events.109.d
	picture = event_soldat_furHunt_image
	trigger = {
		has_country_modifier = soldat_hunting_fur_trade_modifier
		any_owned_province = {
			has_province_modifier = soldat_legal_fur_trade_prov_modifier
			devastation = 25
		}
	}
	hidden = yes
	#mean_time_to_happen = {
	#	months = 0
	#}
	is_triggered_only = yes
	
	immediate = {
		hidden_effect = {
		}
	}
	
	option = {
		name = es_soldat_flavour_events.109.a
		random_owned_province = {
			limit = {
				has_province_modifier = soldat_legal_fur_trade_prov_modifier
				devastation = 50				
			}
			remove_province_modifier = soldat_legal_fur_trade_prov_modifier
			change_culture = ROOT
			add_province_modifier = {
					name = soldat_hunting_fur_trade_prov_modifier
					duration = 3650
			}
			every_neighbor_province = { remove_province_modifier = soldat_hunter_fur_trade_prov_modifier }
		}
	}
}

#other events
country_event = {
	id = es_soldat_flavour_events.2
	title = es_soldat_flavour_events.2.t
	desc = es_soldat_flavour_events.2.d
	picture = event_soldat_placeholder_image
	trigger = {
		NOT = { any_owned_province = { has_province_modifier = soldat_village_witch_prov_modifier } }
	}
	hidden = yes
	
	is_triggered_only = yes
	
	immediate = {
		hidden_effect = {
		set_country_flag = successful_soldat_flavour_event
			if = {
			limit = { NOT = { has_country_flag = soldat_village_witch_debounce } }
			set_country_flag = soldat_village_witch_debounce
			set_country_flag = soldat_debounce
			random_owned_province = {
				limit = {
					is_capital = no
					NOT = { OR = {
						development = 10
						base_tax = 5
						base_production = 5
						base_manpower = 5
					}	}
				}
				add_permanent_province_modifier = {
					name = soldat_village_witch_prov_modifier
					duration = 3650
				}
			}
			}
			else = { clr_country_flag = soldat_village_witch_debounce clr_country_flag = soldat_debounce }
		}
	}
	
	option = {
		name = es_soldat_flavour_events.2.a
	}
}
country_event = {
	id = es_soldat_flavour_events.3
	title = es_soldat_flavour_events.3.t
	desc = es_soldat_flavour_events.3.d
	picture = event_soldat_placeholder_image
	trigger = {
		any_owned_province = {
			has_port = yes
		}
		NOT = { any_owned_province = { has_province_modifier = soldat_village_coast_guard_prov_modifier } }
	}
	hidden = yes

	
	is_triggered_only = yes
	
	immediate = {
		hidden_effect = {
				set_country_flag = successful_soldat_flavour_event
				if = {
			limit = { NOT = { has_country_flag = soldat_village_coast_guard_debounce  } }
			set_country_flag = soldat_village_coast_guard_debounce
			set_country_flag = soldat_debounce
			random_owned_province = {
				limit = {
					has_port = yes
					NOT = { 
						OR = {
							has_province_modifier = soldat_village_coast_guard_prov_modifier
							has_province_modifier = soldat_village_coast_guard_nearby_prov_modifier
						}
						any_neighbor_province = { OR = {
							has_province_modifier = soldat_village_coast_guard_prov_modifier
							has_province_modifier = soldat_village_coast_guard_nearby_prov_modifier
						} }
					}
				}
				add_permanent_province_modifier = {
					name = soldat_village_coast_guard_prov_modifier
					duration = 3650
				}
				every_neighbor_province = {
					limit = {
						has_port = yes
					}
					add_permanent_province_modifier = {
						name = soldat_village_coast_guard_nearby_prov_modifier
						duration = 3650
					}
					every_neighbor_province = {
						limit = {
							has_port = yes
						}
						add_permanent_province_modifier = {
							name = soldat_village_coast_guard_nearby_prov_modifier
							duration = 3650
						}
					}
				}
				remove_province_modifier = soldat_village_coast_guard_nearby_prov_modifier
			}
			}
			else = { clr_country_flag = soldat_village_coast_guard_debounce clr_country_flag = soldat_debounce }
		}
	}
	
	option = {
		name = es_soldat_flavour_events.3.a
	}
}
country_event = { #and Alduin Said Ho Ha Ho
	id = es_soldat_flavour_events.4
	title = es_soldat_flavour_events.4.t
	desc = es_soldat_flavour_events.4.d
	picture = event_soldat_HoHaHo_image
	trigger = {
	}
	fire_only_once = yes
	hidden = no
	
	is_triggered_only = yes
	
	immediate = {
		hidden_effect = {
			set_country_flag = successful_soldat_flavour_event
			set_global_flag = hohaho
		}
	}
	
	option = {
		name = es_soldat_flavour_events.4.a
		add_country_modifier = {
			name = soldat_hohaho_modifier
			duration = 7300
		}
	}
}
country_event = { #Skyrim is a Neighborhood
	id = es_soldat_flavour_events.5
	title = es_soldat_flavour_events.5.t
	desc = es_soldat_flavour_events.5.d
	picture = event_soldat_SkyIsNH_image
	trigger = {
		culture_group = northern_cg
		capital_scope = {
			superregion = skyrim_superregion
		}
		NOT = { has_country_flag = sky_is_neighborhood }
	}
	hidden = no
	
	is_triggered_only = yes
	
	immediate = {
		hidden_effect = {
			set_country_flag = successful_soldat_flavour_event
			set_country_flag = sky_is_neighborhood
		}
	}
	
	option = {
		name = es_soldat_flavour_events.5.a #skyrim is a neighborhood
		trigger = { NOT = { any_country = { capital_scope = { superregion = skyrim_superregion } esu_bard = 3 } } } 
		define_advisor = {
			type = esu_bard
			skill = 3
			name = "Dav Grohl"
			discount = yes
			cost_multiplier = -1
			female = no
			culture = ROOT
			religion = ROOT
		}
		if = { limit = { prestige = 80 }
			add_adm_power = 25
			add_dip_power = 25
			add_mil_power = 25
		}
		else = {
			add_prestige = 15
		}
	}
	option = {
		name = es_soldat_flavour_events.5.b #heart is a storybook
		add_country_modifier = {
			name = soldat_skyisnh_b_modifier
			duration = 1575
		}
		change_innovativeness = 2
	}
	option = {
		name = es_soldat_flavour_events.5.c #mind is a battlefield
		add_country_modifier = {
			name = soldat_skyisnh_c_modifier
			duration = 1575
		}
	}
	option = {
		name = es_soldat_flavour_events.5.e #trouble right and left
		add_country_modifier = {
			name = soldat_skyisnh_e_modifier
			duration = 1575
		}
	}
	option = {
		name = es_soldat_flavour_events.5.f #keep it down
		add_country_modifier = {
			name = soldat_skyisnh_f_modifier
			duration = 1575
		}
	}
}
#centaurs in valenwood
#event where bandits occupy a fort
#event where local mages increase local_cawa(daidra)
#event where leader(jarl) has dealings with bandits
	#followup where the bandits stop paying the leader. choice to send army, mercenaries or let it slide