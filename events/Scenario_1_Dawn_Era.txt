namespace = es_1_scenario

# es_1_scenario.1.t: "Sunset of Merethic Era"
# es_1_scenario.1.d: "For centuries, the elves ruled Tamriel. It seems that nothing lasts forever under Magnus's sun, but we are not so easily relegated to the margins of history. We can still fight for power over this world, but we must decide how to do it. Should we use the tools of trade and finance, or turn to the power of weapons made from moonstone ore? Or maybe, in such turbulent times, it is better for us to stay away for now and concentrate on internal problems so that they do not become the cause of our collapse in the future?"
# es_1_scenario.1.a: "We will conquer those who claim our place."
# es_1_scenario.1.b: "The world is ruled by the one who rules the trade."
# es_1_scenario.1.c: "If we will deal with internal problems, we will solve external ones."
# es_1_scenario.1.e: "We still Rule this World."

country_event = {
	id = es_1_scenario.1
	title = es_1_scenario.1.t
	desc = es_1_scenario.1.d
	picture = ES_THRONEROOM_1_eventPicture	
	
	is_triggered_only = yes

	option = {
		name = "es_1_scenario.1.a"
		ai_chance = { factor = 33 }
		add_country_modifier = {
		name = "01_focus_on_war"
		duration = 9125
		}
		
		every_country = { limit = { ai = no has_country_flag = esu_news_flag } country_event = { id = es_news.5 days = 31 tooltip = es_collapse.2.a.tt } }
	}
	option = {
		name = "es_1_scenario.1.b"	
		ai_chance = { factor = 33 }
		add_country_modifier = {
		name = "01_focus_on_trade"
		duration = 9125
		}
		
		every_country = { limit = { ai = no has_country_flag = esu_news_flag } country_event = { id = es_news.6 days = 31 tooltip = es_collapse.2.a.tt } }
	}	
	option = {
		name = "es_1_scenario.1.c"
		ai_chance = { factor = 33 }
		add_country_modifier = {
		name = "01_focus_on_internal_sphere"
		duration = 9125
		}
		
		every_country = { limit = { ai = no has_country_flag = esu_news_flag } country_event = { id = es_news.7 days = 31 tooltip = es_collapse.2.a.tt } }
	}
	option = {
		name = "es_1_scenario.1.e"
		ai_chance = { factor = 0 }
		add_prestige = 5
		
		every_country = { limit = { ai = no has_country_flag = esu_news_flag } country_event = { id = es_news.8 days = 31 tooltip = es_collapse.2.a.tt } }
	}
	
}

# es_1_scenario.2.t: "Our Old [Root.GovernmentName]"
# es_1_scenario.2.d: "For centuries we have ruled Solstheim, an island of stability amid a sea of chaos. However, every year it becomes clear that our security is at risk. Of course, we can pretend that we can continue to ensure our security, but perhaps we should turn to old allies or look for new ones so that we can face an uncertain future together. In any case, this choice must be made now, because tomorrow it may be too late to choose a side."
# es_1_scenario.2.a: "Side with our Kin."
# es_1_scenario.2.b: "Side with wise Dwemer."
# es_1_scenario.2.c: "Side with powerful Velothi."
# es_1_scenario.2.e: "Nothing threatens us!"

country_event = {
	id = es_1_scenario.2
	title = es_1_scenario.2.t
	desc = es_1_scenario.2.d
	picture = ES_WAR_IS_NEAR_eventPicture	
	
	is_triggered_only = yes
	
	immediate = {
		hidden_effect = {
			1275 = { owner = { save_event_target_as = 1275_owner } }
			1062 = { owner = { save_event_target_as = 1062_owner } }
			1028 = { owner = { save_event_target_as = 1028_owner } }
		}
	}

	option = {
		name = "es_1_scenario.2.a"
		ai_chance = { factor = 33 }
		add_trust = { who = event_target:1275_owner value = 50 mutual = yes }
		add_opinion = { who = event_target:1275_owner modifier = es_friendly_nation }
		reverse_add_opinion = { who = event_target:1275_owner modifier = es_friendly_nation }
		
		add_years_of_income = -1.0
		event_target:1275_owner = { add_years_of_income = 1.0 }
		
		every_country = { limit = { ai = no has_country_flag = esu_news_flag } country_event = { id = es_news.9 days = 31 tooltip = es_collapse.2.a.tt } }
	}
	option = {
		name = "es_1_scenario.2.b"
		ai_chance = { factor = 33 }
		add_trust = { who = event_target:1062_owner value = 50 mutual = yes }
		add_opinion = { who = event_target:1062_owner modifier = es_friendly_nation }
		reverse_add_opinion = { who = event_target:1062_owner modifier = es_friendly_nation }
		
		add_legitimacy = -15
		add_republican_tradition = -15
		add_horde_unity = -15
		add_devotion = -15
		add_meritocracy = -15
		event_target:1062_owner = { 
			add_legitimacy = 15
			add_republican_tradition = 15
			add_horde_unity = 15
			add_devotion = 15
			add_meritocracy = 15
		}
		
		every_country = { limit = { ai = no has_country_flag = esu_news_flag } country_event = { id = es_news.10 days = 31 tooltip = es_collapse.2.a.tt } }
	}
	option = {
		name = "es_1_scenario.2.c"
		ai_chance = { factor = 33 }
		add_trust = { who = event_target:1028_owner value = 50 mutual = yes }
		add_opinion = { who = event_target:1028_owner modifier = es_friendly_nation }
		reverse_add_opinion = { who = event_target:1028_owner modifier = es_friendly_nation }
		
		add_yearly_manpower = -5.0
		event_target:1028_owner = { add_yearly_manpower = 5.0 }
		
		every_country = { limit = { ai = no has_country_flag = esu_news_flag } country_event = { id = es_news.11 days = 31 tooltip = es_collapse.2.a.tt } }
	}
	option = {
		name = "es_1_scenario.1.c"
		ai_chance = { factor = 0 }
		add_prestige = 10
		
		every_country = { limit = { ai = no has_country_flag = esu_news_flag } country_event = { id = es_news.12 days = 31 tooltip = es_collapse.2.a.tt } }
	}
}

# es_1_scenario.3.t: "The Mad Ruler"
# es_1_scenario.3.d: "$MONARCH$'s state of mind has always left much to be desired, but now the situation has worsened significantly. [Root.Monarch.GetHerHis] mood changes every hour. [Root.Monarch.GetSheHe] either cries while sitting on the throne, or abruptly orders the execution of a nobleman in the middle of dinner. Now $MONARCH$ decreed that all the country's gold should be collected in their bedroom, because the chief treasurer was caught petting a black cat. It seems that this was the last straw that overflowed the patience of the nobility and clergy, and the common people, because at that very moment the Royal Guard went straight to the palace."
# es_1_scenario.3.a: "Consult with the clergy to transfer the throne to a relative."
# es_1_scenario.3.b: "Consult with the nobility to transfer power to the Supreme Council."
# es_1_scenario.3.c: "Hand over the conspirators to the Royal Guard!"

country_event = {
	id = es_1_scenario.3
	title = es_1_scenario.3.t
	desc = es_1_scenario.3.d
    picture = ES_ELDENROOT_eventPicture	
	
	is_triggered_only = yes
	
	trigger = {
		NOT = { 820 = { owned_by = ROOT } }
	}
	
	immediate = {
		hidden_effect = {
			820 = { owner = { save_event_target_as = 820_owner } }
		}
	}

	option = {
		name = "es_1_scenario.3.a"
		ai_chance = { factor = 33 }
		es_add_stability_3 = yes
		add_years_of_income = 10.0
		add_adm_power = 100
		add_dip_power = 100
		add_mil_power = 100
		
		if = {
			limit = { NOT = { government = monarchy } }
			change_government = monarchy change_government_reform_progress = -99999 country_event = { id = temporarytitle.1 }
		}
		event_target:820_owner = { create_subject = { subject_type = personal_union subject = ROOT } }
		
		every_country = { limit = { ai = no has_country_flag = esu_news_flag } country_event = { id = es_news.13 days = 31 tooltip = es_collapse.2.a.tt } }
	}
	option = {
		name = "es_1_scenario.3.b"
		ai_chance = { factor = 33 }
		
		add_prestige = -15
		kill_ruler = yes
		if = { limit = { NOT = { government = republic } NOT = { is_emperor_of_china = yes } NOT = { is_emperor = yes } } change_government = republic change_government_reform_progress = -99999 country_event = { id = temporarytitle.1 } }
		add_government_reform = noble_republic_reform
		
		event_target:820_owner = { add_casus_belli = { target = ROOT type = cb_restore_personal_union months = 120 } }
		
		every_country = { limit = { ai = no has_country_flag = esu_news_flag } country_event = { id = es_news.14 days = 31 tooltip = es_collapse.2.a.tt } }
	}
	option = {
		name = "es_1_scenario.3.c"
		ai_chance = { factor = 33 }
		
		add_country_modifier = {
		name = "01_camoran_unrest"
		duration = 3650
		}
		
		event_target:820_owner = { add_casus_belli = { target = ROOT type = cb_restore_personal_union months = 120 } }
		
		every_country = { limit = { ai = no has_country_flag = esu_news_flag } country_event = { id = es_news.1401 days = 31 tooltip = es_collapse.2.a.tt } }
	}
	
}

# es_1_scenario.4.t: "Conquest of [1191_owner.GetName]"
# es_1_scenario.4.d: "The idea of the exclusivity of our government in Tamriel is increasingly spreading among citizens. After all, who, if not us, can ensure prosperity for all our people? However, this is dissonant with the fact that on the opposite shore of Lake Rumare there are lands that challenge our superiority. Of course, this may cause concern to our neighbors in the future, but when else will we have the opportunity to receive such support for an aggressive war? However, the decision to launch a military campaign must be made quickly, because our enemies are also making plans to capture [Root.Capital.GetName]."
# es_1_scenario.4.a: "We will start the campaign right now!"
# es_1_scenario.4.b: "We are not ready for such war."

country_event = {
	id = es_1_scenario.4
	title = es_1_scenario.4.t
	desc = es_1_scenario.4.d
    picture = ES_WEYE_eventPicture	
	
	trigger = {
		NOT = { 1191 = { owned_by = ROOT } }
	}
	
	is_triggered_only = yes
	
	immediate = {
		hidden_effect = {
			1191 = { owner = { save_event_target_as = 1191_owner } }
		}
	}

	option = {
		name = "es_1_scenario.4.a"
		ai_chance = { factor = 50 }
		
		event_target:1191_owner = {
			every_owned_province = {
				add_core = ROOT
			}
		}
	
		define_general = { shock = 2 fire = 2 manuever = 5 siege = 1 }
		
		add_prestige = 10
		
		every_country = { limit = { ai = no has_country_flag = esu_news_flag } country_event = { id = es_news.1402 days = 31 tooltip = es_collapse.2.a.tt } }
	}
	option = {
		name = "es_1_scenario.4.b"
		ai_chance = { factor = 50 }
		
		add_manpower = 50
		add_army_tradition = 10
		add_navy_tradition = 5
		
		every_country = { limit = { ai = no has_country_flag = esu_news_flag } country_event = { id = es_news.1403 days = 31 tooltip = es_collapse.2.a.tt } }
	}
	
}

# es_1_scenario.5.t: "Conquest of [1144_owner.GetName]"
# es_1_scenario.5.d: "[1144_owner.GetName] is an important trading center in Cyrodiil, where many trade routes intersect. However, our merchants' access there is often limited due to the requirement to pay excessive duties. Nonetheless, the [Root.Capital.GetName] Merchant Guild does not want to lose any potential profits so easily and is already willing to pay us a certain amount up front to help them in this matter. This amount is unlikely to cover all the costs of a potential military campaign, but nothing will prevent us from taking what is due to us during its course, say, as reparations. Withal, we should also remember that news of our intentions can spread very quickly, so we have little time to choose the right moment to attack."

country_event = {
	id = es_1_scenario.5
	title = es_1_scenario.5.t
	desc = es_1_scenario.5.d
    picture = ES_COLOVIAN_HOUSE_eventPicture	
	
	trigger = {
		NOT = { 1144 = { owned_by = ROOT } }
	}
	
	is_triggered_only = yes
	
	immediate = {
		hidden_effect = {
			1144 = { owner = { save_event_target_as = 1144_owner } }
		}
	}

	option = {
		name = "es_1_scenario.4.a"
		ai_chance = { factor = 50 }
		
		if = { limit = { government = monarchy } add_casus_belli = { target = event_target:1144_owner type = cb_restore_personal_union months = 12 } }
		else = { add_casus_belli = { target = event_target:1144_owner type = cb_es_scenario months = 50 } }
		
		add_years_of_income = 10.0
		define_general = { shock = 5 fire = 2 manuever = 2 siege = 1 }
		
		every_country = { limit = { ai = no has_country_flag = esu_news_flag } country_event = { id = es_news.1404 days = 31 tooltip = es_collapse.2.a.tt } }
	}
	option = {
		name = "es_1_scenario.4.b"
		ai_chance = { factor = 50 }
		
		es_add_stability_1 = yes
		add_corruption = -5
		es_please_common_estates = yes
		
		every_country = { limit = { ai = no has_country_flag = esu_news_flag } country_event = { id = es_news.1405 days = 31 tooltip = es_collapse.2.a.tt } }
	}
	
}

### Winterborn Clan

# es_1_scenario.6.t: "The Winterborn Clan"
# es_1_scenario.6.d: "Our society is based on the family-clan relationships. Though all the members of different clans are treated equally, some of the families have more influence than the other ones. A good example of such a situation is the Winterborn clan, members of which have taken their rightful place in all spheres of the state, starting from state management to war leadership. Other clans have big doubts about concentrating such power in the hands of one family, thus leading our society to the important choice. Should the Winterborn Clan be put back to the traditional place or our society must embrace the new way of living?"
# es_1_scenario.6.a: "We are all members of the Winterborn Clan!"
# es_1_scenario.6.b: "Clan equality is important, though this family may still keep some of its influence."

country_event = {
	id = es_1_scenario.6
	title = es_1_scenario.6.t
	desc = es_1_scenario.6.d
    picture = ES_WINTERBORN_CLAN_eventPicture	
	
	trigger = {
		NOT = { tag = WTB }
	}
	
	is_triggered_only = yes

	option = {
		name = "es_1_scenario.6.a"
		ai_chance = { factor = 50 }
		add_prestige = 10
		
		wrothgar_lr = {
			limit = {
				culture_group = half_blood_cg
			}
			add_core = ROOT
		}
		
		change_tag = WTB
		if = { limit = { has_custom_ideas = no } country_event = { id = ideagroups.1 days = 31 } restore_country_name = yes }
		if = { limit = { NOT = { government_rank = 5 } } set_government_rank = 5 }
	}
	option = {
		name = "es_1_scenario.6.b"
		ai_chance = { factor = 50 }
		es_add_stability_1 = yes
		add_army_tradition = 10
		add_country_modifier = {
			name = "es_winterborn_clan"
			duration = -1
		}
	}
	
}

# es_1_scenario.7.t: "Foundation of Winterhold College"
# es_1_scenario.7.d: "Today, people from all over Skyrim have come to Winterhold to witness the opening of the first school of magic, where hundreds of students can study simultaneously under the wise tutelage of the legendary wizard Shalidor, who funded its construction and was deeply involved in it himself. Crowds of nobles and eminent warriors have gathered before the as yet locked nations in the hope that it will be their children who can pass the tests that will prove they have the magical abilities worthy of being developed within the walls of this institution. For the same reason, the lesser nobles also gather a little further afield, for whom the opening of the College of Winterhold may be the only way to give their children a chance to escape the daily struggle for life. The sky above people's heads began to illuminate with the bright lights that appeared at the snap of a finger as Shalidor emerged from the main hall, decorated with flags, banners and tropical flowers whose petals gently fluttered in response to the icy wind that shook the air with its howl. Everyone present at that moment will remember for the rest of their lives the silence that prevailed when the iron gates began to open without a single sound, inviting anyone who wished to test their magical powers to come inside."
# es_1_scenario.7.a: "That's a great day for our nation!"
# es_1_scenario.7.b: "[Root.Monarch.GetName] shall be among the ones who enters the College!"

country_event = {
	id = es_1_scenario.7
	title = es_1_scenario.7.t
	desc = es_1_scenario.7.d
    picture = ES_WINTERHOLD_COLLEGE_eventPicture	
	
	trigger = {
		NOT = {
			exists = WIT
		}
	}
	
	is_triggered_only = yes

	option = {
		name = "es_1_scenario.7.a"
		ai_chance = { factor = 100 }
		
		2932 = { add_core = WIT	cede_province = WIT	add_base_tax = 5 add_base_production = 5 add_base_manpower = 5 }
		2785 = { add_core = WIT	cede_province = WIT	add_base_tax = 3 add_base_production = 3 add_base_manpower = 3 }
		7111 = { add_core = WIT	cede_province = WIT	add_base_tax = 1 add_base_production = 1 add_base_manpower = 1 }
		
		WIT = {
			define_ruler = {
				name = "Shalidor"
				adm = 7
				dip = 7
				mil = 7
				age = 50
				culture = nord
				religion = students_of_magnus
			}
			add_ruler_personality = immortal_personality
			add_opinion = { who = ROOT modifier = es_friendly_nation }
		}
		add_opinion = { who = WIT modifier = es_friendly_nation }
		
		create_subject = { subject_type = alliance_member subject =  WIT }
	}
	option = {
		name = "es_1_scenario.7.b"
		ai_chance = { factor = 0 }
		
		2932 = { add_core = WIT	cede_province = WIT	add_base_tax = 5 add_base_production = 5 add_base_manpower = 5 }
		2785 = { add_core = WIT	cede_province = WIT	add_base_tax = 3 add_base_production = 3 add_base_manpower = 3 }
		7111 = { add_core = WIT	cede_province = WIT	add_base_tax = 1 add_base_production = 1 add_base_manpower = 1 }
		
		WIT = {
			define_ruler = {
				name = "Shalidor"
				adm = 7
				dip = 7
				mil = 7
				age = 50
				culture = nord
				religion = students_of_magnus
			}
			add_ruler_personality = immortal_personality
			add_opinion = { who = ROOT modifier = es_friendly_nation }
		}
		add_opinion = { who = WIT modifier = es_friendly_nation }
		
		create_subject = { subject_type = alliance_member subject =  WIT }
		
		kill_ruler = yes
		
		switch_tag = WIT
		WIT = {
			if = { limit = { has_custom_ideas = no } country_event = { id = ideagroups.1 days = 31 } restore_country_name = yes }
			if = { limit = { NOT = { government_rank = 5 } } set_government_rank = 5 }
		}
	}
	
}