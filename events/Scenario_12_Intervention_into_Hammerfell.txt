namespace = es_12_scenario

country_event = {
	id = es_12_scenario.1
	title = es_12_scenario.1.t
	desc = es_12_scenario.1.d
	picture = ES_DRAGON_eventPicture
	
	is_triggered_only = yes

	option = {
		name = "es_12_scenario.1.a"
		ai_chance = { factor = 50 }
		random_list = {
		75 = {
		add_prestige = 25
		}
		25 = {
		kill_ruler = yes
		}
		}
	}
	option = {
		name = "es_12_scenario.1.b"
		ai_chance = { factor = 50 }
		random_list = {
		50 = {
		define_general = {
		name = "Dovahkiin"
        shock = 5
        fire = 5
        manuever = 5
        siege = 5
        }
		add_prestige = -25
		}
		50 = {
		es_remove_stability_1 = yes
		}
		}
	}
}


country_event = {
	id = es_12_scenario.3
	title = es_12_scenario.3.t
	desc = es_12_scenario.3.d
	picture = ES_SKINGRAD_eventPicture
	
	is_triggered_only = yes

	option = {
		name = "es_12_scenario.3.a"
		ai_chance = { factor = 50 }
		every_owned_province = {
		limit = {
		area = cyrodiil_6_la
		}
		add_core = CA6
		add_permanent_province_modifier = {
		name = "12_local_unrest"
		duration = 9125
		}
		}
	}
	option = {
		name = "es_12_scenario.3.b"
		ai_chance = { factor = 50 }
		every_owned_province = {
		limit = {
		area = cyrodiil_6_la
		}
		add_core = CA6
		cede_province = CA6
		}
		if = { limit = { CA6 = { is_free_or_tributary_trigger = yes } }
		create_subject = {
        subject_type = vassal
        subject = CA6
        }
		}
	}
}

country_event = {
	id = es_12_scenario.4
	title = es_12_scenario.4.t
	desc = es_12_scenario.4.d
	picture = ES_CITY_1_eventPicture
	
	is_triggered_only = yes

	option = {
		name = "es_12_scenario.4.a"
		ai_chance = { factor = 50 }
		every_owned_province = {
		limit = {
		area = cyrodiil_7_la
		}
		add_core = CA7
		add_permanent_province_modifier = {
		name = "12_local_unrest"
		duration = 9125
		}
		}
		add_prestige = -15
	}
	option = {
		name = "es_12_scenario.4.b"
		ai_chance = { factor = 50 }
		every_owned_province = {
		limit = {
		area = cyrodiil_7_la
		}
		add_core = CA7
		cede_province = CA7
		}
		if = { limit = { CA7 = { is_free_or_tributary_trigger = yes } }
		create_subject = {
        subject_type = vassal
        subject = CA7
        }
		}
	}
	option = {
		name = "es_12_scenario.12.a"
		ai_chance = { factor = 33 }
		cyrodiil_6_la = { add_core = CA7 cede_province = CA7 }
		cyrodiil_7_la = { add_core = CA7 cede_province = CA7 }
		cyrodiil_8_la = { add_core = CA7 cede_province = CA7 }
		add_prestige = 25
		create_alliance = CA7
		CA7 = { add_opinion = { who = ROOT modifier = es_friendly_nation } }
		add_opinion = { who = CA7 modifier = es_friendly_nation }
	}
	option = {
		name = "es_12_scenario.12.c"
		ai_chance = { factor = 33 }
		trigger = {
		primary_culture = nedic
		NOT = { tag = ABE }
	    NOT = { tag = ORD }
		NOT = { tag = TAE }
		NOT = { tag = COL }
		}
		change_tag = CA7
		set_capital = 1150
		if = {
			limit = {
				NOT = { government_rank = 3 }
			}
		    set_government_rank = 3
		}
		add_country_modifier = {
			name = "centralization_modifier"
			duration = 7300
		}
		if = { limit = { has_custom_ideas = no } country_event = { id = ideagroups.1 days = 31 } restore_country_name = yes }
	}
}

country_event = {
	id = es_12_scenario.5
	title = es_12_scenario.5.t
	desc = es_12_scenario.5.d
	picture = ES_EMPEROR_eventPicture
	
	is_triggered_only = yes

	option = {
		name = "es_12_scenario.5.a"
		ai_chance = { factor = 50 }
		if = {
			limit = { NOT = { government = monarchy } }
			change_government = monarchy change_government_reform_progress = -99999 country_event = { id = temporarytitle.1 }
		}
		define_ruler = {
		name = "Gorieus"
        dynasty = "Alessian"
        adm = 3
        dip = 5
        mil = 4
        age = 35
        claim = 100
		#fixed = yes
		}
		add_prestige = -15
	}
	option = {
		name = "es_12_scenario.5.b"
		ai_chance = { factor = 50 }
		es_remove_stability_1 = yes
	}
}

country_event = {
	id = es_12_scenario.6
	title = es_12_scenario.6.t
	desc = es_12_scenario.6.d
	picture = ES_GENOCIDE_eventPicture
	
	is_triggered_only = yes
	
	immediate = {
	    1342 = { owner = { save_event_target_as = 1342_owner } }
		1510 = { owner = { save_event_target_as = 1510_owner } }
	}

	option = {
		name = "es_12_scenario.6.a"
		ai_chance = { factor = 50 }
		reach_lr = {
			add_claim = ROOT
		}
		
		if = { limit = { can_declare_scenario_war = yes NOT = { truce_with = event_target:1342_owner } NOT = { alliance_with = event_target:1342_owner } NOT = { is_subject_of = event_target:1342_owner } NOT = { overlord_of = event_target:1342_owner } }
		declare_war_with_cb = { who = event_target:1342_owner casus_belli = cb_es_scenario } 
		}
		
	}
	option = {
		name = "es_12_scenario.6.b"
		ai_chance = { factor = 50 }
		alikr_desert_lr = {
			add_claim = ROOT
		}
		
		if = { limit = { can_declare_scenario_war = yes NOT = { truce_with = event_target:1510_owner } NOT = { alliance_with = event_target:1510_owner } NOT = { is_subject_of = event_target:1510_owner } NOT = { overlord_of = event_target:1510_owner } }
		declare_war_with_cb = { who = event_target:1510_owner casus_belli = cb_es_scenario } 
		}
		
	}
	option = {
		name = "es_12_scenario.6.c"
		ai_chance = { factor = 50 }
		add_prestige = -15
	}
}

country_event = {
	id = es_12_scenario.7
	title = es_12_scenario.7.t
	desc = es_12_scenario.7.d
	picture = BATTLE_3_eventPicture
	
	is_triggered_only = yes
	
	goto = 7230
	
	immediate = {
	    7230 = { owner = { save_event_target_as = 1296_owner } }
	}

	option = {
		name = "es_12_scenario.7.a"
		ai_chance = { factor = 50 }
		every_province = {
			limit = {
				culture = reachmen
			}
			add_claim = ROOT
		}
		
		if = { limit = { can_declare_scenario_war = yes NOT = { truce_with = event_target:1296_owner } NOT = { alliance_with = event_target:1296_owner } NOT = { is_subject_of = event_target:1296_owner } NOT = { overlord_of = event_target:1296_owner } }
		declare_war_with_cb = { who = event_target:1296_owner casus_belli = cb_es_scenario } 
		}
		
	}
	option = {
		name = "es_12_scenario.7.b"
		ai_chance = { factor = 50 }
		add_prestige = -10
	}
}

country_event = {
	id = es_12_scenario.8
	title = es_8_scenario.4.t
	desc = es_8_scenario.4.d
	picture = ES_BANK_eventPicture
	
	is_triggered_only = yes

	option = {
		name = "es_8_scenario.4.a"
		ai_chance = { factor = 50 }
		add_country_modifier = {
		name = "08_give_autonomy"
		duration = 9125
		}
	}
	option = {
		name = "es_8_scenario.4.b"
		ai_chance = { factor = 50 }
		add_country_modifier = {
		name = "08_refuse_autonomy"
		duration = 9125
		}
	}
}

country_event = {
	id = es_12_scenario.10
	title = es_4_scenario.2.t
	desc = es_4_scenario.2.d
	picture = ES_WAR_IS_NEAR_eventPicture
	
	is_triggered_only = yes

	option = {
		name = "es_4_scenario.2.a"
		ai_chance = { factor = 50 }
		define_general = {
        shock = 5
        fire = 5
        manuever = 5
        siege = 5
        }
	}
	option = {
		name = "es_4_scenario.2.b"
		ai_chance = { factor = 50 }
		add_country_modifier = {
		name = "04_last_stand_defence"
		duration = 9125
		}
	}
}

country_event = {
	id = es_12_scenario.11
	title = es_12_scenario.11.t
	desc = es_12_scenario.11.d
	picture = GE_HRE_CRUSADE_eventPicture
	
	is_triggered_only = yes
	
	immediate = {
	    5627 = { owner = { save_event_target_as = 5645_owner } }
	}

	option = {
		name = "es_12_scenario.11.a"
		ai_chance = { factor = 50 }

		cyrodiil_15_la = { limit = { NOT = { owned_by = ROOT } } add_core = ROOT }
		cyrodiil_49_la = { limit = { NOT = { owned_by = ROOT } } add_core = ROOT }
		cyrodiil_63_la = { limit = { NOT = { owned_by = ROOT } } add_core = ROOT }
		
		if = { limit = { can_declare_scenario_war = yes NOT = { truce_with = event_target:5645_owner } NOT = { alliance_with = event_target:5645_owner } NOT = { is_subject_of = event_target:5645_owner } NOT = { overlord_of = event_target:5645_owner } }
		declare_war_with_cb = { who = event_target:5645_owner casus_belli = cb_es_scenario } 
		}
	}
	option = {
		name = "es_12_scenario.11.b"
		ai_chance = { factor = 50 }
		add_prestige = -15
	}
}

country_event = {
	id = es_12_scenario.13
	title = es_12_scenario.13.t
	desc = es_12_scenario.13.d
	picture = ES_REBELLION_eventPicture
	
	is_triggered_only = yes
	
	immediate = {
		if = {
			limit = {
				exists = COL
				NOT = { overlord_of = COL }
			}
			COL = {
				save_event_target_as = 5627_owner
			}
		}
		else = {
			5627 = { owner = { save_event_target_as = 5627_owner } }
		}
	}

	option = {
		name = "es_12_scenario.13.a"
		ai_chance = { factor = 50 }
		add_prestige = 25
		
		if = { limit = { government = monarchy  } 
		    declare_war_with_cb = { who = event_target:5627_owner casus_belli = cb_restore_personal_union } 
		}
		else = {
		    declare_war_with_cb = { who = event_target:5627_owner casus_belli = cb_es_scenario } 
		} 
	}
	option = {
		name = "es_12_scenario.13.b"
		ai_chance = { factor = 50 }
		add_prestige = -25
		add_country_modifier = {
		name = "19_expansion_via_trade"
		duration = 9125
		}
	}
}

country_event = {
	id = es_12_scenario.14
	title = es_12_scenario.14.t
	desc = es_12_scenario.14.d
	picture = ES_COLOVIA_1_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
	    NOT = { has_country_flag = es_12_scenario.14_flag }
	    exists = COL
		overlord_of = COL
		COL = {
		any_owned_province = {
		area = cyrodiil_10_la 
		}
		NOT = { capital_scope = { area = cyrodiil_10_la } }
		}
    }

	option = {
		name = "es_12_scenario.14.a"
		ai_chance = { factor = 50 }
		add_prestige = -35
		cyrodiil_10_la = {
			limit = {
				owned_by = COL
			}
			cede_province = ROOT
			add_core = ROOT
			add_core = COL
		}
	}
	option = {
		name = "es_12_scenario.14.b"
		ai_chance = { factor = 50 }
		add_prestige = 10
		add_country_modifier = {
		name = "16_good_reputation"
		duration = 9125
		}
	}
}

###################################################################################################

country_event = {
	id = es_12_scenario.2
	title = es_12_scenario.2.t
	desc = es_12_scenario.2.d
	picture = ES_MOOT_eventPicture
	
	is_triggered_only = yes
	
	immediate = {
	    1275 = { save_event_target_as = king_of_skyrim }
	}

	option = {
		name = "es_12_scenario.2.a"
		ai_chance = { factor = 50 }
		if = { 
			limit = { owns = 1275 NOT = { government = monarchy } } 
			change_government = monarchy add_government_reform = elective_monarchy_reform change_government_reform_progress = -99999 country_event = { id = temporarytitle.1 }
		}
		else_if = {
			limit = {
				owns = 1275 government = monarchy
			}
			add_government_reform = elective_monarchy_reform
		}
		else = { event_target:king_of_skyrim = { owner = { create_subject = { subject_type = alliance_member subject = ROOT } } } }
		
		es_add_stability_3 = yes
		clr_country_flag = es_12_nordic_feud_flag
	}
	option = {
		name = "es_12_scenario.2.b"
		ai_chance = { factor = 50 }
		es_remove_stability_1 = yes
		add_legitimacy = -35
		add_republican_tradition = -35
		add_devotion = -35
		add_horde_unity = -35
		add_meritocracy = -35
	}
}

###################################################################################################

### Nordic Feud
country_event = {
	id = es_12_scenario.100
	title = es_12_scenario.100.t
	desc = es_12_scenario.100.d
	picture = ES_NORDIC_FEUD
	
	is_triggered_only = yes
	
	immediate = {
	    set_country_flag = es_12_nordic_feud_flag
	}

	option = {
		name = "es_12_scenario.100.a"
		ai_chance = { factor = 100 }
		add_manpower = 25
		add_years_of_income = 5.0
		add_war_exhaustion = -10
		add_country_modifier = { name = "12_nordic_feud" duration = 9125 } 
		
		hidden_effect = {
			country_event = { id = es_12_scenario.2 days = 9125  }
		}
	}
	
}

###Dwemers leave Morrowind

country_event = {
	id = es_12_scenario.101
	title = es_12_scenario.101.t
	desc = es_12_scenario.101.d
	picture = ES_EMPTY_DWEMER_CITY_eventPicture
	
	is_triggered_only = yes

	option = {
		name = "es_12_scenario.101.a"
		ai_chance = { factor = 100 }
		add_prestige = -25
		add_yearly_manpower = -2.5
		add_years_of_income = -2.5
		random_owned_province = { add_base_tax = -1 add_base_manpower = -1 add_base_production = -1 }
		random_owned_province = { add_base_tax = -1 add_base_manpower = -1 add_base_production = -1 }
		random_owned_province = { add_base_tax = -1 add_base_manpower = -1 add_base_production = -1 }
		random_owned_province = { add_base_tax = -1 add_base_manpower = -1 add_base_production = -1 }
		random_owned_province = { add_base_tax = -1 add_base_manpower = -1 add_base_production = -1 }
		random_owned_province = { add_base_tax = -1 add_base_manpower = -1 add_base_production = -1 }
		random_owned_province = { add_base_tax = -1 add_base_manpower = -1 add_base_production = -1 }
		random_owned_province = { add_base_tax = -1 add_base_manpower = -1 add_base_production = -1 }
		random_owned_province = { add_base_tax = -1 add_base_manpower = -1 add_base_production = -1 }
		random_owned_province = { add_base_tax = -1 add_base_manpower = -1 add_base_production = -1 }
		random_owned_province = { add_base_tax = -1 add_base_manpower = -1 add_base_production = -1 }
		random_owned_province = { add_base_tax = -1 add_base_manpower = -1 add_base_production = -1 }
		random_owned_province = { add_base_tax = -1 add_base_manpower = -1 add_base_production = -1 }
		random_owned_province = { add_base_tax = -1 add_base_manpower = -1 add_base_production = -1 }
		random_owned_province = { add_base_tax = -1 add_base_manpower = -1 add_base_production = -1 }
		random_owned_province = { add_base_tax = -1 add_base_manpower = -1 add_base_production = -1 }
		random_owned_province = { add_base_tax = -1 add_base_manpower = -1 add_base_production = -1 }
		random_owned_province = { add_base_tax = -1 add_base_manpower = -1 add_base_production = -1 }
		random_owned_province = { add_base_tax = -1 add_base_manpower = -1 add_base_production = -1 }
		random_owned_province = { add_base_tax = -1 add_base_manpower = -1 add_base_production = -1 }
		random_owned_province = { add_base_tax = -1 add_base_manpower = -1 add_base_production = -1 }
		random_owned_province = { add_base_tax = -1 add_base_manpower = -1 add_base_production = -1 }
		random_owned_province = { add_base_tax = -1 add_base_manpower = -1 add_base_production = -1 }
		random_owned_province = { add_base_tax = -1 add_base_manpower = -1 add_base_production = -1 }
		random_owned_province = { add_base_tax = -1 add_base_manpower = -1 add_base_production = -1 }
	}
}

###Morrowind destroy the Old Kingdom

country_event = {
	id = es_12_scenario.102
	title = es_12_scenario.102.t
	desc = es_12_scenario.102.d
	picture = ES_MOURNHOLD_eventPicture
	
	is_triggered_only = yes

	option = {
		name = "es_12_scenario.102.a"
		ai_chance = { factor = 50 }
		
		morrowind_superregion = {
		    limit = {
				owner = { NOT = { culture_group = dwemer_cg } NOT = { culture_group = velothi_cg } }
			}
			add_core = ROOT
			add_province_modifier = { name = "12_local_unrest" duration = 9125 }
		}
		
		es_remove_stability_1 = yes
	}
	option = {
		name = "es_12_scenario.102.b"
		ai_chance = { factor = 50 }
		add_prestige = 10
	}
}

### Annexation of Solstheim

country_event = {
	id = es_12_scenario.103
	title = es_12_scenario.103.t
	desc = es_12_scenario.103.d
	picture = ES_SOLSTHEIM_eventPicture
	
	is_triggered_only = yes
	
	immediate = {
	    952 = { owner = { save_event_target_as = 952_owner } }
	}

	option = {
		name = "es_12_scenario.103.a"
		ai_chance = { factor = 50 }
		
		morrowind_1_la = {
			add_core = ROOT
		}

		if = { limit = { can_declare_scenario_war = yes NOT = { truce_with = event_target:952_owner } NOT = { alliance_with = event_target:952_owner } NOT = { is_subject_of = event_target:952_owner } NOT = { overlord_of = event_target:952_owner } }
		declare_war_with_cb = { who = event_target:952_owner casus_belli = cb_es_scenario } 
		}
	}
	option = {
		name = "es_12_scenario.103.b"
		ai_chance = { factor = 50 }
		add_prestige = 10
		morrowind_1_la = {
			remove_core = ROOT
		}
	}
}

### Dwemer Migration into Hammerfell

# es_12_scenario.104.t: "Dwemers in Rkhardahrk"
# es_12_scenario.104.da: "Thousands of Dwemers come to our lands in search of a better life. Their culture and religion are alien to us, but they have advanced technology and knowledge that can be discovered if we agree to such proximity. Of course, such a large number of immigrants in our area may cause discontent on the part of local residents. However, maybe the advantages of that cohabitation will outweigh its disadvantages? Though, of course, the final decision in this matter remains with the $MONARCH$."
# es_12_scenario.104.db: "Thousands of Dwemers come to our lands in search of a better life. Of course, such a large number of immigrants in our area may cause discontent on the part of those Dwemer who have lived here for many years. However, maybe the influx of such a fresh blood will help us make even more new discoveries? In the end, free hands can always be used both in production lines and in training new soldiers. Though, of course, the final decision in this matter remains with the $MONARCH$."
# es_12_scenario.104.a: "Welcome our Dwemer friends!"
# es_12_scenario.104.b: "We will not allow them to live within our borders."
# es_12_scenario.104.c: "We shall be guided by the Dwemer!"

country_event = {
	id = es_12_scenario.104
	title = es_12_scenario.104.t
	desc = {
		trigger = { NOT = { culture_group = dwemer_cg } }
		desc = es_12_scenario.104.da
	}
	desc = {
		trigger = { culture_group = dwemer_cg }
		desc = es_12_scenario.104.db
	}
	picture = ES_AVANCHENZEL_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = "es_12_scenario.104.a"
		ai_chance = { factor = 50 }
		if = {
			limit = {
				NOT = { culture_group = dwemer_cg }
			}
			7248 = {
				cede_province = HB2
				add_core = HB2
				change_culture = dwemer
				change_religion = reason_and_logic_cult
			}
			1465 = {
				add_core = HB2
			}
			7106 = {
				add_core = HB2
			}
			HB2 = {
				change_primary_culture = dwemer
				change_religion = reason_and_logic_cult
			}
		}
		else = {
			7248 = {
				add_base_tax = 5
				add_base_production = 5
				add_base_manpower = 5
			}
		}
	}
	
	option = {
		name = "es_12_scenario.104.b"
		ai_chance = { factor = 50 }
		add_prestige = 25
		7248 = {
			add_core = HB2
			change_culture = dwemer
			change_religion = reason_and_logic_cult
			add_province_modifier = {
				name = "10_unrest"
				duration = 3650
			}
		}
		1465 = {
				add_core = HB2
			}
		7106 = {
				add_core = HB2
			}
	}
	
	option = {
		name = "es_12_scenario.104.c"
		ai_chance = { factor = 0 }
		trigger = {
			NOT = { culture_group = dwemer_cg }
			NOT = { tag = HB2 }
			NOT = { exists = HB2 }
		}
		change_tag = HB2
		if = {
			limit = {
				NOT = { is_emperor = yes }
				NOT = { is_emperor_of_china = yes }
				NOT = { government = republic }
			}
			change_government = republic change_government_reform_progress = -99999 country_event = { id = temporarytitle.1 }
		}
		set_capital = 7248
		change_religion = reason_and_logic_cult
		change_primary_culture = dwemer
		1465 = {
				add_core = HB2
			}
		7106 = {
				add_core = HB2
			}
	}
}

# es_12_scenario.105.t: "Dwemers in Klathzgar"
# es_12_scenario.105.da: "Thousands of Dwemers come to our lands in search of a better life. Their culture and religion are alien to us, but they have advanced technology and knowledge that can be discovered if we agree to such proximity. Of course, such a large number of immigrants in our area may cause discontent on the part of local residents. However, maybe the advantages of that cohabitation will outweigh its disadvantages? Though, of course, the final decision in this matter remains with the $MONARCH$."
# es_12_scenario.105.db: "Thousands of Dwemers come to our lands in search of a better life. Of course, such a large number of immigrants in our area may cause discontent on the part of those Dwemer who have lived here for many years. However, maybe the influx of such a fresh blood will help us make even more new discoveries? In the end, free hands can always be used both in production lines and in training new soldiers. Though, of course, the final decision in this matter remains with the $MONARCH$."
# es_12_scenario.105.a: "Welcome our Dwemer friends!"
# es_12_scenario.105.b: "We will not allow them to live within our borders."
# es_12_scenario.105.c: "We shall be guided by the Dwemer!"

country_event = {
	id = es_12_scenario.105
	title = es_12_scenario.105.t
	desc = {
		trigger = { NOT = { culture_group = dwemer_cg } }
		desc = es_12_scenario.105.da
	}
	desc = {
		trigger = { culture_group = dwemer_cg }
		desc = es_12_scenario.105.db
	}
	picture = ES_AVANCHENZEL_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = "es_12_scenario.105.a"
		ai_chance = { factor = 50 }
		if = {
			limit = {
				NOT = { culture_group = dwemer_cg }
			}
			6201 = {
				cede_province = HF8
				add_core = HF8
				change_culture = dwemer
				change_religion = reason_and_logic_cult
			}
			6187 = {
				add_core = HF8
			}
			6208 = {
				add_core = HF8
			}
			HF8 = {
				change_primary_culture = dwemer
				change_religion = reason_and_logic_cult
			}
		}
		else = {
			7248 = {
				add_base_tax = 5
				add_base_production = 5
				add_base_manpower = 5
			}
		}
	}
	
	option = {
		name = "es_12_scenario.105.b"
		ai_chance = { factor = 50 }
		add_prestige = 25
		6201 = {
			add_core = HF8
			change_culture = dwemer
			change_religion = reason_and_logic_cult
			add_province_modifier = {
				name = "10_unrest"
				duration = 3650
			}
		}
		6187 = {
				add_core = HF8
			}
		6208 = {
				add_core = HF8
			}
	}
	
	option = {
		name = "es_12_scenario.105.c"
		ai_chance = { factor = 0 }
		trigger = {
			NOT = { culture_group = dwemer_cg }
			NOT = { tag = HF8 }
			NOT = { exists = HF8 }
		}
		change_tag = HF8
		if = {
			limit = {
				NOT = { is_emperor = yes }
				NOT = { is_emperor_of_china = yes }
				NOT = { government = republic }
			}
			change_government = republic change_government_reform_progress = -99999 country_event = { id = temporarytitle.1 }
		}
		set_capital = 6102
		change_religion = reason_and_logic_cult
		change_primary_culture = dwemer
		6187 = {
				add_core = HB2
			}
		6208 = {
				add_core = HB2
			}
	}
}

# es_12_scenario.106.t: "Dwemers in Bthzark"
# es_12_scenario.106.da: "Thousands of Dwemers come to our lands in search of a better life. Their culture and religion are alien to us, but they have advanced technology and knowledge that can be discovered if we agree to such proximity. Of course, such a large number of immigrants in our area may cause discontent on the part of local residents. However, maybe the advantages of that cohabitation will outweigh its disadvantages? Though, of course, the final decision in this matter remains with the $MONARCH$."
# es_12_scenario.106.db: "Thousands of Dwemers come to our lands in search of a better life. Of course, such a large number of immigrants in our area may cause discontent on the part of those Dwemer who have lived here for many years. However, maybe the influx of such a fresh blood will help us make even more new discoveries? In the end, free hands can always be used both in production lines and in training new soldiers. Though, of course, the final decision in this matter remains with the $MONARCH$."
# es_12_scenario.106.a: "Welcome our Dwemer friends!"
# es_12_scenario.106.b: "We will not allow them to live within our borders."
# es_12_scenario.106.c: "We shall be guided by the Dwemer!"

country_event = {
	id = es_12_scenario.106
	title = es_12_scenario.106.t
	desc = {
		trigger = { NOT = { culture_group = dwemer_cg } }
		desc = es_12_scenario.106.da
	}
	desc = {
		trigger = { culture_group = dwemer_cg }
		desc = es_12_scenario.106.db
	}
	picture = ES_AVANCHENZEL_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = "es_12_scenario.106.a"
		ai_chance = { factor = 50 }
		if = {
			limit = {
				NOT = { culture_group = dwemer_cg }
			}
			1501 = {
				cede_province = HF9
				add_core = HF9
				change_culture = dwemer
				change_religion = reason_and_logic_cult
			}
			HF9 = {
				change_primary_culture = dwemer
				change_religion = reason_and_logic_cult
			}
		}
		else = {
			1501 = {
				add_base_tax = 5
				add_base_production = 5
				add_base_manpower = 5
			}
		}
	}
	
	option = {
		name = "es_12_scenario.106.b"
		ai_chance = { factor = 50 }
		add_prestige = 25
		1501 = {
			add_core = HF9
			change_culture = dwemer
			change_religion = reason_and_logic_cult
			add_province_modifier = {
				name = "10_unrest"
				duration = 3650
			}
		}
	}
	
	option = {
		name = "es_12_scenario.106.c"
		ai_chance = { factor = 0 }
		trigger = {
			NOT = { culture_group = dwemer_cg }
			NOT = { tag = HF9 }
			NOT = { exists = HF9 }
		}
		change_tag = HF9
		if = {
			limit = {
				NOT = { is_emperor = yes }
				NOT = { is_emperor_of_china = yes }
				NOT = { government = republic }
			}
			change_government = republic change_government_reform_progress = -99999 country_event = { id = temporarytitle.1 }
		}
		set_capital = 1501
		change_religion = reason_and_logic_cult
		change_primary_culture = dwemer
	}
}
