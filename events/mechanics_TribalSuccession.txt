namespace = es_tribal_succession

# es_tribal_succession.1.t: "The Succession Crisis"
# es_tribal_succession.1.d: "Our previous great [Root.Monarch.GetTitle] is dead and already the vultures are circling. It looks like there will be yet another struggle for the throne of [Root.GetName], unless the power is not transferred to the hands of a strong ruler. Let's hope that the new [Root.Monarch.GetTitle] will have enough strength and political will to overcome all the difficulties that will arise in the near future."
# es_tribal_succession.1.a: "No child can be the [Root.Monarch.GetTitle]!"
# es_tribal_succession.1.b: "No female can be the [Root.Monarch.GetTitle]!"
# es_tribal_succession.1.c: "No male can be the [Root.Monarch.GetTitle]!"
# es_tribal_succession.1.e: "Trinimac will bless [Root.Monarch.GetName]!"

country_event = {
	id = es_tribal_succession.1
	title = es_tribal_succession.1.t
	desc = es_tribal_succession.1.d
	picture = {
		trigger = {
			NOT = { primary_culture = ashlander_dunmer }
			NOT = { primary_culture = ashlander_chimer }
			NOT = { culture_group = orsimer_cg }
			NOT = { culture_group = goblin_cg }
			NOT = { culture_group = lamian_cg }
			NOT = { has_reform = nocturnal_sisterhood_reform }
			NOT = { primary_culture = arachnid }
			NOT = { primary_culture = harvester }
			NOT = { primary_culture = harpy }
		}
		picture = ES_TENT_IN_CAMP_eventPicture
	}
	picture = {
		trigger = {
			OR = {
				primary_culture = ashlander_dunmer
				primary_culture = ashlander_chimer
			}
		}
		picture = ES_MORROWIND_eventPicture
	}
	picture = {
		trigger = {
			OR = {
				culture_group = orsimer_cg
				culture_group = goblin_cg
			}
		}
		picture = ES_ORCS_eventPicture
	}
	picture = {
		trigger = {
			culture_group = lamian_cg
		}
		picture = ES_LAMIAS_eventPicture
	}
	picture = {
		trigger = {
			has_reform = nocturnal_sisterhood_reform
		}
		picture = NOCTURNAL_STATURE_eventPicture
	}
	picture = {
		trigger = {
			primary_culture = arachnid
		}
		picture = ES_SPIDERS_eventPicture
	}
	picture = {
		trigger = {
			primary_culture = harvester
		}
		picture = ES_HARVESTER_eventPicture
	}
	picture = {
		trigger = {
			primary_culture = harpy
		}
		picture = ES_HARPY_eventPicture
	}
	
	is_triggered_only = yes
	
	trigger = {
		OR = {
			AND = {
				government = tribal
				has_regency = yes
			}
			AND = {
				is_female = yes
				do_not_have_female_rulers = yes
			}
			AND = {
				NOT = { is_female = yes }
				do_not_have_male_rulers = yes
			}
		}
	}
	
	immediate = {
		hidden_effect = {
			if = {
				limit = {
					has_regency = yes
				}
				random_owned_province = {
					limit = {
						is_capital = no
						is_state = yes
					}
					spawn_rebels = {
						type = pretender_rebels
						size = 3.5
						win = yes
					}
				}
			}
		}
	}
	
	option = {
		name = es_tribal_succession.1.a
		trigger = {
			government = tribal
			has_regency = yes
		}
		if = { limit = { has_heir = yes } kill_heir = {  } }
		if = {
			limit = {
				NOT = { do_not_have_female_rulers = yes }
				NOT = { do_not_have_male_rulers = yes }
			}
			define_ruler = {
				dynasty = ROOT
			}
		}
		else_if = {
			limit = {
				do_not_have_female_rulers = yes
			}
			define_ruler = {
				dynasty = ROOT
				male = yes
			}
		}
		else = {
			define_ruler = {
				dynasty = ROOT
				female = yes
			}
		}
	}
	
	option = {
		name = es_tribal_succession.1.b
		trigger = {
			is_female = yes
			do_not_have_female_rulers = yes
		}
		if = { limit = { has_heir = yes } kill_heir = {  } }
		define_ruler = {
			dynasty = ROOT
			male = yes
		}
	}
	
	option = {
		name = es_tribal_succession.1.c
		trigger = {
			NOT = { is_female = yes }
			do_not_have_male_rulers = yes
		}
		if = { limit = { has_heir = yes } kill_heir = {  } }
		define_ruler = {
			dynasty = ROOT
			female = yes
		}
	}
	
	option = {
		name = es_tribal_succession.1.e
		trigger = {
			is_female = yes
			OR = {
				culture_group = orsimer_cg
				culture_group = goblin_cg
			}
			religion = orcish_pantheon
		}
		change_personal_deity = Trinimac
	}
}