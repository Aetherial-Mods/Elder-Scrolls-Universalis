namespace = es_mission_events

# PU over Roscrea
country_event = {
	id = es_mission_events.1
	title = "es_mission_events.1.t"
	desc = "es_mission_events.1.d"
	picture = ES_ROSCREA
	
	is_triggered_only = yes

	option = {
		name = "es_mission_events.1.a"
		trigger = {
			OR = {
				government = monarchy
				government = tribal
			}
		}
		ai_chance = { factor = 50 }
		add_prestige = 15
		every_owned_province = {
			limit = {
				region = roscrea_lr
			}
			cede_province = ROS
			add_core = ROS
		}
		create_subject = { subject_type = personal_union subject = ROS }
	}
	
	option = {
		name = "es_mission_events.1.b"
		ai_chance = { factor = 50 }
		every_owned_province = {
			limit = {
				region = roscrea_lr
			}
			add_core = ROS
			add_province_modifier = {
				name = "es_roscrean_independence"
				duration = 3650
			}
		}
	}
}

# Slave Trade Expansion

country_event = {
	id = es_mission_events.2
	title = "es_mission_events.2.t"
	desc = "es_mission_events.2.d"
	picture = ES_GOBLIN_MINES_eventPicture
	
	is_triggered_only = yes

	option = {
		name = "es_mission_events.2.a"
		ai_chance = { factor = 50 }
		add_country_modifier = {
			name = "es_goblin_slaves"
			duration = -1
		}
		random_owned_province = {
			change_culture = goblin
			add_permanent_province_modifier = {
				name = "enslaved_province"
				duration = 36500
			}
		}
		es_upset_common_estates = yes
	}
	
	option = {
		name = "es_mission_events.2.b"
		ai_chance = { factor = 50 }
		add_prestige = 10
		es_upset_merchant_estates = yes
	}
}

# Pact with Altmers

country_event = {
	id = es_mission_events.3
	title = "es_mission_events.3.t"
	desc = "es_mission_events.3.d"
	picture = ES_SIGN_PACT_eventPicture
	
	is_triggered_only = yes

	option = {
		name = "es_mission_events.3.a"
		ai_chance = { 
			factor = 50 
		}
		trigger = {
			NOT = { is_subject_of = event_target:altmeri_domion_nation }
			NOT = { overlord_of = event_target:altmeri_domion_nation }
		}
		es_add_stability_1 = yes
		event_target:altmeri_domion_nation = {
			create_subject = {
				subject_type = alliance_member
				subject  = ROOT
			}
		}
	}
	
	option = {
		name = "es_mission_events.3.b"
		ai_chance = { factor = 50 }
		trigger = {
			OR = {
				ai = no
				NOT = { owns = 820 }
				NOT = { owns = 922 }
				is_subject_of = event_target:altmeri_domion_nation
			}
		}
		add_prestige = -25
	}
}

country_event = {
	id = es_mission_events.4
	title = "es_mission_events.3.t"
	desc = "es_mission_events.3.d"
	picture = ES_SIGN_PACT_eventPicture
	
	is_triggered_only = yes
	hidden = yes
	
	immediate = {
		save_global_event_target_as = altmeri_domion_nation
	}

	option = {
		name = "es_mission_events.3.a"
		ai_chance = { factor = 50 }
	}
	
	after = {
		every_country = {
			limit = {
				primary_culture = bosmer
			}
			country_event = { id = es_mission_events.3 }
		}
	}
	
}

country_event = {
	id = es_mission_events.5
	title = "es_mission_events.3.t"
	desc = "es_mission_events.3.d"
	picture = ES_SIGN_PACT_eventPicture
	
	is_triggered_only = yes
	hidden = yes
	
	immediate = {
		save_global_event_target_as = altmeri_domion_nation
	}

	option = {
		name = "es_mission_events.3.a"
		ai_chance = { factor = 50 }
	}
	
	after = {
		every_country = {
			limit = {
				culture_group = khajiiti_cg
			}
			country_event = { id = es_mission_events.3 }
		}
	}
	
}

# es_mission_events.6.t: "$COUNTRY_ADJ$ Hierarchy"
# es_mission_events.6.d: "The Church has long played an important role in the life of our country, acting as the core around which our society has lived. Certainly it was thanks to the priests that we were able to achieve prosperity, not only behind the high walls of castles, but also in the remote villages, despite the constant raids of orcs or the threat of elves. Nevertheless, it is clear that the clerics' influence continues to grow every day, and perhaps soon it will do more harm than good."
# es_mission_events.6.a: "We must take some action to limit the influence of the church."
# es_mission_events.6.b: "These are merely unsubstantiated assumptions."
# es_mission_events.6.c: "Perhaps our society should follow the example of the church?"
# es_mission_events.6.e: "Harmony will prevail sooner or later."
# REDUCED_INFLUENCE: "Reduced Influence"
# INCREASED_INFLUENCE: "Increased Influence"
# HARMONY_ALWAYS_PREVAILS: "Harmony Always Prevails"

country_event = {
	id = es_mission_events.6
	title = "es_mission_events.6.t"
	desc = "es_mission_events.6.d"
	picture = ES_DIVINES_BRETON_TEMPLE_eventPicture
	
	is_triggered_only = yes

	option = {
		name = "es_mission_events.6.a"
		ai_chance = { factor = 35 }
		add_country_modifier = {
			name = "25_crisis_of_clergy"
			duration = 5475
		}
		if = { limit = { has_estate = estate_priests } add_estate_loyalty_modifier = { estate = estate_priests desc = REDUCED_INFLUENCE loyalty = -25 duration = 9125 } }
		if = { limit = { has_estate = estate_priests } add_estate_influence_modifier = { estate = estate_priests desc = REDUCED_INFLUENCE influence = -25 duration = 9125 } }
	}
	option = {
		name = "es_mission_events.6.b"
		ai_chance = { factor = 15 }
		add_prestige = 15
		if = { limit = { has_estate = estate_priests } add_estate_loyalty_modifier = { estate = estate_priests desc = INCREASED_INFLUENCE loyalty = 35 duration = 9125 } }
		if = { limit = { has_estate = estate_priests } add_estate_influence_modifier = { estate = estate_priests desc = INCREASED_INFLUENCE influence = 35 duration = 9125 } }
	}
	option = {
		name = "es_mission_events.6.c"
		ai_chance = { factor = 0 }
		trigger = {
			NOT = { is_emperor = yes }
			NOT = { is_emperor_of_china = yes }
			NOT = { government = theocracy}
		}
		
		change_government = theocracy change_government_reform_progress = -99999 country_event = { id = temporarytitle.1 }
	}
	option = {
		name = "es_mission_events.6.e"
		ai_chance = { factor = 50 }
		
		trigger = {
			religion = cult_of_lorelia
		}
		
		es_add_stability_1 = yes
		if = { limit = { has_estate = estate_priests } add_estate_loyalty_modifier = { estate = estate_priests desc = HARMONY_ALWAYS_PREVAILS loyalty = 15 duration = 9125 } }
		if = { limit = { has_estate = estate_priests } add_estate_influence_modifier = { estate = estate_priests desc = HARMONY_ALWAYS_PREVAILS influence = 15 duration = 9125 } }
	}
	
}

# Daggerfall Covenant

country_event = {
	id = es_mission_events.7
	title = "es_mission_events.7.t"
	desc = "es_mission_events.7.d"
	picture = ES_SIGN_PACT_eventPicture
	
	is_triggered_only = yes
	hidden = yes
	
	immediate = {
		save_global_event_target_as = daggerfell_covenant_nation
	}

	option = {
		name = "es_mission_events.7.a"
		ai_chance = { factor = 50 }
	}
	
	after = {
		every_country = {
			limit = {
				OR = {
					primary_culture = breton
					primary_culture = redguard
					primary_culture = orsimer
				}
			}
			country_event = { id = es_mission_events.8 }
		}
	}
	
}

country_event = {
	id = es_mission_events.8
	title = "es_mission_events.7.t"
	desc = "es_mission_events.7.d"
	picture = ES_SIGN_PACT_eventPicture
	
	is_triggered_only = yes

	option = {
		name = "es_mission_events.7.a"
		ai_chance = { factor = 50 }
		trigger = {
			NOT = { is_subject_of = event_target:daggerfell_covenant_nation }
			NOT = { overlord_of = event_target:daggerfell_covenant_nation }
		}
		es_add_stability_1 = yes
		event_target:daggerfell_covenant_nation = {
			create_subject = {
				subject_type = alliance_member
				subject  = ROOT
			}
		}
	}
	
	option = {
		name = "es_mission_events.7.b"
		ai_chance = { factor = 50 }
		trigger = {
			OR = {
				ai = no
				NOT = { owns = 1510 }
				NOT = { owns = 6074 }
				is_subject_of = event_target:daggerfell_covenant_nation
			}
		}
		add_prestige = -25
	}
}

# es_mission_events.9.t: "The Old Holds"
# es_mission_events.9.d: "Since our ancestors first landed in Skyrim from Atmora, our fiefdom has been traditionally named for her capital city, [Root.Capital.GetName]. However, much has changed since the days of Ysgramor. There is a spirit of growth among the Nords and their leaders, and spreading now is the idea of establishing new realms, reaching far beyond the might of one city. Already several of our fellow holds have taken on new names to go along with this new consolidation. All eyes are now upon the [Root.Monarch.GetTitle]. Where does the [Root.Monarch.GetTitle] of [Root.GetName] stand in this time of change?"
# es_mission_events.9.a: "Long live for the new regnum!"
# es_mission_events.9.b: "As [Root.Capital.GetName] is glorious, so too shall our nation be!"

country_event = {
	id = es_mission_events.9
	title = "es_mission_events.9.t"
	desc = "es_mission_events.9.d"
	picture = ES_NORDIC_HALL_KING_eventPicture
	
	is_triggered_only = yes

	option = {
		name = "es_mission_events.9.a"
		ai_chance = { factor = 100 }
		add_prestige = 25
	}
	
	option = {
		name = "es_mission_events.9.b"
		ai_chance = { factor = 0 }
		add_legitimacy = 25
		add_republican_tradition = 25
		add_horde_unity = 25
		add_meritocracy = 25
		add_devotion = 25
		restore_country_name = yes
	}
}

# es_mission_events.10.t: "Kingdom of Elsweyr"
# es_mission_events.10.d: "Elsweyr, a land divided into harsh badlands and dry plains in the north and jungles and rainforests in the south, lies at the feet of the [Root.Monarch.GetTitle] [Root.Monarch.GetName]. Nothing prevents [Root.Monarch.GetHerHim] from proclaiming [Root.Monarch.GetHerselfHimself] the ruler of all sixteen Khajiiti Kingdoms and ascending the throne in Torval. However, perhaps the time of the Kingdoms of Elsweyr has passed? Perhaps the time has come to bring these lands into agreement with the authority of the one who rules directly from [Root.Capital.GetName]? After all, no one will dare to object to our right to do so."
# es_mission_events.10.a: "[Root.Monarch.GetTitle] [Root.Monarch.GetName] will be coronated in the city of Torval!"
# es_mission_events.10.b: "Elsweyr Kingdoms shall be dissolved."
# every_nation_of_khajiiti_culture_will_become: "Every nation of §YKhajiiti§! cultural group, which capital is located in §YElsweyr§! superregion will become a §GTributary Subject§! of §Y[Root.GetName]§!."
# get_vassalisation_cb_against_every_country_of_khajiiti_culture: "§Y[Root.GetName]§! will get a §GVassalisation Casus Beli§! for the duration of §Y100 years§! against every nation of §YKhajiiti§! cultural group, which capital is located in §YElsweyr§! superregion."

country_event = {
	id = es_mission_events.10
	title = "es_mission_events.10.t"
	desc = "es_mission_events.10.d"
	picture = ES_KHAJIITI_THRONE_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = "es_mission_events.10.a"
		ai_chance = { factor = 100 }
		
		trigger = {
			OR = {
				is_emperor = yes
				is_emperor_of_china = yes
				has_reform = cyrodiilic_empire_reform
				is_nomad = yes
				primary_culture = vampire
			}
		}
		
		if = {
			limit = {
				922 = { NOT = { culture = ROOT } }
			}
			922 = { ROOT = { add_accepted_culture = PREV } }
		}
		else = {
			add_prestige = 20
		}
		
		custom_tooltip = every_nation_of_khajiiti_culture_will_become
		hidden_effect = {
			every_country = {
				limit = {
					culture_group = khajiiti_cg
					capital_scope = { superregion = elsweyr_superregion }
					NOT = { is_subject_of = ROOT }
				}
				ROOT = { create_subject = { subject_type = tributary_state subject = PREV } }
			}
		}
	}
	
	option = {
		name = "es_mission_events.10.b"
		ai_chance = { factor = 100 }
		custom_tooltip = get_vassalisation_cb_against_every_country_of_khajiiti_culture
		every_country = {
			limit = {
				culture_group = khajiiti_cg
				capital_scope = { superregion = elsweyr_superregion }
				NOT = { is_subject_of = ROOT }
			}
			reverse_add_casus_belli  = {
				target = PREV
				type = cb_vassalize_mission
				months = 1200
			}
		}
	}
}

# es_mission_events.11.t: "Kings of Morrowind"
# es_mission_events.11.d: "Morrowind, a land of volcanos, ashlands and fungal forests, lies at the feet of the [Root.Monarch.GetTitle] [Root.Monarch.GetName]. Nothing prevents [Root.Monarch.GetHerHim] from proclaiming [Root.Monarch.GetHerselfHimself] the ruler of all Morrowind and ascending the throne in Mournhold. However, perhaps the time of the Kingdom of Morrowind has passed? Perhaps the time has come to bring these lands into agreement with the authority of the one who rules directly from [Root.Capital.GetName]? After all, no one will dare to object to our right to do so."
# es_mission_events.11.a: "[Root.Monarch.GetTitle] [Root.Monarch.GetName] will be coronated in the city of Mournhold!"
# es_mission_events.11.b: "Kingdom of Morrowind shall be dissolved."
# every_nation_of_velothi_culture_will_become: "Every nation of §YVelothi§! cultural group, which capital is located in §YMorrowind§! superregion will become a §GTributary Subject§! of §Y[Root.GetName]§!."
# get_vassalisation_cb_against_every_country_of_velothi_culture: "§Y[Root.GetName]§! will get a §GVassalisation Casus Beli§! for the duration of §Y100 years§! against every nation of §YVelothi§! cultural group, which capital is located in §YMorrowind§! superregion."

country_event = {
	id = es_mission_events.11
	title = "es_mission_events.11.t"
	desc = "es_mission_events.11.d"
	picture = ES_TRIBUNAL_BANNERS_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = "es_mission_events.11.a"
		ai_chance = { factor = 100 }
		
		trigger = {
			OR = {
				is_emperor = yes
				is_emperor_of_china = yes
				has_reform = cyrodiilic_empire_reform
				is_nomad = yes
				primary_culture = vampire
			}
		}
		
		if = {
			limit = {
				3248 = { NOT = { culture = ROOT } }
			}
			3248 = { ROOT = { add_accepted_culture = PREV } }
		}
		else = {
			add_prestige = 20
		}
		
		custom_tooltip = every_nation_of_velothi_culture_will_become
		hidden_effect = {
			every_country = {
				limit = {
					culture_group = velothi_cg
					capital_scope = { 
						superregion = morrowind_superregion 
					}
					NOT = { is_subject_of = ROOT }
				}
				ROOT = { create_subject = { subject_type = tributary_state subject = PREV } }
			}
		}
	}
	
	option = {
		name = "es_mission_events.11.b"
		ai_chance = { factor = 100 }
		custom_tooltip = get_vassalisation_cb_against_every_country_of_velothi_culture
		every_country = {
			limit = {
				culture_group = velothi_cg
				capital_scope = { superregion = morrowind_superregion }
				NOT = { is_subject_of = ROOT }
			}
			reverse_add_casus_belli  = {
				target = PREV
				type = cb_vassalize_mission
				months = 1200
			}
		}
	}
}

# es_mission_events.12.t: "Jarls of Skyrim"
# es_mission_events.12.d: "Skyrim, a land of winter, mead and giants, lies at the feet of the [Root.Monarch.GetTitle] [Root.Monarch.GetName]. Nothing prevents [Root.Monarch.GetHerHim] from proclaiming [Root.Monarch.GetHerselfHimself] the ruler of all Skyrim and ascending the throne in Windhelm. However, perhaps the time of Skyrim Jarldoms has passed? Perhaps the time has come to bring these lands into agreement with the authority of the one who rules directly from [Root.Capital.GetName]? After all, no one will dare to object to our right to do so."
# es_mission_events.12.a: "[Root.Monarch.GetTitle] [Root.Monarch.GetName] will be coronated in the city of Windhelm!"
# es_mission_events.12.b: "Jarldoms of Skyrim shall be dissolved."
# every_nation_of_nordic_culture_will_become: "Every nation of §YNordic§! culture, which capital is located in §YSkyrim§! superregion will become a §GTributary Subject§! of §Y[Root.GetName]§!."
# get_vassalisation_cb_against_every_country_of_nordic_culture: "§Y[Root.GetName]§! will get a §GVassalisation Casus Beli§! for the duration of §Y100 years§! against every nation of §YNordic§! culture, which capital is located in §YSkyrim§! superregion."

country_event = {
	id = es_mission_events.12
	title = "es_mission_events.12.t"
	desc = "es_mission_events.12.d"
	picture = ES_WINDHELM_THRONE_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = "es_mission_events.12.a"
		ai_chance = { factor = 100 }
		
		trigger = {
			OR = {
				is_emperor = yes
				is_emperor_of_china = yes
				has_reform = cyrodiilic_empire_reform
				is_nomad = yes
				primary_culture = vampire
			}
		}
		
		if = {
			limit = {
				1275 = { NOT = { culture = ROOT } }
			}
			1275 = { ROOT = { add_accepted_culture = PREV } }
		}
		else = {
			add_prestige = 20
		}
		
		custom_tooltip = every_nation_of_velothi_culture_will_become
		hidden_effect = {
			every_country = {
				limit = {
					primary_culture = nord
					capital_scope = { 
						superregion = skyrim_superregion 
					}
					NOT = { is_subject_of = ROOT }
				}
				ROOT = { create_subject = { subject_type = tributary_state subject = PREV } }
			}
		}
	}
	
	option = {
		name = "es_mission_events.12.b"
		ai_chance = { factor = 100 }
		custom_tooltip = get_vassalisation_cb_against_every_country_of_velothi_culture
		every_country = {
			limit = {
				primary_culture = nord
				capital_scope = { superregion = skyrim_superregion }
				NOT = { is_subject_of = ROOT }
			}
			reverse_add_casus_belli  = {
				target = PREV
				type = cb_vassalize_mission
				months = 1200
			}
		}
	}
}

# es_mission_events.13.t: "Kings of the Reach"
# es_mission_events.13.d: "Reach, a land of mountain peaks and pure rivers, lies at the feet of the [Root.Monarch.GetTitle] [Root.Monarch.GetName]. Nothing prevents [Root.Monarch.GetHerHim] from proclaiming [Root.Monarch.GetHerselfHimself] the ruler of all Reach and ascending the throne in Karthwasten. However, perhaps the time of Reachmen Kingdoms has passed? Perhaps the time has come to bring these lands into agreement with the authority of the one who rules directly from [Root.Capital.GetName]? After all, no one will dare to object to our right to do so."
# es_mission_events.13.a: "[Root.Monarch.GetTitle] [Root.Monarch.GetName] will be coronated in the city of Karthwasten!"
# es_mission_events.13.b: "Kingdoms of the Reach shall be dissolved."
# every_nation_of_reachmen_culture_will_become: "Every nation of §YReachmen§! culture, which capital is located in §YReach§! region will become a §GTributary Subject§! of §Y[Root.GetName]§!."
# get_vassalisation_cb_against_every_country_of_reachmen_culture: "§Y[Root.GetName]§! will get a §GVassalisation Casus Beli§! for the duration of §Y100 years§! against every nation of §YReachmen§! culture, which capital is located in §YReach§! region."

country_event = {
	id = es_mission_events.13
	title = "es_mission_events.13.t"
	desc = "es_mission_events.13.d"
	picture = ES_REACHMEN_THRONE_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = "es_mission_events.13.a"
		ai_chance = { factor = 100 }
		
		trigger = {
			OR = {
				is_emperor = yes
				is_emperor_of_china = yes
				has_reform = cyrodiilic_empire_reform
				is_nomad = yes
				primary_culture = vampire
			}
		}
		
		if = {
			limit = {
				7220 = { NOT = { culture = ROOT } }
			}
			7220 = { ROOT = { add_accepted_culture = PREV } }
		}
		else = {
			add_prestige = 20
		}
		
		custom_tooltip = every_nation_of_velothi_culture_will_become
		hidden_effect = {
			every_country = {
				limit = {
					primary_culture = reachmen
					capital_scope = { 
						region = reach_lr
					}
					NOT = { is_subject_of = ROOT }
				}
				ROOT = { create_subject = { subject_type = tributary_state subject = PREV } }
			}
		}
	}
	
	option = {
		name = "es_mission_events.13.b"
		ai_chance = { factor = 100 }
		custom_tooltip = get_vassalisation_cb_against_every_country_of_velothi_culture
		every_country = {
			limit = {
				primary_culture = reachmen
				capital_scope = { region = reach_lr }
				NOT = { is_subject_of = ROOT }
			}
			reverse_add_casus_belli  = {
				target = PREV
				type = cb_vassalize_mission
				months = 1200
			}
		}
	}
}

# es_mission_events.14.t: "Camoran Throne"
# es_mission_events.14.d: "Valenwood, a land of endless forests, lies at the feet of the [Root.Monarch.GetTitle] [Root.Monarch.GetName]. Nothing prevents [Root.Monarch.GetHerHim] from proclaiming [Root.Monarch.GetHerselfHimself] the ruler of all Valenwood and ascending the throne in Falinesti. However, perhaps the time of Valenwood Kingdom has passed? Perhaps the time has come to bring these lands into agreement with the authority of the one who rules directly from [Root.Capital.GetName]? After all, no one will dare to object to our right to do so."
# es_mission_events.14.a: "[Root.Monarch.GetTitle] [Root.Monarch.GetName] will be coronated in the city of Windhelm!"
# es_mission_events.1b.t: "Treethanes of Valenwood shall be dissolved."
# every_nation_of_bosmer_culture_will_become: "Every nation of §YBosmer§! culture, which capital is located in §YValenwood§! superregion will become a §GTributary Subject§! of §Y[Root.GetName]§!."
# get_vassalisation_cb_against_every_country_of_bosmer_culture: "§Y[Root.GetName]§! will get a §GVassalisation Casus Beli§! for the duration of §Y100 years§! against every nation of §YBosmer§! culture, which capital is located in §YValenwood§! superregion."

country_event = {
	id = es_mission_events.14
	title = "es_mission_events.14.t"
	desc = "es_mission_events.14.d"
	picture = ES_CAMORAN_THRONE_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = "es_mission_events.14.a"
		ai_chance = { factor = 100 }
		
		trigger = {
			OR = {
				is_emperor = yes
				is_emperor_of_china = yes
				has_reform = cyrodiilic_empire_reform
				is_nomad = yes
				primary_culture = vampire
			}
		}
		
		if = {
			limit = {
				820 = { NOT = { culture = ROOT } }
			}
			820 = { ROOT = { add_accepted_culture = PREV } }
		}
		else = {
			add_prestige = 20
		}
		
		custom_tooltip = every_nation_of_bosmer_culture_will_become
		hidden_effect = {
			every_country = {
				limit = {
					primary_culture = bosmer
					capital_scope = { 
						superregion = valenwood_superregion 
					}
					NOT = { is_subject_of = ROOT }
				}
				ROOT = { create_subject = { subject_type = tributary_state subject = PREV } }
			}
		}
	}
	
	option = {
		name = "es_mission_events.14.b"
		ai_chance = { factor = 100 }
		custom_tooltip = get_vassalisation_cb_against_every_country_of_bosmer_culture
		every_country = {
			limit = {
				primary_culture = bosmer
				capital_scope = { superregion = valenwood_superregion }
				NOT = { is_subject_of = ROOT }
			}
			reverse_add_casus_belli  = {
				target = PREV
				type = cb_vassalize_mission
				months = 1200
			}
		}
	}
}