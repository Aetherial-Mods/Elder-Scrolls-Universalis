divine_blessings_mechanic = {
	alert_icon_gfx = GFX_alerticons_government_mechanics
	alert_icon_index = 1
	available = {
		OR = {
			has_dlc = "The Cossacks"
			has_dlc = "Cradle of Civilization"
			has_dlc = "Domination"
			has_dlc = "Rights of Man"
			has_dlc = "Third Rome"
			has_dlc = "King of Kings"
			has_dlc = "Winds of Change"
		}
	}
	
	interactions = {
		es_preserved_heritage = {
			icon = GFX_russian_rule_dip_button
			trigger = { 
			}
			effect = {
				add_corruption = 1.5
				add_country_modifier = {
					name = es_preserved_heritage_modifier
					duration = 3650
				}
			}
			cooldown_years = 10
			ai_chance = {
				factor = 1
			}
		}
		es_plentiful_harvest = {
			icon = GFX_es_plentiful_harvest
			trigger = { 
			}
			effect = {
				add_corruption = 1.5
				add_country_modifier = {
					name = es_plentiful_harvest_modifier
					duration = 3650
				}
			}
			cooldown_years = 10
			ai_chance = {
				factor = 1
			}
		}
		es_unstoppable_warriors = {
			icon = GFX_devshirme_conscript_heathen_recruits_button
			trigger = { 
			}
			effect = {
				add_corruption = 1.5
				add_country_modifier = {
					name = es_unstoppable_warriors_modifier
					duration = 3650
				}
			}
			cooldown_years = 10
			ai_chance = {
				factor = 1
			}
		}
	}
}