land_of_the_christian_sun_mechanic = {
	alert_icon_gfx = GFX_alerticons_government_mechanics
	alert_icon_index = 1
	available = {
		OR = {
			has_dlc = "The Cossacks"
			has_dlc = "Cradle of Civilization"
			has_dlc = "Domination"
			has_dlc = "Rights of Man"
			has_dlc = "Third Rome"
			has_dlc = "King of Kings"
			has_dlc = "Winds of Change"
		}
	}
	
	powers = {
		christian_japan_1 = {
			max = 250
			monarch_power = ADM
			base_monthly_growth = 0.1
		}
		christian_japan_2 = {
			max = 250
			monarch_power = DIP
			base_monthly_growth = 0.1
		}
		christian_japan_3 = {
			max = 250
			monarch_power = MIL
			base_monthly_growth = 0.1
		}
	}
	
	interactions = {
		jap_ability_invite_cardinal = {
			icon = GFX_christian_japan_adm_button
			cost_type = christian_japan_1
			cost = 100
			trigger = {
			}
			effect = {
				add_church_power = 50
				add_fervor = 50
				add_papal_influence = 50
				add_patriarch_authority = 0.15
				add_piety = 0.15
				add_karma = 15
				es_add_stability_1 = yes 
			}
			cooldown_years = 10
			ai_chance = {
				factor = 1
			}
		}
		jap_ability_military_parade = {
			icon = GFX_christian_japan_dip_button 
			cost_type = christian_japan_2
			cost = 100
			trigger = { 
			}
			effect = { 
				random_owned_province = {
					add_base_tax = 1
					add_base_production = 1
					add_base_manpower = 1
				}
				add_army_tradition = 5
				add_navy_tradition = 5
			}
			cooldown_years = 10
			ai_chance = {
				factor = 1
			}
		}
		jap_ability_bolster_morale = {
			icon = GFX_christian_japan_mil_button
			cost_type = christian_japan_3
			cost = 100
			trigger = {
				NOT = { has_country_modifier = es_bolstered_moral }
			}
			effect = { 
				add_country_modifier = {
					name = es_bolstered_moral
					duration = 3650
				}
			}
			cooldown_years = 10
			ai_chance = {
				factor = 1
			}
		}
	}
}