shogunate_mechanic = {
	alert_icon_gfx = GFX_alerticons_government_mechanics
	alert_icon_index = 1
	available = {
		OR = {
			has_dlc = "The Cossacks"
			has_dlc = "Cradle of Civilization"
			has_dlc = "Domination"
			has_dlc = "Rights of Man"
			has_dlc = "Third Rome"
			has_dlc = "King of Kings"
			has_dlc = "Winds of Change"
		}
	}
	
	interactions = {
		es_influence_subjects_action = {
			icon = GFX_expel_ronin
			trigger = {
				legitimacy_equivalent = 20
				num_of_subjects = 1
			}
			effect = { 
				add_legitimacy = -20
				add_republican_tradition = -20
				add_devotion = -20
				add_meritocracy = -20
				add_horde_unity = -20
				add_country_modifier = {
					name = overlord_influence_subjects
					duration = 3650
				}
			}
			cooldown_years = 10
			ai_chance = {
				factor = 1
				modifier = {
					factor = 0
					NOT = { legitimacy = 70 }
				}
			}
		}
		es_mobilize_subjects_action = {
			icon = GFX_sankin_kotai
			trigger = {
				legitimacy_equivalent = 20
				num_of_subjects = 1
			}
			effect = { 
				add_legitimacy = -20
				add_republican_tradition = -20
				add_devotion = -20
				add_meritocracy = -20
				add_horde_unity = -20
				add_country_modifier = {
					name = overlord_mobilize_subjects
					duration = 3650
				}
			}
			cooldown_years = 10
			ai_chance = {
				factor = 1
				modifier = {
					factor = 0
					OR = {
						NOT = { is_at_war = yes }
						NOT = { legitimacy = 70 }
					}
				}
			}
		}
		es_weaken_subjects_action = {
			icon = GFX_sword_hunt
			trigger = {
				legitimacy_equivalent = 20
				num_of_subjects = 1
			}
			effect = { 
				add_legitimacy = -20
				add_republican_tradition = -20
				add_devotion = -20
				add_meritocracy = -20
				add_horde_unity = -20
				add_country_modifier = {
					name = overlord_weaken_subjects
					duration = 3650
				}
			}
			cooldown_years = 10
			ai_chance = {
				factor = 1
				modifier = {
					factor = 0
					OR = {
						all_subject_country = {
							NOT = { liberty_desire = 50 }
						}
						NOT = { legitimacy = 70 }
					}
				}
			}
		}
	}
}