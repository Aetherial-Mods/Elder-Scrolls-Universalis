table_of_ranks_mechanic = {
	alert_icon_gfx = GFX_alerticons_government_mechanics
	alert_icon_index = 1
	available = {
		OR = {
			has_dlc = "The Cossacks"
			has_dlc = "Cradle of Civilization"
			has_dlc = "Domination"
			has_dlc = "Rights of Man"
			has_dlc = "Third Rome"
			has_dlc = "King of Kings"
			has_dlc = "Winds of Change"
		}
	}
	
	powers = {
		table_of_ranks_adm_power = {
			max = 250
			monarch_power = ADM
			base_monthly_growth = 0.10
		}
		table_of_ranks_dip_power = {
			max = 250
			monarch_power = DIP
			base_monthly_growth = 0.10
		}
		table_of_ranks_mil_power = {
			max = 250
			monarch_power = MIL
			base_monthly_growth = 0.10
		}
	}
	interactions = {
		appoint_chancellor = {
			icon = GFX_table_of_ranks_adm_button
			cost_type = table_of_ranks_adm_power
			cost = 100
			effect = {
				add_corruption = -10
				add_adm_power = 150
				random_list = {
					33 = { define_advisor = { type = esu_steward skill = 3 cost_multiplier = 0.05 } }
					33 = { define_advisor = { type = esu_inspector skill = 3 cost_multiplier = 0.05 } }
					33 = { define_advisor = { type = esu_curator skill = 3 cost_multiplier = 0.05 } }
				}
			}
			cooldown_years = 10
			ai_chance = {
				factor = 100
			}
		}
		appoint_general_admiral = {
			icon = GFX_table_of_ranks_dip_button
			cost_type = table_of_ranks_dip_power
			cost = 100
			effect = {
				add_navy_tradition = 10
				add_dip_power = 150
				create_admiral = {
					tradition = 100
				}
			}
			cooldown_years = 10
			ai_chance = {
				factor = 100
			}
		}
		appoint_general_field_marshal = {
			icon = GFX_table_of_ranks_mil_button
			cost_type = table_of_ranks_mil_power
			cost = 100
			effect = {
				add_army_tradition = 10
				add_mil_power = 150
				random_list = {
					10 = { create_general = { tradition = 10 } }
					10 = { create_general = { tradition = 20 } }
					10 = { create_general = { tradition = 30 } }
					10 = { create_general = { tradition = 40 } }
					10 = { create_general = { tradition = 50 } }
					10 = { create_general = { tradition = 60 } }
					10 = { create_general = { tradition = 70 } }
					10 = { create_general = { tradition = 80 } }
					10 = { create_general = { tradition = 90 } }
					10 = { create_general = { tradition = 100 } }
				}
			}
			cooldown_years = 10
			ai_chance = {
				factor = 100
			}
		}
	}
}