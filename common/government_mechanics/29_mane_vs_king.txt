mane_vs_king_mechanic = {
	available = {
		OR = {
			has_dlc = "The Cossacks"
			has_dlc = "Cradle of Civilization"
			has_dlc = "Domination"
			has_dlc = "Rights of Man"
			has_dlc = "Third Rome"
			has_dlc = "King of Kings"
			has_dlc = "Winds of Change"
		}
	}

	powers = {
		mane_vs_king_governmental_power = {
			gui = mane_vs_king_gov_mech
			max = 100	#Monarchy direction
			min = -100	#Parliament direction
			base_monthly_growth = 0
			scaled_modifier = {
				trigger = {
					has_government_power = { 
						mechanic_type = mane_vs_king_mechanic
						power_type = mane_vs_king_governmental_power
						value = 0
					}
				}
				modifier = {
					prestige_per_development_from_conversion = -0.05
					missionary_maintenance_cost = 0.50
					nobility_influence_modifier = 0.25
					discipline = 0.05
					global_tax_modifier = 0.10
				}
				start_value = 0
				end_value = 100
			}
			reverse_scaled_modifier = {
				trigger = {
					NOT = {
						has_government_power = { 
							mechanic_type = mane_vs_king_mechanic
							power_type = mane_vs_king_governmental_power
							value = 0
						}
					}
				}
				modifier = {
					tolerance_heathen = -5
					tolerance_heretic = -5
					priests_influence_modifier = 0.25
					global_missionary_strength = 0.05
					land_morale = 0.10
				}
				start_value = -100
				end_value = 0
			}
			on_max_reached = {
			}
			on_min_reached = {
			}
		}
	}
}