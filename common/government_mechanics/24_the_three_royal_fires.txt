the_three_royal_fires_mechanic = {
	alert_icon_gfx = GFX_alerticons_government_mechanics
	alert_icon_index = 1
	available = {
		OR = {
			has_dlc = "The Cossacks"
			has_dlc = "Cradle of Civilization"
			has_dlc = "Domination"
			has_dlc = "Rights of Man"
			has_dlc = "Third Rome"
			has_dlc = "King of Kings"
			has_dlc = "Winds of Change"
		}
	}

	powers = {
		asha_vahishta = {
			gui = three_flames_gov_mech 
			show_before_interactions = yes
			max = 100
			min = -100
			base_monthly_growth = 0
			scaled_modifier = {
				trigger = {
					has_government_power = { 
						mechanic_type = the_three_royal_fires_mechanic
						power_type = asha_vahishta
						value = 0
					}
				}
				modifier = {
					improve_relation_modifier = 0.50
					monthly_favor_modifier = 0.10
					diplomatic_reputation = 1.5
					global_spy_defence = -0.35
					unjustified_demands = 0.25
				}
				start_value = 0
				end_value = 100			
			}
			reverse_scaled_modifier = {
				trigger = {
					NOT = {
						has_government_power = { 
							mechanic_type = the_three_royal_fires_mechanic
							power_type = asha_vahishta
							value = 0
						}
					}
				}
				modifier = {
					rebel_support_efficiency = 0.5
					province_warscore_cost = -0.10
					ae_impact = 0.15
					spy_offence = 0.35
					discovered_relations_impact = 0.25
				}
				start_value = -100
				end_value = 0
			}
		}
	}
	interactions = {
		adur_farnbag_interaction = {
			gui = three_flames_interaction_button			
			icon = GFX_adur_farnbag_interaction
			cost_type = asha_vahishta
			cost = 15
			effect = {
				add_adm_power = 100
				add_estate_loyalty = {
					estate = all
					loyalty = 10
					short = yes
				}
				add_splendor = 100
			}
			cooldown_years = 10
			ai_chance = {
				factor = 0	
			}
		}
		adur_burzhen_mihr_interaction = {
			gui = three_flames_interaction_button			
			icon = GFX_adur_burzhen_mihr_interaction
			cost_type = asha_vahishta
			cost = 15
			effect = {
				add_dip_power = 100
				add_mercantilism = 1
				capital_scope = {
					random_area_province = {
						limit = {
							owner = {
								tag = ROOT
							}
						}
						add_random_development = 1
					}
				}
			}
			cooldown_years = 10
			ai_chance = {
				factor = 0	
			}
		}
		adur_gushnasp_interaction = {
			gui = three_flames_interaction_button
			icon = GFX_adur_gushnasp_interaction
			cost_type = asha_vahishta
			cost = 15
			effect = {
				add_mil_power = 100
				add_army_tradition = 5
				define_advisor = {
					type = esu_trainer
					skill = 3
					discount = yes
				}
			}
			cooldown_years = 10
			ai_chance = {
				factor = 10
				modifier = {
					factor = 100
					is_at_war = yes
				}
				modifier = {
					factor = 0
					is_at_war = no
				}
			}
		}
	}
}