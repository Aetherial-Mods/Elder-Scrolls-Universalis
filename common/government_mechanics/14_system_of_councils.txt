system_of_councils_mechanic = {
	alert_icon_gfx = GFX_alerticons_government_mechanics
	alert_icon_index = 1
	available = {
		OR = {
			has_dlc = "The Cossacks"
			has_dlc = "Cradle of Civilization"
			has_dlc = "Domination"
			has_dlc = "Rights of Man"
			has_dlc = "Third Rome"
			has_dlc = "King of Kings"
			has_dlc = "Winds of Change"
		}
	}

	powers = {
		council_consensus = {
			max = 100
			base_monthly_growth = 0
			scaled_modifier = {
				modifier = {
					tolerance_own = 5
					tolerance_heathen = -5
					tolerance_heretic = -5
					global_missionary_strength = 0.05
					missionary_maintenance_cost = -0.50
					administrative_efficiency = 0.05
					global_religious_conversion_resistance = 0.50
				}
			}
		}
	}
	interactions = {
		royal_council_interaction = {
			icon = GFX_system_of_councils_adm_button
			cost_type = council_consensus
			cost = 50
			effect = {
				add_country_modifier = {
					name = royal_council_meeting_mod
					duration = 3650
				}
			}
			ai_chance = {
				factor = 0
			}
		}
		state_council_interaction = {
			icon = GFX_system_of_councils_dip_button
			cost_type = council_consensus
			cost = 50
			effect = {
				add_country_modifier = {
					name = state_council_meeting_mod
					duration = 3650
				}
			}
			ai_chance = {
				factor = 0
			}
		}
		war_council_interaction = {
			icon = GFX_system_of_councils_mil_button
			cost_type = council_consensus
			cost = 50
			effect = {
				add_country_modifier = {
					name = war_council_meeting_mod
					duration = 3650
				}
			}
			ai_chance = {
				factor = 0
			}
		}
	}
}