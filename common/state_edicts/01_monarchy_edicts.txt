# Monarchy
# local_manpower_modifier = 0.25
# regiment_recruit_speed = -0.50
# local_ship_cost = -0.25
# local_prosperity_growth = 0.15
# local_governing_cost = -0.25
# local_years_of_nationalism = -10
# local_missionary_strength = 0.015
# local_build_cost = -0.15
# local_fort_maintenance_modifier = -0.25

es_conscript_local_guard = {
	potential = { government = monarchy }
	allow = { always = yes }
	modifier = { local_fort_maintenance_modifier = -0.25 }
	color = { 92 192 224 }
	
	ai_will_do = {
		factor = 10
		modifier = {
			factor = 0
			all_province_in_state = {
				NOT = { has_building = fort_15th }
				NOT = { has_building = fort_16th }
				NOT = { has_building = fort_17th }
				NOT = { has_building = fort_18th }
			}
		}
		modifier = {
			factor = 5
			any_province_in_state = {
				OR = {
					has_building = fort_15th
					has_building = fort_16th
					has_building = fort_17th
					has_building = fort_18th
				}
			}
		}
	}
}

es_broaden_land_rights = {
	potential = { government = monarchy }
	allow = { always = yes }
	modifier = { local_build_cost = -0.15 }
	color = { 255 178 102 }
	
	#ROOT - Country, FROM - Province, if all provinces in a state, that are owned by the country, meet this requirement, the alert will appear.
	notify_trigger = {
		FROM = {
			NOT = { num_free_building_slots = 1 }
		}
	}
	
	ai_will_do = {
		factor = 10
		modifier = {
			factor = 0
			all_province_in_state = {
				NOT = { num_free_building_slots = 1 }
			}
		}
		modifier = {
			factor = 5
			any_province_in_state = {
				num_free_building_slots = 1
			}
		}
	}
}

es_fight_false_believes = {
	potential = { government = monarchy }
	allow = { always = yes }
	modifier = { local_missionary_strength = 0.015 }
	color = { 236 78 78 }
	
	#ROOT - Country, FROM - Province, if all provinces in a state, that are owned by the country, meet this requirement, the alert will appear.
	notify_trigger = {
		FROM = {
			has_owner_religion = yes
		}
	}
	
	ai_will_do = {
		factor = 10
		modifier = {
			factor = 0
			all_province_in_state = {
				religion = ROOT
			}
		}
		modifier = {
			factor = 5
			any_province_in_state = {
				NOT = { religion = ROOT }
			}
		}
	}
}

es_support_local_authorities = {
	potential = { government = monarchy }
	allow = { always = yes }
	modifier = { local_years_of_nationalism = -10 }
	color = { 36 39 64 }
	
	#ROOT - Country, FROM - Province, if all provinces in a state, that are owned by the country, meet this requirement, the alert will appear.
	notify_trigger = {
		FROM = {
			NOT = { nationalism = 1 }
		}
	}
	
	ai_will_do = {
		factor = 10
		modifier = {
			factor = 0
			all_province_in_state = {
				NOT = { nationalism = 1 }
			}
		}
		modifier = {
			factor = 5
			any_province_in_state = {
				nationalism = 10
			}
		}
	}
}

es_grant_autonomous_rights = {
	potential = { government = monarchy }
	allow = { always = yes }
	modifier = { local_governing_cost = -0.25 }
	color = { 181 158 70 }
	
	ai_will_do = {
		factor = 10
		modifier = {
			factor = 0
			all_province_in_state = {
				NOT = { base_tax = 3 }
				NOT = { base_production = 3 }
				NOT = { base_manpower = 3 }
			}
		}
		modifier = {
			factor = 5
			any_province_in_state = {
				base_tax = 7
				base_production = 7
				base_manpower = 7
			}
		}
	}
}

es_encourage_prosperity = {
	potential = { government = monarchy }
	allow = { always = yes }
	modifier = { local_prosperity_growth = 0.15 }
	color = { 69 125 73 }
	
	#ROOT - Country, FROM - Province, if all provinces in a state, that are owned by the country, meet this requirement, the alert will appear.
	notify_trigger = {
		FROM = {
			is_prosperous = yes
		}
	}
	
	ai_will_do = {
		factor = 10
		modifier = {
			factor = 0
			all_province_in_state = {
				is_prosperous = yes
			}
		}
		modifier = {
			factor = 5
			NOT = {
				any_province_in_state = {
					is_prosperous = yes
				}
			}
		}
	}
}

es_subsidize_shipyards = {
	potential = { government = monarchy }
	allow = { always = yes }
	modifier = { local_ship_cost = -0.25 }
	color = { 18 105 230 }
	
	#ROOT - Country, FROM - Province, if all provinces in a state, that are owned by the country, meet this requirement, the alert will appear.
	notify_trigger = {
		FROM = {
			NOT = { has_port = yes }
		}
	}
	
	ai_will_do = {
		factor = 10
		modifier = {
			factor = 0
			owner = {
				navy_size_percentage = 0.95
			}
		}
		modifier = {
			factor = 0
			all_province_in_state = {
				NOT = { has_port = yes }
			}
		}
		modifier = {
			factor = 5
			owner = {
				NOT = { navy_size_percentage = 0.50 }
			}
		}
	}
}

es_boost_army_recruitment = {
	potential = { government = monarchy }
	allow = { always = yes }
	modifier = { regiment_recruit_speed = -0.50 }
	color = { 4 179 75 }
	
	ai_will_do = {
		factor = 10
		modifier = {
			factor = 0
			owner = {
				NOT = { is_at_war = yes }
			}
		}
		modifier = {
			factor = 5
			owner = {
				NOT = { army_size_percentage = 0.5 }
			}
		}
	}
}

es_expand_army_recruitment = {
	potential = { government = monarchy }
	allow = { always = yes }
	modifier = { local_manpower_modifier = 0.25 }
	color = { 193 202 161 }
	
	ai_will_do = {
		factor = 10
		modifier = {
			factor = 0
			owner = {
				manpower_percentage = 0.5
			}
		}
		modifier = {
			factor = 5
			all_province_in_state = {
				base_manpower = 5
			}
			any_province_in_state = {
				base_manpower = 10
			}
		}
	}
}