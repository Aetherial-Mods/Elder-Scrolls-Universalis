# Do not change tags in here without changing every other reference to them.
# If adding new dieties, make sure they are uniquely named.
#######################################################################
#######################################################################
Almalexia = {
	global_unrest = -1
	manpower_recovery_speed = 0.1
	potential = {
		religion = reclamations_pantheon
		OR = { has_country_flag = deity_flag_1 has_country_flag = disable_randomized_deities }
	}
	trigger = {}
	effect = {}
	removed_effect = {}
	sprite = 59
	ai_will_do = {
		factor = 1
	}
}

Nerevar = {
	idea_cost = -0.1
	advisor_cost = -0.1
	potential = {
		religion = reclamations_pantheon
		OR = { has_country_flag = deity_flag_2 has_country_flag = disable_randomized_deities }
	}
	trigger = {}
	effect = {}
	removed_effect = {}
	sprite = 64
	ai_will_do = {
		factor = 1
	}
}

Sotha_Sil = {
	production_efficiency = 0.1
	build_cost = -0.1
	potential = {
		religion = reclamations_pantheon
		OR = { has_country_flag = deity_flag_3 has_country_flag = disable_randomized_deities }
	}
	trigger = {}
	effect = {}
	removed_effect = {}
	sprite = 61
	ai_will_do = {
		factor = 1
	}
}

Vivec = {
	adm_tech_cost_modifier = -0.1
	army_tradition = 0.25
	potential = {
		religion = reclamations_pantheon
		OR = { has_country_flag = deity_flag_4 has_country_flag = disable_randomized_deities }
	}
	trigger = {}
	effect = {}
	removed_effect = {}
	sprite = 60
	ai_will_do = {
		factor = 1
	}
}

Dagoth_Ur = {
	global_trade_goods_size_modifier = 0.10
	free_leader_pool = 1
	potential = {
		religion = reclamations_pantheon
		OR = { has_country_flag = deity_flag_5 has_country_flag = disable_randomized_deities }
	}
	trigger = {}
	effect = {}
	removed_effect = {}
	sprite = 102
	ai_will_do = {
		factor = 1
	}
}
