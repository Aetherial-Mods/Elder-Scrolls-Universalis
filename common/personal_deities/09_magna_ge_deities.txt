ge_magnus = {
	monarch_admin_power = 1
	global_rebel_suppression_efficiency = 0.15
	potential = {
		OR = {
			AND = {
				religion = magne_ge
				OR = { has_country_flag = deity_flag_1 has_country_flag = disable_randomized_deities }
			}
			AND = {
				religion = old_gods_cult
				OR = { has_country_flag = deity_flag_14 has_country_flag = disable_randomized_deities }
			}
		}
	}
	trigger = {}
	effect = {}
	removed_effect = {}
	sprite = 80
	ai_will_do = {
		factor = 1
	}
}

ge_mnemo_li = {
	diplomatic_reputation = 0.5
	global_autonomy = -0.05
	potential = {
		OR = {
			AND = {
				religion = magne_ge
				OR = { has_country_flag = deity_flag_2 has_country_flag = disable_randomized_deities }
			}
			AND = {
				religion = old_gods_cult
				OR = { has_country_flag = deity_flag_15 has_country_flag = disable_randomized_deities }
			}
		}
	}
	trigger = {}
	effect = {}
	removed_effect = {}
	sprite = 81
	ai_will_do = {
		factor = 1
	}
}

ge_merid_nunda = {
	global_monthly_devastation = -0.10
	manpower_recovery_speed = 0.1
	potential = {
		OR = {
			AND = {
				religion = magne_ge
				OR = { has_country_flag = deity_flag_3 has_country_flag = disable_randomized_deities }
			}
			AND = {
				religion = khajiiti_pantheon
				OR = { has_country_flag = deity_flag_16 has_country_flag = disable_randomized_deities }
			}
			AND = {
				religion = old_gods_cult
				OR = { has_country_flag = deity_flag_16 has_country_flag = disable_randomized_deities }
			}
		}
	}
	trigger = {}
	effect = {}
	removed_effect = {}
	sprite = 82
	ai_will_do = {
		factor = 1
	}
}

ge_una = {
	trade_efficiency = 0.1
	advisor_cost = -0.10
	potential = {
		OR = {
			AND = {
				religion = magne_ge
				OR = { has_country_flag = deity_flag_4 has_country_flag = disable_randomized_deities }
			}
			AND = {
				religion = old_gods_cult
				OR = { has_country_flag = deity_flag_17 has_country_flag = disable_randomized_deities }
			}
		}
	}
	trigger = {}
	effect = {}
	removed_effect = {}
	sprite = 83
	ai_will_do = {
		factor = 1
	}
}

ge_xero_lyg = {
	state_governing_cost = -0.1
	global_prosperity_growth = 0.10
	potential = {
		OR = {
			AND = {
				religion = magne_ge
				OR = { has_country_flag = deity_flag_5 has_country_flag = disable_randomized_deities }
			}
			AND = {
				religion = old_gods_cult
				OR = { has_country_flag = deity_flag_18 has_country_flag = disable_randomized_deities }
			}
		}
	}
	trigger = {}
	effect = {}
	removed_effect = {}
	sprite = 84
	ai_will_do = {
		factor = 1
	}
}

ge_iana_lor = {
	reduced_liberty_desire_on_other_continent = -10
	overextension_impact_modifier = -0.10
	potential = {
		OR = {
			AND = {
				religion = magne_ge
				OR = { has_country_flag = deity_flag_6 has_country_flag = disable_randomized_deities }
			}
			AND = {
				religion = old_gods_cult
				OR = { has_country_flag = deity_flag_19 has_country_flag = disable_randomized_deities }
			}
		}
	}
	trigger = {}
	effect = {}
	removed_effect = {}
	sprite = 146
	ai_will_do = {
		factor = 1
	}
}

ge_londa_vera = {
	diplomats = 1
	envoy_travel_time = -0.25
	potential = {
		OR = {
			AND = {
				religion = magne_ge
				OR = { has_country_flag = deity_flag_7 has_country_flag = disable_randomized_deities }
			}
			AND = {
				religion = old_gods_cult
				OR = { has_country_flag = deity_flag_20 has_country_flag = disable_randomized_deities }
			}
		}
	}
	trigger = {}
	effect = {}
	removed_effect = {}
	sprite = 147
	ai_will_do = {
		factor = 1
	}
}

ge_prime_archon = {
	reform_progress_growth = 0.10
	great_project_upgrade_time = -0.15
	potential = {
		OR = {
			AND = {
				religion = magne_ge
				OR = { has_country_flag = deity_flag_8 has_country_flag = disable_randomized_deities }
			}
			AND = {
				religion = old_gods_cult
				OR = { has_country_flag = deity_flag_21 has_country_flag = disable_randomized_deities }
			}
		}
	}
	trigger = {}
	effect = {}
	removed_effect = {}
	sprite = 148
	ai_will_do = {
		factor = 1
	}
}

ge_sheza_rana = {
	army_tradition_decay = -0.01
	navy_tradition_decay = -0.01
	potential = {
		OR = {
			AND = {
				religion = magne_ge
				OR = { has_country_flag = deity_flag_9 has_country_flag = disable_randomized_deities }
			}
			AND = {
				religion = old_gods_cult
				OR = { has_country_flag = deity_flag_22 has_country_flag = disable_randomized_deities }
			}
		}
	}
	trigger = {}
	effect = {}
	removed_effect = {}
	sprite = 149
	ai_will_do = {
		factor = 1
	}
}

ge_unala_se = {
	artillery_power = 0.10
	build_time = -0.15
	potential = {
		OR = {
			AND = {
				religion = magne_ge
				OR = { has_country_flag = deity_flag_10 has_country_flag = disable_randomized_deities }
			}
			AND = {
				religion = old_gods_cult
				OR = { has_country_flag = deity_flag_23 has_country_flag = disable_randomized_deities }
			}
		}
	}
	trigger = {}
	effect = {}
	removed_effect = {}
	sprite = 150
	ai_will_do = {
		factor = 1
	}
}

ge_valia_sha = {
	cavalry_power = 0.10
	cavalry_flanking = 0.10
	potential = {
		OR = {
			AND = {
				religion = magne_ge
				OR = { has_country_flag = deity_flag_11 has_country_flag = disable_randomized_deities }
			}
			AND = {
				religion = old_gods_cult
				OR = { has_country_flag = deity_flag_24 has_country_flag = disable_randomized_deities }
			}
		}
	}
	trigger = {}
	effect = {}
	removed_effect = {}
	sprite = 151
	ai_will_do = {
		factor = 1
	}
}