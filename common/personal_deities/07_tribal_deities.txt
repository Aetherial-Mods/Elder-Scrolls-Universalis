Dibe = {
	diplomatic_reputation = 0.5
	ae_impact = -0.1
	potential = {
		religion = kothri_pantheon
	}
	trigger = {}
	effect = {}
	removed_effect = {}
	sprite = 119
	ai_will_do = {
		factor = 1
	}
}

Kin = {
	land_attrition = -0.1
	movement_speed = 0.1
	potential = {
		religion = kothri_pantheon
	}
	trigger = {}
	effect = {}
	removed_effect = {}
	sprite = 136
	ai_will_do = {
		factor = 1
	}
}

Kota = {
	fabricate_claims_cost = -0.10
	fire_damage = 0.10
	potential = {
		religion = hist
	}
	trigger = {}
	effect = {}
	removed_effect = {}
	sprite = 104
	ai_will_do = {
		factor = 1
	}
}

Atak = {
	manpower_recovery_speed = 0.10
	advisor_cost = -0.10
	potential = {
		religion = hist
	}
	trigger = {}
	effect = {}
	removed_effect = {}
	sprite = 105
	ai_will_do = {
		factor = 1
	}
}

Atakota = {
	global_tax_modifier = 0.10
	diplomatic_upkeep = 1
	potential = {
		religion = hist
	}
	trigger = {}
	effect = {}
	removed_effect = {}
	sprite = 106
	ai_will_do = {
		factor = 1
	}
}

Hist = {
	infantry_cost = -0.10
	morale_damage_received = -0.10
	potential = {
		religion = hist
	}
	trigger = {}
	effect = {}
	removed_effect = {}
	sprite = 107
	ai_will_do = {
		factor = 1
	}
}

Sithis = {
	tolerance_own = 1
	culture_conversion_cost = -0.10
	potential = {
		religion = hist
	}
	trigger = {}
	effect = {}
	removed_effect = {}
	sprite = 108
	ai_will_do = {
		factor = 1
	}
}

Lorkh = {
	legitimacy = 0.25
	republican_tradition = 0.25
	devotion = 0.25
	horde_unity = 0.25
	meritocracy = 0.25
	stability_cost_modifier = -0.1
	potential = {
		religion = old_gods_cult
		OR = { has_country_flag = deity_flag_3 has_country_flag = disable_randomized_deities }
	}
	trigger = {}
	effect = {}
	removed_effect = {}
	sprite = 142
	ai_will_do = {
		factor = 1
	}
}

Headsman = {
	infantry_power = 0.10
	cavalry_power = 0.10
	potential = {
		religion = old_gods_cult
		OR = { has_country_flag = deity_flag_10 has_country_flag = disable_randomized_deities }
	}
	trigger = {}
	effect = {}
	removed_effect = {}
	sprite = 135
	ai_will_do = {
		factor = 1
	}
}

Hunter = {
	vassal_income = 0.1
	mercenary_cost = -0.10
	potential = {
		religion = old_gods_cult
		OR = { has_country_flag = deity_flag_11 has_country_flag = disable_randomized_deities }
	}
	trigger = {}
	effect = {}
	removed_effect = {}
	sprite = 134
	ai_will_do = {
		factor = 1
	}
}

Snake_in_the_Stars = {
	global_tax_modifier = 0.10
	land_morale = 0.10
	potential = {
		religion = old_gods_cult
		OR = { has_country_flag = deity_flag_12 has_country_flag = disable_randomized_deities }
	}
	trigger = {}
	effect = {}
	removed_effect = {}
	sprite = 133
	ai_will_do = {
		factor = 1
	}
}

Witch_in_the_Stars = {
	fire_damage = 0.10
	shock_damage_received = -0.10
	potential = {
		religion = old_gods_cult
		OR = { has_country_flag = deity_flag_13 has_country_flag = disable_randomized_deities }
	}
	trigger = {}
	effect = {}
	removed_effect = {}
	sprite = 132
	ai_will_do = {
		factor = 1
	}
}

Fat_Mother = {
	shock_damage_received = -0.10
	land_attrition = -0.10
	potential = {
		religion = four_parents
	}
	trigger = {}
	effect = {}
	removed_effect = {}
	sprite = 110
	ai_will_do = {
		factor = 1
	}
}

Dragon_of_Time = {
	movement_speed = 0.10
	yearly_corruption = -0.1
	potential = {
		religion = four_parents
	}
	trigger = {}
	effect = {}
	removed_effect = {}
	sprite = 111
	ai_will_do = {
		factor = 1
	}
}

Mother_Serpent = {
	army_tradition = 0.25
	garrison_size = 0.15
	potential = {
		religion = four_parents
	}
	trigger = {}
	effect = {}
	removed_effect = {}
	sprite = 112
	ai_will_do = {
		factor = 1
	}
}

Ox = {
	diplomatic_reputation = 0.5
	global_tax_modifier = 0.10
	potential = {
		religion = four_parents
	}
	trigger = {}
	effect = {}
	removed_effect = {}
	sprite = 113
	ai_will_do = {
		factor = 1
	}
}
