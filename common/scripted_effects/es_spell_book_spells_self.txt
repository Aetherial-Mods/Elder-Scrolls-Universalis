#####################
# Alteration Spells #
#####################

candlelight_effect = {
	spell_notification_logic = {
		which = candlelight_spell
	}
	add_ruler_modifier = {
		name = candlelight_spell
		duration = $effect_duration$
	}
}

detect_life_effect = {
	spell_notification_logic = {
		which = detect_life_spell
	}
	add_ruler_modifier = {
		name = detect_life_spell
		duration = $effect_duration$
	}
}

equilibrium_effect = {
	add_manpower = -15
	add_adm_power = 150
	add_dip_power = 150
	add_mil_power = 150
}

oakflesh_effect = {
	spell_notification_logic = {
		which = oakflesh_spell
	}
	add_ruler_modifier = {
		name = oakflesh_spell
		duration = $effect_duration$
	}
}

sea_stride_effect = {
	spell_notification_logic = {
		which = sea_stride_spell
	}
	add_ruler_modifier = {
		name = sea_stride_spell
		duration = $effect_duration$
	}
}

transmutation_effect = {
	add_years_of_income = 3
	add_inflation = 5
	add_corruption = 0.5
}

water_breathing_effect = {
	spell_notification_logic = {
		which = water_breathing_spell
	}
	add_ruler_modifier = {
		name = water_breathing_spell
		duration = $effect_duration$
	}
}

######################
# Conjuration Spells #
######################

bound_armour_effect = {
	spell_notification_logic = {
		which = bound_armour_spell
	}
	add_ruler_modifier = {
		name = bound_armour_spell
		duration = $effect_duration$
	}
}

bound_bow_effect = {
	spell_notification_logic = {
		which = bound_bow_spell
	}
	add_ruler_modifier = {
		name = bound_bow_spell
		duration = $effect_duration$
	}
}

bound_sword_effect = {
	add_mil_power = 100
	spell_notification_logic = {
		which = bound_sword_spell
	}
	add_ruler_modifier = {
		name = bound_sword_spell
		duration = $effect_duration$
	}
}

conjure_atronach_effect = {
	random_owned_province = {
		limit = {
			area = capital
			controlled_by = ROOT
		}
		hidden_effect = {
			set_variable = {
				which = WhileLoopCounter
				value = 0
			}
		}
		custom_tooltip = es_spell_conjure_atronach_effect_tt
		while = {
			limit = {
				NOT = {
					check_variable = {
						which = WhileLoopCounter
						value = 5
					}
				}
			}
			ROOT = {
				infantry = prev
			}
			hidden_effect = {
				change_variable = {
					which = WhileLoopCounter
					value = 1
				}
			}
		}
	}
	spell_notification_logic = {
		which = conjure_atronach_spell
	}
	add_ruler_modifier = {
		name = conjure_atronach_spell
		duration = $effect_duration$
	}
}

conjure_dremora_lord_effect = {
	spell_notification_logic = {
		which = conjure_dremora_lord_spell
	}
	add_ruler_modifier = {
		name = conjure_dremora_lord_spell
		duration = $effect_duration$
	}
}

dead_thrall_effect = {
	spell_notification_logic = {
		which = dead_thrall_spell
	}
	add_ruler_modifier = {
		name = dead_thrall_spell
		duration = $effect_duration$
	}
}

reanimate_corpses_effect = {
	add_yearly_manpower = 1.5
}

######################
# Destruction Spells #
######################

# Fire
flame_cloak_effect = {
	spell_notification_logic = {
		which = flame_cloak_spell
	}
	add_ruler_modifier = {
		name = flame_cloak_spell
		duration = $effect_duration$
	}
}

wall_of_flames_effect = {
	custom_tooltip = es_spell_wall_of_flames_effect_tt
	hidden_effect = {
		every_owned_province = {
			limit = {
				OR = {
					any_neighbor_province = {
						NOT = { owned_by = ROOT }
					}
					has_empty_adjacent_province = yes
				}
			}
			remove_province_modifier = wall_of_flames_spell
			add_province_modifier = {
				name = wall_of_flames_spell
				duration = $effect_duration$
			}
		}
	}
}

fire_storm_effect = {
	custom_tooltip = es_spell_fire_storm_effect_tt
	hidden_effect = {
		every_owned_province = {
			remove_province_modifier = fire_storm_spell
			add_province_modifier = {
				name = fire_storm_spell
				duration = $effect_duration$
			}
		}
	}
}

# Frost
frost_cloak_effect = {
	spell_notification_logic = {
		which = frost_cloak_spell
	}
	add_ruler_modifier = {
		name = frost_cloak_spell
		duration = $effect_duration$
	}
}

wall_of_frost_effect = {
	custom_tooltip = es_spell_wall_of_frost_effect_tt
	hidden_effect = {
		every_owned_province = {
			limit = {
				OR = {
					any_neighbor_province = {
						NOT = { owned_by = ROOT }
					}
					has_empty_adjacent_province = yes
				}
			}
			remove_province_modifier = wall_of_frost_spell
			add_province_modifier = {
				name = wall_of_frost_spell
				duration = $effect_duration$
			}
		}
	}
}

blizzard_effect = {
	custom_tooltip = es_spell_blizzard_effect_tt
	hidden_effect = {
		every_owned_province = {
			remove_province_modifier = blizzard_spell
			add_province_modifier = {
				name = blizzard_spell
				duration = $effect_duration$
			}
		}
	}
}

# Shock
lightning_cloak_effect = {
	spell_notification_logic = {
		which = lightning_cloak_spell
	}
	add_ruler_modifier = {
		name = lightning_cloak_spell
		duration = $effect_duration$
	}
}

wall_of_storms_effect = {
	custom_tooltip = es_spell_wall_of_storms_effect_tt
	hidden_effect = {
		every_owned_province = {
			limit = {
				OR = {
					any_neighbor_province = {
						NOT = { owned_by = ROOT }
					}
					has_empty_adjacent_province = yes
				}
			}
			remove_province_modifier = wall_of_storms_spell
			add_province_modifier = {
				name = wall_of_storms_spell
				duration = $effect_duration$
			}
		}
	}
}

lightning_storm_effect = {
	custom_tooltip = es_spell_lightning_storm_effect_tt
	hidden_effect = {
		every_owned_province = {
			remove_province_modifier = lightning_storm_spell
			add_province_modifier = {
				name = lightning_storm_spell
				duration = $effect_duration$
			}
		}
	}
}

###################
# Illusion Spells #
###################

calm_effect = {
	spell_notification_logic = {
		which = calm_spell
	}
	add_ruler_modifier = {
		name = calm_spell
		duration = $effect_duration$
	}
}

call_to_arms_effect = {
	spell_notification_logic = {
		which = call_to_arms_spell
	}
	add_ruler_modifier = {
		name = call_to_arms_spell
		duration = $effect_duration$
	}
}

clairvoyance_effect = {
	spell_notification_logic = {
		which = clairvoyance_spell
	}
	add_ruler_modifier = {
		name = clairvoyance_spell
		duration = $effect_duration$
	}
}

courage_effect = {
	spell_notification_logic = {
		which = courage_spell
	}
	add_ruler_modifier = {
		name = courage_spell
		duration = $effect_duration$
	}
}

harmony_effect = {
	spell_notification_logic = {
		which = harmony_spell
	}
	add_ruler_modifier = {
		name = harmony_spell
		duration = $effect_duration$
	}
}

hysteria_effect = {
	custom_tooltip = es_spell_hysteria_effect_tt
	every_country = {
		limit = {
			war_with = ROOT
			any_army = {
				location = {
					owned_by = ROOT
				}
			}
		}
		save_event_target_as = current_hostile_country
		random_owned_province = {
			save_event_target_as = current_rally_point
		}
		ROOT = {
			hysteria_spell_transport_units = {
				type = infantry
			}
			hysteria_spell_transport_units = {
				type = cavalry
			}
			hysteria_spell_transport_units = {
				type = artillery
			}
		}
	}
}

invisibility_effect = {
	spell_notification_logic = {
		which = invisibility_spell
	}
	add_ruler_modifier = {
		name = invisibility_spell
		duration = $effect_duration$
	}
}

mayhem_effect = {
	custom_tooltip = es_spell_mayhem_effect_tt
	hidden_effect = {
		every_country = {
			limit = {
				war_with = ROOT
				any_army = {
					location = {
						owned_by = ROOT
					}
				}
			}
			save_event_target_as = current_hostile_country
			ROOT = {
				every_owned_province = {
					limit = {
						num_of_units_in_province = {
							amount = 1
							who = event_target:current_hostile_country
						}
					}
					spawn_rebels = {
						type = noble_rebels
						size = 1
						culture = THIS
						religion = THIS
					}
					while = {
						limit = {
							num_of_units_in_province = {
								amount = 4
								who = REB
							}
						}
						kill_units = {
							who = REB
							amount = 1
						}
					}
				}
			}
		}
	}
}

muffle_effect = {
	add_ruler_modifier = {
		name = muffle_spell
		duration = $effect_duration$
	}
}

rally_effect = {
}

####################
# Mysticism Spells #
####################

dispel_effect = {
}

mark_and_recall_effect = {
}

telekinesis_effect = {
	add_ruler_modifier = {
		name = telekinesis_spell
		duration = $effect_duration$
	}
}

######################
# Restoration Spells #
######################

circle_of_protection_effect = {
}

close_wounds_effect = {
}

cure_disease_effect = {
}

guardian_circle_effect = {
	event_target:current_spell_target = {
		add_country_modifier = {
			name = fear_spell
			duration = $effect_duration$
		}
	}
	add_ruler_modifier = {
		name = heal_spell
		duration = $effect_duration$
	}
}

healing_effect = {
}

steadfast_ward_effect = {
}