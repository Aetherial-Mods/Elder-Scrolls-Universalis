es_change_country_names = {
	### High Rock Orcs
	if = { 
		limit = { tag = BB2 NOT = { culture_group = orsimer_cg } }
		override_country_name = UMOUR_NAME
	}
	if = { 
		limit = { tag = BB2 culture_group = orsimer_cg }
		restore_country_name = yes
	}
	if = { 
		limit = { tag = BA5 NOT = { culture_group = orsimer_cg } }
		override_country_name = LAGAUR_NAME
	}
	if = { 
		limit = { tag = BA5 culture_group = orsimer_cg }
		restore_country_name = yes
	}
	if = { 
		limit = { tag = BB1 NOT = { culture_group = orsimer_cg } }
		override_country_name = EVANARA_NAME
	}
	if = { 
		limit = { tag = BB1 culture_group = orsimer_cg }
		restore_country_name = yes
	}
	if = { 
		limit = { tag = BB0 NOT = { culture_group = orsimer_cg } }
		override_country_name = FARRUN_NAME
	}
	if = { 
		limit = { tag = BB0 culture_group = orsimer_cg }
		restore_country_name = yes
	}
	if = { 
		limit = { tag = BA7 NOT = { culture_group = orsimer_cg } }
		override_country_name = NORMARIA_NAME
	}
	if = { 
		limit = { tag = BA7 culture_group = orsimer_cg }
		restore_country_name = yes
	}
}