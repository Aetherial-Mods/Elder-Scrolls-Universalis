es_perform_events_on_three_year_pulse = {
	if = {
		limit = {
			has_reform = hereditary_rule_reform
			ruler_age = 35
			has_heir = no
		}
		country_event = { id = es_harem_events.1 days = 31 random = 69 }
	}
	if = {
		limit = {
			treasury = 10000
		}
		remove_country_modifier = es_pledge_of_simplicity_mod
	}
	if = {
		limit = {
			NOT = { crown_land_share = 40 }
		}
		remove_country_modifier = es_pledge_of_obedience_mod
	}
	if = {
		limit = {
			NOT = { manpower_percentage = 0.35 }
		}
		remove_country_modifier = es_pledge_of_vigilance_mod
	}
}

es_perform_events_on_five_year_pulse = {
	if = {
		limit = {
			tag = BLA
		}
		# FIX of abandoned native colonies with 1000 settlers
		every_province = {
			limit = { 
				is_empty = yes
				colonysize = 1
			}
			add_colonysize = -1000
		}
		# Lunar Lattice ###################################################
		if = {
			limit = {
				NOT = { has_country_modifier = moon_timer }
			}
			country_event = { id = moon_events.100 }
		}
		# Tribunal Elections ##############################################
		TRI = {
			if = {
				limit = {
					has_reform = tribunal_rule_reform
					religion = tribunal_pantheon
					NOT = { has_country_modifier = ruler_almalexia }
					NOT = { has_country_modifier = ruler_sotha_sil }
					NOT = { has_country_modifier = ruler_vivec }
				}
				country_event = { id = es_tribunal_events.1 days = 31 random = 1825 }
			}
		}
		#Trade and Prices ################################################
		if = {
			limit = {
				NOT = { has_country_modifier = trade_time }
			}
			country_event = { id = es_trade.1 days = 500 random = 1325 }
			add_country_modifier = {
				name = "trade_time"
				duration = 4059
			}
		}
		if = {
			limit = {
				NOT = { has_country_modifier = crisis_time }
			}
			random_list = {
				15 = {
					country_event = { id = es_trade.2 days = 505 random = 1320 }
					add_country_modifier = {
						name = "crisis_time"
						duration = 20075
					}
				}
				75 = {
					add_treasury = 125
				}
			}
		}
		if = {
			limit = {
				NOT = { has_country_modifier = rise_time }
			}
			random_list = {
				15 = {
					country_event = { id = es_trade.3 days = 510 random = 1315 }
					add_country_modifier = {
						name = "rise_time"
						duration = 20075
					}
				}
				75 = {
					add_treasury = 125
				}
			}
		}
		
		# Weather and Terrain Disasters ###################################
		if = {
			limit = {
				NOT = { has_country_modifier = sand_storm_1 }
			}
			country_event = { id = es_terrain.101 days = 515 random = 1310 }
			add_country_modifier = {
				name = sand_storm_1
				duration = 12775
			}
		}
		if = {
			limit = {
				NOT = { has_country_modifier = tropical_storm_1 }
			}
			country_event = { id = es_terrain.102 days = 520 random = 1305 }
			add_country_modifier = {
				name = tropical_storm_1
				duration = 12775
			}
		}
		if = {
			limit = {
				NOT = { has_country_modifier = floods_1 }
			}
			country_event = { id = es_terrain.103 days = 525 random = 1300 }
			add_country_modifier = {
				name = floods_1
				duration = 12775
			}
		}
		if = {
			limit = {
				NOT = { has_country_modifier = ice_age_1 }
			}
			country_event = { id = es_terrain.104 days = 530 random = 1295 }
			add_country_modifier = {
				name = ice_age_1
				duration = 12775
			}
		}
		if = {
			limit = {
				NOT = { has_country_modifier = red_mountain_eruption }
			}
			country_event = { id = es_terrain.105 days = 535 random = 1290 }
			add_country_modifier = {
				name = "red_mountain_eruption"
				duration = 21900
			}
		}
	}
	
	# Chimer Fetish Cults #############################################
	if = {
		limit = {
			religion = chimer_pantheon
			NOT = {
				has_country_flag = chimer_fetish_cults_1_flag
			}
			any_owned_province = {
				religion = molag_bal_cult
			}
		}
		country_event = { id = chimer_fetish_cults.1 }
	}
	if = {
		limit = {
			religion = chimer_pantheon
			NOT = {
				has_country_flag = chimer_fetish_cults_2_flag
			}
			any_owned_province = {
				religion = mehrunes_dagon_cult
			}
		}
		country_event = { id = chimer_fetish_cults.2 }
	}
	if = {
		limit = {
			religion = chimer_pantheon
			NOT = {
				has_country_flag = chimer_fetish_cults_3_flag
			}
			any_owned_province = {
				religion = malacath_cult
			}
		}
		country_event = { id = chimer_fetish_cults.3 }
	}
	if = {
		limit = {
			religion = chimer_pantheon
			NOT = {
				has_country_flag = chimer_fetish_cults_4_flag
			}
			any_owned_province = {
				religion = sheogorath_cult
			}
		}
		country_event = { id = chimer_fetish_cults.4 }
	}
	if = {
		limit = {
			religion = chimer_pantheon
			NOT = {
				has_country_flag = chimer_fetish_cults_5_flag
			}
			OR = {
				num_of_allies = 3
				any_owned_province = {
					religion = azura_cult
				}
			}
		}
		country_event = { id = chimer_fetish_cults.5 }
	}
	if = {
		limit = {
			religion = chimer_pantheon
			NOT = {
				has_country_flag = chimer_fetish_cults_6_flag
			}
			OR = {
				is_at_war = yes
				any_owned_province = {
					religion = boethiah_cult
				}
			}
		}
		country_event = { id = chimer_fetish_cults.6 }
	}
	if = {
		limit = {
			religion = chimer_pantheon
			NOT = {
				has_country_flag = chimer_fetish_cults_7_flag
			}
			OR = {
				any_known_country = {
					has_spy_network_in = {
						who = ROOT
						value = 30
					}
				}
				any_owned_province = {
					religion = mephala_cult
				}
			}
		}
		country_event = { id = chimer_fetish_cults.7 }
	}
	
	# Spawn of Pirate Nations #########################################
	if = {
		limit = {
			OR = {
				owns = 1171
				owns = 5351
				owns = 5352
			}
			NOT = { exists = ABE }
			OR = {
				1171 = {
				unrest = 10
			}
			5351 = {
				unrest = 10
			}
			5352 = {
				unrest = 10
			}
			}
			NOT = { stability = 3 }
			OR = {
				NOT = { ABE = { is_core = 1171 } }
				NOT = { ABE = { is_core = 5351 } }
				NOT = { ABE = { is_core = 5352 } }
			}
		}
		country_event = { id = es_pirates.1 days = 540 random = 1285 }
	}
	if = {
		limit = {
			OR = {
				owns = 1501
				owns = 1502
				owns = 1503
				owns = 1504
				owns = 1505
				owns = 6116
			}
			NOT = { exists = RES }
			OR = {
				1501 = {
					unrest = 10
				}
				1502 = {
					unrest = 10
				}
				1503 = {
					unrest = 10
				}
				1504 = {
					unrest = 10
				}
				1505 = {
					unrest = 10
				}
				6116 = {
					unrest = 10
				}
			}
			NOT = { stability = 3 }
			OR = {
				NOT = { RES = { is_core = 1501 } }
				NOT = { RES = { is_core = 1502 } }
				NOT = { RES = { is_core = 1503 } }
				NOT = { RES = { is_core = 1504 } }
				NOT = { RES = { is_core = 1505 } }
				NOT = { RES = { is_core = 6116 } }
			}
		}
		country_event = { id = es_pirates.2 days = 545 random = 1280 }
	}
	if = {
		limit = {
			OR = {
				owns = 5441
				owns = 5442
				owns = 5443
				owns = 5444
				owns = 5445
			}
			NOT = { exists = TPL }
			OR = {
				5441 = {
					unrest = 10
				}
				5442 = {
					unrest = 10
				}
				5443 = {
					unrest = 10
				}
				5444 = {
					unrest = 10
				}
				5445 = {
					unrest = 10
				}
			}
			NOT = { stability = 3 }
			OR = {
				NOT = { TPL = { is_core = 5441 } }
				NOT = { TPL = { is_core = 5442 } }
				NOT = { TPL = { is_core = 5443 } }
				NOT = { TPL = { is_core = 5444 } }
				NOT = { TPL = { is_core = 5445 } }
			}
		}
		country_event = { id = es_pirates.3 days = 550 random = 1275 }
	}
	if = {
		limit = {
			OR = {
				owns = 1303
				owns = 1309
				owns = 7115
			}
			NOT = { exists = STA }
			OR = {
				1303 = {
					unrest = 10
				}
				1309 = {
					unrest = 10
				}
				7115 = {
					unrest = 10
				}
			}
			NOT = { stability = 3 }
			OR = {
				NOT = { STA = { is_core = 1303 } }
				NOT = { STA = { is_core = 1309 } }
				NOT = { STA = { is_core = 7115 } }
			}
		}
		country_event = { id = es_pirates.4 days = 555 random = 1270 }
	}
	if = {
		limit = {
			owns = 1035
			NOT = { exists = SED }
			1035 = {
				unrest = 10
			}
			NOT = { stability = 3 }
			NOT = { SED = { is_core = 1035 } }
		}
		country_event = { id = es_pirates.5 days = 560 random = 1265 }
	}
	if = {
		limit = {
			OR = {
				owns = 6485
				owns = 6483
				owns = 6484
			}
			NOT = { exists = KME }
			OR = {
				6485 = { unrest = 10 }
				6483 = { unrest = 10 }
				6484 = { unrest = 10 }
			}
			NOT = { stability = 3 }
			OR = {
				NOT = { KME = { is_core = 6485 } }
				NOT = { KME = { is_core = 6483 } }
				NOT = { KME = { is_core = 6484 } }
			}
		}
		country_event = { id = es_pirates.6 days = 565 random = 1260 }
	}
	if = {
		limit = {
			owns = 1629
			NOT = { exists = ELZ }
			1629 = {
			unrest = 10
			}
			NOT = { stability = 3 }
			NOT = { ELZ = { is_core = 1629 } }
		}
		country_event = { id = es_pirates.7 days = 570 random = 1255 }
	}
	if = {
		limit = {
			OR = {
				owns = 1798
				owns = 1799
				owns = 1800
			}
			NOT = { exists = KHE }
			OR = {
				1798 = { unrest = 10 }
				1799 = { unrest = 10 }
				1800 = { unrest = 10 }
			}
			NOT = { stability = 3 }
			OR = {
				NOT = { KHE = { is_core = 1798 } }
				NOT = { KHE = { is_core = 1799 } }
				NOT = { KHE = { is_core = 1800 } }
			}
		}
		country_event = { id = es_pirates.8 days = 575 random = 1250 }
	}
	if = {
		limit = {
			owns = 3495
			NOT = { exists = KAA }
			3495 = {
				unrest = 10
			}
			NOT = { stability = 3 }
			NOT = { KAA = { is_core = 3495 } }
		}
		country_event = { id = es_pirates.9 days = 580 random = 1245 }
	}
	if = {
		limit = {
			OR = {
				owns = 1670
				owns = 1671
			}
			NOT = { exists = RAL }
			OR = {
				1670 = { unrest = 10 }
				1671 = { unrest = 10 }
			}
			NOT = { stability = 3 }
			OR = {
				NOT = { RAL = { is_core = 1670 } }
				NOT = { RAL = { is_core = 1671 } }
			}
		}
		country_event = { id = es_pirates.10 days = 585 random = 1240 }
	}
	# Endgame #########################################################
	if = {
		limit = {
			ai = no
			OR = {
				AND = {
					NOT = { has_dlc = "Emperor" }
					NOT = { has_dlc = "Rights of Man" }
					total_development = 2500
				}
				NOT = { great_power_rank = 2 }
			}
			NOT = { has_country_flag = first_endgame_is_skipped_flag }
			NOT = { has_country_modifier = es_endgame_timer }
		}
		add_country_modifier = {
			name = "es_endgame_timer"
			duration = 36500
			hidden = yes
		}
		set_country_flag = first_endgame_is_skipped_flag
	}
	if = {
		limit = {
			ai = no
			OR = {
				AND = {
					NOT = { has_dlc = "Emperor" }
					NOT = { has_dlc = "Rights of Man" }
					total_development = 2500
				}
				NOT = { great_power_rank = 2 }
			}
			has_country_flag = first_endgame_is_skipped_flag
			NOT = { has_country_modifier = es_endgame_timer }
		}
		add_country_modifier = {
			name = "es_endgame_timer"
			duration = 36500
			hidden = yes
		}
		country_event = { id = es_endgame.1 }
	}
	# Religious Schoold Events ####################################################
	if = {
		limit = {
			NOT = { has_country_modifier = es_religion_school_modifier_timer }
			has_religious_school = yes
		}
		random_list = {
			50 = {
				country_event = { id = es_school_events.1 days = 600 random = 1225 }
			}
			35 = {
				country_event = { id = es_school_events.2 days = 605 random = 1220 }
			}
			15 = {
				country_event = { id = es_school_events.3 days = 610 random = 1215 }
			}
		}
		add_country_modifier = { name = "es_religion_school_modifier_timer" duration = 3650 hidden = yes }
	}
	# Reclamations Spawn ##########################################################
	if = {
		limit = {
			NOT = { has_country_flag = reclamations_spawn_flag }
			NOT = { is_religion_enabled = reclamations_pantheon }
			1002 = { owned_by = ROOT }
			reform_desire = 1
		}
		country_event = { id = es_reclamations.1 days = 615 random = 1210 }
		set_country_flag = reclamations_spawn_flag
	}
	# Dark Brotherhood Later Spawn ################################################
	if = {
		limit = {
			NOT = { has_country_flag = initialtion_of_dark_brotherhood_flag }
			is_year = 862
			NOT = { has_estate = estate_morag_tong }
			NOT = { has_estate = estate_dark_brotherhood }
			capital_scope = { continent = Tamriel }
			NOT = { has_country_flag = ban_of_dark_brotherhood_flag }
			NOT = { has_country_flag = invitation_to_dark_brotherhood }
		}
		country_event = { id = dark_brotherhood_spawn.5 days = 620 random = 1205 }
		set_country_flag = initialtion_of_dark_brotherhood_flag
	}
	# Later Spawn of Oblivion Nations #############################################
	if = {
		limit = {
			is_year = 1200
			has_global_flag = es_disabled_coldharbour_flag
		}
		es_spawn_countries_coldharbour_superregion = yes
		clr_global_flag = es_disabled_coldharbour_flag
	}
	if = {
		limit = {
			is_year = 1200
			has_global_flag = es_disabled_deadlands_flag
		}
		es_spawn_countries_deadlands_superregion = yes
		clr_global_flag = es_disabled_deadlands_flag
	}
	# Elven Colonies in Valenwood #################################################
	if = {
		limit = {
			culture_group = high_elves_cg
			any_subject_country = {
				NOT = { is_subject_of_type = vassal }
				NOT = { is_subject_of_type = march }
				NOT = { is_subject_of_type = holy_order_1 }
				culture_group = high_elves_cg
				NOT = { has_country_flag = es_subjects_3_flag }
				capital_scope = { superregion = valenwood_superregion }
			}
		}
		country_event = { id = es_subjects.3 days = 630 random = 1195 }
	}
	# Fall of Shiung
	if = {
		limit = {
			owns_core_province = 646
			NOT = { culture_group = po_tun_cg }
			NOT = { has_country_flag = akaviri_empire_events_1_flag }
		}
		set_country_flag = akaviri_empire_events_1_flag
		country_event = { id = akaviri_empire_events.1 days = 635 random = 1190 }
	}
	# Narfinsel Schism Province Set Up
	if = {
		limit = {
			NOT = { is_year = 178 }
			primary_culture = ayleid
			OR = {
				religion_group = polytheistic_group
				religion_group = daedric_group
			}
			any_owned_province = { NOT = { has_province_modifier = es_religious_schism } }
		}
		every_owned_province = {
			limit = {
				NOT = { has_province_modifier = es_religious_schism }
			}
			add_permanent_province_modifier = {
				name = "es_religious_schism"
				duration = 54750
			}
		}
	}
	# Narfinsel Schism Province Cleanup
	if = {
		limit = {
			any_owned_province = { has_province_modifier = es_religious_schism }
			OR = {
				NOT = { primary_culture = ayleid }
				NOT = { religion_group = polytheistic_group }
				NOT = { religion_group = daedric_group }
				has_global_flag = es_5_scenario_3_global_glag
				is_year = 178
			}
		}
		every_owned_province = {
			limit = { has_province_modifier = es_religious_schism }
			remove_province_modifier = es_religious_schism
		}
	}
}