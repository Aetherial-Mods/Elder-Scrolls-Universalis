es_do_things_on_new_monarch = {
	if = {
		limit = { is_playing_custom_nation = no }
		es_change_country_colour = yes
	}
	
	set_variable = {
		which = enforced_peace_num
		value = 0
	}
	set_variable = {
		which = developed_provinces_num
		value = 0
	}
	set_variable = {
		which = num_of_battles_won_ruler
		value = 0
	}
	set_variable = {
		which = num_of_wars_won_ruler
		value = 0
	}
	set_variable = {
		which = infiltrated_administration_num
		value = 0
	}
	set_variable = {
		which = sowed_discontend_num
		value = 0
	}
	set_variable = {
		which = passed_technology_num
		value = 0
	}
	
	if = {
		limit = {
			primary_culture = reachmen
		}
		set_variable = { which = reachmen_authority value = 0 }
	}

	if = {
		limit = {
			OR = {
				has_reform = monastic_democracy_reform
				has_reform = magical_state
				has_reform = reason_and_logic
				has_reform = bureaucratic_tyranny
				has_reform = court_of_darkness
				has_reform = divine_right
				has_reform = subservient_bureaucracy
				has_reform = justified_atrocity
			}
		}
		country_event = { id = elections.722 }
	}

	if = {
		limit = {
			OR = {
				government = republic
				is_emperor = yes 
			}
		}
		add_ruler_modifier = {
			name = "is_republic_or_emperor_ruler_modifier"
			duration = -1
		}
	}
	if = {
		limit = {
			is_half_blood_ruler_trigger = yes
		}
		add_ruler_modifier = {
			name = "is_half_blood_ruler_modifier"
			duration = -1
		}
	}
	if = {
		limit = {
			is_elven_ruler_trigger = yes
		}
		add_ruler_modifier = {
			name = "is_elven_ruler_modifier"
			duration = -1
		}
	}
	if = {
		limit = {
			is_unknown_ruler_trigger = yes
		}
		add_ruler_modifier = {
			name = "is_unknown_ruler_modifier"
			duration = -1
		}
	}
	if = {
		limit = {
			is_immortal_ruler_trigger = yes
		}
		add_ruler_modifier = {
			name = "is_immortal_ruler_modifier"
			duration = -1
		}
	}
	
	if = {
		limit = {
			is_great_power = yes
			is_free_or_tributary_trigger = yes
			has_institution = literacy
			capital_scope = { continent = Tamriel }
			has_dlc = "Emperor"
		}
		country_event = { id = center_of_revolution.1000 days = 371 random = 36500 }
	}
	
	if = {
		limit = {
			has_reform = chosen_by_the_gods
		}
		country_event = { id = event_new_diplomatic_actions.300 days = 372 random = 1825 }
	}
	
	if = {
		limit = {
			OR = {
				has_country_modifier = 23_the_crowns
				has_country_modifier = 23_the_forebears
			}
		}
		country_event = { id = es_general.1 }
	}
	
	if = { limit = { has_reform = co-consul } country_event = { id = event_new_diplomatic_actions.700 } }
	
	if = { limit = { has_reform = martial_ruler } 
	random_list = { 
		2 = { define_ruler_to_general = { fire = 3 shock = 3 manuever = 3 siege = 3 } }
		2 = { define_ruler_to_general = { fire = 4 shock = 3 manuever = 3 siege = 3 } }
		2 = { define_ruler_to_general = { fire = 3 shock = 4 manuever = 3 siege = 3 } }
		2 = { define_ruler_to_general = { fire = 3 shock = 3 manuever = 4 siege = 3 } }
		2 = { define_ruler_to_general = { fire = 3 shock = 3 manuever = 3 siege = 4 } }
		2 = { define_ruler_to_general = { fire = 4 shock = 4 manuever = 3 siege = 3 } }
		2 = { define_ruler_to_general = { fire = 4 shock = 3 manuever = 4 siege = 3 } }
		2 = { define_ruler_to_general = { fire = 4 shock = 3 manuever = 3 siege = 4 } }
		2 = { define_ruler_to_general = { fire = 3 shock = 4 manuever = 4 siege = 3 } }
		2 = { define_ruler_to_general = { fire = 3 shock = 4 manuever = 3 siege = 4 } }
		2 = { define_ruler_to_general = { fire = 3 shock = 3 manuever = 4 siege = 4 } }
		2 = { define_ruler_to_general = { fire = 3 shock = 4 manuever = 4 siege = 4 } }
		2 = { define_ruler_to_general = { fire = 4 shock = 3 manuever = 4 siege = 4 } }
		2 = { define_ruler_to_general = { fire = 4 shock = 4 manuever = 3 siege = 4 } }
		2 = { define_ruler_to_general = { fire = 4 shock = 4 manuever = 4 siege = 3 } }
		2 = { define_ruler_to_general = { fire = 4 shock = 4 manuever = 4 siege = 4 } }
		2 = { define_ruler_to_general = { fire = 5 shock = 3 manuever = 3 siege = 3 } }
		2 = { define_ruler_to_general = { fire = 3 shock = 5 manuever = 3 siege = 3 } }
		2 = { define_ruler_to_general = { fire = 3 shock = 3 manuever = 5 siege = 3 } }
		2 = { define_ruler_to_general = { fire = 3 shock = 3 manuever = 3 siege = 5 } }
		2 = { define_ruler_to_general = { fire = 5 shock = 5 manuever = 3 siege = 3 } }
		2 = { define_ruler_to_general = { fire = 5 shock = 3 manuever = 5 siege = 3 } }
		2 = { define_ruler_to_general = { fire = 5 shock = 3 manuever = 3 siege = 5 } }
		2 = { define_ruler_to_general = { fire = 3 shock = 5 manuever = 5 siege = 3 } }
		2 = { define_ruler_to_general = { fire = 3 shock = 5 manuever = 3 siege = 5 } }
		2 = { define_ruler_to_general = { fire = 3 shock = 3 manuever = 5 siege = 5 } }
		2 = { define_ruler_to_general = { fire = 3 shock = 5 manuever = 5 siege = 5 } }
		2 = { define_ruler_to_general = { fire = 5 shock = 3 manuever = 5 siege = 5 } }
		2 = { define_ruler_to_general = { fire = 5 shock = 5 manuever = 3 siege = 5 } }
		2 = { define_ruler_to_general = { fire = 5 shock = 5 manuever = 5 siege = 3 } }
		2 = { define_ruler_to_general = { fire = 5 shock = 5 manuever = 5 siege = 5 } }
		2 = { define_ruler_to_general = { fire = 5 shock = 4 manuever = 4 siege = 4 } }
		2 = { define_ruler_to_general = { fire = 4 shock = 5 manuever = 4 siege = 4 } }
		2 = { define_ruler_to_general = { fire = 4 shock = 4 manuever = 5 siege = 4 } }
		2 = { define_ruler_to_general = { fire = 4 shock = 4 manuever = 4 siege = 5 } }
		2 = { define_ruler_to_general = { fire = 5 shock = 5 manuever = 4 siege = 4 } }
		2 = { define_ruler_to_general = { fire = 5 shock = 4 manuever = 5 siege = 4 } }
		2 = { define_ruler_to_general = { fire = 5 shock = 4 manuever = 4 siege = 5 } }
		2 = { define_ruler_to_general = { fire = 4 shock = 5 manuever = 5 siege = 4 } }
		2 = { define_ruler_to_general = { fire = 4 shock = 5 manuever = 4 siege = 5 } }
		2 = { define_ruler_to_general = { fire = 4 shock = 4 manuever = 5 siege = 5 } }
		2 = { define_ruler_to_general = { fire = 4 shock = 5 manuever = 5 siege = 5 } }
		2 = { define_ruler_to_general = { fire = 5 shock = 4 manuever = 5 siege = 5 } }
		2 = { define_ruler_to_general = { fire = 5 shock = 5 manuever = 4 siege = 5 } }
		2 = { define_ruler_to_general = { fire = 5 shock = 5 manuever = 5 siege = 4 } }
	}
	}
	
	if = { limit = { religion_group = polytheistic_group } add_country_modifier = { name = "polytheistic_group_modifier" duration = -1 }
		remove_country_modifier = occult_group_modifier
		remove_country_modifier = elemental_group_modifier
		remove_country_modifier = daedric_group_modifier
		remove_country_modifier = harmony_group_modifier
	}
	if = { limit = { religion_group = occult_group } add_country_modifier = { name = "occult_group_modifier" duration = -1 }
		remove_country_modifier = polytheistic_group_modifier
		remove_country_modifier = elemental_group_modifier
		remove_country_modifier = daedric_group_modifier
		remove_country_modifier = harmony_group_modifier
	}
	if = { limit = { religion_group = elemental_group } add_country_modifier = { name = "elemental_group_modifier" duration = -1 }
		remove_country_modifier = polytheistic_group_modifier
		remove_country_modifier = occult_group_modifier
		remove_country_modifier = daedric_group_modifier
		remove_country_modifier = harmony_group_modifier
	}
	if = { limit = { religion_group = daedric_group } add_country_modifier = { name = "daedric_group_modifier" duration = -1 }
		remove_country_modifier = polytheistic_group_modifier
		remove_country_modifier = occult_group_modifier
		remove_country_modifier = elemental_group_modifier
		remove_country_modifier = harmony_group_modifier
	}
	if = { limit = { religion_group = harmony_group } add_country_modifier = { name = "harmony_group_modifier" duration = -1 }
		remove_country_modifier = polytheistic_group_modifier
		remove_country_modifier = occult_group_modifier
		remove_country_modifier = elemental_group_modifier
		remove_country_modifier = daedric_group_modifier
	}
	
	# Shalidor's Maze Event
	if = {
		limit = {
			government = theocracy
			religion = students_of_magnus
			culture_group = northern_cg
			has_global_flag = labyrinthan_founded
			NOT = { has_ruler = "Shalidor" }
		}
		country_event = { id = es_theocracy.2 days = 31 random = 31 }
	}
	
	# Easter Egg Event
	if = {
		limit = {
			government = monarchy
			NOT = { has_reform = cyrodiilic_empire_reform }
			NOT = { has_country_flag = es_government_1_flag }
		}
		country_event = { id = es_government.1 days = 373 random = 150000 }
		set_country_flag = es_government_1_flag
	}
	
	# Lore Heirs
	if = {
		limit = {
			has_government_attribute = heir 
			has_heir = no
		}
		# high_elves_cg
		if = {
			limit = {
				culture_group = high_elves_cg
			}
			if = {
				limit = {
					1382 = { is_capital_of = ROOT }
					NOT = { dynasty = "Direnni" }
				}
				define_heir = { dynasty = "Direnii" ADM = 4 DIP = 4 MIL = 4	claim = 80 }
			}
			else_if = {
				limit = {
					4823 = { is_capital_of = ROOT }
					NOT = { dynasty = "Errinorne" }
				}
				define_heir = { dynasty = "Errinorne" ADM = 4 DIP = 4 MIL = 4	claim = 80 }
			}
		}
		# bosmer_cg
		else_if = {
			limit = {
				culture_group = bosmer_cg
			}
			if = {
				limit = {
					820 = { is_capital_of = ROOT }
					NOT = { dynasty = "Camoran" }
				}
				define_heir = { dynasty = "Camoran" ADM = 4 DIP = 4 MIL = 4	claim = 80 }
			}
		}
		else_if = {
			limit = {
				culture_group = orsimer_cg
			}
			if = {
				limit = {
					OR = {
						tag = ORS
						tag = SOR
						tag = IRO
					}
					NOT = { dynasty = "gro-Orsinium" }
				}
				define_heir = { dynasty = "gro-Orsinium" ADM = 4 DIP = 4 MIL = 4	claim = 80 }
			}
			else_if = {
				limit = {
					6892 = { is_capital_of = ROOT }
					NOT = { dynasty = "gro-Morkul" }
				}
				define_heir = { dynasty = "gro-Morkul" ADM = 4 DIP = 4 MIL = 4	claim = 80 }
			}
			else_if = {
				limit = {
					7068 = { is_capital_of = ROOT }
					NOT = { dynasty = "gro-Shatul" }
				}
				define_heir = { dynasty = "gro-Shatul" ADM = 4 DIP = 4 MIL = 4	claim = 80 }
			}
			else_if = {
				limit = {
					7073 = { is_capital_of = ROOT }
					NOT = { dynasty = "gro-Tumnosh" }
				}
				define_heir = { dynasty = "gro-Tumnosh" ADM = 4 DIP = 4 MIL = 4	claim = 80 }
			}
			else_if = {
				limit = {
					1408 = { is_capital_of = ROOT }
					NOT = { dynasty = "gro-Bagrakh" }
				}
				define_heir = { dynasty = "gro-Bagrakh" ADM = 4 DIP = 4 MIL = 4	claim = 80 }
			}
			else_if = {
				limit = {
					OR = {
						1414 = { is_capital_of = ROOT }
						1413 = { is_capital_of = ROOT }
					}
					NOT = { dynasty = "gro-Fharun" }
				}
				define_heir = { dynasty = "gro-Fharun" ADM = 4 DIP = 4 MIL = 4	claim = 80 }
			}
			else_if = {
				limit = {
					OR = {
						7013 = { is_capital_of = ROOT }
						6914 = { is_capital_of = ROOT }
					}
					NOT = { dynasty = "gro-Khazun" }
				}
				define_heir = { dynasty = "gro-Khazun" ADM = 4 DIP = 4 MIL = 4	claim = 80 }
			}
			else_if = {
				limit = {
					1391 = { is_capital_of = ROOT }
					NOT = { dynasty = "gro-Murtag" }
				}
				define_heir = { dynasty = "gro-Murtag" ADM = 4 DIP = 4 MIL = 4	claim = 80 }
			}
			else_if = {
				limit = {
					1391 = { is_capital_of = ROOT }
					NOT = { dynasty = "gro-Murtag" }
				}
				define_heir = { dynasty = "gro-Murtag" ADM = 4 DIP = 4 MIL = 4	claim = 80 }
			}
			else_if = {
				limit = {
					6959 = { is_capital_of = ROOT }
					NOT = { dynasty = "gro-Igron" }
				}
				define_heir = { dynasty = "gro-Igron" ADM = 4 DIP = 4 MIL = 4	claim = 80 }
			}
		}
		# northern_cg
		else_if = {
			limit = {
				culture_group = northern_cg
			}
			if = {
				limit = {
					OR = {
						1275 = { is_capital_of = ROOT }
						1299 = { is_capital_of = ROOT }
					}
					NOT = { dynasty = "Ysgramor" }
				}
				if = {
					limit = {
						is_year = 241
						1275 = { is_capital_of = ROOT }
					}
					define_heir = { dynasty = "Windhold" ADM = 4 DIP = 4 MIL = 4 claim = 80 }
				}
				else_if = {
					limit = {
						is_year = 241
						1299 = { is_capital_of = ROOT }
					}
					define_heir = { dynasty = "Winterhold" ADM = 4 DIP = 4 MIL = 4 claim = 80 }
				}
				else = {
					define_heir = { dynasty = "Ysgramor" ADM = 4 DIP = 4 MIL = 4 claim = 80 }
				}
			}
			else_if = {
				limit = {
					OR = {
						1319 = { is_capital_of = ROOT }
						3015 = { is_capital_of = ROOT }
					}
					NOT = { dynasty = "Whitehold" }
				}
				define_heir = { dynasty = "Whitehold" ADM = 4 DIP = 4 MIL = 4 claim = 80 }
			}
		}
		# cyrodiil_cg
		else_if = {
			limit = {
				culture_group = cyrodiil_cg
			}
			if = {
				limit = {
					OR = {
						1206 = { is_capital_of = ROOT }
						1133 = { is_capital_of = ROOT }
						5777 = { is_capital_of = ROOT }
					}
					NOT = { dynasty = "Nedic" }
					NOT = { dynasty = "Alessian" }
					NOT = { dynasty = "Cyrodiil" }
				}
				if = {
					limit = {
						religion = marukhism
					}
					define_heir = { dynasty = "Alessian" ADM = 4 DIP = 4 MIL = 4 claim = 80 }
				}
				else_if = {
					limit = {
						primary_culture = nedic
					}
					define_heir = { dynasty = "Nedic" ADM = 4 DIP = 4 MIL = 4 claim = 80 }
				}
				else = {
					define_heir = { dynasty = "Cyrodiil" ADM = 4 DIP = 4 MIL = 4 claim = 80 }
				}
			}
			else_if = {
				limit = {
					1150 = { is_capital_of = ROOT }
					NOT = { dynasty = "Larich" }
				}
				define_heir = { dynasty = "Larich" ADM = 4 DIP = 4 MIL = 4 claim = 80 }
			}
			else_if = {
				limit = {
					5947 = { is_capital_of = ROOT }
					NOT = { dynasty = "Tharn" }
				}
				define_heir = { dynasty = "Tharn" ADM = 4 DIP = 4 MIL = 4 claim = 80 }
			}
		}
		# half_blood_cg
		else_if = {
			limit = {
				culture_group = half_blood_cg
			}
			if = {
				limit = {
					7019 = { is_capital_of = ROOT }
					NOT = { dynasty = "Spenard" }
				}
				define_heir = { dynasty = "Spenard" ADM = 4 DIP = 4 MIL = 4 claim = 80 }
			}
			else_if = {
				limit = {
					4933 = { is_capital_of = ROOT }
					NOT = { dynasty = "Plessington" }
				}
				define_heir = { dynasty = "Plessington" ADM = 4 DIP = 4 MIL = 4 claim = 80 }
			}
			else_if = {
				limit = {
					6268 = { is_capital_of = ROOT }
					NOT = { dynasty = "Guimard" }
				}
				define_heir = { dynasty = "Guimard" ADM = 4 DIP = 4 MIL = 4 claim = 80 }
			}
			else_if = {
				limit = {
					7023 = { is_capital_of = ROOT }
					NOT = { dynasty = "Aurmine" }
				}
				define_heir = { dynasty = "Aurmine" ADM = 4 DIP = 4 MIL = 4 claim = 80 }
			}
			else_if = {
				limit = {
					1493 = { is_capital_of = ROOT }
					NOT = { dynasty = "Lainlyn" }
				}
				define_heir = { dynasty = "Lainlyn" ADM = 4 DIP = 4 MIL = 4 claim = 80 }
			}
			else_if = {
				limit = {
					6989 = { is_capital_of = ROOT }
					NOT = { dynasty = "Cumberland" }
				}
				define_heir = { dynasty = "Cumberland" ADM = 4 DIP = 4 MIL = 4 claim = 80 }
			}
			else_if = {
				limit = {
					1369 = { is_capital_of = ROOT }
					NOT = { dynasty = "Daggerfall" }
				}
				define_heir = { dynasty = "Daggerfall" ADM = 4 DIP = 4 MIL = 4 claim = 80 }
			}
		}
		# dead_cg
		else_if = {
			limit = {
				culture_group = dead_cg
			}
			if = {
				limit = {
					7016 = { is_capital_of = ROOT }
					NOT = { dynasty = "Ravenwatch" }
				}
				define_heir = { dynasty = "Ravenwatch" ADM = 4 DIP = 4 MIL = 4 claim = 80 }
			}
		}
		# velothi_cg
		else_if = {
			limit = {
				culture_group = velothi_cg
			}
			if = {
				limit = {
					OR = {
						3904 = { is_capital_of = ROOT }
						995 = { is_capital_of = ROOT }
					}
					NOT = { dynasty = "Dres" }
				}
				define_heir = { dynasty = "Dres" ADM = 4 DIP = 4 MIL = 4 claim = 80 }
			}
			else_if = {
				limit = {
					990 = { is_capital_of = ROOT }
					NOT = { dynasty = "Hlaalu" }
					NOT = { dynasty = "Sadras" }
				}
				if = {
					limit = {
						religion = reclamations_pantheon
					}
					define_heir = { dynasty = "Sadras" ADM = 4 DIP = 4 MIL = 4 claim = 80 }
				}
				else = {
					define_heir = { dynasty = "Hlaalu" ADM = 4 DIP = 4 MIL = 4 claim = 80 }
				}
			}
			else_if = {
				limit = {
					1017 = { is_capital_of = ROOT }
					NOT = { dynasty = "Indoril" }
				}
				define_heir = { dynasty = "Indoril" ADM = 4 DIP = 4 MIL = 4 claim = 80 }
			}
			else_if = {
				limit = {
					1028 = { is_capital_of = ROOT }
					NOT = { dynasty = "Telvanni" }
				}
				define_heir = { dynasty = "Telvanni" ADM = 4 DIP = 4 MIL = 4 claim = 80 }
			}
			else_if = {
				limit = {
					OR = {
						957 = { is_capital_of = ROOT }
						4263 = { is_capital_of = ROOT }
					}
					NOT = { dynasty = "Redoran" }
				}
				define_heir = { dynasty = "Redoran" ADM = 4 DIP = 4 MIL = 4 claim = 80 }
			}
			else_if = {
				limit = {
					4121 = { is_capital_of = ROOT }
					NOT = { dynasty = "Mora" }
				}
				define_heir = { dynasty = "Mora" ADM = 4 DIP = 4 MIL = 4 claim = 80 }
			}
			else_if = {
				limit = {
					OR = {
						4305 = { is_capital_of = ROOT }
						4279 = { is_capital_of = ROOT }
					}
					NOT = { dynasty = "Dagoth" }
				}
				define_heir = { dynasty = "Dagoth" ADM = 4 DIP = 4 MIL = 4 claim = 80 }
			}
			else_if = {
				limit = {
					4275 = { is_capital_of = ROOT }
					NOT = { dynasty = "Salothan" }
				}
				define_heir = { dynasty = "Salothan" ADM = 4 DIP = 4 MIL = 4 claim = 80 }
			}
			else_if = {
				limit = {
					1043 = { is_capital_of = ROOT }
					NOT = { dynasty = "Sotha" }
				}
				define_heir = { dynasty = "Sotha" ADM = 4 DIP = 4 MIL = 4 claim = 80 }
			}
			else_if = {
				limit = {
					984 = { is_capital_of = ROOT }
					NOT = { dynasty = "Ra'athim" }
				}
				define_heir = { dynasty = "Ra'athim" ADM = 4 DIP = 4 MIL = 4 claim = 80 }
			}
		}
	}
	# yokudo_redguard_cg
	else_if = {
		limit = {
			culture_group = yokudo_redguard_cg
		}
		if = {
			limit = {
				1493 = { is_capital_of = ROOT }
				NOT = { dynasty = "Lainlyn" }
			}
			define_heir = { dynasty = "Lainlyn" ADM = 4 DIP = 4 MIL = 4 claim = 80 }
		}
		else_if = {
			limit = {
				335 = { is_capital_of = ROOT }
				NOT = { dynasty = "Sesnit" }
				NOT = { dynasty = "Torn" }
			}
			if = {
				limit = {
					is_year = 453
				}
				define_heir = { dynasty = "Torn" ADM = 4 DIP = 4 MIL = 4 claim = 80 }
			}
			else = {
				define_heir = { dynasty = "Sesnit" ADM = 4 DIP = 4 MIL = 4 claim = 80 }
			}
		}
	}
}

es_heirs_immortal_rulers = {
	if = {
		limit = {
			has_country_flag = molag_bal_country_flag
		}
		define_heir = {
			name = "Molag" dynasty = "Bal" culture = daedra adm = 3 dip = 7 mil = 5 age = 21 
		}
		add_heir_personality = immortal_personality
	}
	if = {
		limit = {
			has_country_flag = mehrunes_dagon_country_flag
		}
		define_heir = {
			name = "Mehrunes" dynasty = "Dagon" culture = daedra adm = 5 dip = 3 mil = 7 age = 21 
		}
		add_heir_personality = immortal_personality
	}
	if = {
		limit = {
			has_country_flag = orgnum_country_flag
		}
		define_heir = {
			name = "Orgnum" dynasty = "Maormeri" culture = maormer adm = 7 dip = 5 mil = 3 age = 21 
		}
		add_heir_personality = immortal_personality
	}
}