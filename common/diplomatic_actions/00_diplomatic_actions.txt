# INSTRUCTIONS:
#
# condition				A diplomatic action can have any number of condition blocks, each with its own
#						tooltip, potential and allow section
#
# 	tooltip					Sets a custom text string similar to the hardcoded limits
# 							If no tooltip is scripted, the tooltip for the actual trigger will be shown
#							Note that the custom tooltip is only shown if the allow trigger is NOT met
#
# 	potential				Determines if the trigger is applicable or not
# 	allow					Determines if the action is valid or not
#
# effect				A diplomatic action can only have one effect block

# ROOT					actor
# FROM					target

########################################
# DIPLOMATIC ACTIONS
########################################
# royal_marriage
# declarewar
# requestpeace
# support_independence_action
# allianceaction
# embargoaction
# annexationaction
# integrationaction
# vassalaction
# guaranteeaction
# warningaction
# threaten_war
# milaccess
# fleet_access
# offer_fleet_access
# offermilaccess
# insultaction
# giftaction
# claimaction
# callaction
# offerloan
# warsubsidy
# sellprov
# imperial_relations_action
# imperial_realm_war_action
# religious_unity_action
# grant_electorate
# remove_electorate
# grant_freecity
# remove_freecity
# demand_unlawful_territory_action
# call_crusade_action
# excommunicate_action
# enforce_peace
# improve_relation
# fabricate_claim
# justify_trade_conflict
# transfer_trade_power
# infiltrate_administration
# sabotage_reputation
# support_rebels
# sow_discontent
# agitate_for_liberty
# form_coalition
# request_to_join_federation
# invite_to_federation
# support_heir
# break_marriage
# designate_march
# ask_for_march
# sell_ships_action
# abandon_union
# takeondebt
# influence_nation
# claim_states
# steer_trade
# stealmap
# spy_network
# invite_to_trade_league
# request_to_join_trade_league
# sharemap
# intervene_in_war
# break_alliance
# tributary_state_action
# ask_for_tributary_state_action
# knowledge_sharing

declarewar = {
    condition = {
		tooltip = CAN_DECLARE_WAR
        potential = {
            always = yes
        }
		allow = {
			OR = {
				AND = {
					ROOT = { capital_scope = { NOT = { continent = Oblivion } } }
					FROM = { capital_scope = { NOT = { continent = Oblivion } } }
				}
				AND = {
					ROOT = { capital_scope = { continent = Oblivion } }
					FROM = { capital_scope = { continent = Oblivion } }
				}
				AND = {
					ROOT = { capital_scope = { superregion = coldharbour_superregion } }
					FROM = { NOT = { capital_scope = { continent = Oblivion } } }
					has_global_flag = the_planemeld_flag
				}
				AND = {
					ROOT = { capital_scope = { superregion = deadlands_superregion } }
					FROM = { NOT = { capital_scope = { continent = Oblivion } } }
					has_global_flag = oblivion_crisis_flag
				}
				AND = {
					FROM = { capital_scope = { superregion = coldharbour_superregion } }
					ROOT = { NOT = { capital_scope = { continent = Oblivion } } }
					has_global_flag = the_planemeld_flag
				}
				AND = {
					FROM = { capital_scope = { superregion = deadlands_superregion } }
					ROOT = { NOT = { capital_scope = { continent = Oblivion } } }
					has_global_flag = oblivion_crisis_flag
				}
				
			}
		}
    }
	condition = {
		tooltip = AI_CANNOT_DECLARE_WHEN_STABILITY_LOSS
		potential = {
			ai = yes
		}
		allow = {
			ai = yes
			NOT = {
				has_global_modifier_value = {
					which = stability_cost_to_declare_war
					value = 1
				}
			}
		}
	}
	condition = {
		tooltip = CANNOT_DECLARE_WARS_OTHER_THAN_INDEPENDENCE_WARS
		potential = {
			has_country_modifier = revoked_war_rights
			FROM = { NOT = { overlord_of = ROOT } }
		}
		allow = {
			OR = {
				NOT = { has_country_modifier = revoked_war_rights }
				FROM = { overlord_of = ROOT }
			}
		}
	}
	effect = {
		on_war_declaration_effect = yes
		if = { 
			limit = { primary_culture = breton } 
			if = {
				limit = { check_variable = { which = es_chivalry value = -100 } }
				change_variable = { which = es_chivalry value = -10.0 }
			}
			else = {
				set_variable = { which = es_chivalry value = -100.0 }
			}
		}
		random_list = {
			1 = { play_sound = then_pay_with_your_blood }
			99 = { FROM = { add_prestige = 1 } }
		}
		FROM = {
			random_list = {
				5 = { play_sound = this_should_only_take_a_second }
				5 = { play_sound = never_should_have_come_here }
				90 = { add_prestige = 1 }
			}
		}
		if = { 
			limit = {
			FROM = { capital_scope = { NOT = { superregion = ROOT } } }
			}
			FROM = { set_country_flag = war_with_invader_flag }
		}
	    if = {
		    limit = { is_subject_of_type = alliance_member }
			overlord = { country_event = { id = alliancememberwar.1 days = 31 } }
		}
		### Oblivion Invasion
		if = {
			limit = { owns = 3188 NOT = { owns = 1206 } FROM = { owns = 1206 } has_global_flag = the_planemeld_flag } 
			1206 = { cede_province = ROOT add_core = ROOT
				infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT
				cavalry = ROOT cavalry = ROOT cavalry = ROOT cavalry = ROOT cavalry = ROOT cavalry = ROOT cavalry = ROOT cavalry = ROOT cavalry = ROOT cavalry = ROOT
				artillery = ROOT artillery = ROOT artillery = ROOT artillery = ROOT artillery = ROOT artillery = ROOT artillery = ROOT artillery = ROOT artillery = ROOT artillery = ROOT
			}
			
		}
		if = {
			limit = { owns = 4075 NOT = { owns = 1199 } FROM = { owns = 1199 } has_global_flag = oblivion_crisis_flag } 
			1199 = { cede_province = ROOT add_core = ROOT
				infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT
				cavalry = ROOT cavalry = ROOT cavalry = ROOT cavalry = ROOT cavalry = ROOT cavalry = ROOT cavalry = ROOT cavalry = ROOT cavalry = ROOT cavalry = ROOT
				artillery = ROOT artillery = ROOT artillery = ROOT artillery = ROOT artillery = ROOT artillery = ROOT artillery = ROOT artillery = ROOT artillery = ROOT artillery = ROOT
			}
		}
		if = {
			limit = { owns = 4074 NOT = { owns = 1376 } FROM = { owns = 1376 } has_global_flag = oblivion_crisis_flag } 
			1376 = { cede_province = ROOT add_core = ROOT
				infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT
				cavalry = ROOT cavalry = ROOT cavalry = ROOT cavalry = ROOT cavalry = ROOT cavalry = ROOT cavalry = ROOT cavalry = ROOT cavalry = ROOT cavalry = ROOT
				artillery = ROOT artillery = ROOT artillery = ROOT artillery = ROOT artillery = ROOT artillery = ROOT artillery = ROOT artillery = ROOT artillery = ROOT artillery = ROOT
			}
		}
		if = {
			limit = { owns = 4081 NOT = { owns = 1106 } FROM = { owns = 1106 } has_global_flag = oblivion_crisis_flag } 
			1106 = { cede_province = ROOT add_core = ROOT
				infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT infantry = ROOT
				cavalry = ROOT cavalry = ROOT cavalry = ROOT cavalry = ROOT cavalry = ROOT cavalry = ROOT cavalry = ROOT cavalry = ROOT cavalry = ROOT cavalry = ROOT
				artillery = ROOT artillery = ROOT artillery = ROOT artillery = ROOT artillery = ROOT artillery = ROOT artillery = ROOT artillery = ROOT artillery = ROOT artillery = ROOT
			}
		}
		### Oblivion Re-Invasion
		if = {
			limit = { owns = 1206 NOT = { owns = 3188 } FROM = { owns = 3188 } has_global_flag = the_planemeld_flag } 
			3188 = { cede_province = ROOT add_core = ROOT }
		}
		if = {
			limit = { owns = 1199 NOT = { owns = 4075 } FROM = { owns = 4075 } has_global_flag = oblivion_crisis_flag } 
			4075 = { cede_province = ROOT add_core = ROOT }
		}
		if = {
			limit = { owns = 1376 NOT = { owns = 4074 } FROM = { owns = 4074 } has_global_flag = oblivion_crisis_flag } 
			4074 = { cede_province = ROOT add_core = ROOT }
		}
		if = {
			limit = { owns = 1106 NOT = { owns = 4081 } FROM = { owns = 4081 } has_global_flag = oblivion_crisis_flag } 
			4081 = { cede_province = ROOT add_core = ROOT }
		}
		### The Forgotten Vale
		if = {
			limit = { owns = 7205 NOT = { owns = 7171 } FROM = { owns = 7171 } 7205 = { has_great_project = { type = darkfall_passage tier = 1 } } }
			7171 = { cede_province = ROOT add_core = ROOT }
		}
		if = {
			limit = { owns = 7171 NOT = { owns = 7205 } FROM = { owns = 7205 } 7205 = { has_great_project = { type = darkfall_passage tier = 1 } } }
			7205 = { cede_province = ROOT add_core = ROOT }
		}
	}
}

demand_unlawful_territory_action = {
	condition = {
		tooltip = CAN_DEMAND_UNLAWFUL_TERRITORY
		potential = {
			is_emperor = yes
		}
		allow = {
			FROM = {
				any_owned_province = {
					is_part_of_hre = yes
					NOT = { is_core = FROM }
					is_colony = no
				}
			}
		}
	}
}

allianceaction = {
	effect = {
		if = { limit = { has_government_mechanic = the_three_royal_fires_mechanic } add_government_power = { mechanic_type = the_three_royal_fires_mechanic	power_type = asha_vahishta value = 2.5 } }
		if = { 
			limit = { primary_culture = breton } 
			if = {
				limit = { NOT = { check_variable = { which = es_chivalry value = 100.0 } } }
				change_variable = { which = es_chivalry value = 5.0 }
			}
			else = {
				set_variable = { which = es_chivalry value = 100.0 }
			}
		}
	}
}

enforce_peace = {
	#do this before the peace is enforced, or there is no war enemy country
	pre_effect = {
		if = {
			limit = {
				is_defender_of_faith = yes
				FROM = {
					any_war_enemy_country = {
						religion = ROOT
						is_in_war = {
							defender_leader = THIS
							attacker_leader = FROM
						}
					}
				}
			}
			add_prestige = 10
		}
		if = { limit = { has_government_mechanic = the_three_royal_fires_mechanic } add_government_power = { mechanic_type = the_three_royal_fires_mechanic	power_type = asha_vahishta value = 5 } }
		if = { 
			limit = { primary_culture = breton } 
			if = {
				limit = { NOT = { check_variable = { which = es_chivalry value = 100.0 } } }
				change_variable = { which = es_chivalry value = 10.0 }
			}
			else = {
				set_variable = { which = es_chivalry value = 100.0 }
			}
		}
		change_variable = { which = enforced_peace_num value = 1.0 }
	}
	
}

royal_marriage = {
	condition = {
		tooltip = MARWAR
		potential = {
		}
		allow = {
			NOT = { war_with = FROM }
		}
	}
	effect = {
		if = { limit = { has_government_mechanic = the_three_royal_fires_mechanic } add_government_power = { mechanic_type = the_three_royal_fires_mechanic	power_type = asha_vahishta value = 0.5 } }
		if = { 
			limit = { primary_culture = breton } 
			if = {
				limit = { NOT = { check_variable = { which = es_chivalry value = 100.0 } } }
				change_variable = { which = es_chivalry value = 5.0 }
			}
			else = {
				set_variable = { which = es_chivalry value = 100.0 }
			}
		}
	}
}

guaranteeaction = {
	effect = {
		if = { limit = { has_government_mechanic = the_three_royal_fires_mechanic }  add_government_power = { mechanic_type = the_three_royal_fires_mechanic power_type = asha_vahishta value = 1.0 } }
		if = { 
			limit = { primary_culture = breton } 
			if = {
				limit = { NOT = { check_variable = { which = es_chivalry value = 100.0 } } }
				change_variable = { which = es_chivalry value = 2.5 }
			}
			else = {
				set_variable = { which = es_chivalry value = 100.0 }
			}
		}
	}
}
threaten_war = {
	effect = {
		if = { limit = { has_government_mechanic = the_three_royal_fires_mechanic }  add_government_power = { mechanic_type = the_three_royal_fires_mechanic	power_type = asha_vahishta value = -10 } }
		if = { 
			limit = { primary_culture = breton } 
			if = {
				limit = { check_variable = { which = es_chivalry value = -100.0 } }
				change_variable = { which = es_chivalry value = -10.0 }
			}
			else = {
				set_variable = { which = es_chivalry value = -100.0 }
			}
		}
	}
}
insultaction = {
	effect = {
		if = { limit = { has_government_mechanic = the_three_royal_fires_mechanic } add_government_power = { mechanic_type = the_three_royal_fires_mechanic	power_type = asha_vahishta value = -5 } }
		if = { 
			limit = { primary_culture = breton } 
			if = {
				limit = { NOT = { check_variable = { which = es_chivalry value = -100.0 } } }
				change_variable = { which = es_chivalry value = -2.5 }
			}
			else = {
				set_variable = { which = es_chivalry value = -100.0 }
			}
		}
	}
}
giftaction = {
	effect = {
		if = { limit = { has_government_mechanic = the_three_royal_fires_mechanic } add_government_power = { mechanic_type = the_three_royal_fires_mechanic	power_type = asha_vahishta value = 1.0 } }
	}
}
fabricate_claim = {
	effect = {
		if = { limit = { has_government_mechanic = the_three_royal_fires_mechanic } add_government_power = { mechanic_type = the_three_royal_fires_mechanic	power_type = asha_vahishta value = -1.0 } }
		if = { 
			limit = { primary_culture = breton } 
			if = {
				limit = { check_variable = { which = es_chivalry value = -100.0 } }
				change_variable = { which = es_chivalry value = -1.0 }
			}
			else = {
				set_variable = { which = es_chivalry value = -100.0 }
			}
		}
	}
}
break_marriage = {
	effect = {
		if = { limit = { has_government_mechanic = the_three_royal_fires_mechanic } add_government_power = { mechanic_type = the_three_royal_fires_mechanic	power_type = asha_vahishta value = -1.0 } }
		if = { 
			limit = { primary_culture = breton } 
			if = {
				limit = { check_variable = { which = es_chivalry value = -100.0 } }
				change_variable = { which = es_chivalry value = -1.5 }
			}
			else = {
				set_variable = { which = es_chivalry value = -100.0 }
			}
		}
	}
}
takeondebt = {
	effect = {
		if = { limit = { has_government_mechanic = the_three_royal_fires_mechanic } add_government_power = { mechanic_type = the_three_royal_fires_mechanic	power_type = asha_vahishta value = 2.5 } }
		if = { 
			limit = { primary_culture = breton } 
			if = {
				limit = { NOT = { check_variable = { which = es_chivalry value = 100.0 } } }
				change_variable = { which = es_chivalry value = 1.0 }
			}
			else = {
				set_variable = { which = es_chivalry value = 100.0 }
			}
		}
	}
}
influence_nation = {
	effect = {
		if = { limit = { has_government_mechanic = the_three_royal_fires_mechanic } add_government_power = { mechanic_type = the_three_royal_fires_mechanic	power_type = asha_vahishta value = 2.5 } }
		if = { 
			limit = { primary_culture = breton } 
			if = {
				limit = { check_variable = { which = es_chivalry value = -100.0 } }
				change_variable = { which = es_chivalry value = -1.5 }
			}
			else = {
				set_variable = { which = es_chivalry value = -100.0 }
			}
		}
	}
}
intervene_in_war = {
	effect = {
		if = { limit = { has_government_mechanic = the_three_royal_fires_mechanic }  add_government_power = { mechanic_type = the_three_royal_fires_mechanic	power_type = asha_vahishta value = -15 } }
		if = { 
			limit = { primary_culture = breton } 
			if = {
				limit = { NOT = { check_variable = { which = es_chivalry value = 100.0 } } }
				change_variable = { which = es_chivalry value = 10.0 }
			}
			else = {
				set_variable = { which = es_chivalry value = 100.0 }
			}
		}
	}
}

annexationaction = {
	condition = {
		tooltip = ANNEX_TOO_LOW
		potential = {
			FROM = {
				vassal_of = ROOT
			}
		}
		allow = {
			FROM = {
				has_opinion = {
					who = ROOT
					value = 190
				}
			}
		}
	}
	condition = {
		tooltip = CANNOT_ANNEX_HRE_MEMBERS_UNLESS
		potential = {
			is_part_of_hre = yes
			FROM = {
				is_part_of_hre = yes
				vassal_of = ROOT
			}
		}
		allow = {
			OR = {
				hre_reform_passed = es_emperor_guarantee_political_protection
				hre_reform_passed = es_emperor_surpress_independent_opposition
			}
			FROM = {
				is_part_of_hre = yes
			}
		}
	}
}

integrationaction = {
	condition = {
		tooltip = INTEGRATETOOLOWDESC
		potential = {
			senior_union_with = FROM
		}
		allow = {
			FROM = {
				has_opinion = {
					who = ROOT
					value = 190
				}
			}
		}
	}
	condition = {
		tooltip = CANNOT_ANNEX_HRE_MEMBERS_UNLESS
		potential = {
			is_part_of_hre = yes
			FROM = {
				is_part_of_hre = yes
				vassal_of = ROOT
			}
		}
		allow = {
			OR = {
				hre_reform_passed = es_emperor_guarantee_political_protection
				hre_reform_passed = es_emperor_surpress_independent_opposition
			}
			FROM = {
				is_part_of_hre = yes
			}
		}
	}
}

vassalaction = {
	condition = {
		tooltip = VASSALINVALID
		potential = {
		}
		allow = {
			alliance_with = FROM
			FROM = {
				has_opinion = {
					who = ROOT
					value = 190
				}
			}
			FROM = { is_at_war = no }
		}
	}
}

abandon_union_action = {
	condition = {
		tooltip = CAN_ABANDON_UNION
		potential = {
		}
		allow = {
			ROOT = {
				senior_union_with = FROM
				is_at_war = no
			}
		}
	}
	condition = {
		tooltip = PUABANDONINGIMPOSSIBLE
		potential = {
			has_government_attribute = cannot_abandon_personal_unions
		}
		allow = {
			NOT = { has_government_attribute = cannot_abandon_personal_unions }
		}
	}
	effect = {
		if = { 
			limit = { primary_culture = breton } 
			if = {
				limit = { check_variable = { which = es_chivalry value = -100.0 } }
				change_variable = { which = es_chivalry value = -10.0 }
			}
			else = {
				set_variable = { which = es_chivalry value = -100.0 }
			}
		}
	}
}

form_coalition = {
	condition = {
		tooltip = MINAGGRESSIVEEXPANSION
		potential = {	
		}
		allow = {
			has_opinion_modifier = {
				modifier = aggressive_expansion
				who = FROM
			}		
			NOT = {
				has_opinion_modifier = {
					modifier = aggressive_expansion
					who = FROM
					value = -50
				}
			}
		}
	}
	effect = {
		if = { 
			limit = { primary_culture = breton } 
			if = {
				limit = { NOT = { check_variable = { which = es_chivalry value = 100.0 } } }
				change_variable = { which = es_chivalry value = 10.0 }
			}
			else = {
				set_variable = { which = es_chivalry value = 100.0 }
			}
		}
	}
}

request_to_join_trade_league = {
	condition = {
		tooltip = NOT_RELEVANT_TRADE_REGION_FROM
		potential = {
		}
		allow = {
			home_trade_node = {
				trade_share = {
					country = from
					share = 1
				}
			}
		}
	}
}

invite_to_trade_league = {
	condition = {
		tooltip = NOT_RELEVANT_TRADE_REGION
		potential = {
		}
		allow = {
			from = {
				home_trade_node = {
					trade_share = {
						country = root
						share = 1
					}
				}
			}
		}
	}
}

milaccess = {
	condition = {
		tooltip = CAN_MILACCESS
		potential = {
		}
		allow = {
			OR = {
				AND = {
					ROOT = { capital_scope = { NOT = { continent = Oblivion } } }
					FROM = { capital_scope = { NOT = { continent = Oblivion } } }
				}
				AND = {
					ROOT = { capital_scope = { continent = Oblivion } }
					FROM = { capital_scope = { continent = Oblivion } }
				}
			}
		}
	}
}

fleet_access = {
	condition = {
		tooltip = CAN_FLEET_ACCESS
		potential = {
		}
		allow = {
			OR = {
				AND = {
					ROOT = { capital_scope = { NOT = { continent = Oblivion } } }
					FROM = { capital_scope = { NOT = { continent = Oblivion } } }
				}
				AND = {
					ROOT = { capital_scope = { continent = Oblivion } }
					FROM = { capital_scope = { continent = Oblivion } }
				}
			}
		}
	}
}

offer_fleet_access = {
	condition = {
		tooltip = CAN_OFFER_FLEET_ACCESS
		potential = {
		}
		allow = {
			OR = {
				AND = {
					ROOT = { capital_scope = { NOT = { continent = Oblivion } } }
					FROM = { capital_scope = { NOT = { continent = Oblivion } } }
				}
				AND = {
					ROOT = { capital_scope = { continent = Oblivion } }
					FROM = { capital_scope = { continent = Oblivion } }
				}
			}
		}
	}
}

offermilaccess = {
	condition = {
		tooltip = CAN_OFFER_MILACCESS
		potential = {
		}
		allow = {
			OR = {
				AND = {
					ROOT = { capital_scope = { NOT = { continent = Oblivion } } }
					FROM = { capital_scope = { NOT = { continent = Oblivion } } }
				}
				AND = {
					ROOT = { capital_scope = { continent = Oblivion } }
					FROM = { capital_scope = { continent = Oblivion } }
				}
			}
		}
	}
}

sow_discontent = {
	condition = {
		tooltip = CAN_SOW_DISCONTENT
		potential = {
		}
		allow = {
			OR = {
				has_idea_group = gesellschaft0
				has_idea_group = propaganda0
			}
		}
	}
	effect = {
		if = { 
			limit = { primary_culture = breton } 
			if = {
				limit = { check_variable = { which = es_chivalry value = -100.0 } }
				change_variable = { which = es_chivalry value = -5.0 }
			}
			else = {
				set_variable = { which = es_chivalry value = -100.0 }
			}
		}
		change_variable = { which = sowed_discontend_num value = 1.0 }
	}
}

tributary_state_action = {
	condition = {
		tooltip = EMPTY_TOOLTIP
		potential = {
			ai = yes
		}
		allow = {
			OR = {
				is_emperor = yes
				is_emperor_of_china = yes
				government = tribal
			}
		}
	}
}

ask_for_tributary_state_action = {
	condition = {
		tooltip = EMPTY_TOOLTIP
		potential = {
			ai = yes
		}
		allow = {
			FROM = {
				OR = {
					is_emperor = yes
					is_emperor_of_china = yes
					government = tribal
				}
			}
		}
	}
}

break_alliance = {
	effect = {
		on_alliance_broken_effect = yes
		if = { 
			limit = { primary_culture = breton } 
			if = {
				limit = { check_variable = { which = es_chivalry value = -100.0 } }
				change_variable = { which = es_chivalry value = -5.0 }
			}
			else = {
				set_variable = { which = es_chivalry value = -100.0 }
			}
		}
	}
}

#remove_electorate = {
#	condition = {
#		tooltip = CAN_REVOKE_ELECTORATE
#		potential = {
#			is_emperor = yes
#		}
#		allow = {
#			FROM = { is_subject = yes }
#		}
#	}
#}

religious_unity_action = {
	condition = {
		tooltip = CAN_DEMAND_RELIGIOUS_UNITY
		potential = {
			is_emperor = yes
		}
		allow = {
			is_at_war = no
			FROM = { 
				is_at_war = no
				is_part_of_hre = yes
			}
		}
	}
}

support_independence_action = {
	effect = {
		if = { 
			limit = { primary_culture = breton } 
			if = {
				limit = { NOT = { check_variable = { which = es_chivalry value = 100.0 } } }
				change_variable = { which = es_chivalry value = 5.0 }
			}
			else = {
				set_variable = { which = es_chivalry value = 100.0 }
			}
		}
	}
}

support_rebels = {
	effect = {
		if = { 
			limit = { primary_culture = breton } 
			if = {
				limit = { NOT = { check_variable = { which = es_chivalry value = 100.0 } } }
				change_variable = { which = es_chivalry value = 2.5 }
			}
			else = {
				set_variable = { which = es_chivalry value = 100.0 }
			}
		}
	}
}

agitate_for_liberty = {
	effect = {
		if = { 
			limit = { primary_culture = breton } 
			if = {
				limit = { NOT = { check_variable = { which = es_chivalry value = 100.0 } } }
				change_variable = { which = es_chivalry value = 2.5 }
			}
			else = {
				set_variable = { which = es_chivalry value = 100.0 }
			}
		}
	}
}

claim_states = {
	effect = {
		if = { 
			limit = { primary_culture = breton } 
			if = {
				limit = { check_variable = { which = es_chivalry value = -100.0 } }
				change_variable = { which = es_chivalry value = -2.5 }
			}
			else = {
				set_variable = { which = es_chivalry value = -100.0 }
			}
		}
	}
}

sabotage_reputation = {
	effect = {
		if = { 
			limit = { primary_culture = breton } 
			if = {
				limit = { check_variable = { which = es_chivalry value = -100.0 } }
				change_variable = { which = es_chivalry value = -5.0 }
			}
			else = {
				set_variable = { which = es_chivalry value = -100.0 }
			}
		}
	}
}

infiltrate_administration = {
	effect = {
		if = { 
			limit = { primary_culture = breton } 
			if = {
				limit = { check_variable = { which = es_chivalry value = -100.0 } }
				change_variable = { which = es_chivalry value = -2.5 }
			}
			else = {
				set_variable = { which = es_chivalry value = -100.0 }
			}
		}
		change_variable = { which = infiltrated_administration_num value = 1.0 }
	}
}

# Breton Chivalry #################################################################################

# Positive
#+10 enforce_peace / form_coalition / intervene_in_war
#+5.0 support_independence_action / allianceaction / royal_marriage
#+2.5 support_rebels / agitate_for_liberty / guaranteeaction
#+1.0 takeondebt

# Negative
#-10 threaten_war / declare_war / abandon_union
#-5.0 sow_discontent / break_alliance / sabotage_reputation
#-2.5 claim_states / infiltrate_administration / insultaction
#-1.0 fabricate_claim