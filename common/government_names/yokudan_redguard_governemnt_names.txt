redguard_monarchy = {
	rank = {
		1 = BARONY
		2 = BARONY
		3 = BARONY
		4 = BARONY
		5 = KINGDOM
		6 = KINGDOM
		7 = KINGDOM
		8 = HIGH_KINGDOM
		9 = HIGH_KINGDOM
		10 = EMPIRE
	}
	ruler_male = {
		1 = BARON
		2 = BARON
		3 = BARON
		4 = BARON
		5 = KING
		6 = KING
		7 = KING
		8 = KING
		9 = HIGH_KING
		10 = EMPEROR
	}
	ruler_female = {
		1 = BARONESS
		2 = BARONESS
		3 = BARONESS
		4 = BARONESS
		5 = QUEEN
		6 = QUEEN
		7 = QUEEN
		8 = HIGH_QUEEN
		9 = HIGH_QUEEN
		10 = EMPRESS
	}
	consort_male = {
		1 = BARON
		2 = BARON
		3 = BARON
		4 = BARON
		5 = KING
		6 = KING
		7 = KING
		8 = KING
		9 = HIGH_KING
		10 = EMPEROR
	}
	consort_female = {
		1 = BARONESS
		2 = BARONESS
		3 = BARONESS
		4 = BARONESS
		5 = QUEEN
		6 = QUEEN
		7 = QUEEN
		8 = HIGH_QUEEN
		9 = HIGH_QUEEN
		10 = EMPRESS
	}
	heir_male = {
		1 = PRINCE
		2 = PRINCE
		3 = PRINCE
		4 = PRINCE
		5 = PRINCE
		6 = PRINCE
		7 = PRINCE
		8 = PRINCE
		9 = PRINCE
		10 = PRINCE
	}
	heir_female = {
		1 = PRINCESS
		2 = PRINCESS
		3 = PRINCESS
		4 = PRINCESS
		5 = PRINCESS
		6 = PRINCESS
		7 = PRINCESS
		8 = PRINCESS
		9 = PRINCESS
		10 = PRINCESS
	}
	trigger = {
		OR = {
			government = monarchy
			government = republic
		}
		culture_group = yokudo_redguard_cg
		NOT = { capital_scope = { continent = Yokuda } }
	}
}
	
yokudan_monarchy = {
	rank = {
		1 = PETTY_KINGDOM
		2 = PETTY_KINGDOM
		3 = PETTY_KINGDOM
		4 = PETTY_KINGDOM
		5 = KINGDOM
		6 = KINGDOM
		7 = KINGDOM
		8 = HIGH_KINGDOM
		9 = HIGH_KINGDOM
		10 = EMPIRE
	}
	ruler_male = {
		1 = PETTY_KING
		2 = PETTY_KING
		3 = PETTY_KING
		4 = PETTY_KING
		5 = KING
		6 = KING
		7 = KING
		8 = HIGH_KING 
		9 = HIGH_KING
		10 = EMPEROR
	}
	ruler_female = {
		1 = PETTY_QUEEN
		2 = PETTY_QUEEN
		3 = PETTY_QUEEN
		4 = PETTY_QUEEN
		5 = QUEEN
		6 = QUEEN
		7 = QUEEN
		8 = HIGH_QUEEN
		9 = HIGH_QUEEN
		10 = EMPRESS
	}
	consort_male = {
		1 = PETTY_KING
		2 = PETTY_KING
		3 = PETTY_KING
		4 = PETTY_KING
		5 = KING
		6 = KING
		7 = KING
		8 = HIGH_KING 
		9 = HIGH_KING
		10 = EMPEROR
	}
	consort_female = {
		1 = PETTY_QUEEN
		2 = PETTY_QUEEN
		3 = PETTY_QUEEN
		4 = PETTY_QUEEN
		5 = QUEEN
		6 = QUEEN
		7 = QUEEN
		8 = HIGH_QUEEN
		9 = HIGH_QUEEN
		10 = EMPRESS
	}
	heir_male = {
		1 = PRINCE
		2 = PRINCE
		3 = PRINCE
		4 = PRINCE
		5 = PRINCE
		6 = PRINCE
		7 = PRINCE
		8 = PRINCE
		9 = PRINCE
		10 = PRINCE
	}
	heir_female = {
		1 = PRINCESS
		2 = PRINCESS
		3 = PRINCESS
		4 = PRINCESS
		5 = PRINCESS
		6 = PRINCESS
		7 = PRINCESS
		8 = PRINCESS
		9 = PRINCESS
		10 = PRINCESS
	}
	trigger = {
		OR = {
			government = monarchy
			government = republic
		}
		culture_group = yokudo_redguard_cg
		capital_scope = { continent = Yokuda }
	}
}