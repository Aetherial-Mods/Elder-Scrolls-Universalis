dwemer_default = {
	rank = {
		1 = DWEMER_CITADEL
		2 = DWEMER_CITADEL
		3 = STRONGHOLD
		4 = STRONGHOLD
		5 = FREEHOLD
		6 = FREEHOLD
		7 = DWEMER_REALM
		8 = DWEMER_REALM
		9 = DWEMER_KINGDOM
		10 = DWEMER_EMPIRE
	}
	ruler_male = {
		1 = PROTECTOR
		2 = PROTECTOR
		3 = CRAFTLORD
		4 = CRAFTLORD
		5 = HIGH_CRAFTLORD
		6 = HIGH_CRAFTLORD
		7 = DWARF_LORD
		8 = DWARF_LORD
		9 = DWARF_KING
		10 = DWARF_EMPEROR
	}
	ruler_female = {
		1 = PROTECTOR
		2 = PROTECTOR
		3 = CRAFTLADY
		4 = CRAFTLADY
		5 = HIGH_CRAFTLADY
		6 = HIGH_CRAFTLADY
		7 = DWARF_LADY
		8 = DWARF_LADY
		9 = DWARF_QUEEN
		10 = DWARF_EMPRESS
	}
	consort_male = {
		1 = LORD
		2 = LORD
		3 = LORD
		4 = LORD
		5 = LORD
		6 = LORD
		7 = LORD
		8 = LORD
		9 = LORD
		10 = LORD
	}
	consort_female = {
		1 = LADY
		2 = LADY
		3 = LADY
		4 = LADY
		5 = LADY
		6 = LADY
		7 = LADY
		8 = LADY
		9 = LADY
		10 = LADY
	}
	heir_male = {
		1 = LORD
		2 = LORD
		3 = LORD
		4 = LORD
		5 = LORD
		6 = LORD
		7 = LORD
		8 = LORD
		9 = LORD
		10 = LORD
	}
	heir_female = {
		1 = LADY
		2 = LADY
		3 = LADY
		4 = LADY
		5 = LADY
		6 = LADY
		7 = LADY
		8 = LADY
		9 = LADY
		10 = LADY
	}
	trigger = {
		culture_group = dwemer_cg
		OR = {
			government = monarchy
			government = republic
		}
	}
}

dwemer_theocracies = {
	rank = {
		1 = DWEMER_CITADEL
		2 = DWEMER_CITADEL
		3 = STRONGHOLD
		4 = STRONGHOLD
		5 = FREEHOLD
		6 = FREEHOLD
		7 = DWEMER_EXCAVATION
		8 = DWEMER_EXCAVATION
		9 = RESONANT_HOLD
		10 = TONAL_AUTOCRACY
	}
	ruler_male = {
		1 = MAGE_ENGINEER
		2 = MAGE_ENGINEER
		3 = CRAFTLORD
		4 = CRAFTLORD
		5 = HIGH_CRAFTLORD
		6 = HIGH_CRAFTLORD
		7 = MAGE_CRAFTER
		8 = MAGE_CRAFTER
		9 = TONAL_ARCHITECT
		10 = CHIEF_TONAL_ARCHITECT
	}
	ruler_female = {
		1 = MAGE_ENGINEER
		2 = MAGE_ENGINEER
		3 = CRAFTLADY
		4 = CRAFTLADY
		5 = HIGH_CRAFTLADY
		6 = HIGH_CRAFTLADY
		7 = MAGE_CRAFTER
		8 = MAGE_CRAFTER
		9 = TONAL_ARCHITECT
		10 = CHIEF_TONAL_ARCHITECT
	}
	consort_male = {
		1 = MAGE_ENGINEER
		2 = MAGE_ENGINEER
		3 = CRAFTLORD
		4 = CRAFTLORD
		5 = HIGH_CRAFTLORD
		6 = HIGH_CRAFTLORD
		7 = MAGE_CRAFTER
		8 = MAGE_CRAFTER
		9 = TONAL_ARCHITECT
		10 = CHIEF_TONAL_ARCHITECT
	}
	consort_female = {
		1 = MAGE_ENGINEER
		2 = MAGE_ENGINEER
		3 = CRAFTLADY
		4 = CRAFTLADY
		5 = HIGH_CRAFTLADY
		6 = HIGH_CRAFTLADY
		7 = MAGE_CRAFTER
		8 = MAGE_CRAFTER
		9 = TONAL_ARCHITECT
		10 = CHIEF_TONAL_ARCHITECT
	}
	heir_male = {
		1 = LORD
		2 = LORD
		3 = MAGE_ENGINEER
		4 = MAGE_ENGINEER
		5 = CRAFTLORD
		6 = CRAFTLORD
		7 = HIGH_CRAFTLORD
		8 = HIGH_CRAFTLORD
		9 = MAGE_CRAFTER
		10 = TONAL_ARCHITECT
	}
	heir_female = {
		1 = LADY
		2 = LADY
		3 = MAGE_ENGINEER
		4 = MAGE_ENGINEER
		5 = CRAFTLADY
		6 = CRAFTLADY
		7 = HIGH_CRAFTLADY
		8 = HIGH_CRAFTLADY
		9 = MAGE_CRAFTER
		10 = TONAL_ARCHITECT
	}
	trigger = {
		OR = {
			culture_group = dwemer_cg
			religion = reason_and_logic_cult
		}
		government = theocracy
	}
}