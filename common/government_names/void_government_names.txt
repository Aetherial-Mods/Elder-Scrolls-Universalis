void_monarchies = {
	rank = {
		1 = PACK
		2 = PACK
		3 = PACK
		4 = PACK
		5 = FEROCIOUS_PACK
		6 = FEROCIOUS_PACK
		7 = FEROCIOUS_PACK
		8 = FEROCIOUS_PACK
		9 = SAVAGE_PACK
		10 = DREADFUL_PACK
	}
	ruler_male = {
		1 = BEAST
		2 = BEAST
		3 = BEAST
		4 = BEAST
		5 = PACEMAKER
		6 = PACEMAKER
		7 = PACEMAKER
		8 = PACEMAKER
		9 = RUINATOR
		10 = ALPHA
	}
	ruler_female = {
		1 = BEAST
		2 = BEAST
		3 = BEAST
		4 = BEAST
		5 = PACEMAKER
		6 = PACEMAKER
		7 = PACEMAKER
		8 = PACEMAKER
		9 = RUINATOR
		10 = ALPHA
	}
	consort_male = {
		1 = POSSESSION
		2 = POSSESSION
		3 = POSSESSION
		4 = POSSESSION
		5 = POSSESSION
		6 = POSSESSION
		7 = POSSESSION
		8 = POSSESSION
		9 = POSSESSION
		10 = POSSESSION
	}
	consort_female = {
		1 = POSSESSION
		2 = POSSESSION
		3 = POSSESSION
		4 = POSSESSION
		5 = POSSESSION
		6 = POSSESSION
		7 = POSSESSION
		8 = POSSESSION
		9 = POSSESSION
		10 = POSSESSION
	}
	heir_male = {
		1 = WHELP
		2 = WHELP
		3 = WHELP
		4 = WHELP
		5 = WHELP
		6 = WHELP
		7 = WHELP
		8 = WHELP
		9 = WHELP
		10 = WHELP
	}
	heir_female = {
		1 = WHELP
		2 = WHELP
		3 = WHELP
		4 = WHELP
		5 = WHELP
		6 = WHELP
		7 = WHELP
		8 = WHELP
		9 = WHELP
		10 = WHELP
	}
	trigger = {
		OR = {
			primary_culture = clanfear
			primary_culture = daedroth
		}
	}
}

void_arachnid_monarchies = {
	rank = {
		1 = LAIR
		2 = LAIR
		3 = LAIR
		4 = LAIR
		5 = NEST
		6 = NEST
		7 = NEST
		8 = DOMAIN
		9 = DOMAIN
		10 = EMPIRE
	}
	ruler_male = {
		1 = WEB_FATHER
		2 = WEB_FATHER
		3 = WEB_FATHER
		4 = WEB_FATHER
		5 = WEB_MASTER
		6 = WEB_MASTER
		7 = WEB_MASTER
		8 = WEB_KING
		9 = WEB_KING
		10 = WEB_EMPEROR
	}
	ruler_female = {
		1 = WEB_MOTHER
		2 = WEB_MOTHER
		3 = WEB_MOTHER
		4 = WEB_MOTHER
		5 = WEB_MISTRESS
		6 = WEB_MISTRESS
		7 = WEB_MISTRESS
		8 = WEB_QUEEN
		9 = WEB_QUEEN
		10 = WEB_EMPRESS
	}
	consort_male = {
		1 = WEB_FATHER
		2 = WEB_FATHER
		3 = WEB_FATHER
		4 = WEB_FATHER
		5 = WEB_MASTER
		6 = WEB_MASTER
		7 = WEB_MASTER
		8 = WEB_KING
		9 = WEB_KING
		10 = WEB_EMPEROR
	}
	consort_female = {
		1 = WEB_MOTHER
		2 = WEB_MOTHER
		3 = WEB_MOTHER
		4 = WEB_MOTHER
		5 = WEB_MISTRESS
		6 = WEB_MISTRESS
		7 = WEB_MISTRESS
		8 = WEB_QUEEN
		9 = WEB_QUEEN
		10 = WEB_EMPRESS
	}
	heir_male = {
		1 = SPIDERLING
		2 = SPIDERLING
		3 = SPIDERLING
		4 = SPIDERLING
		5 = SPIDERLING
		6 = SPIDERLING
		7 = SPIDERLING
		8 = SPIDERLING
		9 = SPIDERLING
		10 = SPIDERLING
	}
	heir_female = {
		1 = SPIDERLING
		2 = SPIDERLING
		3 = SPIDERLING
		4 = SPIDERLING
		5 = SPIDERLING
		6 = SPIDERLING
		7 = SPIDERLING
		8 = SPIDERLING
		9 = SPIDERLING
		10 = SPIDERLING
	}
	trigger = {
		primary_culture = arachnid
	}
}

void_harvester_monarchies = {
	rank = {
		1 = LAIR
		2 = LAIR
		3 = LAIR
		4 = LAIR
		5 = NEST
		6 = NEST
		7 = NEST
		8 = DOMAIN
		9 = DOMAIN
		10 = EMPIRE
	}
	ruler_male = {
		1 = FATHER
		2 = FATHER
		3 = FATHER
		4 = FATHER
		5 = NEST_FATHER
		6 = NEST_FATHER
		7 = NEST_FATHER
		8 = DOMINUS
		9 = DOMINUS
		10 = PATRIARCH
	}
	ruler_female = {
		1 = MOTHER
		2 = MOTHER
		3 = MOTHER
		4 = MOTHER
		5 = NEST_MOTHER
		6 = NEST_MOTHER
		7 = NEST_MOTHER
		8 = DOMINA
		9 = DOMINA
		10 = MATRIARCH
	}
	consort_male = {
		1 = FATHER
		2 = FATHER
		3 = FATHER
		4 = FATHER
		5 = NEST_FATHER
		6 = NEST_FATHER
		7 = NEST_FATHER
		8 = DOMINUS
		9 = DOMINUS
		10 = PATRIARCH
	}
	consort_female = {
		1 = MOTHER
		2 = MOTHER
		3 = MOTHER
		4 = MOTHER
		5 = NEST_MOTHER
		6 = NEST_MOTHER
		7 = NEST_MOTHER
		8 = DOMINA
		9 = DOMINA
		10 = MATRIARCH
	}
	heir_male = {
		1 = SNAKELET
		2 = SNAKELET
		3 = SNAKELET
		4 = SNAKELET
		5 = SNAKELET
		6 = SNAKELET
		7 = SNAKELET
		8 = SNAKELET
		9 = SNAKELET
		10 = SNAKELET
	}
	heir_female = {
		1 = SNAKELET
		2 = SNAKELET
		3 = SNAKELET
		4 = SNAKELET
		5 = SNAKELET
		6 = SNAKELET
		7 = SNAKELET
		8 = SNAKELET
		9 = SNAKELET
		10 = SNAKELET
	}
	trigger = {
		primary_culture = harvester
	}
}