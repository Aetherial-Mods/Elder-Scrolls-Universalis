reachmen_monarchy_reach = {
	rank = {
		1 = CHIEFDOM
		2 = CHIEFDOM
		3 = CHIEFDOM
		4 = HIGH_CHIEFDOM
		5 = HIGH_CHIEFDOM
		6 = HIGH_CHIEFDOM
		7 = KINGDOM
		8 = KINGDOM
		9 = HIGH_KINGDOM
		10 = EMPIRE
	}
	ruler_male = {
		1 = CHIEF
		2 = CHIEF
		3 = CHIEF
		4 = HIGH_CHIEF
		5 = HIGH_CHIEF
		6 = HIGH_CHIEF
		7 = KING
		8 = KING
		9 = HIGH_KING
		10 = ARD
	}
	ruler_female = {
		1 = CHIEFTESS
		2 = CHIEFTESS
		3 = CHIEFTESS
		4 = HIGH_CHIEFTESS
		5 = HIGH_CHIEFTESS
		6 = HIGH_CHIEFTESS
		7 = QUEEN
		8 = QUEEN
		9 = HIGH_QUEEN
		10 = ARD
	}
	consort_male = {
		1 = CHIEF
		2 = CHIEF
		3 = CHIEF
		4 = HIGH_CHIEF
		5 = HIGH_CHIEF
		6 = HIGH_CHIEF
		7 = KING
		8 = KING
		9 = HIGH_KING
		10 = EMPEROR
	}
	consort_female = {
		1 = CHIEFTESS
		2 = CHIEFTESS
		3 = CHIEFTESS
		4 = HIGH_CHIEFTESS
		5 = HIGH_CHIEFTESS
		6 = HIGH_CHIEFTESS
		7 = QUEEN
		8 = QUEEN
		9 = HIGH_QUEEN
		10 = EMPRESS
	}
	heir_male = {
		1 = HEIR
		2 = HEIR
		3 = HEIR
		4 = HEIR
		5 = HEIR
		6 = HEIR
		7 = PRINCE
		8 = PRINCE
		9 = PRINCE
		10 = PRINCE
	}
	heir_female = {
		1 = HEIR
		2 = HEIR
		3 = HEIR
		4 = HEIR
		5 = HEIR
		6 = HEIR
		7 = PRINCESS
		8 = PRINCESS
		9 = PRINCESS
		10 = PRINCESS
	}
	trigger = {
		OR = {
			government = tribal
			government = monarchy
		}
		culture_group = half_blood_cg
		NOT = { primary_culture = breton }
		capital_scope = { superregion = skyrim_superregion }
	}
}

reachmen_republic_reach = {
	rank = {
		1 = CHIEFDOM
		2 = CHIEFDOM
		3 = CHIEFDOM
		4 = HIGH_CHIEFDOM
		5 = HIGH_CHIEFDOM
		6 = HIGH_CHIEFDOM
		7 = KINGDOM
		8 = KINGDOM
		9 = HIGH_KINGDOM
		10 = EMPIRE
	}
	ruler_male = {
		1 = SPEAKER
		2 = SPEAKER
		3 = SPEAKER
		4 = ELDER
		5 = ELDER
		6 = ELDER
		7 = KING
		8 = KING
		9 = HIGH_KING
		10 = ARD
	}
	ruler_female = {
		1 = SPEAKER
		2 = SPEAKER
		3 = SPEAKER
		4 = ELDER
		5 = ELDER
		6 = ELDER
		7 = QUEEN
		8 = QUEEN
		9 = HIGH_QUEEN
		10 = ARD
	}
	consort_male = {
		1 = SPEAKER
		2 = SPEAKER
		3 = SPEAKER
		4 = ELDER
		5 = ELDER
		6 = ELDER
		7 = KING
		8 = KING
		9 = HIGH_KING
		10 = EMPEROR
	}
	consort_female = {
		1 = SPEAKER
		2 = SPEAKER
		3 = SPEAKER
		4 = ELDER
		5 = ELDER
		6 = ELDER
		7 = QUEEN
		8 = QUEEN
		9 = HIGH_QUEEN
		10 = EMPRESS
	}
	heir_male = {
		1 = HEIR
		2 = HEIR
		3 = HEIR
		4 = HEIR
		5 = HEIR
		6 = HEIR
		7 = PRINCE
		8 = PRINCE
		9 = PRINCE
		10 = PRINCE
	}
	heir_female = {
		1 = HEIR
		2 = HEIR
		3 = HEIR
		4 = HEIR
		5 = HEIR
		6 = HEIR
		7 = PRINCESS
		8 = PRINCESS
		9 = PRINCESS
		10 = PRINCESS
	}
	trigger = {
		government = republic
		culture_group = half_blood_cg
		NOT = { primary_culture = breton }
		capital_scope = { superregion = skyrim_superregion }
	}
}

reachmen_theocracy_reach = {
	rank = {
		1 = COVEN
		2 = COVEN
		3 = COVEN
		4 = CULT
		5 = CULT
		6 = CULT
		7 = WITCH_KINGDOM
		8 = WITCH_KINGDOM
		9 = WITCH_KINGDOM
		10 = EMPIRE
	}
	ruler_male = {
		1 = SORCERER
		2 = SORCERER
		3 = SORCERER
		4 = WITCH_SORCERER
		5 = WITCH_SORCERER
		6 = WITCH_SORCERER
		7 = WITCH_KING
		8 = WITCH_KING
		9 = WITCH_KING
		10 = ARD
	}
	ruler_female = {
		1 = SORCERESS
		2 = SORCERESS
		3 = SORCERESS
		4 = HAG_SORCERESS
		5 = HAG_SORCERESS
		6 = HAG_SORCERESS
		7 = HAG_QUEEN
		8 = HAG_QUEEN
		9 = HAG_QUEEN
		10 = ARD
	}
	consort_male = {
		1 = SORCERER
		2 = SORCERER
		3 = SORCERER
		4 = WITCH_SORCERER
		5 = WITCH_SORCERER
		6 = WITCH_SORCERER
		7 = WITCH_KING
		8 = WITCH_KING
		9 = WITCH_KING
		10 = EMPEROR
	}
	consort_female = {
		1 = SORCERESS
		2 = SORCERESS
		3 = SORCERESS
		4 = HAG_SORCERESS
		5 = HAG_SORCERESS
		6 = HAG_SORCERESS
		7 = HAG_QUEEN
		8 = HAG_QUEEN
		9 = HAG_QUEEN
		10 = EMPRESS
	}
	heir_male = {
		1 = WARLOCK
		2 = WARLOCK
		3 = WARLOCK
		4 = SORCERER
		5 = SORCERER
		6 = SORCERER
		7 = WITCH_PRINCE
		8 = WITCH_PRINCE
		9 = WITCH_PRINCE
		10 = PRINCE
	}
	heir_female = {
		1 = WITCH
		2 = WITCH
		3 = WITCH
		4 = SORCERESS
		5 = SORCERESS
		6 = SORCERESS
		7 = HAG_PRINCESS
		8 = HAG_PRINCESS
		9 = HAG_PRINCESS
		10 = PRINCESS
	}
	trigger = {
		government = theocracy
		religion = old_gods_cult
		capital_scope = { superregion = skyrim_superregion }
	}
}

reachmen_monarchy = {
	rank = {
		1 = CHIEFDOM
		2 = CHIEFDOM
		3 = CHIEFDOM
		4 = HIGH_CHIEFDOM
		5 = HIGH_CHIEFDOM
		6 = HIGH_CHIEFDOM
		7 = KINGDOM
		8 = KINGDOM
		9 = HIGH_KINGDOM
		10 = EMPIRE
	}
	ruler_male = {
		1 = CHIEF
		2 = CHIEF
		3 = CHIEF
		4 = HIGH_CHIEF
		5 = HIGH_CHIEF
		6 = HIGH_CHIEF
		7 = KING
		8 = KING
		9 = HIGH_KING
		10 = EMPEROR
	}
	ruler_female = {
		1 = CHIEFTESS
		2 = CHIEFTESS
		3 = CHIEFTESS
		4 = HIGH_CHIEFTESS
		5 = HIGH_CHIEFTESS
		6 = HIGH_CHIEFTESS
		7 = QUEEN
		8 = QUEEN
		9 = HIGH_QUEEN
		10 = EMPRESS
	}
	consort_male = {
		1 = CHIEF
		2 = CHIEF
		3 = CHIEF
		4 = HIGH_CHIEF
		5 = HIGH_CHIEF
		6 = HIGH_CHIEF
		7 = KING
		8 = KING
		9 = HIGH_KING
		10 = EMPEROR
	}
	consort_female = {
		1 = CHIEFTESS
		2 = CHIEFTESS
		3 = CHIEFTESS
		4 = HIGH_CHIEFTESS
		5 = HIGH_CHIEFTESS
		6 = HIGH_CHIEFTESS
		7 = QUEEN
		8 = QUEEN
		9 = HIGH_QUEEN
		10 = EMPRESS
	}
	heir_male = {
		1 = HEIR
		2 = HEIR
		3 = HEIR
		4 = HEIR
		5 = HEIR
		6 = HEIR
		7 = PRINCE
		8 = PRINCE
		9 = PRINCE
		10 = PRINCE
	}
	heir_female = {
		1 = HEIR
		2 = HEIR
		3 = HEIR
		4 = HEIR
		5 = HEIR
		6 = HEIR
		7 = PRINCESS
		8 = PRINCESS
		9 = PRINCESS
		10 = PRINCESS
	}
	trigger = {
		OR = {
			government = tribal
			government = monarchy
		}
		culture_group = half_blood_cg
		NOT = { primary_culture = breton }
		NOT = { capital_scope = { superregion = skyrim_superregion } }
	}
}

reachmen_republic = {
	rank = {
		1 = CHIEFDOM
		2 = CHIEFDOM
		3 = CHIEFDOM
		4 = HIGH_CHIEFDOM
		5 = HIGH_CHIEFDOM
		6 = HIGH_CHIEFDOM
		7 = KINGDOM
		8 = KINGDOM
		9 = HIGH_KINGDOM
		10 = EMPIRE
	}
	ruler_male = {
		1 = SPEAKER
		2 = SPEAKER
		3 = SPEAKER
		4 = ELDER
		5 = ELDER
		6 = ELDER
		7 = KING
		8 = KING
		9 = HIGH_KING
		10 = EMPEROR
	}
	ruler_female = {
		1 = SPEAKER
		2 = SPEAKER
		3 = SPEAKER
		4 = ELDER
		5 = ELDER
		6 = ELDER
		7 = QUEEN
		8 = QUEEN
		9 = HIGH_QUEEN
		10 = EMPRESS
	}
	consort_male = {
		1 = SPEAKER
		2 = SPEAKER
		3 = SPEAKER
		4 = ELDER
		5 = ELDER
		6 = ELDER
		7 = KING
		8 = KING
		9 = HIGH_KING
		10 = EMPEROR
	}
	consort_female = {
		1 = SPEAKER
		2 = SPEAKER
		3 = SPEAKER
		4 = ELDER
		5 = ELDER
		6 = ELDER
		7 = QUEEN
		8 = QUEEN
		9 = HIGH_QUEEN
		10 = EMPRESS
	}
	heir_male = {
		1 = HEIR
		2 = HEIR
		3 = HEIR
		4 = HEIR
		5 = HEIR
		6 = HEIR
		7 = PRINCE
		8 = PRINCE
		9 = PRINCE
		10 = PRINCE
	}
	heir_female = {
		1 = HEIR
		2 = HEIR
		3 = HEIR
		4 = HEIR
		5 = HEIR
		6 = HEIR
		7 = PRINCESS
		8 = PRINCESS
		9 = PRINCESS
		10 = PRINCESS
	}
	trigger = {
		government = republic
		culture_group = half_blood_cg
		NOT = { primary_culture = breton }
		NOT = { capital_scope = { superregion = skyrim_superregion } }
	}
}

reachmen_theocracy = {
	rank = {
		1 = COVEN
		2 = COVEN
		3 = COVEN
		4 = CULT
		5 = CULT
		6 = CULT
		7 = WITCH_KINGDOM
		8 = WITCH_KINGDOM
		9 = WITCH_KINGDOM
		10 = EMPIRE
	}
	ruler_male = {
		1 = SORCERER
		2 = SORCERER
		3 = SORCERER
		4 = WITCH_SORCERER
		5 = WITCH_SORCERER
		6 = WITCH_SORCERER
		7 = WITCH_KING
		8 = WITCH_KING
		9 = WITCH_KING
		10 = EMPEROR
	}
	ruler_female = {
		1 = SORCERESS
		2 = SORCERESS
		3 = SORCERESS
		4 = HAG_SORCERESS
		5 = HAG_SORCERESS
		6 = HAG_SORCERESS
		7 = HAG_QUEEN
		8 = HAG_QUEEN
		9 = HAG_QUEEN
		10 = EMPRESS
	}
	consort_male = {
		1 = SORCERER
		2 = SORCERER
		3 = SORCERER
		4 = WITCH_SORCERER
		5 = WITCH_SORCERER
		6 = WITCH_SORCERER
		7 = WITCH_KING
		8 = WITCH_KING
		9 = WITCH_KING
		10 = EMPEROR
	}
	consort_female = {
		1 = SORCERESS
		2 = SORCERESS
		3 = SORCERESS
		4 = HAG_SORCERESS
		5 = HAG_SORCERESS
		6 = HAG_SORCERESS
		7 = HAG_QUEEN
		8 = HAG_QUEEN
		9 = HAG_QUEEN
		10 = EMPRESS
	}
	heir_male = {
		1 = WARLOCK
		2 = WARLOCK
		3 = WARLOCK
		4 = SORCERER
		5 = SORCERER
		6 = SORCERER
		7 = WITCH_PRINCE
		8 = WITCH_PRINCE
		9 = WITCH_PRINCE
		10 = PRINCE
	}
	heir_female = {
		1 = WITCH
		2 = WITCH
		3 = WITCH
		4 = SORCERESS
		5 = SORCERESS
		6 = SORCERESS
		7 = HAG_PRINCESS
		8 = HAG_PRINCESS
		9 = HAG_PRINCESS
		10 = PRINCESS
	}
	trigger = {
		government = theocracy
		religion = old_gods_cult
		NOT = { capital_scope = { superregion = skyrim_superregion } }
	}
}