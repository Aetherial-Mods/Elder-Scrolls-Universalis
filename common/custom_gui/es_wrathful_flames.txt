# Open / Close Buttons

custom_window = {
    name = es_wraithful_flames_window
    potential = { has_country_flag = esu_wraithful_menu_opened }
}

custom_button = {
    name = es_wraithful_flames_menu_open
    potential = { 
		NOT = { has_country_flag = esu_wraithful_menu_opened } 
		culture_group = orsimer_cg
	}
    trigger = { 
		hidden_trigger = {
			NOT = { has_country_flag = esu_wraithful_menu_opened }
		}
	}
	effect = {
		set_country_flag = esu_wraithful_menu_opened
		6959 = { owner = { save_global_event_target_as = es_wrathful_flame_shield_1_event_target } }
		1391 = { owner = { save_global_event_target_as = es_wrathful_flame_shield_2_event_target } }
		1408 = { owner = { save_global_event_target_as = es_wrathful_flame_shield_3_event_target } }
		6979 = { owner = { save_global_event_target_as = es_wrathful_flame_shield_4_event_target } }
		6074 = { owner = { save_global_event_target_as = es_wrathful_flame_shield_5_event_target } }
		5732 = { owner = { save_global_event_target_as = es_wrathful_flame_shield_6_event_target } }
		6295 = { owner = { save_global_event_target_as = es_wrathful_flame_shield_7_event_target } }
		7167 = { owner = { save_global_event_target_as = es_wrathful_flame_shield_8_event_target } }
		7288 = { owner = { save_global_event_target_as = es_wrathful_flame_shield_9_event_target } }
		2950 = { owner = { save_global_event_target_as = es_wrathful_flame_shield_10_event_target } }
		2700 = { owner = { save_global_event_target_as = es_wrathful_flame_shield_11_event_target } }
		5877 = { owner = { save_global_event_target_as = es_wrathful_flame_shield_12_event_target } }
		3412 = { owner = { save_global_event_target_as = es_wrathful_flame_shield_13_event_target } }
		961 = { owner = { save_global_event_target_as = es_wrathful_flame_shield_14_event_target } }
		4996 = { owner = { save_global_event_target_as = es_wrathful_flame_shield_15_event_target } }
		4940 = { owner = { save_global_event_target_as = es_wrathful_flame_shield_16_event_target } }
	}
	tooltip = esu_wraithful_menu_opened_tooltip
}

custom_button = {
    name = es_wraithful_flames_menu_close
    potential = { 
	}
    trigger = { 
		hidden_trigger = {
			has_country_flag = esu_wraithful_menu_opened
		}
	}
    effect = {
		clr_country_flag = esu_wraithful_menu_opened
	}
    tooltip = esu_wraithful_menu_closed_tooltip
}

# Background

custom_icon = {
    name = es_wraithful_flames_background
    potential = { }
}

custom_text_box = {
    name = es_wraithful_flames_title
    potential = { }
}

# Value display

custom_text_box = {
    name = es_wraithful_flames_value
    potential = { }
}

# Support the Order

custom_button = {
    name = es_support_wrathful_flames
    potential = { 
	}
    trigger = { 
		manpower = 10
	}
    effect = {
		add_manpower = -10
		change_variable = { which = orsimer_wrath_value value = 10 }
	}
    tooltip = es_support_wrathful_flames_tt
}

# Lit the Flames

custom_button = {
    name = es_lit_wrathful_flame_1
    potential = { 
	}
    trigger = { 
		check_variable = { which = orsimer_wrath_value value = 100 }
		6959 = { owner = { culture_group = orsimer_cg } }
	}
    effect = {
		add_country_modifier = { name = "es_wrathful_flame_1_event_modifier" duration = 3650 }
		change_variable = { which = orsimer_wrath_value value = -100 }
	}
	frame = { 
        number = 1
        trigger = { has_country_modifier = es_wrathful_flame_1_event_modifier }
    }
	frame = { 
        number = 2
        trigger = { NOT = { has_country_modifier = es_wrathful_flame_1_event_modifier } }
    }
    tooltip = es_lit_wrathful_flame_1_tt
}

custom_shield = {
    name = es_lit_wrathful_flame_shield_1
    potential = { 
	}
    trigger = { 
	}
    effect = { }
    global_event_target = es_wrathful_flame_shield_1_event_target
    open_country = yes
}

custom_text_box = {
    name = es_lit_wrathful_flame_textbox_1
    potential = { }
}

#####

custom_button = {
    name = es_lit_wrathful_flame_2
    potential = { 
	}
    trigger = { 
		check_variable = { which = orsimer_wrath_value value = 100 }
		1391 = { owner = { culture_group = orsimer_cg } }
	}
    effect = {
		add_country_modifier = { name = "es_wrathful_flame_2_event_modifier" duration = 3650 }
		change_variable = { which = orsimer_wrath_value value = -100 }
	}
	frame = { 
        number = 1
        trigger = { has_country_modifier = es_wrathful_flame_2_event_modifier }
    }
	frame = { 
        number = 2
        trigger = { NOT = { has_country_modifier = es_wrathful_flame_2_event_modifier } }
    }
    tooltip = es_lit_wrathful_flame_2_tt
}

custom_shield = {
    name = es_lit_wrathful_flame_shield_2
    potential = { 
	}
    trigger = { 
	}
    effect = { }
    global_event_target = es_wrathful_flame_shield_2_event_target
    open_country = yes
}

custom_text_box = {
    name = es_lit_wrathful_flame_textbox_2
    potential = { }
}

#####

custom_button = {
    name = es_lit_wrathful_flame_3
    potential = { 
	}
    trigger = { 
		check_variable = { which = orsimer_wrath_value value = 100 }
		1408 = { owner = { culture_group = orsimer_cg } }
	}
    effect = {
		add_country_modifier = { name = "es_wrathful_flame_3_event_modifier" duration = 3650 }
		change_variable = { which = orsimer_wrath_value value = -100 }
	}
	frame = { 
        number = 1
        trigger = { has_country_modifier = es_wrathful_flame_3_event_modifier }
    }
	frame = { 
        number = 2
        trigger = { NOT = { has_country_modifier = es_wrathful_flame_3_event_modifier } }
    }
    tooltip = es_lit_wrathful_flame_3_tt
}

custom_shield = {
    name = es_lit_wrathful_flame_shield_3
    potential = { 
	}
    trigger = { 
	}
    effect = { }
    global_event_target = es_wrathful_flame_shield_3_event_target
    open_country = yes
}

custom_text_box = {
    name = es_lit_wrathful_flame_textbox_3
    potential = { }
}

#####

custom_button = {
    name = es_lit_wrathful_flame_4
    potential = { 
	}
    trigger = { 
		check_variable = { which = orsimer_wrath_value value = 100 }
		6979 = { owner = { culture_group = orsimer_cg } }
	}
    effect = {
		add_country_modifier = { name = "es_wrathful_flame_4_event_modifier" duration = 3650 }
		change_variable = { which = orsimer_wrath_value value = -100 }
	}
	frame = { 
        number = 1
        trigger = { has_country_modifier = es_wrathful_flame_4_event_modifier }
    }
	frame = { 
        number = 2
        trigger = { NOT = { has_country_modifier = es_wrathful_flame_4_event_modifier } }
    }
    tooltip = es_lit_wrathful_flame_4_tt
}

custom_shield = {
    name = es_lit_wrathful_flame_shield_4
    potential = { 
	}
    trigger = { 
	}
    effect = { }
    global_event_target = es_wrathful_flame_shield_4_event_target
    open_country = yes
}

custom_text_box = {
    name = es_lit_wrathful_flame_textbox_4
    potential = { }
}

#####

custom_button = {
    name = es_lit_wrathful_flame_5
    potential = { 
	}
    trigger = { 
		check_variable = { which = orsimer_wrath_value value = 100 }
		6074 = { owner = { culture_group = orsimer_cg } }
	}
    effect = {
		add_country_modifier = { name = "es_wrathful_flame_5_event_modifier" duration = 3650 }
		change_variable = { which = orsimer_wrath_value value = -100 }
	}
	frame = { 
        number = 1
        trigger = { has_country_modifier = es_wrathful_flame_5_event_modifier }
    }
	frame = { 
        number = 2
        trigger = { NOT = { has_country_modifier = es_wrathful_flame_5_event_modifier } }
    }
    tooltip = es_lit_wrathful_flame_5_tt
}

custom_shield = {
    name = es_lit_wrathful_flame_shield_5
    potential = { 
	}
    trigger = { 
	}
    effect = { }
    global_event_target = es_wrathful_flame_shield_5_event_target
    open_country = yes
}

custom_text_box = {
    name = es_lit_wrathful_flame_textbox_5
    potential = { }
}

#####

custom_button = {
    name = es_lit_wrathful_flame_6
    potential = { 
	}
    trigger = { 
		check_variable = { which = orsimer_wrath_value value = 100 }
		5732 = { owner = { culture_group = orsimer_cg } }
	}
    effect = {
		add_country_modifier = { name = "es_wrathful_flame_6_event_modifier" duration = 3650 }
		change_variable = { which = orsimer_wrath_value value = -100 }
	}
	frame = { 
        number = 1
        trigger = { has_country_modifier = es_wrathful_flame_6_event_modifier }
    }
	frame = { 
        number = 2
        trigger = { NOT = { has_country_modifier = es_wrathful_flame_6_event_modifier } }
    }
    tooltip = es_lit_wrathful_flame_6_tt
}

custom_shield = {
    name = es_lit_wrathful_flame_shield_6
    potential = { 
	}
    trigger = { 
	}
    effect = { }
    global_event_target = es_wrathful_flame_shield_6_event_target
    open_country = yes
}

custom_text_box = {
    name = es_lit_wrathful_flame_textbox_6
    potential = { }
}

#####

custom_button = {
    name = es_lit_wrathful_flame_7
    potential = { 
	}
    trigger = { 
		check_variable = { which = orsimer_wrath_value value = 100 }
		6295 = { owner = { culture_group = orsimer_cg } }
	}
    effect = {
		add_country_modifier = { name = "es_wrathful_flame_7_event_modifier" duration = 3650 }
		change_variable = { which = orsimer_wrath_value value = -100 }
	}
	frame = { 
        number = 1
        trigger = { has_country_modifier = es_wrathful_flame_7_event_modifier }
    }
	frame = { 
        number = 2
        trigger = { NOT = { has_country_modifier = es_wrathful_flame_7_event_modifier } }
    }
    tooltip = es_lit_wrathful_flame_7_tt
}

custom_shield = {
    name = es_lit_wrathful_flame_shield_7
    potential = { 
	}
    trigger = { 
	}
    effect = { }
    global_event_target = es_wrathful_flame_shield_7_event_target
    open_country = yes
}

custom_text_box = {
    name = es_lit_wrathful_flame_textbox_7
    potential = { }
}

#####

custom_button = {
    name = es_lit_wrathful_flame_8
    potential = { 
	}
    trigger = { 
		check_variable = { which = orsimer_wrath_value value = 100 }
		7163 = { owner = { culture_group = orsimer_cg } }
	}
    effect = {
		add_country_modifier = { name = "es_wrathful_flame_8_event_modifier" duration = 3650 }
		change_variable = { which = orsimer_wrath_value value = -100 }
	}
	frame = { 
        number = 1
        trigger = { has_country_modifier = es_wrathful_flame_8_event_modifier }
    }
	frame = { 
        number = 2
        trigger = { NOT = { has_country_modifier = es_wrathful_flame_8_event_modifier } }
    }
    tooltip = es_lit_wrathful_flame_8_tt
}

custom_shield = {
    name = es_lit_wrathful_flame_shield_8
    potential = { 
	}
    trigger = { 
	}
    effect = { }
    global_event_target = es_wrathful_flame_shield_8_event_target
    open_country = yes
}

custom_text_box = {
    name = es_lit_wrathful_flame_textbox_8
    potential = { }
}

#####

custom_button = {
    name = es_lit_wrathful_flame_9
    potential = { 
	}
    trigger = { 
		check_variable = { which = orsimer_wrath_value value = 100 }
		7163 = { owner = { culture_group = orsimer_cg } }
	}
    effect = {
		add_country_modifier = { name = "es_wrathful_flame_9_event_modifier" duration = 3650 }
		change_variable = { which = orsimer_wrath_value value = -100 }
	}
	frame = { 
        number = 1
        trigger = { has_country_modifier = es_wrathful_flame_9_event_modifier }
    }
	frame = { 
        number = 2
        trigger = { NOT = { has_country_modifier = es_wrathful_flame_9_event_modifier } }
    }
    tooltip = es_lit_wrathful_flame_9_tt
}

custom_shield = {
    name = es_lit_wrathful_flame_shield_9
    potential = { 
	}
    trigger = { 
	}
    effect = { }
    global_event_target = es_wrathful_flame_shield_9_event_target
    open_country = yes
}

custom_text_box = {
    name = es_lit_wrathful_flame_textbox_9
    potential = { }
}

#####

custom_button = {
    name = es_lit_wrathful_flame_10
    potential = { 
	}
    trigger = { 
		check_variable = { which = orsimer_wrath_value value = 100 }
		7163 = { owner = { culture_group = orsimer_cg } }
	}
    effect = {
		add_country_modifier = { name = "es_wrathful_flame_10_event_modifier" duration = 3650 }
		change_variable = { which = orsimer_wrath_value value = -100 }
	}
	frame = { 
        number = 1
        trigger = { has_country_modifier = es_wrathful_flame_10_event_modifier }
    }
	frame = { 
        number = 2
        trigger = { NOT = { has_country_modifier = es_wrathful_flame_10_event_modifier } }
    }
    tooltip = es_lit_wrathful_flame_10_tt
}

custom_shield = {
    name = es_lit_wrathful_flame_shield_10
    potential = { 
	}
    trigger = { 
	}
    effect = { }
    global_event_target = es_wrathful_flame_shield_10_event_target
    open_country = yes
}

custom_text_box = {
    name = es_lit_wrathful_flame_textbox_10
    potential = { }
}

#####

custom_button = {
    name = es_lit_wrathful_flame_11
    potential = { 
	}
    trigger = { 
		check_variable = { which = orsimer_wrath_value value = 100 }
		2700 = { owner = { culture_group = orsimer_cg } }
	}
    effect = {
		add_country_modifier = { name = "es_wrathful_flame_11_event_modifier" duration = 3650 }
		change_variable = { which = orsimer_wrath_value value = -100 }
	}
	frame = { 
        number = 1
        trigger = { has_country_modifier = es_wrathful_flame_11_event_modifier }
    }
	frame = { 
        number = 2
        trigger = { NOT = { has_country_modifier = es_wrathful_flame_11_event_modifier } }
    }
    tooltip = es_lit_wrathful_flame_11_tt
}

custom_shield = {
    name = es_lit_wrathful_flame_shield_11
    potential = { 
	}
    trigger = { 
	}
    effect = { }
    global_event_target = es_wrathful_flame_shield_11_event_target
    open_country = yes
}

custom_text_box = {
    name = es_lit_wrathful_flame_textbox_11
    potential = { }
}

#####

custom_button = {
    name = es_lit_wrathful_flame_12
    potential = { 
	}
    trigger = { 
		check_variable = { which = orsimer_wrath_value value = 100 }
		5877 = { owner = { culture_group = orsimer_cg } }
	}
    effect = {
		add_country_modifier = { name = "es_wrathful_flame_12_event_modifier" duration = 3650 }
		change_variable = { which = orsimer_wrath_value value = -100 }
	}
	frame = { 
        number = 1
        trigger = { has_country_modifier = es_wrathful_flame_12_event_modifier }
    }
	frame = { 
        number = 2
        trigger = { NOT = { has_country_modifier = es_wrathful_flame_12_event_modifier } }
    }
    tooltip = es_lit_wrathful_flame_12_tt
}

custom_shield = {
    name = es_lit_wrathful_flame_shield_12
    potential = { 
	}
    trigger = { 
	}
    effect = { }
    global_event_target = es_wrathful_flame_shield_12_event_target
    open_country = yes
}

custom_text_box = {
    name = es_lit_wrathful_flame_textbox_12
    potential = { }
}

#####

custom_button = {
    name = es_lit_wrathful_flame_13
    potential = { 
	}
    trigger = { 
		check_variable = { which = orsimer_wrath_value value = 100 }
		3412 = { owner = { culture_group = orsimer_cg } }
	}
    effect = {
		add_country_modifier = { name = "es_wrathful_flame_13_event_modifier" duration = 3650 }
		change_variable = { which = orsimer_wrath_value value = -100 }
	}
	frame = { 
        number = 1
        trigger = { has_country_modifier = es_wrathful_flame_13_event_modifier }
    }
	frame = { 
        number = 2
        trigger = { NOT = { has_country_modifier = es_wrathful_flame_13_event_modifier } }
    }
    tooltip = es_lit_wrathful_flame_13_tt
}

custom_shield = {
    name = es_lit_wrathful_flame_shield_13
    potential = { 
	}
    trigger = { 
	}
    effect = { }
    global_event_target = es_wrathful_flame_shield_13_event_target
    open_country = yes
}

custom_text_box = {
    name = es_lit_wrathful_flame_textbox_13
    potential = { }
}

#####

custom_button = {
    name = es_lit_wrathful_flame_14
    potential = { 
	}
    trigger = { 
		check_variable = { which = orsimer_wrath_value value = 100 }
		961 = { owner = { culture_group = orsimer_cg } }
	}
    effect = {
		add_country_modifier = { name = "es_wrathful_flame_14_event_modifier" duration = 3650 }
		change_variable = { which = orsimer_wrath_value value = -100 }
	}
	frame = { 
        number = 1
        trigger = { has_country_modifier = es_wrathful_flame_14_event_modifier }
    }
	frame = { 
        number = 2
        trigger = { NOT = { has_country_modifier = es_wrathful_flame_14_event_modifier } }
    }
    tooltip = es_lit_wrathful_flame_14_tt
}

custom_shield = {
    name = es_lit_wrathful_flame_shield_14
    potential = { 
	}
    trigger = { 
	}
    effect = { }
    global_event_target = es_wrathful_flame_shield_14_event_target
    open_country = yes
}

custom_text_box = {
    name = es_lit_wrathful_flame_textbox_14
    potential = { }
}

#####

custom_button = {
    name = es_lit_wrathful_flame_15
    potential = { 
	}
    trigger = { 
		check_variable = { which = orsimer_wrath_value value = 100 }
		4996 = { owner = { culture_group = orsimer_cg } }
	}
    effect = {
		add_country_modifier = { name = "es_wrathful_flame_15_event_modifier" duration = 3650 }
		change_variable = { which = orsimer_wrath_value value = -100 }
	}
	frame = { 
        number = 1
        trigger = { has_country_modifier = es_wrathful_flame_15_event_modifier }
    }
	frame = { 
        number = 2
        trigger = { NOT = { has_country_modifier = es_wrathful_flame_15_event_modifier } }
    }
    tooltip = es_lit_wrathful_flame_15_tt
}

custom_shield = {
    name = es_lit_wrathful_flame_shield_15
    potential = { 
	}
    trigger = { 
	}
    effect = { }
    global_event_target = es_wrathful_flame_shield_15_event_target
    open_country = yes
}

custom_text_box = {
    name = es_lit_wrathful_flame_textbox_15
    potential = { }
}

#####

custom_button = {
    name = es_lit_wrathful_flame_16
    potential = { 
	}
    trigger = { 
		check_variable = { which = orsimer_wrath_value value = 100 }
		4940 = { owner = { culture_group = orsimer_cg } }
	}
    effect = {
		add_country_modifier = { name = "es_wrathful_flame_16_event_modifier" duration = 3650 }
		change_variable = { which = orsimer_wrath_value value = -100 }
	}
	frame = { 
        number = 1
        trigger = { has_country_modifier = es_wrathful_flame_16_event_modifier }
    }
	frame = { 
        number = 2
        trigger = { NOT = { has_country_modifier = es_wrathful_flame_16_event_modifier } }
    }
    tooltip = es_lit_wrathful_flame_16_tt
}

custom_shield = {
    name = es_lit_wrathful_flame_shield_16
    potential = { 
	}
    trigger = { 
	}
    effect = { }
    global_event_target = es_wrathful_flame_shield_16_event_target
    open_country = yes
}

custom_text_box = {
    name = es_lit_wrathful_flame_textbox_16
    potential = { }
}

# Descriptions of order and mechanics

custom_text_box = {
    name = es_wrathful_flame_description
    potential = { }
}

custom_text_box = {
    name = es_wrathful_flame_mechanic_description
    potential = { }
}