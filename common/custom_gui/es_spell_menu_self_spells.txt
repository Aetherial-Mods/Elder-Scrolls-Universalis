#####################
### Racial Powers ###
#####################

# Adrenaline Rush
custom_button = {
	name = es_racial_power_button_adrenaline_rush	#Must match a scripted guiButtonType in a .gui file
	potential = {
		FROM = {
			has_country_flag = es_spell_menu_self_open
			es_spell_menu_self_submenu_open = no
			culture_group = yokudo_redguard_cg
		}
	}
	#Determines when the button is visible
	trigger = {
		if = {
			limit = {
				has_ruler_modifier = racial_power_cooldown
			}
			custom_trigger_tooltip = {
				tooltip = racial_power_cooldown_tt
				always = no
			}	
		}
	}	#Determines when the button is clickable
	effect = {
		cast_ability_option = {
			which = adrenaline_rush_effect
			effect_duration = 180
			power_cooldown = 365
			spell_caster = ROOT
			spell_target = ROOT
		}
	}
	#Effect
	tooltip = es_spell_adrenaline_rush_tt	#Can use customizable localisation
}

# Berserker Rage
custom_button = {
	name = es_racial_power_button_berserker_rage	#Must match a scripted guiButtonType in a .gui file
	potential = {
		FROM = {
			has_country_flag = es_spell_menu_self_open
			es_spell_menu_self_submenu_open = no
			culture_group = orsimer_cg
		}
	}
	#Determines when the button is visible
	trigger = {
		if = {
			limit = {
				has_ruler_modifier = racial_power_cooldown
			}
			custom_trigger_tooltip = {
				tooltip = racial_power_cooldown_tt
				always = no
			}	
		}
	}	#Determines when the button is clickable
	effect = {
		cast_ability_option = {
			which = berserker_rage_effect
			effect_duration = 45
			power_cooldown = 365
			spell_caster = ROOT
			spell_target = ROOT
		}
	}
	#Effect
	tooltip = es_spell_berserker_rage_tt	#Can use customizable localisation
}

# Command Animal
custom_button = {
	name = es_racial_power_button_command_animal	#Must match a scripted guiButtonType in a .gui file
	potential = {
		FROM = {
			has_country_flag = es_spell_menu_self_open
			es_spell_menu_self_submenu_open = no
			culture_group = bosmer_cg
		}
	}
	#Determines when the button is visible
	trigger = {
		if = {
			limit = {
				has_ruler_modifier = racial_power_cooldown
			}
			custom_trigger_tooltip = {
				tooltip = racial_power_cooldown_tt
				always = no
			}	
		}
	}	#Determines when the button is clickable
	effect = {
		cast_ability_option = {
			which = command_animal_effect
			effect_duration = 365
			power_cooldown = 365
			spell_caster = ROOT
			spell_target = ROOT
		}
	}
	#Effect
	tooltip = es_spell_command_animal_tt	#Can use customizable localisation
}

# Dragonskin
custom_button = {
	name = es_racial_power_button_dragonskin	#Must match a scripted guiButtonType in a .gui file
	potential = {
		FROM = {
			has_country_flag = es_spell_menu_self_open
			es_spell_menu_self_submenu_open = no
			culture_group = half_blood_cg
		}
	}
	#Determines when the button is visible
	trigger = {
		if = {
			limit = {
				has_ruler_modifier = racial_power_cooldown
			}
			custom_trigger_tooltip = {
				tooltip = racial_power_cooldown_tt
				always = no
			}	
		}
	}	#Determines when the button is clickable
	effect = {
		cast_ability_option = {
			which = dragonskin_effect
			effect_duration = 365
			power_cooldown = 365
			spell_caster = ROOT
			spell_target = ROOT
		}
	}
	#Effect
	tooltip = es_spell_dragonskin_tt	#Can use customizable localisation
}

# Highborn
custom_button = {
	name = es_racial_power_button_highborn	#Must match a scripted guiButtonType in a .gui file
	potential = {
		FROM = {
			has_country_flag = es_spell_menu_self_open
			es_spell_menu_self_submenu_open = no
			culture_group = high_elves_cg
		}
	}
	#Determines when the button is visible
	trigger = {
		if = {
			limit = {
				has_ruler_modifier = racial_power_cooldown
			}
			custom_trigger_tooltip = {
				tooltip = racial_power_cooldown_tt
				always = no
			}	
		}
	}	#Determines when the button is clickable
	effect = {
		cast_ability_option = {
			which = highborn_effect
			power_cooldown = 365
			spell_caster = ROOT
			spell_target = ROOT
		}
	}
	#Effect
	tooltip = es_spell_highborn_tt	#Can use customizable localisation
}

# Histskin
custom_button = {
	name = es_racial_power_button_histskin	#Must match a scripted guiButtonType in a .gui file
	potential = {
		FROM = {
			has_country_flag = es_spell_menu_self_open
			es_spell_menu_self_submenu_open = no
			culture_group = marsh_cg
		}
	}
	#Determines when the button is visible
	trigger = {
		if = {
			limit = {
				has_ruler_modifier = racial_power_cooldown
			}
			custom_trigger_tooltip = {
				tooltip = racial_power_cooldown_tt
				always = no
			}	
		}
	}	#Determines when the button is clickable
	effect = {
		cast_ability_option = {
			which = histskin_effect
			effect_duration = 365
			power_cooldown = 365
			spell_caster = ROOT
			spell_target = ROOT
		}
	}
	#Effect
	tooltip = es_spell_histskin_tt	#Can use customizable localisation
}

# Night Eye
custom_button = {
	name = es_racial_power_button_night_eye	#Must match a scripted guiButtonType in a .gui file
	potential = {
		FROM = {
			has_country_flag = es_spell_menu_self_open
			es_spell_menu_self_submenu_open = no
			culture_group = khajiiti_cg
		}
	}
	#Determines when the button is visible
	trigger = {
		if = {
			limit = {
				has_ruler_modifier = racial_power_cooldown
			}
			custom_trigger_tooltip = {
				tooltip = racial_power_cooldown_tt
				always = no
			}	
		}
	}	#Determines when the button is clickable
	effect = {
		cast_ability_option = {
			which = night_eye_effect
			# 6 months
			effect_duration = 6
			power_cooldown = 365
			spell_caster = ROOT
			spell_target = ROOT
		}
	}
	#Effect
	tooltip = es_spell_night_eye_tt	#Can use customizable localisation
}

#########################
### Alteration Spells ###
#########################

# Candlelight
custom_button = {
	name = es_spell_button_candlelight	#Must match a scripted guiButtonType in a .gui file
	potential = {
		has_country_flag = es_spell_menu_self_open
		has_country_flag = es_spell_menu_self_alteration_open
	}
	#Determines when the button is visible
	trigger = {
		hidden_trigger = {
			meets_mana_requirements = {
				cost = 200
				mana_type = magicka
			}
		}
	}
	#Determines when the button is clickable
	effect = {
        cast_ability_option = {
			which = candlelight_effect
			effect_duration = 365
			cost = 200
			mana_type = magicka
			spell_caster = ROOT
			spell_target = ROOT
		}
	}
	#Effect
	tooltip = es_spell_candlelight_tt	#Can use customizable localisation
}

# Detect Life
custom_button = {
	name = es_spell_button_detect_life	#Must match a scripted guiButtonType in a .gui file
	potential = {
		has_country_flag = es_spell_menu_self_open
		has_country_flag = es_spell_menu_self_alteration_open
	}
	#Determines when the button is visible
	trigger = {
		hidden_trigger = {
			meets_mana_requirements = {
				cost = 200
				mana_type = magicka
			}
		}
	}
	#Determines when the button is clickable
	effect = {
        cast_ability_option = {
			which = detect_life_effect
			effect_duration = 365
			cost = 200
			mana_type = magicka
			spell_caster = ROOT
			spell_target = ROOT
		}
	}
	#Effect
	tooltip = es_spell_detect_life_tt	#Can use customizable localisation
}

# Equilibrium
custom_button = {
	name = es_spell_button_equilibrium	#Must match a scripted guiButtonType in a .gui file
	potential = {
		has_country_flag = es_spell_menu_self_open
		has_country_flag = es_spell_menu_self_alteration_open
	}
	#Determines when the button is visible
	trigger = {
		if = {
			limit = { NOT = { manpower = 15 } }
			manpower = 15
		}
		hidden_trigger = {
			meets_mana_requirements = {
				cost = 200
				mana_type = magicka
			}
		}
	}
	#Determines when the button is clickable
	effect = {
        cast_ability_option = {
			which = equilibrium_effect
			cost = 200
			mana_type = magicka
			spell_caster = ROOT
			spell_target = ROOT
		}
	}
	#Effect
	tooltip = es_spell_equilibrium_tt	#Can use customizable localisation
}

# Oakflesh
custom_button = {
	name = es_spell_button_oakflesh	#Must match a scripted guiButtonType in a .gui file
	potential = {
		has_country_flag = es_spell_menu_self_open
		has_country_flag = es_spell_menu_self_alteration_open
	}
	#Determines when the button is visible
	trigger = {
		hidden_trigger = {
			meets_mana_requirements = {
				cost = 200
				mana_type = magicka
			}
		}
	}
	#Determines when the button is clickable
	effect = {
        cast_ability_option = {
			which = oakflesh_effect
			effect_duration = 365
			cost = 200
			mana_type = magicka
			spell_caster = ROOT
			spell_target = ROOT
		}
	}
	#Effect
	tooltip = es_spell_oakflesh_tt	#Can use customizable localisation
}

# Sea Stride
custom_button = {
	name = es_spell_button_sea_stride	#Must match a scripted guiButtonType in a .gui file
	potential = {
		has_country_flag = es_spell_menu_self_open
		has_country_flag = es_spell_menu_self_alteration_open
	}
	#Determines when the button is visible
	trigger = {
		hidden_trigger = {
			meets_mana_requirements = {
				cost = 200
				mana_type = magicka
			}
		}
	}
	#Determines when the button is clickable
	effect = {
        cast_ability_option = {
			which = sea_stride_effect
			effect_duration = 365
			cost = 200
			mana_type = magicka
			spell_caster = ROOT
			spell_target = ROOT
		}
	}
	#Effect
	tooltip = es_spell_sea_stride_tt	#Can use customizable localisation
}

# Transmutation
custom_button = {
	name = es_spell_button_transmutation	#Must match a scripted guiButtonType in a .gui file
	potential = {
		has_country_flag = es_spell_menu_self_open
		has_country_flag = es_spell_menu_self_alteration_open
	}
	#Determines when the button is visible
	trigger = {
		hidden_trigger = {
			meets_mana_requirements = {
				cost = 200
				mana_type = magicka
			}
		}
	}
	#Determines when the button is clickable
	effect = {
        cast_ability_option = {
			which = transmutation_effect
			cost = 200
			mana_type = magicka
			spell_caster = ROOT
			spell_target = ROOT
		}
	}
	#Effect
	tooltip = es_spell_transmutation_tt	#Can use customizable localisation
}

# Water Breathing
custom_button = {
	name = es_spell_button_water_breathing	#Must match a scripted guiButtonType in a .gui file
	potential = {
		has_country_flag = es_spell_menu_self_open
		has_country_flag = es_spell_menu_self_alteration_open
	}
	#Determines when the button is visible
	trigger = {
		hidden_trigger = {
			meets_mana_requirements = {
				cost = 200
				mana_type = magicka
			}
		}
	}
	#Determines when the button is clickable
	effect = {
        cast_ability_option = {
			which = water_breathing_effect
			effect_duration = 365
			cost = 200
			mana_type = magicka
			spell_caster = ROOT
			spell_target = ROOT
		}
	}
	#Effect
	tooltip = es_spell_water_breathing_tt	#Can use customizable localisation
}

##########################
### Conjuration Spells ###
##########################

# Bound Armour
custom_button = {
	name = es_spell_button_bound_armour	#Must match a scripted guiButtonType in a .gui file
	potential = {
		has_country_flag = es_spell_menu_self_open
		has_country_flag = es_spell_menu_self_conjuration_open
	}
	#Determines when the button is visible
	trigger = {
		hidden_trigger = {
			meets_mana_requirements = {
				cost = 200
				mana_type = magicka
			}
		}
	}
	#Determines when the button is clickable
	effect = {
        cast_ability_option = {
			which = bound_armour_effect
			effect_duration = 365
			cost = 200
			mana_type = magicka
			spell_caster = ROOT
			spell_target = ROOT
		}
	}
	#Effect
	tooltip = es_spell_bound_armour_tt	#Can use customizable localisation
}

# Bound Bow
custom_button = {
	name = es_spell_button_bound_bow	#Must match a scripted guiButtonType in a .gui file
	potential = {
		has_country_flag = es_spell_menu_self_open
		has_country_flag = es_spell_menu_self_conjuration_open
	}
	#Determines when the button is visible
	trigger = {
		hidden_trigger = {
			meets_mana_requirements = {
				cost = 200
				mana_type = magicka
			}
		}
	}
	#Determines when the button is clickable
	effect = {
        cast_ability_option = {
			which = bound_bow_effect
			effect_duration = 365
			cost = 200
			mana_type = magicka
			spell_caster = ROOT
			spell_target = ROOT
		}
	}
	#Effect
	tooltip = es_spell_bound_bow_tt	#Can use customizable localisation
}

# Bound Sword
custom_button = {
	name = es_spell_button_bound_sword	#Must match a scripted guiButtonType in a .gui file
	potential = {
		has_country_flag = es_spell_menu_self_open
		has_country_flag = es_spell_menu_self_conjuration_open
	}
	#Determines when the button is visible
	trigger = {
		hidden_trigger = {
			meets_mana_requirements = {
				cost = 200
				mana_type = magicka
			}
		}
	}
	#Determines when the button is clickable
	effect = {
        cast_ability_option = {
			which = bound_sword_effect
			effect_duration = 365
			cost = 200
			mana_type = magicka
			spell_caster = ROOT
			spell_target = ROOT
		}
	}
	#Effect
	tooltip = es_spell_bound_sword_tt	#Can use customizable localisation
}

# Conjure Atronach
custom_button = {
	name = es_spell_button_conjure_atronach	#Must match a scripted guiButtonType in a .gui file
	potential = {
		has_country_flag = es_spell_menu_self_open
		has_country_flag = es_spell_menu_self_conjuration_open
	}
	#Determines when the button is visible
	trigger = {
		hidden_trigger = {
			meets_mana_requirements = {
				cost = 200
				mana_type = magicka
			}
		}
	}
	#Determines when the button is clickable
	effect = {
        cast_ability_option = {
			which = conjure_atronach_effect
			effect_duration = 365
			cost = 200
			mana_type = magicka
			spell_caster = ROOT
			spell_target = ROOT
		}
	}
	#Effect
	tooltip = es_spell_conjure_atronach_tt	#Can use customizable localisation
}

# Conjure Dremora Lord
custom_button = {
	name = es_spell_button_conjure_dremora_lord	#Must match a scripted guiButtonType in a .gui file
	potential = {
		has_country_flag = es_spell_menu_self_open
		has_country_flag = es_spell_menu_self_conjuration_open
	}
	#Determines when the button is visible
	trigger = {
		hidden_trigger = {
			meets_mana_requirements = {
				cost = 200
				mana_type = magicka
			}
		}
	}
	#Determines when the button is clickable
	effect = {
        cast_ability_option = {
			which = conjure_dremora_lord_effect
			effect_duration = 365
			cost = 200
			mana_type = magicka
			spell_caster = ROOT
			spell_target = ROOT
		}
	}
	#Effect
	tooltip = es_spell_conjure_dremora_lord_tt	#Can use customizable localisation
}

# Dead Thrall
custom_button = {
	name = es_spell_button_dead_thrall	#Must match a scripted guiButtonType in a .gui file
	potential = {
		has_country_flag = es_spell_menu_self_open
		has_country_flag = es_spell_menu_self_conjuration_open
	}
	#Determines when the button is visible
	trigger = {
		hidden_trigger = {
			meets_mana_requirements = {
				cost = 200
				mana_type = magicka
			}
		}
	}
	#Determines when the button is clickable
	effect = {
        cast_ability_option = {
			which = dead_thrall_effect
			effect_duration = 365
			cost = 200
			mana_type = magicka
			spell_caster = ROOT
			spell_target = ROOT
		}
	}
	#Effect
	tooltip = es_spell_dead_thrall_tt	#Can use customizable localisation
}

# Dead Thrall
custom_button = {
	name = es_spell_button_reanimate_corpses	#Must match a scripted guiButtonType in a .gui file
	potential = {
		has_country_flag = es_spell_menu_self_open
		has_country_flag = es_spell_menu_self_conjuration_open
	}
	#Determines when the button is visible
	trigger = {
		hidden_trigger = {
			meets_mana_requirements = {
				cost = 200
				mana_type = magicka
			}
		}
	}
	#Determines when the button is clickable
	effect = {
        cast_ability_option = {
			which = reanimate_corpses_effect
			cost = 200
			mana_type = magicka
			spell_caster = ROOT
			spell_target = ROOT
		}
	}
	#Effect
	tooltip = es_spell_reanimate_corpses_tt	#Can use customizable localisation
}

##########################
### Destruction Spells ###
##########################

# Flame Cloak
custom_button = {
	name = es_spell_button_flame_cloak	#Must match a scripted guiButtonType in a .gui file
	potential = {
		has_country_flag = es_spell_menu_self_open
		has_country_flag = es_spell_menu_self_destruction_open
	}
	#Determines when the button is visible
	trigger = {
		hidden_trigger = {
			meets_mana_requirements = {
				cost = 200
				mana_type = magicka
			}
		}
	}
	#Determines when the button is clickable
	effect = {
        cast_ability_option = {
			which = flame_cloak_effect
			effect_duration = 365
			cost = 200
			mana_type = magicka
			spell_caster = ROOT
			spell_target = ROOT
		}
	}
	#Effect
	tooltip = es_spell_flame_cloak_tt	#Can use customizable localisation
}

# Wall of Flames
custom_button = {
	name = es_spell_button_wall_of_flames	#Must match a scripted guiButtonType in a .gui file
	potential = {
		has_country_flag = es_spell_menu_self_open
		has_country_flag = es_spell_menu_self_destruction_open
	}
	#Determines when the button is visible
	trigger = {
		hidden_trigger = {
			meets_mana_requirements = {
				cost = 200
				mana_type = magicka
			}
		}
	}
	#Determines when the button is clickable
	effect = {
        cast_ability_option = {
			which = wall_of_flames_effect
			effect_duration = 180
			cost = 200
			mana_type = magicka
			spell_caster = ROOT
			spell_target = ROOT
		}
	}
	#Effect
	tooltip = es_spell_wall_of_flames_tt	#Can use customizable localisation
}

# Fire Storm
custom_button = {
	name = es_spell_button_fire_storm	#Must match a scripted guiButtonType in a .gui file
	potential = {
		has_country_flag = es_spell_menu_self_open
		has_country_flag = es_spell_menu_self_destruction_open
	}
	#Determines when the button is visible
	trigger = {
		hidden_trigger = {
			meets_mana_requirements = {
				cost = 200
				mana_type = magicka
			}
		}
	}
	#Determines when the button is clickable
	effect = {
        cast_ability_option = {
			which = fire_storm_effect
			effect_duration = 180
			cost = 200
			mana_type = magicka
			spell_caster = ROOT
			spell_target = ROOT
		}
	}
	#Effect
	tooltip = es_spell_fire_storm_tt	#Can use customizable localisation
}

# Frost Cloak
custom_button = {
	name = es_spell_button_frost_cloak	#Must match a scripted guiButtonType in a .gui file
	potential = {
		has_country_flag = es_spell_menu_self_open
		has_country_flag = es_spell_menu_self_destruction_open
	}
	#Determines when the button is visible
	trigger = {
		hidden_trigger = {
			meets_mana_requirements = {
				cost = 200
				mana_type = magicka
			}
		}
	}
	#Determines when the button is clickable
	effect = {
		cast_ability_option = {
			which = frost_cloak_effect
			effect_duration = 365
			cost = 200
			mana_type = magicka
			spell_caster = ROOT
			spell_target = ROOT
		}
	}
	#Effect
	tooltip = es_spell_frost_cloak_tt	#Can use customizable localisation
}

# Wall of Frost
custom_button = {
	name = es_spell_button_wall_of_frost	#Must match a scripted guiButtonType in a .gui file
	potential = {
		has_country_flag = es_spell_menu_self_open
		has_country_flag = es_spell_menu_self_destruction_open
	}
	#Determines when the button is visible
	trigger = {
		hidden_trigger = {
			meets_mana_requirements = {
				cost = 200
				mana_type = magicka
			}
		}
	}
	#Determines when the button is clickable
	effect = {
		cast_ability_option = {
			which = wall_of_frost_effect
			effect_duration = 180
			cost = 200
			mana_type = magicka
			spell_caster = ROOT
			spell_target = ROOT
		}
	}
	#Effect
	tooltip = es_spell_wall_of_frost_tt	#Can use customizable localisation
}

# Blizzard
custom_button = {
	name = es_spell_button_blizzard	#Must match a scripted guiButtonType in a .gui file
	potential = {
		has_country_flag = es_spell_menu_self_open
		has_country_flag = es_spell_menu_self_destruction_open
	}
	#Determines when the button is visible
	trigger = {
		hidden_trigger = {
			meets_mana_requirements = {
				cost = 200
				mana_type = magicka
			}
		}
	}
	#Determines when the button is clickable
	effect = {
        cast_ability_option = {
			which = blizzard_effect
			effect_duration = 180
			cost = 200
			mana_type = magicka
			spell_caster = ROOT
			spell_target = ROOT
		}
	}
	#Effect
	tooltip = es_spell_blizzard_tt	#Can use customizable localisation
}

# Lightning Cloak
custom_button = {
	name = es_spell_button_lightning_cloak	#Must match a scripted guiButtonType in a .gui file
	potential = {
		has_country_flag = es_spell_menu_self_open
		has_country_flag = es_spell_menu_self_destruction_open
	}
	#Determines when the button is visible
	trigger = {
		hidden_trigger = {
			meets_mana_requirements = {
				cost = 200
				mana_type = magicka
			}
		}
	}
	#Determines when the button is clickable
	effect = {
        cast_ability_option = {
			which = lightning_cloak_effect
			effect_duration = 365
			cost = 200
			mana_type = magicka
			spell_caster = ROOT
			spell_target = ROOT
		}
	}
	#Effect
	tooltip = es_spell_lightning_cloak_tt	#Can use customizable localisation
}

# Wall of Storms
custom_button = {
	name = es_spell_button_wall_of_storms	#Must match a scripted guiButtonType in a .gui file
	potential = {
		has_country_flag = es_spell_menu_self_open
		has_country_flag = es_spell_menu_self_destruction_open
	}
	#Determines when the button is visible
	trigger = {
		hidden_trigger = {
			meets_mana_requirements = {
				cost = 200
				mana_type = magicka
			}
		}
	}
	#Determines when the button is clickable
	effect = {
		cast_ability_option = {
			which = wall_of_storms_effect
			effect_duration = 180
			cost = 200
			mana_type = magicka
			spell_caster = ROOT
			spell_target = ROOT
		}
	}
	#Effect
	tooltip = es_spell_wall_of_storms_tt	#Can use customizable localisation
}

# Lightning Storm
custom_button = {
	name = es_spell_button_lightning_storm	#Must match a scripted guiButtonType in a .gui file
	potential = {
		has_country_flag = es_spell_menu_self_open
		has_country_flag = es_spell_menu_self_destruction_open
	}
	#Determines when the button is visible
	trigger = {
		hidden_trigger = {
			meets_mana_requirements = {
				cost = 200
				mana_type = magicka
			}
		}
	}
	#Determines when the button is clickable
	effect = {
        cast_ability_option = {
			which = lightning_storm_effect
			effect_duration = 180
			cost = 200
			mana_type = magicka
			spell_caster = ROOT
			spell_target = ROOT
		}
	}
	#Effect
	tooltip = es_spell_lightning_storm_tt	#Can use customizable localisation
}

#######################
### Illusion Spells ###
#######################

# Calm
custom_button = {
	name = es_spell_button_calm	#Must match a scripted guiButtonType in a .gui file
	potential = {
		has_country_flag = es_spell_menu_self_open
		has_country_flag = es_spell_menu_self_illusion_open
	}
	#Determines when the button is visible
	trigger = {
		hidden_trigger = {
			meets_mana_requirements = {
				cost = 200
				mana_type = magicka
			}
		}
	}
	#Determines when the button is clickable
	effect = {
        cast_ability_option = {
			which = calm_effect
			effect_duration = 365
			cost = 200
			mana_type = magicka
			spell_caster = ROOT
			spell_target = ROOT
		}
	}
	#Effect
	tooltip = es_spell_calm_tt	#Can use customizable localisation
}

# Call to Arms
custom_button = {
	name = es_spell_button_call_to_arms	#Must match a scripted guiButtonType in a .gui file
	potential = {
		has_country_flag = es_spell_menu_self_open
		has_country_flag = es_spell_menu_self_illusion_open
	}
	#Determines when the button is visible
	trigger = {
		hidden_trigger = {
			meets_mana_requirements = {
				cost = 200
				mana_type = magicka
			}
		}
	}
	#Determines when the button is clickable
	effect = {
        cast_ability_option = {
			which = call_to_arms_effect
			effect_duration = 365
			cost = 200
			mana_type = magicka
			spell_caster = ROOT
			spell_target = ROOT
		}
	}
	#Effect
	tooltip = es_spell_call_to_arms_tt	#Can use customizable localisation
}

# Call to Arms
custom_button = {
	name = es_spell_button_clairvoyance	#Must match a scripted guiButtonType in a .gui file
	potential = {
		has_country_flag = es_spell_menu_self_open
		has_country_flag = es_spell_menu_self_illusion_open
	}
	#Determines when the button is visible
	trigger = {
		hidden_trigger = {
			meets_mana_requirements = {
				cost = 200
				mana_type = magicka
			}
		}
	}
	#Determines when the button is clickable
	effect = {
        cast_ability_option = {
			which = clairvoyance_effect
			effect_duration = 365
			cost = 200
			mana_type = magicka
			spell_caster = ROOT
			spell_target = ROOT
		}
	}
	#Effect
	tooltip = es_spell_clairvoyance_tt	#Can use customizable localisation
}

# Courage
custom_button = {
	name = es_spell_button_courage	#Must match a scripted guiButtonType in a .gui file
	potential = {
		has_country_flag = es_spell_menu_self_open
		has_country_flag = es_spell_menu_self_illusion_open
	}
	#Determines when the button is visible
	trigger = {
		hidden_trigger = {
			meets_mana_requirements = {
				cost = 200
				mana_type = magicka
			}
		}
	}
	#Determines when the button is clickable
	effect = {
        cast_ability_option = {
			which = courage_effect
			effect_duration = 365
			cost = 200
			mana_type = magicka
			spell_caster = ROOT
			spell_target = ROOT
		}
	}
	#Effect
	tooltip = es_spell_courage_tt	#Can use customizable localisation
}

# Harmony
custom_button = {
	name = es_spell_button_harmony	#Must match a scripted guiButtonType in a .gui file
	potential = {
		has_country_flag = es_spell_menu_self_open
		has_country_flag = es_spell_menu_self_illusion_open
	}
	#Determines when the button is visible
	trigger = {
		hidden_trigger = {
			meets_mana_requirements = {
				cost = 200
				mana_type = magicka
			}
		}
	}
	#Determines when the button is clickable
	effect = {
        cast_ability_option = {
			which = harmony_effect
			effect_duration = 365
			cost = 200
			mana_type = magicka
			spell_caster = ROOT
			spell_target = ROOT
		}
	}
	#Effect
	tooltip = es_spell_harmony_tt	#Can use customizable localisation
}

# Hysteria
custom_button = {
	name = es_spell_button_hysteria	#Must match a scripted guiButtonType in a .gui file
	potential = {
		has_country_flag = es_spell_menu_self_open
		has_country_flag = es_spell_menu_self_illusion_open
	}
	#Determines when the button is visible
	trigger = {
		hidden_trigger = {
			meets_mana_requirements = {
				cost = 200
				mana_type = magicka
			}
		}
	}
	#Determines when the button is clickable
	effect = {
        cast_ability_option = {
			which = hysteria_effect
			cost = 200
			mana_type = magicka
			spell_caster = ROOT
			spell_target = ROOT
		}
	}
	#Effect
	tooltip = es_spell_hysteria_tt	#Can use customizable localisation
}

# Invisibility
custom_button = {
	name = es_spell_button_invisibility	#Must match a scripted guiButtonType in a .gui file
	potential = {
		has_country_flag = es_spell_menu_self_open
		has_country_flag = es_spell_menu_self_illusion_open
	}
	#Determines when the button is visible
	trigger = {
		hidden_trigger = {
			meets_mana_requirements = {
				cost = 200
				mana_type = magicka
			}
		}
	}
	#Determines when the button is clickable
	effect = {
        cast_ability_option = {
			which = invisibility_effect
			cost = 200
			effect_duration = 365
			mana_type = magicka
			spell_caster = ROOT
			spell_target = ROOT
		}
	}
	#Effect
	tooltip = es_spell_invisibility_tt	#Can use customizable localisation
}

# Mayhem
custom_button = {
	name = es_spell_button_mayhem	#Must match a scripted guiButtonType in a .gui file
	potential = {
		has_country_flag = es_spell_menu_self_open
		has_country_flag = es_spell_menu_self_illusion_open
	}
	#Determines when the button is visible
	trigger = {
		hidden_trigger = {
			meets_mana_requirements = {
				cost = 200
				mana_type = magicka
			}
		}
	}
	#Determines when the button is clickable
	effect = {
        cast_ability_option = {
			which = mayhem_effect
			cost = 200
			mana_type = magicka
			spell_caster = ROOT
			spell_target = ROOT
		}
	}
	#Effect
	tooltip = es_spell_mayhem_tt	#Can use customizable localisation
}