# Open / Close Buttons

custom_window = {
    name = es_breton_deities_window
    potential = { has_country_flag = esu_breton_deities_menu_opened }
}

custom_button = {
    name = es_breton_deities_menu_open
    potential = { 
		NOT = { has_country_flag = esu_breton_deities_menu_opened } 
		primary_culture = horsemen
	}
    trigger = { 
		hidden_trigger = {
			NOT = { has_country_flag = esu_breton_deities_menu_opened }
		}
	}
	effect = {
		set_country_flag = esu_breton_deities_menu_opened
		
		1428 = { owner = { save_global_event_target_as = es_breton_owner_1428_event_target } }
		7039 = { owner = { save_global_event_target_as = es_breton_owner_7039_event_target } }
		1411 = { owner = { save_global_event_target_as = es_breton_owner_1411_event_target } }
		6850 = { owner = { save_global_event_target_as = es_breton_owner_6850_event_target } }
		6268 = { owner = { save_global_event_target_as = es_bretono_wner_6268_event_target } }
		7015 = { owner = { save_global_event_target_as = es_breton_owner_7015_event_target } }
		1383 = { owner = { save_global_event_target_as = es_breton_owner_1383_event_target } }
		6857 = { owner = { save_global_event_target_as = es_breton_owner_6857_event_target } }
		6960 = { owner = { save_global_event_target_as = es_breton_owner_6960_event_target } }
		6908 = { owner = { save_global_event_target_as = es_breton_owner_6908_event_target } }
		6279 = { owner = { save_global_event_target_as = es_breton_owner_6279_event_target } }
		6996 = { owner = { save_global_event_target_as = es_breton_owner_6996_event_target } }
		6985 = { owner = { save_global_event_target_as = es_breton_owner_6985_event_target } }
		1417 = { owner = { save_global_event_target_as = es_breton_owner_1417_event_target } }
	}
	tooltip = esu_breton_deities_menu_opened_tooltip
}

custom_button = {
    name = es_breton_deities_menu_close
    potential = { 
	}
    trigger = { 
		hidden_trigger = {
			has_country_flag = esu_breton_deities_menu_opened
		}
	}
    effect = {
		clr_country_flag = esu_breton_deities_menu_opened
	}
    tooltip = esu_breton_deities_menu_closed_tooltip
}

# Background

custom_icon = {
    name = es_breton_deities_background
    potential = { }
}

custom_text_box = {
    name = es_breton_deities_title
    potential = { }
}

custom_shield = { name = es_breton_deity_shield_1 potential = { } trigger = { } effect = { } global_event_target = es_breton_owner_1428_event_target open_country = yes }
custom_shield = { name = es_breton_deity_shield_2 potential = { } trigger = { } effect = { } global_event_target = es_breton_owner_7039_event_target open_country = yes }
custom_shield = { name = es_breton_deity_shield_3 potential = { } trigger = { } effect = { } global_event_target = es_breton_owner_1411_event_target open_country = yes }
custom_shield = { name = es_breton_deity_shield_4 potential = { } trigger = { } effect = { } global_event_target = es_breton_owner_6850_event_target open_country = yes }
custom_shield = { name = es_breton_deity_shield_5 potential = { } trigger = { } effect = { } global_event_target = es_bretono_wner_6268_event_target open_country = yes }
custom_shield = { name = es_breton_deity_shield_6 potential = { } trigger = { } effect = { } global_event_target = es_breton_owner_7015_event_target open_country = yes }
custom_shield = { name = es_breton_deity_shield_7 potential = { } trigger = { } effect = { } global_event_target = es_breton_owner_1383_event_target open_country = yes }
custom_shield = { name = es_breton_deity_shield_8 potential = { } trigger = { } effect = { } global_event_target = es_breton_owner_6857_event_target open_country = yes }
custom_shield = { name = es_breton_deity_shield_9 potential = { } trigger = { } effect = { } global_event_target = es_breton_owner_6960_event_target open_country = yes }
custom_shield = { name = es_breton_deity_shield_10 potential = { } trigger = { } effect = { } global_event_target = es_breton_owner_6908_event_target open_country = yes }
custom_shield = { name = es_breton_deity_shield_11 potential = { } trigger = { } effect = { } global_event_target = es_breton_owner_6279_event_target open_country = yes }
custom_shield = { name = es_breton_deity_shield_12 potential = { } trigger = { } effect = { } global_event_target = es_breton_owner_6996_event_target open_country = yes }
custom_shield = { name = es_breton_deity_shield_13 potential = { } trigger = { } effect = { } global_event_target = es_breton_owner_6985_event_target open_country = yes }
custom_shield = { name = es_breton_deity_shield_14 potential = { } trigger = { } effect = { } global_event_target = es_breton_owner_1417_event_target open_country = yes }

# Interactions

custom_icon = { name = es_breton_baan_dar_icon potential = { } }
custom_text_box = { name = es_breton_baan_dar_title potential = { } }
custom_text_box = { name = es_breton_baan_dar_desc potential = { } }
custom_button = { name = es_breton_baan_dar_button potential = { NOT = { has_country_modifier = es_breton_baan_dar_modifier } } trigger = { prestige = 15 owns_or_non_sovereign_subject_of = 1428  } 
	effect = { add_prestige = -15 add_country_modifier = { name = "es_breton_baan_dar_modifier" duration = -1 } } tooltip = es_breton_baan_dar_button_tooltip }
custom_icon = { name = es_breton_baan_dar_yes potential = { has_country_modifier = es_breton_baan_dar_modifier } }

custom_icon = { name = es_breton_druagaa_icon potential = { } }
custom_text_box = { name = es_breton_druagaa_title potential = { } }
custom_text_box = { name = es_breton_druagaa_desc potential = { } }
custom_button = { name = es_breton_druagaa_button potential = { NOT = { has_country_modifier = es_breton_druagaa_modifier } } trigger = { prestige = 15 owns_or_non_sovereign_subject_of = 7039 } 
	effect = { add_prestige = -15 add_country_modifier = { name = "es_breton_druagaa_modifier" duration = -1 } } tooltip = es_breton_druagaa_button_tooltip }
custom_icon = { name = es_breton_druagaa_yes potential = { has_country_modifier = es_breton_druagaa_modifier } }

custom_icon = { name = es_breton_ephen_icon potential = { } }
custom_text_box = { name = es_breton_ephen_title potential = { } }
custom_text_box = { name = es_breton_ephen_desc potential = { } }
custom_button = { name = es_breton_ephen_button potential = { NOT = { has_country_modifier = es_breton_ephen_modifier } } trigger = { prestige = 15 owns_or_non_sovereign_subject_of = 1411 } 
	effect = { add_prestige = -15 add_country_modifier = { name = "es_breton_ephen_modifier" duration = -1 } } tooltip = es_breton_ephen_button_tooltip }
custom_icon = { name = es_breton_ephen_yes potential = { has_country_modifier = es_breton_ephen_modifier } }

custom_icon = { name = es_breton_jeh_free_icon potential = { } }
custom_text_box = { name = es_breton_jeh_free_title potential = { } }
custom_text_box = { name = es_breton_jeh_free_desc potential = { } }
custom_button = { name = es_breton_jeh_free_button potential = { NOT = { has_country_modifier = es_breton_jeh_free_modifier } } trigger = { prestige = 15 owns_or_non_sovereign_subject_of = 6850 } 
	effect = { add_prestige = -15 add_country_modifier = { name = "es_breton_jeh_free_modifier" duration = -1 } } tooltip = es_breton_jeh_free_button_tooltip }
custom_icon = { name = es_breton_jeh_free_yes potential = { has_country_modifier = es_breton_jeh_free_modifier } }

custom_icon = { name = es_breton_jhim_sei_icon potential = { } }
custom_text_box = { name = es_breton_jhim_sei_title potential = { } }
custom_text_box = { name = es_breton_jhim_sei_desc potential = { } }
custom_button = { name = es_breton_jhim_sei_button potential = { NOT = { has_country_modifier = es_breton_jhim_sei_modifier } } trigger = { prestige = 15 owns_or_non_sovereign_subject_of = 6268 } 
	effect = { add_prestige = -15 add_country_modifier = { name = "es_breton_jhim_sei_modifier" duration = -1 } } tooltip = es_breton_jhim_sei_button_tooltip }
custom_icon = { name = es_breton_jhim_sei_yes potential = { has_country_modifier = es_breton_jhim_sei_modifier } }

custom_icon = { name = es_breton_maras_tear_icon potential = { } }
custom_text_box = { name = es_breton_maras_tear_title potential = { } }
custom_text_box = { name = es_breton_maras_tear_desc potential = { } }
custom_button = { name = es_breton_maras_tear_button potential = { NOT = { has_country_modifier = es_breton_maras_tear_modifier } } trigger = { prestige = 15 owns_or_non_sovereign_subject_of = 7015 }
	effect = { add_prestige = -15 add_country_modifier = { name = "es_breton_maras_tear_modifier" duration = -1 } } tooltip = es_breton_maras_tear_button_tooltip }
custom_icon = { name = es_breton_maras_tear_yes potential = { has_country_modifier = es_breton_maras_tear_modifier } }
	
custom_icon = { name = es_breton_notorgo_icon potential = { } }
custom_text_box = { name = es_breton_notorgo_title potential = { } }
custom_text_box = { name = es_breton_notorgo_desc potential = { } }
custom_button = { name = es_breton_notorgo_button potential = { NOT = { has_country_modifier = es_breton_notorgo_modifier } } trigger = { prestige = 15 owns_or_non_sovereign_subject_of = 1383 } 
	effect = { add_prestige = -15 add_country_modifier = { name = "es_breton_notorgo_modifier" duration = -1 } } tooltip = es_breton_notorgo_button_tooltip }
custom_icon = { name = es_breton_notorgo_yes potential = { has_country_modifier = es_breton_notorgo_modifier } }

custom_icon = { name = es_breton_qolwen_icon potential = { } }
custom_text_box = { name = es_breton_qolwen_title potential = { } }
custom_text_box = { name = es_breton_qolwen_desc potential = { } }
custom_button = { name = es_breton_qolwen_button potential = { NOT = { has_country_modifier = es_breton_qolwen_modifier } } trigger = { prestige = 15 owns_or_non_sovereign_subject_of = 6857 }
	effect = { add_prestige = -15 add_country_modifier = { name = "es_breton_qolwen_modifier" duration = -1 } } tooltip = es_breton_qolwen_button_tooltip }
custom_icon = { name = es_breton_qolwen_yes potential = { has_country_modifier = es_breton_qolwen_modifier } }

custom_icon = { name = es_breton_raen_icon potential = { } }
custom_text_box = { name = es_breton_raen_title potential = { } }
custom_text_box = { name = es_breton_raen_desc potential = { } }
custom_button = { name = es_breton_raen_button potential = { NOT = { has_country_modifier = es_breton_raen_modifier } } trigger = { prestige = 15 owns_or_non_sovereign_subject_of = 6960 } 
	effect = { add_prestige = -15 add_country_modifier = { name = "es_breton_raen_modifier" duration = -1 } } tooltip = es_breton_raen_button_tooltip }
custom_icon = { name = es_breton_raen_yes potential = { has_country_modifier = es_breton_raen_modifier } }

custom_icon = { name = es_breton_reymon_ebonarm_icon potential = { } }
custom_text_box = { name = es_breton_reymon_ebonarm_title potential = { } }
custom_text_box = { name = es_breton_reymon_ebonarm_desc potential = { } }
custom_button = { name = es_breton_reymon_ebonarm_button potential = { NOT = { has_country_modifier = es_breton_reymon_ebonarm_modifier } } trigger = { prestige = 15 owns_or_non_sovereign_subject_of = 6908 } 
	effect = { add_prestige = -15 add_country_modifier = { name = "es_breton_reymon_ebonarm_modifier" duration = -1 } } tooltip = es_breton_reymon_ebonarm_button_tooltip }
custom_icon = { name = es_breton_reymon_ebonarm_yes potential = { has_country_modifier = es_breton_reymon_ebonarm_modifier } }

custom_icon = { name = es_breton_sai_icon potential = { } }
custom_text_box = { name = es_breton_sai_title potential = { } }
custom_text_box = { name = es_breton_sai_desc potential = { } }
custom_button = { name = es_breton_sai_button potential = { NOT = { has_country_modifier = es_breton_sai_modifier } } trigger = { prestige = 15 owns_or_non_sovereign_subject_of = 6279 } 
	effect = { add_prestige = -15 add_country_modifier = { name = "es_breton_sai_modifier" duration = -1 } } tooltip = es_breton_sai_button_tooltip }
custom_icon = { name = es_breton_sai_yes potential = { has_country_modifier = es_breton_sai_modifier } }

custom_icon = { name = es_breton_sethiete_icon potential = { } }
custom_text_box = { name = es_breton_sethiete_title potential = { } }
custom_text_box = { name = es_breton_sethiete_desc potential = { } }
custom_button = { name = es_breton_sethiete_button potential = { NOT = { has_country_modifier = es_breton_sethiete_modifier } } trigger = { prestige = 15 owns_or_non_sovereign_subject_of = 6996 }
	effect = { add_prestige = -15 add_country_modifier = { name = "es_breton_sethiete_modifier" duration = -1 } } tooltip = es_breton_sethiete_button_tooltip }
custom_icon = { name = es_breton_sethiete_yes potential = { has_country_modifier = es_breton_sethiete_modifier } }

custom_icon = { name = es_breton_shandars_sorrow_icon potential = { } }
custom_text_box = { name = es_breton_shandars_sorrow_title potential = { } }
custom_text_box = { name = es_breton_shandars_sorrow_desc potential = { } }
custom_button = { name = es_breton_shandars_sorrow_button potential = { NOT = { has_country_modifier = es_breton_shandars_sorrow_modifier } } trigger = { prestige = 15 owns_or_non_sovereign_subject_of = 6985 } 
	effect = { add_prestige = -15 add_country_modifier = { name = "es_breton_shandars_sorrow_modifier" duration = -1 } } tooltip = es_breton_shandars_sorrow_button_tooltip }
custom_icon = { name = es_breton_shandars_sorrow_yes potential = { has_country_modifier = es_breton_shandars_sorrow_modifier } }

custom_icon = { name = es_breton_vigryl_icon potential = { } }
custom_text_box = { name = es_breton_vigryl_title potential = { } }
custom_text_box = { name = es_breton_vigryl_desc potential = { } }
custom_button = { name = es_breton_vigryl_button potential = { NOT = { has_country_modifier = es_breton_vigryl_modifier } } trigger = { prestige = 15 owns_or_non_sovereign_subject_of = 1417 } 
	effect = { add_prestige = -15 add_country_modifier = { name = "es_breton_vigryl_modifier" duration = -1 } } tooltip = es_breton_vigryl_button_tooltip }
custom_icon = { name = es_breton_vigryl_yes potential = { has_country_modifier = es_breton_vigryl_modifier } }