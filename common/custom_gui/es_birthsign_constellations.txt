# Open / Close Buttons

custom_window = {
    name = es_birthsign_constellations_window
    potential = { has_country_flag = esu_birthsign_constellations_menu_opened }
}

custom_button = {
    name = es_birthsign_constellations_menu_open
    potential = { 
		NOT = { has_country_flag = esu_birthsign_constellations_menu_opened } 
		primary_culture = keptu
	}
    trigger = { 
		hidden_trigger = {
			NOT = { has_country_flag = esu_birthsign_constellations_menu_opened }
		}
	}
	effect = {
		set_country_flag = esu_birthsign_constellations_menu_opened
	}
	tooltip = esu_birthsign_constellations_opened_tooltip
}

custom_button = {
    name = es_birthsign_constellations_menu_close
    potential = { 
	}
    trigger = { 
		hidden_trigger = {
			has_country_flag = esu_birthsign_constellations_menu_opened
		}
	}
    effect = {
		clr_country_flag = esu_birthsign_constellations_menu_opened
	}
    tooltip = esu_birthsign_constellations_menu_closed_tooltip
}

# Background

custom_icon = {
    name = es_birthsign_constellations_background
    potential = { }
}

custom_text_box = {
    name = es_birthsign_constellations_title
    potential = { }
}

custom_text_box = {
    name = es_birthsign_constellations_desc
    potential = { }
}

custom_text_box = {
    name = es_birthsign_pledges_title
    potential = { }
}

# Interactions

custom_button = {
    name = es_birthsign_apprentice_card
    potential = { 
		NOT = { has_ruler_modifier = blessing_apprentice_birthsign_mod }
	}
	trigger = {
		OR = {
			has_ruler_modifier = apprentice_birthsign_mod
			AND = {
				OR = {
					esu_mage = 3
					esu_researcher = 3
					esu_mad_scholar = 3
				}
				prestige = 5
			}
		}
	}
	effect = {
		if = { limit = { NOT = { has_ruler_modifier = apprentice_birthsign_mod } } add_prestige = -5 }
		add_ruler_modifier = { name = blessing_apprentice_birthsign_mod duration = -1 }
	}
	frame = { number = 14 trigger = { always = yes } }
	tooltip = es_birthsign_apprentice_tooltip
}

custom_icon = {
    name = es_birthsign_apprentice_card_icon
    potential = { has_ruler_modifier = blessing_apprentice_birthsign_mod }
	frame = { number = 1 trigger = { always = yes } }
}

custom_button = {
    name = es_birthsign_atronach_card
    potential = { 
		NOT = { has_ruler_modifier = blessing_atronach_birthsign_mod }
	}
	trigger = {
		OR = {
			has_ruler_modifier = atronach_birthsign_mod
			AND = {
				custom_trigger_tooltip = { tooltip = es_captured_enemy_flagship_flag_tooltip has_ruler_flag = es_captured_enemy_flagship_flag }
				prestige = 5
			}
		}
	}
	effect = {
		if = { limit = { NOT = { has_ruler_modifier = atronach_birthsign_mod } } add_prestige = -5 }
		add_ruler_modifier = { name = blessing_atronach_birthsign_mod duration = -1 }
	}
	frame = { number = 14 trigger = { always = yes } }
	tooltip = es_birthsign_atronach_tooltip
}

custom_icon = {
    name = es_birthsign_atronach_card_icon
    potential = { has_ruler_modifier = blessing_atronach_birthsign_mod }
	frame = { number = 2 trigger = { always = yes } }
}

custom_button = {
    name = es_birthsign_lady_card
    potential = { 
		NOT = { has_ruler_modifier = blessing_lady_birthsign_mod }
	}
	trigger = {
		OR = {
			has_ruler_modifier = lady_birthsign_mod
			AND = {
				check_variable = { which = enforced_peace_num value = 10.0 }
				prestige = 5
			}
		}
	}
	effect = {
		if = { limit = { NOT = { has_ruler_modifier = lady_birthsign_mod } } add_prestige = -5 }
		add_ruler_modifier = { name = blessing_lady_birthsign_mod duration = -1 }
	}
	frame = { number = 14 trigger = { always = yes } }
	tooltip = es_birthsign_lady_tooltip
}

custom_icon = {
    name = es_birthsign_lady_card_icon
    potential = { has_ruler_modifier = blessing_lady_birthsign_mod }
	frame = { number = 3 trigger = { always = yes } }
}

custom_button = {
    name = es_birthsign_lord_card
    potential = { 
		NOT = { has_ruler_modifier = blessing_lord_birthsign_mod }
	}
	trigger = {
		OR = {
			has_ruler_modifier = lord_birthsign_mod
			AND = {
				check_variable = { which = num_of_wars_won_ruler value = 10.0 }
				prestige = 5
			}
		}
	}
	effect = {
		if = { limit = { NOT = { has_ruler_modifier = lord_birthsign_mod } } add_prestige = -5 }
		add_ruler_modifier = { name = blessing_lord_birthsign_mod duration = -1 }
	}
	frame = { number = 14 trigger = { always = yes } }
	tooltip = es_birthsign_lord_tooltip
}

custom_icon = {
    name = es_birthsign_lord_card_icon
    potential = { has_ruler_modifier = blessing_lord_birthsign_mod }
	frame = { number = 4 trigger = { always = yes } }
}

custom_button = {
    name = es_birthsign_lover_card
    potential = { 
		NOT = { has_ruler_modifier = blessing_lover_birthsign_mod }
	}
	trigger = {
		OR = {
			has_ruler_modifier = lover_birthsign_mod
			AND = {
				check_variable = { which = infiltrated_administration_num value = 10.0 }
				prestige = 5
			}
		}
	}
	effect = {
		if = { limit = { NOT = { has_ruler_modifier = lover_birthsign_mod } } add_prestige = -5 }
		add_ruler_modifier = { name = blessing_lover_birthsign_mod duration = -1 }
	}
	frame = { number = 14 trigger = { always = yes } }
	tooltip = es_birthsign_lover_tooltip
}

custom_icon = {
    name = es_birthsign_lover_card_icon
    potential = { has_ruler_modifier = blessing_lover_birthsign_mod }
	frame = { number = 5 trigger = { always = yes } }
}

custom_button = {
    name = es_birthsign_mage_card
    potential = { 
		NOT = { has_ruler_modifier = blessing_mage_birthsign_mod }
	}
	trigger = {
		OR = {
			has_ruler_modifier = mage_birthsign_mod
			AND = {
				check_variable = { which = passed_technology_num value = 5.0 }
				prestige = 5
			}
		}
	}
	effect = {
		if = { limit = { NOT = { has_ruler_modifier = mage_birthsign_mod } } add_prestige = -5 }
		add_ruler_modifier = { name = blessing_mage_birthsign_mod duration = -1 }
	}
	frame = { number = 14 trigger = { always = yes } }
	tooltip = es_birthsign_mage_tooltip
}

custom_icon = {
    name = es_birthsign_mage_card_icon
    potential = { has_ruler_modifier = blessing_mage_birthsign_mod }
	frame = { number = 6 trigger = { always = yes } }
}

custom_button = {
    name = es_birthsign_ritual_card
    potential = { 
		NOT = { has_ruler_modifier = blessing_ritual_birthsign_mod }
	}
	trigger = {
		OR = {
			has_ruler_modifier = ritual_birthsign_aspect_cunning_mod
			has_ruler_modifier = ritual_birthsign_aspect_knowledge_mod
			has_ruler_modifier = ritual_birthsign_aspect_charm_mod
			has_ruler_modifier = ritual_birthsign_aspect_expedition_mod
			AND = {
				OR = { adm = 7 dip = 7 mil = 7 }
				prestige = 5
			}
		}
	}
	effect = {
		if = { limit = { NOT = { has_ruler_modifier = ritual_birthsign_aspect_cunning_mod } NOT = { has_ruler_modifier = ritual_birthsign_aspect_knowledge_mod } NOT = { has_ruler_modifier = ritual_birthsign_aspect_charm_mod } NOT = { has_ruler_modifier = ritual_birthsign_aspect_expedition_mod } } add_prestige = -5 }
		add_ruler_modifier = { name = blessing_ritual_birthsign_mod duration = -1 }
	}
	frame = { number = 14 trigger = { always = yes } }
	tooltip = es_birthsign_ritual_tooltip
}

custom_icon = {
    name = es_birthsign_ritual_card_icon
    potential = { has_ruler_modifier = blessing_ritual_birthsign_mod }
	frame = { number = 7 trigger = { always = yes } }
}

custom_button = {
    name = es_birthsign_shadow_card
    potential = { 
		NOT = { has_ruler_modifier = blessing_shadow_birthsign_mod }
	}
	trigger = {
		OR = {
			has_ruler_modifier = shadow_birthsign_mod
			AND = {
				check_variable = { which = sowed_discontend_num value = 10.0 }
				prestige = 5
			}
		}
	}
	effect = {
		if = { limit = { NOT = { has_ruler_modifier = shadow_birthsign_mod } } add_prestige = -5 }
		add_ruler_modifier = { name = blessing_shadow_birthsign_mod duration = -1 }
	}
	frame = { number = 14 trigger = { always = yes } }
	tooltip = es_birthsign_shadow_tooltip
}

custom_icon = {
    name = es_birthsign_shadow_card_icon
    potential = { has_ruler_modifier = blessing_shadow_birthsign_mod }
	frame = { number = 9 trigger = { always = yes } }
}

custom_button = {
    name = es_birthsign_steed_card
    potential = { 
		NOT = { has_ruler_modifier = blessing_steed_birthsign_mod }
	}
	trigger = {
		OR = {
			has_ruler_modifier = steed_birthsign_mod
			AND = {
				calc_true_if = {
					all_subject_country = {
						transfers_trade_power_to = ROOT
						NOT = { liberty_desire = 50 }
					}
					amount = 5
				}
				prestige = 5
			}
		}
	}
	effect = {
		if = { limit = { NOT = { has_ruler_modifier = steed_birthsign_mod } } add_prestige = -5 }
		add_ruler_modifier = { name = blessing_steed_birthsign_mod duration = -1 }
	}
	frame = { number = 14 trigger = { always = yes } }
	tooltip = es_birthsign_steed_tooltip
}

custom_icon = {
    name = es_birthsign_steed_card_icon
    potential = { has_ruler_modifier = blessing_steed_birthsign_mod }
	frame = { number = 10 trigger = { always = yes } }
}

custom_button = {
    name = es_birthsign_thief_card
    potential = { 
		NOT = { has_ruler_modifier = blessing_thief_birthsign_mod }
	}
	trigger = {
		OR = {
			has_ruler_modifier = thief_birthsign_mod
			AND = {
				any_known_country = { is_enemy = ROOT }
				all_rival_country = {
					capital_scope = {
						controlled_by = ROOT
						devastation = 100
					}
				}
				prestige = 5
			}
		}
	}
	effect = {
		if = { limit = { NOT = { has_ruler_modifier = thief_birthsign_mod } } add_prestige = -5 }
		add_ruler_modifier = { name = blessing_thief_birthsign_mod duration = -1 }
	}
	frame = { number = 14 trigger = { always = yes } }
	tooltip = es_birthsign_thief_tooltip
}

custom_icon = {
    name = es_birthsign_thief_card_icon
    potential = { has_ruler_modifier = blessing_thief_birthsign_mod }
	frame = { number = 11 trigger = { always = yes } }
}

custom_button = {
    name = es_birthsign_tower_card
    potential = { 
		NOT = { has_ruler_modifier = blessing_tower_birthsign_mod }
	}
	trigger = {
		OR = {
			has_ruler_modifier = tower_birthsign_mod
			AND = {
				check_variable = { which = developed_provinces_num value = 25.0 }
				prestige = 5
			}
		}
	}
	effect = {
		if = { limit = { NOT = { has_ruler_modifier = tower_birthsign_mod } } add_prestige = -5 }
		add_ruler_modifier = { name = blessing_tower_birthsign_mod duration = -1 }
	}
	frame = { number = 14 trigger = { always = yes } }
	tooltip = es_birthsign_tower_tooltip
}

custom_icon = {
    name = es_birthsign_tower_card_icon
    potential = { has_ruler_modifier = blessing_tower_birthsign_mod }
	frame = { number = 12 trigger = { always = yes } }
}

custom_button = {
    name = es_birthsign_warrior_card
    potential = { 
		NOT = { has_ruler_modifier = blessing_warrior_birthsign_mod }
	}
	trigger = {
		OR = {
			has_ruler_modifier = warrior_birthsign_mod
			AND = {
				check_variable = {
					which = num_of_battles_won_ruler
					value = 50
				}
				prestige = 5
			}
		}
	}
	effect = {
		if = { limit = { NOT = { has_ruler_modifier = warrior_birthsign_mod } } add_prestige = -5 }
		add_ruler_modifier = { name = blessing_warrior_birthsign_mod duration = -1 }
	}
	frame = { number = 14 trigger = { always = yes } }
	tooltip = es_birthsign_warrior_tooltip
}

custom_icon = {
    name = es_birthsign_warrior_card_icon
    potential = { has_ruler_modifier = blessing_warrior_birthsign_mod }
	frame = { number = 13 trigger = { always = yes } }
}

custom_button = {
    name = es_birthsign_serpent_card
    potential = { 
		NOT = { has_ruler_modifier = blessing_serpent_birthsign_mod }
	}
	trigger = {
		OR = {
			has_ruler_modifier = serpent_birthsign_mod
			AND = {
				has_ruler_modifier = blessing_apprentice_birthsign_mod
				has_ruler_modifier = blessing_atronach_birthsign_mod
				has_ruler_modifier = blessing_lady_birthsign_mod
				has_ruler_modifier = blessing_lord_birthsign_mod
				has_ruler_modifier = blessing_lover_birthsign_mod
				has_ruler_modifier = blessing_mage_birthsign_mod
				has_ruler_modifier = blessing_ritual_birthsign_mod
				has_ruler_modifier = blessing_shadow_birthsign_mod
				has_ruler_modifier = blessing_steed_birthsign_mod
				has_ruler_modifier = blessing_thief_birthsign_mod
				has_ruler_modifier = blessing_tower_birthsign_mod
				has_ruler_modifier = blessing_warrior_birthsign_mod
				prestige = 10
			}
		}
	}
	effect = {
		if = { limit = { NOT = { has_ruler_modifier = serpent_birthsign_mod } } add_prestige = -10 }
		add_ruler_modifier = { name = blessing_serpent_birthsign_mod duration = -1 }
		country_event = { id = es_gui.1 }
	}
	frame = { number = 14 trigger = { always = yes } }
	tooltip = es_birthsign_serpent_tooltip
}

custom_icon = {
    name = es_birthsign_serpent_card_icon
    potential = { has_ruler_modifier = blessing_serpent_birthsign_mod }
	frame = { number = 8 trigger = { always = yes } }
}

custom_text_box = {
    name = es_birthsign_serpent_desc
    potential = { }
}

###################################################################################################

custom_button = {
    name = es_pledge_of_courage_button
    potential = { 
		NOT = { has_ruler_modifier = es_pledge_of_courage_mod }
	}
	trigger = {
		mil_power = 500
		any_known_country = {
			alliance_with = ROOT
		}
	}
	effect = {
		add_mil_power = -500
		add_ruler_modifier = { name = es_pledge_of_courage_mod duration = -1 }
		custom_tooltip = breaking_alliance_will_void_this_pledge
	}
	tooltip = es_pledge_of_courage_tooltip
}

custom_icon = {
    name = es_pledge_of_courage_icon
    potential = { has_ruler_modifier = es_pledge_of_courage_mod }
}

custom_text_box = {
    name = es_pledge_of_courage_text
    potential = { }
}

custom_button = {
    name = es_pledge_of_perfection_button
    potential = { 
		NOT = { has_ruler_modifier = es_pledge_of_perfection_mod }
	}
	trigger = {
		dip_power = 500
		is_at_war = yes
	}
	effect = {
		add_dip_power = -500
		add_ruler_modifier = { name = es_pledge_of_perfection_mod duration = -1 }
		custom_tooltip = loosing_war_will_void_this_pledge
	}
	tooltip = es_pledge_of_perfection_tooltip
}

custom_icon = {
    name = es_pledge_of_perfection_icon
    potential = { has_ruler_modifier = es_pledge_of_perfection_mod }
}

custom_text_box = {
    name = es_pledge_of_perfection_text
    potential = { }
}

custom_button = {
    name = es_pledge_of_piety_button
    potential = { 
		NOT = { has_ruler_modifier = es_pledge_of_piety_mod }
	}
	trigger = {
		adm_power = 500
		is_defender_of_faith = yes
	}
	effect = {
		add_adm_power = -500
		add_ruler_modifier = { name = es_pledge_of_piety_mod duration = -1 }
		custom_tooltip = loosing_defender_of_faith_will_void_this_pledge
	}
	tooltip = es_pledge_of_piety_tooltip
}

custom_icon = {
    name = es_pledge_of_piety_icon
    potential = { has_ruler_modifier = es_pledge_of_piety_mod }
}

custom_text_box = {
    name = es_pledge_of_piety_text
    potential = { }
}

custom_button = {
    name = es_pledge_of_simplicity_button
    potential = { 
		NOT = { has_ruler_modifier = es_pledge_of_simplicity_mod }
	}
	trigger = {
		dip_power = 500
		NOT = { treasury = 10000 }
	}
	effect = {
		add_dip_power = -500
		add_ruler_modifier = { name = es_pledge_of_simplicity_mod duration = -1 }
		custom_tooltip = gaining_wealth_will_void_this_pledge
	}
	tooltip = es_pledge_of_simplicity_tooltip
}

custom_icon = {
    name = es_pledge_of_simplicity_icon
    potential = { has_ruler_modifier = es_pledge_of_simplicity_mod }
}

custom_text_box = {
    name = es_pledge_of_simplicity_text
    potential = { }
}

custom_button = {
    name = es_pledge_of_obedience_button
    potential = { 
		NOT = { has_ruler_modifier = es_pledge_of_obedience_mod }
	}
	trigger = {
		adm_power = 500
		crown_land_share = 50
	}
	effect = {
		add_adm_power = -500
		add_ruler_modifier = { name = es_pledge_of_obedience_mod duration = -1 }
		custom_tooltip = loosing_crownland_will_void_this_pledge
	}
	tooltip = es_pledge_of_obedience_tooltip
}

custom_icon = {
    name = es_pledge_of_obedience_icon
    potential = { has_ruler_modifier = es_pledge_of_obedience_mod }
}

custom_text_box = {
    name = es_pledge_of_obedience_text
    potential = { }
}

custom_button = {
    name = es_pledge_of_vigilance_button
    potential = { 
		NOT = { has_ruler_modifier = es_pledge_of_vigilance_mod }
	}
	trigger = {
		mil_power = 500
		manpower_percentage = 0.95
	}
	effect = {
		add_mil_power = -500
		add_ruler_modifier = { name = es_pledge_of_vigilance_mod duration = -1 }
		custom_tooltip = loosing_manpower_will_void_this_pledge
	}
	tooltip = es_pledge_of_vigilance_tooltip
}

custom_icon = {
    name = es_pledge_of_vigilance_icon
    potential = { has_ruler_modifier = es_pledge_of_vigilance_mod }
}

custom_text_box = {
    name = es_pledge_of_vigilance_text
    potential = { }
}