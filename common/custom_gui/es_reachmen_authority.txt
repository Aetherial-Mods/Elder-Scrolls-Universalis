# Open / Close Buttons

custom_window = {
    name = es_reachmen_authority_window
    potential = { has_country_flag = esu_reachmen_authority_menu_opened }
}

custom_button = {
    name = es_reachmen_authority_menu_open
    potential = { 
		NOT = { has_country_flag = esu_reachmen_authority_menu_opened } 
		primary_culture = reachmen
	}
    trigger = { 
		hidden_trigger = {
			NOT = { has_country_flag = esu_reachmen_authority_menu_opened }
		}
	}
	effect = {
		hidden_effect = {
			set_country_flag = esu_reachmen_authority_menu_opened
			1294 = { owner = { save_global_event_target_as = es_reachmen_shield_1_event_target } }
			1297 = { owner = { save_global_event_target_as = es_reachmen_shield_2_event_target } }
			1351 = { owner = { save_global_event_target_as = es_reachmen_shield_3_event_target } }
			3038 = { owner = { save_global_event_target_as = es_reachmen_shield_4_event_target } }
			3061 = { owner = { save_global_event_target_as = es_reachmen_shield_5_event_target } }
			3079 = { owner = { save_global_event_target_as = es_reachmen_shield_6_event_target } }
			3104 = { owner = { save_global_event_target_as = es_reachmen_shield_7_event_target } }
			7213 = { owner = { save_global_event_target_as = es_reachmen_shield_8_event_target } }
			7221 = { owner = { save_global_event_target_as = es_reachmen_shield_9_event_target } }
			7229 = { owner = { save_global_event_target_as = es_reachmen_shield_10_event_target } }
			7230 = { owner = { save_global_event_target_as = es_reachmen_shield_11_event_target } }
			7232 = { owner = { save_global_event_target_as = es_reachmen_shield_12_event_target } }
			7235 = { owner = { save_global_event_target_as = es_reachmen_shield_13_event_target } }
			7278 = { owner = { save_global_event_target_as = es_reachmen_shield_14_event_target } }
			7289 = { owner = { save_global_event_target_as = es_reachmen_shield_15_event_target } }
			7291 = { owner = { save_global_event_target_as = es_reachmen_shield_16_event_target } }
		}
	}
	tooltip = esu_reachmen_authority_opened_tooltip
}

custom_button = {
    name = es_reachmen_authority_menu_close
    potential = { 
	}
    trigger = { 
		hidden_trigger = {
			has_country_flag = esu_reachmen_authority_menu_opened
		}
	}
    effect = {
		clr_country_flag = esu_reachmen_authority_menu_opened
	}
    tooltip = esu_reachmen_authority_menu_closed_tooltip
}

# Background

custom_icon = {
    name = es_reachmen_authority_background
    potential = { }
}

custom_text_box = {
    name = es_reachmen_authority_title
    potential = { }
}

custom_text_box = {
    name = es_reachmen_authority_desc
    potential = { }
}

# Clans

custom_text_box = { name = es_mistrunner_clan_title potential = { } }
custom_shield = { name = es_reachmen_shield_1 potential = { } trigger = { } effect = { } global_event_target = es_reachmen_shield_1_event_target open_country = yes }

custom_text_box = { name = es_treeshade_clan_title potential = { } }
custom_shield = { name = es_reachmen_shield_2 potential = { } trigger = { } effect = { } global_event_target = es_reachmen_shield_2_event_target open_country = yes }

custom_text_box = { name = es_quicktalon_clan_title potential = { } }
custom_shield = { name = es_reachmen_shield_3 potential = { } trigger = { } effect = { } global_event_target = es_reachmen_shield_3_event_target open_country = yes }

custom_text_box = { name = es_ghostsong_clan_title potential = { } }
custom_shield = { name = es_reachmen_shield_4 potential = { } trigger = { } effect = { } global_event_target = es_reachmen_shield_4_event_target open_country = yes }

custom_text_box = { name = es_spiritdancer_clan_title potential = { } }
custom_shield = { name = es_reachmen_shield_5 potential = { } trigger = { } effect = { } global_event_target = es_reachmen_shield_5_event_target open_country = yes }

custom_text_box = { name = es_black_moon_clan_title potential = { } }
custom_shield = { name = es_reachmen_shield_6 potential = { } trigger = { } effect = { } global_event_target = es_reachmen_shield_6_event_target open_country = yes }

custom_text_box = { name = es_deathwing_clan_title potential = { } }
custom_shield = { name = es_reachmen_shield_7 potential = { } trigger = { } effect = { } global_event_target = es_reachmen_shield_7_event_target open_country = yes }

custom_text_box = { name = es_stranglehollow_clan_title potential = { } }
custom_shield = { name = es_reachmen_shield_8 potential = { } trigger = { } effect = { } global_event_target = es_reachmen_shield_8_event_target open_country = yes }

custom_text_box = { name = es_twisted_briar_clan_title potential = { } }
custom_shield = { name = es_reachmen_shield_9 potential = { } trigger = { } effect = { } global_event_target = es_reachmen_shield_9_event_target open_country = yes }

custom_text_box = { name = es_dragonclaw_clan_title potential = { } }
custom_shield = { name = es_reachmen_shield_10 potential = { } trigger = { } effect = { } global_event_target = es_reachmen_shield_10_event_target open_country = yes }

custom_text_box = { name = es_hearteater_clan_title potential = { } }
custom_shield = { name = es_reachmen_shield_11 potential = { } trigger = { } effect = { } global_event_target = es_reachmen_shield_11_event_target open_country = yes }

custom_text_box = { name = es_starsinger_clan_title potential = { } }
custom_shield = { name = es_reachmen_shield_12 potential = { } trigger = { } effect = { } global_event_target = es_reachmen_shield_12_event_target open_country = yes }

custom_text_box = { name = es_stag_heart_clan_title potential = { } }
custom_shield = { name = es_reachmen_shield_13 potential = { } trigger = { } effect = { } global_event_target = es_reachmen_shield_13_event_target open_country = yes }

custom_text_box = { name = es_crow_wife_clan_title potential = { } }
custom_shield = { name = es_reachmen_shield_14 potential = { } trigger = { } effect = { } global_event_target = es_reachmen_shield_14_event_target open_country = yes }

custom_text_box = { name = es_spiritblood_clan_title potential = { } }
custom_shield = { name = es_reachmen_shield_15 potential = { } trigger = { } effect = { } global_event_target = es_reachmen_shield_15_event_target open_country = yes }

custom_text_box = { name = es_spirit_tale_clan_title potential = { } }
custom_shield = { name = es_reachmen_shield_16 potential = { } trigger = { } effect = { } global_event_target = es_reachmen_shield_16_event_target open_country = yes }

# Reforms
custom_text_box = { name = es_reachmen_authority_value potential = { } }

custom_button = {
    name = es_formalise_clan_structure_reform
    potential = { NOT = { has_country_modifier = formalised_clan_structure } }
    trigger = { stability = 3 check_variable = { which = reachmen_authority value = 100 } }
    effect = { 
		add_country_modifier = {
			name = "formalised_clan_structure"
			duration = -1
		}
		es_remove_stability_5 = yes
		set_variable = { which = reachmen_authority value = 0 }
	}
    tooltip = es_formalise_clan_structure_reform_tt
}

custom_icon = { name = es_formalise_clan_structure_reform_icon potential = { has_country_modifier = formalised_clan_structure } }
custom_text_box = { name = es_formalise_clan_structure_reform_text potential = { has_country_modifier = formalised_clan_structure } }

custom_button = {
    name = es_unify_religious_rites_reform
    potential = { NOT = { has_country_modifier = unified_religious_rites } }
    trigger = { has_country_modifier = formalised_clan_structure check_variable = { which = reachmen_authority value = 200 } }
    effect = { 
		add_country_modifier = {
			name = "unified_religious_rites"
			duration = -1
		}
		capital_scope = { spawn_rebels = { type = heretic_rebels size = 5 unrest = 10 } }
		random_owned_province = { spawn_rebels = { type = heretic_rebels size = 3.5 unrest = 10 } }
		random_owned_province = { spawn_rebels = { type = heretic_rebels size = 3.5 unrest = 10 } }
		random_owned_province = { spawn_rebels = { type = heretic_rebels size = 2.5 unrest = 10 } }
		random_owned_province = { spawn_rebels = { type = heretic_rebels size = 2.5 unrest = 10 } }
		set_variable = { which = reachmen_authority value = 0 }
	}
    tooltip = es_unify_religious_rites_reform_tt
}

custom_icon = { name = es_unify_religious_rites_reform_icon potential = { has_country_modifier = unified_religious_rites } }
custom_text_box = { name = es_unify_religious_rites_reform_text potential = { has_country_modifier = unified_religious_rites } }

custom_button = {
    name = es_gather_council_of_elders_reform
    potential = { NOT = { has_country_modifier = gathered_council_of_elders } }
    trigger = { has_country_modifier = unified_religious_rites check_variable = { which = reachmen_authority value = 300 } }
    effect = { 
		add_prestige = -100
		add_country_modifier = {
			name = "gathered_council_of_elders"
			duration = -1
		}
		set_variable = { which = reachmen_authority value = 0 }
	}
    tooltip = es_gather_council_of_elders_reform_tt
}

custom_icon = { name = es_gather_council_of_elders_reform_icon potential = { has_country_modifier = gathered_council_of_elders } }
custom_text_box = { name = es_gather_council_of_elders_reform_text potential = { has_country_modifier = gathered_council_of_elders } }

custom_button = {
    name = es_consolidate_the_tribes_reform
    potential = { NOT = { has_country_modifier = consolidated_the_tribes } }
    trigger = { has_country_modifier = gathered_council_of_elders check_variable = { which = reachmen_authority value = 400 } }
    effect = { 
		create_general = { tradition = 100 }
		add_country_modifier = {
			name = "consolidated_the_tribes"
			duration = -1
		}
		custom_tooltip = every_alliance_member_will_be_vassal
		capital_scope = { add_base_tax = 5 add_base_production = 5 add_base_manpower = 5 }
		hidden_effect = {
			every_subject_country = {
				limit = {
					is_subject_of = ROOT
					is_subject_of_type = alliance_member
				}
				ROOT = { create_subject = { subject_type = vassal subject = PREV } }
			}
		}
		set_variable = { which = reachmen_authority value = 0 }
	}
    tooltip = es_consolidate_the_tribes_reform_tt
}

custom_icon = { name = es_consolidate_the_tribes_reform_icon potential = { has_country_modifier = consolidated_the_tribes } }
custom_text_box = { name = es_consolidate_the_tribes_reform_text potential = { has_country_modifier = consolidated_the_tribes } }

custom_button = {
    name = es_elect_the_king_of_the_reach
    potential = { NOT = { has_country_modifier = elected_the_king_of_the_reach } }
    trigger = { has_country_modifier = consolidated_the_tribes check_variable = { which = reachmen_authority value = 500 } }
    effect = { 
		add_country_modifier = {
			name = "elected_the_king_of_the_reach"
			duration = -1
		}
		if = {
			limit = {
				government = tribal
			}
			change_government = monarchy
			add_government_reform = elective_monarchy_reform
		}
		custom_tooltip = every_alliance_member_will_be_vassal
		custom_tooltip = every_vassal_will_be_inherited
		set_variable = { which = reachmen_authority value = 0 }
		hidden_effect = {
			every_subject_country = {
				limit = {
					is_subject_of = ROOT
					is_subject_of_type = alliance_member
				}
				ROOT = { create_subject = { subject_type = vassal subject = PREV } }
			}
			every_subject_country = {
				limit = {
					is_subject_of = ROOT
					is_subject_of_type = vassal
				}
				overlord = { inherit = PREV }
			}
		}
	}
    tooltip = es_elect_the_king_of_the_reach_tt
}

custom_icon = { name = es_elect_the_king_of_the_reach_icon potential = { has_country_modifier = elected_the_king_of_the_reach } }
custom_text_box = { name = es_elect_the_king_of_the_reach_text potential = { has_country_modifier = elected_the_king_of_the_reach } }

# Actions

custom_button = {
    name = es_increase_development_for_authority
    potential = { 
	}
    trigger = { 
		capital_scope = { NOT = { development = 30 } }
		check_variable = { which = reachmen_authority value = 300 }
		manpower_percentage = 0.95
	}
    effect = {
		capital_scope = {
			add_base_tax = 1
			add_base_production = 1
			add_base_manpower = 1
		}
		add_yearly_manpower = -1.5
		change_variable = { which = reachmen_authority value = -300 }
	}
    tooltip = es_increase_development_for_authority_tt
}

custom_button = {
    name = es_disinherit_for_authority
    potential = { 
	}
    trigger = { 
		check_variable = { which = reachmen_authority value = 250 }
		has_heir = yes
		stability = 1
	}
    effect = {
		kill_heir = { }
		es_remove_stability_1 = yes
		change_variable = { which = reachmen_authority value = -250 }
	}
    tooltip = es_disinherit_for_authority_tt
}

custom_button = {
    name = es_divorce_for_authority
    potential = { 
	}
    trigger = { 
		check_variable = { which = reachmen_authority value = 200 }
		prestige = 15
		has_consort = yes
	}
    effect = {
		remove_consort = yes
		add_prestige = -15
		change_variable = { which = reachmen_authority value = -200 }
	}
    tooltip = es_divorce_for_authority_tt
}

custom_button = {
    name = es_change_deity_for_authority
    potential = { 
	}
    trigger = { 
		check_variable = { which = reachmen_authority value = 150 }
		adm_power = 150
		OR = {
			has_personal_deity = Kyne
			has_personal_deity = Bal
			has_personal_deity = Malacath
			has_personal_deity = Mehrunes
			has_personal_deity = Namira
			has_personal_deity = Hircine
			has_personal_deity = Peryite
			has_personal_deity = Nocturnal
			has_personal_deity = Lorkh
			has_personal_deity = Headsman
			has_personal_deity = Hunter
			has_personal_deity = Snake_in_the_Stars
			has_personal_deity = Witch_in_the_Stars
			has_personal_deity = ge_magnus
			has_personal_deity = ge_mnemo_li
			has_personal_deity = ge_merid_nunda
			has_personal_deity = ge_una
			has_personal_deity = ge_xero_lyg
			has_personal_deity = ge_iana_lor
			has_personal_deity = ge_londa_vera
			has_personal_deity = ge_prime_archon
			has_personal_deity = ge_sheza_rana
			has_personal_deity = ge_unala_se
			has_personal_deity = ge_valia_sha
		}
	}
    effect = {
		custom_tooltip = es_change_deity_for_authority_tooltip
		hidden_effect = {
			# Audax Validator "." Ignore_NEXT
			change_personal_deity = nothing
		}
		add_adm_power = -150
		change_variable = { which = reachmen_authority value = -150 }
	}
    tooltip = es_change_deity_for_authority_tt
}

custom_button = {
    name = es_recruit_warriors_for_authority
    potential = { 
	}
    trigger = { 
		NOT = { army_tradition = 75 }
		NOT = { corruption = 15.0 }
		check_variable = { which = reachmen_authority value = 100 }
	}
    effect = {
		add_army_tradition = 1.0
		add_corruption = 1.5
		change_variable = { which = reachmen_authority value = -100 }
	}
    tooltip = es_recruit_warriors_for_authority_tt
}

#1294 mistrunner_clan
#1297 treeshade_clan
#1351 quicktalon_clan
#3038 ghostsong_clan
#3061 spiritdancer_clan
#3079 black_moon_clan
#3104 deathwing_clan
#7213 stranglehollow_clan
#7221 twisted_briar_clan
#7229 dragonclaw_clan
#7230 hearteater_clan
#7232 starsinger_clan
#7235 stag_heart_clan
#7278 crow_wife_clan
#7289 spiritblood_clan
#7291 spirit_tale_clan

