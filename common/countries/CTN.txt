graphical_culture = easterngfx

color = { 151 23 165 }

revolutionary_colors = { 1 13 2}


historical_idea_groups = {
}

historical_units = {
}

monarch_names = {
    "Noro #0" = 10
    "Virvok #0" = 10
    "Drikkuc #0" = 10
    "Chited #0" = 10
    "Chirruk #0" = 10
    "Caiqqit #0" = 10
    "Turo #0" = 10
    "Bresqacoq #0" = 10
    "Kruidgekrex #0" = 10
    "Rerdosqeg #0" = 10
    "Ziaktag #0" = 10
    "Crusqauz #0" = 10
    "Auqet #0" = 10
    "Eqriz #0" = 10
    "Engru #0" = 10
    "Rikkaux #0" = 10
    "Crungud #0" = 10
    "Quaqqaazzuaq #0" = 10
    "Ezzakkaux #0" = 10
    "Tarvuicre #0" = 10
    "Uqroz #0" = 10
    "Jargec #0" = 10
    "Zeinnon #0" = 10
    "Graiqri #0" = 10
    "Chaqoz #0" = 10
    "Chekag #0" = 10
    "Zusqaat #0" = 10
    "Qerqeicix #0" = 10
    "Irriqaz #0" = 10
    "Drarvaarreit #0" = 10
    "Braigrauq #0" = 10
    "Zirig #0" = 10
    "Xanzaz #0" = 10
    "Aqre #0" = 10
    "Grizzig #0" = 10
    "Guvit #0" = 10
    "Drengic #0" = 10
    "Vatakkiag #0" = 10
    "Orvikkec #0" = 10
    "Ingroriaq #0" = 10
    "Vatrak #0" = 10
    "Nerdok #0" = 10
    "Kijiad #0" = 10
    "Enzac #0" = 10
    "Nurdun #0" = 10
    "Aivug #0" = 10
    "Nanze #0" = 10
    "Gudrucret #0" = 10
    "Krervikruuk #0" = 10
    "Qotrocron #0" = 10
    "Kotzo #0" = 10
    "Traisqiq #0" = 10
    "Javruk #0" = 10
    "Cedreq #0" = 10
    "Nekoq #0" = 10
    "Crirqon #0" = 10
    "Angruq #0" = 10
    "Xekoqa #0" = 10
    "Grejukoz #0" = 10
    "Eikgiaraz #0" = 10
    "Droktid #0" = 10
    "Crodguac #0" = 10
    "Nujeq #0" = 10
    "Chitren #0" = 10
    "Ondruc #0" = 10
    "Trangod #0" = 10
    "Kruudgok #0" = 10
    "Arqirraq #0" = 10
    "Qendriqeq #0" = 10
    "Krudrugrox #0" = 10
    "Noktok #0" = 10
    "Odgiz #0" = 10
    "Rauktug #0" = 10
    "Raagrud #0" = 10
    "Tusqan #0" = 10
    "Irchox #0" = 10
    "Craajot #0" = 10
    "Gannigro #0" = 10
    "Gerrikuk #0" = 10
    "Oqiandrex #0" = 10
    "Guruq #0" = 10
    "Krivrun #0" = 10
    "Kesqiak #0" = 10
    "Zuudjan #0" = 10
    "Cukad #0" = 10
    "Ukzaq #0" = 10
    "Kiqqaz #0" = 10
    "Cizuukuuk #0" = 10
    "Dradgiccek #0" = 10
    "Guqqerix #0" = 10
    "Iakzic #0" = 10
    "Kogruc #0" = 10
    "Krivuax #0" = 10
    "Viaqruq #0" = 10
    "Kraqqa #0" = 10
    "Gujon #0" = 10
    "Narduuq #0" = 10
    "Redronduig #0" = 10
    "Drorruandi #0" = 10
    "Grurqadox #0" = 10

    "Gnahraaq #0" = -10
    "Qaervu #0" = -10
    "Knaashuad #0" = -10
    "Throtid #0" = -10
    "Aingu #0" = -10
    "Gnudris #0" = -10
    "Khrazi #0" = -10
    "Khrudtoqu #0" = -10
    "Xeqqurit #0" = -10
    "Harqokhot #0" = -10
    "Thraerra #0" = -10
    "Khuhnet #0" = -10
    "Konzaa #0" = -10
    "Kututh #0" = -10
    "Vaiken #0" = -10
    "Kugnuh #0" = -10
    "Bhaonzaes #0" = -10
    "Rhedrukkith #0" = -10
    "Ugnegad #0" = -10
    "Cirqishod #0" = -10
    "Navu #0" = -10
    "Honqaeh #0" = -10
    "Driaze #0" = -10
    "Khonqe #0" = -10
    "Thirchoh #0" = -10
    "Xeqqo #0" = -10
    "Kathud #0" = -10
    "Vunqiakki #0" = -10
    "Khuahrezzet #0" = -10
    "Tirgeza #0" = -10
    "Riathae #0" = -10
    "Knozud #0" = -10
    "Rivraod #0" = -10
    "Khrihnid #0" = -10
    "Xoka #0" = -10
    "Throrrit #0" = -10
    "Hazzuq #0" = -10
    "Ghinqizuq #0" = -10
    "Qungadhed #0" = -10
    "Riarruenzu #0" = -10
    "Reirchen #0" = -10
    "Reihnac #0" = -10
    "Gasquh #0" = -10
    "Bhirgeh #0" = -10
    "Ukrud #0" = -10
    "Evo #0" = -10
    "Taeqhaq #0" = -10
    "Kedtaica #0" = -10
    "Kheqrani #0" = -10
    "Gethecac #0" = -10
    "Tervo #0" = -10
    "Rhaagua #0" = -10
    "Erhiaq #0" = -10
    "Gadde #0" = -10
    "Gneqruh #0" = -10
    "Theqreth #0" = -10
    "Khoqhueq #0" = -10
    "Khurrokhua #0" = -10
    "Vorokaos #0" = -10
    "Qekhiccid #0" = -10
    "Aorzec #0" = -10
    "Knorchih #0" = -10
    "Gnarvih #0" = -10
    "Chuerchi #0" = -10
    "Takke #0" = -10
    "Drarhoth #0" = -10
    "Erzis #0" = -10
    "Zuvekkes #0" = -10
    "Rhogukhu #0" = -10
    "Thinnecith #0" = -10
    "erraq #0" = -10
    "Turchain #0" = -10
    "Irzat #0" = -10
    "Qenge #0" = -10
    "Eizzec #0" = -10
    "Gaqhit #0" = -10
    "Zokriad #0" = -10
    "Zahnaindaq #0" = -10
    "Uthuzzai #0" = -10
    "Gherreqaot #0" = -10
    "Rhaqhoq #0" = -10
    "Khrurhi #0" = -10
    "Rhazid #0" = -10
    "Bhidtas #0" = -10
    "Ghevas #0" = -10
    "Iaqran #0" = -10
    "Dronzeth #0" = -10
    "Vikhidoh #0" = -10
    "Kevruzze #0" = -10
    "Chinzekkos #0" = -10
    "Rarchoh #0" = -10
    "Ghivrin #0" = -10
    "Rhonqoth #0" = -10
    "Gutheq #0" = -10
    "Chonnun #0" = -10
    "Irzah #0" = -10
    "Qagnaq #0" = -10
    "Tiknici #0" = -10
    "Rhaoqrukhae #0" = -10
    "Tungundin #0" = -10
}

leader_names = {
    Noro Virvok Drikkuc Chited Chirruk Caiqqit Turo Bresqacoq Kruidgekrex Rerdosqeg Ziaktag Crusqauz Auqet Eqriz Engru Rikkaux Crungud Quaqqaazzuaq Ezzakkaux Tarvuicre Uqroz Jargec Zeinnon Graiqri Chaqoz Chekag Zusqaat Qerqeicix Irriqaz Drarvaarreit Braigrauq Zirig Xanzaz Aqre Grizzig Guvit Drengic Vatakkiag Orvikkec Ingroriaq Vatrak Nerdok Kijiad Enzac Nurdun Aivug Nanze Gudrucret Krervikruuk Qotrocron Kotzo Traisqiq Javruk Cedreq Nekoq Crirqon Angruq Xekoqa Grejukoz Eikgiaraz Droktid Crodguac Nujeq Chitren Ondruc Trangod Kruudgok Arqirraq Qendriqeq Krudrugrox Noktok Odgiz Rauktug Raagrud Tusqan Irchox Craajot Gannigro Gerrikuk Oqiandrex Guruq Krivrun Kesqiak Zuudjan Cukad Ukzaq Kiqqaz Cizuukuuk Dradgiccek Guqqerix Iakzic Kogruc Krivuax Viaqruq Kraqqa Gujon Narduuq Redronduig Drorruandi Grurqadox
}

ship_names = {
    Gnahraaq Qaervu Knaashuad Throtid Aingu Gnudris Khrazi Khrudtoqu Xeqqurit Harqokhot Thraerra Khuhnet Konzaa Kututh Vaiken Kugnuh Bhaonzaes Rhedrukkith Ugnegad Cirqishod Navu Honqaeh Driaze Khonqe Thirchoh Xeqqo Kathud Vunqiakki Khuahrezzet Tirgeza Riathae Knozud Rivraod Khrihnid Xoka Throrrit Hazzuq Ghinqizuq Qungadhed Riarruenzu Reirchen Reihnac Gasquh Bhirgeh Ukrud Evo Taeqhaq Kedtaica Kheqrani Gethecac Tervo Rhaagua Erhiaq Gadde Gneqruh Theqreth Khoqhueq Khurrokhua Vorokaos Qekhiccid Aorzec Knorchih Gnarvih Chuerchi Takke Drarhoth Erzis Zuvekkes Rhogukhu Thinnecith erraq Turchain Irzat Qenge Eizzec Gaqhit Zokriad Zahnaindaq Uthuzzai Gherreqaot Rhaqhoq Khrurhi Rhazid Bhidtas Ghevas Iaqran Dronzeth Vikhidoh Kevruzze Chinzekkos Rarchoh Ghivrin Rhonqoth Gutheq Chonnun Irzah Qagnaq Tiknici Rhaoqrukhae Tungundin
}

army_names = {
    "Army of $PROVINCE$"
}

fleet_names = {
    "Fleet of $PROVINCE$"
}