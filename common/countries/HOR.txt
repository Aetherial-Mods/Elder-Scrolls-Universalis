graphical_culture = northamericagfx

color = { 135 209 38 }

revolutionary_colors = { 10 3 10}


historical_idea_groups = {
}

historical_units = {
}

monarch_names = {
    "Kytisar #0" = 10
    "Gaurmas #0" = 10
    "Laordalen #0" = 10
    "Dehlath #0" = 10
    "Tihravoth #0" = 10
    "Kastudoth #0" = 10
    "Nhamrar #0" = 10
    "Shyhos #0" = 10
    "Turedos #0" = 10
    "Vredrer #0" = 10
    "Tyvmauzas #0" = 10
    "Tedrurak #0" = 10
    "Gamulek #0" = 10
    "Kardauseth #0" = 10
    "Luvnumes #0" = 10
    "Baluris #0" = 10
    "Nauma #0" = 10
    "Kalos #0" = 10
    "Vrytilik #0" = 10
    "Timryvoc #0" = 10
    "Lahlor #0" = 10
    "Banheth #0" = 10
    "Yhlith #0" = 10
    "Renhe #0" = 10
    "Shaormek #0" = 10
    "Lavmizir #0" = 10
    "Nhaatalok #0" = 10
    "Lidrurek #0" = 10
    "Amac #0" = 10
    "Helith #0" = 10
    "Destoc #0" = 10
    "Zuras #0" = 10
    "Mevnec #0" = 10
    "Shysek #0" = 10
    "Shiraoles #0" = 10
    "Liti #0" = 10
    "Baorduris #0" = 10
    "Vrustis #0" = 10
    "Tehrydac #0" = 10
    "Hyrmer #0" = 10
    "Ulraodac #0" = 10
    "Garlelas #0" = 10
    "Shyrlon #0" = 10
    "Lukhades #0" = 10
    "Vimusar #0" = 10
    "Ykuzok #0" = 10
    "Rivmamen #0" = 10
    "Gaunoc #0" = 10
    "Maurlir #0" = 10
    "Naalrures #0" = 10
    "Aulroth #0" = 10
    "Biteth #0" = 10
    "Mitek #0" = 10
    "Nermizar #0" = 10
    "Vylrusin #0" = 10
    "Linhauzec #0" = 10
    "Rikivak #0" = 10
    "Heken #0" = 10
    "Nikunath #0" = 10
    "Bemik #0" = 10
    "Melimor #0" = 10
    "Huhron #0" = 10
    "Dytan #0" = 10
    "Nhylrak #0" = 10
    "Nhyhak #0" = 10
    "Vryvmos #0" = 10
    "Davmuron #0" = 10
    "Nades #0" = 10
    "Taovmek #0" = 10
    "Shehlo #0" = 10
    "Nhysyna #0" = 10
    "Nhevnith #0" = 10
    "Limrath #0" = 10
    "Shevnic #0" = 10
    "Mavmen #0" = 10
    "Nahroc #0" = 10
    "Raurmevan #0" = 10
    "Ninher #0" = 10
    "Lermadoth #0" = 10
    "Raadek #0" = 10
    "Nharda #0" = 10
    "Gaohrec #0" = 10
    "Kenhos #0" = 10
    "Harinir #0" = 10
    "Zaudyroc #0" = 10
    "Kaahlin #0" = 10
    "Ekidos #0" = 10
    "Merlir #0" = 10
    "Vyhris #0" = 10
    "Vrinhaose #0" = 10
    "Ykhik #0" = 10
    "Emrimek #0" = 10
    "Duruvic #0" = 10
    "Lystader #0" = 10
    "Limular #0" = 10
    "Vrekaoric #0" = 10
    "Rurmek #0" = 10
    "Zumrive #0" = 10
    "Dalarac #0" = 10
    "Valros #0" = 10

    "Roseta #0" = -10
    "Nosu #0" = -10
    "Ehaniv #0" = -10
    "Zellihi #0" = -10
    "Dannora #0" = -10
    "Rozabi #0" = -10
    "Orrir #0" = -10
    "Lessidev #0" = -10
    "Ihamen #0" = -10
    "Irovaeh #0" = -10
    "Vonohi #0" = -10
    "Mezaran #0" = -10
    "Errel #0" = -10
    "Sezer #0" = -10
    "Millu #0" = -10
    "Tannur #0" = -10
    "Zillin #0" = -10
    "Erebun #0" = -10
    "Doshi #0" = -10
    "Toha #0" = -10
    "Assiv #0" = -10
    "Zorru #0" = -10
    "Ihebal #0" = -10
    "Arobav #0" = -10
    "Nimadeh #0" = -10
    "Rethovin #0" = -10
    "Sirra #0" = -10
    "Notev #0" = -10
    "Ronnenil #0" = -10
    "Davora #0" = -10
    "Hezohi #0" = -10
    "Sari #0" = -10
    "Lehori #0" = -10
    "Orreh #0" = -10
    "Issih #0" = -10
    "Nahu #0" = -10
    "Tezah #0" = -10
    "Molir #0" = -10
    "Serrarae #0" = -10
    "Harran #0" = -10
    "Lithavi #0" = -10
    "Relonal #0" = -10
    "Ressa #0" = -10
    "Novuh #0" = -10
    "Zehaen #0" = -10
    "Lannanuh #0" = -10
    "Rithev #0" = -10
    "Lethevu #0" = -10
    "Zassel #0" = -10
    "Ovedin #0" = -10
    "Naruh #0" = -10
    "Lasili #0" = -10
    "Nennamih #0" = -10
    "Soshu #0" = -10
    "Lezav #0" = -10
    "Neshebaev #0" = -10
    "Ronnevel #0" = -10
    "Ressin #0" = -10
    "Letev #0" = -10
    "Eshuv #0" = -10
    "Anevi #0" = -10
    "Zoshih #0" = -10
    "Nihe #0" = -10
    "Zihun #0" = -10
    "Miza #0" = -10
    "Tesae #0" = -10
    "Vomavih #0" = -10
    "Ellolav #0" = -10
    "Lothatah #0" = -10
    "Reridu #0" = -10
    "Molluv #0" = -10
    "Zashe #0" = -10
    "Daha #0" = -10
    "Nele #0" = -10
    "Zannomal #0" = -10
    "Lollih #0" = -10
    "Tisi #0" = -10
    "Vahone #0" = -10
    "Dahae #0" = -10
    "Sathan #0" = -10
    "Vellahaen #0" = -10
    "Voradu #0" = -10
    "Dirih #0" = -10
    "Mirrevul #0" = -10
    "Razonah #0" = -10
    "Etemuv #0" = -10
    "Solihar #0" = -10
    "Moru #0" = -10
    "Veselu #0" = -10
    "Tiveduh #0" = -10
    "Save #0" = -10
    "Voshi #0" = -10
    "Vatar #0" = -10
    "Ehin #0" = -10
    "Atun #0" = -10
    "Halu #0" = -10
    "Azin #0" = -10
    "Dalleduv #0" = -10
    "Vivatur #0" = -10
    "Tozara #0" = -10
}

leader_names = {
    Kytisar Gaurmas Laordalen Dehlath Tihravoth Kastudoth Nhamrar Shyhos Turedos Vredrer Tyvmauzas Tedrurak Gamulek Kardauseth Luvnumes Baluris Nauma Kalos Vrytilik Timryvoc Lahlor Banheth Yhlith Renhe Shaormek Lavmizir Nhaatalok Lidrurek Amac Helith Destoc Zuras Mevnec Shysek Shiraoles Liti Baorduris Vrustis Tehrydac Hyrmer Ulraodac Garlelas Shyrlon Lukhades Vimusar Ykuzok Rivmamen Gaunoc Maurlir Naalrures Aulroth Biteth Mitek Nermizar Vylrusin Linhauzec Rikivak Heken Nikunath Bemik Melimor Huhron Dytan Nhylrak Nhyhak Vryvmos Davmuron Nades Taovmek Shehlo Nhysyna Nhevnith Limrath Shevnic Mavmen Nahroc Raurmevan Ninher Lermadoth Raadek Nharda Gaohrec Kenhos Harinir Zaudyroc Kaahlin Ekidos Merlir Vyhris Vrinhaose Ykhik Emrimek Duruvic Lystader Limular Vrekaoric Rurmek Zumrive Dalarac Valros
}

ship_names = {
    Roseta Nosu Ehaniv Zellihi Dannora Rozabi Orrir Lessidev Ihamen Irovaeh Vonohi Mezaran Errel Sezer Millu Tannur Zillin Erebun Doshi Toha Assiv Zorru Ihebal Arobav Nimadeh Rethovin Sirra Notev Ronnenil Davora Hezohi Sari Lehori Orreh Issih Nahu Tezah Molir Serrarae Harran Lithavi Relonal Ressa Novuh Zehaen Lannanuh Rithev Lethevu Zassel Ovedin Naruh Lasili Nennamih Soshu Lezav Neshebaev Ronnevel Ressin Letev Eshuv Anevi Zoshih Nihe Zihun Miza Tesae Vomavih Ellolav Lothatah Reridu Molluv Zashe Daha Nele Zannomal Lollih Tisi Vahone Dahae Sathan Vellahaen Voradu Dirih Mirrevul Razonah Etemuv Solihar Moru Veselu Tiveduh Save Voshi Vatar Ehin Atun Halu Azin Dalleduv Vivatur Tozara
}

army_names = {
    "Army of $PROVINCE$"
}

fleet_names = {
    "Fleet of $PROVINCE$"
}