################
# ESU Factions #
################

es_monks = {
	monarch_power = ADM
	always = yes
	
	allow = {
		NOT = { primary_culture = breton } NOT = { primary_culture = horsemen } NOT = { primary_culture = reachmen }
	}
	
	modifier = {
		tolerance_heathen = 1.5
		tolerance_heretic = 2.5
		warscore_cost_vs_other_religion = 0.10
	}
}

es_clergy = {
	monarch_power = ADM
	always = yes
	
	allow = {
		NOT = { primary_culture = breton } NOT = { primary_culture = horsemen } NOT = { primary_culture = reachmen }
	}

	modifier = {
		global_missionary_strength = 0.015
		tolerance_own = 1
		missionary_maintenance_cost = 0.25
	}
}

es_governors = {
	monarch_power = ADM
	always = yes
	
	allow = {
		NOT = { primary_culture = breton } NOT = { primary_culture = horsemen } NOT = { primary_culture = reachmen }
	}

	modifier = {
		culture_conversion_time = -0.15
		state_maintenance_modifier = -0.25
		liberty_desire_from_subject_development = 0.10
	}
}

es_bureaucrats = {
	monarch_power = ADM
	always = yes
	
	allow = {
		NOT = { primary_culture = breton } NOT = { primary_culture = horsemen } NOT = { primary_culture = reachmen }
	}

	modifier = {
		stability_cost_modifier = -0.1
		governing_capacity_modifier = 0.25
		advisor_cost = 0.15
	}
}

es_aristocrats = {
	monarch_power = ADM
	always = yes
	
	allow = {
		NOT = { primary_culture = breton } NOT = { primary_culture = horsemen } NOT = { primary_culture = reachmen }
	}

	modifier = {
		unjustified_demands = -0.10
		diplomatic_annexation_cost = -0.25
		global_unrest = 1.5
	}
}

#######################################################################

es_monopolists = {
	monarch_power = DIP
	always = yes
	
	allow = {
		NOT = { primary_culture = breton } NOT = { primary_culture = horsemen } NOT = { primary_culture = reachmen }
	}

	modifier = {
		global_own_trade_power = 0.1
		embargo_efficiency = 0.25
		global_institution_spread = -0.15
	}
}

es_traders = {
	monarch_power = DIP
	always = yes
	
	allow = {
		NOT = { primary_culture = breton } NOT = { primary_culture = horsemen } NOT = { primary_culture = reachmen }
	}

	modifier = {
		global_foreign_trade_power = 0.15
		trade_steering = 0.10
		global_spy_defence = -0.25
	}
}

es_merchants = {
	monarch_power = DIP
	always = yes
	
	allow = {
		NOT = { primary_culture = breton } NOT = { primary_culture = horsemen } NOT = { primary_culture = reachmen }
	}

	modifier = {
		trade_efficiency = 0.10
		trade_range_modifier = 0.25
		ae_impact = 0.15
	}
}

es_guilds = {
	monarch_power = DIP
	always = yes
	
	allow = {
		NOT = { primary_culture = breton } NOT = { primary_culture = horsemen } NOT = { primary_culture = reachmen }
	}

	modifier = {
		global_trade_goods_size_modifier = 0.1
		global_trade_power = 0.15
		embracement_cost = 0.25
	}
}

es_artisans = {
	monarch_power = DIP
	always = yes
	allow = {
		NOT = { primary_culture = breton } NOT = { primary_culture = horsemen } NOT = { primary_culture = reachmen }
	}

	modifier = {
		caravan_power = 0.10
		global_prov_trade_power_modifier = 0.15
		build_cost = 0.25
	}
}

#######################################################################

es_knights = {
	monarch_power = MIL
	always = yes
	allow = {
		NOT = { primary_culture = breton } NOT = { primary_culture = horsemen } NOT = { primary_culture = reachmen }
	}

	modifier = {
		defensiveness = 0.10
		garrison_size = 0.15
		fort_maintenance_modifier = 0.25
	}
}

es_traditionalists = {
	monarch_power = MIL
	always = yes
	
	allow = {
		NOT = { primary_culture = breton } NOT = { primary_culture = horsemen } NOT = { primary_culture = reachmen }
	}

	modifier = {
		global_manpower_modifier = 0.1
		global_sailors_modifier = 0.15
		merc_maintenance_modifier = 0.25
	}
}

es_generals = {
	monarch_power = MIL
	always = yes
	
	allow = {
		NOT = { primary_culture = breton } NOT = { primary_culture = horsemen } NOT = { primary_culture = reachmen }
	}

	modifier = {
		siege_ability = 0.1
		drill_decay_modifier = -0.15
		core_creation = 0.25
	}
}

es_militarists = {
	monarch_power = MIL
	always = yes
	
	allow = {
		NOT = { primary_culture = breton } NOT = { primary_culture = horsemen } NOT = { primary_culture = reachmen }
	}

	modifier = {
		manpower_recovery_speed = 0.10
		sailors_recovery_speed = 0.15
		drill_gain_modifier = -0.25
	}
}

es_warriors = {
	monarch_power = MIL
	always = yes
	
	allow = {
		NOT = { primary_culture = breton } NOT = { primary_culture = horsemen } NOT = { primary_culture = reachmen }
	}

	modifier = {
		mercenary_manpower = 0.10
		global_regiment_cost = -0.15
		reinforce_cost_modifier = 0.25
	}
}