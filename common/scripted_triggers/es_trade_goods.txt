es_magic_goods = {
	OR = {
		trade_goods = scrolls
		trade_goods = magic_goods
		trade_goods = soul_gems
	}
}

es_has_metal_goods = {
	custom_trigger_tooltip = {
		tooltip = es_has_metal_goods_tooltip
		OR = {
			trade_goods = dwemer_metal
			trade_goods = iron
			trade_goods = copper
			trade_goods = gold
			trade_goods = orichalcum
			trade_goods = moonstone
			trade_goods = quicksilver
			trade_goods = silver
			trade_goods = corundum
			trade_goods = mithril
			trade_goods = adamantium
			trade_goods = rubedite
			trade_goods = platinum
			trade_goods = chromium
			trade_goods = cobalt
			trade_goods = cassiterite
			trade_goods = calcinium
			trade_goods = galatite
			trade_goods = electrum
			trade_goods = antimony
			trade_goods = argentum
			trade_goods = dibellium
			trade_goods = iridium
			trade_goods = madness_ore
			trade_goods = manganese
			trade_goods = molybdenum
			trade_goods = nickel
			trade_goods = orgnium
			trade_goods = palladium
			trade_goods = pewter
			trade_goods = sphalerite
			trade_goods = stalhrim
			trade_goods = starmetal
			trade_goods = terne
			trade_goods = titanium
			trade_goods = voidstone
			trade_goods = wispmetal
			trade_goods = zinc
			trade_goods = zircon
		}
	}
}