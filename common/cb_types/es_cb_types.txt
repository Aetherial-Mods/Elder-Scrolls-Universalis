cb_gn_empire = {
	valid_for_subject = no
	
	prerequisites_self = {
		NOT = { has_country_modifier = daedric_invasion }
		is_great_power = yes
	}
	
	prerequisites = {
		
		NOT = { truce_with = FROM }
		NOT = { alliance_with = FROM }
		FROM = { is_great_power = yes }
	}

	war_goal = ge_empire
}

cb_native_reservation = {
	valid_for_subject = no
	
	prerequisites_self = {
		 NOT = { government = native }
	}
	
	prerequisites = {
		NOT = { truce_with = FROM }
		NOT = { alliance_with = FROM }
		FROM = {
			is_neighbor_of = ROOT
			government = native
		}
	}

	war_goal = ge_native_reservation
}

cb_vassal_annex = {
	valid_for_subject = yes
	
	prerequisites_self = {
		 is_revolution_target = no
		 is_subject = yes
	}
	
	prerequisites = {
		is_neighbor_of = FROM
		FROM = {
			is_subject = yes
		}
	}
	
	attacker_disabled_po = {
		po_release_vassals
		po_release_annexed
		po_return_cores
	}

	war_goal = annex_country
}

cb_es_scenario = {
	valid_for_subject = no

	is_triggered_only = yes

	war_goal = es_scenario
}

cb_es_ayleid_purification = {
	valid_for_subject = no
	
	prerequisites_self = {
		NOT = { culture_group = high_elves_cg }
		OR = {
			tag = TAE
			tag = ORD
			religion = marukhism		   
		}
	}
	
	prerequisites = {
		FROM = {
			is_neighbor_of = ROOT
			primary_culture = ayleid
		}
	}

	war_goal = es_ayleid_purification
}

cb_es_purification = {
	valid_for_subject = no
	
	prerequisites_self = {
		OR = {
			culture_group = cyrodiil_cg
			culture_group = yokudo_redguard_cg
			culture_group = northern_cg
			culture_group = half_blood_cg
			culture_group = imga_cg
			culture_group = transpandomaic_cg
			culture_group = marsh_men_cg
		}
		tag = ORD
	}

	prerequisites = {
		FROM = {
			is_neighbor_of = ROOT
			NOT = { culture_group = cyrodiil_cg }
			NOT = { culture_group = yokudo_redguard_cg }
			NOT = { culture_group = northern_cg }
			NOT = { culture_group = half_blood_cg }
			NOT = { culture_group = imga_cg }
			NOT = { culture_group = transpandomaic_cg }
			NOT = { culture_group = marsh_men_cg }
		}
	}

	war_goal = es_purification
}

cb_es_nordic_feud = {
	valid_for_subject = no
	
	prerequisites_self = {
		has_country_flag = es_12_nordic_feud_flag
		culture_group = northern_cg
	}

	prerequisites = {
		FROM = {
		    is_neighbor_of = ROOT
			culture_group = northern_cg
		}
	}

	war_goal = es_nordic_feud
}

cb_es_tribal_war = {
	valid_for_subject = no
	
	prerequisites_self = {
		culture_group = minotaur_cg
	}

	prerequisites = {
		FROM = {
		    is_neighbor_of = ROOT
			culture_group = minotaur_cg
		}
	}

	war_goal = es_nordic_feud
}

cb_es_witch_hunting = {
	valid_for_subject = no
	
	prerequisites_self = {
		has_country_flag = es_magic_persecution_flag
	}
	
	prerequisites = {
		FROM = {
		    has_country_flag = es_magic_nation_flag
		}
	}

	war_goal = es_witch_hunting
}

cb_es_daedric_invasion = {
	valid_for_subject = no
	
	prerequisites_self = {
		has_country_modifier = daedric_invasion
	}
	
	prerequisites = {
		OR = {
		FROM = { is_neighbor_of = ROOT }
		AND = {
			owns = 3188
			FROM = { owns = 1206 }
		}
		AND = {
			owns = 4075
			FROM = { owns = 1199 }
		}
		AND = {
			owns = 4074
			FROM = { owns = 1376 }
		}
		AND = {
			owns = 4071
			FROM = { owns = 1106 }
		}
		}
	}

	war_goal = es_daedric_invasion
}

cb_es_slave_raid = {
	valid_for_subject = no
	
	prerequisites_self = {
		capital_scope = { superregion = cyrodiil_superregion }
		culture_group = high_elves_cg
	}

	prerequisites = {
		FROM = { 
			OR = {
				culture_group = cyrodiil_cg
				culture_group = ket_keptu_cg
				primary_culture = kreathmen
			}
			is_neighbor_of = ROOT 
		}
	}

	war_goal = es_slave_raid
}

cb_es_slave_raid_bsaerbic = {
	valid_for_subject = no
	
	prerequisites_self = {
		culture_group = high_elves_cg
	}

	prerequisites = {
		FROM = { 
			culture_group = marsh_cg
			is_neighbor_of = ROOT 
		}
	}

	war_goal = es_slave_raid
}

cb_es_slave_raid_chimer = {
	valid_for_subject = no
	
	prerequisites_self = {
		capital_scope = { superregion = morrowind_superregion }
		culture_group = velothi_cg
	}

	prerequisites = {
		FROM = { 
			OR = {
				culture_group = marsh_cg
				culture_group = khajiiti_cg
			}
			is_neighbor_of = ROOT 
		}
	}

	war_goal = es_slave_raid
}

cb_es_slave_raid_altmer = {
	valid_for_subject = no
	
	prerequisites_self = {
		primary_culture = altmer
	}

	prerequisites = {
		FROM = { 
			primary_culture = goblin
			is_neighbor_of = ROOT 
		}
	}

	war_goal = es_slave_raid
}

cb_es_slave_raid_kamal = {
	valid_for_subject = no
	
	prerequisites_self = {
		primary_culture = al_dremoran
	}

	prerequisites = {
		FROM = { 
			OR = {
				culture_group = kamal_cg
				culture_group = tangmo_cg
			}
			is_neighbor_of = ROOT 
		}
	}

	war_goal = es_slave_raid
}

cb_es_slave_estate = {
	valid_for_subject = no
	
	prerequisites_self = {
		has_estate_privilege = estate_slavers_exploration_expedition
	}

	prerequisites = {
		FROM = { 
			NOT = { culture_group = ROOT }
			NOT = { truce_with = ROOT } 
			NOT = { alliance_with = ROOT }
			NOT = { is_subject_of = ROOT }
			is_neighbor_of = ROOT 
		}
	}

	war_goal = es_slave_raid
}

cb_become_elector_of_hre = {
	valid_for_subject = no
	
	prerequisites_self = {
		is_part_of_hre = yes
		is_elector = no
	}
	
	prerequisites = {
		FROM = {
			is_elector = yes
		}
	}

	war_goal = es_become_elector_of_hre
}

cb_tsaesci_hunger = {
	valid_for_subject = yes
	
	prerequisites_self = {
		primary_culture = tsaesci
	}
	
	prerequisites = {
		FROM = {
			is_neighbor_of = ROOT
		}
	}

	war_goal = es_tsaesci_hunger
}

cb_fight_dragon_cultists = {
	valid_for_subject = no
	
	prerequisites_self = {
		culture_group = northern_cg
		NOT = { religion = dragon_cult }
	}
	
	prerequisites = {
		FROM = {
			is_neighbor_of = ROOT
			religion = dragon_cult
		}
	}

	war_goal = es_show_supremacy
}

cb_fight_nords = {
	prerequisites_self = {
		primary_culture = reachmen
	}

	prerequisites = {
		FROM = {
			is_neighbor_of = ROOT
			primary_culture = nord
		}
	}

	war_goal = es_show_supremacy
}

cb_pte_lift_excommunication = {
	valid_for_subject = no
	
	prerequisites_self = {
		religion = tribunal_pantheon
		is_excommunicated = yes
	}

	prerequisites = {
		FROM = {
			religion = tribunal_pantheon
			is_papal_controller = yes
		}
	}

	attacker_disabled_po = {
		po_demand_provinces
		po_revoke_cores
		po_release_vassals
		po_release_annexed
		po_change_religion
		po_form_personal_union
		po_become_vassal
		po_subjugate_vassal
		po_annul_treaties
		po_change_government
		po_revoke_elector
		po_trade_power
		po_humiliate
		po_enforce_rebel_demands
	}

	war_goal = pte_lift_excommunication_war_goal
}

cb_pte_protect_religious_bretheren = {
	valid_for_subject = no
	
	prerequisites_self = {
		is_defender_of_faith = yes
	}
	
	prerequisites = {
		religion_group = FROM
		NOT = { religion = FROM }
		OR = {
			is_neighbor_of = FROM
			NOT = {
				border_distance = {
					who = FROM
					distance = 500
				}
			}
		}
		FROM = {
			any_owned_province = {
				has_owner_religion = no
				religion = ROOT
				is_city = yes
			}
		}
	}

	attacker_disabled_po = {
		po_demand_provinces
		po_form_personal_union
		po_become_vassal
		po_subjugate_vassal
	}

	war_goal = pte_protect_religious_bretheren_war_goal
}

cb_pte_liberation = {
	valid_for_subject = no

	prerequisites = {
		has_opinion_modifier = {
			who = FROM
			modifier = aggressive_expansion
		}
		NOT = {
			has_opinion_modifier = {
				who = FROM
				modifier = aggressive_expansion
				value = -35
			}
		}
		FROM = {
			is_neighbor_of = ROOT
		}
	}

	attacker_disabled_po = {
		po_demand_provinces
		po_form_personal_union
		po_become_vassal
		po_subjugate_vassal
	}

	war_goal = pte_liberation_war_goal
}

cb_ruler_revenge = {
	valid_for_subject = no

	is_triggered_only = yes

	prerequisites = {
		FROM = {
			is_neighbor_of = ROOT
			primary_culture = nord
		}
	}
	
	attacker_disabled_po = {
		po_form_personal_union
		po_become_vassal
		po_subjugate_vassal
	}
	war_goal = ruler_revenge_war_goal
}

cb_reclaim_lost_lands = {
	valid_for_subject = no

	is_triggered_only = yes
	
	prerequisites_self = {
		primary_culture = sload
	}
	
	prerequisites = {
		FROM = {
			is_neighbor_of = ROOT
			primary_culture = altmer
			any_province = {
				superregion = summerset_islands_superregion
			}
		}
	}

	attacker_disabled_po = {
		po_form_personal_union
		po_become_vassal
		po_subjugate_vassal
		po_change_religion
		po_join_empire
		po_transfer_vassals
	}
	war_goal = reclaim_lost_lands_war_goal
}

cb_tribal_hegemony_valenwood = {
	valid_for_subject = no
	
	prerequisites_self = {
		OR = {
			primary_culture = bosmer
			primary_culture = wood_orsimer
			primary_culture = imga
			primary_culture = centaur
			primary_culture = hollow
		}
	}

	prerequisites = {
		FROM = { 
			OR = {
				primary_culture = bosmer
				primary_culture = wood_orsimer
				primary_culture = imga
				primary_culture = centaur
				primary_culture = hollow
			}
		}
	}

	war_goal = es_tribal_hegemony
}

cb_tribal_hegemony_elsweyr = {
	valid_for_subject = no
	
	prerequisites_self = {
		OR = {
			culture_group = khajiiti_cg
			tag = HOW
			tag = TEN
		}
	}

	prerequisites = {
		FROM = { 
			OR = {
				culture_group = khajiiti_cg
				tag = HOW
				tag = TEN
			}
		}
	}

	war_goal = es_tribal_hegemony
}

cb_narfinsel_schism = {
	valid_for_subject = no
	
	prerequisites_self = {
		has_country_flag = country_narfinsel_schism_flag
		primary_culture = ayleid
	}

	prerequisites = {
		FROM = { 
			primary_culture = ayleid
			is_neighbor_of = ROOT
			NOT = { religion_group = ROOT }
		}
	}

	war_goal = es_narfinsel_schism
}

cb_preemptive_war_scripted = {
	is_triggered_only = yes
	valid_for_subject = no
	months = 60
	war_goal = es_protect_borders
}

cb_es_great_raid = {
	valid_for_subject = yes
	
	prerequisites_self = {
		has_country_flag = es_orcish_raid_flag
	}

	prerequisites = {
		FROM = { 
			has_country_flag = es_orcish_fury_flag
		}
	}

	war_goal = es_great_raid
}