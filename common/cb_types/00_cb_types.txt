# INSTRUCTIONS:
# -------------
# is_triggered_only - Triggered from within the code or by event effects. DO NOT REMOVE THE STOCK ONES.
# months - The number of months this CB will be valid. Only works for triggered CBs.
# prerequisites - A condition that automatically activates a CB. Does not work for triggered CBs.
# no_opinion_hit - If yes you get no negative opinion from this
#
# ROOT = attacker
# FROM = target
#
# NOTE: The order in which the peace options are listed is the order in which the AI will normally prioritize them in peace treaties
#
# TRIGGERED - Triggered from within the code or by event effects
# --------------------------------------------------------------

# Restoration of recently broken PU
cb_restore_personal_union = {
	valid_for_subject = no

	is_triggered_only = yes
	months = 240
	
	prerequisites_self = {
		OR = {	
			government = monarchy
			has_reform = presidential_republic_reform
		}
		is_revolutionary = no
	}
	
	prerequisites = {
		FROM = {
			government = monarchy
			is_subject = no
		}
	}

	war_goal = take_capital_personal_union
}

# Defected province
cb_defection = {
	valid_for_subject = no

	is_triggered_only = yes
	months = 12

	war_goal = take_core_defection
}

# Cancelled loan
cb_loan_cancelled = {
	valid_for_subject = no

	is_triggered_only = yes
	months = 12

	war_goal = superiority_loan
}

# Hostile spy discovered
cb_spy_discovered = {
	valid_for_subject = no

	is_triggered_only = yes
	months = 12

	war_goal = superiority_spy
}

# Vassal that broke free
cb_disloyal_vassal = {
	valid_for_subject = no

	is_triggered_only = yes
	months = 120

	war_goal = take_capital_disloyal
}

# A HRE prince has been annexed
cb_hre_attacked = {
	valid_for_subject = no

	is_triggered_only = yes
	months = 60

	war_goal = take_province_hre
}

# War of Honor
cb_insult = {
	valid_for_subject = no

	is_triggered_only = yes

	months = 12

	war_goal = superiority_insult_mutual
}

# Dishonored an alliance
cb_dishonored_call = {
	valid_for_subject = no

	is_triggered_only = yes
	months = 96

	war_goal = superiority_insult
}

# CB for vassalization missions
cb_vassalize_mission = {
	valid_for_subject = no

	is_triggered_only = yes
	months = 120

	war_goal = take_capital_vassalize
}

# CB for fabricated claim on throne. (from events)
cb_fabricated_claims = {
	valid_for_subject = no

	is_triggered_only = yes
	months = 12

	war_goal = take_capital_fabricated_claims
}

# CB for Emperor against HRE members that refuse religious conformance
cb_religious_conformance = {
	valid_for_subject = no

	is_triggered_only = yes
	months = 24

	war_goal = take_capital_conformance
}

# Friction along a border
cb_border_war = {
	valid_for_subject = no

	is_triggered_only = yes
	months = 12

	war_goal = take_border
}

# Trade War (from events and others)
cb_trade_war_triggered = {
	valid_for_subject = no

	is_triggered_only = yes
	months = 24

	war_goal = superiority_trade

	attacker_disabled_po = {
		po_demand_provinces
		po_revoke_cores
		po_release_vassals
		po_release_annexed
		po_return_cores
		po_become_vassal
		po_subjugate_vassal			 
		po_become_tributary_state
		po_subjugate_tributary_state					  
		po_form_personal_union
		po_transfer_vassals
	}
}

cb_trade_conflict = {
	valid_for_subject = no

	is_triggered_only = yes
	months = 24

	war_goal = blockade_ports

	attacker_disabled_po = {
		po_demand_provinces
		po_revoke_cores
		po_release_vassals
		po_release_annexed
		po_return_cores
		po_become_vassal
		po_subjugate_vassal			 
		po_form_personal_union
		po_become_tributary_state
		po_subjugate_tributary_state					  
		po_transfer_vassals
	}
}

cb_trade_conflict_triggered = {
	valid_for_subject = no

	is_triggered_only = yes
	months = 24

	war_goal = blockade_ports_mutual

	attacker_disabled_po = {
		po_demand_provinces
		po_revoke_cores
		po_release_vassals
		po_release_annexed
		po_return_cores
		po_become_vassal
		po_subjugate_vassal
		po_form_personal_union
		po_become_tributary_state
		po_subjugate_tributary_state
		po_transfer_vassals
	}
}
cb_trade_league_conflict = {
	valid_for_subject = no

	prerequisites_self = {
		is_trade_league_leader = yes
	}

	prerequisites = {		  
		can_justify_trade_conflict = FROM
	}
	months = 24

	war_goal = blockade_ports

	attacker_disabled_po = {
		po_demand_provinces
		po_revoke_cores
		po_release_vassals
		po_release_annexed
		po_return_cores
		po_become_vassal
		po_subjugate_vassal			 
		po_become_tributary_state
		po_subjugate_tributary_state					  
		po_form_personal_union
		po_transfer_vassals
	}
}

cb_annex = {
	valid_for_subject = no

	is_triggered_only = yes
	months = 24

	war_goal = annex_country
}

cb_change_government = {
	valid_for_subject = no
	
	prerequisites_self = {
		has_reform = war_against_secularization
	}
	
	prerequisites = {
		is_neighbor_of = FROM
		FROM = { NOT = { government = theocracy } }
	}

	war_goal = war_goal_change_government
}

# AUTOMATIC - Constantly in effect while 'prerequisites' is true
# --------------------------------------------------------------

# Post Great Peasants War CB against HRE peasants
cb_change_government_great_peasants_war = {
	valid_for_subject = no
	
	prerequisites_self = {
		is_part_of_hre = yes
		NOT = { has_reform = peasants_republic_reform }
		OR = {
			is_emperor = yes
			is_neighbor_of = FROM
		}
		NOT = { government = republic }
	}
	
	prerequisites = {
		FROM = {
			has_reform = peasants_republic_reform
			is_part_of_hre = yes
		}
	}

	war_goal = war_goal_change_government
}

cb_peasants_war_for_peasants = {
	valid_for_subject = no
	
	prerequisites_self = {
		has_reform = peasants_republic_reform
		is_part_of_hre = yes
	}
	
	prerequisites = {
		is_neighbor_of = FROM
		FROM = {
			NOT = { government = republic }
			is_part_of_hre = yes
		}
	}
	
	war_goal = war_goal_peasants_change_government	
}
# One OPM can try to humiliate another OPM.
cb_humiliate = {
	valid_for_subject = no
	
	prerequisites_self = {
		government = native
		NOT = {
			num_of_cities = 2
		}
		is_revolutionary = no
	}
	
	prerequisites = {
		is_neighbor_of = FROM
		NOT = {
			truce_with = FROM
			FROM = {
				num_of_cities = 2
			}
		}
	}

	attacker_disabled_po = {
		po_demand_provinces
		po_become_vassal
		po_subjugate_vassal			 
	}

	war_goal = humiliate
}


# Temporary claim CB (mostly used by missions)
cb_conquest = {

	prerequisites = {
		claim = FROM
	}

	war_goal = take_claim
}

# We can take core provinces at no badboy
cb_core = {

	prerequisites = {
		core_claim = FROM
	}

	war_goal = take_core
}

# Independence War
cb_independence_war = {
	exclusive = yes
	independence = yes
	
	prerequisites_self = {
		is_subject_other_than_tributary_trigger = yes
	}
	
	prerequisites = {
		is_subject_of = FROM
		NOT = { is_colonial_nation_of = FROM }
	}

	war_goal = defend_capital_independence
}

# Colonial Independance
cb_colonial_independance_war = {
	exclusive = yes
	independence = yes

	prerequisites_self = {
		is_subject = yes
	}
	
	prerequisites = {
		is_colonial_nation_of = FROM
	}

	war_goal = defend_capital_independence
}

# Nationalism
cb_nationalist = {
	
	prerequisites_self = {
		dip_tech = 91
		is_free_or_tributary_trigger = yes
		is_revolutionary = no
	}
	
	prerequisites = {
		culture_group_claim = FROM	   
	}

	war_goal = take_province_nationalist
}

cb_hegemon = {
	valid_for_subject = no
	
	prerequisites_self = {
		is_hegemon = no
	}
	
	prerequisites = {
		from = { is_hegemon = yes }
	}

	war_goal = take_capital_imperial
}


# War Against the World CB for pirates
cb_war_against_the_world = {
	valid_for_subject = no
	
	prerequisites_self = {
		has_reform = criminal_consortium_reform
		is_free_or_tributary_trigger = yes
		is_revolutionary = no
	}

	war_goal = take_capital_pirates
}						

# Revolutionary CB
cb_revolutionary = {
	valid_for_subject = no
	
	prerequisites_self = {
		NOT = { has_reform = priesthood_rule_reform }
		NOT = { has_reform = nomad_tribe_reform }
		NOT = { has_reform = presidential_republic_reform }				
		NOT = { has_reform = cyrodiilic_empire_reform }
		NOT = { has_reform = colonial_government_reform }
		NOT = { government = native }
		cb_on_government_enemies = yes
		is_revolutionary = no
	}
	
	prerequisites = {
		is_neighbor_of = FROM
		FROM = {
			NOT = { has_reform = priesthood_rule_reform }
			NOT = { has_reform = nomad_tribe_reform }
			NOT = { has_reform = presidential_republic_reform }				
			NOT = { has_reform = cyrodiilic_empire_reform }
			NOT = { has_reform = colonial_government_reform }
			NOT = { government = native }
		}
		OR = {
			AND = {
				government = monarchy
				FROM = { government = republic }
			}
			AND = {
				government = republic
				NOT = { FROM = { government = republic } }
			}
		}
	}

	war_goal = war_goal_change_government_mutual
}

# Colonialist CB
cb_colonial = {
	
	prerequisites_self = {
		is_revolutionary = no
	}
	
	prerequisites = {
		colony_claim = FROM
	}

	war_goal = take_colony
}

# Holy War
cb_crusade = {
	valid_for_subject = no
	
	prerequisites_self = {
		is_revolutionary = no
	}
	
	prerequisites = {
		is_neighbor_of = FROM
		NOT = { religion_group = FROM }
		NOT = {	has_matching_religion = FROM }
		OR = {
			AND = {
				crusade_target = { tag = FROM }
				religion = chimer_pantheon
			}
			cb_on_religious_enemies = yes
			has_reform = chosen_by_the_gods
			has_reform = es_crusade
		}
	}

	war_goal = superiority_crusade
}

# Holy War
cb_crusade_pheasants = {
	valid_for_subject = no
	holy_war = yes		   
	is_triggered_only = yes
	months = 60

	prerequisites = {
		NOT = {
			has_casus_belli = {
				type = cb_crusade
				target = from
			}
		}
		NOT = { religion_group = FROM }
		NOT = {	has_matching_religion = FROM }
	}

	war_goal = superiority_crusade
}

# Defender of the Faith
cb_defender_of_the_faith = {
	valid_for_subject = no
	
	prerequisites_self = {
		is_defender_of_faith = yes
		cb_on_religious_enemies = yes
		is_revolutionary = no
	}
	
	prerequisites = {
		is_neighbor_of = FROM
		NOT = {	has_matching_religion = FROM }
		OR = {
			capital_scope = { is_part_of_hre = no }
			FROM = { capital_scope = { is_part_of_hre = no } }
		}
	}

	war_goal = defend_country_faith
}

# Purging of Heresy
cb_heretic = {
	valid_for_subject = no
	
	prerequisites_self = {
		is_revolutionary = no
	}
	
	prerequisites = {
		is_neighbor_of = FROM
		OR = {
			cb_on_religious_enemies = yes
			FROM = { has_ruler_flag = hussite_heretic }
		}
		religion_group = from
		NOT = {	has_matching_religion = FROM }
		OR = {
			capital_scope = { is_part_of_hre = no }
			FROM = { capital_scope = { is_part_of_hre = no } }
		}
	}

	allowed_provinces = {
		always = yes
	}

	war_goal = superiority_heretic
}

# Excommunication War
cb_excommunication = {
	valid_for_subject = no
	
	prerequisites_self = {
		religion = tribunal_pantheon
		is_excommunicated = no
		is_revolutionary = no
	}
	
	prerequisites = {
		FROM = {
			is_excommunicated = yes
			religion = tribunal_pantheon
			is_neighbor_of = ROOT
		}
	}

	war_goal = take_province_excommunication
}

# Trade War
cb_trade_war = {
	
	prerequisites_self = {
		is_revolutionary = no
	}
	
	prerequisites = {
		OR = {
			AND = {
				trade_embargo_by = FROM
				NOT = { FROM = { trade_embargo_by = ROOT } }
			}
			AND = {
				has_government_attribute = is_merchant_republic
				FROM = { has_government_attribute = is_merchant_republic }
			}
		}
	}

	war_goal = superiority_trade_mutual

	attacker_disabled_po = {
		po_demand_provinces
		po_revoke_cores
		po_release_vassals
		po_release_annexed
		po_return_cores
		po_become_vassal
		po_subjugate_vassal
		po_become_tributary_state
		po_subjugate_tributary_state					  
		po_form_personal_union
		po_transfer_vassals
	}
}

# Trade War for embargoed Trade League members
cb_trade_league_dispute = {

	prerequisites_self = {
		is_trade_league_leader = yes
		is_revolutionary = no
	}

	prerequisites = {
		NOT = { ROOT = { trade_embargo_by = FROM } }
		ROOT = { trade_league_embargoed_by = FROM }
	}

	war_goal = superiority_trade_mutual

	attacker_disabled_po = {
		po_demand_provinces
		po_revoke_cores
		po_release_vassals
		po_release_annexed
		po_return_cores
		po_become_vassal
		po_subjugate_vassal
		po_become_tributary_state
		po_subjugate_tributary_state				  
		po_form_personal_union
		po_transfer_vassals
	}
}

# Imperial Ban
cb_imperial_ban = {
	valid_for_subject = no
	
	prerequisites_self = {
		is_emperor = yes
		is_imperial_ban_allowed = yes
		is_revolutionary = no
	}

	prerequisites = {
		FROM = {
			capital_scope = { is_part_of_hre = no }
			any_owned_province = {
				is_part_of_hre = yes
			}
		}
	}

	war_goal = take_province_ban
}

# Liberate Elector
cb_liberate_elector = {
	valid_for_subject = no
	
	prerequisites_self = {
		capital_scope = { is_part_of_hre = yes }
		is_revolutionary = no
	}

	prerequisites = {
		FROM = {
			any_subject_country = {
				is_elector = yes
			}
		}
	}

	war_goal = superiority_liberation
}

# Coalition CB
cb_super_badboy = {
	coalition = yes
	
	prerequisites_self = {
		num_of_coalition_members = 3
		is_revolutionary = no		
	}

	prerequisites = {
		coalition_target = FROM
		NOT = { FROM = { is_in_coalition_war = yes } }
	}

	war_goal = superiority_punitive
}


# Claim Throne
cb_claim_throne = {
	valid_for_subject = no
	
	prerequisites_self = {
		is_revolutionary = no
	}

	prerequisites = {
		succession_claim = FROM
	}

	war_goal = take_capital_throne
}

# Horde vs Civilized
cb_horde_vs_civ = {
	valid_for_subject = no
	
	prerequisites_self = {
		is_nomad = yes
		is_revolutionary = no
	}

	prerequisites = {
		is_neighbor_of = FROM
		FROM = {
			is_nomad = no
		}
	}

	war_goal = superiority_horde
}

# Tribal vs Tribal Feud
cb_tribal_feud = {
	valid_for_subject = no
	
	prerequisites_self = {
		is_nomad = yes
		is_revolutionary = no
	}
	
	prerequisites = {
		FROM = {
			is_nomad = yes
		}
		is_neighbor_of = FROM
	}

	war_goal = take_province_tribal_feud
}

# Revoke electorate
cb_revoke_electorate = {
	no_opinion_hit = yes
	
	prerequisites_self = {
		is_emperor = yes
		is_revolutionary = no
	}
	
	prerequisites = {
		FROM = {
			is_elector = yes
		}
		NOT = { religion = FROM }
	}

	war_goal = revoke_elector
}

# When someone is privateering in a node that you trade and have some power in.
cb_privateers = {
	
	prerequisites_self = {
		
		is_revolutionary = no
	}
	
	prerequisites = {
		FROM = {
			has_privateers = yes
		}
		OR = {
			home_trade_node = {
				trade_share = {
					country = ROOT
					share = 10
				}
				privateer_power = {
					country = FROM
					share = 1
				}
			}
			any_active_trade_node = {
				trade_share = {
					country = ROOT
					share = 10
				}
				privateer_power = {
					country = FROM
					share = 1
				}
			}
		}
	}

	war_goal = blockade_ports
}

# War in support of rebels
cb_support_rebels = {
	support_rebels = yes
	valid_for_subject = no

	prerequisites = {
		has_spawned_supported_rebels = FROM
	}

	war_goal = take_capital_support_rebels
}

# Revolution
cb_crush_the_revolution = {
	ai_peace_desire = -50
	valid_for_subject = no

	prerequisites_self = {
		is_revolutionary = no
		is_free_or_tributary_trigger = yes
	}
	
	prerequisites = {
		OR = {
			is_neighbor_of = FROM
			NOT = {
				border_distance = {
					who = FROM
					distance = 200
				}
			}
		}
		NOT = { truce_with = FROM }
		FROM = {
			is_revolutionary = yes
		}
	}

	war_goal = take_capital_revolution
}

cb_spread_the_revolution = {
	valid_for_subject = no
	
	prerequisites_self = {
		is_revolutionary = yes
		OR = {
			ai = no
			is_revolution_target = yes
		}
	}
	
	prerequisites = {
		FROM = {
			NOT = { is_subject_of = ROOT }
			is_revolutionary = no			
		}
	}

	war_goal = superiority_revolution
}

cb_annex_the_revolution = {
	valid_for_subject = no
	
	prerequisites_self = {
		is_revolutionary = yes
	}
	
	prerequisites = {
		FROM = {
			NOT = { is_subject_of = ROOT }
			is_revolutionary = yes
		}
	}

	war_goal = superiority_annex_revolution
}

cb_religious_league = {
	league = yes
	ai_peace_desire = -50

	valid_for_subject = no
	
	prerequisites_self = {
		is_in_league_war = no
		is_league_leader = yes
		is_revolutionary = no
	}

	prerequisites = {
		FROM = {
			is_emperor = yes
			is_in_league_war = no
		}
		hre_heretic_religion = ROOT
		is_league_enemy = FROM
	}

	war_goal = superiority_religious_league
}

cb_flower_wars = {

	valid_for_subject = no
	
	prerequisites_self = {
		religion = serpant_king
		is_free_or_tributary_trigger = yes
		is_religion_reformed = no
	}

	prerequisites = {
		FROM = {
			NOT = { is_subject_of = ROOT }
		}
		is_neighbor_of = FROM
	}

	war_goal = take_capital_vassalize
}

cb_maya_expansion = {

	valid_for_subject = no
	
	prerequisites_self = {
		religion = tang_mo_pantheon
		is_free_or_tributary_trigger = yes
		is_religion_reformed = no
	}

	prerequisites = {
		FROM = {
			NOT = { is_subject_of = ROOT }
			religion = tang_mo_pantheon
		}
		is_neighbor_of = FROM
	}

	war_goal = take_capital_mayan
}

# Rival Humiliate CB
cb_humiliate_rotw = {
	valid_for_subject = no
	
	prerequisites_self = {
		is_revolutionary = no
	}
	
	prerequisites = {
		NOT = {
			truce_with = FROM
		}
		is_rival = FROM
	}

	attacker_disabled_po = {
		po_demand_provinces
		po_release_vassals
		po_release_annexed
		po_return_cores
		po_become_vassal
		po_subjugate_vassal
		po_become_tributary_state
		po_subjugate_tributary_state					  
		po_form_personal_union
		po_transfer_vassals
	}

	war_goal = humiliate_rotw
}

# Reunification of China
cb_chinese_unification = {
	valid_for_subject = no
	
	prerequisites_self = {
		is_free_or_tributary_trigger = yes
		OR = {
			is_emperor_of_china = yes
			owns_core_province = 1206
		}
	}

	prerequisites = {		
		FROM = {
			NOT = { is_subject_of = ROOT }
			any_owned_province = {
				superregion = cyrodiil_superregion
			}
			NOT = { has_country_modifier = the_mandate_of_heaven }
		}
		is_neighbor_of = FROM
	}

	war_goal = superiority_chinese_unification
}

# Take the Mandate of Heaven
cb_take_mandate = {
	valid_for_subject = no
	
	prerequisites_self = {
		has_dlc = "Mandate of Heaven"
		is_subject = no
	}

	prerequisites = {
		is_neighbor_of = FROM
		FROM = { is_emperor_of_china = yes }
	}

	war_goal = take_capital_take_mandate
}

# Force migration
cb_force_migration = {

	valid_for_subject = no
	
	prerequisites_self = {
		has_dlc = "Conquest of Paradise"
		OR = {
			is_native_tribe = yes
			is_migratory_tribe = yes
		}
	}

	prerequisites = {
		FROM = {
			is_migratory_tribe = yes
			OR = {
				AND = {
					is_native_tribe = yes
					capital_scope = {
						tribal_land_of = ROOT
					}
				}
				is_native_tribe = no
			}
		}
	}
	war_goal = force_migration
}

# Forced our ally to break alliance
cb_forced_break_alliance = {

	valid_for_subject = no
	is_triggered_only = yes
	months = 120
	
	war_goal = superiority_insult
}

cb_force_tributary = {

	valid_for_subject = no
	
	prerequisites_self = {
		has_dlc = "Mandate of Heaven"
		is_emperor_of_china = yes
	}

	prerequisites = {
		FROM = { is_subject = no }
		is_neighbor_of = FROM
	}
	
	war_goal = take_capital_force_tributary
}

cb_force_tributary_mission = {

	valid_for_subject = no

	is_triggered_only = yes
	months = 300
	
	war_goal = take_capital_force_tributary
}

cb_force_join_hre = {
	valid_for_subject = no
	
	prerequisites_self = {
		is_emperor = yes
		hre_reform_passed = es_emperor_reform_martial_conscription
	}

	prerequisites = {
		ROOT = {
			can_use_peace_treaty = {
				who = FROM
				treaty = po_join_empire
			}
		}
		FROM = {
			religion_group = ROOT
			capital_scope = {
				same_continent = ROOT
				is_part_of_hre = no
			}
			any_owned_province = {
				any_neighbor_province = {
					is_part_of_hre = yes
				}
			}
		}
	}

	war_goal = take_capital_force_join_hre
}

cb_reintegrate_into_hre = {
	valid_for_subject = no
	
	prerequisites_self = {
		is_part_of_hre = yes
	}

	prerequisites = {
		FROM = {
			is_part_of_hre = no
			is_neighbor_of = ROOT
			any_owned_province = {
				is_part_of_hre = yes
			}
		}
	}

	war_goal = take_capital_reintegrate_into_hre
}
# A HRE prince has been annexed
cb_imperial_realm_war = {
	valid_for_subject = no

	is_triggered_only = yes
	months = 12

	call_empire_members = yes

	war_goal = superiority_reichskrieg
}
# Force religion on Heathens
cb_world_crusade = {
	valid_for_subject = no
	
	prerequisites_self = {
		has_reform = religious_committee_reform
		is_revolutionary = no
	}
	
	prerequisites = {
		OR = {
			is_neighbor_of = FROM
			claim = FROM
			cb_on_religious_enemies = yes
			cb_on_primitives = yes
			cb_on_overseas = yes
		}
		NOT = { religion_group = FROM }
		NOT = {	has_matching_religion = FROM }
	}

	war_goal = superiority_world_crusade
}	

cb_native_american_tribal_feud = {
	valid_for_subject = no
	
	prerequisites_self = {
		has_dlc = "Conquest of Paradise"
		is_native_tribe = yes
	}
	
	prerequisites = {
		FROM = {
			is_native_tribe = yes
			if = {
				limit = { 
					capital_scope = {
						tribal_land_of = FROM
					}
				}
				is_neighbor_of = ROOT # This is faster method so let's do this for the general case
			}
			else = {
				any_tribal_land = {
					any_neighbor_province = {
						tribal_land_of = ROOT
					}
				}
			}
		}
	}
	war_goal = take_native_american_land

	attacker_disabled_po = {
		po_become_vassal
		po_subjugate_vassal
		po_become_tributary_state
		po_subjugate_tributary_state
	}
}

cb_push_back_colonizers = {
	valid_for_subject = no
	
	prerequisites_self = {
		has_dlc = "Conquest of Paradise"
		is_native_tribe = yes
	}
	
	prerequisites = {
		FROM = {
			is_native_tribe = no
			OR = {
				is_neighbor_of = ROOT
				any_owned_province = {
					tribal_land_of = ROOT
				}
			}
		}
	}

	war_goal = push_back_colonizers
}		  

# CB for enforcing becoming an elector
cb_enforce_electorate_right = {
	valid_for_subject = no
	
	prerequisites_self = {
		is_part_of_hre = yes
		is_elector = no
		is_emperor = no
		any_subject_country = {
			is_elector = yes
		}
	}
	prerequisites = {
		FROM = {
			is_emperor = yes
		}
	}

	war_goal = war_goal_usurp_electorate
}

# Force religion on Heathens
cb_war_against_heresy = {
	valid_for_subject = no

	prerequisites_self = {
		government = theocracy
		is_revolutionary = no
	}

	prerequisites = {
		is_neighbor_of = FROM
		religion_group = FROM
		NOT = { religion = FROM }
		NOT = {	has_matching_religion = FROM }
		FROM = { government = theocracy }
	}

	war_goal = superiority_world_crusade
}

## This loc error is a false positive
# Holy War
cb_external_perfectionism = {
	valid_for_subject = no
	holy_war = yes

	prerequisites_self = {
		is_revolutionary = no
		full_idea_group = humanist_ideas
		government = theocracy
	}

	prerequisites = {
		is_neighbor_of = FROM
		FROM = { 
			government = theocracy
			NOT = { religion = ROOT }
			NOT = { full_idea_group = humanist_ideas }
		}
	}

	war_goal = superiority_crusade
}

# Force Conversion Only CB - must be fabricated
cb_spread_the_true_faith = {
	valid_for_subject = no
	holy_war = yes
	is_triggered_only = yes
	months = 60

	war_goal = superiority_spread_the_true_faith
}