estate_dark_brotherhood = {
	icon = 7
	color = { 0 0 0 }
	# If true, country will get estate
	trigger = {
		OR = {
			AND = {
				culture_group = velothi_cg
				has_country_flag = supported_mephala_worship
			}
			AND = {
				OR = {
					has_reform = court_of_darkness
					has_reform = assassins_guild
					has_country_flag = es_assassins_guild_flag
				}
				has_country_flag = approached_by_dark_brotherhood
			}
		}
		NOT = {
			has_estate = estate_morag_tong
		}
		capital_scope = {
			continent = Tamriel
		}
	}
	# These scale with loyalty & power
	country_modifier_happy = {
		discovered_relations_impact = -0.25
		defensiveness = 0.25
		diplomatic_annexation_cost = -0.25
	}
	country_modifier_neutral = {
		discovered_relations_impact = -0.1
		defensiveness = 0.10
		diplomatic_annexation_cost = -0.10
	}
	country_modifier_angry = {
		discovered_relations_impact = 0.25
		defensiveness = -0.25
		diplomatic_annexation_cost = 0.25
	}
	land_ownership_modifier = {
		dark_brotherhood_loyalty_modifier = 0.25
	}
	province_independence_weight = {
		factor = 1
		modifier = {
			factor = 1.5
			OR = { 
				has_building = barracks
				has_building = training_fields
			}
		}
		modifier = {
			factor = 1.5
			NOT = { development = 10 }
		}
		modifier = {
			factor = 0.75
			development = 15
		}
		modifier = {
			factor = 0.75
			development = 20
		}
	}
	
	# Influence modifiers
	base_influence = 10
	
	influence_modifier = {
		desc = EST_VAL_OTHER_ESTATE_IN_POWER
		trigger = {
			es_has_any_estate_disaster_active = yes
			es_has_not_own_estate_disaster_active = { estate = estate_dark_brotherhood }
		}
		influence = -40
	}
	
	influence_modifier = {
		desc = EST_GOVERNMENT_MONARCHY
		trigger = {
			government = monarchy
		}
		influence = -5
	}
	influence_modifier = {
		desc = EST_GOVERNMENT_REPUBLIC
		trigger = {
			government = republic
		}
		influence = -10
	}
	influence_modifier = {
		desc = EST_GOVERNMENT_THEOCRACY
		trigger = {
			government = theocracy
		}
		influence = 0
	}
	influence_modifier = {
		desc = EST_GOVERNMENT_TRIBAL
		trigger = {
			government = tribal
		}
		influence = 15
	}
	
	# Loyalty Modifiers
	loyalty_modifier = {
		desc = EST_VAL_OTHER_ESTATE_IN_POWER_LOY
		trigger = {
			es_has_any_estate_disaster_active = yes
			es_has_not_own_estate_disaster_active = { estate = estate_dark_brotherhood }
		}
		loyalty = -20
	}
	
	privileges = {
		estate_dark_brotherhood_elite_entourage
		estate_dark_brotherhood_regulate_arms
		estate_dark_brotherhood_donate_safehouses
		estate_dark_brotherhood_wartime_assassination
		estate_dark_brotherhood_guaranteed_autonomy
		estate_dark_brotherhood_military_leadership
		estate_dark_brotherhood_supremacy_over_the_nobility
		estate_dark_brotherhood_professional_assasins
		estate_dark_brotherhood_share_of_the_spoils
		estate_dark_brotherhood_give_gifts
	}

	######################################################
	# Also add custom names in customizable localization #
	######################################################
	
	custom_name = {
		desc = estate_dark_brotherhood_shadowscales
		trigger = { 
			culture_group = marsh_cg
		}
	}
	custom_name = {
		desc = estate_dark_brotherhood_crimson_scars
		trigger = {
			primary_culture = vampire
		}
	}
	agendas = {
		estate_dark_brotherhood_reduce_war_exhaustion
		estate_dark_brotherhood_recover_stability
		estate_dark_brotherhood_increase_stability
		estate_dark_brotherhood_reduce_overextension
		estate_dark_brotherhood_get_allies
		estate_dark_brotherhood_relations_with_X
		estate_dark_brotherhood_break_coalition
		estate_dark_brotherhood_restore_legitimacy
		estate_dark_brotherhood_hire_advisor
		estate_dark_brotherhood_fire_advisor
		estate_dark_brotherhood_build_fort_15th
		estate_dark_brotherhood_build_barracks
		estate_dark_brotherhood_build_regimental_camp
		estate_dark_brotherhood_build_armory
		estate_dark_brotherhood_increase_autonomy_in_province_x
		estate_dark_brotherhood_three_privileges
		estate_dark_brotherhood_despoil_x
		estate_dark_brotherhood_support_independence
		estate_dark_brotherhood_retake_core
		estate_dark_brotherhood_protect_our_culture
		estate_dark_brotherhood_vassalise_vulnerable_country
		estate_dark_brotherhood_expand_into_x
		estate_dark_brotherhood_complete_conquest_of_x
		estate_dark_brotherhood_build_an_army
		estate_dark_brotherhood_increase_monthly_military_power
		estate_dark_brotherhood_humiliate_rival
		estate_dark_brotherhood_defeat_large_army
		estate_dark_brotherhood_gain_corruption
	}
	influence_from_dev_modifier = 1	#Determines percent influence from development
	independence_government = theocracy
}
