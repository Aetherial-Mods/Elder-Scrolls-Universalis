# Professional Warriors

#fire_damage = 0.15
#shock_damage = 0.15
#morale_damage = 0.15
#shock_damage_received = -0.15
#fire_damage_received = -0.15
#morale_damage_received = -0.15
#land_morale = 0.15
#discipline = 0.10
#infantry_power = 0.15
#cavalry_power = 0.15
#artillery_power = 0.15
#reserves_organisation = 0.15

#reinforce_speed = -0.50
#movement_speed = -0.15
#siege_ability = -0.35
#prestige_from_land = -0.75
#recover_army_morale_speed = -0.15
#cavalry_flanking = -0.35

an-xileel_company = {
    regiments_per_development = 0.1
	cost_modifier = 1.0
    home_province = 1075
	cavalry_weight = 0.30
	artillery_weight = 0.20
	sprites = { argonian_sprite_pack }
    trigger = { NOT = { num_of_hired_mercenary_companies = 4 }
		culture_group = marsh_cg
	}
	modifier = { 
		land_morale = 0.15
		reinforce_speed = -0.50
	}
}

blackrose_knights_company = {
    regiments_per_development = 0.1
	cost_modifier = 1.0
    home_province = 6745
	cavalry_weight = 0.30
	artillery_weight = 0.20
	sprites = { lilmothiit_sprite_pack }
    trigger = { NOT = { num_of_hired_mercenary_companies = 4 }
		culture_group = lilmothiit_cg
	}
	modifier = { 
		cavalry_power = 0.10
		siege_ability = -0.35
	}
}

# Semi-professional warriors

#fire_damage = 0.10
#shock_damage = 0.10
#morale_damage = 0.10
#shock_damage_received = -0.10
#fire_damage_received = -0.10
#morale_damage_received = -0.10
#land_morale = 0.10
#discipline = 0.075
#infantry_power = 0.10
#cavalry_power = 0.10
#artillery_power = 0.10
#reserves_organisation = 0.10

#reinforce_speed = -0.35
#movement_speed = -0.10
#siege_ability = -0.25
#prestige_from_land = -0.50
#recover_army_morale_speed = -0.10
#cavalry_flanking = -0.25

hee_tepsleel_tribe_company = {
    regiments_per_development = 0.075
	cost_modifier = 0.75
    home_province = 1092
	cavalry_weight = 0.25
	artillery_weight = 0.15
	sprites = { argonian_sprite_pack }
    trigger = { NOT = { num_of_hired_mercenary_companies = 4 }
		OR = {
			culture_group = marsh_cg
			culture_group = marsh_men_cg
		}
		capital_scope = { superregion = black_marsh_superregion }
	}
	modifier = { 
		infantry_power = 0.10
		recover_army_morale_speed = -0.10
	}
}

shellbacks_company = {
    regiments_per_development = 0.075
	cost_modifier = 0.75
    home_province = 1106
	cavalry_weight = 0.25
	artillery_weight = 0.15
	sprites = { argonian_sprite_pack }
    trigger = { NOT = { num_of_hired_mercenary_companies = 4 }
		OR = {
			culture_group = marsh_cg
			culture_group = marsh_men_cg
		}
		capital_scope = { superregion = black_marsh_superregion }
	}
	modifier = { 
		fire_damage_received = -0.10
		movement_speed = -0.10
	}
}

silken_ring_company = {
    regiments_per_development = 0.075
	cost_modifier = 0.75
    home_province = 2427
	cavalry_weight = 0.25
	artillery_weight = 0.15
	sprites = { velothi_sprite_pack }
    trigger = { NOT = { num_of_hired_mercenary_companies = 4 }
		OR = {
			religion = mephala_cult
			secondary_religion = mephala_cult
		}
	}
	modifier = { 
		morale_damage = 0.10
		reinforce_speed = -0.35
	}
}


# Bandits & Mercenaries

#fire_damage = 0.05
#shock_damage = 0.05
#morale_damage = 0.05
#shock_damage_received = -0.05
#fire_damage_received = -0.05
#morale_damage_received = -0.05
#land_morale = 0.05
#discipline = 0.05
#infantry_power = 0.05
#cavalry_power = 0.05
#artillery_power = 0.05
#reserves_organisation = 0.05

#reinforce_speed = -0.15
#movement_speed = -0.05
#siege_ability = -0.15
#prestige_from_land = -0.25
#recover_army_morale_speed = -0.05
#cavalry_flanking = -0.15

blackguards_company = {
    regiments_per_development = 0.05
	cost_modifier = 0.5
    home_province = 1091
	cavalry_weight = 0.15
	artillery_weight = 0.05
	sprites = { argonian_sprite_pack }
    trigger = { NOT = { num_of_hired_mercenary_companies = 4 }
		capital_scope = { superregion = black_marsh_superregion }
	}
	modifier = { 
		shock_damage_received = -0.05
		prestige_from_land = -0.25
	}
}