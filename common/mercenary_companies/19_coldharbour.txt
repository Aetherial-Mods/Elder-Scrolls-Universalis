# Professional Warriors

#fire_damage = 0.15
#shock_damage = 0.15
#morale_damage = 0.15
#shock_damage_received = -0.15
#fire_damage_received = -0.15
#morale_damage_received = -0.15
#land_morale = 0.15
#discipline = 0.10
#infantry_power = 0.15
#cavalry_power = 0.15
#artillery_power = 0.15
#reserves_organisation = 0.15

#reinforce_speed = -0.50
#movement_speed = -0.15
#siege_ability = -0.35
#prestige_from_land = -0.75
#recover_army_morale_speed = -0.15
#cavalry_flanking = -0.35

# Semi-professional warriors

#fire_damage = 0.10
#shock_damage = 0.10
#morale_damage = 0.10
#shock_damage_received = -0.10
#fire_damage_received = -0.10
#morale_damage_received = -0.10
#land_morale = 0.10
#discipline = 0.075
#infantry_power = 0.10
#cavalry_power = 0.10
#artillery_power = 0.10
#reserves_organisation = 0.10

#reinforce_speed = -0.35
#movement_speed = -0.10
#siege_ability = -0.25
#prestige_from_land = -0.50
#recover_army_morale_speed = -0.10
#cavalry_flanking = -0.25

# Bandits & Mercenaries

#fire_damage = 0.05
#shock_damage = 0.05
#morale_damage = 0.05
#shock_damage_received = -0.05
#fire_damage_received = -0.05
#morale_damage_received = -0.05
#land_morale = 0.05
#discipline = 0.05
#infantry_power = 0.05
#cavalry_power = 0.05
#artillery_power = 0.05
#reserves_organisation = 0.05

#reinforce_speed = -0.15
#movement_speed = -0.05
#siege_ability = -0.15
#prestige_from_land = -0.25
#recover_army_morale_speed = -0.05
#cavalry_flanking = -0.15