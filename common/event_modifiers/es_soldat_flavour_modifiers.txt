#khajiit fur trade
soldat_stop_fur_trade_prov_modifier = {
	local_unrest = -5
	local_defensiveness = 0.1
	local_manpower_modifier = -1
	province_trade_power_modifier = 0.25
}
soldat_evict_fur_trade_prov_modifier = {
	local_monthly_devastation = 0.5
	local_culture_conversion_cost = -0.4
	local_unrest = 5
	min_local_autonomy = 100
	province_trade_power_modifier = -0.5
}
soldat_legal_fur_trade_prov_modifier = {
	local_monthly_devastation = 1
	local_unrest = 10
	province_trade_power_value = 5
	tax_income = 1.2
	min_local_autonomy = 50
	local_manpower_modifier = -1
	local_sailors_modifier = -1
	local_culture_conversion_cost = 10
}
soldat_legal_fur_trade_prov_old_modifier = {
	local_monthly_devastation = 0.1
	local_unrest = -1
	province_trade_power_modifier = -0.25
	min_local_autonomy = 15
	local_manpower_modifier = -0.25
	local_sailors_modifier = -0.25
}
soldat_extinct_fur_trade_prov_modifier = {
	local_monthly_devastation = -0.1
	local_unrest = -1
	province_trade_power_modifier = -0.1
	min_local_autonomy = 10
	local_manpower_modifier = -0.25
	local_sailors_modifier = -0.25
}
soldat_hunting_fur_trade_prov_modifier = {
	local_manpower_modifier = 0.5
	local_sailors_modifier = 0.5
	local_unrest = 3
}
soldat_hunter_fur_trade_prov_modifier = {
	local_manpower_modifier = 0.25
	local_sailors_modifier = 0.25
	province_trade_power_modifier = 0.1
}
soldat_hunted_fur_trade_prov_modifier = {
	local_monthly_devastation = -0.1
	local_unrest = -5
	province_trade_power_modifier = 0.1
	local_manpower_modifier = 0.25
	local_sailors_modifier = 0.25
}
soldat_evicted_fur_trade_prov_modifier = {
	local_monthly_devastation = -0.1
	local_culture_conversion_cost = 1
	local_unrest = -1
	min_local_autonomy = 30
	province_trade_power_modifier = 0.1
}

soldat_stop_fur_trade_modifier = {
	relation_with_other_culture = 10
	promote_culture_cost = -0.10
}
soldat_evict_fur_trade_modifier = {
	culture_conversion_cost = -0.1
	promote_culture_cost = 0.25
}
soldat_evicted_fur_trade_modifier = {
	promote_culture_cost = -0.25
}
soldat_legal_fur_trade_modifier = {
	diplomatic_reputation = -1
	ae_impact = 0.1
	province_warscore_cost = 0.1
	promote_culture_cost = 0.25
}
soldat_extinct_fur_trade_modifier = {
	promote_culture_cost = 0.25
}
soldat_hunting_fur_trade_modifier = {
	diplomatic_reputation = -1
	ae_impact = 0.25
	province_warscore_cost = -0.25
	promote_culture_cost = 1
}
#other
soldat_village_witch_prov_modifier = {
	province_trade_power_value = 1 #local_colonist_placement_chance = 0.01 #only for the icon, I know it has no effect
	local_monthly_devastation = -0.1
	local_prosperity_growth = 0.1
	local_institution_spread = 0.1
	local_missionary_maintenance_cost = 0.25
}

soldat_village_coast_guard_prov_modifier = {
	local_ship_repair = 0.25
	block_slave_raid = yes
	blockade_force_required = 0.5
	hostile_disembark_speed = 0.5
	local_sailors_modifier = -1
	province_trade_power_modifier = 0.25
}
soldat_village_coast_guard_nearby_prov_modifier = {
	local_ship_repair = 0.1
	block_slave_raid = yes
	blockade_force_required = 0.25
	hostile_disembark_speed = 0.25
	local_sailors_modifier = -0.25
	province_trade_power_modifier = 0.1
}

soldat_hohaho_modifier = {
	global_unrest = -0.5
	improve_relation_modifier = 0.1
	war_exhaustion_cost = -0.05
	land_morale = 0.05
}

soldat_skyisnh_b_modifier = {
	all_power_cost = -0.02
	global_institution_spread = 0.1
}
soldat_skyisnh_c_modifier = {
	land_morale = 0.05
	morale_damage = 0.005
	morale_damage_received = -0.05
}
soldat_skyisnh_e_modifier = {
	global_spy_defence = 0.1
	all_estate_loyalty_equilibrium = 0.1
	all_estate_influence_modifier = -0.05
}
soldat_skyisnh_f_modifier = {
	heir_chance = 0.25
	unjustified_demands = -0.1
}