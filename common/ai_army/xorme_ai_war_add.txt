province = {
    war = {
        active = {
            is_at_war = yes
        }
        eval_add = {
            factor = 2000.0
            modifier = {
                factor = 0.75
                fort_level = 1
            }
            modifier = {
                factor = 1.25
                NOT = { development = 10 }
            }
            modifier = {
                factor = 1.25
                NOT = { development = 25 }
            }
            modifier = {
                factor = 1.25
                OR = {
					has_terrain = mountain
					has_terrain = highlands
					has_terrain = hills
					has_terrain = barrow
					has_terrain = dwemer_stronghold
					has_terrain = mountain_meadows
					has_terrain = orcish_stronghold
				}
            }
            modifier = {
				factor = 1.5
                OR = {
                    has_terrain = glacier
                    has_terrain = desert
                }
            }
            modifier = {
                factor = 1.5
				is_in_capital_area = no
			}
        }
    }
}