#Volkihar (skyrim vampires)

VOL_ideas = {
	start = {
		advisor_cost = -0.20
		reinforce_cost_modifier = -0.33
	}
	
	bonus = {
		governing_capacity_modifier = 0.25
	}
	
	trigger = {
		tag = VOL
		has_country_flag = harkon_uprising_flag
	}
	
	free = yes

	# Thralls
	# Local vampires are powerful enough to have established strongholds and hold captured cattle: prisoners or thralls who they regularly feed on without turning them into vampires.
	vol_ideas_1 = {
		state_maintenance_modifier = -0.33
		morale_damage = 0.10
		global_unrest = 5.0
	}
	# Daughters Of Coldharbour 
	# The first Daughter of Coldharbour can be roughly dated back to at least the early First Era. Lamae Beolfag was a Nedic virgin and priestess of Arkay who was raped by Molag Bal. Legends recount that after the ordeal, Molag Bal shed a droplet of blood upon her brow and left her to die. During the night she passed into death, but as her funeral pyre was still burning, she emerged as the first pure-blood vampire.
	vol_ideas_2 = {
		general_cost = -0.25
		leader_land_shock = 1
		female_advisor_chance = 0.15
	}
	# Sanguinare Vampiris 
	# Similar in nature to Porphyric Hemophilia and Noxiphilic Sanguivoria, victims of the less-common Sanguinare Vampiris will become vampires after a three day incubation period. By itself, the disease negatively affects the victim's health: the victim will experience weakness during the day and acquire a peculiar thirst. On the third night of infection, the victim will turn into a vampire. The disease can easily be cured within the three-day period, but vampirism is notoriously hard to cure. Sanguinare Vampiris is found in the province of Skyrim.
	vol_ideas_3 = {
		shock_damage = 0.15
		fire_damage_received = 0.15
	}
	# Tyranny of the Sun 
	# Lord Harkon became obsessed with the Tyranny of the Sun, a prophecy that foretold a time when the sun would be blotted out of the sky, shrouding the world in eternal darkness. He sought to carry out this prophecy so vampires could rule Tamriel without fear of burning in the sunlight.
	vol_ideas_4 = {
		spy_offence = 0.33
		movement_speed = 0.05
		no_stability_loss_on_monarch_death = yes
	}
   	# Castle Volkihar 
	# Castle Volkihar is an ancient castle perhaps dating back to the Merethic Era or the early First Era. It is located on an isolated island off the Giant's Coast of Haafingar Hold in Skyrim, near the border with High Rock. Long thought lost to the eternal mists of the Sea of Ghosts, the sprawling castle manifesting on the rocky islands north of Icewater Jetty has been seen by fishermen on Skyrim’s northwest coast.
	vol_ideas_5 = {
		defensiveness = 0.15
		hostile_attrition = 1
	}
	# Gargoyle Guardians 
	# Gargoyles are a race of living statues that have been animated by a wizard or vampire. They are a type of golem, and are often found guarding dungeons, castles, and ancient ruins. They can form a stony skin when stationary. Some will remain frozen for long periods of time to make their prey think they are just a statue, before finally bursting out of their stone skin and ambushing their victim. Since they are made of stone, they are resistant to damage to some extent. They have been known to utilize basic magic to absorb the health of those stuck by their claws. Gargoyles sometimes wield melee weapons in combat.
	vol_ideas_6 = {
		cavalry_power = 0.15
	}
	# Vampire's Seduction
	# Farmer is too powerful for Vampire's seduction. The thot has no power over the simple working man.
	vol_ideas_7 = {
		improve_relation_modifier = 0.25
		ae_impact = -0.15
	}
}