# base_price = X			base price in $. (default is 1.0 if not specified
# goldtype = yes/no			This resource works like 'gold' if yes, using prices from mine-value in province instead of supply/demand and baseprice. Also creates gold-inflation

unknown = { base_price = 0 }

###################################################################################################

ebony = { base_price = 5.0 }
gold = { base_price = 0 goldtype = yes }
iron = { base_price = 1.15 }
orichalcum = { base_price = 2.0 }
quicksilver = { base_price = 2.35 }
malachite = { base_price = 1.75 }
moonstone = { base_price = 2.75 }
silver = { base_price = 0 goldtype = yes }
copper = { base_price = 1.5 }
corundum = { base_price = 1.75 }
coal = { base_price = 1.25 }
stalhrim = { base_price = 3.75 }
dwemer_metal = { base_price = 3.0 }

###################################################################################################

crops = { base_price = 1.5 }
fruits_and_vegetables = { base_price = 1.25 }
mushrooms = { base_price = 2.75 }
moon_sugar = { base_price = 4.25 }
herbs = { base_price = 3.0 }
wine = { base_price = 3.5 }
beer = { base_price = 2.35 }
livestock = { base_price = 1.5 }
salt = { base_price = 2.0 }
fish = { base_price = 1.15 }
crabs = { base_price = 3.5 }
algae = { base_price = 1.0 }

###################################################################################################

fur = { base_price = 1.75 }
wool = { base_price = 1.15 }
cloth = { base_price = 2.35 }
silk = { base_price = 3.0 }
leather = { base_price = 1.5 }
weapons_and_armor = { base_price = 4.5 }
potions = { base_price = 3.75 }

###################################################################################################

ivory = { base_price = 2.35 }
corals = { base_price = 1.15 }
pearls = { base_price = 3.5 }
gems = { base_price = 4.25 }
dyes = { base_price = 1.25 }

###################################################################################################

glass = { base_price = 3.5 }
paper = { base_price = 3.75 }
wood = { base_price = 1.15 }
naval_supplies = { base_price = 1.25 }
slaves = { base_price = 2.35 }
aetherium = { base_price = 5.0 }
nirncrux = { base_price = 2.75 }

###################################################################################################

fire_salts = { base_price = 2.0 }
frost_salts = { base_price = 2.75 }
void_salts = { base_price = 3.5 }
vampire_dust = { base_price = 3.0 }
daedra_hearts = { base_price = 8 }
bloodgrass = { base_price = 1.0 }
daedra_silk = { base_price = 5.0 }
clannfear_claws = { base_price = 1.5 }
daedra_venin = { base_price = 4.25 }
harrada_root = { base_price = 1.0 }
daedroth_teeth = { base_price = 3.0 }
scamp_skin = { base_price = 1.5 }
spiddal_stick = { base_price = 1.15 }

#######################################################################

scrolls = { base_price = 7.0 }
magic_goods = { base_price = 5.0 }
soul_gems = { base_price = 4.5 }
stone = { base_price = 1.0 }
honey_and_wax = { base_price = 2.75 }
chitin = { base_price = 3.75 }
sload_soap = { base_price = 6.0 }
eggs = { base_price = 1.5 }
human_flesh = { base_price = 4.25 }
nirnroot = { base_price = 4.5 }
orgnium = { base_price = 2.35 }
mithril = { base_price = 4.5 }
adamantium = { base_price = 6.0 }
milk_of_kynareth = { base_price = 7.0 }
skooma = { base_price = 4.5 }
tobacco = { base_price = 2.75 }
hist_sap = { base_price = 4.25 }
incense_of_mara = { base_price = 3.75 }
ayleid_nose_hash = { base_price = 2.75 }
sleeping_tree_sap = { base_price = 6.0 }
daril = { base_price = 2.35 }
hags_breath = { base_price = 5.0 }

#######################################################################

amber = { base_price = 2.75 }
chalcopyrite = { base_price = 1.15 }
shale = { base_price = 1.5 }
sandstone = { base_price = 1.0 }
lapis_lazuli = { base_price = 4.25 }
marble = { base_price = 4.5 }
ironstone = { base_price = 1.5 }

#######################################################################

ale = { base_price = 2.35 }
mazte = { base_price = 1.5 }
dagoth_brandy = { base_price = 7.0 }
colovian_brandy = { base_price = 6.0 }
cyrodilic_brandy = { base_price = 8.5 }
greef = { base_price = 5.0 }
mead = { base_price = 2.75 }
black_briar_mead = { base_price = 4.25 }
honningbrew_mead = { base_price = 4.25 }
numbskin_mead = { base_price = 2.35 }
sujiamma = { base_price = 8.0 }
flin = { base_price = 4.25 }
alto = { base_price = 3.5 }
argonian_bloodwine = { base_price = 4.5 }
colovian_battlecry = { base_price = 3.0 }
emberbrand_wine = { base_price = 7.0 }
firebrand_wine = { base_price = 10.0 }
frostdew_blanc = { base_price =  2.35 }
julianos_firebelly = { base_price = 3.75 }
shein = { base_price = 2.0 }
sparkling_honeydew = { base_price = 3.75 }
spiced_wine = { base_price = 7.0 }
stumblefoots_reserve = { base_price = 3.15 }
surilie_brothers_wine = { base_price = 5.0 }
west_weald_wine = { base_price = 4.5 }
moonshine = { base_price = 1.5 }
ogres_teeth = { base_price = 6.0 }

#######################################################################

adamantite = { base_price = 3.75 }
aeonstone = { base_price = 10.0 }
antimony = { base_price = 1.5 }
argentum = { base_price = 2.75 }
atronite = { base_price = 2.35 }
aurbic_amber = { base_price = 4.5 }
bloodstone = { base_price = 2.75 }
bones = { base_price = 1.5 }
calcinium = { base_price = 1.75 }
carnelian = { base_price = 2.75 }
cassiterite = { base_price = 1.25 }
chromium = { base_price = 1.75 }
chrysocolla = { base_price = 4.5 }
clay = { base_price = 1.0 }
cobalt = { base_price = 1.5 }
dawn_prism = { base_price = 2.35 }
dibellium = { base_price = 3.0 }
dragon_scales = { base_price = 10.0 }
electrum = { base_price = 1.25 }
epidote = { base_price = 1.75 }
faerite = { base_price = 1.5 }
ferrous_salts = { base_price = 2.0 }
flint = { base_price = 1.15 }
galatite = { base_price = 1.5 }
incense = { base_price = 1.5 }
iridium = { base_price = 7.0 }
jade = { base_price = 2.0 }
larimar = { base_price = 1.75 }
limestone = { base_price = 1.0 }
madness_ore = { base_price = 2.75 }
manganese = { base_price = 1.5 }
molybdenum = { base_price = 1.5 }
nickel = { base_price = 2.0 }
night_pumice = { base_price = 1.75 }
obsidian = { base_price = 2.75 }
ochre = { base_price = 1.0 }
palladium = { base_price = 2.0 }
pewter = { base_price = 1.75 }
platinum = { base_price = 2.0 }
potash = { base_price = 1.15 }
quartz = { base_price = 1.0 }
regulus = { base_price = 2.0 }
rubedite = { base_price = 3.0 }
slaughterstone = { base_price = 1.5 }
sphalerite = { base_price = 1.15 }
starmetal = { base_price = 2.35 }
terne = { base_price = 1.0 }
titanium = { base_price = 1.75 }
turquoise = { base_price = 1.25 }
viridian = { base_price = 1.0 }
voidstone = { base_price = 4.75 }
wispmetal = { base_price = 2.75 }
zinc = { base_price = 1.25 }
zircon = { base_price = 1.75 }