dock = {
	cost = 75			
	time = 12
	build_trigger = {
		has_port = yes
	}
	modifier = {
		local_sailors_modifier = 0.5
	}
	
	on_built = {
		if = { limit = { owner = { NOT = { government = native } } }
			add_development_from_building = {
				building = dock
				type = production
				value = 1
			}
		}
	}
	on_destroyed = {
		remove_development_from_building = {
			building = dock
			type = production
			value = 1
		}
	}
	on_obsolete = {
	}
	
	on_construction_started = { } #Fires when you start the construction,  ROOT - Province FROM - Country that started
	on_construction_canceled = { } #Fires when you cancel the construction, ROOT - Province FROM - Country that canceled
	
	ai_will_do = {
		factor = 25			
		modifier = {
			factor = 0.5
			NOT = { FROM = { navy_tradition = 15 } }			
		}
		modifier = {
			factor = 0.75
			NOT = { FROM = { navy_tradition = 35 } }			
		}
		modifier = {
			factor = 1.25
			FROM = { navy_tradition = 50 }			
		}
		modifier = {
			factor = 1.5
			FROM = { navy_tradition = 75 }			
		}
		modifier = {
			factor = 1.25
			NOT = { owner = { sailors_percentage = 0.35 } }			
		}
		modifier = {
			factor = 0.75
			owner = { sailors_percentage = 0.75 }
		}
		modifier = {
			factor = 0.5
			local_autonomy = 50		
		}
		modifier = {
			factor = 0.5
			is_state = no
		}
		modifier = {
			factor = 5
			base_production = 5
		}
		modifier = {
			factor = 100
			FROM = { treasury = 5000 }
		}
	}
}

drydock = {
	cost = 150
	time = 24
	
	make_obsolete = dock
	
	build_trigger = {
		has_port = yes
	}
	
	modifier = {
		local_sailors_modifier = 1.0
	}
	
	on_built = {
		if = { limit = { owner = { NOT = { government = native } } has_province_flag = built_dev_dock }
			add_development_from_upgrading_or_building = {
				building = drydock
				obsolete_building = dock
				type = production
				value = 2
				upgrade_value = 1
			}
		}
		else = {
			add_development_from_building = {
				building = drydock
				type = production
				value = 2
			}
		}
	}
	on_destroyed = {
		remove_development_from_building = {
			building = drydock
			type = production
			value = 2
		}
	}
	on_obsolete = {
	}
	
	on_construction_started = { } #Fires when you start the construction,  ROOT - Province FROM - Country that started
	on_construction_canceled = { } #Fires when you cancel the construction, ROOT - Province FROM - Country that canceled
	
	ai_will_do = {
		factor = 50			
		modifier = {
			factor = 0.5
			NOT = { FROM = { navy_tradition = 15 } }			
		}
		modifier = {
			factor = 0.75
			NOT = { FROM = { navy_tradition = 35 } }			
		}
		modifier = {
			factor = 1.25
			FROM = { navy_tradition = 50 }			
		}
		modifier = {
			factor = 1.5
			FROM = { navy_tradition = 75 }			
		}
		modifier = {
			factor = 1.25
			NOT = { owner = { sailors_percentage = 0.35 } }			
		}
		modifier = {
			factor = 0.75
			owner = { sailors_percentage = 0.75 }
		}
		modifier = {
			factor = 0.5
			local_autonomy = 50		
		}
		modifier = {
			factor = 0.5
			is_state = no
		}
		modifier = {
			factor = 5
			base_production = 10
		}
		modifier = {
			factor = 100
			FROM = { treasury = 10000 }
		}
	}
}

#######################################################################

shipyard = {
	cost = 75
	time = 12
	build_trigger = {
		has_port = yes
	}
	modifier = {
		naval_forcelimit = 0.5
		ship_recruit_speed = -0.25
		local_ship_repair = 0.25 
	}
	
	on_built = {
		if = { limit = { owner = { NOT = { government = native } } }
			add_development_from_building = {
				building = shipyard
				type = production
				value = 1
			}
		}
	}
	on_destroyed = {
		remove_development_from_building = {
			building = shipyard
			type = production
			value = 1
		}
	}
	on_obsolete = {
	}
	
	on_construction_started = { } #Fires when you start the construction,  ROOT - Province FROM - Country that started
	on_construction_canceled = { } #Fires when you cancel the construction, ROOT - Province FROM - Country that canceled
	
	ai_will_do = {
		factor = 50	
		modifier = {
			factor = 1.25			
			FROM = { navy_size_percentage = 0.75 }
		}
		modifier = {
			factor = 1.35		
			FROM = { navy_size_percentage = 1.0 }
		}
		modifier = {
			factor = 1.5			
			FROM = { navy_size_percentage = 1.25 }
		}
		modifier = {
			factor = 1.75			
			FROM = { navy_size_percentage = 1.5 }
		}
		modifier = {
			factor = 0.5
			NOT = { FROM = { navy_tradition = 15 } }			
		}
		modifier = {
			factor = 0.75
			NOT = { FROM = { navy_tradition = 35 } }			
		}
		modifier = {
			factor = 1.25
			FROM = { navy_tradition = 50 }			
		}
		modifier = {
			factor = 1.5
			FROM = { navy_tradition = 75 }			
		}
		modifier = {
			factor = 0.50
			local_autonomy = 50			
		}
		modifier = {
			factor = 0.50
			is_state = no
		}
		modifier = {
			factor = 5
			base_production = 5
		}
		modifier = {
			factor = 100
			FROM = { treasury = 5000 }
		}
	}
}

grand_shipyard = {
	cost = 150
	time = 24
	
	make_obsolete = shipyard
	
	build_trigger = {
		has_port = yes
	}
	modifier = {
		naval_forcelimit = 1.0
		ship_recruit_speed = -0.50
		local_ship_repair = 0.50
	}
	
	on_built = {
		if = { limit = { owner = { NOT = { government = native } } has_province_flag = built_dev_shipyard }
			add_development_from_upgrading_or_building = {
				building = grand_shipyard
				obsolete_building = shipyard
				type = production
				value = 2
				upgrade_value = 1
			}
		}
		else = {
			add_development_from_building = {
				building = grand_shipyard
				type = production
				value = 2
			}
		}
	}
	on_destroyed = {
		remove_development_from_building = {
			building = grand_shipyard
			type = production
			value = 2
		}
	}
	on_obsolete = {
	}
	
	on_construction_started = { } #Fires when you start the construction,  ROOT - Province FROM - Country that started
	on_construction_canceled = { } #Fires when you cancel the construction, ROOT - Province FROM - Country that canceled
	
	ai_will_do = {
		factor = 100	
		modifier = {
			factor = 1.25			
			FROM = { navy_size_percentage = 0.75 }
		}
		modifier = {
			factor = 1.35		
			FROM = { navy_size_percentage = 1.0 }
		}
		modifier = {
			factor = 1.5			
			FROM = { navy_size_percentage = 1.25 }
		}
		modifier = {
			factor = 1.75			
			FROM = { navy_size_percentage = 1.5 }
		}
		modifier = {
			factor = 0.5
			NOT = { FROM = { navy_tradition = 15 } }			
		}
		modifier = {
			factor = 0.75
			NOT = { FROM = { navy_tradition = 35 } }			
		}
		modifier = {
			factor = 1.25
			FROM = { navy_tradition = 50 }			
		}
		modifier = {
			factor = 1.5
			FROM = { navy_tradition = 75 }			
		}
		modifier = {
			factor = 0.50
			local_autonomy = 50			
		}
		modifier = {
			factor = 0.50
			is_state = no
		}
		modifier = {
			factor = 5
			base_production = 10
		}
		modifier = {
			factor = 100
			FROM = { treasury = 10000 }
		}
	}
}

#######################################################################

coastal_defence = {

	cost = 50
	time = 12
	build_trigger = {
		has_port = yes
	}
	modifier = {
		blockade_force_required = 0.5
		hostile_disembark_speed = 1.0
		hostile_fleet_attrition = 2.5
	}
	
	on_built = {
		if = { limit = { owner = { NOT = { government = native } } }
			add_development_from_building = {
				building = coastal_defence
				type = production
				value = 1
			}
		}
	}
	on_destroyed = {
		remove_development_from_building = {
			building = coastal_defence
			type = production
			value = 1
		}
	}
	on_obsolete = {
	}
	
	on_construction_started = { } #Fires when you start the construction,  ROOT - Province FROM - Country that started
	on_construction_canceled = { } #Fires when you cancel the construction, ROOT - Province FROM - Country that canceled

	ai_will_do = {
		factor = 1
		modifier = {
			factor = 5
			base_production = 5
		}
		modifier = {
			factor = 100
			FROM = { treasury = 5000 }
		}
	}
}

naval_battery = {

	make_obsolete = coastal_defence
	cost = 125
	time = 24
	build_trigger = {
		has_port = yes
	}
	modifier = {
		blockade_force_required = 1.0
		hostile_disembark_speed = 2.5
		hostile_fleet_attrition = 5
	}
	
	on_built = {
		if = { limit = { owner = { NOT = { government = native } } has_province_flag = built_dev_coastal_defenced }
			add_development_from_upgrading_or_building = {
				building = naval_battery
				obsolete_building = coastal_defence
				type = production
				value = 2
				upgrade_value = 1
			}
		}
		else = {
			add_development_from_building = {
				building = naval_battery
				type = production
				value = 2
			}
		}
	}
	on_destroyed = {
		remove_development_from_building = {
			building = naval_battery
			type = production
			value = 2
		}
	}
	on_obsolete = {
	}
	
	on_construction_started = { } #Fires when you start the construction,  ROOT - Province FROM - Country that started
	on_construction_canceled = { } #Fires when you cancel the construction, ROOT - Province FROM - Country that canceled

	ai_will_do = {
		factor = 2.5
		modifier = {
			factor = 5
			base_production = 10
		}
		modifier = {
			factor = 100
			FROM = { treasury = 10000 }
		}
	}
}