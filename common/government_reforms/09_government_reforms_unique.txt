religious_encampment_reform = {
	icon = "religious_encampment"
	allow_normal_conversion = yes
	potential = {
		OR = {
			primary_culture = ashlander_chimer
			primary_culture = ashlander_dunmer
			primary_culture = lilmothiit
		}
		NOT = { has_country_flag = es_settled_morrowind_flag }
		NOT = { has_global_flag = es_settled_morrowind_global_flag }
	}
	trigger = {
		OR = { has_reform = religious_encampment_reform
			OR = {
				primary_culture = ashlander_chimer
				primary_culture = ashlander_dunmer
				primary_culture = lilmothiit
			}
		}
	}
	#legacy_equivalent = nomad_tribe_reform_legacy
	valid_for_nation_designer = yes
	nation_designer_cost = 0
	native_mechanic = yes
	nomad = yes
	
	factions = {
		es_clergy
		es_guilds 
		es_knights
	}
	
	modifiers = {
		global_tax_modifier = 0.10
		land_morale = 0.10
		tolerance_own = 1.5
		warscore_cost_vs_other_religion = -0.25
		priests_influence_modifier = 0.15
	}
	
	conditional = {
		allow = { 
			OR = {
				has_dlc = "The Cossacks"
				has_dlc = "Cradle of Civilization"
				has_dlc = "Domination"
				has_dlc = "Rights of Man"
				has_dlc = "Third Rome" has_dlc = "King of Kings" has_dlc = "Winds of Change"
			}
		}
		government_abilities = {
			egyptian_westernization_mechanic
		}
	}
	
	conditional = {
		allow = { has_dlc = "Conquest of Paradise" }
		allow_migration = no
		custom_attributes = {
			enable_settled_mechanics = yes
			enable_tribal_grazing = no
		}
	}
	
	effect = {
		if = { 
			limit = { 
				NOT = { has_dlc = "Conquest of Paradise" }
				NOT = { has_country_modifier = es_tribal_migration }
			}
			add_country_modifier = { name = "es_tribal_migration" duration = 49275 }
		}
	}
	
	ai = {
		factor = 1
	}
}

merchant_encampment_reform = {
	icon = "merchant_encampment"
	allow_normal_conversion = yes
	potential = {
		OR = {
			primary_culture = ashlander_chimer
			primary_culture = ashlander_dunmer
			primary_culture = lilmothiit
		}
		NOT = { has_country_flag = es_settled_morrowind_flag }
		NOT = { has_global_flag = es_settled_morrowind_global_flag }
	}
	trigger = {
		OR = { has_reform = merchant_encampment_reform
			OR = {
				primary_culture = ashlander_chimer
				primary_culture = ashlander_dunmer
				primary_culture = lilmothiit
			}
		}
	}
	#legacy_equivalent = nomad_tribe_reform_legacy
	valid_for_nation_designer = yes
	nation_designer_cost = 0
	native_mechanic = yes
	nomad = yes
	
	factions = {
		es_clergy
		es_guilds 
		es_knights
	}
	
	modifiers = {
		global_trade_power = 0.10
		production_efficiency = 0.10
		reserves_organisation = 0.15
		improve_relation_modifier = 0.25
		merchants_influence_modifier = 0.15
	}
	
	conditional = {
		allow = { 
			OR = {
				has_dlc = "The Cossacks"
				has_dlc = "Cradle of Civilization"
				has_dlc = "Domination"
				has_dlc = "Rights of Man"
				has_dlc = "Third Rome" has_dlc = "King of Kings" has_dlc = "Winds of Change"
			}
		}
		government_abilities = {
			arabic_trade_influence
		}
	}
	
	conditional = {
		allow = { has_dlc = "Conquest of Paradise" }
		allow_migration = no
		custom_attributes = {
			enable_settled_mechanics = yes
			enable_tribal_grazing = no
		}
	}
	
	effect = {
		if = { 
			limit = { 
				NOT = { has_dlc = "Conquest of Paradise" }
				NOT = { has_country_modifier = es_tribal_migration }
			}
			add_country_modifier = { name = "es_tribal_migration" duration = 49275 }
		}
	}
	
	ai = {
		factor = 1
	}
}

military_encampment_reform = {
	icon = "military_encampment"
	allow_normal_conversion = yes
	potential = {
		OR = {
			primary_culture = ashlander_chimer
			primary_culture = ashlander_dunmer
			primary_culture = lilmothiit
		}
		NOT = { has_country_flag = es_settled_morrowind_flag }
		NOT = { has_global_flag = es_settled_morrowind_global_flag }
	}
	trigger = {
		OR = { has_reform = military_encampment_reform
			OR = {
				primary_culture = ashlander_chimer
				primary_culture = ashlander_dunmer
				primary_culture = lilmothiit
			}
		}
	}
	#legacy_equivalent = nomad_tribe_reform_legacy
	valid_for_nation_designer = yes
	nation_designer_cost = 0
	native_mechanic = yes
	nomad = yes
	
	factions = {
		es_clergy
		es_guilds 
		es_knights
	}
	
	modifiers = {
		global_manpower_modifier = 0.10
		discipline = 0.10
		land_maintenance_modifier = -0.15
		available_province_loot = 0.25
		nobility_influence_modifier = 0.15
	}
	
	conditional = {
		allow = { 
			OR = {
				has_dlc = "The Cossacks"
				has_dlc = "Cradle of Civilization"
				has_dlc = "Domination"
				has_dlc = "Rights of Man"
				has_dlc = "Third Rome" has_dlc = "King of Kings" has_dlc = "Winds of Change"
			}
		}
		government_abilities = {
			blood_gathering_mechanic
		}
	}
	
	conditional = {
		allow = { has_dlc = "Conquest of Paradise" }
		allow_migration = no
		custom_attributes = {
			enable_settled_mechanics = yes
			enable_tribal_grazing = no
		}
	}
	
	effect = {
		if = { 
			limit = { 
				NOT = { has_dlc = "Conquest of Paradise" }
				NOT = { has_country_modifier = es_tribal_migration }
			}
			add_country_modifier = { name = "es_tribal_migration" duration = 49275 }
		}
	}
	
	ai = {
		factor = 1
	}
}

es_settle_lands_reform = {
	icon = "settle_lands"
	allow_normal_conversion = yes
	potential = {
		OR = {
			primary_culture = ashlander_chimer
			primary_culture = ashlander_dunmer
			primary_culture = lilmothiit
		}
		NOT = { has_country_flag = es_settled_morrowind_flag }
		NOT = { has_global_flag = es_settled_morrowind_global_flag }
	}
	trigger = {
		is_year = 190
	}
	effect = {
		hidden_effect = {
			set_country_flag = es_settled_morrowind_flag
			add_legitimacy = 100
			add_war_exhaustion = -10
		}
		add_yearly_manpower = 10.0
		add_years_of_income = 5.0
		add_prestige = 15
		every_tribal_land_province = {
			limit = { NOT = { owner = { exists = yes } } }
			settle_province = ROOT
		}
	}
	ai = {
		factor = 1000
	}
}
	
	
	
	


# + add Driladan Nation
# + add Parikh Tribe invasion disaster