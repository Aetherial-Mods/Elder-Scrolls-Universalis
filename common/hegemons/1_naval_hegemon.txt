naval_hegemon = {
	allow = {
		is_great_power = yes
		num_of_heavy_ship = 100
		NOT = { any_other_great_power = { num_of_heavy_ship = root } }
		NOT = { has_country_modifier = lost_hegemony }
	}
	
	base = {
		#Buffs
		country_diplomatic_power = 1
		colonists = 1
		merchants = 1
	}
	
	# At max 100% tickup.
	scale = {	
		# Buffs
		colonist_placement_chance = 0.05
		naval_morale = 0.10
		reduced_liberty_desire_on_other_continent = 15
		war_exhaustion_cost = -0.20
		global_ship_trade_power = 0.25
		privateer_efficiency = 0.35
		regiment_disembark_speed = 0.50
	}
	
	max = {
		#Buffs
		may_perform_slave_raid_on_same_religion = yes
		cb_on_overseas = yes
		cb_on_primitives = yes
	}
	
}