type = transport

hull_size = 12
base_cannons = 4
blockade = 5
sail_speed = 6.6
sailors = 50
sprite_level = 1
trigger = { primary_culture = redguard  }