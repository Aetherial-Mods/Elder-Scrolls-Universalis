type = light_ship

hull_size = 12.0
base_cannons = 12.0
blockade = 12.0

sail_speed = 11.0
trade_power = 3.0

sailors = 80

sprite_level = 1
trigger = { primary_culture = ogre }