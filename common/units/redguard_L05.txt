type = light_ship
trade_power = 5
hull_size = 24
base_cannons = 30
blockade = 10
sail_speed = 16.5
sailors = 150
sprite_level = 3
trigger = { primary_culture = redguard  }