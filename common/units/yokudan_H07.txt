type = heavy_ship

hull_size = 120
base_cannons = 176
blockade = 8
sail_speed = 11.0
sailors = 1200
sprite_level = 4
trigger = { culture_group = yokudo_redguard_cg  }