type = heavy_ship

hull_size = 45
base_cannons = 60
blockade = 8
sail_speed = 7.7
sailors = 450
sprite_level = 2
trigger = { OR = { culture_group = high_elves_cg culture_group = snow_elves_cg }  }