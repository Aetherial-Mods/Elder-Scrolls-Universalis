type = transport

hull_size = 36
base_cannons = 12
blockade = 5
sail_speed = 9.0
sailors = 100
sprite_level = 3
trigger = { culture_group = fowl_cg  }