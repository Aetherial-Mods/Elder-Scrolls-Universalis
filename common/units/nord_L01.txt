type = light_ship
trade_power = 2.5
hull_size = 10
base_cannons = 13
blockade = 10
sail_speed = 11.0
sailors = 68
sprite_level = 1
trigger = { OR = { primary_culture = nord primary_culture = atmoran }  }