type = transport

hull_size = 48
base_cannons = 16
blockade = 5
sail_speed = 10.0
sailors = 108
sprite_level = 4
trigger = { OR = { primary_culture = nord primary_culture = atmoran }  }