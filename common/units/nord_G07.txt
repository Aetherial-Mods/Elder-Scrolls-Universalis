type = galley

hull_size = 32
base_cannons = 48
blockade = 8
sail_speed = 5.5
sailors = 216
sprite_level = 4
trigger = { OR = { primary_culture = nord primary_culture = atmoran }  }