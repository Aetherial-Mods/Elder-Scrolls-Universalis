type = galley

hull_size = 20
base_cannons = 33
blockade = 8
sail_speed = 5.2
sailors = 150
sprite_level = 3
trigger = { culture_group = yokudo_redguard_cg  }