type = heavy_ship

hull_size = 65.0
base_cannons = 113.0
blockade = 5.0

sail_speed = 11.5

sailors = 900

sprite_level = 5
trigger = { primary_culture = ogre }