type = galley

hull_size = 16.0
base_cannons = 24.0
blockade = 7.0

sail_speed = 6.0

sailors = 95

sprite_level = 2
trigger = { primary_culture = horsemen }