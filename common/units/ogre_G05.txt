type = galley

hull_size = 20.0
base_cannons = 30.0
blockade = 7.0

sail_speed = 8.0

sailors = 125

sprite_level = 3
trigger = { primary_culture = ogre }