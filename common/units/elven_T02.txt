type = transport

hull_size = 18
base_cannons = 6
blockade = 5
sail_speed = 7.7
sailors = 70
sprite_level = 2
trigger = { OR = { culture_group = high_elves_cg culture_group = snow_elves_cg }  }