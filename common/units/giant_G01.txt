type = galley

hull_size = 10
base_cannons = 15
blockade = 8
sail_speed = 4.0
sailors = 75
sprite_level = 1
trigger = { culture_group = giant_cg  }