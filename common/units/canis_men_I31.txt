type = infantry

maneuver = 2.0
offensive_morale = 4.0
defensive_morale = 4.0
offensive_fire = 6.0
defensive_fire = 8.0
offensive_shock = 6.0
defensive_shock = 3.0
trigger = { primary_culture = canis_men }