type = light_ship

hull_size = 24.0
base_cannons = 24.0
blockade = 10.0

sail_speed = 14.0
trade_power = 6.0

sailors = 140

sprite_level = 4
trigger = { primary_culture = bird_men }