type = transport

hull_size = 42
base_cannons = 14
blockade = 5
sail_speed = 9.5
sailors = 110
sprite_level = 4
trigger = { OR = { primary_culture = house_chimer primary_culture = house_dunmer }  }