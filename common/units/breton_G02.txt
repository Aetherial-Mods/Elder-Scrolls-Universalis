type = galley

hull_size = 12
base_cannons = 20
blockade = 8
sail_speed = 4.3
sailors = 90
sprite_level = 2
trigger = { primary_culture = breton  }