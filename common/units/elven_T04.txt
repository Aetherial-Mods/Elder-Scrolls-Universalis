type = transport

hull_size = 30
base_cannons = 10
blockade = 5
sail_speed = 9.4
sailors = 90
sprite_level = 3
trigger = { OR = { culture_group = high_elves_cg culture_group = snow_elves_cg }  }