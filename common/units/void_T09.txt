type = transport

hull_size = 60
base_cannons = 20
blockade = 5
sail_speed = 11.0
sailors = 140
sprite_level = 5
trigger = { culture_group = void_cg  }