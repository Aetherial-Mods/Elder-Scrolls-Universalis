type = galley

hull_size = 24
base_cannons = 36
blockade = 8
sail_speed = 5.0
sailors = 180
sprite_level = 3
trigger = { culture_group = imga_cg  }