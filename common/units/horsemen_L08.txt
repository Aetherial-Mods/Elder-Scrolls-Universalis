type = light_ship

hull_size = 26.0
base_cannons = 26.0
blockade = 10.0

sail_speed = 14.0
trade_power = 6.5

sailors = 150

sprite_level = 5
trigger = { primary_culture = horsemen }