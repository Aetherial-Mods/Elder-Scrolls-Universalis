type = light_ship
trade_power = 6
hull_size = 32
base_cannons = 40
blockade = 10
sail_speed = 17.0
sailors = 200
sprite_level = 4
trigger = { culture_group = canis_men_cg  }