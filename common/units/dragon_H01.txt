type = heavy_ship

hull_size = 35
base_cannons = 50
blockade = 8
sail_speed = 6.5
sailors = 300
sprite_level = 1
trigger = { culture_group = dragon_cg  }