type = transport

hull_size = 48
base_cannons = 16
blockade = 5
sail_speed = 11.0
sailors = 120
sprite_level = 4
trigger = { OR = { culture_group = high_elves_cg culture_group = snow_elves_cg }  }