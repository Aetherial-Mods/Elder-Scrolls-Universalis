type = heavy_ship

hull_size = 30
base_cannons = 40
blockade = 8
sail_speed = 6.0
sailors = 200
sprite_level = 1
trigger = { culture_group = marsh_men_cg  }