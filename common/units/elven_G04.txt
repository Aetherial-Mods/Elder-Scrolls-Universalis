type = galley

hull_size = 20
base_cannons = 30
blockade = 8
sail_speed = 5.2
sailors = 150
sprite_level = 3
trigger = { OR = { culture_group = high_elves_cg culture_group = snow_elves_cg }  }