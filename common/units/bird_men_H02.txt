type = heavy_ship

hull_size = 36.0
base_cannons = 50.0
blockade = 5.0

sail_speed = 8.5

sailors = 300

sprite_level = 2
trigger = { primary_culture = bird_men }