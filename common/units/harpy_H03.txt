type = heavy_ship

hull_size = 40.0
base_cannons = 60.0
blockade = 5.0

sail_speed = 10.0

sailors = 400

sprite_level = 2
trigger = { primary_culture = harpy }