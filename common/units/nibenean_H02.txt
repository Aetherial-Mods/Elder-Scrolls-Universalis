type = heavy_ship

hull_size = 45
base_cannons = 60
blockade = 8
sail_speed = 7.0
sailors = 450
sprite_level = 2
trigger = { primary_culture = nibenean  }