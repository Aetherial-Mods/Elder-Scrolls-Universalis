type = galley

hull_size = 22.0
base_cannons = 33.0
blockade = 8.0

sail_speed = 5.5

sailors = 140

sprite_level = 4
trigger = { primary_culture = harpy }