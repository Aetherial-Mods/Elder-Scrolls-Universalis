type = light_ship

hull_size = 22.0
base_cannons = 22.0
blockade = 10.0

sail_speed = 13.0
trade_power = 6.5

sailors = 130

sprite_level = 4
trigger = { primary_culture = harpy }