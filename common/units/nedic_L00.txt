type = light_ship
trade_power = 2
hull_size = 8
base_cannons = 10
blockade = 10
sail_speed = 10.0
sailors = 50
sprite_level = 1
trigger = { primary_culture = nedic  }