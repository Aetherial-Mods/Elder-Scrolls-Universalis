type = galley

hull_size = 24.0
base_cannons = 36.0
blockade = 7.0

sail_speed = 7.0

sailors = 155

sprite_level = 4
trigger = { primary_culture = harpy }