type = transport

hull_size = 15
base_cannons = 5
blockade = 5
sail_speed = 6.5
sailors = 60
sprite_level = 1
trigger = { OR = { primary_culture = house_chimer primary_culture = house_dunmer }  }