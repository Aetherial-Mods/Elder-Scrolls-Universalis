type = light_ship

hull_size = 10.0
base_cannons = 10.0
blockade = 10.0

sail_speed = 10.0
trade_power = 2.5

sailors = 70

sprite_level = 1
trigger = { primary_culture = harpy }