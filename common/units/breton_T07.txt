type = transport

hull_size = 48
base_cannons = 18
blockade = 5
sail_speed = 10.0
sailors = 120
sprite_level = 4
trigger = { primary_culture = breton  }