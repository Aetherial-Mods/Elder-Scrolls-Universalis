type = light_ship

hull_size = 18.0
base_cannons = 18.0
blockade = 10.0

sail_speed = 12.0
trade_power = 4.0

sailors = 100

sprite_level = 2
trigger = { primary_culture = bird_men }