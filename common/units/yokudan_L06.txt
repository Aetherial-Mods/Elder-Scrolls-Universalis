type = light_ship
trade_power = 5.5
hull_size = 28
base_cannons = 39
blockade = 10
sail_speed = 17.6
sailors = 175
sprite_level = 4
trigger = { culture_group = yokudo_redguard_cg  }