type = light_ship

hull_size = 14.0
base_cannons = 14.0
blockade = 10.0

sail_speed = 11.0
trade_power = 3.5

sailors = 90

sprite_level = 2
trigger = { primary_culture = harpy }