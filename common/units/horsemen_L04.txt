type = light_ship

hull_size = 18.0
base_cannons = 18.0
blockade = 10.0

sail_speed = 13.0
trade_power = 4.5

sailors = 110

sprite_level = 3
trigger = { primary_culture = horsemen }