type = transport

hull_size = 12.0
base_cannons = 4.0
blockade = 3.0

sail_speed = 1.5

sailors = 15

sprite_level = 1
trigger = { primary_culture = ogre }