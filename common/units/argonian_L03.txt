type = light_ship
trade_power = 3.5
hull_size = 16
base_cannons = 20
blockade = 10
sail_speed = 13.0
sailors = 125
sprite_level = 2
trigger = { culture_group = marsh_cg  }