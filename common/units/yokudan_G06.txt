type = galley

hull_size = 28
base_cannons = 46
blockade = 8
sail_speed = 5.8
sailors = 210
sprite_level = 4
trigger = { culture_group = yokudo_redguard_cg  }