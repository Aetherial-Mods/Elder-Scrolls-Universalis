type = light_ship
trade_power = 3
hull_size = 12
base_cannons = 17
blockade = 10
sail_speed = 12.0
sailors = 100
sprite_level = 2
trigger = { primary_culture = breton  }