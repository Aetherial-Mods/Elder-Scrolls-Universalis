type = heavy_ship

hull_size = 150
base_cannons = 220
blockade = 8
sail_speed = 12.1
sailors = 1500
sprite_level = 5
trigger = { culture_group = maormer_cg  }