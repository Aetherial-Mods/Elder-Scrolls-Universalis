type = transport

hull_size = 54
base_cannons = 18
blockade = 5
sail_speed = 11.6
sailors = 130
sprite_level = 5
trigger = { OR = { culture_group = high_elves_cg culture_group = snow_elves_cg }  }