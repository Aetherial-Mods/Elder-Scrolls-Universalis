type = heavy_ship

hull_size = 105
base_cannons = 140
blockade = 8
sail_speed = 9.5
sailors = 1050
sprite_level = 4
trigger = { primary_culture = keptu  }