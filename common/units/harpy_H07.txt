type = heavy_ship

hull_size = 60.0
base_cannons = 101.0
blockade = 6.0

sail_speed = 14.0

sailors = 800

sprite_level = 4
trigger = { primary_culture = harpy }