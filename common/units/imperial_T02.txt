type = transport

hull_size = 20
base_cannons = 6
blockade = 5
sail_speed = 7.0
sailors = 70
sprite_level = 2
trigger = { primary_culture = imperial  }