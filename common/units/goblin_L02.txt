type = light_ship
trade_power = 3
hull_size = 12
base_cannons = 15
blockade = 10
sail_speed = 12.0
sailors = 100
sprite_level = 2
trigger = { culture_group = goblin_cg NOT = { primary_culture = ogre}  }