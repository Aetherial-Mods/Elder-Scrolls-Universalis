type = light_ship
trade_power = 3
hull_size = 12
base_cannons = 15
blockade = 10
sail_speed = 12.0
sailors = 90
sprite_level = 2
trigger = { OR = { primary_culture = nord primary_culture = atmoran }  }