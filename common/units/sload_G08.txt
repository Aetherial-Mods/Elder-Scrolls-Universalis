type = galley

hull_size = 36
base_cannons = 54
blockade = 8
sail_speed = 5.8
sailors = 270
sprite_level = 5
trigger = { culture_group = sload_cg  }