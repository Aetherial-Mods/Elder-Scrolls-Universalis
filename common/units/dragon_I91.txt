type = infantry

maneuver = 4.0
offensive_morale = 11.0
defensive_morale = 12.0
offensive_fire = 12.0
defensive_fire = 11.0
offensive_shock = 10.0
defensive_shock = 9.0
trigger = { primary_culture = dragon }