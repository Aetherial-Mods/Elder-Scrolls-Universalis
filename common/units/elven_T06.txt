type = transport

hull_size = 42
base_cannons = 14
blockade = 5
sail_speed = 10.5
sailors = 110
sprite_level = 4
trigger = { OR = { culture_group = high_elves_cg culture_group = snow_elves_cg }  }