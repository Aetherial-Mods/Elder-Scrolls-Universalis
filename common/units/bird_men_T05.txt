type = transport

hull_size = 23.0
base_cannons = 7.0
blockade = 3.0

sail_speed = 5.0

sailors = 50

sprite_level = 3
trigger = { primary_culture = bird_men }