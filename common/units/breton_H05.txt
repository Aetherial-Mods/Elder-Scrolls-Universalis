type = heavy_ship

hull_size = 90
base_cannons = 132
blockade = 8
sail_speed = 9.0
sailors = 900
sprite_level = 3
trigger = { primary_culture = breton  }