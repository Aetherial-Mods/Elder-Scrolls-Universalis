type = transport

hull_size = 29.0
base_cannons = 10.0
blockade = 3.0

sail_speed = 7.5

sailors = 75

sprite_level = 4
trigger = { primary_culture = harpy }