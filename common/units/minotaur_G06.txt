type = galley

hull_size = 28
base_cannons = 42
blockade = 8
sail_speed = 5.3
sailors = 210
sprite_level = 4
trigger = { culture_group = minotaur_cg  }