type = transport

hull_size = 60
base_cannons = 20
blockade = 5
sail_speed = 12.1
sailors = 140
sprite_level = 5
trigger = { OR = { culture_group = high_elves_cg culture_group = snow_elves_cg }  }