type = galley

hull_size = 16
base_cannons = 24
blockade = 8
sail_speed = 5.0
sailors = 120
sprite_level = 2
trigger = { primary_culture = redguard  }