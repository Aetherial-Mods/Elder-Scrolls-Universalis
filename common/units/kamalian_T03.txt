type = transport

hull_size = 24
base_cannons = 8
blockade = 5
sail_speed = 7.5
sailors = 80
sprite_level = 2
trigger = { culture_group = kamal_cg  }