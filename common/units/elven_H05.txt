type = heavy_ship

hull_size = 90
base_cannons = 120
blockade = 8
sail_speed = 9.9
sailors = 900
sprite_level = 3
trigger = { OR = { culture_group = high_elves_cg culture_group = snow_elves_cg }  }