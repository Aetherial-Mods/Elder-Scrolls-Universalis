type = heavy_ship

hull_size = 70.0
base_cannons = 120.0
blockade = 5.0

sail_speed = 12.0

sailors = 1000

sprite_level = 5
trigger = { primary_culture = ogre }