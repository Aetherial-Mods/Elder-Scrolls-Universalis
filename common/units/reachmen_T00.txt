type = transport

hull_size = 12
base_cannons = 4
blockade = 5
sail_speed = 6.0
sailors = 50
sprite_level = 1
trigger = { OR = { primary_culture = reachmen primary_culture = kreathmen}  }