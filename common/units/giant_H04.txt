type = heavy_ship

hull_size = 75
base_cannons = 100
blockade = 8
sail_speed = 8.0
sailors = 750
sprite_level = 3
trigger = { culture_group = giant_cg  }