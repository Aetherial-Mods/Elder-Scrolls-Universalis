type = light_ship
trade_power = 5.5
hull_size = 28
base_cannons = 35
blockade = 10
sail_speed = 16.0
sailors = 175
sprite_level = 4
trigger = { culture_group = canis_men_cg  }