type = heavy_ship

hull_size = 135
base_cannons = 180
blockade = 8
sail_speed = 10.5
sailors = 1215
sprite_level = 5
trigger = { OR = { primary_culture = nord primary_culture = atmoran }  }