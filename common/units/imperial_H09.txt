type = heavy_ship

hull_size = 165
base_cannons = 200
blockade = 8
sail_speed = 11.0
sailors = 1500
sprite_level = 5
trigger = { primary_culture = imperial  }