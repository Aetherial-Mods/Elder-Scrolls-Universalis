type = transport

hull_size = 54
base_cannons = 20
blockade = 5
sail_speed = 11.6
sailors = 130
sprite_level = 5
trigger = { culture_group = yokudo_redguard_cg  }