type = heavy_ship

hull_size = 135
base_cannons = 180
blockade = 8
sail_speed = 10.5
sailors = 1350
sprite_level = 5
trigger = { culture_group = goblin_cg NOT = { primary_culture = ogre}  }