type = galley

hull_size = 40
base_cannons = 60
blockade = 8
sail_speed = 6.0
sailors = 300
sprite_level = 5
trigger = { culture_group = rat_men_cg  }