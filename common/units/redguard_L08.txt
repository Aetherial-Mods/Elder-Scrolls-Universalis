type = light_ship
trade_power = 6.5
hull_size = 36
base_cannons = 45
blockade = 10
sail_speed = 19.8
sailors = 225
sprite_level = 5
trigger = { primary_culture = redguard  }