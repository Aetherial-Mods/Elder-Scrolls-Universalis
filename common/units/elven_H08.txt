type = heavy_ship

hull_size = 135
base_cannons = 180
blockade = 8
sail_speed = 11.6
sailors = 1350
sprite_level = 5
trigger = { OR = { culture_group = high_elves_cg culture_group = snow_elves_cg }  }