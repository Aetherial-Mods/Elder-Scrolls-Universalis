type = transport

hull_size = 20.0
base_cannons = 5.0
blockade = 3.0

sail_speed = 3.5

sailors = 35

sprite_level = 2
trigger = { primary_culture = bird_men }