type = transport

hull_size = 15.0
base_cannons = 6.0
blockade = 3.0

sail_speed = 2.5

sailors = 25

sprite_level = 2
trigger = { primary_culture = harpy }