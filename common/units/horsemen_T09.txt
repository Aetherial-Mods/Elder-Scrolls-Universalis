type = transport

hull_size = 37.0
base_cannons = 10.0
blockade = 3.0

sail_speed = 11.0

sailors = 100

sprite_level = 5
trigger = { primary_culture = horsemen }