type = galley

hull_size = 20
base_cannons = 30
blockade = 8
sail_speed = 4.8
sailors = 150
sprite_level = 3
trigger = { culture_group = void_cg  }