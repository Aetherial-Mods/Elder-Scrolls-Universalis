type = light_ship
trade_power = 4
hull_size = 20
base_cannons = 25
blockade = 10
sail_speed = 14.0
sailors = 113
sprite_level = 3
trigger = { OR = { primary_culture = nord primary_culture = atmoran }  }