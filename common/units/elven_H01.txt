type = heavy_ship

hull_size = 35
base_cannons = 50
blockade = 8
sail_speed = 7.2
sailors = 300
sprite_level = 1
trigger = { OR = { culture_group = high_elves_cg culture_group = snow_elves_cg }  }