type = light_ship
trade_power = 2.5
hull_size = 11
base_cannons = 13
blockade = 10
sail_speed = 11.0
sailors = 75
sprite_level = 1
trigger = { primary_culture = imperial  }