es_last_bystander = {
	potential = {
		capital_scope = { continent = Tamriel }
		culture_group = northern_cg
		religion = dragon_cult
		NOT = { has_country_flag = es_last_bystander_had_disaster }
		NOT = { is_year = 407 }
	}
	
	can_start = {
		has_country_modifier = es_last_bystander_modifier
	}
	
	can_stop = { 
		is_at_war = yes
	}
	
	progress = {
		modifier = {  
			factor = 50
			NOT = { is_year = 407 }
			NOT = { 
				OR = {
					1342 = { owned_by = ROOT }
					1336 = { owned_by = ROOT }
					3015 = { owned_by = ROOT }
				}
			}
		}
	}
	
	can_end = {
		OR = {
			NOT = { capital_scope = { continent = Tamriel } }
			NOT = { religion = dragon_cult }
			NOT = { culture_group = northern_cg }
			NOT = { has_country_modifier = es_last_bystander_modifier }
			OR = {
				1342 = { owned_by = ROOT }
				1336 = { owned_by = ROOT }
				3015 = { owned_by = ROOT }
			}
		}
	}
	
	modifier = {
		global_prosperity_growth = -1.0
	}
	on_start = es_last_bystander_event.1
	on_end = es_last_bystander_event.2
	
	on_monthly = {
	}
}