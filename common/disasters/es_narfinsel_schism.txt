es_narfinsel_schism_apex = {
	potential = {
		has_country_flag = country_narfinsel_schism_flag
	}

	can_start = {
		has_global_flag = narfinsel_schism_flag
		primary_culture = ayleid
		OR = {
			religion_group = polytheistic_group
			religion_group = daedric_group
		}
	}
	
	can_stop = {
		NOT = { has_global_flag = narfinsel_schism_flag }
	}
	
	progress = {
		modifier = {
			factor = 10
			has_global_flag = narfinsel_schism_flag
		}
	}

	can_end = {
		OR = {
			NOT = { has_global_flag = narfinsel_schism_flag }
			AND = {
				1206 = { OR = { is_empty = yes owner = { NOT = { primary_culture = ayleid } } } }
				1223 = { OR = { is_empty = yes owner = { NOT = { primary_culture = ayleid } } } }
			}
			AND = {
				1206 = { OR = { is_empty = yes owner = { primary_culture = ayleid religion_group = polytheistic_group } } }
				1223 = { OR = { is_empty = yes owner = { primary_culture = ayleid religion_group = polytheistic_group } } }
			}
			AND = {
				1206 = { OR = { is_empty = yes owner = { primary_culture = ayleid religion_group = daedric_group } } }
				1223 = { OR = { is_empty = yes owner = { primary_culture = ayleid religion_group = daedric_group } } }
			}
			AND = {
				1206 = { OR = { is_empty = yes owner = { overlord = { primary_culture = ayleid religion_group = polytheistic_group } } } }
				1223 = { OR = { is_empty = yes owner = { primary_culture = ayleid religion_group = polytheistic_group } } }
			}
			AND = {
				1206 = { OR = { is_empty = yes owner = { primary_culture = ayleid religion_group = daedric_group } } }
				1223 = { OR = { is_empty = yes owner = { overlord = { primary_culture = ayleid religion_group = daedric_group } } } }
			}
		}
	}
	
	modifier = {
		missionary_maintenance_cost = 2.5
		religious_unity = -0.25
		tolerance_own = -5
	}		
	
	on_start = es_5_scenario.2
	on_end = es_5_scenario.3
	
	on_monthly = {
		events = {
		}
		random_events = { 
		}
	}
}