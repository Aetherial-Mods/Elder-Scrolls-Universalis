# pridehome_temple
# senchal_palace
# ashen_scar
# weeping_scar
# temple_of_the_two_moons
# temple_of_the_mourning_springs
# laughing_moons_plantation
# anequina_aqueduct
# moon_gate_of_anequina
# halls_of_colossus

moon_gate_of_anequina = {
	start = 5070
	
	date = 54.01.01
	
	time = { months = 0 }
	
	build_cost = 1000
	can_be_moved = no
	move_days_per_unit_distance = 10
	starting_tier = 0
	
	type = monument

	build_trigger = { 
		OR = {
			AND = {
				culture_group = khajiiti_cg
				OR = {
					has_owner_culture = yes
					has_owner_accepted_culture = yes
				}
			}
			AND = {
				religion = khajiiti_pantheon
				has_owner_religion = yes
			}
		}
	}

	on_built = { }
	on_destroyed = { }

	can_use_modifiers_trigger = {
		OR = {
			AND = {
				culture_group = khajiiti_cg
				OR = {
					has_owner_culture = yes
					has_owner_accepted_culture = yes
				}
			}
			AND = {
				religion = khajiiti_pantheon
				has_owner_religion = yes
			}
		}
	}
	can_upgrade_trigger = {
		OR = {
			AND = {
				culture_group = khajiiti_cg
				OR = {
					has_owner_culture = yes
					has_owner_accepted_culture = yes
				}
			}
			AND = {
				religion = khajiiti_pantheon
				has_owner_religion = yes
			}
		}
	}
	keep_trigger = { always = yes }

	tier_0 = {
		upgrade_time = { months = 0 }
		cost_to_upgrade = { factor = 0 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { }
		on_upgraded = { }
	}

	tier_1 = {
		upgrade_time = { months = 120 }
		cost_to_upgrade = { factor = 1000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { manpower_recovery_speed = 0.05 }
		on_upgraded = { }
	}

	tier_2 = {
		upgrade_time = { months = 240 }
		cost_to_upgrade = { factor = 2500 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { manpower_recovery_speed = 0.10 }
		on_upgraded = { }
	}

	tier_3 = {
		upgrade_time = { months = 360 }
		cost_to_upgrade = { factor = 5000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { manpower_recovery_speed = 0.15 }
		on_upgraded = { }
	}
}

temple_of_the_mourning_springs = {
	start = 5156
	
	date = 54.01.01
	
	time = { months = 0 }
	
	build_cost = 1000
	can_be_moved = no
	move_days_per_unit_distance = 10
	starting_tier = 0
	
	type = monument

	build_trigger = { 
		owner = { is_beast_nation_trigger = yes }
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		}
	}

	on_built = { }
	on_destroyed = { }

	can_use_modifiers_trigger = {
		owner = { is_beast_nation_trigger = yes }
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		} 
	}
	can_upgrade_trigger = {
		owner = { is_beast_nation_trigger = yes }
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		} 
	}
	keep_trigger = { always = yes }

	tier_0 = {
		upgrade_time = { months = 0 }
		cost_to_upgrade = { factor = 0 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { }
		on_upgraded = { }
	}

	tier_1 = {
		upgrade_time = { months = 120 }
		cost_to_upgrade = { factor = 1000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { global_sailors_modifier = 0.10 }
		on_upgraded = { }
	}

	tier_2 = {
		upgrade_time = { months = 240 }
		cost_to_upgrade = { factor = 2500 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { global_sailors_modifier = 0.15 }
		on_upgraded = { }
	}

	tier_3 = {
		upgrade_time = { months = 360 }
		cost_to_upgrade = { factor = 5000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { global_sailors_modifier = 0.25 }
		on_upgraded = { }
	}
}

temple_of_the_two_moons = {
	start = 5135
	
	date = 54.01.01
	
	time = { months = 0 }
	
	build_cost = 1000
	can_be_moved = no
	move_days_per_unit_distance = 10
	starting_tier = 0
	
	type = monument

	build_trigger = { 
		culture_group = khajiiti_cg
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		}
	}

	on_built = { }
	on_destroyed = { }

	can_use_modifiers_trigger = {
		culture_group = khajiiti_cg
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		} 
	}
	can_upgrade_trigger = {
		culture_group = khajiiti_cg
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		} 
	}
	keep_trigger = { always = yes }

	tier_0 = {
		upgrade_time = { months = 0 }
		cost_to_upgrade = { factor = 0 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { }
		on_upgraded = { }
	}

	tier_1 = {
		upgrade_time = { months = 120 }
		cost_to_upgrade = { factor = 1000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { mercantilism_cost = -0.10 }
		on_upgraded = { }
	}

	tier_2 = {
		upgrade_time = { months = 240 }
		cost_to_upgrade = { factor = 2500 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { mercantilism_cost = -0.15 }
		on_upgraded = { }
	}

	tier_3 = {
		upgrade_time = { months = 360 }
		cost_to_upgrade = { factor = 5000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { mercantilism_cost = -0.25 }
		on_upgraded = { }
	}
}

weeping_scar = {
	start = 5240
	
	date = 54.01.01
	
	time = { months = 0 }
	
	build_cost = 1000
	can_be_moved = no
	move_days_per_unit_distance = 10
	starting_tier = 0
	
	type = monument

	build_trigger = { 
		OR = {
			AND = {
				culture = vampire
				OR = {
					has_owner_culture = yes
					has_owner_accepted_culture = yes
				}
			}
			AND = {
				religion = molag_bal_cult
				has_owner_religion = yes
			}
		}
	}

	on_built = { }
	on_destroyed = { }

	can_use_modifiers_trigger = {
		OR = {
			AND = {
				culture = vampire
				OR = {
					has_owner_culture = yes
					has_owner_accepted_culture = yes
				}
			}
			AND = {
				religion = molag_bal_cult
				has_owner_religion = yes
			}
		}
	}
	can_upgrade_trigger = {
		OR = {
			AND = {
				culture = vampire
				OR = {
					has_owner_culture = yes
					has_owner_accepted_culture = yes
				}
			}
			AND = {
				religion = molag_bal_cult
				has_owner_religion = yes
			}
		}
	}
	keep_trigger = { always = yes }

	tier_0 = {
		upgrade_time = { months = 0 }
		cost_to_upgrade = { factor = 0 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { }
		on_upgraded = { }
	}

	tier_1 = {
		upgrade_time = { months = 120 }
		cost_to_upgrade = { factor = 1000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { mercenary_manpower = 0.05 }
		on_upgraded = { }
	}

	tier_2 = {
		upgrade_time = { months = 240 }
		cost_to_upgrade = { factor = 2500 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { mercenary_manpower = 0.10 }
		on_upgraded = { }
	}

	tier_3 = {
		upgrade_time = { months = 360 }
		cost_to_upgrade = { factor = 5000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { mercenary_manpower = 0.15 }
		on_upgraded = { }
	}
}

ashen_scar = {
	start = 5145
	
	date = 54.01.01
	
	time = { months = 0 }
	
	build_cost = 1000
	can_be_moved = no
	move_days_per_unit_distance = 10
	starting_tier = 0
	
	type = monument

	build_trigger = { 
		religion = azura_cult
		has_owner_religion = yes
	}

	on_built = { }
	on_destroyed = { }

	can_use_modifiers_trigger = {
		religion = azura_cult
		has_owner_religion = yes
	}
	can_upgrade_trigger = {
		religion = azura_cult
		has_owner_religion = yes
	}
	keep_trigger = { always = yes }

	tier_0 = {
		upgrade_time = { months = 0 }
		cost_to_upgrade = { factor = 0 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { }
		on_upgraded = { }
	}

	tier_1 = {
		upgrade_time = { months = 120 }
		cost_to_upgrade = { factor = 1000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { war_exhaustion_cost = -0.05 }
		on_upgraded = { }
	}

	tier_2 = {
		upgrade_time = { months = 240 }
		cost_to_upgrade = { factor = 2500 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { war_exhaustion_cost = -0.10 }
		on_upgraded = { }
	}

	tier_3 = {
		upgrade_time = { months = 360 }
		cost_to_upgrade = { factor = 5000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { war_exhaustion_cost = -0.15 }
		on_upgraded = { }
	}
}

senchal_palace = {
	start = 5117
	
	date = 54.01.01
	
	time = { months = 0 }
	
	build_cost = 1000
	can_be_moved = no
	move_days_per_unit_distance = 10
	starting_tier = 0
	
	type = monument

	build_trigger = { 
		owner = { government = republic }
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		}
	}

	on_built = { }
	on_destroyed = { }

	can_use_modifiers_trigger = {
		owner = { government = republic }
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		} 
	}
	can_upgrade_trigger = {
		owner = { government = republic }
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		} 
	}
	keep_trigger = { always = yes }

	tier_0 = {
		upgrade_time = { months = 0 }
		cost_to_upgrade = { factor = 0 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { }
		on_upgraded = { }
	}

	tier_1 = {
		upgrade_time = { months = 120 }
		cost_to_upgrade = { factor = 1000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { global_heretic_missionary_strength = 0.01 }
		on_upgraded = { }
	}

	tier_2 = {
		upgrade_time = { months = 240 }
		cost_to_upgrade = { factor = 2500 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { global_heretic_missionary_strength = 0.015 }
		on_upgraded = { }
	}

	tier_3 = {
		upgrade_time = { months = 360 }
		cost_to_upgrade = { factor = 5000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { global_heretic_missionary_strength = 0.025 }
		on_upgraded = { }
	}
}

pridehome_temple = {
	start = 941
	
	date = 54.01.01
	
	time = { months = 0 }
	
	build_cost = 1000
	can_be_moved = no
	move_days_per_unit_distance = 10
	starting_tier = 0
	
	type = monument

	build_trigger = { 
		religion = khajiiti_pantheon
		has_owner_religion = yes
	}

	on_built = { }
	on_destroyed = { }

	can_use_modifiers_trigger = {
		religion = khajiiti_pantheon
		has_owner_religion = yes
	}
	can_upgrade_trigger = {
		religion = khajiiti_pantheon
		has_owner_religion = yes
	}
	keep_trigger = { always = yes }

	tier_0 = {
		upgrade_time = { months = 0 }
		cost_to_upgrade = { factor = 0 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { }
		on_upgraded = { }
	}

	tier_1 = {
		upgrade_time = { months = 120 }
		cost_to_upgrade = { factor = 1000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { global_heathen_missionary_strength = 0.01 }
		on_upgraded = { }
	}

	tier_2 = {
		upgrade_time = { months = 240 }
		cost_to_upgrade = { factor = 2500 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { global_heathen_missionary_strength = 0.015 }
		on_upgraded = { }
	}

	tier_3 = {
		upgrade_time = { months = 360 }
		cost_to_upgrade = { factor = 5000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { global_heathen_missionary_strength = 0.025 }
		on_upgraded = { }
	}
}

laughing_moons_plantation = {
	start = 5160
	
	date = 54.01.01
	
	time = { months = 0 }
	
	build_cost = 1000
	can_be_moved = no
	move_days_per_unit_distance = 10
	starting_tier = 0
	
	type = monument

	build_trigger = { 
		owner = { is_free_or_tributary_trigger = yes }
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		}
	}

	on_built = { }
	on_destroyed = { }

	can_use_modifiers_trigger = {
		owner = { is_free_or_tributary_trigger = yes }
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		} 
	}
	can_upgrade_trigger = {
		owner = { is_free_or_tributary_trigger = yes }
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		} 
	}
	keep_trigger = { always = yes }

	tier_0 = {
		upgrade_time = { months = 0 }
		cost_to_upgrade = { factor = 0 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { }
		on_upgraded = { }
	}

	tier_1 = {
		upgrade_time = { months = 120 }
		cost_to_upgrade = { factor = 1000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { global_trade_goods_size_modifier = 0.015 }
		on_upgraded = { }
	}

	tier_2 = {
		upgrade_time = { months = 240 }
		cost_to_upgrade = { factor = 2500 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { global_trade_goods_size_modifier = 0.025 }
		on_upgraded = { }
	}

	tier_3 = {
		upgrade_time = { months = 360 }
		cost_to_upgrade = { factor = 5000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { global_trade_goods_size_modifier = 0.05 }
		on_upgraded = { }
	}
}

anequina_aqueduct = {
	start = 5222
	
	date = 54.01.01
	
	time = { months = 0 }
	
	build_cost = 1000
	can_be_moved = no
	move_days_per_unit_distance = 10
	starting_tier = 0
	
	type = monument

	build_trigger = { 
		owner = { is_free_or_tributary_trigger = yes }
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		}
	}

	on_built = { }
	on_destroyed = { }

	can_use_modifiers_trigger = {
		owner = { is_free_or_tributary_trigger = yes }
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		} 
	}
	can_upgrade_trigger = {
		owner = { is_free_or_tributary_trigger = yes }
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		} 
	}
	keep_trigger = { always = yes }

	tier_0 = {
		upgrade_time = { months = 0 }
		cost_to_upgrade = { factor = 0 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { }
		on_upgraded = { }
	}

	tier_1 = {
		upgrade_time = { months = 120 }
		cost_to_upgrade = { factor = 1000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { defensiveness = 0.10 }
		on_upgraded = { }
	}

	tier_2 = {
		upgrade_time = { months = 240 }
		cost_to_upgrade = { factor = 2500 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { defensiveness = 0.15 }
		on_upgraded = { }
	}

	tier_3 = {
		upgrade_time = { months = 360 }
		cost_to_upgrade = { factor = 5000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { defensiveness = 0.25 }
		on_upgraded = { }
	}
}

halls_of_colossus = {
	start = 913
	
	date = 54.01.01
	
	time = { months = 0 }
	
	build_cost = 1000
	can_be_moved = no
	move_days_per_unit_distance = 10
	starting_tier = 0
	
	type = monument

	build_trigger = { 
		owner = { is_free_or_tributary_trigger = yes }
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		}
	}

	on_built = { }
	on_destroyed = { }

	can_use_modifiers_trigger = {
		owner = { is_free_or_tributary_trigger = yes }
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		} 
	}
	can_upgrade_trigger = {
		owner = { is_free_or_tributary_trigger = yes }
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		} 
	}
	keep_trigger = { always = yes }

	tier_0 = {
		upgrade_time = { months = 0 }
		cost_to_upgrade = { factor = 0 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { }
		on_upgraded = { }
	}

	tier_1 = {
		upgrade_time = { months = 120 }
		cost_to_upgrade = { factor = 1000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { leader_siege = 1 }
		on_upgraded = { }
	}

	tier_2 = {
		upgrade_time = { months = 240 }
		cost_to_upgrade = { factor = 2500 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { leader_siege = 2 }
		on_upgraded = { }
	}

	tier_3 = {
		upgrade_time = { months = 360 }
		cost_to_upgrade = { factor = 5000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { leader_siege = 3 }
		on_upgraded = { }
	}
}