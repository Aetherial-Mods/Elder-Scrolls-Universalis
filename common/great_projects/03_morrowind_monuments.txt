# obsidian_gorge
# cormaris_crypt
# gnisis_mine
# barilzars_tower
# temple_of_miraak
# telvanni_mushroom_tower
# slave_market_of_tear 
# shrine_of_ularradallaku
# abandonned_site foyada ashur dan
# nchuleftingth
# ghostgate
# high_fane
# clockwork_city
# tribunal_temple

high_fane = {
	start = 2397
	
	date = 54.01.01
	
	time = { months = 0 }
	
	build_cost = 1000
	can_be_moved = no
	move_days_per_unit_distance = 10
	starting_tier = 0
	
	type = monument

	build_trigger = { 
		religion = tribunal_pantheon
		has_owner_religion = yes
		owner = {
			OR = {
				has_ruler = "Vivec"
				prestige = 35
			}
		}
	}

	on_built = { }
	on_destroyed = { }

	can_use_modifiers_trigger = {
		religion = tribunal_pantheon
		has_owner_religion = yes
		owner = {
			OR = {
				has_ruler = "Vivec"
				prestige = 35
			}
		}
	}
	can_upgrade_trigger = {
		religion = tribunal_pantheon
		has_owner_religion = yes
		owner = {
			OR = {
				has_ruler = "Vivec"
				prestige = 35
			}
		}
	}
	keep_trigger = { always = yes }

	tier_0 = {
		upgrade_time = { months = 0 }
		cost_to_upgrade = { factor = 0 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { }
		on_upgraded = { }
	}

	tier_1 = {
		upgrade_time = { months = 120 }
		cost_to_upgrade = { factor = 1000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { mercenary_discipline = 0.025 }
		on_upgraded = { }
	}

	tier_2 = {
		upgrade_time = { months = 240 }
		cost_to_upgrade = { factor = 2500 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { mercenary_discipline = 0.05 }
		on_upgraded = { }
	}

	tier_3 = {
		upgrade_time = { months = 360 }
		cost_to_upgrade = { factor = 5000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { mercenary_discipline = 0.075 }
		on_upgraded = { }
	}
}

clockwork_city = {
	start = 3246
	
	date = 54.01.01
	
	time = { months = 0 }
	
	build_cost = 1000
	can_be_moved = no
	move_days_per_unit_distance = 10
	starting_tier = 0
	
	type = monument

	build_trigger = { 
		religion = tribunal_pantheon
		has_owner_religion = yes
		owner = {
			OR = {
				has_ruler = "Sotha Sil"
				NOT = { is_lacking_institutions = yes }
			}
		}
	}

	on_built = { }
	on_destroyed = { }

	can_use_modifiers_trigger = {
		religion = tribunal_pantheon
		has_owner_religion = yes
		owner = {
			OR = {
				has_ruler = "Sotha Sil"
				NOT = { is_lacking_institutions = yes }
			}
		}
	}
	can_upgrade_trigger = {
		religion = tribunal_pantheon
		has_owner_religion = yes
		owner = {
			OR = {
				has_ruler = "Sotha Sil"
				NOT = { is_lacking_institutions = yes }
			}
		}
	}
	keep_trigger = { always = yes }

	tier_0 = {
		upgrade_time = { months = 0 }
		cost_to_upgrade = { factor = 0 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { }
		on_upgraded = { }
	}

	tier_1 = {
		upgrade_time = { months = 120 }
		cost_to_upgrade = { factor = 1000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { advisor_pool = 1 }
		on_upgraded = { }
	}

	tier_2 = {
		upgrade_time = { months = 240 }
		cost_to_upgrade = { factor = 2500 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { advisor_pool = 2 }
		on_upgraded = { }
	}

	tier_3 = {
		upgrade_time = { months = 360 }
		cost_to_upgrade = { factor = 5000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { advisor_pool = 3 }
		on_upgraded = { }
	}
}

tribunal_temple = {
	start = 3248
	
	date = 54.01.01
	
	time = { months = 0 }
	
	build_cost = 1000
	can_be_moved = no
	move_days_per_unit_distance = 10
	starting_tier = 0
	
	type = monument

	build_trigger = { 
		religion = tribunal_pantheon
		has_owner_religion = yes
		owner = {
			OR = {
				has_ruler = "Almalexia"
				num_of_cities = 1000
			}
		}
	}

	on_built = { }
	on_destroyed = { }

	can_use_modifiers_trigger = {
		religion = tribunal_pantheon
		has_owner_religion = yes
		owner = {
			OR = {
				has_ruler = "Almalexia"
				num_of_cities = 1000
			}
		}
	}
	can_upgrade_trigger = {
		religion = tribunal_pantheon
		has_owner_religion = yes
		owner = {
			OR = {
				has_ruler = "Almalexia"
				num_of_cities = 1000
			}
		}
	}
	keep_trigger = { always = yes }

	tier_0 = {
		upgrade_time = { months = 0 }
		cost_to_upgrade = { factor = 0 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { }
		on_upgraded = { }
	}

	tier_1 = {
		upgrade_time = { months = 120 }
		cost_to_upgrade = { factor = 1000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { manpower_in_true_faith_provinces = 0.05 }
		on_upgraded = { }
	}

	tier_2 = {
		upgrade_time = { months = 240 }
		cost_to_upgrade = { factor = 2500 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { manpower_in_true_faith_provinces = 0.10 }
		on_upgraded = { }
	}

	tier_3 = {
		upgrade_time = { months = 360 }
		cost_to_upgrade = { factor = 5000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { manpower_in_true_faith_provinces = 0.15 }
		on_upgraded = { }
	}
}

ghostgate = {
	start = 2362
	
	date = 54.01.01
	
	time = { months = 0 }
	
	build_cost = 1000
	can_be_moved = no
	move_days_per_unit_distance = 10
	starting_tier = 0
	
	type = monument

	build_trigger = { 
		religion = tribunal_pantheon
		has_owner_religion = yes
	}

	on_built = { }
	on_destroyed = { }

	can_use_modifiers_trigger = {
		religion = tribunal_pantheon
		has_owner_religion = yes
	}
	can_upgrade_trigger = {
		religion = tribunal_pantheon
		has_owner_religion = yes
	}
	keep_trigger = { always = yes }

	tier_0 = {
		upgrade_time = { months = 0 }
		cost_to_upgrade = { factor = 0 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { }
		on_upgraded = { }
	}

	tier_1 = {
		upgrade_time = { months = 120 }
		cost_to_upgrade = { factor = 1000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { claim_duration = 0.15 }
		on_upgraded = { }
	}

	tier_2 = {
		upgrade_time = { months = 240 }
		cost_to_upgrade = { factor = 2500 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { claim_duration = 0.35 }
		on_upgraded = { }
	}

	tier_3 = {
		upgrade_time = { months = 360 }
		cost_to_upgrade = { factor = 5000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { claim_duration = 0.50 }
		on_upgraded = { }
	}
}

nchuleftingth = {
	start = 4216
	
	date = 54.01.01
	
	time = { months = 0 }
	
	build_cost = 1000
	can_be_moved = no
	move_days_per_unit_distance = 10
	starting_tier = 0
	
	type = monument

	build_trigger = { 
		culture = dwemer
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		}
	}

	on_built = { }
	on_destroyed = { }

	can_use_modifiers_trigger = {
		culture = dwemer
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		} 
	}
	can_upgrade_trigger = {
		culture = dwemer
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		} 
	}
	keep_trigger = { always = yes }

	tier_0 = {
		upgrade_time = { months = 0 }
		cost_to_upgrade = { factor = 0 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { }
		on_upgraded = { }
	}

	tier_1 = {
		upgrade_time = { months = 120 }
		cost_to_upgrade = { factor = 1000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { claim_duration = 0.15 }
		on_upgraded = { }
	}

	tier_2 = {
		upgrade_time = { months = 240 }
		cost_to_upgrade = { factor = 2500 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { claim_duration = 0.35 }
		on_upgraded = { }
	}

	tier_3 = {
		upgrade_time = { months = 360 }
		cost_to_upgrade = { factor = 5000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { claim_duration = 0.50 }
		on_upgraded = { }
	}
}

slave_market_of_tear = {
	start = 3904
	
	date = 54.01.01
	
	time = { months = 0 }
	
	build_cost = 1000
	can_be_moved = no
	move_days_per_unit_distance = 10
	starting_tier = 0
	
	type = monument

	build_trigger = { 
		3904 = { trade_goods = slaves }
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		}
	}

	on_built = { }
	on_destroyed = { }

	can_use_modifiers_trigger = {
		3904 = { trade_goods = slaves }
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		} 
	}
	can_upgrade_trigger = {
		3904 = { trade_goods = slaves }
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		} 
	}
	keep_trigger = { always = yes }

	tier_0 = {
		upgrade_time = { months = 0 }
		cost_to_upgrade = { factor = 0 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { }
		on_upgraded = { }
	}

	tier_1 = {
		upgrade_time = { months = 120 }
		cost_to_upgrade = { factor = 1000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { trade_company_investment_cost = -0.10 }
		on_upgraded = { }
	}

	tier_2 = {
		upgrade_time = { months = 240 }
		cost_to_upgrade = { factor = 2500 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { trade_company_investment_cost = -0.25 }
		on_upgraded = { }
	}

	tier_3 = {
		upgrade_time = { months = 360 }
		cost_to_upgrade = { factor = 5000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { trade_company_investment_cost = -0.35 }
		on_upgraded = { }
	}
}

telvanni_mushroom_tower = {
	start = 4121
	
	date = 54.01.01
	
	time = { months = 0 }
	
	build_cost = 1000
	can_be_moved = no
	move_days_per_unit_distance = 10
	starting_tier = 0
	
	type = monument

	build_trigger = { 
		culture_group = velothi_cg
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		}
	}

	on_built = { }
	on_destroyed = { }

	can_use_modifiers_trigger = {
		culture_group = velothi_cg
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		} 
	}
	can_upgrade_trigger = {
		culture_group = velothi_cg
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		} 
	}
	keep_trigger = { always = yes }

	tier_0 = {
		upgrade_time = { months = 0 }
		cost_to_upgrade = { factor = 0 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { }
		on_upgraded = { }
	}

	tier_1 = {
		upgrade_time = { months = 120 }
		cost_to_upgrade = { factor = 1000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { fire_damage = 0.025 }
		on_upgraded = { }
	}

	tier_2 = {
		upgrade_time = { months = 240 }
		cost_to_upgrade = { factor = 2500 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { fire_damage = 0.05 }
		on_upgraded = { }
	}

	tier_3 = {
		upgrade_time = { months = 360 }
		cost_to_upgrade = { factor = 5000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { fire_damage = 0.075 }
		on_upgraded = { }
	}
}

temple_of_miraak = {
	start = 950
	
	date = 54.01.01
	
	time = { months = 0 }
	
	build_cost = 1000
	can_be_moved = no
	move_days_per_unit_distance = 10
	starting_tier = 0
	
	type = monument

	build_trigger = { 
		base_tax = 10
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		}
	}

	on_built = { }
	on_destroyed = { }

	can_use_modifiers_trigger = {
		base_tax = 10
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		} 
	}
	can_upgrade_trigger = {
		base_tax = 10
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		} 
	}
	keep_trigger = { always = yes }

	tier_0 = {
		upgrade_time = { months = 0 }
		cost_to_upgrade = { factor = 0 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { }
		on_upgraded = { }
	}

	tier_1 = {
		upgrade_time = { months = 120 }
		cost_to_upgrade = { factor = 1000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { missionary_maintenance_cost = -0.10 }
		on_upgraded = { owner = { country_event = { id = es_miraak.1 days = 365 random = 3720 } } }
	}

	tier_2 = {
		upgrade_time = { months = 240 }
		cost_to_upgrade = { factor = 2500 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { missionary_maintenance_cost = -0.15 }
		on_upgraded = { }
	}

	tier_3 = {
		upgrade_time = { months = 360 }
		cost_to_upgrade = { factor = 5000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { missionary_maintenance_cost = -0.25 }
		on_upgraded = { }
	}
}

barilzars_tower = {
	start = 4224
	
	date = 54.01.01
	
	time = { months = 0 }
	
	build_cost = 1000
	can_be_moved = no
	move_days_per_unit_distance = 10
	starting_tier = 0
	
	type = monument

	build_trigger = { 
		OR = {
			religion = tribunal_pantheon
			religion = reclamations_pantheon
			religion = chimer_pantheon
		}
		has_owner_religion = yes
	}

	on_built = { }
	on_destroyed = { }

	can_use_modifiers_trigger = {
		OR = {
			religion = tribunal_pantheon
			religion = reclamations_pantheon
			religion = chimer_pantheon
		}
		has_owner_religion = yes
	}
	can_upgrade_trigger = {
		OR = {
			religion = tribunal_pantheon
			religion = reclamations_pantheon
			religion = chimer_pantheon
		}
		has_owner_religion = yes
	}
	keep_trigger = { always = yes }

	tier_0 = {
		upgrade_time = { months = 0 }
		cost_to_upgrade = { factor = 0 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { }
		on_upgraded = { }
	}

	tier_1 = {
		upgrade_time = { months = 120 }
		cost_to_upgrade = { factor = 1000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { institution_spread_from_true_faith = 0.05 }
		on_upgraded = { }
	}

	tier_2 = {
		upgrade_time = { months = 240 }
		cost_to_upgrade = { factor = 2500 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { institution_spread_from_true_faith = 0.10 }
		on_upgraded = { }
	}

	tier_3 = {
		upgrade_time = { months = 360 }
		cost_to_upgrade = { factor = 5000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { institution_spread_from_true_faith = 0.15 }
		on_upgraded = { }
	}
}

gnisis_mine = {
	start = 4275
	
	date = 54.01.01
	
	time = { months = 0 }
	
	build_cost = 1000
	can_be_moved = no
	move_days_per_unit_distance = 10
	starting_tier = 0
	
	type = monument

	build_trigger = { 
		culture_group = velothi_cg
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		}
	}

	on_built = { }
	on_destroyed = { }

	can_use_modifiers_trigger = {
		culture_group = velothi_cg
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		} 
	}
	can_upgrade_trigger = {
		culture_group = velothi_cg
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		} 
	}
	keep_trigger = { always = yes }

	tier_0 = {
		upgrade_time = { months = 0 }
		cost_to_upgrade = { factor = 0 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { }
		on_upgraded = { }
	}

	tier_1 = {
		upgrade_time = { months = 120 }
		cost_to_upgrade = { factor = 1000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { production_efficiency = 0.05 }
		on_upgraded = { }
	}

	tier_2 = {
		upgrade_time = { months = 240 }
		cost_to_upgrade = { factor = 2500 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { production_efficiency = 0.075 }
		on_upgraded = { }
	}

	tier_3 = {
		upgrade_time = { months = 360 }
		cost_to_upgrade = { factor = 5000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { production_efficiency = 0.10 }
		on_upgraded = { }
	}
}

cormaris_crypt = {
	start = 3204
	
	date = 54.01.01
	
	time = { months = 0 }
	
	build_cost = 1000
	can_be_moved = no
	move_days_per_unit_distance = 10
	starting_tier = 0
	
	type = monument

	build_trigger = { 
		OR = {
			culture_group = dwemer_cg
			culture_group = velothi_cg
		}
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		}
	}

	on_built = { }
	on_destroyed = { }

	can_use_modifiers_trigger = {
		OR = {
			culture_group = dwemer_cg
			culture_group = velothi_cg
		}
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		} 
	}
	can_upgrade_trigger = {
		OR = {
			culture_group = dwemer_cg
			culture_group = velothi_cg
		}
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		} 
	}
	keep_trigger = { always = yes }

	tier_0 = {
		upgrade_time = { months = 0 }
		cost_to_upgrade = { factor = 0 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { }
		on_upgraded = { }
	}

	tier_1 = {
		upgrade_time = { months = 120 }
		cost_to_upgrade = { factor = 1000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { years_of_nationalism = -2.5 }
		on_upgraded = { }
	}

	tier_2 = {
		upgrade_time = { months = 240 }
		cost_to_upgrade = { factor = 2500 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { years_of_nationalism = -5 }
		on_upgraded = { }
	}

	tier_3 = {
		upgrade_time = { months = 360 }
		cost_to_upgrade = { factor = 5000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { years_of_nationalism = -7.5 }
		on_upgraded = { }
	}
}

obsidian_gorge = {
	start = 3975
	
	date = 54.01.01
	
	time = { months = 0 }
	
	build_cost = 1000
	can_be_moved = no
	move_days_per_unit_distance = 10
	starting_tier = 0
	
	type = monument

	build_trigger = { 
		OR = {
			culture_group = dwemer_cg
			culture_group = velothi_cg
		}
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		}
	}

	on_built = { }
	on_destroyed = { }

	can_use_modifiers_trigger = {
		OR = {
			culture_group = dwemer_cg
			culture_group = velothi_cg
		}
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		} 
	}
	can_upgrade_trigger = {
		OR = {
			culture_group = dwemer_cg
			culture_group = velothi_cg
		}
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		} 
	}
	keep_trigger = { always = yes }

	tier_0 = {
		upgrade_time = { months = 0 }
		cost_to_upgrade = { factor = 0 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { }
		on_upgraded = { }
	}

	tier_1 = {
		upgrade_time = { months = 120 }
		cost_to_upgrade = { factor = 1000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { artillery_power = 0.025 }
		on_upgraded = { }
	}

	tier_2 = {
		upgrade_time = { months = 240 }
		cost_to_upgrade = { factor = 2500 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { artillery_power = 0.05 }
		on_upgraded = { }
	}

	tier_3 = {
		upgrade_time = { months = 360 }
		cost_to_upgrade = { factor = 5000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { artillery_power = 0.075 }
		on_upgraded = { }
	}
}

shrine_of_ularradallaku = {
	start = 4262
	
	date = 54.01.01
	
	time = { months = 0 }
	
	build_cost = 1000
	can_be_moved = no
	move_days_per_unit_distance = 10
	starting_tier = 0
	
	type = monument

	build_trigger = { 
		religion = mehrunes_dagon_cult
		has_owner_religion = yes
	}

	on_built = { }
	on_destroyed = { }

	can_use_modifiers_trigger = {
		religion = mehrunes_dagon_cult
		has_owner_religion = yes
	}
	can_upgrade_trigger = {
		religion = mehrunes_dagon_cult
		has_owner_religion = yes
	}
	keep_trigger = { always = yes }

	tier_0 = {
		upgrade_time = { months = 0 }
		cost_to_upgrade = { factor = 0 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { }
		on_upgraded = { }
	}

	tier_1 = {
		upgrade_time = { months = 120 }
		cost_to_upgrade = { factor = 1000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { discipline = 0.025 }
		on_upgraded = { }
	}

	tier_2 = {
		upgrade_time = { months = 240 }
		cost_to_upgrade = { factor = 2500 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { discipline = 0.05 }
		on_upgraded = { }
	}

	tier_3 = {
		upgrade_time = { months = 360 }
		cost_to_upgrade = { factor = 5000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { discipline = 0.075 }
		on_upgraded = { }
	}
}

abandonned_site = {
	start = 4232
	
	date = 54.01.01
	
	time = { months = 0 }
	
	build_cost = 1000
	can_be_moved = no
	move_days_per_unit_distance = 10
	starting_tier = 0
	
	type = monument

	build_trigger = { 
		culture_group = dwemer_cg 
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		}
	}

	on_built = { }
	on_destroyed = { }

	can_use_modifiers_trigger = {
		culture_group = dwemer_cg 
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		} 
	}
	can_upgrade_trigger = {
		culture_group = dwemer_cg 
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		} 
	}
	keep_trigger = { always = yes }

	tier_0 = {
		upgrade_time = { months = 0 }
		cost_to_upgrade = { factor = 0 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { }
		on_upgraded = { }
	}

	tier_1 = {
		upgrade_time = { months = 120 }
		cost_to_upgrade = { factor = 1000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { war_exhaustion = -0.015 }
		on_upgraded = { }
	}

	tier_2 = {
		upgrade_time = { months = 240 }
		cost_to_upgrade = { factor = 2500 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { war_exhaustion = -0.025 }
		on_upgraded = { }
	}

	tier_3 = {
		upgrade_time = { months = 360 }
		cost_to_upgrade = { factor = 5000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { war_exhaustion = -0.05 }
		on_upgraded = { }
	}
}