#tor_hame_khard
#karnwasten
#king_haven_pass
#castle_rilis
#ceporah_tower

ceporah_tower = {
	start = 2269
	date = 54.01.01
	time = {
		months = 0
	}
	build_cost = 1000
	can_be_moved = no
	move_days_per_unit_distance = 10
	starting_tier = 0
	type = monument
	build_trigger = {
		religion = old_way_cult
		has_owner_religion = yes
	}
	on_built = {
	}
	on_destroyed = {
	}
	can_use_modifiers_trigger = {
		religion = old_way_cult
		has_owner_religion = yes
	}
	can_upgrade_trigger = {
		religion = old_way_cult
		has_owner_religion = yes
	}
	keep_trigger = {
		always = yes
	}
	tier_0 = {
		upgrade_time = { months = 0 }
		cost_to_upgrade = { factor = 0 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { }
		on_upgraded = { }
	}

	tier_1 = {
		upgrade_time = { months = 120 }
		cost_to_upgrade = { factor = 1000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { advisor_pool = 1 }
		on_upgraded = { }
	}

	tier_2 = {
		upgrade_time = { months = 240 }
		cost_to_upgrade = { factor = 2500 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { advisor_pool = 2 }
		on_upgraded = { }
	}

	tier_3 = {
		upgrade_time = { months = 360 }
		cost_to_upgrade = { factor = 5000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { advisor_pool = 3 }
		on_upgraded = { }
	}
}

castle_rilis = {
	start = 4673
	date = 54.01.01
	time = {
		months = 0
	}
	build_cost = 1000 
	can_be_moved = no
	move_days_per_unit_distance = 10
	starting_tier = 0
	type = monument
	build_trigger = {
		fort_level = 1
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		}
	}
	on_built = {
	}
	on_destroyed = {
	}
	can_use_modifiers_trigger = {
		fort_level = 1
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		}
	}
	can_upgrade_trigger = {
		fort_level = 1
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		}
	}
	keep_trigger = {
		always = yes
	}
	tier_0 = {
		upgrade_time = { months = 0 }
		cost_to_upgrade = { factor = 0 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { }
		on_upgraded = { }
	}

	tier_1 = {
		upgrade_time = {
			months = 60 
		}
		cost_to_upgrade = {
			factor = 1000 
		}
		province_modifiers = { 
			fort_level = 1
		}
		area_modifier = { 
			local_defensiveness = 0.10
		}
		country_modifiers = { }
		on_upgraded = { }
	}
	tier_2 = {
		upgrade_time = {
			months = 48
		}
		cost_to_upgrade = {
			factor = 2500 
		}
		province_modifiers = { 
			fort_level = 2
		}
		area_modifier = { 
			local_defensiveness = 0.15
		}
		country_modifiers = { }
		on_upgraded = { }
	}
	tier_3 = {
		upgrade_time = {
			months = 72
		}
		cost_to_upgrade = {
			factor = 5000 
		}
		province_modifiers = { 
			fort_level = 3
		}
		area_modifier = {
			local_defensiveness = 0.25
		}
		country_modifiers = { }
		on_upgraded = { }
	}
}

king_haven_pass = {
	start = 4783
	date = 54.01.01
	time = {
		months = 0
	}
	build_cost = 1000 
	can_be_moved = no
	move_days_per_unit_distance = 10
	starting_tier = 0
	type = canal
	build_trigger = {
		fort_level = 1
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		}
	}
	on_built = {
	}
	on_destroyed = {
		4783 = { remove_canal = king_haven_pass }
	}
	can_use_modifiers_trigger = {
		fort_level = 1
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		}
	}
	can_upgrade_trigger = {
		fort_level = 1
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		}
	}
	keep_trigger = {
		always = yes
	}
	tier_0 = {
		upgrade_time = { months = 0 }
		cost_to_upgrade = { factor = 0 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { }
		on_upgraded = { }
	}

	tier_1 = {
		upgrade_time = { months = 120 }
		cost_to_upgrade = { factor = 1000 }
		province_modifiers = { 
			fort_level = 1
		}
		area_modifier = {
			local_hostile_movement_speed = -0.05
		}
		country_modifiers = { 
		}
		on_upgraded = { add_canal = king_haven_pass custom_tooltip = king_haven_pass_canal_tooltip }
	}

	tier_2 = {
		upgrade_time = { months = 240 }
		cost_to_upgrade = { factor = 2500 }
		province_modifiers = { 
			fort_level = 2
		}
		area_modifier = {
			local_hostile_movement_speed = -0.10
		}
		country_modifiers = { 
		}
		on_upgraded = { add_canal = king_haven_pass custom_tooltip = king_haven_pass_canal_tooltip }
	}

	tier_3 = {
		upgrade_time = { months = 360 }
		cost_to_upgrade = { factor = 5000 }
		province_modifiers = { 
			fort_level = 3
		}
		area_modifier = {
			local_hostile_movement_speed = -0.15
		}
		country_modifiers = { 
		}
		on_upgraded = { add_canal = king_haven_pass custom_tooltip = king_haven_pass_canal_tooltip }
	}
}

karnwasten = {
	start = 4781
	date = 54.01.01
	time = {
		months = 0
	}
	build_cost = 1000
	can_be_moved = no
	move_days_per_unit_distance = 10
	starting_tier = 0
	type = monument
	build_trigger = {
			has_owner_religion = yes
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		}
	}
	on_built = {
	}
	on_destroyed = {
	}
	can_use_modifiers_trigger = {
			has_owner_religion = yes
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		}
	}
	can_upgrade_trigger = {
			has_owner_religion = yes
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		}
	}
	keep_trigger = {
		always = yes
	}
	tier_0 = {
		upgrade_time = { months = 0 }
		cost_to_upgrade = { factor = 0 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { }
		on_upgraded = { }
	}

	tier_1 = {
		upgrade_time = {
			months = 120
		}
		cost_to_upgrade = {
			factor = 1000 
		}
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { 
			global_prov_trade_power_modifier = 0.015
		}
		on_upgraded = { }
	}
	tier_2 = {
		upgrade_time = {
			months = 240
		}
		cost_to_upgrade = {
			factor = 2500 
		}
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { 
			global_prov_trade_power_modifier = 0.025
		}
		on_upgraded = { }
	}
	tier_3 = {
		upgrade_time = {
			months = 360
		}
		cost_to_upgrade = {
			factor = 5000 
		}
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { 
			global_prov_trade_power_modifier = 0.05
		}
		on_upgraded = { }
	}
}

tor_hame_khard = {
	start = 280
	date = 54.01.01
	time = {
		months = 0
	}
	build_cost = 1000
	can_be_moved = no
	move_days_per_unit_distance = 10
	starting_tier = 0
	type = monument
	build_trigger = {
		has_owner_religion = yes
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		}
	}
	on_built = {
	}
	on_destroyed = {
	}
	can_use_modifiers_trigger = {
		has_owner_religion = yes
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		}
	}
	can_upgrade_trigger = {
		has_owner_religion = yes
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		}
	}
	keep_trigger = {
		always = yes
	}
	tier_0 = {
		upgrade_time = { months = 0 }
		cost_to_upgrade = { factor = 0 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { }
		on_upgraded = { }
	}

	tier_1 = {
		upgrade_time = {
			months = 120
		}
		cost_to_upgrade = {
			factor = 1000 
		}
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = {
			morale_damage = 0.025
		}
		on_upgraded = { }
	}
	tier_2 = {
		upgrade_time = {
			months = 240
		}
		cost_to_upgrade = {
			factor = 2500 
		}
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = {
			morale_damage = 0.05
		}
		on_upgraded = { }
	}
	tier_3 = {
		upgrade_time = {
			months = 360
		}
		cost_to_upgrade = {
			factor = 5000 
		}
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = {
			morale_damage = 0.075
		}
		on_upgraded = { }
	}
}