# chapel_of_light
# the_everfull_flagon
# font_of_shcems
# the_vile_laboratory
# tower_of_lies

tower_of_lies = {
	start = 2744
	
	date = 54.01.01
	
	time = { months = 0 }
	
	build_cost = 1000
	can_be_moved = no
	move_days_per_unit_distance = 10
	starting_tier = 0
	
	type = monument

	build_trigger = { 
		OR = {
			culture_group = daedra_cg
			culture_group = dead_cg
		}
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		}
	}

	on_built = { }
	on_destroyed = { }

	can_use_modifiers_trigger = {
		OR = {
			culture_group = daedra_cg
			culture_group = dead_cg
		}
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		} 
	}
	can_upgrade_trigger = {
		OR = {
			culture_group = daedra_cg
			culture_group = dead_cg
		}
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		} 
	}
	keep_trigger = { always = yes }

	tier_0 = {
		upgrade_time = { months = 0 }
		cost_to_upgrade = { factor = 0 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { }
		on_upgraded = { }
	}

	tier_1 = {
		upgrade_time = { months = 120 }
		cost_to_upgrade = { factor = 1000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { diplomatic_upkeep = 1 }
		on_upgraded = { }
	}

	tier_2 = {
		upgrade_time = { months = 240 }
		cost_to_upgrade = { factor = 2500 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { diplomatic_upkeep = 2 }
		on_upgraded = { }
	}

	tier_3 = {
		upgrade_time = { months = 360 }
		cost_to_upgrade = { factor = 5000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { diplomatic_upkeep = 3 }
		on_upgraded = { }
	}
}

the_vile_laboratory = {
	start = 2903
	
	date = 54.01.01
	
	time = { months = 0 }
	
	build_cost = 1000
	can_be_moved = no
	move_days_per_unit_distance = 10
	starting_tier = 0
	
	type = monument

	build_trigger = { 
		owner = { 
			is_free_or_tributary_trigger = yes
		}
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		}
	}

	on_built = { }
	on_destroyed = { }

	can_use_modifiers_trigger = {
		owner = { 
			is_free_or_tributary_trigger = yes
		}
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		} 
	}
	can_upgrade_trigger = {
		owner = { 
			is_free_or_tributary_trigger = yes
		}
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		} 
	}
	keep_trigger = { always = yes }

	tier_0 = {
		upgrade_time = { months = 0 }
		cost_to_upgrade = { factor = 0 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { }
		on_upgraded = { }
	}

	tier_1 = {
		upgrade_time = { months = 120 }
		cost_to_upgrade = { factor = 1000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { artillery_fire = 0.05 }
		on_upgraded = { }
	}

	tier_2 = {
		upgrade_time = { months = 240 }
		cost_to_upgrade = { factor = 2500 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { artillery_fire = 0.10 }
		on_upgraded = { }
	}

	tier_3 = {
		upgrade_time = { months = 360 }
		cost_to_upgrade = { factor = 5000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { artillery_fire = 0.15 }
		on_upgraded = { }
	}
}

font_of_shcems = {
	start = 2880
	
	date = 54.01.01
	
	time = { months = 0 }
	
	build_cost = 1000
	can_be_moved = no
	move_days_per_unit_distance = 10
	starting_tier = 0
	
	type = monument

	build_trigger = { 
		religion = molag_bal_cult
		has_owner_religion = yes
	}

	on_built = { }
	on_destroyed = { }

	can_use_modifiers_trigger = {
		religion = molag_bal_cult
		has_owner_religion = yes
	}
	can_upgrade_trigger = {
		religion = molag_bal_cult
		has_owner_religion = yes
	}
	keep_trigger = { always = yes }

	tier_0 = {
		upgrade_time = { months = 0 }
		cost_to_upgrade = { factor = 0 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { }
		on_upgraded = { }
	}

	tier_1 = {
		upgrade_time = { months = 120 }
		cost_to_upgrade = { factor = 1000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { loot_amount = 0.15 }
		on_upgraded = { }
	}

	tier_2 = {
		upgrade_time = { months = 240 }
		cost_to_upgrade = { factor = 2500 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { loot_amount = 0.25 }
		on_upgraded = { }
	}

	tier_3 = {
		upgrade_time = { months = 360 }
		cost_to_upgrade = { factor = 5000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { loot_amount = 0.35 }
		on_upgraded = { }
	}
}

the_everfull_flagon = {
	start = 2953
	
	date = 54.01.01
	
	time = { months = 0 }
	
	build_cost = 1000
	can_be_moved = no
	move_days_per_unit_distance = 10
	starting_tier = 0
	
	type = monument

	build_trigger = { 
		owner = { 
			is_free_or_tributary_trigger = yes
		}
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		}
	}

	on_built = { }
	on_destroyed = { }

	can_use_modifiers_trigger = {
		owner = { 
			is_free_or_tributary_trigger = yes
		}
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		} 
	}
	can_upgrade_trigger = {
		owner = { 
			is_free_or_tributary_trigger = yes
		}
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		} 
	}
	keep_trigger = { always = yes }

	tier_0 = {
		upgrade_time = { months = 0 }
		cost_to_upgrade = { factor = 0 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { }
		on_upgraded = { }
	}

	tier_1 = {
		upgrade_time = { months = 120 }
		cost_to_upgrade = { factor = 1000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { global_own_trade_power = 0.05 }
		on_upgraded = { }
	}

	tier_2 = {
		upgrade_time = { months = 240 }
		cost_to_upgrade = { factor = 2500 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { global_own_trade_power = 0.10 }
		on_upgraded = { }
	}

	tier_3 = {
		upgrade_time = { months = 360 }
		cost_to_upgrade = { factor = 5000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { global_own_trade_power = 0.15 }
		on_upgraded = { }
	}
}

chapel_of_light = {
	start = 2710
	
	date = 54.01.01
	
	time = { months = 0 }
	
	build_cost = 1000
	can_be_moved = no
	move_days_per_unit_distance = 10
	starting_tier = 0
	
	type = monument

	build_trigger = { 
		owner = { 
			great_power_rank = 7
		}
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		}
	}

	on_built = { }
	on_destroyed = { }

	can_use_modifiers_trigger = {
		owner = { 
			great_power_rank = 7
		}
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		} 
	}
	can_upgrade_trigger = {
		owner = { 
			great_power_rank = 7
		}
		OR = {
			has_owner_culture = yes
			has_owner_accepted_culture = yes
		} 
	}
	keep_trigger = { always = yes }

	tier_0 = {
		upgrade_time = { months = 0 }
		cost_to_upgrade = { factor = 0 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { }
		on_upgraded = { }
	}

	tier_1 = {
		upgrade_time = { months = 120 }
		cost_to_upgrade = { factor = 1000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { dip_tech_cost_modifier = -0.025 }
		on_upgraded = { }
	}

	tier_2 = {
		upgrade_time = { months = 240 }
		cost_to_upgrade = { factor = 2500 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { dip_tech_cost_modifier = -0.05 }
		on_upgraded = { }
	}

	tier_3 = {
		upgrade_time = { months = 360 }
		cost_to_upgrade = { factor = 5000 }
		province_modifiers = { }
		area_modifier = { }
		country_modifiers = { dip_tech_cost_modifier = -0.075 }
		on_upgraded = { }
	}
}