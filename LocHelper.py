import os

directory = os.getcwd()

file_list = list()

try:
    # Just to be sure that those directories exist
    if not os.path.exists(directory + "\\localisation"):
        os.makedirs(directory + "\\localisation")
        
    if not os.path.exists(directory + "\\localisation\\english"):
        os.makedirs(directory + "\\localisation\\english")
    
    with open(directory+"\\localisation\\english\\loc_helper_events_l_english.yml", "w", encoding='utf-8-sig') as localization_events_output:
        localization_events_output.write("l_english:\n\n")
        localization_events_output.write(" # This file was generated using the LocHelper.\n # Under NO Circumstances DO NOT change the content of this file, as your changes will be overwritten!\n # You should edit the original .txt file instead and launch LocHelper afterwards.\n\n")
        
        # List all the files
        for root, dirs, files in os.walk(os.path.abspath(directory+"\\events")):
            for file in files:
                file_list.append(os.path.join(root, file))
        
        for file in file_list:
            text_file = open(file, "r", encoding="utf8")
            text_file_lines = text_file.readlines()
            text_file.close()
            
            # Check if localisation should be loaded
            if("UseLocHelper = yes" in text_file_lines[0]):
                print("Working with file:"+file)
                localization_events_output.write(" # "+os.path.basename(file)+"\n")
                for text_file_line in text_file_lines:
                    if("#" in text_file_line and ":" in text_file_line and '"' in text_file_line):
                        localisation_line = text_file_line.replace('#', '').lstrip()
                        localization_events_output.write(" "+localisation_line)
    
    print("Done!")
    
except BaseException:
    import sys
    print(sys.exc_info()[0])
    import traceback
    print(traceback.format_exc())
finally:
    print("Press Enter to continue ...")
    input()