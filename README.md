# Elder Scrolls Universalis
This is the developement repository for Elder Scrolls Universalis, a total overhaul mod for Europa Universalis IV.

## Download Links
* [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=1449952810)
* [Paradox Mods](https://mods.paradoxplaza.com/mods/20012/Any)
* [Codeberg (Direct Download)](https://codeberg.org/Aetherial-Mods/Elder-Scrolls-Universalis/releases/latest)

## Other Links
* [ESU Wiki](https://elder-scrolls-universalis.fandom.com/wiki/Elder_Scrolls_Universalis_Wiki)
* [ESU Discord](https://discordapp.com/invite/bGVcZyK)
* [ESU Subreddit](https://www.reddit.com/r/elderscrolls_mod/)